package com.orb.tour.bean;

import java.util.Date;
import java.util.List;

public class TourCluster {
	private String planningHorizon;
	private String quarterlyCycle;
	private String monthOfMaintenance;
	private String yearOfMaintenance;
	private String employee;
	private List<String> facilitiesList;
	private List<String> employeeList;
	private String korfmannOfficeLocation;
	private String workingHours;
	private String maintenanceCycleConsideration;
	private String threeTimesYearMaintenance;
	private String halfYearlyMaintenance;
	private String breakAtWork;
	private Date dueDate;
	private String project;
	private String estimatedTime;
	private String considerAbsence;
	
	public String getEstimatedTime() {
		return estimatedTime;
	}
	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getBreakAtWork() {
		return breakAtWork;
	}
	public void setBreakAtWork(String breakAtWork) {
		this.breakAtWork = breakAtWork;
	}
	public String getThreeTimesYearMaintenance() {
		return threeTimesYearMaintenance;
	}
	public void setThreeTimesYearMaintenance(String threeTimesYearMaintenance) {
		this.threeTimesYearMaintenance = threeTimesYearMaintenance;
	}
	public String getHalfYearlyMaintenance() {
		return halfYearlyMaintenance;
	}
	public void setHalfYearlyMaintenance(String halfYearlyMaintenance) {
		this.halfYearlyMaintenance = halfYearlyMaintenance;
	}
	public String getMaintenanceCycleConsideration() {
		return maintenanceCycleConsideration;
	}
	public void setMaintenanceCycleConsideration(String maintenanceCycleConsideration) {
		this.maintenanceCycleConsideration = maintenanceCycleConsideration;
	}
	public String getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}
	public String getKorfmannOfficeLocation() {
		return korfmannOfficeLocation;
	}
	public void setKorfmannOfficeLocation(String korfmannOfficeLocation) {
		this.korfmannOfficeLocation = korfmannOfficeLocation;
	}
	public String getPlanningHorizon() {
		return planningHorizon;
	}
	public void setPlanningHorizon(String planningHorizon) {
		this.planningHorizon = planningHorizon;
	}
	public String getQuarterlyCycle() {
		return quarterlyCycle;
	}
	public void setQuarterlyCycle(String quarterlyCycle) {
		this.quarterlyCycle = quarterlyCycle;
	}
	public String getMonthOfMaintenance() {
		return monthOfMaintenance;
	}
	public void setMonthOfMaintenance(String monthOfMaintenance) {
		this.monthOfMaintenance = monthOfMaintenance;
	}
	public String getYearOfMaintenance() {
		return yearOfMaintenance;
	}
	public void setYearOfMaintenance(String yearOfMaintenance) {
		this.yearOfMaintenance = yearOfMaintenance;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public List<String> getFacilitiesList() {
		return facilitiesList;
	}
	public void setFacilitiesList(List<String> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}
	public List<String> getEmployeeList() {
		return employeeList;
	}
	public void setEmployeeList(List<String> employeeList) {
		this.employeeList = employeeList;
	}
	public String getConsiderAbsence() {
		return considerAbsence;
	}
	public void setConsiderAbsence(String considerAbsence) {
		this.considerAbsence = considerAbsence;
	}
	
	
}
