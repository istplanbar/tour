package com.orb.tour.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.employee.dao.EmployeeDAO;
import com.orb.employee.dao.VacationDAO;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.dao.FacilitiesDAO;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.dao.TaskDAO;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.bean.TourCluster;
import com.orb.tour.constants.TourConstants;
import com.orb.tour.dao.KorfmannLocationDAO;
import com.orb.tour.dao.TourDAO;
import com.orb.tour.dao.TourPDFDAO;
import com.orb.tour.service.TourService;

@Service
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TourServiceImpl implements TourService {
	private static Log logger = LogFactory.getLog(TourServiceImpl.class);

	@Autowired
	TourDAO tourDAO;

	@Autowired
	KorfmannLocationDAO korfmannDAO;

	@Autowired
	TourPDFDAO tourPDFDAO;

	@Autowired
	TaskDAO taskDAO;

	@Autowired
	FacilitiesDAO facilitiesDAO;

	@Autowired
	EmployeeDAO employeeDAO;

	@Autowired
	VacationDAO vactionDAO;

	/**
	 * This method calls DAO to get all Inactive tour from DB
	 * 
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Tour> getInactiveTourList(CommonError commonError) {
		return tourDAO.getTourList(commonError, CommonConstants.N);
	}

	/**
	 * This method calls DAO to get all Inactive tour list from DB
	 * 
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Tour> getActiveTourList(CommonError commonError) {
		return tourDAO.getTourList(commonError, CommonConstants.Y);
	}

	/**
	 * This method will interact with DAO to get tour in DB
	 * 
	 * @param commonError
	 * @param tourId
	 * @return
	 */
	@Override
	public Tour getTour(CommonError commonError, String tourId) {
		return tourDAO.getTour(commonError, tourId);
	}

	/**
	 * This method calls DAO to de-activate tour
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param selectedTourList
	 */
	@Override
	public void lockTour(CommonError commonError, User loginUser, List<Tour> selectedTourList) {
		List<String> idList = new ArrayList<>();
		if (selectedTourList == null || selectedTourList.isEmpty()) {
			return;
		}
		for (Tour tour : selectedTourList) {
			idList.add(tour.getId());
		}
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.N);
	}

	/**
	 * This method calls DAO to activate the locked tour
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param selectedTourList
	 */
	@Override
	public void unlockTour(CommonError commonError, User loginUser, List<Tour> selectedTourList) {
		List<String> idList = new ArrayList<>();
		if (selectedTourList == null || selectedTourList.isEmpty()) {
			logger.info("No record is available to activate");
			return;
		}
		for (Tour tour : selectedTourList) {
			idList.add(tour.getId());
		}
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);
	}

	/**
	 * This method will interact with DAO to delete a tour in DB
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param selectedTourList
	 */
	@Override
	public void deleteTour(CommonError commonError, User loginUser, List<Tour> selectedTourList) {
		List<String> idList = new ArrayList<>();
		if (selectedTourList == null || selectedTourList.isEmpty()) {
			logger.info("No record is available to activate");
			return;
		}
		for (Tour tour : selectedTourList) {
			idList.add(tour.getId());
		}
		tourDAO.deleteTour(commonError, loginUser, idList);
	}

	/**
	 * This method calls DAO to deletes particular tour
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	@Override
	public void deleteViewedTour(CommonError commonError, User loginUser, Tour tour) {
		List<String> idList = new ArrayList<>();
		idList.add(tour.getId());
		tourDAO.deleteTour(commonError, loginUser, idList);
	}

	/**
	 * This method calls DAO to de-activate tour
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	@Override
	public void lockViewedTour(CommonError commonError, User loginUser, Tour tour) {
		List<String> idList = new ArrayList<>();
		idList.add(tour.getId());
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.N);
	}

	/**
	 * This method calls DAO to activate tour
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	@Override
	public void unlockViewedTour(CommonError commonError, User loginUser, Tour tour) {
		List<String> idList = new ArrayList<>();
		idList.add(tour.getId());
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);
	}

	/**
	 * This method will interact with DAO to insert a tour in DB
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	@Override
	public void add(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds) {
		tourDAO.addTour(commonError, loginUser, tour, facilityIds);
	}

	/**
	 * This method will interact with DAO to update a tour in DB
	 * 
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	@Override
	public void update(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds) {
		tourDAO.updateTour(commonError, loginUser, tour, facilityIds);
	}

	@Override
	public List<String> getTourFacilitiesList(CommonError commonError, String tourId) {
		return tourDAO.getTourFacilitiesList(commonError, tourId);
	}

	@Override
	public List<KorfAddress> getInactiveKorfAddressList(CommonError commonError) {
		return korfmannDAO.getKorfAddressList(commonError, CommonConstants.N);
	}

	@Override
	public List<KorfAddress> getActiveKorfAddressList(CommonError commonError) {
		return korfmannDAO.getKorfAddressList(commonError, CommonConstants.Y);
	}

	@Override
	public KorfAddress getKorfAddress(CommonError commonError, String korfAddressId) {
		return korfmannDAO.getKorfAddress(commonError, korfAddressId);
	}

	@Override
	public void addAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		korfmannDAO.addAddress(commonError, loginUser, korfAddress);
	}

	@Override
	public void updateAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		korfmannDAO.updateAddress(commonError, loginUser, korfAddress);

	}

	@Override
	public void lockKorfAddress(CommonError commonError, User loginUser, List<KorfAddress> selectedKorfAddressList) {
		List<String> idList = new ArrayList<>();
		if (selectedKorfAddressList == null || selectedKorfAddressList.isEmpty()) {
			return;
		}
		for (KorfAddress korfAddress : selectedKorfAddressList) {
			idList.add(korfAddress.getId());
		}
		korfmannDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.N);

	}

	@Override
	public void unlockKorfAddress(CommonError commonError, User loginUser, List<KorfAddress> selectedKorfAddressList) {
		List<String> idList = new ArrayList<>();
		if (selectedKorfAddressList == null || selectedKorfAddressList.isEmpty()) {
			return;
		}
		for (KorfAddress korfAddress : selectedKorfAddressList) {
			idList.add(korfAddress.getId());
		}
		korfmannDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);

	}

	@Override
	public void deleteKorfAddress(CommonError commonError, User loginUser, List<KorfAddress> selectedKorfAddressList) {
		List<String> idList = new ArrayList<>();
		if (selectedKorfAddressList == null || selectedKorfAddressList.isEmpty()) {
			logger.info("No record is available to activate");
			return;
		}
		for (KorfAddress korfAddress : selectedKorfAddressList) {
			idList.add(korfAddress.getId());
		}
		korfmannDAO.deleteKorfAddress(commonError, loginUser, idList);

	}

	@Override
	public void deleteViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		List<String> idList = new ArrayList<>();
		idList.add(korfAddress.getId());
		korfmannDAO.deleteKorfAddress(commonError, loginUser, idList);

	}

	@Override
	public void lockViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		List<String> idList = new ArrayList<>();
		idList.add(korfAddress.getId());
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.N);

	}

	@Override
	public void unlockViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		List<String> idList = new ArrayList<>();
		idList.add(korfAddress.getId());
		tourDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);
	}

	@Override
	public void createPDF(CommonError commonError, String tourId) {
		tourPDFDAO.createPDF(commonError, tourId);

	}

	@Async
	public Future<List<Task>> getActiveTaskList(CommonError commonError) {
		List<Task> tasksList = new ArrayList<>();
		try {
			tasksList = taskDAO.getActiveTaskList(commonError, CommonConstants.Y);
			if (!tasksList.isEmpty()) {
				tasksList = tasksList.stream()
						.filter(t -> t.getStatus() != null
						&& !t.getStatus().equals(TaskPlannerConstants.TOUR_TASKS_STATUS))
						.collect(Collectors.toList());
			}
		} catch (Exception e) {
			logger.info("Task Getting Error" + e.getMessage());
		}
		return new AsyncResult<List<Task>>(tasksList);
	}

	@Async
	public Future<List<Facilities>> getActiveFacilitiesList(CommonError commonError) {
		return new AsyncResult<List<Facilities>>(facilitiesDAO.getFacilitiesList(commonError, CommonConstants.Y));
	}

	@Async
	public Future<List<Employee>> getActiveEmployeeList(CommonError commonError) {
		return new AsyncResult<List<Employee>>(employeeDAO.getEmployeeList(commonError, CommonConstants.Y));
	}

	@Async
	public Future<List<String>> getMaintenanceYearList() {
		logger.info("Get Year List info Start");
		List<String> yearList = new ArrayList<>();
		int yearNo = Year.now().getValue();
		yearList = IntStream.rangeClosed(yearNo, Integer.valueOf(TourConstants.TOUR_FACILITY_BUILT_END_YEAR)).boxed()
				.map(Object::toString).collect(Collectors.toList());
		logger.info("Get Year List info End");
		return new AsyncResult<List<String>>(yearList);
	}

	public Date getLastTourDate(CommonError commonError, String empId, int month, int year) {
		return tourDAO.getLatestTourDate(commonError, empId, month, year);
	}

	@Async
	public Future<List<Vacation>> getActiveVacationList(CommonError commonError) {
		return new AsyncResult<List<Vacation>>(vactionDAO.getVacationList(commonError, CommonConstants.Y));
	}

	@Override
	public Date getNextPossibleTourDate(CommonError commonError, String empId, TourCluster tourCluster) {
		logger.info("Get Next Possible Tour Date method is entered");
		Date tourDate = null;
		
		switch (tourCluster.getPlanningHorizon()) {
		case "1":
			tourDate = getLastTourDate(commonError, empId, Integer.parseInt(tourCluster.getMonthOfMaintenance()),
					Integer.parseInt(tourCluster.getYearOfMaintenance()));
			break;

		case "2":
			int returnedQuarter = Integer.parseInt(tourCluster.getQuarterlyCycle());
			int maxMonth = returnedQuarter * 3;
			int minMonth = maxMonth - 2;

			for (int month = minMonth; month <= maxMonth; month++) {
				tourDate = getLastTourDate(commonError, empId, month, Integer.parseInt(tourCluster.getYearOfMaintenance()));
				if (tourDate != null) {
					break;
				}
			}
			break;
		case "3":
			if (Integer.parseInt(tourCluster.getYearOfMaintenance()) == new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear()) {
				tourDate = getLastTourDate(commonError, empId, 0, Integer.parseInt(tourCluster.getYearOfMaintenance()));
			}else{
				for (int month = 1; month <= 12; month++) {
					tourDate = getLastTourDate(commonError, empId, month, Integer.parseInt(tourCluster.getYearOfMaintenance()));
					if (tourDate != null) {
						break;
					}
				}
			}
			break;
		}
		logger.info("Based on planning inputs, previouse tourDate is retrieved");
		tourDate = getPossibleTourDate(commonError, tourDate, empId, tourCluster);
		return tourDate;
	}

	public Date getPossibleTourDate(CommonError commonError, Date trDate, String empId, TourCluster tourCluster) {
		logger.info("Possible Tour Date method is entered.");
		Date possibleTourDate = null;
		List<Vacation> empVacationList = null;
		try{
			Calendar calendar = Calendar.getInstance();
			if (trDate == null || trDate.before(new Date())) {
				possibleTourDate = returnFirstDateBasedOnInput(tourCluster);
			} else {
				possibleTourDate = trDate;
			}
			calendar.setTime(possibleTourDate);
			calendar.add(Calendar.DATE, 1);
			possibleTourDate = calendar.getTime();

			List<Vacation> vacationList = getActiveVacationList(commonError).get();

			if(vacationList != null){
				empVacationList = vacationList.stream()
						.filter(vacation -> Arrays.asList(vacation.getEmployee().split(",")).stream().map(String::trim).collect(Collectors.toList()).contains(empId))
						.collect(Collectors.toList());
			}

			if (!empVacationList.isEmpty()) {
				possibleTourDate = getPossibleTourDateFromVacationList(empVacationList,possibleTourDate,tourCluster);
				calendar.setTime(possibleTourDate);
			}

			//calendar.setTime(possibleTourDate);
			//int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			//possibleTourDate = returnNextWorkingDate(calendar, dayOfWeek);
			possibleTourDate = returnNextDate(possibleTourDate);
		}catch(Exception e){
			logger.info("Error on vacation list getting"+ e.getMessage());
		}
		logger.info("Possible Tour Date method is returned with a DATE.");
		return possibleTourDate;
	}

	/**
	 * This method returns the day of the tour date
	 * @param tourStartDate
	 * @return
	 */
	private Date returnNextDate(Date tourStartDate){
		LocalDate localDate = tourStartDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		//localDate = localDate.minusDays(1);
		localDate = checkWeekendMethod(localDate);
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * This method make sure that the given tour date is not on weekend 
	 * @param localDate
	 * @return
	 */
	private LocalDate checkWeekendMethod(LocalDate localDate) {
		DayOfWeek dayOfWeek = localDate.getDayOfWeek();
		int day = dayOfWeek.getValue();
		if (day == 6) {
			localDate = localDate.plusDays(2);
		} else if (day == 7) {
			localDate = localDate.plusDays(1);
		}
		return localDate;
	}

	/**
	 * This method used to check whether the next possible tour date is lies in vacation days
	 */
	private Date getPossibleTourDateFromVacationList(List<Vacation> empVacationList, Date trDate, TourCluster tourCluster) {
		logger.info("Possible Tour Date from vacation list method is entered ");
		try {
			for (Vacation v : empVacationList) {
				if (CommonConstants.FALSE.equals(tourCluster.getConsiderAbsence())) {
					if (v.getVacationType().equals("0")) {
						trDate = getPossibleDates(v.getStartDate(), v.getEndDate(), trDate);
					}
				} else {
					trDate = getPossibleDates(v.getStartDate(), v.getEndDate(), trDate);
				}
			}
		} catch (Exception e) {
			logger.info("Error when collecting dates" + e.getMessage());
		}
		logger.info("Possible Tour Date from vacation list method is returned with a Date ");
		return trDate;
	}

	/**
	 * This method returns best possible date after consideringvacations
	 */
	private Date getPossibleDates(Date startDate, Date endDate, Date trDate) {
		logger.info("Possible Tour Date after vacation list method is entered ");
		Calendar c = Calendar.getInstance();
		try {
			if(endDate != null && trDate.compareTo(startDate) >= 0 && trDate.compareTo(endDate) <= 0){
				c.setTime(endDate);
				c.add(Calendar.DATE, 1);
			}else if (trDate.equals(startDate)) {
				c.setTime(startDate);
				c.add(Calendar.DATE, 1);
			}else{
				c.setTime(trDate);
			}
		} catch (Exception e) {
			logger.info("Error when retrieving dates" + e.getMessage());
		}
		logger.info("Possible Tour Date after vacation list method is entered with date ");
		return returnNextWorkingDate(c, c.get(Calendar.DAY_OF_WEEK));
	}

	private Date returnFirstDateBasedOnInput(TourCluster tourCluster){
		LocalDate firstDate = LocalDate.now();
		switch (tourCluster.getPlanningHorizon()) {
		case "1":

			LocalDate firstDateInMonth = LocalDate.of(Integer.parseInt(tourCluster.getYearOfMaintenance()), Integer.parseInt(tourCluster.getMonthOfMaintenance()), 1); 

			//if(firstDateInMonth.isAfter(firstDate)){
			firstDate = firstDateInMonth;
			//}
			break;

		case "2":
			int returnedQuarter = Integer.parseInt(tourCluster.getQuarterlyCycle());
			int maxMonth = returnedQuarter * 3;
			int minMonth = maxMonth - 2;

			for (int month = minMonth; month <= maxMonth; month++) {
				LocalDate firstDateInQuarter = LocalDate.of(Integer.parseInt(tourCluster.getYearOfMaintenance()), month, 1); 
				//if(firstDateInQuarter.isAfter(firstDate)){
				firstDate = firstDateInQuarter;
				//}
				break;
			}
			break;

		case "3":
			LocalDate firstDateInYear = LocalDate.of(Integer.parseInt(tourCluster.getYearOfMaintenance()), 1, 1); 
			//if(firstDateInYear.isAfter(firstDate)){
			firstDate = firstDateInYear;
			//}
			break;
		}	
		return Date.from(firstDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * This method helps to return te next woring day if the possible tour date is lies in weekend
	 */
	public Date returnNextWorkingDate(Calendar calendar, int dayOfWeek) {
		logger.info("The next working day returning method is entered");
		if (dayOfWeek == Calendar.SATURDAY) {
			calendar.add(Calendar.DATE, 2);
		} else if (dayOfWeek == Calendar.SUNDAY) {
			calendar.add(Calendar.DATE, 1);
		}
		logger.info("The next working day returning method is returned with date");
		return calendar.getTime();
	}

	/**
	 * This method helps to return te next woring day if the possible tour date is lies in weekend
	 */
	public int returnNextWorkingDay(Calendar calendar, int dayOfWeek) {
		logger.info("The next working day returning method is entered");
		if (dayOfWeek == Calendar.SATURDAY) {
			calendar.add(Calendar.DATE, 2);
		} else if (dayOfWeek == Calendar.SUNDAY) {
			calendar.add(Calendar.DATE, 1);
		}
		logger.info("The next working day returning method is returned with date");
		return calendar.get(Calendar.DAY_OF_WEEK);
	}



}
