package com.orb.tour.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Future;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.facilities.bean.Facilities;
import com.orb.taskplanner.bean.Task;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.bean.TourCluster;

public interface TourService {
	
	//Tour
		public List<Tour> getInactiveTourList(CommonError commonError);
		public List<Tour> getActiveTourList(CommonError commonError);
		public Tour getTour(CommonError commonError, String tourId);
		public void lockTour(CommonError commonError, User loginUser, List<Tour> selectedTourList);
		public void unlockTour(CommonError commonError, User loginUser, List<Tour> selectedTourList);
		public void deleteTour(CommonError commonError, User loginUser, List<Tour> selectedTourList);
		public void deleteViewedTour(CommonError commonError, User loginUser, Tour tour);
		public void lockViewedTour(CommonError commonError, User loginUser, Tour tour);
		public void unlockViewedTour(CommonError commonError, User loginUser, Tour tour);
		public void add(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds); 
		public void update(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds);
		
		//Tour Facilities
		public List<String> getTourFacilitiesList(CommonError commonError, String tourId);
		
		//KorfAddress
		public List<KorfAddress> getInactiveKorfAddressList(CommonError commonError);
		public List<KorfAddress> getActiveKorfAddressList(CommonError commonError);
		public KorfAddress getKorfAddress(CommonError commonError, String korfAddressId);
		public void lockKorfAddress(CommonError commonError, User loginUser, List<KorfAddress> selectedKorfAddressList);
		public void unlockKorfAddress(CommonError commonError, User loginUser,
				List<KorfAddress> selectedKorfAddressList);
		public void deleteKorfAddress(CommonError commonError, User loginUser,
				List<KorfAddress> selectedKorfAddressList);
		public void deleteViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress);
		public void lockViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress);
		public void unlockViewedKorfAddress(CommonError commonError, User loginUser, KorfAddress korfAddress);
		public void addAddress(CommonError commonError, User loginUser, KorfAddress korfAddress);
		public void updateAddress(CommonError commonError, User loginUser, KorfAddress korfAddress);
		public void createPDF(CommonError commonError, String tourId);
		
				
		//Task List
		public Future<List<Task>> getActiveTaskList(CommonError commonError);
		
		//Facilities List
		public Future<List<Facilities>> getActiveFacilitiesList(CommonError commonError);
		
		//Employee List
		public Future<List<Employee>> getActiveEmployeeList(CommonError commonError);
		
		//YearList
		public Future<List<String>> getMaintenanceYearList();
		
		//Get Last Year Date
		//public Date getLastTourDate(CommonError commonError, String empId, int month, int year);
		
		//Get Vacation List
		public Future<List<Vacation>> getActiveVacationList(CommonError commonError);
		
		//Get Next possible tour date
		public Date getNextPossibleTourDate(CommonError commonError, String empId, TourCluster tourClusetr);
		
		//Get Possible Tour
		public Date getPossibleTourDate(CommonError commonError, Date trDate, String empId, TourCluster tourCluster);
		public int returnNextWorkingDay(Calendar localCalendar, int dayOfWeek);
}		


