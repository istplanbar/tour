package com.orb.tour.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.tour.bean.Tour;
import com.orb.tour.constants.TourDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class TourDAO extends AbstractDAO{
private static Log logger = LogFactory.getLog(TourDAO.class);
	
	
	/**
     * This method will return list of tour from DB
     * @param commonError
     * @param activeFlag
     * @return
     */	
	public List<Tour> getTourList(CommonError commonError, String s) {
		logger.info(" ==> Get Tour List START <== ");
		List<Tour> tourList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : TourDBConstants.TOUR.keySet() ){
				   qry.append( TourDBConstants.TOUR.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(TourDBConstants.TBL_TOUR);qry.append(" WHERE ");
			qry.append(TourDBConstants.TOUR.get("ACTIVE"));qry.append("=? ");
			tourList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(Tour.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to  get tour list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==>  Get Tour List END <== ");
		return tourList;
	}

	/**
	 * This method used to get a particular tour from DB
	 * @param tourId
	 * @param commonError
	 * @return tour
	 */
	public Tour getTour(CommonError commonError, String tourId) {
		logger.info(" ==> Get a Tour START <== ");
		Tour tour = null;
		Tour lockedHist = null;
		Tour unlockedHist = null; 
		try{
			//getLatestTourDate(commonError,"MA00010",5,2018);
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : TourDBConstants.TOUR.keySet() ){
				newQry.append( TourDBConstants.TOUR.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(TourDBConstants.TBL_TOUR);newQry.append(" WHERE ");
			newQry.append( TourDBConstants.TOUR.get("IDENTIFIER") );newQry.append( "=? ");
			tour = (Tour)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{tourId}, new BeanPropertyRowMapper(Tour.class) );
			
		lockedHist = getUserAction(commonError, tour.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			tour.setLockedBy( lockedHist.getUpdateBy());
			tour.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, tour.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			tour.setUnlockedBy( unlockedHist.getUpdateBy());
			tour.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch tour. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Tour END <== ");
		return tour;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */	
	private Tour getUserAction(CommonError commonError, String id, String action) {
		Tour historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TourDBConstants.TOUR_HISTORY.keySet() ){
			newQryHist.append( TourDBConstants.TOUR_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TourDBConstants.TBL_TOUR_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( TourDBConstants.TOUR.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(TourDBConstants.TOUR_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(TourDBConstants.TOUR_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Tour)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Tour.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}

	/**
	 * This method will activate or deactivate the Tour
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> UpdateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertTourHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertTourHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( TourDBConstants.TBL_TOUR );qry.append(" SET ");
			qry.append( TourDBConstants.TOUR.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( TourDBConstants.TOUR.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a tour.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> UpdateActiveStatus END <== ");				
	}

	/**
     * This method inserts updated Role into Role history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	private void insertTourHistory(CommonError commonError, User loginUser, List<String> idList,
			String action) {
		logger.info(" ==> Insert Tour History START <== ");
		logger.info("Tour Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : TourDBConstants.TOUR.keySet() ){
			newQryHist.append( TourDBConstants.TOUR.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TourDBConstants.TBL_TOUR);newQryHist.append(" WHERE ");newQryHist.append( TourDBConstants.TOUR.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Tour> tourList = (List<Tour>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Tour>(Tour.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TourDBConstants.TBL_TOUR_HISTORY);newQry.append("( ");
			for( String column : TourDBConstants.TOUR_HISTORY.keySet() ){
				if(!column.equals(TourDBConstants.TOUR_HISTORY.get("ID"))){
				newQry.append( TourDBConstants.TOUR_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, tourList.get(i).getIdentifier());ps.setString(2, tourList.get(i).getName());
					ps.setObject(3, tourList.get(i).getTourDate());
					ps.setString(4, tourList.get(i).getFacilities());ps.setString(5, tourList.get(i).getTasks());
					ps.setString(6, loginUser.getUserId());
					ps.setTimestamp(7, date);ps.setString(8, loginUser.getUserId());
					ps.setTimestamp(9, date);ps.setString(10,action);
					ps.setString(11, tourList.get(i).getKorfmannOfficeLocation());
					
				}		
				public int getBatchSize() {
					return tourList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when Insert Tour History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Tour History END <== ");				
	}

	/**
	 * This method delete the tour into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteTour(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Tour START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertTourHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TourDBConstants.TBL_TOUR);newQry.append(" WHERE ");
			newQry.append( TourDBConstants.TOUR.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch tour list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Tour END <== ");		
	}

	
	/**
	 * This method inserts new tour into DB
	 * @param commonError
	 * @param loginUser
	 * @param tour
	 */
	public void addTour(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds) {
		logger.info(" ==> Insert Tour START <== ");
		try{
			String nextRowId =  getNextRowId( TourDBConstants.TBL_TOUR, TourDBConstants.tourDBConfig.get("rowPrefix") );
			insertRecord(loginUser, tour,TourDBConstants.TBL_TOUR, TourDBConstants.tourDBConfig.get("rowPrefix")); 
			addTourFacility(commonError, nextRowId, facilityIds);
		}catch( Exception e ){
			logger.info("Error when Insert Tour.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Tour END <== "); 		
	}

	/**
     * This method updates existing tour in DB
     * @param commonError
     * @param loginUser
     * @param tour
	 * @return 
     */
	public List<String> getTourFacilitiesList(CommonError commonError, String tourId) {
		logger.info(" ==> Getting Tour  Facilities START <== ");
		List<String> facilityList = new ArrayList<>();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append( TourDBConstants.TOUR.get("FACILITIES") );
			qry.append(" FROM ");qry.append(TourDBConstants.TBL_TOUR_FACILITIES);qry.append(" WHERE ");
			qry.append( TourDBConstants.TOUR.get("IDENTIFIER") );qry.append( " = ? ");
			facilityList = (List<String>) getJdbcTemplate().queryForList( qry.toString(),new String[]{tourId},String.class );
			facilityList = facilityList.stream().map(String::trim).collect(Collectors.toList());
		}catch( Exception e ){ 
			logger.info("Error Getting Tour  Facilities.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Getting Tour  Facilities <== ");	
		return facilityList;
	}
	
	
	/**
     * This method updates existing tour in DB
     * @param commonError
     * @param loginUser
     * @param tour
     */
	public void updateTour(CommonError commonError, User loginUser, Tour tour, List<String> facilityIds) {
		logger.info(" ==> Update Tour START <== ");
		try{
			updateRecordById(loginUser, tour,TourDBConstants.TBL_TOUR);
			deleteTourFacilities(commonError, tour.getIdentifier());
			addTourFacility(commonError, tour.getIdentifier(), facilityIds);
		}catch( Exception e ){ 
			logger.info("Error when Update Tour.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Tour END <== ");		
	}

	/**
	 * This method delets the preconfigure facilities and updates new facilities.
	 * @param commonError
	 * @param identifier
	 */
	private void deleteTourFacilities(CommonError commonError, String identifier) {
		logger.info(" ==> delete Tour Facilities START <== ");
		try{
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TourDBConstants.TBL_TOUR_FACILITIES);newQry.append(" WHERE ");
			newQry.append( TourDBConstants.TOUR.get("IDENTIFIER") );newQry.append( " =? " );
			getJdbcTemplate().update(newQry.toString(),new String[]{identifier});
		}catch( Exception e ){ 
				logger.info("Error delete Tour Facilities  " + e.getMessage());
				commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==>  delete Tour Facilities   END <== ");	
	}

	/**
	 * Adding all the facility details to this particular Tour
	 * @param commonError
	 * @param tourId
	 * @param facilityIds
	 */
	public void addTourFacility(CommonError commonError, String tourId, List<String> facilityIds) {
		logger.info(" ==> adding Tour Facilities START <== ");
		try{
			StringBuffer newQry = new StringBuffer();
			newQry.append("INSERT INTO ");newQry.append( TourDBConstants.TBL_TOUR_FACILITIES);newQry.append("( ");
			newQry.append( TourDBConstants.TOUR.get("IDENTIFIER"));newQry.append( "," );
			newQry.append( TourDBConstants.TOUR.get("FACILITIES"));
			newQry.append(") VALUES(?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, tourId);ps.setString(2, facilityIds.get(i).trim());
				}		
				public int getBatchSize() {
					return facilityIds.size();
				}
				
			});
		}catch( Exception e ){ 
			logger.info("Error adding Tour Facilities  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==>  adding Tour Facilities  END <== ");		
		
	}
	
	/**
	 * This methods retrieves the latest date
	 */
	public Date getLatestTourDate(CommonError commonError, String empId, int maintenanceMonth, int maintenanceYear) {
		logger.info(" ==> Get latest tour date for START <== ");
		Date tourDate = null;
		try{
			Object[] argObj = new Object[]{};
			int i =1;
			StringBuffer qry = new StringBuffer("SELECT MAX(");
			qry.append(TourDBConstants.TOUR.get("TOUR_DATE"));
			qry.append(") FROM ");qry.append(TourDBConstants.TBL_TOUR);qry.append(" WHERE ");
			qry.append("YEAR(");qry.append(TourDBConstants.TOUR.get("TOUR_DATE"));qry.append( ") = ? ");
			
			boolean emptyEmpId = !CommonUtils.getValidString(empId).isEmpty();
			
			if(emptyEmpId){
				i++;
				qry.append(" AND ");
				qry.append( TourDBConstants.TOUR.get("EMPLOYEE"));qry.append( " = ? ");
			}
			if(maintenanceMonth != 0){
				i++;
				qry.append(" AND ");
				qry.append("MONTH(");qry.append(TourDBConstants.TOUR.get("TOUR_DATE"));qry.append( ") = ? ");
			}
			qry.append(" AND ACTIVE = 'Y'");
			if(emptyEmpId){
				qry.append(" GROUP BY ");qry.append(TourDBConstants.TOUR.get("EMPLOYEE"));
			}
			argObj = new Object[i];
			for(int j = 0; j< i; j++){
				argObj[j] = new Object();
			}
			argObj[0] = maintenanceYear;
			if(emptyEmpId && maintenanceMonth == 0){
				argObj[1] = empId;
			}else if(!emptyEmpId && maintenanceMonth != 0){
				argObj[1] = maintenanceMonth;
			}else if(emptyEmpId && maintenanceMonth != 0){
				argObj[1] = empId;
				argObj[2] = maintenanceMonth;
			}
			
			logger.info(qry.toString());
			tourDate = getJdbcTemplate().queryForObject( qry.toString(), argObj, java.sql.Date.class);
			logger.info(tourDate);
		}catch( Exception e ){ 
		logger.info("Error on getting latest Tour date  " + e.getMessage());
			//commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get latest tour date for END <== ");
		return tourDate;
	}

}
