package com.orb.tour.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.orb.admin.client.service.impl.AbstractAdminService;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.dao.EmployeeDAO;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.facilities.dao.FacilitiesDAO;
import com.orb.facilities.service.FacilitiesService;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.dao.TaskDAO;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;

@Repository
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TourPDFDAO extends AbstractAdminService {

	private static Log logger = LogFactory.getLog(TourPDFDAO.class);

	@Autowired
	TourDAO tourDAO;

	@Autowired
	KorfmannLocationDAO korfmannDAO;

	@Autowired
	EmployeeDAO employeeDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	FacilitiesDAO facilitiesDAO;

	@Autowired
	FacilitiesService faciltiesService;
	
	@Autowired
	TaskDAO taskDAO;

	public void createPDF(CommonError commonError, String tourId) {

		List<KorfAddress> korfmannLocationList = korfmannDAO.getKorfAddressList(commonError, CommonConstants.Y);
		List<Employee> employeeList = employeeDAO.getEmployeeList(commonError, CommonConstants.Y);
		List<Facilities> facilitiesList = faciltiesService.getActiveFacilitiesList(commonError);
		List<Task> tasksList = taskDAO.getTaskList(commonError, CommonConstants.Y, null);
		Tour tour = tourDAO.getTour(commonError, tourId);

		List<String> selectedFacilitiesIds = tourDAO.getTourFacilitiesList(commonError, tourId);

		List<Facilities> selectedFacilitysList = new ArrayList<>();
		if (!selectedFacilitiesIds.isEmpty()) {
			selectedFacilitysList = facilitiesList.stream()
					.filter(fa -> selectedFacilitiesIds.contains(fa.getIdentifier())).collect(Collectors.toList());
		}
		for (Facilities f : selectedFacilitysList) {
			List<String> filteredMaintenanceIds = new ArrayList<>();
			filteredMaintenanceIds = tasksList.stream()
					.filter(t -> t.getFacilities() != null && t.getFacilities().equals(f.getId()))
					.map(t -> t.getIdentifier() + returnWorkHour(t.getEstimatedTime())).collect((Collectors.toList()));
			f.setMaintenanceActId(String.join(", ", filteredMaintenanceIds));

		}

		try {
			String locationName = null;
			if (!korfmannLocationList.isEmpty()) {
				locationName = korfmannLocationList.stream()
						.filter(i -> i.getIdentifier().equals(tour.getKorfmannOfficeLocation())).findFirst().get()
						.getName();
			}

			String empName = null;
			if (!employeeList.isEmpty()) {
				empName = employeeList.stream().filter(i -> i.getIdentifier().equals(tour.getEmployee())).findFirst()
						.get().getFirstName();
			}
			User user = null;
			user = userDAO.getLoginUser(empName);

			Rectangle pagesize = new Rectangle(850f, 720f);
          
			//Rectangle pagesize = new Rectangle(950f, 780f);
			
			String filePath = null;
			String fileName = tourId + "_tour" + ".pdf";
			filePath = System.getProperty("upload.dir") + "tourPDF/" + fileName;
			Document document = new Document(new Rectangle(pagesize), -60, -60, 40, 40);
			PdfWriter.getInstance(document, new FileOutputStream(filePath));

			StringBuffer headerString = new StringBuffer(tour.getName());
			
			if(tour.getTourDate() != null){
				Date tourDate = tour.getTourDate();
	
				SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
				String dayOfTheWeek = simpleDateformat.format(tourDate);
				simpleDateformat = new SimpleDateFormat("dd.MM.yyyy");
				String formattedDate = simpleDateformat.format(tourDate);
				headerString.append(" (" + getJSFMessage("lbl_"+dayOfTheWeek) + ", " + formattedDate + ")") ;
			}
			
			HeaderFooter header = new HeaderFooter(new Phrase(headerString.toString()), false);
			header.setPageNumber(1);
			header.setAlignment(Element.ALIGN_CENTER);
			header.setBorder(Rectangle.NO_BORDER);
			document.setHeader(header);

			HeaderFooter footer = new HeaderFooter(new Phrase(tour.getName()), false);
			footer.setPageNumber(document.getPageNumber());
			footer.setAlignment(Element.ALIGN_CENTER);
			footer.setBorder(Rectangle.NO_BORDER);
			document.setFooter(footer);
			document.open();
			document.addTitle("Tour");
			Font mySizeSpecification = new Font();

			// Anand changed font size from 15 to 11 28/05/2018
			mySizeSpecification.setSize(10);

			Font boldFont = new Font();
			boldFont.setStyle(Font.BOLD);
			// Anand added font size for boldfont 28/05/2018
			boldFont.setSize(10);
			PdfPTable table = new PdfPTable(11);

			Chunk label = new Chunk(getJSFMessage("lbl_tourId") + " ");
			Chunk value = new Chunk(tour.getIdentifier(), boldFont);
			Paragraph p = new Paragraph();
			p.add(label);
			p.add(value);
			PdfPCell tableCell = new PdfPCell(new Phrase(p));
			tableCell.setBorder(Rectangle.NO_BORDER);

			tableCell.setColspan(7);

			table.addCell(tableCell);
			label = new Chunk(getJSFMessage("lbl_tourEmployee") + " ");
			value = new Chunk(user.getFirstName() + " " + user.getLastName(), boldFont);
			p = new Paragraph();
			p.add(label);
			p.add(value);
			PdfPCell tableCell2 = new PdfPCell(new Phrase(p));
			tableCell2.setBorder(Rectangle.NO_BORDER);
			// tableCell2.setHorizontalAlignment(Element.ALIGN_RIGHT);

			tableCell2.setColspan(4);
			table.addCell(tableCell2);
			label = new Chunk(getJSFMessage("lbl_tourName") + ": ");


			value = new Chunk(tour.getName(), boldFont);
			p = new Paragraph();
			p.add(label);
			p.add(value);
			PdfPCell tableCell1 = new PdfPCell(new Phrase(p));
			tableCell1.setBorder(Rectangle.NO_BORDER);
			tableCell1.setColspan(7);
			table.addCell(tableCell1);
			label = new Chunk(getJSFMessage("lbl_tourStart") + " ");
			value = new Chunk(locationName, boldFont);
			p = new Paragraph();
			p.add(label);
			p.add(value);
			PdfPCell tableCell3 = new PdfPCell(new Phrase(p));
			tableCell3.setBorder(Rectangle.NO_BORDER);
			// tableCell3.setHorizontalAlignment(Element.ALIGN_RIGHT);

			tableCell3.setColspan(4);
			table.addCell(tableCell3);
			document.add(table);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("\n"));
			PdfPTable facilitiesTable = new PdfPTable(12);

			facilitiesTable.setWidths(new float[] { 0.8f, 1.3f, 1.3f, 0.9f, 1.7f, 1.4f, 1.0f, 1.1f, 1.1f, 1.0f, 1.2f,1.1f });

			PdfPCell facilitiesCell = new PdfPCell();
			facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (selectedFacilitysList != null) {
				logger.info("Facilities PDF Data START");
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_station"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_facilitiesId"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_name"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_kdnr"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_street"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_houseNumber"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_pin"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_place"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_map"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_billing"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_withoutOffer"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_reminder"), boldFont));
				facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				facilitiesTable.addCell(facilitiesCell);
				
				document.add(facilitiesTable);
				int i = 1;
				for (Facilities facilities : selectedFacilitysList) {
					List<String> facilitiesMaintenaceActIds = new ArrayList<>();
					facilitiesTable = new PdfPTable(12);
					facilitiesTable.setWidths(
							new float[] { 0.8f, 1.3f, 1.3f, 0.9f, 1.7f, 1.4f, 1.0f, 1.1f, 1.1f, 1.0f, 1.2f, 1.1f });
					facilitiesCell = new PdfPCell(new Phrase(String.valueOf(i), boldFont));
					facilitiesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					facilitiesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					if (!CommonUtils.getValidString(facilities.getMaintenanceActId()).isEmpty()) {
						facilitiesMaintenaceActIds = Arrays.asList(facilities.getMaintenanceActId().split("\\s*,\\s*"));
						facilitiesCell.setRowspan(facilitiesMaintenaceActIds.size() + 3);
					}
					facilitiesTable.addCell(facilitiesCell);
					facilitiesTable.addCell(facilities.getIdentifier());
					facilitiesTable.addCell(facilities.getName());
					facilitiesTable.addCell(facilities.getCustomerNumber());
					facilitiesTable.addCell(facilities.getLocationStreet());
					facilitiesTable.addCell(facilities.getLocationHouseNumber());
					facilitiesTable.addCell(facilities.getLocationPostCode());
					facilitiesTable.addCell(facilities.getLocationPlace());
					facilitiesTable.addCell(facilities.getBuildingPlan());
					facilitiesTable.addCell(facilities.getBillingMode());
					facilitiesTable.addCell(facilities.getCostWithoutOffer());
					if(!CommonUtils.getValidString(facilities.getReminder()).isEmpty())
						facilitiesTable.addCell(getJSFMessage("reminder"+facilities.getReminder()));
					else
						facilitiesTable.addCell("");
   
					
					if (!CommonUtils.getValidString(facilities.getMaintenanceActId()).isEmpty()) {
						
					    facilitiesCell = new PdfPCell(new Phrase(getJSFMessage("lbl_tasks"),boldFont));
					    facilitiesCell.setBorder(Rectangle.RIGHT);
					    facilitiesCell.setColspan(11);
					    
					    facilitiesTable.addCell(facilitiesCell);
						int count = facilitiesMaintenaceActIds.size();
						for (String s : facilitiesMaintenaceActIds) {
							
							facilitiesCell = new PdfPCell(new Phrase(s));
							if (count == 1 && i == selectedFacilitysList.size()) {
								
								facilitiesCell.setBorder( Rectangle.BOTTOM | Rectangle.RIGHT);
							} else {
								
								facilitiesCell.setBorder(Rectangle.RIGHT);
							}
							facilitiesCell.setColspan(11);
							facilitiesTable.addCell(facilitiesCell);
							count--;
						}
					}
					document.add(facilitiesTable);
					i++;
				}
			}
			logger.info("Facilities PDF Data END");

			document.newPage();

			document.close();
			downloadFile(commonError, fileName, "Tour");

		} catch (Exception e) {
			logger.info(e.getMessage());
		}

	}

	public void downloadFile(CommonError commonError, String pdfFile, String name) throws IOException {

		String path = null;
		path = System.getProperty("upload.dir") + "tourPDF/" + pdfFile;

		File downFile = new File(path);
		Path filePath = Paths.get(path);
		if (Files.exists(filePath) && !"".equals(pdfFile)) {
			String fileName = downFile.getName();
			InputStream fis = new FileInputStream(downFile);
			byte[] buf = new byte[fis.available()];
			int offset = 0;
			int numRead = 0;
			while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length - offset)) >= 0))
				offset += numRead;
			fis.close();
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			response.setContentType(contentType);
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			response.getOutputStream().write(buf);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			FacesContext.getCurrentInstance().responseComplete();
			commonError.clearErrors();
		} else {
			commonError.addError("err_docNotFound");
		}

	}

	private String returnWorkHour(String value) {
		Map<String, String> estimatedWorkHoursList = (Map<String, String>) FacilitiesConstants.ESTIMATED_WORK_HOURS;
		String workHour = "";
		for (String key : estimatedWorkHoursList.keySet()) {
			if (estimatedWorkHoursList.get(key).equals(value)) {
				workHour = " (" + key + ")";
				break;
			}
		}
		return workHour;
	}

}
