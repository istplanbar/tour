package com.orb.tour.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.constants.TourDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class KorfmannLocationDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(KorfmannLocationDAO.class);
	
	/**
     * This method will return list of KorfAddress from DB
     * @param commonError
     * @param activeFlag
     * @return
     */	
	public List<KorfAddress> getKorfAddressList(CommonError commonError, String s) {
		logger.info(" ==> Get KorfAddress List START <== ");
		List<KorfAddress> korfAddressList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : TourDBConstants.KORFMANN_LOCATION.keySet() ){
				   qry.append( TourDBConstants.KORFMANN_LOCATION.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(TourDBConstants.TBL_KORFMANN_LOCATION);qry.append(" WHERE ");
			qry.append(TourDBConstants.KORFMANN_LOCATION.get("ACTIVE"));qry.append("=? ");
			korfAddressList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(KorfAddress.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to  get KorfAddress list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==>  Get KorfAddress List END <== ");
		return korfAddressList;
	}
	
	
	/**
	 * This method used to get a particular korfmann address from DB
	 * @param korfLocationID
	 * @param commonError
	 * @return korfAddress
	 */
	public KorfAddress getKorfAddress(CommonError commonError, String korfLocationID) {
		logger.info(" ==> Get a KorfAddress START <== ");
		KorfAddress korfAddress = new KorfAddress();
		KorfAddress lockedHist = null;
		KorfAddress unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : TourDBConstants.KORFMANN_LOCATION.keySet() ){
				newQry.append( TourDBConstants.KORFMANN_LOCATION.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(TourDBConstants.TBL_KORFMANN_LOCATION);newQry.append(" WHERE ");
			newQry.append( TourDBConstants.KORFMANN_LOCATION.get("IDENTIFIER") );newQry.append( "=? ");
			korfAddress = (KorfAddress)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{korfLocationID}, new BeanPropertyRowMapper(KorfAddress.class) );
			
		lockedHist = getUserAction(commonError, korfAddress.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			korfAddress.setLockedBy( lockedHist.getUpdateBy());
			korfAddress.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, korfAddress.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			korfAddress.setUnlockedBy( unlockedHist.getUpdateBy());
			korfAddress.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to KorfAddress . Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a KorfAddress END <== ");
		return korfAddress;
	}

	
	/**
	 * This method inserts new Address into DB
	 * @param commonError
	 * @param loginUser
	 * @param korfAddress
	 */
	public void addAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		logger.info(" ==> Insert korfAddress START <== ");
		try{
			String nextRowId =  getNextRowId( TourDBConstants.TBL_KORFMANN_LOCATION, TourDBConstants.korfDBConfig.get("rowPrefix") );
			insertRecord(loginUser, korfAddress,TourDBConstants.TBL_KORFMANN_LOCATION, TourDBConstants.korfDBConfig.get("rowPrefix")); 
		}catch( Exception e ){
			logger.info("Error when Insert korfAddress.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert korfAddress END <== "); 		
	}

	/**
     * This method updates existing Address in DB
     * @param commonError
     * @param loginUser
     * @param korfAddress
     */
	public void updateAddress(CommonError commonError, User loginUser, KorfAddress korfAddress) {
		logger.info(" ==> Update korfAddress START <== ");
		try{
			updateRecordById(loginUser, korfAddress,TourDBConstants.TBL_KORFMANN_LOCATION);
		}catch( Exception e ){ 
			logger.info("Error when Update korfAddress.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update korfAddress END <== ");		
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */	
	private KorfAddress getUserAction(CommonError commonError, String id, String action) {
		KorfAddress historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TourDBConstants.KORMANN_LOCATION_HISTORY.keySet() ){
			newQryHist.append( TourDBConstants.KORMANN_LOCATION_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TourDBConstants.TBL_KORFMANN_LOCATION_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( TourDBConstants.KORFMANN_LOCATION.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(TourDBConstants.KORMANN_LOCATION_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(TourDBConstants.KORMANN_LOCATION_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (KorfAddress)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(KorfAddress.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}
	
	/**
	 * This method will activate or deactivate the Korfmann
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> UpdateActiveStatus Korfmann START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertAddressHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertAddressHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( TourDBConstants.TBL_KORFMANN_LOCATION );qry.append(" SET ");
			qry.append( TourDBConstants.KORFMANN_LOCATION.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( TourDBConstants.KORFMANN_LOCATION.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				public int getBatchSize() {
					return idList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus Korfmann .  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> UpdateActiveStatus Korfmann END <== ");				
	}

	/**
     * This method inserts history of action made on korfmann address
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	private void insertAddressHistory(CommonError commonError, User loginUser, List<String> idList, String action) {
		logger.info(" ==> Insert KorfAddress History START <== ");
		logger.info("KorfAddress Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : TourDBConstants.KORFMANN_LOCATION.keySet() ){
			newQryHist.append( TourDBConstants.KORFMANN_LOCATION.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TourDBConstants.TBL_KORFMANN_LOCATION);newQryHist.append(" WHERE ");newQryHist.append( TourDBConstants.KORFMANN_LOCATION.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<KorfAddress> korfAddressList = (List<KorfAddress>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<KorfAddress>(KorfAddress.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TourDBConstants.TBL_KORFMANN_LOCATION_HISTORY);newQry.append("( ");
			for( String column : TourDBConstants.KORMANN_LOCATION_HISTORY.keySet() ){
				if(!column.equals(TourDBConstants.KORMANN_LOCATION_HISTORY.get("ID"))){
				newQry.append( TourDBConstants.KORMANN_LOCATION_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, korfAddressList.get(i).getIdentifier());ps.setString(2, korfAddressList.get(i).getName());
					ps.setString(3, korfAddressList.get(i).getStreet());ps.setString(4, korfAddressList.get(i).getHouseNumber());
					ps.setString(5, korfAddressList.get(i).getAddress());ps.setString(6, korfAddressList.get(i).getState());
					ps.setString(7, korfAddressList.get(i).getZipCode());ps.setString(8, korfAddressList.get(i).getPlace());
					ps.setString(9, korfAddressList.get(i).getCountry());ps.setDouble(10, korfAddressList.get(i).getFacilityLatitude());
					ps.setDouble(11, korfAddressList.get(i).getFacilityLongitude());ps.setString(12, loginUser.getUserId());
					ps.setTimestamp(13, date);ps.setString(14, loginUser.getUserId());
					ps.setTimestamp(15, date);ps.setString(16,action);
					
				}		
				public int getBatchSize() {
					return korfAddressList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when Insert korfAddress History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert korfAddress History END <== ");				
	}
	
	/**
	 * This method delete the korfmann  into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteKorfAddress(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete korfmann START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertAddressHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TourDBConstants.TBL_KORFMANN_LOCATION);newQry.append(" WHERE ");
			newQry.append( TourDBConstants.KORFMANN_LOCATION.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch korfmann list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete korfmann END <== ");		
	}
	
}
