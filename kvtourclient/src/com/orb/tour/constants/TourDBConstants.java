package com.orb.tour.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class TourDBConstants {
	

		//Tour
	
		public static final String TBL_TOUR,TBL_TOUR_HISTORY, TBL_TOUR_FACILITIES, 
					TBL_KORFMANN_LOCATION, TBL_KORFMANN_LOCATION_HISTORY;
		public static final Map<String,String> TOUR,TOUR_HISTORY, KORFMANN_LOCATION, KORMANN_LOCATION_HISTORY;
		public static final Map<String,String> tourDBConfig, korfDBConfig;
	
static{
		
		//Tour
		TBL_TOUR=CommonUtils.getPropertyValue("TOUR_TABLE_NAMES", "MOD_TOUR");
		TBL_TOUR_HISTORY=CommonUtils.getPropertyValue("TOUR_TABLE_NAMES", "MOD_TOUR_HISTORY");
		TBL_KORFMANN_LOCATION = CommonUtils.getPropertyValue("KORF_TABLE_NAMES", "MOD_KORFMANN_ADDRESS");
		TBL_KORFMANN_LOCATION_HISTORY = CommonUtils.getPropertyValue("KORF_TABLE_NAMES", "MOD_KORFMANN_ADDRESS_HISTORY");
		TBL_TOUR_FACILITIES = CommonUtils.getPropertyValue("TOUR_TABLE_NAMES", "MOD_TOUR_FACILITIES");
			
		TOUR=(Map<String,String>)CommonUtils.getBean("MOD_TOUR");
		TOUR_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_TOUR_HISTORY");
		KORFMANN_LOCATION =(Map<String,String>)CommonUtils.getBean("MOD_KORFMANN_ADDRESS");
		KORMANN_LOCATION_HISTORY =(Map<String,String>)CommonUtils.getBean("MOD_KORFMANN_ADDRESS_HISTORY");
			
		tourDBConfig=(Map<String,String>)CommonUtils.getBean("tourDBConfig");
		korfDBConfig=(Map<String,String>)CommonUtils.getBean("korfDBConfig");
       }
}
