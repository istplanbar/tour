package com.orb.tour.constants;

import com.orb.common.client.utils.CommonUtils;

public class TourConstants {


	//Tour
	public final static String SELECTED_TOUR_ID = "SELECTED_TOUR_ID";
	public final static String TOUR_LIST_PAGE = "/common/process/tour/loadTour.html";
	public final static String TOUR_EDIT_PAGE = "/common/process/tour/manipulateTour.html";
	public final static String TOUR_MAP_PAGE = "/common/process/tour/loadGoogleMaps.html";
	public final static String TOUR_CLUSTER_PAGE_URL = "optaplanner/showCluster.html";
	public final static String TOUR_CLUSTERING_URL = "/common/process/tour/loadTourCluster.html";
	public final static String TOUR_MAP_PAGE_URL = "optaplanner/googleMaps.xhtml";
	public final static String TOUR_LIST_HTML = "tour/list.xhtml";

	public final static String TOUR_TOGGLER_LIST = "tour_toggler_list" ;
	public final static String TOUR_DELETE_SUCCESS = "tour_delete_success";
	public final static String TOUR_LOCKED_SUCCESS = "tour_locked_success";
	public final static String TOUR_UNLOCKED_SUCCESS = "tour_unlocked_success";
	public final static String TOUR_VIEW_PAGE = "/common/process/tour/viewTour.html";
	public final static String TOUR_EDIT_HTML = "tour/addUpdate.xhtml";
	public final static String TOUR_VIEW_HTML = "tour/view.xhtml";
	public final static String TOUR_CLUSTER_CREATE_VIEW_HTML = "tour/tourcluster/createCluster.xhtml";
	public final static String TOUR_CLUSTER_CREATE_TASK_HTML = "tour/tourcluster/createTask.xhtml";
	public final static String TOUR_ADDED_SUCCESS = "tour_added_success";
	public final static String TOUR_UPDATED_SUCCESS = "tour_update_success";
	//KorfAddress
	public final static String SELECTED_ADDRESS_ID = "SELECTED_ADDRESS_ID";
	public final static String ADDRESS_LIST_PAGE = "/common/process/tour/loadAddress.html";
	public final static String ADDRESS_EDIT_PAGE = "/common/process/tour/manipulateAddress.html";
	public final static String ADDRESS_LIST_HTML = "tour/address/list.xhtml";
	public final static String ADDRESS_TOGGLER_LIST = "tour_toggler_list" ;
	public final static String ADDRESS_DELETE_SUCCESS = "korfAddress_delete_success";
	public final static String ADDRESS_LOCKED_SUCCESS = "korfAddress_locked_success";
	public final static String ADDRESS_UNLOCKED_SUCCESS = "korfAddress_unlocked_success";
	public final static String ADDRESS_VIEW_PAGE = "/common/process/tour/viewAddress.html";
	public final static String ADDRESS_EDIT_HTML = "tour/address/addUpdate.xhtml";
	public final static String ADDRESS_VIEW_HTML = "tour/address/view.xhtml";
	public final static String ADDRESS_ADDED_SUCCESS = "korfAddress_added_success";
	public final static String ADDRESS_UPDATED_SUCCESS = "korfAddress_update_success";
	public final static String GOOGLE_DISTANCE_MAP_API;
	public final static String OFFICE_HOURS, BREAK_HOURS, ROUTE_OPERATOR, ROUTE_APPEND_OPERATOR, ROUTE_COMMA_OPERATOR, TOUR_FACILITY_BUILT_END_YEAR, SOLVER_CONFIG_PATH;

	static{
		GOOGLE_DISTANCE_MAP_API = CommonUtils.getPropertyValue("GOOGLE_DISTANCE_MAP_API", "API_KEY");
		SOLVER_CONFIG_PATH = System.getProperty("config.dir") + "moduleConfig/optaplanner/vehicleRoutingSolverConfig.xml" ;
		OFFICE_HOURS = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "OFFICE_HOURS");
		BREAK_HOURS = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "BREAK_HOURS");
		ROUTE_OPERATOR = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "ROUTE_OPERATOR");
		ROUTE_APPEND_OPERATOR = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "ROUTE_APPEND_OPERATOR");
		ROUTE_COMMA_OPERATOR = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "ROUTE_COMMA_OPERATOR");
		TOUR_FACILITY_BUILT_END_YEAR = CommonUtils.getPropertyValue("CLUSTERING_DEFAULT_VALUES", "TOUR_FACILITY_BUILT_END_YEAR");
	}
}
