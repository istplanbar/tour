package com.orb.qualifications.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.qualifications.bean.Qualifications;
import com.orb.qualifications.constants.QualificationsDBConstants;
import org.springframework.beans.factory.config.BeanDefinition;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class QualificationsDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(QualificationsDAO.class);
	
	
	/**
     * This method will return list of qualifications from DB
     * @param commonError
     * @param activeFlag
     * @return
     */	
	public List<Qualifications> getQualificationsList(CommonError commonError, String s) {
		logger.info(" ==> Get Qualifications List START <== ");
		List<Qualifications> qualificationsList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : QualificationsDBConstants.QUALIFICATIONS.keySet() ){
				   qry.append( QualificationsDBConstants.QUALIFICATIONS.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(QualificationsDBConstants.TBL_QUALIFICATIONS);qry.append(" WHERE ");
			qry.append(QualificationsDBConstants.QUALIFICATIONS.get("ACTIVE"));qry.append("=? ");
			qualificationsList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(Qualifications.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to  get qualifications list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==>  Get Qualifications List END <== ");
		return qualificationsList;
	}

	/**
	 * This method used to get a particular qualifications from DB
	 * @param qualificationsId
	 * @param commonError
	 * @return qualifications
	 */
	public Qualifications getQualifications(CommonError commonError, String qualificationsId) {
		logger.info(" ==> Get a Qualifications START <== ");
		Qualifications qualifications = null;
		Qualifications lockedHist = null;
		Qualifications unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : QualificationsDBConstants.QUALIFICATIONS.keySet() ){
				newQry.append( QualificationsDBConstants.QUALIFICATIONS.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(QualificationsDBConstants.TBL_QUALIFICATIONS);newQry.append(" WHERE ");
			newQry.append( QualificationsDBConstants.QUALIFICATIONS.get("IDENTIFIER") );newQry.append( "=? ");
			qualifications = (Qualifications)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{qualificationsId}, new BeanPropertyRowMapper(Qualifications.class) );
			
		lockedHist = getUserAction(commonError, qualifications.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			qualifications.setLockedBy( lockedHist.getUpdateBy());
			qualifications.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, qualifications.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			qualifications.setUnlockedBy( unlockedHist.getUpdateBy());
			qualifications.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch qualifications. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Qualifications END <== ");
		return qualifications;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */	
	private Qualifications getUserAction(CommonError commonError, String id, String action) {
		Qualifications historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : QualificationsDBConstants.QUALIFICATIONS_HISTORY.keySet() ){
			newQryHist.append( QualificationsDBConstants.QUALIFICATIONS_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(QualificationsDBConstants.TBL_QUALIFICATIONS_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( QualificationsDBConstants.QUALIFICATIONS.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(QualificationsDBConstants.QUALIFICATIONS_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(QualificationsDBConstants.QUALIFICATIONS_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Qualifications)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Qualifications.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}

	/**
	 * This method will activate or deactivate the Qualifications
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> UpdateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertQualificationsHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertQualificationsHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( QualificationsDBConstants.TBL_QUALIFICATIONS );qry.append(" SET ");
			qry.append( QualificationsDBConstants.QUALIFICATIONS.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( QualificationsDBConstants.QUALIFICATIONS.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a qualifications.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> UpdateActiveStatus END <== ");				
	}

	/**
     * This method inserts updated Role into Role history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	private void insertQualificationsHistory(CommonError commonError, User loginUser, List<String> idList,
			String action) {
		logger.info(" ==> Insert Qualifications History START <== ");
		logger.info("Qualifications Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : QualificationsDBConstants.QUALIFICATIONS.keySet() ){
			newQryHist.append( QualificationsDBConstants.QUALIFICATIONS.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(QualificationsDBConstants.TBL_QUALIFICATIONS);newQryHist.append(" WHERE ");newQryHist.append( QualificationsDBConstants.QUALIFICATIONS.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Qualifications> qualificationsList = (List<Qualifications>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Qualifications>(Qualifications.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( QualificationsDBConstants.TBL_QUALIFICATIONS_HISTORY);newQry.append("( ");
			for( String column : QualificationsDBConstants.QUALIFICATIONS_HISTORY.keySet() ){
				if(!column.equals(QualificationsDBConstants.QUALIFICATIONS_HISTORY.get("ID"))){
				newQry.append( QualificationsDBConstants.QUALIFICATIONS_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, qualificationsList.get(i).getIdentifier());ps.setString(2, qualificationsList.get(i).getQualificationName());
					ps.setString(3, loginUser.getUserId());
					ps.setTimestamp(4, date);ps.setString(5, loginUser.getUserId());
					ps.setTimestamp(6, date);ps.setString(7,action);
					
				}		
				public int getBatchSize() {
					return qualificationsList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when Insert Qualifications History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Qualifications History END <== ");				
	}

	/**
	 * This method delete the qualifications into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteQualifications(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Qualifications START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertQualificationsHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(QualificationsDBConstants.TBL_QUALIFICATIONS);newQry.append(" WHERE ");
			newQry.append( QualificationsDBConstants.QUALIFICATIONS.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch qualifications list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Qualifications END <== ");		
	}

	
	/**
	 * This method inserts new qualifications into DB
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	public void addQualifications(CommonError commonError, User loginUser, Qualifications qualifications) {
		logger.info(" ==> Insert Qualifications START <== ");
		try{
			insertRecord(loginUser, qualifications,QualificationsDBConstants.TBL_QUALIFICATIONS,QualificationsDBConstants.qualificationsDBConfig.get("rowPrefix"));  
		}catch( Exception e ){
			logger.info("Error when Insert Qualifications.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Qualifications END <== "); 		
	}

	
	/**
     * This method updates existing qualifications in DB
     * @param commonError
     * @param loginUser
     * @param qualifications
     */
	public void updateQualifications(CommonError commonError, User loginUser, Qualifications qualifications) {
		logger.info(" ==> Update Qualifications START <== ");
		try{
			updateRecordById(loginUser, qualifications,QualificationsDBConstants.TBL_QUALIFICATIONS); 
		}catch( Exception e ){ 
			logger.info("Error when Update Qualifications.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Qualifications END <== ");		
	}

}
