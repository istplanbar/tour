package com.orb.qualifications.constants;

public class QualificationsConstants {
	
	//Qualifications
	public final static String SELECTED_QUALIFICATION_ID = "SELECTED_QUALIFICATION_ID";
	public final static String QUALIFICATION_LIST_PAGE = "/common/process/qualifications/loadQualifications.html";
	public final static String QUALIFICATION_EDIT_PAGE = "/common/process/qualifications/manipulateQualifications.html";
	public final static String QUALIFICATION_LIST_HTML = "qualifications/list.xhtml";
	public final static String QUALIFICATION_TOGGLER_LIST = "qual_toggler_list" ;
	public final static String QUALIFICATION_DELETE_SUCCESS = "qual_delete_success";
	public final static String QUALIFICATION_LOCKED_SUCCESS = "qual_locked_success";
	public final static String QUALIFICATION_UNLOCKED_SUCCESS = "qual_unlocked_success";
	public final static String QUALIFICATION_VIEW_PAGE = "/common/process/qualifications/viewQualifications.html";
	public final static String QUALIFICATION_EDIT_HTML = "qualifications/addUpdate.xhtml";
	public final static String QUALIFICATION_VIEW_HTML = "qualifications/view.xhtml";
	public final static String QUALIFICATION_ADDED_SUCCESS = "qual_added_success";
	public final static String QUALIFICATION_UPDATED_SUCCESS = "qual_update_success";


	//Employee
	public final static String SELECTED_EMPLOYEE_ID = "SELECTED_EMPLOYEE_ID";
	public final static String EMPLOYEE_LIST_PAGE = "/common/process/qualifications/loadEmployee.html";
	public final static String EMPLOYEE_EDIT_PAGE = "/common/process/qualifications/manipulateEmployee.html";
	public final static String EMPLOYEE_LIST_HTML = "qualifications/employee/list.xhtml";
	public final static String EMPLOYEE_TOGGLER_LIST = "employee_toggler_list" ;
	public final static String EMPLOYEE_DELETE_SUCCESS = "employee_delete_success";
	public final static String EMPLOYEE_LOCKED_SUCCESS = "employee_locked_success";
	public final static String EMPLOYEE_UNLOCKED_SUCCESS = "employee_unlocked_success";
	public final static String EMPLOYEE_VIEW_PAGE = "/common/process/qualifications/viewEmployee.html";
	public final static String EMPLOYEE_EDIT_HTML = "qualifications/employee/addUpdate.xhtml";
	public final static String EMPLOYEE_VIEW_HTML = "qualifications/employee/view.xhtml";
	public final static String EMPLOYEE_ADDED_SUCCESS = "employee_added_success";
	public final static String EMPLOYEE_UPDATED_SUCCESS = "employee_update_success";
	
	
	
}
