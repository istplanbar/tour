package com.orb.qualifications.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class QualificationsDBConstants {

	
	//Qualifications
	
	public static final String TBL_QUALIFICATIONS,TBL_QUALIFICATIONS_HISTORY;
	public static final Map<String,String> QUALIFICATIONS,QUALIFICATIONS_HISTORY;
	public static final Map<String,String> qualificationsDBConfig;
	
	//Employee
	
	public static final String TBL_EMPLOYEE,TBL_EMPLOYEE_HISTORY;
	public static final Map<String,String> EMPLOYEE,EMPLOYEE_HISTORY;
	public static final Map<String,String> employeeDBConfig;
	
	
	static{
		
		//Qualifications
			
		TBL_QUALIFICATIONS=CommonUtils.getPropertyValue("QUALIFICATIONS_TABLE_NAMES", "MOD_QUALIFICATIONS");
		TBL_QUALIFICATIONS_HISTORY=CommonUtils.getPropertyValue("QUALIFICATIONS_TABLE_NAMES", "MOD_QUALIFICATIONS_HISTORY");
			
		QUALIFICATIONS=(Map<String,String>)CommonUtils.getBean("MOD_QUALIFICATIONS");
		QUALIFICATIONS_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_QUALIFICATIONS_HISTORY");
			
		qualificationsDBConfig=(Map<String,String>)CommonUtils.getBean("qualificationsDBConfig");
		
	   //Employee
		
		TBL_EMPLOYEE=CommonUtils.getPropertyValue("EMPLOYEE_TABLE_NAMES", "MOD_EMPLOYEE");
		TBL_EMPLOYEE_HISTORY=CommonUtils.getPropertyValue("EMPLOYEE_TABLE_NAMES", "MOD_EMPLOYEE_HISTORY");
					
		EMPLOYEE=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE");
		EMPLOYEE_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_HISTORY");
					
		employeeDBConfig=(Map<String,String>)CommonUtils.getBean("employeeDBConfig");
   }
}
