package com.orb.qualifications.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

import com.orb.qualifications.bean.Qualifications;

public interface QualificationsService {

	
	//Qualifications
	public List<Qualifications> getInactiveQualificationsList(CommonError commonError);
	public List<Qualifications> getActiveQualificationsList(CommonError commonError);
	public Qualifications getQualifications(CommonError commonError, String qualificationsId);
	public void lockQualifications(CommonError commonError, User loginUser, List<Qualifications> selectedQualificationsList);
	public void unlockQualifications(CommonError commonError, User loginUser, List<Qualifications> selectedQualificationsList);
	public void deleteQualifications(CommonError commonError, User loginUser, List<Qualifications> selectedQualificationsList);
	public void deleteViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications);
	public void lockViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications);
	public void unlockViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications);
	public void add(CommonError commonError, User loginUser, Qualifications qualifications);
	public void update(CommonError commonError, User loginUser, Qualifications qualifications);
	
	//Employee

}
