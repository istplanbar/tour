package com.orb.qualifications.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

import com.orb.qualifications.bean.Qualifications;

import com.orb.qualifications.dao.QualificationsDAO;
import com.orb.qualifications.service.QualificationsService;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class QualificationsServiceImpl implements QualificationsService {
	private static Log logger = LogFactory.getLog(QualificationsServiceImpl.class);

	
	@Autowired
	QualificationsDAO qualificationsDAO;
	

	
	/**
	 * This method calls DAO to get all Inactive qualifications from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Qualifications> getInactiveQualificationsList(CommonError commonError) {
		return  qualificationsDAO.getQualificationsList(commonError,CommonConstants.N);
	}

	
	/**
	 * This method calls DAO to get all Inactive qualifications list from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Qualifications> getActiveQualificationsList(CommonError commonError) {
		return qualificationsDAO.getQualificationsList(commonError,CommonConstants.Y);
	}

	
	/**
	 * This method will interact with DAO to get  qualifications in DB
	 * @param commonError
	 * @param qualificationsId
	 * @return
	 */
	@Override
	public Qualifications getQualifications(CommonError commonError, String qualificationsId) {
		return qualificationsDAO.getQualifications(commonError, qualificationsId);
	}
	
	/**
	 * This method calls DAO to de-activate qualifications
	 * @param commonError
	 * @param loginUser
	 * @param selectedQualificationsList
	 */
	@Override
	public void lockQualifications(CommonError commonError, User loginUser,
			List<Qualifications> selectedQualificationsList) {
		List<String> idList=new ArrayList<>();
		if( selectedQualificationsList == null || selectedQualificationsList.isEmpty() ){
		return;
		}
		for( Qualifications qualifications : selectedQualificationsList ){
			idList.add(qualifications.getId());
		}
		qualificationsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );				
	}

	/**
	 * This method calls DAO to activate the locked qualifications
	 * @param commonError
	 * @param loginUser
	 * @param selectedQualificationsList
	 */
	@Override
	public void unlockQualifications(CommonError commonError, User loginUser,
			List<Qualifications> selectedQualificationsList) {
		List<String> idList=new ArrayList<>();
		if( selectedQualificationsList == null || selectedQualificationsList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Qualifications qualifications : selectedQualificationsList ){
			idList.add(qualifications.getId());
		}
		qualificationsDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);		}

	
	/**
	 * This method will interact with DAO to delete a qualifications in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedQualificationsList
	 */
	@Override
	public void deleteQualifications(CommonError commonError, User loginUser,
			List<Qualifications> selectedQualificationsList) {
		List<String> idList=new ArrayList<>();
		if( selectedQualificationsList == null || selectedQualificationsList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Qualifications qualifications : selectedQualificationsList ){
			idList.add(qualifications.getId());
		}
		qualificationsDAO.deleteQualifications(commonError, loginUser, idList);				
	}

	/**
	 * This method calls DAO to deletes particular qualifications
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	@Override
	public void deleteViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications) {
		List<String> idList=new ArrayList<>();
		idList.add(qualifications.getId());
		qualificationsDAO.deleteQualifications(commonError, loginUser, idList);		
	}

	/**
	 * This method calls DAO to de-activate qualifications
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	@Override
	public void lockViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications) {
		List<String> idList=new ArrayList<>();
		idList.add(qualifications.getId());
		qualificationsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );		
	}

	/**
	 * This method calls DAO to activate qualifications
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	@Override
	public void unlockViewedQualifications(CommonError commonError, User loginUser, Qualifications qualifications) {
		List<String> idList=new ArrayList<>();
		idList.add(qualifications.getId());
		qualificationsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );			
	}
	
	/**
	 * This method will interact with DAO to insert a qualifications in DB
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	@Override
	public void add(CommonError commonError, User loginUser, Qualifications qualifications) {
		qualificationsDAO.addQualifications(commonError, loginUser, qualifications );				
	}

	/**
	 * This method will interact with DAO to update a qualifications in DB
	 * @param commonError
	 * @param loginUser
	 * @param qualifications
	 */
	@Override
	public void update(CommonError commonError, User loginUser, Qualifications qualifications) {
		qualificationsDAO.updateQualifications(commonError, loginUser, qualifications );			
	}


	
	

}
