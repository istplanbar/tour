package com.orb.mail.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.mail.bean.Mail;
import com.orb.mail.constants.MailConstants;
import com.orb.mail.constants.MailDBConstants;



@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class MailDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(MailDAO.class);
	

	/**
	 * This method inserts new sender mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param m
	 */
	public void addSenderMail(CommonError commonError, User loginUser,Mail m) {
		logger.info(" ==> Insert Sender Mail START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			String nextRowId = getNextRowId( MailDBConstants.TBL_MAIL_SENDER, MailDBConstants.mailDBConfig.get("rowPrefix") );
			m.setIdentifier(nextRowId);
			java.util.Date date = CommonUtils.getCurrentUTCDate();
			qry.append("INSERT INTO ");qry.append( MailDBConstants.TBL_MAIL_SENDER);qry.append("( ");
			for( String column : MailDBConstants.MAIL_SENDER.keySet() ){
				if(!column.equals(MailDBConstants.MAIL_SENDER.get("ID"))){
					qry.append( MailDBConstants.MAIL_SENDER.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			for(int i=0;i< m.getMailReceiverList().size();i++){
				List<Object> args = new ArrayList<Object>();
				args.add(m.getIdentifier());args.add(loginUser.getUserId());args.add(m.getMailReceiverList().get(i));

				args.add(m.getSubject());args.add(m.getText());args.add(m.getExpiryDate());
				args.add(m.getAttachment());args.add(m.getDirName());args.add(null);
				args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);

				args.add(CommonConstants.Y);



				getJdbcTemplate().update( qry.toString(),args.toArray() );
				args.clear();
				}
			addMail(commonError,loginUser,m);
			addReceiverMail(commonError,loginUser,m);
		}catch( Exception e ){
			logger.info("Error when Insert SenderMail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> Insert SenderMail END <== ");
	}
	
	/**
	 * This method inserts new receiver mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param m
	 */
	private void addReceiverMail(CommonError commonError, User loginUser,Mail m) {
		logger.info(" ==> Insert ReceiverMail START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			java.util.Date date = CommonUtils.getCurrentUTCDate();
			qry.append("INSERT INTO ");qry.append( MailDBConstants.TBL_MAIL_RECEIVER);qry.append("( ");
			for( String column : MailDBConstants.MAIL_RECEIVER.keySet() ){
				if(!column.equals(MailDBConstants.MAIL_RECEIVER.get("ID"))){
					qry.append( MailDBConstants.MAIL_RECEIVER.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			for(int i=0;i< m.getMailReceiverList().size();i++){
				List<Object> args = new ArrayList<Object>();
				args.add(m.getIdentifier());args.add(loginUser.getUserId());args.add(m.getMailReceiverList().get(i));
				args.add(m.getSubject());args.add(m.getText());args.add(m.getExpiryDate());
				args.add(m.getAttachment());args.add(m.getDirName());args.add(CommonConstants.FALSE);args.add(m.getReceiverReadTime());
				args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);
				args.add(CommonConstants.Y);
				getJdbcTemplate().update( qry.toString(),args.toArray() );
				args.clear();
				}
			
			
		}catch( Exception e ){
			logger.info("Error when Insert ReceiverMail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> Insert ReceiverMail END <== ");
		
	}
	
	/**
	 * This method inserts new receiver mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param m
	 */
	private void addMail(CommonError commonError, User loginUser,Mail m) {
		logger.info(" ==> Insert Mail START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			java.util.Date date = CommonUtils.getCurrentUTCDate();
			qry.append("INSERT INTO ");qry.append( MailDBConstants.TBL_MAIL);qry.append("( ");
			for( String column : MailDBConstants.MAIL.keySet() ){
				if(!column.equals(MailDBConstants.MAIL.get("ID"))){
					qry.append( MailDBConstants.MAIL.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			for(int i=0;i< m.getMailReceiverList().size();i++){
			List<Object> args = new ArrayList<Object>();
			args.add(m.getIdentifier());args.add(loginUser.getUserId());args.add(m.getMailReceiverList().get(i));
			args.add(m.getSubject());args.add(m.getText());args.add(m.getExpiryDate());
			args.add(m.getAttachment());args.add(m.getDirName());args.add(CommonConstants.FALSE);args.add(m.getReceiverReadTime());
			args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);
			args.add(CommonConstants.Y);
			getJdbcTemplate().update( qry.toString(),args.toArray() );
			args.clear();
			}
			
			
		}catch( Exception e ){
			logger.info("Error when Insert Mail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> Insert Mail END <== ");
		
	}
	
	/**
	 * This method will return list sender mail from DB
	 * @param commonError
	 * @return
	 */
	public List<Mail> getMailSenderList( CommonError commonError) {
		logger.info(" ==> getMailSenderList START <== ");
		List<Mail> mailSenderList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(MailDBConstants.MAIL_SENDER.get("IDENTIFIER"));
			qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("ID"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("SENDER"));qry.append(",");
			qry.append(" GROUP_CONCAT(");
			qry.append(MailDBConstants.MAIL_SENDER.get("RECEIVER"));
			qry.append(" SEPARATOR ', ')");qry.append(" AS ");qry.append(MailDBConstants.MAIL_SENDER.get("RECEIVER"));
			qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("SUBJECT"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("TEXT"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("EXPIRY_DATE"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("ATTACHMENT") );qry.append( "," );
			qry.append(MailDBConstants.MAIL_SENDER.get("DIR_NAME") );qry.append( "," );
			qry.append(MailDBConstants.MAIL_SENDER.get("RECEIVER_READ_TIME"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("CREATE_BY"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("CREATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("UPDATE_BY"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("UPDATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.MAIL_SENDER.get("ACTIVE") );
			qry.append(" FROM ");qry.append(MailDBConstants.TBL_MAIL_SENDER);
			qry.append(" GROUP BY IDENTIFIER ORDER BY CREATE_TIME DESC ");
			mailSenderList = getJdbcTemplate().query( qry.toString(),new String[]{},new BeanPropertyRowMapper<Mail>(Mail.class) );
			
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch mail sender list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getMailSenderList END <== ");
		return mailSenderList;
	}
	
	/**
	 * This method will return list sender mail from DB
	 * @param commonError
	 * @return
	 */
	public List<Mail> getMailReceiverList( CommonError commonError) {
		logger.info(" ==> getMailReceiverList START <== ");
		List<Mail> mailReceiverList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_RECEIVER.keySet() ){
				newQry.append( MailDBConstants.MAIL_RECEIVER.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_RECEIVER);
			newQry.append(" ORDER BY CREATE_TIME DESC ");
			mailReceiverList = getJdbcTemplate().query( newQry.toString(),new String[]{},new BeanPropertyRowMapper<Mail>(Mail.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch mail receiver list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getMailReceiverList END <== ");
		return mailReceiverList;
	}
	
	
	/**
	 * This method delete Sender mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param mail 
	 */
	public void deleteSenderMail(CommonError commonError, User loginUser, List<String> identifierList) {
		logger.info(" ==> Delete Sender Mail START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertSenderMailHistory( commonError, loginUser,identifierList,CommonConstants.DELETED);	
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", identifierList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(MailDBConstants.TBL_MAIL_SENDER);newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_SENDER.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the Sender mail. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Sender Mail END <== ");


	}
	
	/**
	 * This method delete the Receiver mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList 
	 */
	public void deleteReceiverMail(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Receiver Mail START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertReceiverMailHistory( commonError, loginUser,idList,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(MailDBConstants.TBL_MAIL_RECEIVER);newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_RECEIVER.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the Receiver mail. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Receiver Mail END <== ");


	}
	
	/**
	 * This method used to get a particular Receiver Mail from DB
	 * @param commonError
	 * @param mailId
	 * @return
	 */
	public Mail getReceiverMail(CommonError commonError, String mailId) {
		logger.info(" ==> Get a Receiver Mail START <== ");
		logger.info("Mail Id ==> " + mailId );
		Mail mail = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_RECEIVER.keySet() ){
				newQry.append( MailDBConstants.MAIL_RECEIVER.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_RECEIVER);
			newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_RECEIVER.get("ID") );newQry.append( " = ? " );
			mail = (Mail)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{mailId}, new BeanPropertyRowMapper<Mail>(Mail.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Receiver Mail list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get a Receiver Mail END <== ");
		return mail;
	}
	
	/**
	 * This method used to get a particular Mail from DB
	 * @param commonError
	 * @param mailId
	 * @return
	 */
	public Mail getSenderMail(CommonError commonError, String mailId) {
		logger.info(" ==> Get a Sender Mail START <== ");
		logger.info("Mail Id ==> " + mailId );
		Mail mail = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_SENDER.keySet() ){
				newQry.append( MailDBConstants.MAIL_SENDER.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_SENDER);
			newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_SENDER.get("ID") );newQry.append( " = ? " );
			mail = (Mail)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{mailId}, new BeanPropertyRowMapper<Mail>(Mail.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Sender Mail list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get a Sender Mail END <== ");
		return mail;
	}
	
	
	/**
	 * This method inserts Sender mail into mail history table
	 * @param commonError
	 * @param loginUser
	 * @param id
	 * @param action
	 * @throws Exception
	 */
	public void insertSenderMailHistory(CommonError commonError, User loginUser,List<String> identifierList, String action) throws Exception {
		logger.info(" ==> Insert Sender Mail History START <== ");
		StringBuffer newQryHist = new StringBuffer();
		StringBuffer newQry = new StringBuffer("SELECT ");
		for( String column : MailDBConstants.MAIL_SENDER.keySet() ){
			newQry.append( MailDBConstants.MAIL_SENDER.get(column) );newQry.append( "," );
		}
		newQry.setLength(newQry.length()-1);
		newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_SENDER);newQry.append(" WHERE ");newQry.append( MailDBConstants.MAIL_SENDER.get("IDENTIFIER") );
		newQry.append(" IN ( ");
		for ( int i=0; i<identifierList.size(); i++ ) {
			newQry.append(" ? ");newQry.append(",");
		}
		newQry.setLength(newQry.length() - 1);
		newQry.append(") ");
		List<Mail> mailList = (List<Mail>)getJdbcTemplate().query(newQry.toString(), identifierList.toArray(),  new BeanPropertyRowMapper<Mail>(Mail.class));
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQryHist.append("INSERT INTO ");newQryHist.append( MailDBConstants.TBL_MAIL_SENDER_HISTORY);newQryHist.append("( ");
			for( String column : MailDBConstants.MAIL_SENDER_HISTORY.keySet() ){
				if(!column.equals(MailDBConstants.MAIL_SENDER_HISTORY.get("ID"))){
				newQryHist.append( MailDBConstants.MAIL_SENDER_HISTORY.get(column) );newQryHist.append( "," );
				}
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			getJdbcTemplate().batchUpdate(newQryHist.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, mailList.get(i).getIdentifier());ps.setString(2, mailList.get(i).getSender());
					ps.setString(3, mailList.get(i).getReceiver());ps.setString(4, mailList.get(i).getSubject());
					ps.setString(5, mailList.get(i).getText());ps.setObject(6, mailList.get(i).getExpiryDate());
					ps.setString(7, mailList.get(i).getAttachment());ps.setString(8, mailList.get(i).getDirName());
					ps.setObject(9, mailList.get(i).getReceiverReadTime());
					ps.setString(10, loginUser.getUserId());ps.setTimestamp(11, date);
					ps.setString(12, loginUser.getUserId());ps.setTimestamp(13, date);
					ps.setString(14, action);
					
				}		
				public int getBatchSize() {
					return mailList.size();
				}
			});
			

		}catch( Exception e ){
			logger.info("Error when Insert Sender Mail History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Mail Sender History END <== ");
	}
	
	
	/**
	 * This method inserts Receiver mail into mail history table
	 * @param commonError
	 * @param loginUser
	 * @param id
	 * @param action
	 * @throws Exception
	 */
	public void insertReceiverMailHistory(CommonError commonError, User loginUser,List<String> idList, String action) throws Exception {
		logger.info(" ==> Insert Receiver Mail History START <== ");
		StringBuffer newQryHist = new StringBuffer();
		StringBuffer newQry = new StringBuffer("SELECT ");
		for( String column : MailDBConstants.MAIL_RECEIVER.keySet() ){
			newQry.append( MailDBConstants.MAIL_RECEIVER.get(column) );newQry.append( "," );
		}
		newQry.setLength(newQry.length()-1);
		newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_RECEIVER);newQry.append(" WHERE ");newQry.append( MailDBConstants.MAIL_RECEIVER.get("ID") );
		newQry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQry.append(" ? ");newQry.append(",");
		}
		newQry.setLength(newQry.length() - 1);
		newQry.append(") ");
		List<Mail> mailList = (List<Mail>)getJdbcTemplate().query(newQry.toString(), idList.toArray(),  new BeanPropertyRowMapper<Mail>(Mail.class));
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQryHist.append("INSERT INTO ");newQryHist.append( MailDBConstants.TBL_MAIL_RECEIVER_HISTORY);newQryHist.append("( ");
			for( String column : MailDBConstants.MAIL_RECEIVER_HISTORY.keySet() ){
				if(!column.equals(MailDBConstants.MAIL_RECEIVER_HISTORY.get("ID"))){
				newQryHist.append( MailDBConstants.MAIL_RECEIVER_HISTORY.get(column) );newQryHist.append( "," );
				}
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			for(int i=0;i < mailList.size();i++){
				List<Object> args = new ArrayList<Object>();
				args.add(mailList.get(i).getIdentifier());args.add(mailList.get(i).getSender());args.add(mailList.get(i).getReceiver());
				args.add(mailList.get(i).getSubject());args.add(mailList.get(i).getText());args.add(mailList.get(i).getExpiryDate());
				args.add(mailList.get(i).getAttachment());args.add(mailList.get(i).getDirName());args.add(mailList.get(i).getFlag());args.add(mailList.get(i).getReceiverReadTime());
				args.add(loginUser.getUserId());args.add(mailList.get(i).getCreateTime());args.add(loginUser.getUserId());args.add(mailList.get(i).getUpdateTime());
				args.add(action);
				getJdbcTemplate().update( newQryHist.toString(),args.toArray() );
				args.clear();
				}
			

		}catch( Exception e ){
			logger.info("Error when Insert Receiver Mail History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Mail Receiver History END <== ");
	}
	
	/**
	 * This method update flag in mail table
	 * @param commonError
	 * @param mail
	 * @param flag
	 */
	public void updateFlag(CommonError commonError, Mail mail, User loginUser, String flag) {
		logger.info(" ==> update Flag START <== ");
		logger.info("Mail Id ==> " + mail.getId());
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());				
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( MailDBConstants.TBL_MAIL_RECEIVER );qry.append(" SET ");
			qry.append( MailDBConstants.MAIL_RECEIVER.get("FLAG") );qry.append(" = ?,");
			qry.append( MailDBConstants.MAIL_RECEIVER.get("RECEIVER_READ_TIME") );qry.append(" = ? WHERE ");
			qry.append( MailDBConstants.MAIL_RECEIVER.get("ID") );qry.append(" = ? ");
			List<Object> args = new ArrayList<Object>();
			args.add(flag);
			args.add(date); args.add(mail.getId());
			getJdbcTemplate().update( qry.toString(),args.toArray() );
			updateReceiverReadTime(commonError, mail, loginUser, flag);
		}catch( Exception e ){
			logger.info("Error when update  Flag of a Mail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> update Flag END <== ");
	}
	
	
	public void updateMarkUnreadList(CommonError commonError, List<String> idList, User loginUser, String flag) {
		logger.info(" ==> update Flag START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( MailDBConstants.TBL_MAIL_RECEIVER );qry.append(" SET ");
			qry.append( MailDBConstants.MAIL_RECEIVER.get("FLAG") );qry.append(" = ");qry.append("'"+flag+"'");
			qry.append(" WHERE ");
			qry.append( MailDBConstants.MAIL_RECEIVER.get("ID") );qry.append(" IN ");
			qry.append(" (:ids) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when update   Flag of a Mail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> update  Flag END <== ");
	}

	public List<Mail> getDeletedMailList(CommonError commonError, String action, String userId) {
		logger.info(" ==> getDeletedMailList START <== ");
		List<Mail> deletedMailList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_RECEIVER_HISTORY.keySet() ){
				newQry.append( MailDBConstants.MAIL_RECEIVER_HISTORY.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_RECEIVER_HISTORY);newQry.append(" WHERE ");
			newQry.append(MailDBConstants.MAIL_RECEIVER_HISTORY.get("ACTION"));newQry.append(" = ? ");newQry.append(" AND ");
			newQry.append(MailDBConstants.MAIL_RECEIVER_HISTORY.get("CREATE_BY"));newQry.append(" = ? ");
			newQry.append(" ORDER BY CREATE_TIME DESC ");
			deletedMailList = getJdbcTemplate().query( newQry.toString(),new String[]{action,userId},new BeanPropertyRowMapper<Mail>(Mail.class) );
			
		    }catch( Exception e ){
				logger.error(" Exception occured when tries to fetch mail sender list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> getMailSenderList END <== ");
			return deletedMailList;
		}

	public void deleteMailHistory(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Receiver Mail History START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(MailDBConstants.TBL_MAIL_RECEIVER_HISTORY);newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_RECEIVER_HISTORY.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the receiver mail. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Receiver Mail History END <== ");

	}

	public List<Mail> getFilteredMailReceiverList(CommonError commonError, String mailId) {
		logger.info(" ==> getFilteredMailReceiverList START <== ");
		List<Mail> mailFilteredReceiverList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_SENDER.keySet() ){
				newQry.append( MailDBConstants.MAIL_SENDER.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_SENDER);
			newQry.append(" WHERE ");newQry.append(MailDBConstants.MAIL_SENDER.get("IDENTIFIER") );newQry.append( " = ? " );
			mailFilteredReceiverList = getJdbcTemplate().query( newQry.toString(),new String[]{mailId}, new BeanPropertyRowMapper<Mail>(Mail.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch filtered mail receiver list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getFilteredMailReceiverList END <== ");
		return mailFilteredReceiverList;
	}
	
	
	/**
	 * This method update flag in mail table
	 * @param commonError
	 * @param mail
	 * @param flag
	 */
	public void updateReceiverReadTime(CommonError commonError, Mail mail, User loginUser, String flag) {
		logger.info(" ==> update Flag START <== ");
		logger.info("Mail Id ==> " + mail.getIdentifier());
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());				
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( MailDBConstants.TBL_MAIL_SENDER );qry.append(" SET ");
			qry.append( MailDBConstants.MAIL_SENDER.get("RECEIVER_READ_TIME") );qry.append(" = ? WHERE ");
			qry.append( MailDBConstants.MAIL_SENDER.get("IDENTIFIER") );qry.append(" = ? ");qry.append(" AND ");
			qry.append( MailDBConstants.MAIL_SENDER.get("RECEIVER") );qry.append(" = ? ");
			List<Object> args = new ArrayList<Object>();
			args.add(date); args.add(mail.getIdentifier());
			args.add(mail.getReceiver());
			getJdbcTemplate().update( qry.toString(),args.toArray() );
		}catch( Exception e ){
			logger.info("Error when update  Flag of a Mail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> update Flag END <== ");
	}

	public Mail getTrashViewMail(CommonError commonError, String mailId) {
		logger.info(" ==> Get a Trash View Mail START <== ");
		logger.info("Mail Id ==> " + mailId );
		Mail mail = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : MailDBConstants.MAIL_RECEIVER_HISTORY.keySet() ){
				newQry.append( MailDBConstants.MAIL_RECEIVER_HISTORY.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL_RECEIVER_HISTORY);
			newQry.append(" WHERE ");
			newQry.append( MailDBConstants.MAIL_RECEIVER_HISTORY.get("ID") );newQry.append( " = ? " );
			mail = (Mail)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{mailId}, new BeanPropertyRowMapper<Mail>(Mail.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Trash Mail. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get a Trash View END <== ");
		return mail;
	}

	public List<String> getReceiverList(CommonError commonError, String identifier) {
		logger.info(" ==> getReceiverList START <== ");
		List<String> receiverList = null;
		try{
			
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( MailDBConstants.MAIL_SENDER.get("RECEIVER"));
			newQry.append(" FROM ");newQry.append(MailDBConstants.TBL_MAIL);newQry.append(" WHERE ");
			newQry.append(MailDBConstants.MAIL_SENDER.get("IDENTIFIER"));newQry.append(" =? ") ;
			receiverList = getJdbcTemplate().queryForList( newQry.toString() ,new Object[]{identifier}, String.class );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch mail receiver list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getReceiverList END <== ");
		return receiverList;
	}

	public String genNextDirName(){
		return getNextRowId( MailDBConstants.TBL_MAIL, MailConstants.NEXT_DIR_NAME);
	}
		
}

