package com.orb.mail.dao;


import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.mail.bean.GroupMail;
import com.orb.mail.constants.MailDBConstants;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class GroupMailDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(GroupMailDAO.class);
	
	

	/**
	 * This method will return list GroupUser from DB
	 * @param commonError
	 * @return
	 */
	public List<GroupMail> getGroupUserList( CommonError commonError) {
		logger.info(" ==> getGroupUserList START <== ");
		List<GroupMail> mailUserList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(MailDBConstants.GROUP_MAIL.get("ID"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("IDENTIFIER"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("GROUP_NAME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("LIST_OF_USER"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("CREATE_BY"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("CREATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("UPDATE_BY"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("UPDATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("ACTIVE") );
			qry.append(" FROM ");qry.append(MailDBConstants.TBL_GROUP_MAIL);
			
			mailUserList = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(GroupMail.class) );			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch mail sender list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getGroupUserList END <== ");
		return mailUserList;
	}

	/**
	 * This method inserts new Group mail into DB
	 * @param commonError
	 * @param loginUser
	 * @param g
	 */
	public void addGroupMail(CommonError commonError, User loginUser,GroupMail g) {
		logger.info(" ==> Sent GroupMail START <== ");
		StringBuffer newQry = new StringBuffer();

		try{
			String nextRowId = getNextRowId( MailDBConstants.TBL_GROUP_MAIL, MailDBConstants.groupMailDBConfig.get("rowPrefix") );
			g.setIdentifier(nextRowId);
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( MailDBConstants.TBL_GROUP_MAIL );newQry.append("( ");
			newQry.append( MailDBConstants.GROUP_MAIL.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("GROUP_NAME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("LIST_OF_USER") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("CREATE_BY") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(g.getIdentifier());args.add(g.getGroupName());
			args.add(g.getListOfUser());args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );
			

		}catch( Exception e ){
			logger.info("Error when Sent Group Mail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Sent Group Mail END <== ");
	}
	

	/**
	 * This method used to get a particular GroupMail from DB
	 * @param commonError
	 * @param groupMailId
	 * @return
	 */
	public GroupMail getGroupMail(CommonError commonError, String groupMailId) {
		logger.info(" ==> Get a GroupMail View START <== ");
		logger.info("Mail Id ==> " + groupMailId );
		GroupMail groupMail = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(MailDBConstants.GROUP_MAIL.get("ID"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("IDENTIFIER"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("GROUP_NAME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("LIST_OF_USER"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("CREATE_BY"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("CREATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("UPDATE_BY"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("UPDATE_TIME"));qry.append(",");
			qry.append(MailDBConstants.GROUP_MAIL.get("ACTIVE") );
			qry.append(" FROM ");qry.append(MailDBConstants.TBL_GROUP_MAIL);qry.append(" WHERE ");
			qry.append( MailDBConstants.GROUP_MAIL.get("IDENTIFIER") );qry.append( " = ? " );
			groupMail = (GroupMail)getJdbcTemplate().queryForObject( qry.toString(),new String[]{groupMailId}, new BeanPropertyRowMapper(GroupMail.class) );
	
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Mail list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a GroupMail View END <== ");
		return groupMail;
	}
	

    /**
     *  This method updates existing GroupMail in DB
     * @param commonError
     * @param loginUser
     * @param g
     */
	public void updateGroupMail(CommonError commonError, User loginUser, GroupMail g) {
		logger.info(" ==> Update GroupMail Update START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			insertGroupMailHistory( commonError, loginUser,g.getId(), CommonConstants.UPDATED );
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append(" UPDATE ");newQry.append( MailDBConstants.TBL_GROUP_MAIL );newQry.append(" SET ");
			newQry.append( MailDBConstants.GROUP_MAIL.get("IDENTIFIER") );newQry.append( " = ?," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("GROUP_NAME") );newQry.append( " = ?," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("LIST_OF_USER") );newQry.append( " = ?," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("UPDATE_BY") );newQry.append( " = ?," );
			newQry.append( MailDBConstants.GROUP_MAIL.get("UPDATE_TIME") );newQry.append( " = ?" );
			newQry.append(" WHERE ");newQry.append( MailDBConstants.GROUP_MAIL.get("ID") );newQry.append(" = ? ");
			List<Object> args = new ArrayList<Object>();
			args.add(g.getIdentifier());args.add(g.getGroupName());
			args.add(g.getListOfUser());
			args.add(loginUser.getUserId());
			args.add(date);args.add(g.getId());
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}
		catch( Exception e ){
			logger.info("Error when Update GroupMail.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==>  Update GroupMail END <== ");

	}
	

 	/**
 	 * This method delete the GroupMail into DB
 	 * @param commonError
 	 * @param loginUser
 	 * @param m
 	 */
	public void deleteGroupMail(CommonError commonError, User loginUser, GroupMail m) {
 		logger.info(" ==> Delete GroupMail START <== ");
 		logger.info("Mail Id ==> " + m.getId());
 		try{
 			insertGroupMailHistory( commonError, loginUser,m.getId(),CommonConstants.DELETED);	
 			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
 			newQry.append(MailDBConstants.TBL_GROUP_MAIL);newQry.append(" WHERE ");
 			newQry.append(MailDBConstants.GROUP_MAIL.get("ID") );newQry.append( " = ? " );
 			getJdbcTemplate().update( newQry.toString(),new Object[]{m.getId()} );
 		}catch( Exception e ){
 			logger.error(" Exception occured when tries to delete the GroupMail. Exception ==> " + e.getMessage() );
 			commonError.addError(CommonConstants.DB_ERROR);
 		}
 		logger.info(" ==> Delete GroupMail END <== ");


 	}
     
	

	/**
	 * This method inserts delete GroupMail into mail history table
	 * @param commonError
	 * @param loginUser
	 * @param id
	 * @param action
	 * @throws Exception
	 */
	public void insertGroupMailHistory(CommonError commonError, User loginUser,String id, String action) throws Exception {
		logger.info(" ==> Insert GroupMail History START <== ");
		logger.info("Mail Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : MailDBConstants.GROUP_MAIL.keySet() ){
			newQryHist.append( MailDBConstants.GROUP_MAIL.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(MailDBConstants.TBL_GROUP_MAIL);newQryHist.append(" WHERE ");newQryHist.append( MailDBConstants.GROUP_MAIL.get("ID") );newQryHist.append( "=?" );
		GroupMail m = (GroupMail)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(GroupMail.class) );
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( MailDBConstants.TBL_GROUP_MAIL_HISTORY);newQry.append("( ");
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("GROUP_NAME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("LIST_OF_USER") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("CREATE_BY") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( MailDBConstants.GROUP_MAIL_HISTORY.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(m.getIdentifier());args.add(m.getGroupName());
			args.add(m.getListOfUser());
			args.add(m.getCreateBy());args.add(m.getCreateTime());
			args.add(loginUser.getUserId());args.add(date);
			args.add(action);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert GroupMailHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert GroupMail History END <== ");
	}
 	
 	
}