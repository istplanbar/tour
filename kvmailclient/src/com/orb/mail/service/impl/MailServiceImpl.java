package com.orb.mail.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.mail.bean.GroupMail;
import com.orb.mail.bean.Mail;
import com.orb.mail.constants.MailConstants;
import com.orb.mail.dao.MailDAO;
import com.orb.mail.dao.GroupMailDAO;
import com.orb.mail.service.MailService;



@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class MailServiceImpl implements MailService{
	private static Log logger = LogFactory.getLog(MailServiceImpl.class);
	
	 @Autowired
	 MailDAO mailDAO;
	 
	 @Autowired
	 GroupMailDAO groupMailDAO;
	 
	    /**
		 * This method will interact with DAO to insert a Mail in DB
		 */
		@Override
		public void add(CommonError commonError, User loginUser, Mail mail ) {
			mailDAO.addSenderMail(commonError, loginUser, mail );
		}
		
		
		/**
		 * This method will interact with DAO to get the Mail Sender list in DB
		 */
		@Override
		public List<Mail> getMailSenderList(CommonError commonError) {
			return mailDAO.getMailSenderList(commonError);
		}
		
		/**
		 * This method will interact with DAO to get the Mail Receiver list in DB
		 */
		@Override
		public List<Mail> getMailReceiverList(CommonError commonError) {
			return mailDAO.getMailReceiverList(commonError);
		}
		
		/**
		 * This method will interact with DAO to get the Mail Deleted list in DB
		 */
		@Override
		public List<Mail> getDeletedMailList(CommonError commonError, String action, String userId) {
			return mailDAO.getDeletedMailList(commonError,action,userId);
		}
		
		
		/**
		 * This method will interact with DAO to delete a mail in DB
		 */
		@Override
		public void deleteSenderMail(CommonError commonError, User loginUser, List<Mail>  selectedMailList) {
			List<String> identifierList = new ArrayList<>();
			if( selectedMailList == null || selectedMailList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for(Mail mail: selectedMailList ){
				identifierList.add(mail.getIdentifier());
			}
			mailDAO.deleteSenderMail(commonError, loginUser, identifierList);
		}
		
		/**
		 * This method will interact with DAO to delete a mail in DB
		 */
		@Override
		public void deleteReceiverMail(CommonError commonError, User loginUser, List<Mail>  selectedMailList) {
			List<String> idList = new ArrayList<>();
			if( selectedMailList == null || selectedMailList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for(Mail mail: selectedMailList ){
				idList.add(mail.getId());
			}
			mailDAO.deleteReceiverMail(commonError, loginUser, idList);
			/*for( Mail mail: selectedMailList ){
				mailDAO.deleteReceiverMail(commonError, loginUser, mail);
			}*/
		}
		
		
		/**
		 * This method will interact with DAO to delete a mail in DB
		 */
		@Override
		public void deleteMailHistory(CommonError commonError, User loginUser, List<Mail>  selectedMail) {
			List<String> idList = new ArrayList<>();
			if( selectedMail == null || selectedMail.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Mail mail: selectedMail ){
				idList.add(mail.getId());
			}
			mailDAO.deleteMailHistory(commonError, loginUser, idList);
		}
		
		
		@Override
		public void deleteViewedSenderMail(CommonError commonError, User loginUser, Mail mail){
			List<String> identifierList = new ArrayList<>();
			identifierList.add(mail.getIdentifier());
			mailDAO.deleteSenderMail(commonError, loginUser, identifierList);
		}
		
		@Override
		public void deleteViewedReceiverMail(CommonError commonError, User loginUser, Mail mail){
			List<String> idList = new ArrayList<>();
			idList.add(mail.getId());
			mailDAO.deleteReceiverMail(commonError, loginUser, idList);
		}
		
		/**
		 * This method calls DAO to get existing mail from DB
		 */
		@Override
		public Mail getSenderMail(CommonError commonError, String mailId) {
			return mailDAO.getSenderMail(commonError,mailId);
		}
		
		/**
		 * This method calls DAO to get existing mail from DB
		 */
		@Override
		public Mail getReceiverMail(CommonError commonError, String mailId) {
			return mailDAO.getReceiverMail(commonError,mailId);
		}
		
		/**
		 * This method calls MailMenu
		 */
		public List<String> getMailMenu(int size) {
	        List<String> list = new ArrayList<String>();
	        for(int i = 0 ; i < 1 ; i++) {
	            list.add(MailConstants.MAIL_INBOX);
	            list.add(MailConstants.MAIL_OUTBOX);
	            list.add(MailConstants.MAIL_POSTBOX);
	            list.add(MailConstants.MAIL_TRASH);
	            list.add(MailConstants.MAIL_GROUP);
	        	
	        }
	       return list;
	    }
		
		@Override
		public void updateFlag(CommonError commonError, Mail mail, User loginUser, String flag) {
			mailDAO.updateFlag(commonError, mail,loginUser, flag );
		}
		
		
		@Override
		public void updateMarkUnreadList(CommonError commonError, List<Mail>  SelectedMail, User loginUser, String flag) {
			List<String> idList = new ArrayList<>();
			if( SelectedMail == null || SelectedMail.isEmpty() ){
				//logger.info("No record is available to activate");
				return;
			}
			for( Mail mail : SelectedMail ){
				idList.add(mail.getId());
			}
			mailDAO.updateMarkUnreadList(commonError, idList,loginUser,flag);
		}
		
		@Override
		public Mail getTrashViewMail(CommonError commonError, String mailId) {
			return mailDAO.getTrashViewMail(commonError,mailId);
		}
		
		/**
		 * This method calls DAO to get filtered Receiver mail from DB
		 */
		@Override
		public List<Mail> getFilteredMailReceiverList(CommonError commonError, String mailId) {
			return mailDAO.getFilteredMailReceiverList(commonError,mailId);
		}
		
		@Override
		public void deleteTrashViewedMail(CommonError commonError, User loginUser, Mail mail) {
			List<String> idList = new ArrayList<>();
			idList.add(mail.getId());
			mailDAO.deleteMailHistory(commonError, loginUser, idList);
		}
		
		@Override
		public List<String> getReceiverList(CommonError commonError, String identifier) {
			return mailDAO.getReceiverList(commonError,identifier);
		}
		
		
//Group Mail
		/**
		 * This method will interact with DAO to get the Group Mail Sender list in DB
		 */
		@Override
		public List<GroupMail> getGroupUserList(CommonError commonError) {
			return groupMailDAO.getGroupUserList(commonError);
		}
		
		/**
		 * This method will interact with DAO to insert a Group Mail in DB
		 */
		@Override
		public void addGroupMail(CommonError commonError, User loginUser, GroupMail mail ) {
			groupMailDAO.addGroupMail(commonError, loginUser, mail );
		}		
		
		/**
		 * This method calls DAO to get existing mail from DB
		 */
		@Override
		public GroupMail getGroupMail(CommonError commonError, String groupMailId) {
			return groupMailDAO.getGroupMail(commonError,groupMailId);
		}
		
		/**
		 * This method will interact with DAO to update a GroupMail in DB
		 */
		@Override
		public void updateGroupMail(CommonError commonError, User loginUser, GroupMail mail ) {
			groupMailDAO.updateGroupMail(commonError, loginUser, mail);
		}
		
		
		/**
		 * This method will interact with DAO to delete a GroupMail in DB
		 */
		@Override
		public void deleteGroupMail(CommonError commonError, User loginUser, List<GroupMail>  SelectedMail) {
			if( SelectedMail == null || SelectedMail.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( GroupMail groupMail: SelectedMail ){
				groupMailDAO.deleteGroupMail(commonError, loginUser, groupMail);
			}
		}


		/**
		 * Generate directory for uploading files
		 */
		public String getNextDir(String rowId){
			logger.info(StringUtils.leftPad("foobar", 10, '*'));
			String dirName = null;
			try{
				if(rowId == null)
					dirName = mailDAO.genNextDirName();
				else
					dirName =  String.format("task-dir-"+StringUtils.leftPad(rowId, 5, '0'));
			}catch (Exception e) {
				   logger.error(e.getMessage());
			       return "";
			   }
			
			return dirName;
		}


		
		
}
