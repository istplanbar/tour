package com.orb.mail.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.mail.bean.GroupMail;
import com.orb.mail.bean.Mail;


public interface MailService {
	
	public void add( CommonError commonError, User loginUser, Mail mail);
	public List<Mail> getMailSenderList(CommonError commonError);
	public List<Mail> getMailReceiverList(CommonError commonError);
	public void deleteSenderMail(CommonError commonError, User loginUser, List<Mail> mailList);
	public void deleteReceiverMail(CommonError commonError, User loginUser, List<Mail> mailList);
	
	public Mail getSenderMail(CommonError commonError, String mailId);
	public Mail getReceiverMail(CommonError commonError, String mailId);
	public List<String> getMailMenu(int size);
	public void updateFlag( CommonError commonError, Mail mail, User loginUser, String flag );
	public void updateMarkUnreadList( CommonError commonError, List<Mail> mailList, User loginUser, String flag );
	public List<Mail> getDeletedMailList(CommonError commonError, String action, String userId);
	public void deleteMailHistory(CommonError commonError, User loginUser, List<Mail> selectedMail);
	public void deleteViewedSenderMail(CommonError commonError, User loginUser, Mail mail);
	public void deleteViewedReceiverMail(CommonError commonError, User loginUser, Mail mail);
	public List<Mail> getFilteredMailReceiverList(CommonError commonError, String mailId);
	public Mail getTrashViewMail(CommonError commonError, String mailId);
	public void deleteTrashViewedMail(CommonError commonError, User loginUser, Mail mail);
	public List<String> getReceiverList(CommonError commonError, String identifier);
	
	//GroupMail
	public List<GroupMail> getGroupUserList(CommonError commonError);
	public void addGroupMail( CommonError commonError, User loginUser, GroupMail mail);
	public GroupMail getGroupMail(CommonError commonError, String groupMailId);
	public void updateGroupMail( CommonError commonError, User loginUser, GroupMail mail);
	public void deleteGroupMail(CommonError commonError, User loginUser, List<GroupMail> mailList);
	
	public String getNextDir( String id );
	
	
	
	
	
	
}