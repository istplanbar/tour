package com.orb.mail.constants;

import java.util.Map;
import com.orb.common.client.utils.CommonUtils;
public class MailDBConstants {
	
	public static final String TBL_MAIL,TBL_MAIL_HISTORY;
	public static final Map<String,String> MAIL,MAIL_HISTORY;
	
	public static final String TBL_MAIL_SENDER,TBL_MAIL_SENDER_HISTORY;
	public static final Map<String,String> MAIL_SENDER,MAIL_SENDER_HISTORY;
	
	public static final String TBL_MAIL_RECEIVER,TBL_MAIL_RECEIVER_HISTORY;
	public static final Map<String,String> MAIL_RECEIVER,MAIL_RECEIVER_HISTORY;
	
	public static final Map<String,String> mailDBConfig;
	
	//Group Mail
	public static final String TBL_GROUP_MAIL,TBL_GROUP_MAIL_HISTORY;
	public static final Map<String,String> GROUP_MAIL,GROUP_MAIL_HISTORY;
	public static final Map<String,String> groupMailDBConfig;
	
	static{
		
		mailDBConfig=(Map<String,String>)CommonUtils.getBean("mailDBConfig");
		
		MAIL=(Map<String,String>)CommonUtils.getBean("MOD_MAIL");
		MAIL_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_MAIL_HISTORY");
		
		TBL_MAIL=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL");
		TBL_MAIL_HISTORY=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL_HISTORY");
			
		MAIL_SENDER=(Map<String,String>)CommonUtils.getBean("MOD_MAIL_SENDER");
		MAIL_SENDER_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_MAIL_SENDER_HISTORY");
		
		TBL_MAIL_SENDER=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL_SENDER");
		TBL_MAIL_SENDER_HISTORY=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL_SENDER_HISTORY");
		
		MAIL_RECEIVER=(Map<String,String>)CommonUtils.getBean("MOD_MAIL_RECEIVER");
		MAIL_RECEIVER_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_MAIL_RECEIVER_HISTORY");
		
		TBL_MAIL_RECEIVER=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL_RECEIVER");
		TBL_MAIL_RECEIVER_HISTORY=CommonUtils.getPropertyValue("MAIL_TABLE_NAMES", "MOD_MAIL_RECEIVER_HISTORY");
		
		
		groupMailDBConfig=(Map<String,String>)CommonUtils.getBean("groupMailDBConfig");
		
		GROUP_MAIL=(Map<String,String>)CommonUtils.getBean("GROUP_MAIL_TABLE");
		GROUP_MAIL_HISTORY=(Map<String,String>)CommonUtils.getBean("GROUP_MAIL_TABLE_HISTORY");
		
		TBL_GROUP_MAIL=CommonUtils.getPropertyValue("GROUP_MAIL_TABLE_NAMES", "GROUP_MAIL_TABLE");
		TBL_GROUP_MAIL_HISTORY=CommonUtils.getPropertyValue("GROUP_MAIL_TABLE_NAMES", "GROUP_MAIL_TABLE_HISTORY");
		
		
	}
}

