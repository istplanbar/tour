package com.orb.mail.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class MailConstants {
	
	public final static String SELECTED_MAIL_ID = "SELECTED_MAIL_ID";
	public final static String MAIL_INBOX_PAGE = "/common/process/admin/loadInboxMail.html";
	public final static String MAIL_OUTBOX_PAGE = "/common/process/admin/loadOutboxMail.html";
	public final static String MAIL_POST_PAGE = "/common/process/admin/loadPostMail.html";
	public final static String MAIL_INBOXVIEW_PAGE = "/common/process/admin/loadInboxViewMail.html";
	public final static String MAIL_OUTBOXVIEW_PAGE = "/common/process/admin/loadOutboxViewMail.html";
	public final static String MAIL_INBOX_HTML = "mail/inbox.xhtml";
	public final static String MAIL_OUTBOX_HTML = "mail/outbox.xhtml";
	public final static String MAIL_POST_HTML = "mail/post.xhtml";
	public final static String MAIL_INBOXVIEW_HTML = "mail/inboxView.xhtml";
	public final static String MAIL_OUTBOXVIEW_HTML = "mail/outboxView.xhtml";
	public final static String MAIL_SENT_SUCCESS = "mail_sent_success";
	public final static String MAIL_DELETE_SUCCESS = "mail_delete_success";
	public final static String MAIL_INBOX = "Inbox";
	public final static String MAIL_OUTBOX = "Outbox";
	public final static String MAIL_POSTBOX = "PostBox";
	public static final String MAIL_TRASH = "Trash";;
	public final static String MAIL_GROUP = "GroupUser";
	public final static String SHOWING_MAIL_VIEW = "SHOWING_MAIL_VIEW" ;
	public static final Map<String,String> SUPPORTED_FORMAT;
	public final static String MAIL_ADD_SUCCESS="mail_added_success";
	public final static String MAIL_UPDATE_SUCCESS="mail_update_success";
	public final static String MAIL_TOGGLER_LIST = "MAIL_TOGGLER_LIST" ;
	public final static String MARK_UNREAD_MAIL_SUCCESS="mark_unread_mail_success";
	public static final String MAIL_TRASH_HTML = "mail/trash.xhtml";
	public static final String MAIL_TRASHVIEW_PAGE = "/common/process/admin/loadTrashViewMail.html";
	public static final String MAIL_TRASHVIEW_HTML = "mail/trashView.xhtml";
	public static final String MAIL_TRASH_PAGE = "/common/process/admin/loadTrash.html";
	public static final String TRASH_VIEW_PAGE = "TRASH_VIEW_PAGE";
	public final static String MAIL_INBOX_COUNT = "MAIL_INBOX_COUNT";
	public static final String MAIL_DATE_FORMAT = "dd.MM.yyyy hh:MM 'Uhr'";
	public final static String NEXT_DIR_NAME = "mail-dir-";
	
	
	
//GroupMail
	public final static String SELECTED_GROUP_MAIL_ID = "SELECTED_GROUP_MAIL_ID";
	public final static String MAIL_GROUP_PAGE = "/common/process/admin/loadGroupMail.html";
	//public final static String MAIL_GROUPEDIT_PAGE = "/common/process/admin/loadGroupAddMail.html";
	public final static String MAIL_GROUPVIEW_PAGE = "/common/process/admin/loadGroupViewMail.html";
	public final static String MAIL_GROUP_HTML = "mail/group.xhtml";
	public final static String MAIL_GROUPEDIT_HTML = "mail/groupAdd.xhtml";
	public final static String MAIL_GROUPVIEW_HTML = "mail/groupView.xhtml";
	public final static String GROUP_DELETE_SUCCESS = "groupuser_delete_success";
	public final static String BOOL_BTN_VIEW="BOOL_BTN_VIEW";
	public final static String MAIL_VIEW = "view";
	public final static String MAIL_EDIT = "edit";
	
	
	public final static String MAIL_GROUPEDIT_PAGE = "/common/process/admin/loadEditGroupMail.html";
	//public final static String MAIL_GROUPEDIT_HTML1 = "mail/groupEdit.xhtml";
	public final static String MAIL_GROUPADD_SUCCESS = "groupuser_added_success";
	public final static String MAIL_GROUPUPDATE_SUCCESS="groupuser_update_success";
	public final static String GROUPMAIL_TOGGLER_LIST = "GROUPMAIL_TOGGLER_LIST" ;
	
	
	
	
	
	
	static{
	SUPPORTED_FORMAT = (Map<String,String>)CommonUtils.getBean("FILE_FORMATS");
	}
}
