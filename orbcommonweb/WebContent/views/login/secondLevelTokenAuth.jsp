<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>${ADMIN_CONF['companyName']}</title>	
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
            <meta name="apple-mobile-web-app-capable" content="yes"/>
            
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/primefaces-modena/theme.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/loginfiles/primefaces.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/core-layout.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/animate.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/modena-font.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/ripple-effect.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/font-awesome.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/modena-layout.css"/>
			<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/kurver.css" />
			<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/login.css" />
			
<link rel="apple-touch-icon" type="image/png"  sizes="57x57" href="<%=request.getContextPath() %>/resources/images/apple-icon-57x57.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="60x60" href="<%=request.getContextPath() %>/resources/images/apple-icon-60x60.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="72x72" href="<%=request.getContextPath() %>/resources/images/apple-icon-72x72.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="76x76" href="<%=request.getContextPath() %>/resources/images/apple-icon-76x76.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="114x114" href="<%=request.getContextPath() %>/resources/images/apple-icon-114x114.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="120x120" href="<%=request.getContextPath() %>/resources/images/apple-icon-120x120.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="144x144" href="<%=request.getContextPath() %>/resources/images/apple-icon-144x144.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="152x152" href="<%=request.getContextPath() %>/resources/images/apple-icon-152x152.png"/>
<link rel="apple-touch-icon" type="image/png"  sizes="180x180" href="<%=request.getContextPath() %>/resources/images/apple-icon-180x180.png"/>
<link rel="icon" type="image/png" sizes="192x192"  href="<%=request.getContextPath() %>/resources/images/android-icon-192x192.png"/>
<link rel="icon" type="image/png" sizes="32x32" href="<%=request.getContextPath() %>/resources/images/favicon-32x32.png"/>
<link rel="icon" type="image/png" sizes="96x96" href="<%=request.getContextPath() %>/resources/images/favicon-96x96.png"/>
<link rel="icon" type="image/png" sizes="16x16" href="<%=request.getContextPath() %>/resources/images/favicon-16x16.png"/>
<link rel="manifest" href="<%=request.getContextPath() %>/resources/images/manifest.json"/>
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="<%=request.getContextPath() %>/resources/images/ms-icon-144x144.png" />
<meta name="theme-color" content="#ffffff" />
	
<script>
		var request = ${data};
		setTimeout(function() {
			u2f.sign(request.authenticateRequests,
					function(data) {
		        if(data.errorCode) {
		            alert("U2F failed with error code: " + data.errorCode);
		            return;
		        }
		        document.getElementById('tokenResponse').value = JSON.stringify(data);
		        document.secondAuthAuthForm.submit();
		    });
		}, 1000);
</script>			
    
</head>
<body class="GrayModena Geometry" style="height:auto;" >
<form name='secondAuthAuthForm'  action="<%=request.getContextPath() %>/common/init/secondLevelAuthTokenFinishAuthentication.html" method='GET' >
<div class="Wid90 DispBlock MarAuto MarTopPerc5 TexAlCenter">
            <div class="Wid33 MarAuto WidAutoOnMobile">					
                
                <div class="Card ShadowEffect">
                    
                    <div class="Wid100 OvHidden BigTopic Fs30"><img src="<%=request.getContextPath() %>/images/${ADMIN_CONF['companyLogo']}"/><div class="EmptyBox30"></div>${LOGIN_RES_BUN['hdr_registration'] }</div>
					<div class="msg">Your token has been registered successfully. Please press again the KEY symbol to access your application....</div>
                    <input type="hidden" name="username" value="${username}"/>
            		<input type="hidden" name="tokenResponse" id="tokenResponse"/>
                    
                 </div>
                <div class="Wid100 TexAlCenter MarTop20 Fs14 FontRobotoRegular Gray">
                        ${ADMIN_CONF['version']}
                    </div>
            </div>
        </div>
</form>

<script type="text/javascript" src="<%=request.getContextPath() %>/resources/loginfiles/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/resources/loginfiles/primefaces.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/resources/loginfiles/u2f-api.js"></script>
	
</body>
</html>