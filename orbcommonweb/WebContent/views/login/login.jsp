<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import = "com.orb.common.client.constants.CommonConstants"%>
<%@page language="java" session="true" %>
<%
String contextPath = request.getContextPath();
session.setAttribute(CommonConstants.FORGOT_PASS_LINK, contextPath+"/common/resetpass/resetPass?action=");
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>${ADMIN_CONF['companyName']}</title>	
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
            <meta name="apple-mobile-web-app-capable" content="yes"/>
            
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/primefaces-modena/theme.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/loginfiles/primefaces.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/core-layout.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/animate.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/modena-font.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/ripple-effect.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/font-awesome.css"/>
			<link type="text/css" rel="stylesheet" href="<%=request.getContextPath() %>/resources/modena-layout/css/modena-layout.css"/>
			<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
			<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/kurver.css" />
			<link rel="stylesheet" href="<%=request.getContextPath() %>/resources/css/login.css" />
			
			
<link rel="shortcut icon" type="image/x-icon" href="<%=request.getContextPath() %>/images/${ADMIN_CONF['favIcon']}"/>				
    
</head>
<body class="GrayModena Geometry" style="height:auto;" onload='document.loginForm.username.focus();'>

<form name='loginForm'  action="<%=request.getContextPath() %>/j_spring_security_check" method='POST' accept-charset="ISO-8859-1">
<div class="Wid90 DispBlock MarAuto MarTopPerc5 TexAlCenter">
            <div class="Wid33 MarAuto WidAutoOnMobile">					
                
                <div class="Card ShadowEffect">
                    
                    <div class="Wid100 OvHidden BigTopic Fs30"><img src="<%=request.getContextPath() %>/images/${ADMIN_CONF['companyLogo']}"/><div class="EmptyBox30"></div>${LOGIN_RES_BUN['hdr_login'] }</div>
								<c:if test="${not empty error}">
									<div class="error">${error}</div>
								</c:if>
								<c:if test="${not empty msg}">
									<div class="msg">${msg}</div>
								</c:if>

                    <div class="EmptyBox30"></div>
                    <div class="Wid100"><input placeholder="${LOGIN_RES_BUN['lbl_userName'] }" aria-readonly="false" aria-disabled="false" role="textbox" id="j_idt9" name="username" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all Wid80 TexAlCenter Fs18" type="text"></div>

                    <div class="EmptyBox10"></div>
                    <div class="Wid100"><input placeholder="${LOGIN_RES_BUN['lbl_password'] }" aria-readonly="false" aria-disabled="false" role="textbox" id="j_idt11" name="password" class="ui-inputfield ui-inputtext ui-widget ui-state-default ui-corner-all Wid80 TexAlCenter Fs18" type="password"></div>



                  <!--  <div class="EmptyBox30"></div><div id="j_idt13" class="ui-chkbox ui-widget"><div class="ui-helper-hidden-accessible"><input id="j_idt13_input" value="remember-me" autocomplete="off" aria-checked="false" type="checkbox"></div><div class="ui-chkbox-box ui-widget ui-corner-all ui-state-default"><span class="ui-chkbox-icon ui-icon ui-icon-blank ui-c"></span></div><span class="ui-chkbox-label">${LOGIN_RES_BUN['lbl_rememberMe'] }</span></div> -->


                   <div class="EmptyBox30"></div>
					     
                  
                    <div class="Wid10 Fleft EmptyBox10"></div>
                    <div class="Wid40 Fleft Responsive50">
                    <button aria-disabled="false" role="button" id="j_idt15" name="j_idt15" class="Wid100 heightlogin ui-button ui-widget ui-state-default
                     ui-corner-all ui-button-text-icon-left  Fs18 White RaisedButton Fright FloatNoneOnMobile" type="submit">
					<span class="ui-button-icon-left ui-icon ui-c fa fa-sign-in"></span><span class="ui-button-text ui-c">${LOGIN_RES_BUN['lbl_login'] }</span></button></div>
					<div class="EmptyBox5 ShowOnMobile"></div>
                    <div class="Wid40 Fleft Responsive50">
                    <button aria-disabled="false" role="button" id="j_idt17" name="j_idt17" type="button" 
                    class="Wid100 heightlogin ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left RedButton
                     Fs18 White RaisedButton Fleft FloatNoneOnMobile" 
                    onclick="window.open('<%=request.getContextPath() %>/common/resetpass/forgotPass','_self')">
                    <span class="ui-button-icon-left ui-icon ui-c fa fa-lock"></span><span class="ui-button-text ui-c">${LOGIN_RES_BUN['lbl_forgotPassword'] }</span></button></div> 
                  
                    <c:if test="${ADMIN_CONF['secondLevelAuth'] eq 'GoogleAuth'}">
                     <div class="EmptyBox20"></div>
					 <div class="Wid100 Fleft Responsive100">
                    	 <a href="#" aria-disabled="false" role="button" id="j_idt17" name="j_idt17" type="button" class="ripplelink"
                    	 onclick="window.open('<%=request.getContextPath() %>/common/googleAuth/openGoogleAuth','_self')"><span class="ui-button-text ui-c Red">${LOGIN_RES_BUN['btn_gotoGoogleAuthOpenPage'] }</span></a>
                     </div> 
                    </c:if>
					
                 </div>
                <div class="Wid100 TexAlCenter MarTop20 Fs14 FontRobotoRegular Gray">
                        ${ADMIN_CONF['version']}
                    </div>
            </div>
        </div>
</form>

<script type="text/javascript" src="<%=request.getContextPath() %>/resources/loginfiles/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/resources/loginfiles/primefaces.js"></script>
	
</body>
</html>