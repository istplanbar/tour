//<![CDATA[
    function checkButtons(obj){
    	var val = obj.value;
    	if( val.length == 0 ){
    		document.getElementById('form:deleteButton').disabled = true;
    		document.getElementById('form:addButton').disabled = true;
    	}else{
    		document.getElementById('form:deleteButton').disabled = false;
    		document.getElementById('form:addButton').disabled = false;
    	}
    }
//]]>