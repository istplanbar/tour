function launchIntoFullscreen(element) {
	if(element.requestFullscreen) {
	   element.requestFullscreen();
	} else if(element.mozRequestFullScreen) {
		element.mozRequestFullScreen();
	} else if(element.webkitRequestFullscreen) {
	   element.webkitRequestFullscreen();
	} else if(element.msRequestFullscreen) {
	  $("#showFull").hide();
	  $("#exitFull").show();
	  element.msRequestFullscreen();
	}
	
	$("#showFull").hide();
	$("#exitFull").show();
}


function exitFullscreen() {
	  if(document.exitFullscreen) {
		  document.exitFullscreen();
	  } else if (document.msExitFullscreen) {
          document.msExitFullscreen();
      } else if(document.mozCancelFullScreen) {
		  document.mozCancelFullScreen();
	  } else if(document.webkitExitFullscreen) {
		
		     document.webkitExitFullscreen();
	  }
	  $("#exitFull").hide();
	  $("#showFull").show();
}

jQuery(document).on('keyup',function(fullscreenchange) {
    if (fullscreenchange.keyCode == 27) {
    	 $("#exitFull").hide();
    	 $("#showFull").show();
    }
});

$(window).resize(function(){
	if (window.navigator.standalone || (document.fullScreenElement && document.fullScreenElement !=null) || (document.mozFullScreen || document.msExitFullscreen || document.webkitIsFullScreen) || (!window.screenTop && !window.screenY)){
		
	}else{
		 $("#exitFull").hide();
    	 $("#showFull").show();
	}
	});


	
	/*function resizeText(multiplier) {
		  if (document.getElementsByClassName('ui-widget').style.fontSize == "") {
			  document.getElementsByClassName('.ui-widget, .ui-widget .ui-widget').style.fontSize = "1.0em";
		  }
		  document.getElementsByClassName('.ui-widget, .ui-widget .ui-widget').style.fontSize = parseFloat(document.getElementsByClassName('.ui-widget, .ui-widget .ui-widget').style.fontSize) + (multiplier * 0.2) + "em";
		}
	*/

function resizeText(multiplier) {
	  if (document.body.style.fontSize == "") {
		  document.body.style.fontSize = "1.0em";
	  }
	  document.body.style.fontSize = parseFloat(document.body.style.fontSize) + (multiplier * 0.2) + "em";
	}

