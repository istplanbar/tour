function handleDrop(event, ui) {
    var droppedField = ui.draggable;
    droppedField.fadeOut('fast');
}

function collapseAllPanelMenu(elem){
	$(".ui-panelmenu-panel h3").not($(elem).children('h3')).removeClass("ui-state-active ui-corner-top");
	$(".ui-panelmenu-panel h3").not($(elem).children('h3')).addClass("ui-corner-all");
	$(".ui-panelmenu-panel h3 span").not($(elem).children('h3').children('span')).removeClass("ui-icon-triangle-1-s");
	$(".ui-panelmenu-panel h3 span").not($(elem).children('h3').children('span')).addClass("ui-icon-triangle-1-e");
	$(".ui-panelmenu-panel div").not($(elem).children('div')).slideUp();
	$(".ui-panelmenu-panel div").not($(elem).children('div')).attr("aria-hidden", "true");
}
$( document ).ready(function() {
	var currentSubMenuId= localStorage.currentSubMenuId;
	$("#sidebar .ui-layout-unit-header-title").html($("#sidebar .ui-layout-unit-header-title").html().replace(/^(\w+)/, '$1<br>'))
	$(".ui-panelmenu-panel").each(function() {
		$(this).children('h3').removeClass("ui-state-active ui-corner-top");
		$(this).children('h3').addClass("ui-corner-all");
		$(this).children('h3').children('span').removeClass("ui-icon-triangle-1-s");
		$(this).children('h3').children('span').addClass("ui-icon-triangle-1-e");
		$(this).children('div').hide();
		$(this).children('div').attr("aria-hidden", "true");
		$(this).click(function() {
			collapseAllPanelMenu(this);
		});
	});
	jQuery('div.ui-panelmenu ui-widget ui-widget-header').is(':visible');
    var currentSubMenu = document.getElementById(currentSubMenuId);
    if( currentSubMenu == null ){
    	if( currentSubMenuId.indexOf('sideMenuBar:j_idt74') != -1 )
    		currentSubMenuId = currentSubMenuId.replace('sideMenuBar:j_idt74','sideMenuBar:j_idt75');
    	else
    		currentSubMenuId = currentSubMenuId.replace('sideMenuBar:j_idt75','sideMenuBar:j_idt74');
    	currentSubMenu = document.getElementById(currentSubMenuId);
    }
	currentSubMenu.style.display="block";
    currentSubMenu.parentNode.getElementsByTagName("h3")[0].className="ui-widget ui-panelmenu-header ui-state-default ui-state-active ui-corner-top";
    currentSubMenu.parentNode.getElementsByTagName("h3")[0].getElementsByTagName("span")[0].className="ui-icon ui-icon-triangle-1-s";
});
$(document).on("click", ".ui-menuitem-text", function(e) {
	var elementId; 
	var target = e.target;
	while (target && !target.id) {
	    target = target.parentNode;
	}
	elementId = target.id;
	var tagName = document.getElementById(elementId).tagName;
	if( 'DIV' != tagName ){
		while (true ) {
		    target = target.parentNode;
		    if( target && target.id  ){
			    if('DIV' == document.getElementById(target.id).tagName){
			    	break;
			    }
		    }
		    
		}
	}
	localStorage.currentSubMenuId = target.id;
});
