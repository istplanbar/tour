function isValidDate(fieldValue) 
{ 
    var dateformat = /^(0?[1-9]|1[012])[\/](0?[1-9]|[12][0-9]|3[01])[\/]\d{4}$/; 
    // Match the date format through regular expression 
    if(fieldValue.match(dateformat)) 
    { 
        var pdate = fieldValue.split('/');
       
        var lopera = pdate.length;
        
        var mm = parseInt(pdate[0]); 
        var dd  = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default] 
        var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31]; 
        if (mm==1 || mm>2){ 
            if (dd>ListofDays[mm-1]){ 
                return false; 
            } 
        } 
        if (mm==2){ 
            var lyear = false; 
            if ( (!(yy % 4) && yy % 100) || !(yy % 400)){ 
                lyear = true; 
            } 
            if ((lyear==false) && (dd>=29)){ 
                return false; 
            } 
            if ((lyear==true) && (dd>29)){ 
                return false; 
            } 
        } 
    }else { 
        return false; 
    } 
    return true;
} 