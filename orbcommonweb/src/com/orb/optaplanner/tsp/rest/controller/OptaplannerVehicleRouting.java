package com.orb.optaplanner.tsp.rest.controller;

import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.optaplanner.rest.domain.bean.JsonMessage;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.optaplanner.rest.domain.service.OptaplannerTSPService;


@RestController
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@RequestMapping("/process/tsp")
public class OptaplannerVehicleRouting extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(OptaplannerVehicleRouting.class);
	
	private JsonVehicleRoutingSolution solution = null;
	
	@Autowired @ManagedProperty("#{optaPlannerTSPServiceImpl}")
	OptaplannerTSPService optaPlannerTSPService;
	
	@RequestMapping(value ="/loadSolution",method = RequestMethod.GET, produces = "application/json")
	public JsonVehicleRoutingSolution loadSolution(@RequestParam("tourId") String tourId){
		JsonVehicleRoutingSolution solution = new JsonVehicleRoutingSolution();
		solution = optaPlannerTSPService.loadSolution(commonError, tourId);
		logger.info("Optaplanner Solution is created or retrieved for the Tour: " + tourId);
		return solution;
	}
	
	@RequestMapping(value ="/solution",method = RequestMethod.GET, produces = "application/json")
	public JsonVehicleRoutingSolution getSolution(@RequestParam("tourId") String tourId){
		JsonVehicleRoutingSolution solution = new JsonVehicleRoutingSolution();
		solution = optaPlannerTSPService.getSolution(commonError, tourId);
		logger.info("Optaplanner Solution is created or retrieved for the Tour: " + tourId);
		return solution;
	}
	
	@RequestMapping(value ="/solution/solve",method = RequestMethod.POST, produces = "application/json")
	public JsonMessage solve(){
		JsonMessage jsonMessage = new JsonMessage();
		jsonMessage = optaPlannerTSPService.solve(commonError);
		logger.info("Solve method is called");
		return jsonMessage;
	}
	
}
