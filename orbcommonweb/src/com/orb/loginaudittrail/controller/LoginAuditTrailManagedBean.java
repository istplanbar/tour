package com.orb.loginaudittrail.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.service.AdminService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.common.loginaudittrail.model.LoginAuditTrail;
import com.orb.common.loginaudittrail.service.LoginAuditTrailService;




@ManagedBean(name="systemLogList")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/loginformation")

public class LoginAuditTrailManagedBean  extends AbstractCommonController {
		
	@Autowired 
	private LoginAuditTrailService loginAuditTrailService;
	
	@Autowired 
	protected AdminService adminService;
	

	private static Log logger = LogFactory.getLog(LoginAuditTrailManagedBean.class);
	private List<LoginAuditTrail> loginAuditTrailList = null;
	private List<LoginAuditTrail> filteredLoginAuditTrailList = null;
	private List<LoginAuditTrail> selectedLoginAuditTrailList = null;
	private boolean ipAddressLogBool = true;
	private User loginUser=null;
	
	@RequestMapping("/loadSystemlog")
	public String loadSystemlog(ModelMap model){
		return CommonConstants.LOGIN_AUDIT_TRAIL_PAGE;
	}
	
	
 	public void onLoadAction(){
 		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
 		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
 		loginAuditTrailList = loginAuditTrailService.getLoginAuditTrailList(commonError);
 		if("2".equals(adminConfMap.get("ipAddressLog"))){
 			for(LoginAuditTrail loginAuditTrail:loginAuditTrailList){
 				if(!CommonUtils.getValidString(loginAuditTrail.getIpAddress()).isEmpty()){
 					//String listOfWords = "This is a sentence";
 			        int index= loginAuditTrail.getIpAddress().lastIndexOf(".");
 			        loginAuditTrail.setIpAddress(loginAuditTrail.getIpAddress().substring(0, index)+".*");
	 			  /*String finalString= loginAuditTrail.getIpAddress().substring(loginAuditTrail.getIpAddress().lastIndexOf("."));
	 			  logger.info(loginAuditTrail.getIpAddress());*/
 			      //String[] ipAddress = loginAuditTrail.getIpAddress().split(".");
 	 		  }
 			}
 			
 		}else if("3".equals(adminConfMap.get("ipAddressLog"))){
 			ipAddressLogBool = false;
 		}
 		
	}	
 	
 	public String getDateFormat(String date){
 		SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		myFormat.setTimeZone(TimeZone.getTimeZone(loginUser.getTimeZone()));
		String reformattedStr = null;
		try {
			Date myDate = fromUser.parse(date);
			reformattedStr = myFormat.format(myDate);
		} catch (ParseException e) {
		   logger.info(e.getMessage());
		}
		return reformattedStr;
 		
	}
	
	public static Log getLogger() {
		return logger;
	}


	public List<LoginAuditTrail> getFilteredLoginAuditTrailList() {
		return filteredLoginAuditTrailList;
	}

	public void setFilteredLoginAuditTrailList(List<LoginAuditTrail> filteredLoginAuditTrailList) {
		this.filteredLoginAuditTrailList = filteredLoginAuditTrailList;
	}

	public List<LoginAuditTrail> getLoginAuditTrailList() {
		return loginAuditTrailList;
	}


	public void setLoginAuditTrailList(List<LoginAuditTrail> loginAuditTrailList) {
		this.loginAuditTrailList = loginAuditTrailList;
	}
	
	public LoginAuditTrailService getLoginAuditTrailService() {
		return loginAuditTrailService;
	}


	public void setLoginAuditTrailService(LoginAuditTrailService loginAuditTrailService) {
		this.loginAuditTrailService = loginAuditTrailService;
	}


	public List<LoginAuditTrail> getSelectedLoginAuditTrailList() {
		return selectedLoginAuditTrailList;
	}


	public void setSelectedLoginAuditTrailList(List<LoginAuditTrail> selectedLoginAuditTrailList) {
		this.selectedLoginAuditTrailList = selectedLoginAuditTrailList;
	}


	public boolean isIpAddressLogBool() {
		return ipAddressLogBool;
	}


	public void setIpAddressLogBool(boolean ipAddressLogBool) {
		this.ipAddressLogBool = ipAddressLogBool;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	
	
	}