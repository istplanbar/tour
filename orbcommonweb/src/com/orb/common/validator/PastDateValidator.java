package com.orb.common.validator;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.factory.AbstractCommonFactory;

@FacesValidator("pastDateValidator")
public class PastDateValidator extends AbstractCommonFactory implements Validator{

	@Override  
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	    Date date = (java.util.Date) value;
	    ReloadableResourceBundleMessageSource bundle = context.getApplication().evaluateExpressionGet(context, "#{msg}", ReloadableResourceBundleMessageSource.class);
	    if (date.before(new Date())) {
	    	errorMessage(bundle, CommonConstants.PAST_DATE_ERROR);
	        throw new ValidatorException(new FacesMessage("Please enter future date...")); // IST planbar / PaPe: No need to move to constant..
	    } 
	}
	}   
	