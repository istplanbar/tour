package com.orb.common.theme;


public class Message {
	
	private String from;
	private String subject;
	private String time;
	
	public Message(String from, String time, String subject) {
		this.from = from;
		this.setTime(time);
		this.subject = subject;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}

}
