package com.orb.common.converter	;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;

@FacesConverter(value = "dateConverter")
@ApplicationScoped
public class DateConverter extends AbstractCommonController implements Converter {

  private SimpleDateFormat appDateConvertor; 

  public DateConverter() {
    appDateConvertor = new SimpleDateFormat(CommonConstants.APP_DATE_FORMAT);
    appDateConvertor.setLenient(false); 
  }

  @Override
  public Object getAsObject(FacesContext fc, UIComponent component, String value) throws ConverterException {

   if( value==null || value.isEmpty()) {
      return new Date();
    }

    Date result = null;

    try {
    	result = (Date)appDateConvertor.parseObject(value); 
    }catch (ParseException e) {
    	 FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"",getJSFMessage("invalid_date"));
    	  throw new ConverterException(message);
    }

    return result;
  }

  @Override
  public String getAsString(FacesContext fc, UIComponent component, Object date) throws     ConverterException {
    try {
      return appDateConvertor.format((Date)date);
    }
    catch (NullPointerException e) {
      return null;
    }
  }

}