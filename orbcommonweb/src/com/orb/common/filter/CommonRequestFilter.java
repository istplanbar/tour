package com.orb.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class CommonRequestFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = ((HttpServletRequest) request);
		//HttpServletResponse httpServletResponse = ((HttpServletResponse) response);
		HttpSession session = httpServletRequest.getSession();
		Cookie[] cookies = httpServletRequest.getCookies();
		String requestURI = httpServletRequest.getRequestURI();
		if ((requestURI.endsWith(".xhtml") && session.isNew()) || cookies == null) {
			httpServletRequest.setAttribute("SESSION_TIMED_OUT", "TRUE");
        }
		chain.doFilter(request, response);
		if (cookies != null) {
			 for (Cookie cookie : cookies) {
			   if (cookie.getName().equals("JSESSIONID")) {
				   cookie.setMaxAge(3600);
				   break;
			    }
			  }
			}
		
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO Auto-generated method stub
		
	}


}
