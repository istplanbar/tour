package com.orb.messages.controller;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.service.CommonService;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.constants.CrmConstants;
import com.orb.messages.bean.MessageResource;
import com.orb.messages.constants.MessagesConstants;
import com.orb.messages.dao.ExcelUploadDAO;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


@ManagedBean(name="messagesListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/messages")
public class MessagesManagedBean extends AbstractCommonController{
	
	private static Log logger=LogFactory.getLog(MessagesManagedBean.class);
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
	@Autowired 
	private ExcelUploadDAO excelUploadDAO;
	
	@Autowired 
	protected CommonService commonService;
	
	private List<MetaTable> mainModuleList = null;
	private String parentPage=null;
	private List<MetaTable> subModuleList = null;
	private Map<String, String> messageResourceMap = null;
	private User loginUser = null;
    private boolean disable = false;
    private List<MessageResource> messageResourceList = null;
    private List<MessageResource> selectedMsgResourceList = null;
    private List<MessageResource> filteredMsgResourceList = null;
    private List<Boolean> togglerList = null;
	
	/**
	 * This Method will be called to view the messages 
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMessages")
	public String loadMessages(HttpServletRequest httpRequest, ModelMap model){
		clearSessionObjects(model);
	    return MessagesConstants.MESSAGES_LIST_HTML; 
	}
		
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		togglerList = (List<Boolean>)getSessionObj(MessagesConstants.MESSAGES_TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList( false,true,true,true );
		}
		mainModuleList = moduleManagementService.getParentPageList(commonError);
	}
		
	/**
	 * This method will add the messages in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		commonService.addMessageResource(commonError,loginUser,messageResourceMap);
		customRedirector( MessagesConstants.MESSAGE_ADDED_SUCCESS, MessagesConstants.MESSAGES_LIST_PAGE);
	}
		
	/**
	 * This method will get the messages List
	 */
	public void getMsgResourceList(){
		disable = true;
		logger.info("Parent Page Id: "+parentPage);
		if(messageResourceMap == null) getMessageResourceMap(); 
		messageResourceMap.put(MessagesConstants.PARENT_PAGE,parentPage );
		messageResourceList = commonService.getMsgResourceList(commonError,parentPage);
	}
	
	/**
	 * This method will update the messages
	 * @param messageResource
	 */
	public void updateMessageResource(MessageResource messageResource){
		commonService.updateMessageResource(commonError,loginUser,messageResource);
		//customRedirector( MessagesConstants.MESSAGE_UPDATED_SUCCESS, MessagesConstants.MESSAGES_LIST_PAGE);
	}
	
	/**
	 * This method will delete the messages
	 * @param event
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedMsgResourceList.isEmpty()){
		   commonService.deleteMessageResource( commonError, loginUser, selectedMsgResourceList );
		   customRedirector( MessagesConstants.MESSAGE_DELETE_SUCCESS, MessagesConstants.MESSAGES_LIST_PAGE);
		}
	}
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 */
	public void onToggle(ToggleEvent event) {
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * To clear the session variables
	 * @param model
	 */	
	@SuppressWarnings("rawtypes")
	private void clearSessionObjects( ModelMap model ){
		List toggleList = (List)model.get( togglerList );
		if( toggleList != null ) model.remove(MessagesConstants.MESSAGES_TOGGLER_LIST);
	}
	
	public String getParentPage() {
		return parentPage;
	}
	
	/* public void handleFileUpload(FileUploadEvent event) {
	        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
	        FacesContext.getCurrentInstance().addMessage(null, message);
	    }*/
	 /**
		 * This method process the Excel upload
		 * @param fileUploadEvent
		 * @throws IOException
		 */
		public void handleFileUpload(FileUploadEvent fileUploadEvent) throws IOException{
			
			try{
				UploadedFile file = fileUploadEvent.getFile();
				String filePath = null;	
				byte[] bytes = null;
				
			 if (file!=null) {
				 
				 filePath = System.getProperty("config.dir") + "moduleUploads\\translations\\" ;
				 bytes = file.getContents();
				 String filename = FilenameUtils.getName(file.getFileName());
				 BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
				 stream.write(bytes);
	             stream.close();
				 excelUploadDAO.addTranslationsFromExcel(filePath+filename,loginUser, parentPage);	
				 
			     customRedirector( MessagesConstants.EXCEL_ADDED_SUCCESS, MessagesConstants.MESSAGES_LIST_PAGE);
	            
			 }
			
	         }catch(Exception e){
				 logger.info("Error uploading file" +e);
			 }
		}
	
	public void setParentPage(String parentPage) {
		this.parentPage = parentPage;
	}


	public List<MetaTable> getSubModuleList() {
		return subModuleList;
	}


	public void setSubModuleList(List<MetaTable> subModuleList) {
		this.subModuleList = subModuleList;
	}

	public List<MetaTable> getMainModuleList() {
		return mainModuleList;
	}


	public void setMainModuleList(List<MetaTable> mainModuleList) {
		this.mainModuleList = mainModuleList;
	}


	public Map<String, String> getMessageResourceMap() {
		if(messageResourceMap == null){
			messageResourceMap = new HashMap<String, String>();
			messageResourceMap.put(MessagesConstants.KEY, "");
			messageResourceMap.put(MessagesConstants.GERMAN, "");
			messageResourceMap.put(MessagesConstants.ENGLISH, "");
		}
		return messageResourceMap;
	}


	public void setMessageResourceMap(Map<String, String> messageResourceMap) {
		this.messageResourceMap = messageResourceMap;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public boolean isDisable() {
		return disable;
	}


	public void setDisable(boolean disable) {
		this.disable = disable;
	}


	public List<MessageResource> getMessageResourceList() {
		return messageResourceList;
	}


	public void setMessageResourceList(List<MessageResource> messageResourceList) {
		this.messageResourceList = messageResourceList;
	}


	public List<MessageResource> getSelectedMsgResourceList() {
		return selectedMsgResourceList;
	}


	public void setSelectedMsgResourceList(List<MessageResource> selectedMsgResourceList) {
		this.selectedMsgResourceList = selectedMsgResourceList;
	}


	public List<MessageResource> getFilteredMsgResourceList() {
		return filteredMsgResourceList;
	}


	public void setFilteredMsgResourceList(List<MessageResource> filteredMsgResourceList) {
		this.filteredMsgResourceList = filteredMsgResourceList;
	}
    
	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	
}
}
