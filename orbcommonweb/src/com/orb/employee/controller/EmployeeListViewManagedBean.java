package com.orb.employee.controller;
/**
 * @author IST planbar / NaKa
 * 
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.qualifications.bean.Qualifications;


@ManagedBean(name="employeeView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class EmployeeListViewManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(EmployeeListViewManagedBean.class);
	
	private User loginUser=null;
    private List<Boolean> togglerList = null;
    private Employee employee = null;
    private List<Employee> employeeList = null;
	private List<Employee> filteredEmployeeList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
    private String showActivateBtn = CommonConstants.FALSE;
	private List<Employee> selectedEmployeeList = null;
	private String employeeId = null;
	private List<Qualifications> qualificationsList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private List<Vacation> vacationList = null;
	private List<Vacation> empVacationList = null;
		 
    /**
	 * This page will be called when user clicks the employee link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadEmployee")
    public String loadEmployee(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return EmployeeConstants.EMPLOYEE_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular employee from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewEmployee")
	public String viewEmployee(HttpServletRequest request, ModelMap model){
		 employeeId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( EmployeeConstants.SELECTED_EMPLOYEE_ID, employeeId );
		 return EmployeeConstants.EMPLOYEE_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		employeeList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		employeeId = CommonUtils.getValidString(getJSFRequestAttribute(EmployeeConstants.SELECTED_EMPLOYEE_ID) );
		logger.info(" EmployeeId :" +employeeId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		qualificationsList = qualificationsService.getActiveQualificationsList(commonError);
		vacationList=employeeService.getActiveVacationList(commonError);
		if(qualificationsList == null) qualificationsList = new ArrayList<>();
		if(CommonConstants.N.equals(showActive)){
			employeeList=employeeService.getInactiveEmployeeList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			employeeList=employeeService.getActiveEmployeeList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(EmployeeConstants.EMPLOYEE_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true,true,true,true,true);
		}
		if(!CommonUtils.getValidString(employeeId).isEmpty()){
			employee = employeeService.getEmployee(commonError, employeeId);
			empVacationList = vacationList.stream().filter(vac -> vac.getEmployee().contains(employee.getIdentifier())).collect(Collectors.toList());
		}
	}
	
	/**
	 * This method will get all the active employee
	 * @param event
	 */
	public void showActive(ActionEvent event){
		employeeList = employeeService.getActiveEmployeeList( commonError );
		if( filteredEmployeeList != null ) filteredEmployeeList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	public String getQualificationName(String id){
		String tempName = null;
		if(id!=null){
		  List<String> tempQualificationList = qualificationsList.stream().filter(q -> Arrays.asList(id.split(",")).contains(q.getId())).map(n -> n.getQualificationName()).collect(Collectors.toList());
		  tempName = tempQualificationList.stream().map(x -> x).collect(Collectors.joining(", "));
		}
		return tempName;
	} 
	
	/**
	 * This method will show all the inactive Employee
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		employeeList = employeeService.getInactiveEmployeeList(commonError);
		if( filteredEmployeeList != null ) filteredEmployeeList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks employee
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		employeeService.lockEmployee( commonError, loginUser, selectedEmployeeList );
		showActive(event);
		customRedirector( EmployeeConstants.EMPLOYEE_LOCKED_SUCCESS,EmployeeConstants.EMPLOYEE_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Employee
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		employeeService.unlockEmployee( commonError, loginUser, selectedEmployeeList );
		showInactive(event);
		customRedirector( EmployeeConstants.EMPLOYEE_UNLOCKED_SUCCESS, EmployeeConstants.EMPLOYEE_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the Employee
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(selectedEmployeeList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(EmployeeConstants.EMPLOYEE_DELETE_SUCCESS);
		}else{	
			employeeService.deleteEmployee( commonError, loginUser, selectedEmployeeList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( EmployeeConstants.EMPLOYEE_DELETE_SUCCESS, EmployeeConstants.EMPLOYEE_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will navigate to the page where user can add a role
	 */
	public void addAction(){
		redirector(EmployeeConstants.EMPLOYEE_EDIT_PAGE);
	}
	/**
	 * This method to returns to main employee list page
	 */
	public void cancelAction(){
		redirector(EmployeeConstants.EMPLOYEE_LIST_PAGE +"?activeFlag="+employee.getActive());
	}
	
	/**
	 * This method will navigate to employee Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(EmployeeConstants.EMPLOYEE_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to employee View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( EmployeeConstants.EMPLOYEE_TOGGLER_LIST , togglerList );
		redirector(EmployeeConstants.EMPLOYEE_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed employee
	 * @param index
	 */
	public void deleteViewedEmployee(String index){
		employee = employeeService.getEmployee(commonError, index);
		employeeService.deleteViewedEmployee( commonError, loginUser, employee);
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.EMPLOYEE_DELETE_SUCCESS, EmployeeConstants.EMPLOYEE_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed employee
	 */
	public void lockViewedEmployee(String index){
		employee = employeeService.getEmployee(commonError, index);
		employeeService.lockViewedEmployee( commonError, loginUser, employee );
		customRedirector( EmployeeConstants.EMPLOYEE_LOCKED_SUCCESS, EmployeeConstants.EMPLOYEE_LIST_PAGE);
	}

	/**
	 * This method locks viewed employee
	 * @param index
	 * @return
	 */
	public void unlockViewedEmployee(String index){
		employee = employeeService.getEmployee(commonError, index);
		employeeService.unlockViewedEmployee( commonError, loginUser, employee );
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.EMPLOYEE_UNLOCKED_SUCCESS, EmployeeConstants.EMPLOYEE_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( EmployeeConstants.SELECTED_EMPLOYEE_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( EmployeeConstants.SELECTED_EMPLOYEE_ID );
		if( toggleList != null ) model.remove( EmployeeConstants.EMPLOYEE_TOGGLER_LIST );		
    }


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public List<Employee> getEmployeeList() {
		return employeeList;
	}


	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}


	public List<Employee> getFilteredEmployeeList() {
		return filteredEmployeeList;
	}


	public void setFilteredEmployeeList(List<Employee> filteredEmployeeList) {
		this.filteredEmployeeList = filteredEmployeeList;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<Employee> getSelectedEmployeeList() {
		return selectedEmployeeList;
	}


	public void setSelectedEmployeeList(List<Employee> selectedEmployeeList) {
		this.selectedEmployeeList = selectedEmployeeList;
	}


	public String getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}


	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}


	public List<Vacation> getVacationList() {
		return vacationList;
	}


	public void setVacationList(List<Vacation> vacationList) {
		this.vacationList = vacationList;
	}


	public List<Vacation> getEmpVacationList() {
		return empVacationList;
	}


	public void setEmpVacationList(List<Vacation> empVacationList) {
		this.empVacationList = empVacationList;
	}
}
