package com.orb.employee.controller;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * @author IST planbar / NaKa
 * 
 */
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.qualifications.bean.Qualifications;

@ManagedBean(name="employeeAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class EmployeeAddUpdateManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(EmployeeAddUpdateManagedBean.class);
	  
	private Employee employee =null;
	private List<Employee> employeeList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String employeeId = null;
	private User loginUser = null;
	private List<User> userList = null;
	private List<Qualifications> qualificationsList = null;
	private List<String> qualificationsNameList = null;
	
	/**
	 * This method will be called when user clicks Add button / employee Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateEmployee")
	public String manipulateEmployee( HttpServletRequest request, ModelMap model){
		employeeId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( EmployeeConstants.SELECTED_EMPLOYEE_ID, employeeId );
		return EmployeeConstants.EMPLOYEE_EDIT_HTML;
	}	

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		employeeList = new ArrayList<>();
		qualificationsNameList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		employeeId = getJSFRequestAttribute( EmployeeConstants.SELECTED_EMPLOYEE_ID );
		logger.info(" Employee Id :" +employeeId);
		userList = getAllUsersList();
		qualificationsList = qualificationsService.getActiveQualificationsList(commonError);
		employeeList=employeeService.getActiveEmployeeList(commonError);
		if(employeeList.isEmpty()) employeeList = new ArrayList<>();
		
		if(CommonUtils.getValidString(employeeId).isEmpty()){
			employee = new Employee();
			showIdentifier=CommonConstants.FALSE;
			if(userList != null){
				for(Employee e: employeeList){
					userList = userList.stream().filter(u -> !u.getUserId().equals(e.getFirstName()))
												.collect(Collectors.toList());
				}
			}
		}else{
			employee=employeeService.getEmployee(commonError, employeeId);
			if(userList != null){
				for(Employee e: employeeList){
					userList = userList.stream().filter(u -> !u.getUserId().equals(e.getFirstName()) || u.getUserId().equals(employee.getFirstName()))
												.collect(Collectors.toList());
				}
			}
			if(!CommonUtils.getValidString(employee.getQualificationsMaId()).isEmpty()){
				  qualificationsNameList = Arrays.asList(employee.getQualificationsMaId().split(","));
			}
		 }
	}

	/**
	 * This method will add / update a employee in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(!qualificationsNameList.isEmpty()){
			String qualificationName = qualificationsNameList.stream().map(n -> n).collect(Collectors.joining(","));
			employee.setQualificationsMaId(qualificationName);
		}
		if(CommonUtils.getValidString(employee.getId()).isEmpty()){
			employeeService.addEmployee( commonError, loginUser, employee );
		}else{
			employeeService.updateEmployee( commonError, loginUser, employee );
		}
		
		String msg=CommonUtils.getValidString(employee.getId()).isEmpty() ? EmployeeConstants.EMPLOYEE_ADDED_SUCCESS : EmployeeConstants.EMPLOYEE_UPDATED_SUCCESS ;
		customRedirector( msg, EmployeeConstants.EMPLOYEE_LIST_PAGE);		
	}


	/**
	 * This method to returns to employee view page
	 * @return 
	 */
	public void cancelAction(){
		if(employee.getId()==null){
			redirector(EmployeeConstants.EMPLOYEE_LIST_PAGE);
		}else{
			redirector(EmployeeConstants.EMPLOYEE_VIEW_PAGE+"?objectId="+employee.getIdentifier());
		}
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}

	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}
	
	public List<String> getQualificationsNameList() {
		return qualificationsNameList;
	}

	public void setQualificationsNameList(List<String> qualificationsNameList) {
		this.qualificationsNameList = qualificationsNameList;
	}
}
