package com.orb.employee.controller;
/**
 * @author IST planbar / NaKa
 * 
 */
import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.employee.service.EmployeeService;
import com.orb.qualifications.service.QualificationsService;

public abstract class AbstractEmployeeController extends AbstractCommonController {
	@Autowired
	protected EmployeeService employeeService;
	
	
	@Autowired
	protected QualificationsService qualificationsService;

}
