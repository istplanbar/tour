package com.orb.employee.vacation.controller;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.employee.controller.AbstractEmployeeController;


@ManagedBean(name="vacationView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class VacationListViewManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(VacationListViewManagedBean.class);
	
	private User loginUser=null;
    private List<Boolean> togglerList = null;
    private Vacation vacation = null;
    private List<Vacation> vacationList = null;
	private List<Vacation> filteredVacationList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
    private String showActivateBtn = CommonConstants.FALSE;
	private List<Vacation> selectedVacationList = null;
	private String vacationId = null;
	private List<Employee> employeeList = null;
		 
    /**
	 * This page will be called when user clicks the employee link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadVacation")
    public String loadVacation(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return EmployeeConstants.VACATION_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular vacation from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewVacation")
	public String viewVacation(HttpServletRequest request, ModelMap model){
		 vacationId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( EmployeeConstants.SELECTED_VACATION_ID, vacationId );
		 return EmployeeConstants.VACATION_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		vacationId = CommonUtils.getValidString(getJSFRequestAttribute(EmployeeConstants.SELECTED_VACATION_ID) );
		logger.info(" Vacation Id :" +vacationId);
		employeeList=employeeService.getActiveEmployeeList(commonError);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			vacationList=employeeService.getInactiveVacationList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			vacationList=employeeService.getActiveVacationList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(EmployeeConstants.VACATION_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false,true,true,true,true,true);
		}
		if(!vacationId.isEmpty()){
			vacation = employeeService.getVacation(commonError, vacationId);
		}
	}
	
	/**
	 * This method will get all the active vacation
	 * @param event
	 */
	public void showActive(ActionEvent event){
		vacationList = employeeService.getActiveVacationList( commonError );
		if( filteredVacationList != null ) filteredVacationList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Vacation
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		vacationList = employeeService.getInactiveVacationList( commonError );
		if( filteredVacationList != null ) filteredVacationList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks vacation
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		if(!selectedVacationList.isEmpty()){
			employeeService.lockVacation( commonError, loginUser, selectedVacationList );
			showActive(event);
			customRedirector( EmployeeConstants.VACATION_LOCKED_SUCCESS,EmployeeConstants.VACATION_LIST_PAGE);
		}
	}
	
	/**
	 * This method unlocks Vacation
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		if(!selectedVacationList.isEmpty()){
			employeeService.unlockVacation( commonError, loginUser, selectedVacationList );
			showInactive(event);
			customRedirector( EmployeeConstants.VACATION_UNLOCKED_SUCCESS, EmployeeConstants.VACATION_LIST_PAGE);
		}
	}
	
	
	/**
	 * This method will delete the Vacation
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(!selectedVacationList.isEmpty()){
			employeeService.deleteVacation( commonError, loginUser, selectedVacationList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( EmployeeConstants.VACATION_DELETE_SUCCESS, EmployeeConstants.VACATION_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
		}
	}
	
	/**
	 * This method will navigate to the page where user can add a role
	 */
	public void addAction(){
		redirector(EmployeeConstants.VACATION_EDIT_PAGE);
	}
	/**
	 * This method to returns to main vacation list page
	 */
	public void cancelAction(){
		redirector(EmployeeConstants.VACATION_LIST_PAGE +"?activeFlag="+vacation.getActive());
	}
	
	/**
	 * This method will navigate to vacation Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(EmployeeConstants.VACATION_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to vacation View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( EmployeeConstants.VACATION_TOGGLER_LIST , togglerList );
		redirector(EmployeeConstants.VACATION_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed vacation
	 * @param index
	 */
	public void deleteViewedVacation(String index){
		vacation = employeeService.getVacation(commonError, index);
		employeeService.deleteViewedVacation( commonError, loginUser, vacation);
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.VACATION_DELETE_SUCCESS, EmployeeConstants.VACATION_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed vacation
	 */
	public void lockViewedVacation(String index){
		vacation = employeeService.getVacation(commonError, index);
		employeeService.lockViewedVacation( commonError, loginUser, vacation );
		customRedirector( EmployeeConstants.VACATION_LOCKED_SUCCESS, EmployeeConstants.VACATION_LIST_PAGE);
	}

	/**
	 * This method locks viewed vacation
	 * @param index
	 * @return
	 */
	public void unlockViewedVacation(String index){
		vacation = employeeService.getVacation(commonError, index);
		employeeService.unlockViewedVacation( commonError, loginUser, vacation );
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.VACATION_UNLOCKED_SUCCESS, EmployeeConstants.VACATION_LIST_PAGE );
		}
	}
	
	/**
	 * This method will get the employee name from DB
	 * @param empIdentifier
	 */
	public String getEmployeeName(String empIdentifier){
		String empName = null;
		if(empIdentifier!=null){
		  List<String> empNameList = employeeList.stream().filter(e -> Arrays.asList(empIdentifier.split(",")).contains(e.getIdentifier())).map(n -> getUserFullName(n.getFirstName())).collect(Collectors.toList());
		  empName = empNameList.stream().map(x -> x).collect(Collectors.joining(", "));
		}
		return empName;
	} 
	
	/**
	 * This method will get application date format from DB
	 * @param createDate
	 */
	public String getAppDateFormat(Date createDate){
		SimpleDateFormat sdf = new SimpleDateFormat(CommonConstants.APP_DATE_FORMAT);
		String dateTime = null;
		if(createDate != null){
		   dateTime = sdf.format(createDate);
		}
		return dateTime;
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( EmployeeConstants.SELECTED_VACATION_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( EmployeeConstants.SELECTED_VACATION_ID );
		if( toggleList != null ) model.remove( EmployeeConstants.VACATION_TOGGLER_LIST );		
    }


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}




	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public Vacation getVacation() {
		return vacation;
	}


	public void setVacation(Vacation vacation) {
		this.vacation = vacation;
	}


	public List<Vacation> getFilteredVacationList() {
		return filteredVacationList;
	}


	public void setFilteredVacationList(List<Vacation> filteredVacationList) {
		this.filteredVacationList = filteredVacationList;
	}


	public List<Vacation> getSelectedVacationList() {
		return selectedVacationList;
	}


	public void setSelectedVacationList(List<Vacation> selectedVacationList) {
		this.selectedVacationList = selectedVacationList;
	}


	public List<Vacation> getVacationList() {
		return vacationList;
	}

	public void setVacationList(List<Vacation> vacationList) {
		this.vacationList = vacationList;
	}

	public String getVacationId() {
		return vacationId;
	}
	public void setVacationId(String vacationId) {
		this.vacationId = vacationId;
	}


	public List<Employee> getEmployeeList() {
		return employeeList;
	}


	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}
	
	
}
