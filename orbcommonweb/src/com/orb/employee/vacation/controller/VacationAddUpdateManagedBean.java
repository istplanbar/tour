package com.orb.employee.vacation.controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.employee.controller.AbstractEmployeeController;

@ManagedBean(name="vacationAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class VacationAddUpdateManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(VacationAddUpdateManagedBean.class);
	  
	private Vacation vacation =null;
	private List<Vacation> vacationList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String vacationId = null;
	private User loginUser = null;
	private List<Employee> employeeList = null;
	private List<String> vacationEmpList = null;
	
	/**
	 * This method will be called when user clicks Add button / vacation Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateVacation")
	public String manipulateVacation( HttpServletRequest request, ModelMap model){
		vacationId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( EmployeeConstants.SELECTED_VACATION_ID, vacationId );
		return EmployeeConstants.VACATION_EDIT_HTML;
	}	

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		vacationId = getJSFRequestAttribute( EmployeeConstants.SELECTED_VACATION_ID );
		logger.info(" Vacation Id :" +vacationId);
		employeeList=employeeService.getActiveEmployeeList(commonError);
		if(CommonUtils.getValidString(vacationId).isEmpty()){
			vacation = new Vacation();
			showIdentifier=CommonConstants.FALSE;
		}else{
			vacation=employeeService.getVacation(commonError, vacationId);
			if(!CommonUtils.getValidString(vacation.getEmployee()).isEmpty()){
				vacationEmpList = Arrays.asList(vacation.getEmployee().split(","));
			}
		 }
	}

	/**
	 * This method will add / update a vacation in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(vacationEmpList != null){
			String employee = vacationEmpList.stream().map(v -> v).collect(Collectors.joining(","));
			vacation.setEmployee(employee);
		}
		if(CommonUtils.getValidString(vacation.getId()).isEmpty()){
			employeeService.addVacation( commonError, loginUser, vacation );
		}else{
			employeeService.updateVacation( commonError, loginUser, vacation );
		}
		String msg=CommonUtils.getValidString(vacation.getId()).isEmpty() ? EmployeeConstants.VACATION_ADDED_SUCCESS : EmployeeConstants.VACATION_UPDATED_SUCCESS ;
		customRedirector( msg, EmployeeConstants.VACATION_LIST_PAGE);		
	}


	/**
	 * This method to returns to vacation view page
	 * @return 
	 */
	public void cancelAction(){
		if(vacation.getId()==null){
			redirector(EmployeeConstants.VACATION_LIST_PAGE);
		}else{
			redirector(EmployeeConstants.VACATION_VIEW_PAGE+"?objectId="+vacation.getIdentifier());
		}
	}
	

	public Vacation getVacation() {
		return vacation;
	}

	public void setVacation(Vacation vacation) {
		this.vacation = vacation;
	}

	public List<Vacation> getVacationList() {
		return vacationList;
	}

	public void setVacationList(List<Vacation> vacationList) {
		this.vacationList = vacationList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public String getVacationId() {
		return vacationId;
	}

	public void setVacationId(String vacationId) {
		this.vacationId = vacationId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<String> getVacationEmpList() {
		return vacationEmpList;
	}

	public void setVacationEmpList(List<String> vacationEmpList) {
		this.vacationEmpList = vacationEmpList;
	}


}
