package com.orb.employee.transport.controller;
/**
 * @author IST planbar / NaKa
 * 
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Transport;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.employee.controller.AbstractEmployeeController;


@ManagedBean(name="transportView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class TransportListViewManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(TransportListViewManagedBean.class);
	
	private User loginUser=null;
    private List<Boolean> togglerList = null;
    private Transport transport = null;
    private List<Transport> transportList = null;
	private List<Transport> filteredTransportList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
    private String showActivateBtn = CommonConstants.FALSE;
	private List<Transport> selectedTransportList = null;
	private String transportId = null;
	private String showIdentifier = CommonConstants.TRUE;
		 
    /**
	 * This page will be called when user clicks the transport link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadTransport")
    public String loadTransport(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return EmployeeConstants.TRANSPORT_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular transport from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewTransport")
	public String viewTransport(HttpServletRequest request, ModelMap model){
		 transportId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( EmployeeConstants.SELECTED_TRANSPORT_ID, transportId );
		 return EmployeeConstants.TRANSPORT_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		transportList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		transportId = CommonUtils.getValidString(getJSFRequestAttribute(EmployeeConstants.SELECTED_TRANSPORT_ID) );
		logger.info(" TransportId :" +transportId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			transportList=employeeService.getInactiveTransportList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			transportList=employeeService.getActiveTransportList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(EmployeeConstants.TRANSPORT_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true);
		}
		if(!CommonUtils.getValidString(transportId).isEmpty()){
			transport = employeeService.getTransport(commonError, transportId);
		}
	}
	
	/**
	 * This method will get all the active transport
	 * @param event
	 */
	public void showActive(ActionEvent event){
		transportList = employeeService.getActiveTransportList( commonError );
		if( filteredTransportList != null ) filteredTransportList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Transport
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		transportList = employeeService.getInactiveTransportList(commonError);
		if( filteredTransportList != null ) filteredTransportList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks transport
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		employeeService.lockTransport( commonError, loginUser, selectedTransportList );
		showActive(event);
		customRedirector( EmployeeConstants.TRANSPORT_LOCKED_SUCCESS,EmployeeConstants.TRANSPORT_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Transport
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		employeeService.unlockTransport( commonError, loginUser, selectedTransportList );
		showInactive(event);
		customRedirector( EmployeeConstants.TRANSPORT_UNLOCKED_SUCCESS, EmployeeConstants.TRANSPORT_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the Transport
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(selectedTransportList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(EmployeeConstants.TRANSPORT_DELETE_SUCCESS);
		}else{	
			employeeService.deleteTransport( commonError, loginUser, selectedTransportList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( EmployeeConstants.TRANSPORT_DELETE_SUCCESS, EmployeeConstants.TRANSPORT_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will navigate to the page where user can add a role
	 */
	public void addAction(){
		redirector(EmployeeConstants.TRANSPORT_EDIT_PAGE);
	}
	/**
	 * This method to returns to main transport list page
	 */
	public void cancelAction(){
		redirector(EmployeeConstants.TRANSPORT_LIST_PAGE +"?activeFlag="+transport.getActive());
	}
	
	/**
	 * This method will navigate to transport Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(EmployeeConstants.TRANSPORT_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to transport View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( EmployeeConstants.TRANSPORT_TOGGLER_LIST , togglerList );
		redirector(EmployeeConstants.TRANSPORT_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed transport
	 * @param index
	 */
	public void deleteViewedTransport(String index){
		transport = employeeService.getTransport(commonError, index);
		employeeService.deleteViewedTransport( commonError, loginUser, transport);
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.TRANSPORT_DELETE_SUCCESS, EmployeeConstants.TRANSPORT_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed transport
	 */
	public void lockViewedTransport(String index){
		transport = employeeService.getTransport(commonError, index);
		employeeService.lockViewedTransport( commonError, loginUser, transport );
		customRedirector( EmployeeConstants.TRANSPORT_LOCKED_SUCCESS, EmployeeConstants.TRANSPORT_LIST_PAGE);
	}

	/**
	 * This method locks viewed transport
	 * @param index
	 * @return
	 */
	public void unlockViewedTransport(String index){
		transport = employeeService.getTransport(commonError, index);
		employeeService.unlockViewedTransport( commonError, loginUser, transport );
		if( commonError.getError().isEmpty() ){
			customRedirector( EmployeeConstants.TRANSPORT_UNLOCKED_SUCCESS, EmployeeConstants.TRANSPORT_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( EmployeeConstants.SELECTED_TRANSPORT_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( EmployeeConstants.SELECTED_TRANSPORT_ID );
		if( toggleList != null ) model.remove( EmployeeConstants.TRANSPORT_TOGGLER_LIST );		
    }


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public Transport getTransport() {
		return transport;
	}


	public void setTransport(Transport transport) {
		this.transport = transport;
	}


	public List<Transport> getTransportList() {
		return transportList;
	}


	public void setTransportList(List<Transport> transportList) {
		this.transportList = transportList;
	}


	public List<Transport> getFilteredTransportList() {
		return filteredTransportList;
	}


	public void setFilteredTransportList(List<Transport> filteredTransportList) {
		this.filteredTransportList = filteredTransportList;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<Transport> getSelectedTransportList() {
		return selectedTransportList;
	}


	public void setSelectedTransportList(List<Transport> selectedTransportList) {
		this.selectedTransportList = selectedTransportList;
	}


	public String getTransportId() {
		return transportId;
	}


	public void setTransportId(String transportId) {
		this.transportId = transportId;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


}
