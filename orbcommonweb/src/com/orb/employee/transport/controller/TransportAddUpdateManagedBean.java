package com.orb.employee.transport.controller;
/**
 * @author IST planbar / NaKa
 * 
 */
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Transport;
import com.orb.employee.constants.EmployeeConstants;
import com.orb.employee.controller.AbstractEmployeeController;

@ManagedBean(name="transportAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/employee")
public class TransportAddUpdateManagedBean extends AbstractEmployeeController {
	private static Log logger = LogFactory.getLog(TransportAddUpdateManagedBean.class);
	  
	private Transport transport =null;
	private List<Transport> transportList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String transportId = null;
	private User loginUser = null;
	private List<User> userList = null;
	
	/**
	 * This method will be called when user clicks Add button / transport Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateTransport")
	public String manipulateTransport( HttpServletRequest request, ModelMap model){
		transportId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( EmployeeConstants.SELECTED_TRANSPORT_ID, transportId );
		return EmployeeConstants.TRANSPORT_EDIT_HTML;
	}	

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		transportId = getJSFRequestAttribute( EmployeeConstants.SELECTED_TRANSPORT_ID );
		logger.info(" Transport Id :" +transportId);
		userList = getAllUsersList();
		if(CommonUtils.getValidString(transportId).isEmpty()){
			transport = new Transport();
			showIdentifier=CommonConstants.FALSE;
		}else{
			transport=employeeService.getTransport(commonError, transportId);
		 }
	}

	/**
	 * This method will add / update a transport in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(CommonUtils.getValidString(transport.getId()).isEmpty()){
			employeeService.addTransport( commonError, loginUser, transport );
		}else{
			employeeService.updateTransport( commonError, loginUser, transport );
		}
		
		String msg=CommonUtils.getValidString(transport.getId()).isEmpty() ? EmployeeConstants.TRANSPORT_ADDED_SUCCESS : EmployeeConstants.TRANSPORT_UPDATED_SUCCESS ;
		customRedirector( msg, EmployeeConstants.TRANSPORT_LIST_PAGE);		
	}


	/**
	 * This method to returns to transport view page
	 * @return 
	 */
	public void cancelAction(){
		if(transport.getId()==null){
			redirector(EmployeeConstants.TRANSPORT_LIST_PAGE);
		}else{
			redirector(EmployeeConstants.TRANSPORT_VIEW_PAGE+"?objectId="+transport.getIdentifier());
		}
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public List<Transport> getTransportList() {
		return transportList;
	}

	public void setTransportList(List<Transport> transportList) {
		this.transportList = transportList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public String getTransportId() {
		return transportId;
	}

	public void setTransportId(String transportId) {
		this.transportId = transportId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}


	

}
