package com.orb.calendar.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.calendar.bean.CalendarEvent;
import com.orb.calendar.bean.CalendarEventBean;
import com.orb.calendar.model.CalendarVal;
import com.orb.calendar.service.CalendarService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.service.FacilitiesService;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.service.TaskPlannerService;
@ManagedBean(name="calendarListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/calendar")
public class CalendarManagedBean extends AbstractCommonController {

	@Autowired 
	private CalendarService calendarService;
	
	@Autowired
	protected FacilitiesService facilitiesService;
	
	@Autowired
	protected TaskPlannerService taskPlannerService;
	
	@Autowired 
	private CommonError commonError;

	private ScheduleModel eventModel = null;
	private ScheduleModel lazyEventModel;
	private ScheduleEvent event = new CalendarEvent();
	private List<CalendarEventBean> calendarEventList = null;
	private Map<String,String> cachedEvents = new HashMap<String,String>();
	private List<CalendarVal> calendars = null;
	private List<String> selectedCalIds = new ArrayList<String>();
	private List<CalendarVal> selectedCalendar = null;
	private List<CalendarVal> calendarList = null;
	private String calName = null;
	private String calType = null;
	private String calColor = null;
	private String calFacilities = null;
	private String calTask = null;
	private String calId = null;
	private User user = null;
	private String displayDateFormat = null;
	private List<Facilities> facilitiesList = null;
	private List<Task> tasksList =  null;
	private List<Task> filterTaskList =  null;
	private CalendarVal calendarVal = null;
	private Facilities facilities =null;
	private Task task = null;
	
	@RequestMapping("/loadCalendar")
	public String startCalendar(HttpServletRequest httpRequest, ModelMap model, Locale locale){
		return "calendar/calendar.xhtml";
	}

	/**
	 * OnLoad action
	 */
	public void onLoadAction(){
		facilitiesList = new ArrayList<>();
		tasksList = new ArrayList<>();
		user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		displayDateFormat = user.getDisplayDateFormat();
		facilitiesList =facilitiesService.getActiveFacilitiesList(commonError);
		tasksList = taskPlannerService.getActiveTaskList(commonError);
		calendars = calendarService.getCalendars( commonError );
	}

	public ScheduleModel getEventModel() {
		if( eventModel == null ){
			eventModel = new DefaultScheduleModel();
			manipulateEvents();
		}
		return eventModel;
	}

	/**
	 * This method will call event insert, update and split services
	 *  
	 * @param actionEvent
	 */
	public void addEvent(ActionEvent actionEvent) {
		CalendarEventBean calendarEvent = getCalendarEvent(event);

		if(event.getId() == null){
			calendarService.insertEvent( commonError, calendarEvent );
		}else{
			if( event.getId() == null ) return;
			String recordId = cachedEvents.get(event.getId());
			calendarEvent.setId(recordId);
			if (calendarEvent.getEditAll()) {
				calendarService.updateEvents( commonError, calendarEvent );
			} else {
				calendarService.splitEvents( commonError, calendarEvent );
			}
		}
		calendarEventList = calendarService.getCalendarEvents(commonError, selectedCalIds);
		eventModel.clear();
		manipulateEvents();
	}

	/**
	 * This method will call the event delete services
	 * @param actionEvent
	 */
	public void deleteEvent(ActionEvent actionEvent){
		if( event.getId() == null ) return;
		String recordId = cachedEvents.get(event.getId());
		if( recordId == null){
			eventModel.deleteEvent(event);
		}else{
			CalendarEventBean calendarEvent = getCalendarEvent(event);
			calendarEvent.setId(recordId);
			if (calendarEvent.getEditAll() || null == calendarEvent.getActualType() || 0 == calendarEvent.getActualCount() ) {
				calendarService.deleteEvent( commonError, calendarEvent );
			} else {
				calendarService.splitDeleteEvent( commonError, calendarEvent );
			}
			calendarEventList = calendarService.getCalendarEvents(commonError, selectedCalIds);
			eventModel.clear();
			manipulateEvents();
		}
	}

	/**
	 * Selecting event on calendar 
	 * @param selectEvent
	 */
	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
	}

	/**
	 * Selecting date on calendar
	 * @param selectEvent
	 */
	public void onDateSelect(SelectEvent selectEvent) {
		event = new CalendarEvent("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
	}

	/**
	 * Moving event on calendar
	 * @param event
	 */
	public void onEventMove(ScheduleEntryMoveEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
		addMessage(message);
		CalendarEventBean calendarEvent = getCalendarEvent(event.getScheduleEvent());
		String recordId = cachedEvents.get(event.getScheduleEvent().getId());
		if( recordId != null ){
			calendarEvent.setId(recordId);
		}
		manipulateEvents();
	}

	/**
	 * Change the event schedule
	 * @param event
	 */
	public void onEventResize(ScheduleEntryResizeEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());
		addMessage(message);
	}

	/**
	 * While selecting calendars, This method calls service for fetch events
	 * @param selectEvent
	 */
	public void selectCalendar(SelectEvent selectEvent){

		CalendarVal selectedCalendar = (CalendarVal)selectEvent.getObject();

		if( selectedCalendar != null ){
			selectedCalIds.add(selectedCalendar.getId());
		}
		calendarEventList = calendarService.getCalendarEvents(commonError, selectedCalIds);
		eventModel.clear();
		manipulateEvents();
	}

	/**
	 * While unselecting calendars, This method calls service for fetch events
	 * @param selectEvent
	 */
	public void unselectCalendar(UnselectEvent selectEvent){

		CalendarVal selectedCalendar = (CalendarVal)selectEvent.getObject();

		if( selectedCalendar != null ){
			selectedCalIds.remove(selectedCalendar.getId());
		}
		calendarEventList = calendarService.getCalendarEvents(commonError, selectedCalIds);
		eventModel.clear();
		manipulateEvents();
	}

	/**
	 * This method calls create calendar service
	 */
	public void createCalendar() {
		CalendarVal calAttributes = new CalendarVal();
		calAttributes.setUserId(user.getUserId());
		calAttributes.setValue(getCalName());
		calAttributes.setType(getCalType());
		calAttributes.setFacilities(getCalFacilities());
		calAttributes.setTask(getCalTask());
		calAttributes.setColor(getCalColor());
		
		calendarService.createCalendar(commonError, calAttributes);
		calendars = calendarService.getCalendars(commonError);
	}

	/**
	 * This method calls fetch calendar service for edit calendars
	 */
	public void editCalendar() {
		String editCalId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("editCalId");
		List<CalendarVal> calAttributes = calendarList;
		calAttributes = calendarService.getCalendars(commonError,editCalId);
		setCalName(calAttributes.get(0).getValue());
		setCalType(calAttributes.get(0).getType());
		setCalFacilities(calAttributes.get(0).getFacilities());
		setCalTask(calAttributes.get(0).getTask());
		setCalColor(calAttributes.get(0).getColor());
		setCalId(calAttributes.get(0).getId());
	}

	/**
	 * This method calls update calendar service
	 */
	public void updateCalendar() {
		CalendarVal calAttributes = new CalendarVal();
		calAttributes.setId(getCalId());
		calAttributes.setValue(getCalName());
		calAttributes.setType(getCalType());
		calAttributes.setFacilities(getCalFacilities());
		calAttributes.setTask(getCalTask());
		calAttributes.setColor(getCalColor());
		calendarService.updateCalendar(commonError, calAttributes);
		calendars = calendarService.getCalendars(commonError);
	}

	/**
	 * This method calls delete calendar service
	 */
	public void deleteCalendar() {
		CalendarVal calAttributes = new CalendarVal();
		calAttributes.setId(getCalId());
		calendarService.deleteCalendar(commonError, calAttributes);
		calendars = calendarService.getCalendars(commonError);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	/**
	 * This method performs setter operations for events
	 * @param event
	 * @return
	 */
	private CalendarEventBean getCalendarEvent( ScheduleEvent event ){
		CalendarEventBean calendarEventBean = new CalendarEventBean();
		CalendarEvent calendarEvent = (CalendarEvent)event;
		calendarEventBean.setUserId(user.getUserId());
		calendarEventBean.setRelId(calendarEvent.getRelId());
		calendarEventBean.setTitle(calendarEvent.getTitle());
		calendarEventBean.setStartDate(calendarEvent.getStartDate());
		calendarEventBean.setEndDate(calendarEvent.getEndDate());
		calendarEventBean.setAllDay(calendarEvent.isAllDay());
		calendarEventBean.setCalId(calendarEvent.getCalId());
		calendarEventBean.setType(calendarEvent.getType());
		calendarEventBean.setCount(calendarEvent.getCount());
		calendarEventBean.setSkip(calendarEvent.getSkip());
		calendarEventBean.setTillDate(calendarEvent.getTillDate());
		calendarEventBean.setEditAll(calendarEvent.getEditAll());
		calendarEventBean.setActualStartDate(calendarEvent.getActualStartDate());
		calendarEventBean.setActualEndDate(calendarEvent.getActualEndDate());
		calendarEventBean.setActualTillDate(calendarEvent.getActualTillDate());
		calendarEventBean.setActualTitle(calendarEvent.getActualTitle());
		calendarEventBean.setActualCalId(calendarEvent.getActualCalId());
		calendarEventBean.setActualType(calendarEvent.getActualType());
		calendarEventBean.setActualSkip(calendarEvent.getActualSkip());
		calendarEventBean.setActualCount(calendarEvent.getActualCount());
		calendarEventBean.setEventIndex(calendarEvent.getEventIndex());
		return calendarEventBean;
	}

	/**
	 * Manipulate events
	 */
	@SuppressWarnings("unchecked")
	private void manipulateEvents(){
		cachedEvents.clear();
		if(calendarEventList != null){
			for( CalendarEventBean calendarEvent : calendarEventList){
				event = createCalendarEvent(calendarEvent);
				eventModel.addEvent(event);
				cachedEvents.put( event.getId(), calendarEvent.getId());
			}
		}

	}

	/**
	 * This method performs setter method for events while creating events
	 * @param calendarEvent
	 * @return
	 */
	private CalendarEvent createCalendarEvent( CalendarEventBean calendarEvent ){
		CalendarEvent event = new CalendarEvent(calendarEvent.getTitle(), calendarEvent.getStartDate(), calendarEvent.getEndDate());
		event.setRelId(calendarEvent.getRelId());
		event.setCalId(calendarEvent.getCalId());
		event.setEditAll(calendarEvent.getEditAll());
		event.setActualTitle(calendarEvent.getActualTitle());
		event.setActualCalId(calendarEvent.getActualCalId());
		event.setActualStartDate(calendarEvent.getActualStartDate());
		event.setActualEndDate(calendarEvent.getActualEndDate());
		event.setActualTillDate(calendarEvent.getActualTillDate());
		event.setActualType(calendarEvent.getActualType());
		event.setActualSkip(calendarEvent.getActualSkip());
		event.setActualCount(calendarEvent.getActualCount());
		event.setSkip(calendarEvent.getSkip());
		event.setType(calendarEvent.getType());
		event.setCount(calendarEvent.getCount());
		event.setEventIndex(calendarEvent.getEventIndex());
		if( calendarEvent.getAllDay() != null )
			event.setAllDay( calendarEvent.getAllDay() );
		return event;

	}
	
	/**
	 * This method is used to filter task name 
	 * @param id
	 * @return
	 */	
	public void filterTask(String id){
		filterTaskList = new ArrayList<>();
		if(tasksList!=null){
			for(Task task:tasksList){
			   if(id.equals(task.getFacilities())){
				   filterTaskList.add(task);
			    }
			}
		}
	}

	
	

	public String getCalId() {
		return calId;
	}

	public void setCalId(String calId) {
		this.calId = calId;
	}

	public List<CalendarVal> getSelectedCalendar() {
		return selectedCalendar;
	}

	public void setSelectedCalendar(List<CalendarVal> selectedCalendar) {
		this.selectedCalendar = selectedCalendar;
	}

	public String getCalType() {
		return calType;
	}

	public void setCalType(String calType) {
		this.calType = calType;
	}

	public String getCalColor() {
		return calColor;
	}

	public void setCalColor(String calColor) {
		this.calColor = calColor;
	}

	public String getCalName() {
		return calName;
	}

	public void setCalName(String calName) {
		this.calName = calName;
	}

	public List<CalendarVal> getCalendarList() {
		return calendarList;
	}

	public void setCalendarList(List<CalendarVal> calendarList) {
		this.calendarList = calendarList;
	}
	
	public List<CalendarVal> getCalendars() {
		return calendars;
	}

	public void setCalendars(List<CalendarVal> calendars) {
		this.calendars = calendars;
	}
	
	public ScheduleModel getLazyEventModel() {
		return lazyEventModel;
	}

	public ScheduleEvent getEvent() {
		return event;
	}

	public void setEvent(ScheduleEvent event) {
		this.event = event;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public CalendarService getCalendarService() {
		return calendarService;
	}

	public List<CalendarEventBean> getCalendarEventList() {
		return calendarEventList;
	}

	public void setCalendarEventList(List<CalendarEventBean> calendarEventList) {
		this.calendarEventList = calendarEventList;
	}

	public Map<String, String> getCachedEvents() {
		return cachedEvents;
	}

	public void setCachedEvents(Map<String, String> cachedEvents) {
		this.cachedEvents = cachedEvents;
	}

	public List<String> getSelectedCalIds() {
		return selectedCalIds;
	}

	public void setSelectedCalIds(List<String> selectedCalIds) {
		this.selectedCalIds = selectedCalIds;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public void setLazyEventModel(ScheduleModel lazyEventModel) {
		this.lazyEventModel = lazyEventModel;
	}

	public void setCalendarService(CalendarService calendarService) {
		this.calendarService = calendarService;
	}
	public CommonError getCommonError() {
		return commonError;
	}

	public void setCommonError(CommonError commonError) {
		this.commonError = commonError;
	}
	public String getDisplayDateFormat() {
		return displayDateFormat;
	}

	public void setDisplayDateFormat(String displayDateFormat) {
		this.displayDateFormat = displayDateFormat;
	}

	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}

	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}

	public FacilitiesService getFacilitiesService() {
		return facilitiesService;
	}

	public void setFacilitiesService(FacilitiesService facilitiesService) {
		this.facilitiesService = facilitiesService;
	}

	public TaskPlannerService getTaskPlannerService() {
		return taskPlannerService;
	}

	public void setTaskPlannerService(TaskPlannerService taskPlannerService) {
		this.taskPlannerService = taskPlannerService;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}

	
	public List<Task> getFilterTaskList() {
		return filterTaskList;
	}

	public void setFilterTaskList(List<Task> filterTaskList) {
		this.filterTaskList = filterTaskList;
	}

	public CalendarVal getCalendarVal() {
		return calendarVal;
	}

	public void setCalendarVal(CalendarVal calendarVal) {
		this.calendarVal = calendarVal;
	}

	public Facilities getFacilities() {
		return facilities;
	}

	public void setFacilities(Facilities facilities) {
		this.facilities = facilities;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getCalFacilities() {
		return calFacilities;
	}

	public void setCalFacilities(String calFacilities) {
		this.calFacilities = calFacilities;
	}

	public String getCalTask() {
		return calTask;
	}

	public void setCalTask(String calTask) {
		this.calTask = calTask;
	}
	

}
