package com.orb.taskplanner.task.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Task;

public class TaskCreateMandatoryValidator implements Validator{
	
	public String fieldValidator(Task task){
		String checkValidation = null;
		// 
		if("".equals(CommonUtils.getValidString(task.getName())) || task.getProject() == null || task.getDueDate() == null
				|| task.getCategory() == null || task.getDeputy() == null || task.getFacilities() == null){
			checkValidation = CommonConstants.FALSE;
		}else
			checkValidation = CommonConstants.TRUE;
		
		return checkValidation;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object arg0, Errors arg1) {
		// TODO Auto-generated method stub
		
	}
}
