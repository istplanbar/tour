package com.orb.taskplanner.task.controller;

/**
 * @author Kamala Kannan Chellapandi
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.Crm;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.controller.AbstractTaskPlannerController;



@ManagedBean(name="taskPlannerView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
@SessionAttributes({TaskPlannerConstants.NEW_TASK_DIALOG})
public class TaskPlannerListViewManagedBean extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(TaskPlannerListViewManagedBean.class);
	private User user = null;
	private User loggedUser =null;
	private User deputyUser = null;
	private Task task = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Task> taskList = null;
	private List<Task> taskStatusHistoryList = null;
	private List<Task> filteredTaskStatusHistoryList = null;
	private List<Authorization> authorizationList = null;
	private List<Project> projectList = null;
	private List<MultiVal> statusList = null;
	private List<MultiVal> priorityList = null;
	private List<TaskCategory> categoryList = null;
	private List<TaskType> typeList = null;
	private List<User> userList = null;
	private List<User> deputyList = null;
	private List<Task> filteredTaskList = null;
	private List<String> catList =null;
	private List<String> projList =null;
	private List<String> statList =null;
	private Boolean createNewTask = false;
	private String userId = null;
	private List<Boolean> togglerList = null;
	private String priority = TaskPlannerConstants.PRIORITY;
	private String loggedUserId = null;
	private List<Task> watchList = null;
	private List<Task> selectedTaskList = null;
	private Boolean showWatchList = false;
	//private List<Documents> documentList = null;
	private List<String> uploadedFileList = null;
	private String defaultView = null;
	private Boolean isDetailView = null;
	private Boolean isListView = null;
	private String changeView = null;
	private List<Crm> crmList = null;
	private List<String> crmNameList = null;
	//private List<Participant> participantList = null;
	//private List<String> participantNameList = null;
	/*private List<Course> courseList = null;*/
	private List<String> courseNameList = null;
	private List<Facilities> facilitiesList = null;
	private Map<String, String> estimatedWorkHoursList = null;
	
	/**
	 * This returns the Task list page when user clicks TaskPlanner menu
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadTaskPlanner")
    public String loadTaskPlanner(ModelMap model , HttpServletRequest request){
		clearSessionObjects(model);
		defaultView = CommonUtils.getValidString( request.getParameter( TaskPlannerConstants.DEFAULT_VIEW ) );
		model.addAttribute(TaskPlannerConstants.DEFAULT_VIEW, defaultView);
		return TaskPlannerConstants.TASK_LIST_HTML;
    }
	/**
	 * This returns the Task list page when user clicks TaskPlanner menu
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadTaskPlannerTopBar")
    public String loadTaskPlannerTopBar(ModelMap model , HttpServletRequest request){
		clearSessionObjects(model);
		user =(User) request.getSession().getAttribute(CommonConstants.USER_IN_CONTEXT);
		model.addAttribute( TaskPlannerConstants.LOGGEDIN_USER_TASKS, user.getUserId() );
		return TaskPlannerConstants.TASK_LIST_HTML;
    }
	
	/**
	 * This returns the watch list page when user clicks TaskPlanner menu
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadWatchListTopBar")
    public String loadWatchListTopBar(ModelMap model , HttpServletRequest request){
		clearSessionObjects(model);
		model.addAttribute( TaskPlannerConstants.LOGGEDIN_USER_TASKS, TaskPlannerConstants.LOGGEDIN_USER_TASKS );
		return TaskPlannerConstants.TASK_LIST_HTML;
    }
	
	/**
	 * This methods helps to view particular task
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/viewTaskPlanner")
    public String viewTask(ModelMap model , HttpServletRequest request){
		String taskId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		defaultView = CommonUtils.getValidString( request.getParameter( TaskPlannerConstants.DEFAULT_VIEW ) );
		model.addAttribute(TaskPlannerConstants.DEFAULT_VIEW, defaultView);
		model.addAttribute( TaskPlannerConstants.SELECTED_TASK_ID, taskId );
		return TaskPlannerConstants.TASK_VIEW_HTML;
    }
	
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		estimatedWorkHoursList = (Map<String,String>)FacilitiesConstants.ESTIMATED_WORK_HOURS;
		userId = CommonUtils.getValidString( getJSFRequestAttribute( TaskPlannerConstants.LOGGEDIN_USER_TASKS ));
		loggedUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		loggedUserId = loggedUser.getUserId();
		catList = new ArrayList<>();
		statList = new ArrayList<>();
		projList = new ArrayList<>();
		crmNameList = new ArrayList<>();
		//participantNameList = new ArrayList<>();
		courseNameList = new ArrayList<>();
		uploadedFileList = new ArrayList<>();
		List<String> fileList = new ArrayList<>();
		defaultView = CommonUtils.getValidString(getJSFRequestAttribute( TaskPlannerConstants.DEFAULT_VIEW ) );
		logger.info("Default View: "+defaultView);
		if(defaultView.isEmpty() || defaultView.equals(TaskPlannerConstants.TASK_DETAIL_VIEW)){
			isDetailView = true;
			changeView = CommonConstants.FALSE;
		}else{
			isListView = true;
			changeView = CommonConstants.TRUE;
		}
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList(true, false, true, true, true, true, true, true, false, false);
		}
		deputyList = getAllUsersList();
		if(deputyList != null){
			deputyList = deputyList.stream().filter(p -> !p.getUserId().equals(loggedUser.getUserId())).collect(Collectors.toList());
		}
		deputyUser = userProfileService.getLoginUser( loggedUserId );
		statusList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_STATUS );
		priorityList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_PRIORITY );
		projectList = taskPlannerService.getActiveProjectList(commonError);
		typeList = taskPlannerService.getActiveTypeList(commonError);
		authorizationList = taskPlannerService.getActiveAuthorizationList(commonError);
		categoryList = taskPlannerService.getActiveCategoryList(commonError);
		crmList = crmService.getActiveCrmList(commonError);
		facilitiesList=facilitiesService.getActiveFacilitiesList(commonError);
		if(facilitiesList == null) facilitiesList = new ArrayList<>();
		//participantList = participantService.getActiveParticipantList(commonError);
		/*courseList = courseService.getActiveCourseList(commonError);
		courseList = courseList.stream().filter(c -> !(CommonUtils.getValidString(c.getCourseId()).isEmpty() && c.getCourseId() == null) ).collect(Collectors.toList());*/
		watchList =  taskPlannerService.getTaskWatchList(commonError);
		if("".equals(CommonUtils.getValidString(userId))){
			taskList = taskPlannerService.getActiveTaskList(commonError);
		}else if(TaskPlannerConstants.LOGGEDIN_USER_TASKS.equals(userId)){
			taskList = taskPlannerService.getActiveTaskList(commonError);
			showWatchList = true;
			List<Task> tempTaskList = new ArrayList<>();
			Set<Task> tempTaskSet = new HashSet<>();
			watchList.forEach(t->{
				tempTaskList.addAll((List<Task>)taskList.stream().filter(s -> s.getIdentifier().equals(t.getIdentifier()) && t.getCreateBy().equals(loggedUserId) ).collect(Collectors.toList()));
			});
			tempTaskSet.addAll(tempTaskList);
			tempTaskList.clear();
			tempTaskList.addAll(tempTaskSet);
			taskList = new ArrayList<>(tempTaskList);
		}else{
			user = getUserById(commonError, userId);
			taskList = taskPlannerService.getUserTaskList(commonError, user);
		}
		if(taskList != null && !taskList.isEmpty()) {
			taskStatusHistoryList = taskPlannerService.getTaskStatusHistoryList(commonError); 
			if(taskStatusHistoryList == null) taskStatusHistoryList = new ArrayList<>();
			for(Task tsk: taskList){
				if(!"".equals(tsk.getAttachment())){
					fileList = new ArrayList<>(Arrays.asList(tsk.getAttachment().split(",")));
				}else{
					fileList = new ArrayList<>();
				}
			Set<String> hs = new HashSet<>();
			hs.addAll(fileList);
			fileList.clear();
			fileList.addAll(hs);
			tsk.setAttachmentList(fileList);	
			}
			filteredTaskList = new ArrayList<>(taskList);
		}else{
			taskList = new ArrayList<>();
		}
		if(categoryList !=  null){
		catList = categoryList.stream().map(TaskCategory::getName).collect(Collectors.toList());
		}
		if(statusList !=  null) {
			statList = statusList.stream().map(MultiVal::getId).collect(Collectors.toList());
		}
		if(projectList !=  null) {
			projList = projectList.stream().map(Project::getName).collect(Collectors.toList());
		}
		if(crmList !=  null){
			crmNameList = crmList.stream().map(Crm::getIdentifier).collect(Collectors.toList());
		}
		/*if(participantList !=  null) {
			participantNameList = participantList.stream().map(Participant::getIdentifier).collect(Collectors.toList());
		}*/
		/*if(courseList !=  null) {
			courseNameList = courseList.stream().map(Course::getIdentifier).collect(Collectors.toList());
		}
		*/
		String taskId = CommonUtils.getValidString( getJSFRequestAttribute( TaskPlannerConstants.SELECTED_TASK_ID ));
		logger.info(taskId);
		String taskCreation = CommonUtils.getValidString(( String )getSessionObj( TaskPlannerConstants.NEW_TASK_DIALOG ));
		if(CommonConstants.TRUE.equals(taskCreation)){
			RequestContext.getCurrentInstance().execute("myTaskDialog.show()");
			addSessionObj( TaskPlannerConstants.NEW_TASK_DIALOG ,"false" );
		}else{
			RequestContext.getCurrentInstance().execute("myTaskDialog.hide()");
		}
		if( "".equals(taskId) ){
			if(!taskList.isEmpty() && taskList != null ){
				task = getTaskList().get(0);
			}else{
				task = new Task();
				taskList = new ArrayList<>();
			}
		}else{
			filteredTaskStatusHistoryList = new ArrayList<>();
			task = taskPlannerService.getTask( commonError, taskId );
			if(!"".equals(task.getAttachment())){
				fileList = new ArrayList<>(Arrays.asList(task.getAttachment().split(",")));
			}else{
				fileList = new ArrayList<>();
			}
			Set<String> hs = new HashSet<>();
			hs.addAll(fileList);
			fileList.clear();
			fileList.addAll(hs);
			task.setAttachmentList(fileList);	
			if(projectList != null){
				task.setProject(projectList.stream().filter(proj -> proj.getId().equals(task.getProject())).map(Project::getName)					
						.findAny().orElse(null));
			}
			if(categoryList != null){
				task.setCategory(categoryList.stream().filter(cat -> cat.getId().equals(task.getCategory())).map(TaskCategory::getName)					
						.findAny().orElse(null));
			}
			if( taskStatusHistoryList != null){
				filteredTaskStatusHistoryList = taskStatusHistoryList.stream().filter(s -> s.getIdentifier().equals(task.getIdentifier()))
						.collect(Collectors.toList());
			}
		}
		task = getTaskWatch(watchList,task);
	}
	
	@PostConstruct
	public void init(){
		estimatedWorkHoursList = (Map<String,String>)FacilitiesConstants.ESTIMATED_WORK_HOURS;
		categoryList = taskPlannerService.getActiveCategoryList(commonError);
		statusList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_STATUS );
		projectList = taskPlannerService.getActiveProjectList(commonError);
		typeList = taskPlannerService.getActiveTypeList(commonError);
		authorizationList = taskPlannerService.getActiveAuthorizationList(commonError);
		priorityList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_PRIORITY );
		crmList = crmService.getActiveCrmList(commonError);
		facilitiesList=facilitiesService.getActiveFacilitiesList(commonError);
		if(facilitiesList == null) facilitiesList = new ArrayList<>();
		//participantList = participantService.getActiveParticipantList(commonError);
		/*courseList = courseService.getActiveCourseList(commonError);
		courseList = courseList.stream().filter(c -> !(CommonUtils.getValidString(c.getCourseId()).isEmpty() && c.getCourseId() == null) ).collect(Collectors.toList());*/
		setUserList(getAllUsersList());
		loggedUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		/*if(loggedUser!=null){
		documentList = qmsService.getActiveDocumentList(commonError, loggedUser);
		}
		if(documentList != null){
			documentList = (List<Documents>) documentList.stream().filter(p -> !"".equals(p.getUploadedDoc())).collect(Collectors.toList());
		}else{
			documentList = new ArrayList<>();
		}*/
		if(loggedUser != null){
			loggedUserId = loggedUser.getUserId();
		}
		deputyList = getAllUsersList();
		if(deputyList != null && loggedUser != null){
			deputyList = deputyList.stream().filter(p -> !p.getUserId().equals(loggedUser.getUserId())).collect(Collectors.toList());
		}
		deputyUser = userProfileService.getLoginUser( loggedUserId );
	}
	
	/**
	 * show history of the particular task
	 */
	public void showTaskHistory(){
		taskStatusHistoryList = taskPlannerService.getTaskStatusHistoryList(commonError);
		taskStatusHistoryList = (List<Task>)taskStatusHistoryList.stream().filter(f -> f.getIdentifier().equals(task.getIdentifier())).collect(Collectors.toList());
		watchList =  taskPlannerService.getTaskWatchList(commonError);
		task = getTaskWatch(watchList,task);
	}
	

	/**
	 * Return the estimated Work in String
	 */
	public String returnedEstimatedWork(String mins){
		String estimatedHour = null;
		for(Map.Entry<String, String> workHour : estimatedWorkHoursList.entrySet()){
			if(workHour.getValue().equals(mins)){
				estimatedHour = workHour.getKey();
			}
		}
		return CommonUtils.getValidString(estimatedHour);
	}
	
	private Task getTaskWatch(List<Task> watchList,Task task){
		watchList.forEach(w->{
			if(w.getIdentifier().equals(task.getIdentifier()) && w.getCreateBy().equals(loggedUserId)){
				task.setWatchStatus(CommonConstants.TRUE);
			}
		});
		return task;
    }
	
	/**
	 * Check paticular whether is  in watch list
	 */
	public String isInWatchList(String taskId){
		task = taskPlannerService.getTask( commonError, taskId );
		task.setWatchStatus(CommonConstants.FALSE);
		watchList.forEach(w->{
			if(w.getIdentifier().equals(task.getIdentifier()) && w.getCreateBy().equals(task.getCreateBy())){
				task.setWatchStatus(CommonConstants.TRUE);
			}
		});
		return task.getWatchStatus();	
	}
	
	/**
	 * This method will navigate to the view page
	 * @param index
	 * @return
	 */
	public void viewAction( String index ){
		addSessionObj( TaskPlannerConstants.TOGGLER_LIST , togglerList );
		if(changeView == CommonConstants.TRUE){
			defaultView = TaskPlannerConstants.TASK_LIST_VIEW;
		}else{
			defaultView = TaskPlannerConstants.TASK_DETAIL_VIEW;
		}
		redirector(TaskPlannerConstants.TASK_VIEW_PAGE + "?objectId=" + index+"&view="+defaultView);
	}	

	
	/**
	 * This method helps to edit particular task
	 */
	public void editAction( String index ){
		redirector( TaskPlannerConstants.TASK_ADDUPDATE_PAGE + "?objectId=" + task.getId()+"&view="+TaskPlannerConstants.TASK_DETAIL_VIEW );
	}
	/**
	 * This method helps to edit particular task
	 */
	public void editViewAction( String index ){
		redirector( TaskPlannerConstants.TASK_ADDUPDATE_PAGE + "?objectId=" + index +"&view="+TaskPlannerConstants.TASK_LIST_VIEW);
	}

	/**
	 * This method to returns to main Task list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		taskList = taskPlannerService.getActiveTaskList(commonError); // null check 
		if( taskList != null ) task = taskList.get(0);
		redirector( TaskPlannerConstants.TASK_LIST_PAGE+"?view="+defaultView );
	}
	
	/**
	 * This method deletes Task
	 * @param list page
	 * @return 
	 */
	public void deleteViewedAction(String taskId){
		task = taskPlannerService.getTask( commonError, taskId );
		taskPlannerService.deleteTask( commonError, loggedUser, task);
		if( commonError.getError().isEmpty() ){
			customRedirector(TaskPlannerConstants.TASK_DELETE_SUCCESS, TaskPlannerConstants.TASK_LIST_PAGE+"?view="+defaultView );
		}
	}
	
	/**
	 * This method deletes Task
	 * @param list page
	 * @return 
	 */
	public void deleteAction(String taskId){
		task = taskPlannerService.getTask( commonError, taskId );
		taskPlannerService.deleteTask( commonError, loggedUser, task);
		if( commonError.getError().isEmpty() ){
			customRedirector(TaskPlannerConstants.TASK_DELETE_SUCCESS, TaskPlannerConstants.TASK_LIST_PAGE );
		}
	}
	
	/**
	 * This method deletes Task
	 * @param list page
	 * @return 
	 */
	public void deleteMultipleTaskAction(){
		if(!selectedTaskList.isEmpty()){
			for(Task tsk : selectedTaskList){
				taskPlannerService.deleteTask( commonError, loggedUser, tsk);
			}
			if( commonError.getError().isEmpty() ){
				customRedirector(TaskPlannerConstants.TASK_DELETE_SUCCESS, TaskPlannerConstants.TASK_LIST_PAGE+"?view="+TaskPlannerConstants.TASK_LIST_VIEW );
				//redirector(TaskPlannerConstants.TASK_LIST_PAGE+"?view="+TaskPlannerConstants.TASK_LIST_VIEW );
			}
		}
	}
	
	/**
	 * This method helps to filter the results in data table
	 * @param event
	 */
	public List<Task> onChangeEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		filteredTaskList = taskList.stream().filter(p -> p.getCategory().equals(newValue)).collect(Collectors.toList());
		return filteredTaskList;
	}

	
	/**
	 * MultiSelection filter category wise
	 * @param event
	 * @return
	 */
	public List<Task> onChangeCategoryEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			catList.forEach(newValue->{
				filteredTaskList.addAll(taskList.stream().filter(p -> p.getCategory().equals(newValue)).collect(Collectors.toList()));
			});
			filteredTaskList = filteredTaskList.stream().sorted((p1,p2) -> p1.getCategory().compareTo(p2.getCategory())).collect(Collectors.toList());
		}
		return filteredTaskList;
	}
	
	
	/**
	 * MultiSelection filter project wise
	 * @param event
	 * @return
	 */
	public List<Task> onChangeProjectEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			projList.forEach(newValue->{
			  filteredTaskList.addAll(taskList.stream().filter(p -> p.getProject().equals(newValue)).collect(Collectors.toList()));
			});
			filteredTaskList = filteredTaskList.stream().sorted((p1,p2) -> p1.getProject().compareTo(p1.getProject())).collect(Collectors.toList());
		}
		return filteredTaskList;
	}
	
	
	/**
	 * MultiSelection filter Status wise
	 * @param event
	 * @return
	 */
	public List<Task> onChangeStatusEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			statList.forEach(newValue->{
			   filteredTaskList.addAll(taskList.stream().filter(p -> p.getStatus().equals(newValue)).collect(Collectors.toList()));
			});
			
		}
		return filteredTaskList;
	}
	

	/**
	 * MultiSelection filter crmName wise
	 * @param event
	 * @return
	 */
	public List<Task> onChangeCrmEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			crmNameList.forEach(newValue->{
			   filteredTaskList.addAll(taskList.stream().filter(p -> newValue.equals(p.getCrmName())).collect(Collectors.toList()));
			});
			filteredTaskList = filteredTaskList.stream().sorted((p1,p2) -> p1.getCrmName().compareTo(p1.getCrmName())).collect(Collectors.toList());
		}
		return filteredTaskList;
	}
	
	/**
	 * MultiSelection filter participantName wise
	 * @param event
	 * @return
	 *//*
	public List<Task> onChangeParticipantEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			participantNameList.forEach(newValue->{
			   filteredTaskList.addAll(taskList.stream().filter(p -> newValue.equals(p.getParticipantName())).collect(Collectors.toList()));
			});
			filteredTaskList = filteredTaskList.stream().sorted((p1,p2) -> p1.getParticipantName().compareTo(p1.getParticipantName())).collect(Collectors.toList());
		}
		return filteredTaskList;
	}*/
	
	/**
	 * MultiSelection filter courseId wise
	 * @param event
	 * @return
	 *//*
	public List<Task> onChangeCourseEvent(AjaxBehaviorEvent event) {
		filteredTaskList.clear();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		if(taskList !=  null){
			courseNameList.forEach(newValue->{
			   filteredTaskList.addAll(taskList.stream().filter(p -> newValue.equals(p.getCourseId())).collect(Collectors.toList()));
			});
			filteredTaskList = filteredTaskList.stream().sorted((p1,p2) -> p1.getCourseId().compareTo(p1.getCourseId())).collect(Collectors.toList());
		}
		return filteredTaskList;
	}*/
	
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void onChangeView(String view){
		logger.info(view);
		if(changeView.equals(CommonConstants.FALSE)){
        	isListView = true;
			changeView = CommonConstants.TRUE;
			isDetailView = false;
		}else{
			changeView = CommonConstants.FALSE;
			isDetailView = true;
			isListView = false;
		}
	}
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void addTowatchListAction(){
		taskPlannerService.addTaskToWatchList(commonError,task.getIdentifier(),loggedUser);
		task.setWatchStatus(CommonConstants.TRUE);
	}
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void watchListAction(AjaxBehaviorEvent e){
		taskPlannerService.addTaskToWatchList(commonError,task.getIdentifier(),loggedUser);
		task.setWatchStatus(CommonConstants.TRUE);
	}
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void removeWatchListAction(){
		taskPlannerService.removeTaskFromWatchList(commonError,task.getIdentifier(),loggedUser);
		task.setWatchStatus(CommonConstants.FALSE);
	}
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void changeWatchListAction(AjaxBehaviorEvent e){
		taskPlannerService.removeTaskFromWatchList(commonError,task.getIdentifier(),loggedUser);
		task.setWatchStatus(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void changeToWatchList(){
		showWatchList = true;
		if(taskList != null && !taskList.isEmpty()){
			watchList =  taskPlannerService.getTaskWatchList(commonError);
			List<Task> tempTaskList = new ArrayList<>();
			Set<Task> tempTaskSet = new HashSet<>();
			watchList.forEach(t->{
			   tempTaskList.addAll((List<Task>)taskList.stream().filter(s -> s.getIdentifier().equals(t.getIdentifier()) && t.getCreateBy().equals(loggedUserId) ).collect(Collectors.toList()));
			});
			tempTaskSet.addAll(tempTaskList);
			tempTaskList.clear();
			tempTaskList.addAll(tempTaskSet);
			taskList = new ArrayList<>(tempTaskList);
			if(taskList != null && !taskList.isEmpty()) { 
				task = taskList.get(0);
				task = getTaskWatch(watchList,task);
			}else{
				task = new Task();
				taskStatusHistoryList = new ArrayList<>();
			}
		}else{
			taskList = new ArrayList<>();
			task = new Task();
			taskStatusHistoryList = new ArrayList<>();
		}
		filteredTaskList = new ArrayList<>(taskList);
	}
	
	
	/**
	 * This method is use to toggle task page between list and detail view
	 */
	public void changeFromWatchList(){
		showWatchList = false;	
		taskList = taskPlannerService.getActiveTaskList(commonError);
		watchList =  taskPlannerService.getTaskWatchList(commonError);
		if(taskList != null && !taskList.isEmpty()){ 
			 task = taskList.get(0);
			 if( watchList != null ){ 
				 for(Task tsk:watchList){
					 if(tsk.getIdentifier().equals(task.getIdentifier())){
						 task.setWatchStatus(CommonConstants.TRUE);
					 }
				 }
			 }
			 taskStatusHistoryList = taskPlannerService.getTaskStatusHistoryList(commonError); 
			 filteredTaskList = new ArrayList<>(taskList);
			 
		}
	}
	
	
	/**
	 * This method will get all the active Task of documents
	 * @param event
	 */
	public void showActive(ActionEvent event){
		taskList = taskPlannerService.getActiveTaskList( commonError );
		if( filteredTaskList != null ) filteredTaskList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive Task of documents
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		taskList = taskPlannerService.getInactiveTaskList( commonError );
		if( filteredTaskList != null ) filteredTaskList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	public void cloneAction(){
		redirector(TaskPlannerConstants.TASK_ADDUPDATE_CLONE_PAGE + "?objectId=" + task.getId() );
	}
			
	public void subTaskViewAction(String identifier){
		String id=taskPlannerService.getSubTaskId( commonError,identifier);
		viewAction(id);
	}	
	
	
	/**
	 * Filter company name based on company ID
	 * @param index
	 * @return
	 */
	public String filterClients(String index){
		String insName= "" ;
		StringBuffer stringBuffer = new StringBuffer();
		for(Crm c :crmList){
			if(c.getId().equals(index)){
				stringBuffer.append(" (");
				insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName());
				if(!"".equals(CommonUtils.getValidString(c.getCompany())) && CommonConstants.FALSE.equals(c.getType())){
					//insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName())+" ("+getCompanyName(c.getCompany())+")";
					insName = insName+" ("+getCompanyName(c.getCompany())+")";
				}
				stringBuffer.append(insName);
				stringBuffer.append(" )");
			break;	
			}
		}
		return stringBuffer.toString();
	}
	
	
	private String getCompanyName(String id){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(id)){
				insName = c.getFirstName();
			break;	
			}
		}
		return insName;
	}
	
	
	public String filterFacilityName(String index){
		String facilityName = null;
		facilityName = facilitiesList.stream().filter(facilities -> facilities.getId().equals(index)).map(i -> i.getName()+filterClients(i.getClientId()))					
				.findAny().orElse(null);
		return facilityName;
	}
	
	
	/**
	 * To create new dialog when user click for new task creation
	 */
	public void setCreateTaskDialog(){
		addSessionObj( TaskPlannerConstants.NEW_TASK_DIALOG , CommonConstants.TRUE);
	}
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_TASK_ID );
		String createNewTask = (String)model.get( TaskPlannerConstants.NEW_TASK_DIALOG);
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_TASK_ID );
		if( createNewTask != null ) model.remove( TaskPlannerConstants.NEW_TASK_DIALOG );
		if( toggleList != null ) model.remove( TaskPlannerConstants.TOGGLER_LIST );
		
	}
	
	/*public void qmsFileUploadListener(String value){
		if(documentList!=null){
		   uploadedFileList = documentList.stream().filter(doc->doc.getId().equals(value)).map(Documents::getName).collect(Collectors.toList());
		}
	}*/
	
	/*public String filteredParticipantName(String identifier){
		String participantName = null;
		for(Participant participant:participantList){
			if(participant.getIdentifier().equals(identifier)){
				participantName = participant.getFirstName()+" "+participant.getSurName();
			}
		}
		return participantName;
	}*/
	
	public String filteredCrmName(String identifier){
		String crmName = null;
		for(Crm crm:crmList){
			if(crm.getIdentifier().equals(identifier)){
				crmName = crm.getFirstName();
			}
		}
		return crmName;
	}
	
	/*public String filteredCourseId(String identifier){
		String courseId = null;
		for(Course course:courseList){
			if(course.getIdentifier().equals(identifier)){
				courseId = course.getCourseId();
			}
		}
		return courseId;
	}*/
	
	public String filteredType(String id){
		String typeId = null;
		for(TaskType type:typeList){
			if(type.getId().equals(id)){
				typeId = type.getType();
			}
		}
		return typeId;
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}

	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public List<Task> getFilteredTaskList() {
		return filteredTaskList;
	}

	public void setFilteredTaskList(List<Task> filteredTaskList) {
		this.filteredTaskList = filteredTaskList;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

	public List<Authorization> getAuthorizationList() {
		return authorizationList;
	}

	public void setAuthorizationList(List<Authorization> authorizationList) {
		this.authorizationList = authorizationList;
	}

	public List<TaskCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<TaskCategory> categoryList) {
		this.categoryList = categoryList;
	}
	public List<MultiVal> getStatusList() {
		return statusList;
	}
	public void setStatusList(List<MultiVal> statusList) {
		this.statusList = statusList;
	}
	public List<Project> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public Boolean getCreateNewTask() {
		return createNewTask;
	}

	public void setCreateNewTask(Boolean createNewTask) {
		this.createNewTask = createNewTask;
	}

	public List<String> getCatList() {
		return catList;
	}

	public void setCatList(List<String> catList) {
		this.catList = catList;
	}

	public List<String> getProjList() {
		return projList;
	}

	public void setProjList(List<String> projList) {
		this.projList = projList;
	}

	public List<String> getStatList() {
		return statList;
	}

	public void setStatList(List<String> statList) {
		this.statList = statList;
	}

	public List<MultiVal> getPriorityList() {
		return priorityList;
	}

	public void setPriorityList(List<MultiVal> priorityList) {
		this.priorityList = priorityList;
	}
	public List<Boolean> getTogglerList() {
		return togglerList;
	}
	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	public List<TaskType> getTypeList() {
		return typeList;
	}
	public void setTypeList(List<TaskType> typeList) {
		this.typeList = typeList;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public User getLoggedUser() {
		return loggedUser;
	}
	public void setLoggedUser(User loggedUser) {
		this.loggedUser = loggedUser;
	}
	public String getLoggedUserId() {
		return loggedUserId;
	}
	public void setLoggedUserId(String loggedUserId) {
		this.loggedUserId = loggedUserId;
	}
	public List<Task> getTaskStatusHistoryList() {
		return taskStatusHistoryList;
	}
	public void setTaskStatusHistoryList(List<Task> taskStatusHistoryList) {
		this.taskStatusHistoryList = taskStatusHistoryList;
	}
	public List<Task> getWatchList() {
		return watchList;
	}
	public void setWatchList(List<Task> watchList) {
		this.watchList = watchList;
	}
	
	public Boolean getShowWatchList() {
		return showWatchList;
	}
	public void setShowWatchList(Boolean showWatchList) {
		this.showWatchList = showWatchList;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<User> getDeputyList() {
		return deputyList;
	}
	public void setDeputyList(List<User> deputyList) {
		this.deputyList = deputyList;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public User getDeputyUser() {
		return deputyUser;
	}
	public void setDeputyUser(User deputyUser) {
		this.deputyUser = deputyUser;
	}
	public List<Task> getFilteredTaskStatusHistoryList() {
		return filteredTaskStatusHistoryList;
	}
	public void setFilteredTaskStatusHistoryList(List<Task> filteredTaskStatusHistoryList) {
		this.filteredTaskStatusHistoryList = filteredTaskStatusHistoryList;
	}
	/*public List<Documents> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<Documents> documentList) {
		this.documentList = documentList;
	}*/
	
	public String getDefaultView() {
		return defaultView;
	}
	public void setDefaultView(String defaultView) {
		this.defaultView = defaultView;
	}
	public Boolean getIsDetailView() {
		return isDetailView;
	}
	public void setIsDetailView(Boolean isDetailView) {
		this.isDetailView = isDetailView;
	}
	public Boolean getIsListView() {
		return isListView;
	}
	public void setIsListView(Boolean isListView) {
		this.isListView = isListView;
	}
	public String getChangeView() {
		return changeView;
	}
	public void setChangeView(String changeView) {
		this.changeView = changeView;
	}
	public List<String> getUploadedFileList() {
		return uploadedFileList;
	}
	public void setUploadedFileList(List<String> uploadedFileList) {
		this.uploadedFileList = uploadedFileList;
	}
	public List<Crm> getCrmList() {
		return crmList;
	}
	public void setCrmList(List<Crm> crmList) {
		this.crmList = crmList;
	}
	public List<String> getCrmNameList() {
		return crmNameList;
	}
	public void setCrmNameList(List<String> crmNameList) {
		this.crmNameList = crmNameList;
	}
	/*public List<Participant> getParticipantList() {
		return participantList;
	}
	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}
	public List<String> getParticipantNameList() {
		return participantNameList;
	}
	public void setParticipantNameList(List<String> participantNameList) {
		this.participantNameList = participantNameList;
	}
	/*public List<Course> getCourseList() {
		return courseList;
	}
	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}*/
	public List<String> getCourseNameList() {
		return courseNameList;
	}
	public void setCourseNameList(List<String> courseNameList) {
		this.courseNameList = courseNameList;
	}
	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}
	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}
	public Map<String, String> getEstimatedWorkHoursList() {
		return estimatedWorkHoursList;
	}
	public void setEstimatedWorkHoursList(Map<String, String> estimatedWorkHoursList) {
		this.estimatedWorkHoursList = estimatedWorkHoursList;
	}
	public List<Task> getSelectedTaskList() {
		return selectedTaskList;
	}
	public void setSelectedTaskList(List<Task> selectedTaskList) {
		this.selectedTaskList = selectedTaskList;
	}
	
			
}
