package com.orb.taskplanner.task.controller;

/**
 * @author Kamala Kannan Chellapandi
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.Crm;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.controller.AbstractTaskPlannerController;
import com.orb.taskplanner.task.validator.TaskCreateMandatoryValidator;

@ManagedBean(name="taskPlannerAddUpdateView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
@SessionAttributes({TaskPlannerConstants.NEW_TASK_DIALOG })
public class TaskPlannerAddUpdateManagedBean extends AbstractTaskPlannerController{
	
	private static Log logger = LogFactory.getLog(TaskPlannerAddUpdateManagedBean.class);
	private Task task = new Task();
	private UploadedFile uploadedFile = null;
	private String validateCheck = CommonConstants.TRUE;
	private List<Task> taskList = null;
	private List<TaskType> typeList = null;
	private List<Authorization> authorizationList = null ;
	private List<Project> projectList = null;
	private List<MultiVal> statusList = null;
	private List<MultiVal> priorityList = null;
	private List<TaskCategory> categoryList = null;
	private List<User> userList = null;
	private List<User> deputyList = null;
	private List<String> uploadedFileList = new ArrayList<>();;
	private User loginUser = null;
	private User deputyUser = null;
	//private List<Documents> documentList = null;
	private String cloneTask = CommonConstants.FALSE;
	private String defaultView = null;
	private List<Crm> crmList = null;
	//private List<Participant> participantList = null;
	/*private List<Course> courseList = null;*/
	private List<Facilities> facilitiesList = null;
	private Map<String, String> estimatedWorkHoursList = null;
		
	/**
	 * This methods is called when particular is called for updating.
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/editTaskPlanner")
	 public String editTaskPlanner(ModelMap model, HttpServletRequest request){
		clearSessionObjects(model);
		String taskId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		defaultView = CommonUtils.getValidString( request.getParameter( TaskPlannerConstants.DEFAULT_VIEW ) );
		model.addAttribute(TaskPlannerConstants.DEFAULT_VIEW, defaultView);
		model.addAttribute( TaskPlannerConstants.SELECTED_TASK_ID, taskId );
		return TaskPlannerConstants.TASK_EDIT_HTML;
   }
	
	
	/**
	 * This methods is called when particular is called for cloning the task.
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("/cloneTaskPlanner")
	 public String cloneTaskPlanner(ModelMap model, HttpServletRequest request){
		clearSessionObjects(model);
		String cloneId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( TaskPlannerConstants.SELECTED_CLONE_TASK_ID, cloneId );
		return TaskPlannerConstants.TASK_EDIT_HTML;
   }
	
	public void onLoadAction(){
		estimatedWorkHoursList = (Map<String,String>)FacilitiesConstants.ESTIMATED_WORK_HOURS;
		List<String> fileList = new ArrayList<>();
		uploadedFileList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		deputyUser = userProfileService.getLoginUser( loginUser.getUserId() );
		deputyList = getAllUsersList();
		taskList = taskPlannerService.getActiveTaskList(commonError);
		categoryList = taskPlannerService.getActiveCategoryList(commonError);
		typeList = taskPlannerService.getActiveTypeList(commonError);
		crmList = crmService.getActiveCrmList(commonError);
		facilitiesList=facilitiesService.getActiveFacilitiesList(commonError);
		//participantList = participantService.getActiveParticipantList(commonError);
		/*courseList = courseService.getActiveCourseList(commonError);
		courseList = courseList.stream().filter(c -> !(CommonUtils.getValidString(c.getCourseId()).isEmpty() && c.getCourseId() == null) ).collect(Collectors.toList());*/
		statusList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_STATUS );
		priorityList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FIELD_PRIORITY );
		projectList = taskPlannerService.getActiveProjectList(commonError);
		userList = getAllUsersList();
		authorizationList = taskPlannerService.getActiveAuthorizationList(commonError);
		String taskId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_TASK_ID );
		String cloneId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_CLONE_TASK_ID );
		defaultView = CommonUtils.getValidString(getJSFRequestAttribute( TaskPlannerConstants.DEFAULT_VIEW ) );
		logger.info(defaultView);
		if(cloneId!=null){
			cloneTask = CommonConstants.TRUE;	
			task = taskPlannerService.getTask( commonError, cloneId );
		}else{
			cloneTask = CommonConstants.FALSE;
			task = taskPlannerService.getTask( commonError, taskId );
		}
		/*documentList = qmsService.getActiveDocumentList(commonError, loginUser);
		if(documentList != null){
			documentList = (List<Documents>) documentList.stream().filter(p -> !"".equals(p.getUploadedDoc())).collect(Collectors.toList());
		}else
			documentList = new ArrayList<>();*/
		if(!"".equals(task.getAttachment())){
			fileList = new ArrayList<>(Arrays.asList(task.getAttachment().split(",")));
		}else{
			fileList = new ArrayList<>();
		}
		Set<String> hs = new HashSet<>();
		hs.addAll(fileList);
		fileList.clear();
		fileList.addAll(hs);
		task.setAttachmentList(fileList);
		uploadedFileList.addAll(fileList);
    }
	
	/**
	 * This method will add / update a Task to DB
	 * @param event
	 */
	public void saveAction() throws InterruptedException, IOException,NullPointerException {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String url = request.getRequestURL().toString();
		String uri = request.getRequestURI();
		logger.info(url);
		logger.info(uri);
		logger.info("Parent Task ->"+task.getParentTask());
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		deputyUser = userProfileService.getLoginUser( loginUser.getUserId() );
		/*documentList = qmsService.getActiveDocumentList(commonError, loginUser);*/
		if( !"".equals(deputyUser.getDeputy()) && "".equals(CommonUtils.getValidString(task.getId())) && task.getDeputy() == null){ 
		   task.setDeputy(deputyUser.getDeputy());
		}
		TaskCreateMandatoryValidator taskCreateMandatoryValidator = new TaskCreateMandatoryValidator();
		validateCheck = taskCreateMandatoryValidator.fieldValidator(task);
		if( validateCheck.equals(CommonConstants.TRUE) && commonError.getError().isEmpty()){
			if( "".equals(CommonUtils.getValidString(task.getId())) ){
				projectList = taskPlannerService.getActiveProjectList(commonError);
				String nextTaskId = taskPlannerService.genNextTaskId( commonError, task.getProject() );
			    task.setIdentifier(nextTaskId);
			    task.setStatus("notYet");
			    if(task.getReporter() == null){
				  task.setReporter(loginUser.getUserId());
			    }
			    if(task.getPriority() ==  null){
				  task.setPriority(TaskPlannerConstants.PRIORITY);
			    }
			    if(task.getDeputy() == null){
				   task.setDeputy(deputyUser.getDeputy());
			    }
			    String fileNames = String.join(",", uploadedFileList);
			    task.setAttachment(fileNames);
			    taskPlannerService.addTask( commonError, loginUser, task );
		      }else{
			    String fileNames = String.join(",", uploadedFileList);
			    task.setAttachment(fileNames);
			    taskPlannerService.updateTask( commonError, loginUser, task );
		      }
		     if( commonError.getError().isEmpty() ){
			     String msg = "".equals(CommonUtils.getValidString(task.getId())) ?  TaskPlannerConstants.TASK_ADDED_SUCCESS : TaskPlannerConstants.TASK_UPDATED_SUCCESS;
			     if(uri.contains(TaskPlannerConstants.TASK_VIEW_HTML)){
			  	    String id;
				    taskList = taskPlannerService.getActiveTaskList(commonError);
				    id = taskList.stream().filter(parentTask -> task.getParentTask().equals(parentTask.getIdentifier())).map(Task::getId)					
				             .findAny().orElse(null);
				    logger.info(id);
			        customRedirector( TaskPlannerConstants.TASK_ADDED_SUCCESS , TaskPlannerConstants.TASK_VIEW_PAGE + "?objectId=" + id+"&view="+TaskPlannerConstants.TASK_LIST_VIEW);	
			      }else{
			        customRedirector( msg , TaskPlannerConstants.TASK_LIST_PAGE );
			      }
		       }
		  }else{
			  RequestContext.getCurrentInstance().execute("myTaskDialog.show()");
			  addErrorMessage(commonError);
			  Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
	    	  String value = bundle.getMessage("required_field", null, locale);
			  FacesMessage msg= new FacesMessage(FacesMessage.SEVERITY_ERROR, "", value);
			  FacesContext.getCurrentInstance().addMessage("dialogForm", msg);
		   }
	}
	
	public String cloneTask(String taskClone){
		cloneTask = taskClone;
		return cloneTask;
	}
	
	/**
	 * Return the estimated Work in String
	 */
	public String returnedEstimatedWork(String mins){
		String estimatedHour = null;
		for(Map.Entry<String, String> workHour : estimatedWorkHoursList.entrySet()){
			if(workHour.getValue().equals(mins)){
				estimatedHour = workHour.getKey();
			}
		}
		return CommonUtils.getValidString(estimatedHour);
	}
	
	public void fileUploadListener(FileUploadEvent e) throws IOException{
		 	UploadedFile uploadedPhoto=e.getFile();
			String fileName = new String(uploadedPhoto.getFileName().getBytes("iso-8859-1"),"UTF-8");
			String dirName = null; 
			String filePath = null;
			uploadedFileList.add(fileName);
		    Set<String> hs = new HashSet<>();
			hs.addAll(uploadedFileList);
			uploadedFileList.clear();
			uploadedFileList.addAll(hs);
			if(task.getIdentifier() != null){
				if(task.getDirName() != null){
						dirName = task.getDirName();
				}else{
					dirName = taskPlannerService.getNextDir(task.getId());
				}
			}else{
				dirName = taskPlannerService.getNextDir(null);
			}
			task.setDirName(dirName);
	        filePath = System.getProperty("upload.dir") + "taskplanner/"+dirName+"/";
	        File dir = new File(filePath);
	        if(!dir.exists())  dir.mkdir();
	        byte[] bytes=null;
	           if (null!=uploadedPhoto) {
	                bytes = uploadedPhoto.getContents();
	                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+fileName)));
	                stream.write(bytes);
	                stream.close();
	            }
	           /*if(null!=uploadedPhoto && CommonConstants.TRUE.equals(task.getQmsDoc())){
	            	documentList = qmsService.getActiveDocumentList(commonError, loginUser);
	        		for(Documents doc : documentList){
	        			if(doc.getUploadedDoc().equals(uploadedFile) ){
	        				uploadedFileList.add(doc.getUploadedDoc());
	        				File src = new File(System.getProperty("upload.dir") + "qms/"+ doc.getUploadedDoc());
	        				File dest = new File(System.getProperty("upload.dir") + "taskplanner/");
	        				FileUtils.copyFileToDirectory(src, dest);
	        			}
	        		}
	           }*/
	    }
	
	 	/**
		 * This method helps to download the Document
		 * @param model
		 */
		public void downloadFile(String dirName,String fileVersion) throws IOException{
			String path = System.getProperty("upload.dir") + "taskplanner/" + dirName +"/"+ fileVersion.replaceAll("^\\s+", "");;
			File downFile = new File(path);
			Path filePath = Paths.get(path);
			if(Files.exists(filePath) && !"".equals(fileVersion)){
				String fileName = downFile.getName();
				InputStream fis = new FileInputStream(downFile);
				byte[] buf = new byte[fis.available()];
				int offset = 0;
				int numRead = 0;
				while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length -offset)) >= 0)) 
					offset += numRead;
				fis.close();
				HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
				String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
				response.setContentType(contentType);
				response.setHeader( "Content-Disposition", "attachment;filename=\"" + fileName + "\"" );
				response.getOutputStream().write(buf);
				response.getOutputStream().flush();
				response.getOutputStream().close();
				FacesContext.getCurrentInstance().responseComplete();
				commonError.clearErrors();
			}else{
				commonError.addError("err_docNotFound");
			}			
		}
	 
	
		/**
		 * Delete the file which is selected
		 */
		public void deleteFile(String dirName, String fileVersion) throws IOException{
			String path = System.getProperty("upload.dir") + "taskplanner/" + dirName +"/"+ fileVersion.replaceAll("^\\s+", "");
			File f = new File(path);
			if(f.exists()){
				f.delete();
				uploadedFileList.remove(fileVersion);
			}else{
			   commonError.addError("err_docNotFound");
		    }	
		}
		
		/**
		 * To clear faces messages when dialgo closes
		 * 
		 */
		public void clearDialogErrors(){
			RequestContext.getCurrentInstance().reset("dialogForm:panelDialog");
			RequestContext.getCurrentInstance().update("dialogForm:panelDialog");
			FacesContext context = FacesContext.getCurrentInstance();
			Iterator<FacesMessage> it = context.getMessages();
			while ( it.hasNext() ) {
			    it.next();
			    it.remove();
			}
			if(commonError != null)
				commonError.clearErrors();
			validateCheck = CommonConstants.TRUE;
			if(task.getParentTask() == null){
			    RequestContext.getCurrentInstance().execute("myTaskDialog.hide()");
			}else{
				RequestContext.getCurrentInstance().execute("mySubTaskDialog.hide()");
			}
		}
		
	 
	/**
	 * This method to returns to main Task list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		setTaskList(taskPlannerService.getActiveTaskList(commonError));
		task = getTaskList().get(0);
		addSessionObj( TaskPlannerConstants.SELECTED_TASK_ID, task.getId() );
		addSessionObj(TaskPlannerConstants.NEW_TASK_DIALOG ,"false");
		if(defaultView.equals(TaskPlannerConstants.TASK_DETAIL_VIEW) || defaultView.isEmpty() ){
		redirector( TaskPlannerConstants.TASK_LIST_PAGE );
		}else{
		redirector(TaskPlannerConstants.TASK_VIEW_PAGE + "?objectId=" + task.getId()+"&view="+defaultView);	
		}
	}
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_TASK_ID );
		String createNewTask = (String)model.get( TaskPlannerConstants.NEW_TASK_DIALOG);
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_TASK_ID );
		if( createNewTask != null ) model.remove( TaskPlannerConstants.NEW_TASK_DIALOG );
	}
	
	public void onChangePriorityEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		task.setPriority(newValue);
	}
	
	public void onChangeReporterEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		validateCheck = CommonConstants.TRUE;
		task.setReporter(newValue);
	}
	
	public void onChangeDeputyEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		validateCheck = CommonConstants.TRUE;
		task.setDeputy(newValue);
	}
	
	public void onChangeCrmEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		validateCheck = CommonConstants.TRUE;
		task.setCrmName(newValue);
	}
	
	public void onChangeParticipantEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		validateCheck = CommonConstants.TRUE;
		task.setParticipantName(newValue);
	}
	
	/*public void onChangeCourseEvent(ValueChangeEvent event) {
		String newValue = ( String )event.getNewValue();
		validateCheck = CommonConstants.TRUE;
		task.setCourseId(newValue);
	}*/
	public String getUserName(String userId){
		User requiredUser = getUserById(commonError, userId);
		return requiredUser.getFirstName()+ " " + requiredUser.getLastName();
	}
	

	/*public void qmsFileUploadListener(String file){
		String dirName = null; 
		String filePath = null;
		if(task.getIdentifier() != null){
			if(task.getDirName() != null){
					dirName = task.getDirName();
			}else{
				dirName = taskPlannerService.getNextDir(task.getId());
			}
		}
		else{
			dirName = taskPlannerService.getNextDir(null);
		}
		task.setDirName(dirName);
        filePath = System.getProperty("upload.dir") + "taskplanner/"+dirName+"/";
        File dir = new File(filePath);
        if(!dir.exists()){
        	dir.mkdir();
        }
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		documentList = qmsService.getActiveDocumentList(commonError, loginUser);
		for(Documents doc : documentList){
			if(doc.getUploadedDoc().equals(file) ){
				uploadedFileList.add(doc.getUploadedDoc());
				File src = new File(System.getProperty("upload.dir") + "qms/"+ doc.getUploadedDoc());
				File dest = new File(filePath);
			try{
				FileUtils.copyFileToDirectory(src, dest);
			}catch(IOException e){
				commonError.addError("err_docNotFound");
				return;
				}
			}
		 }
     }*/
	
	
	public void cloneAction(){
		projectList = taskPlannerService.getActiveProjectList(commonError);
		categoryList = taskPlannerService.getActiveCategoryList(commonError);
		task.setProject(projectList.stream().filter(proj -> proj.getId().equals(task.getProject())).map(Project::getId)					
				.findAny().orElse(null));
		task.setCategory(categoryList.stream().filter(cat -> cat.getId().equals(task.getCategory())).map(TaskCategory::getId)					
				.findAny().orElse(null));
		String nextTaskId = taskPlannerService.genNextTaskId( commonError, task.getProject() );
		task.setIdentifier(nextTaskId);
		taskPlannerService.cloneTask( commonError, loginUser, task );
		customRedirector(TaskPlannerConstants.CLONE_ADDED_SUCCESS,TaskPlannerConstants.TASK_LIST_PAGE );
	}
	
	/**
	 * This method will navigate to the task create page
	 * @param index
	 */
	public void subTaskAction(String identifier){
		task.setParentTask(identifier);
	}
	
	public void normalTaskAction(){
		task.setParentTask(null);
	}
	
	
	/**
	 * Filter company name based on company ID
	 * @param index
	 * @return
	 */
	public String filterClients(String index){
		String insName= "" ;
		StringBuffer stringBuffer = new StringBuffer();
		for(Crm c :crmList){
			if(c.getId().equals(index)){
				stringBuffer.append(" (");
				insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName());
				if(!"".equals(CommonUtils.getValidString(c.getCompany())) && CommonConstants.FALSE.equals(c.getType())){
					//insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName())+" ("+getCompanyName(c.getCompany())+")";
					insName = insName+ " "+" ("+getCompanyName(c.getCompany())+")";
				}
				stringBuffer.append(insName);
				stringBuffer.append(" )");
			break;	
			}
		}
		return stringBuffer.toString();
	}
	
	
	private String getCompanyName(String id){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(id)){
				insName = c.getFirstName();
			break;	
			}
		}
		return insName;
	}


	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public List<Authorization> getAuthorizationList() {
		return authorizationList;
	}

	public void setAuthorizationList(List<Authorization> authorizationList) {
		this.authorizationList = authorizationList;
	}

	public List<TaskCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<TaskCategory> categoryList) {
		this.categoryList = categoryList;
	}
	public List<MultiVal> getStatusList() {
		return statusList;
	}
	public void setStatusList(List<MultiVal> statusList) {
		this.statusList = statusList;
	}
	public List<Project> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}
	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getValidateCheck() {
		return validateCheck;
	}

	public void setValidateCheck(String validateCheck) {
		this.validateCheck = validateCheck;
	}

	public List<MultiVal> getPriorityList() {
		return priorityList;
	}

	public void setPriorityList(List<MultiVal> priorityList) {
		this.priorityList = priorityList;
	}

	public List<TaskType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<TaskType> typeList) {
		this.typeList = typeList;
	}

	public User getDeputyUser() {
		return deputyUser;
	}

	public void setDeputyUser(User deputyUser) {
		this.deputyUser = deputyUser;
	}

	public List<User> getDeputyList() {
		return deputyList;
	}

	public void setDeputyList(List<User> deputyList) {
		this.deputyList = deputyList;
	}

	/*public List<Documents> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Documents> documentList) {
		this.documentList = documentList;
	}*/
	
	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getCloneTask() {
		return cloneTask;
	}

	public void setCloneTask(String cloneTask) {
		this.cloneTask = cloneTask;
	}


	public String getDefaultView() {
		return defaultView;
	}


	public void setDefaultView(String defaultView) {
		this.defaultView = defaultView;
	}


	public List<String> getUploadedFileList() {
		return uploadedFileList;
	}


	public void setUploadedFileList(List<String> uploadedFileList) {
		this.uploadedFileList = uploadedFileList;
	}


	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}


	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}


	public List<Crm> getCrmList() {
		return crmList;
	}


	public void setCrmList(List<Crm> crmList) {
		this.crmList = crmList;
	}


	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}


	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}


	public Map<String, String> getEstimatedWorkHoursList() {
		return estimatedWorkHoursList;
	}


	public void setEstimatedWorkHoursList(Map<String, String> estimatedWorkHoursList) {
		this.estimatedWorkHoursList = estimatedWorkHoursList;
	}


	/*public List<Participant> getParticipantList() {
		return participantList;
	}


	public void setParticipantList(List<Participant> participantList) {
		this.participantList = participantList;
	}*/


	/*public List<Course> getCourseList() {
		return courseList;
	}


	public void setCourseList(List<Course> courseList) {
		this.courseList = courseList;
	}*/
	
	
}
