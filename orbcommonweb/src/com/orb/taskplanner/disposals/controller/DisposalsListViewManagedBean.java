package com.orb.taskplanner.disposals.controller;
/**
 * @author Manikandan Chithirangan
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Disposals;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.controller.AbstractTaskPlannerController;

import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.config.BeanDefinition;

@ManagedBean(name="disposalsView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class DisposalsListViewManagedBean extends AbstractTaskPlannerController{
	private static Log logger = LogFactory.getLog(DisposalsListViewManagedBean.class);
	
	private User loginUser=null;
	private List<Boolean> togglerList = null;
	private Disposals disposals = null;
	private List<Disposals> disposalsList = null;
	private List<Disposals> filteredDisposalsList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Disposals> selectedDisposalsList = null;
	private String disposalsId = null;
	private String showIdentifier = CommonConstants.TRUE;
	
	 /**
	 * This page will be called when user clicks the disposals link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadDisposals")
    public String loadDisposals(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return TaskPlannerConstants.DISPOSALS_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular Disposals from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewDisposals")
	public String viewDisposals(HttpServletRequest request, ModelMap model){
		 disposalsId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( TaskPlannerConstants.SELECTED_DISPOSALS_ID, disposalsId );
		 return TaskPlannerConstants.DISPOSALS_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		disposalsList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		disposalsId = CommonUtils.getValidString(getJSFRequestAttribute(TaskPlannerConstants.SELECTED_DISPOSALS_ID) );
		logger.info(" Disposals Id :" +disposalsId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			disposalsList=taskPlannerService.getInactiveDisposalsList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			disposalsList=taskPlannerService.getActiveDisposalsList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.DISPOSALS_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true, true);
		}
		if(!CommonUtils.getValidString(disposalsId).isEmpty()){
			disposals = taskPlannerService.getDisposals(commonError, disposalsId);
		}
		
	}
	
	
	/**
	 * This method will get all the active Disposals
	 * @param event
	 */
	public void showActive(ActionEvent event){
		disposalsList = taskPlannerService.getActiveDisposalsList( commonError );
		if( filteredDisposalsList != null ) filteredDisposalsList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Disposals
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		disposalsList = taskPlannerService.getInactiveDisposalsList(commonError);
		if( filteredDisposalsList != null ) filteredDisposalsList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks Disposals
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		taskPlannerService.lockDisposals( commonError, loginUser, selectedDisposalsList );
		showActive(e);
		customRedirector( TaskPlannerConstants.DISPOSALS_LOCKED_SUCCESS,TaskPlannerConstants.DISPOSALS_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Disposals
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		taskPlannerService.unlockDisposals( commonError, loginUser, selectedDisposalsList );
		showInactive(e);
		customRedirector( TaskPlannerConstants.DISPOSALS_UNLOCKED_SUCCESS, TaskPlannerConstants.DISPOSALS_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the Disposals
	 * @param event
	 */
	public void deleteAction(ActionEvent e){
		if(selectedDisposalsList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(TaskPlannerConstants.DISPOSALS_DELETE_SUCCESS);
		}else{	
			taskPlannerService.deleteDisposals( commonError, loginUser, selectedDisposalsList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( TaskPlannerConstants.DISPOSALS_DELETE_SUCCESS, TaskPlannerConstants.DISPOSALS_LIST_PAGE);
		    }
		    showInactive(e);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will navigate to the page where user can add a disposals
	 */
	public void addAction(){
		addSessionObj( TaskPlannerConstants.DISPOSALS_TOGGLER_LIST , togglerList );
		redirector(TaskPlannerConstants.DISPOSALS_EDIT_PAGE);
	}
	/**
	 * This method to returns to main disposals list page
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.DISPOSALS_LIST_PAGE +"?activeFlag="+disposals.getActive());
	}
	
	/**
	 * This method will navigate to disposals Edit page
	 * @param index
	 */
	public void editAction(String index){

		redirector(TaskPlannerConstants.DISPOSALS_EDIT_PAGE+"?objectId="+index);
	}
	
	
	/**
	 * This method will navigate to disposals View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( TaskPlannerConstants.DISPOSALS_TOGGLER_LIST , togglerList );
		redirector(TaskPlannerConstants.DISPOSALS_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed disposals
	 * @param index
	 */
	public void deleteViewedDisposals(String index){
		disposals = taskPlannerService.getDisposals(commonError, index);
		taskPlannerService.deleteViewedDisposals( commonError, loginUser, disposals);
		if( commonError.getError().isEmpty() ){
			customRedirector( TaskPlannerConstants.DISPOSALS_DELETE_SUCCESS, TaskPlannerConstants.DISPOSALS_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed disposals
	 */
	public void lockViewedDisposals(String index){
		disposals = taskPlannerService.getDisposals(commonError, index);
		taskPlannerService.lockViewedDisposals( commonError, loginUser, disposals );
		customRedirector( TaskPlannerConstants.DISPOSALS_LOCKED_SUCCESS, TaskPlannerConstants.DISPOSALS_LIST_PAGE);
	}

	/**
	 * This method locks viewed disposals
	 * @param index
	 * @return
	 */
	public void unlockViewedDisposals(String index){
		disposals = taskPlannerService.getDisposals(commonError, index);
		taskPlannerService.unlockViewedDisposals( commonError, loginUser, disposals );
		if( commonError.getError().isEmpty() ){
			customRedirector( TaskPlannerConstants.DISPOSALS_UNLOCKED_SUCCESS, TaskPlannerConstants.DISPOSALS_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( TaskPlannerConstants.SELECTED_DISPOSALS_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_DISPOSALS_ID );
		if( toggleList != null ) model.remove( TaskPlannerConstants.DISPOSALS_TOGGLER_LIST );		
    }
	
	
	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	public List<Boolean> getTogglerList() {
		return togglerList;
	}
	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	public Disposals getDisposals() {
		return disposals;
	}
	public void setDisposals(Disposals disposals) {
		this.disposals = disposals;
	}
	public List<Disposals> getDisposalsList() {
		return disposalsList;
	}
	public void setDisposalsList(List<Disposals> disposalsList) {
		this.disposalsList = disposalsList;
	}
	public List<Disposals> getFilteredDisposalsList() {
		return filteredDisposalsList;
	}
	public void setFilteredDisposalsList(List<Disposals> filteredDisposalsList) {
		this.filteredDisposalsList = filteredDisposalsList;
	}
	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}
	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	public String getShowActivateBtn() {
		return showActivateBtn;
	}
	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}
	public List<Disposals> getSelectedDisposalsList() {
		return selectedDisposalsList;
	}
	public void setSelectedDisposalsList(List<Disposals> selectedDisposalsList) {
		this.selectedDisposalsList = selectedDisposalsList;
	}
	public String getDisposalsId() {
		return disposalsId;
	}
	public void setDisposalsId(String disposalsId) {
		this.disposalsId = disposalsId;
	}
	public String getShowIdentifier() {
		return showIdentifier;
	}
	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}
	 
	 

}
