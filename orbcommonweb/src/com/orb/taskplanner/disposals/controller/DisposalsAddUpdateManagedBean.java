package com.orb.taskplanner.disposals.controller;
/**
 * @author Manikandan Chithirangan
 * 
 */

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Disposals;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.controller.AbstractTaskPlannerController;

@ManagedBean(name="disposalsAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class DisposalsAddUpdateManagedBean extends AbstractTaskPlannerController{
	private static Log logger = LogFactory.getLog(DisposalsAddUpdateManagedBean.class);
	
	private Disposals disposals =null;
	private List<Disposals> disposalsList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String disposalsId = null;
	private User loginUser = null;
	
	
	/**
	 * This method will be called when user clicks Add button / disposals Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateDisposals")
	public String manipulateDisposals( HttpServletRequest request, ModelMap model){
		disposalsId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( TaskPlannerConstants.SELECTED_DISPOSALS_ID, disposalsId );
		return TaskPlannerConstants.DISPOSALS_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		disposalsId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_DISPOSALS_ID );
		logger.info(" Disposals Id :" +disposalsId);
		if(CommonUtils.getValidString(disposalsId).isEmpty()){
			disposals = new Disposals();
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			disposals=taskPlannerService.getDisposals(commonError, disposalsId);
		 }
	}

	/**
	 * This method will add / update a Disposals in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(CommonUtils.getValidString(disposals.getId()).isEmpty()){
			taskPlannerService.add( commonError, loginUser, disposals );
		}else{
			taskPlannerService.update( commonError, loginUser, disposals );
		}
		disposalsList=taskPlannerService.getActiveDisposalsList(commonError);
		String msg=CommonUtils.getValidString(disposals.getId()).isEmpty() ? TaskPlannerConstants.DISPOSALS_ADDED_SUCCESS : TaskPlannerConstants.DISPOSALS_UPDATED_SUCCESS ;
		customRedirector( msg, TaskPlannerConstants.DISPOSALS_LIST_PAGE);		
	}

	/**
	 * This method to returns to disposals view page
	 * @return 
	 */
	public void cancelAction(){
		if(disposals.getIdentifier()==null){
			redirector(TaskPlannerConstants.DISPOSALS_LIST_PAGE);
		}else{
			redirector(TaskPlannerConstants.DISPOSALS_VIEW_PAGE+"?objectId="+disposals.getIdentifier());
		}
	}

	public Disposals getDisposals() {
		return disposals;
	}


	public void setDisposals(Disposals disposals) {
		this.disposals = disposals;
	}


	public List<Disposals> getDisposalsList() {
		return disposalsList;
	}


	public void setDisposalsList(List<Disposals> disposalsList) {
		this.disposalsList = disposalsList;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


	public String getDisposalsId() {
		return disposalsId;
	}


	public void setDisposalsId(String disposalsId) {
		this.disposalsId = disposalsId;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


}
