package com.orb.taskplanner.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.constants.TaskPlannerConstants;

@ManagedBean(name="authorizationView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class AuthorizationLevelManagedBean extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(AuthorizationLevelManagedBean.class);
	private Authorization authorization = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Authorization> authorizationList = null;
	private List<Authorization> filteredAuthorizationList = null;
	private List<Division> divisionList = null;
	private List<Authorization> selectedDocs=null;
	private List<String> selectedUsers = null;
	private String confId=null;
	private List<User> userList=null;
	private List<Boolean> togglerList = null;
	private User loginUser = null;
	
	/**
	 * This Method will be called to view the authorization list page when use clicks authorization link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadAuthorizationLevel")
    public String loadAuthorizationLevel(ModelMap model){
    	return TaskPlannerConstants.AUTHORIZATION_LIST_HTML;
    }

	/**
	 * This method will be called when user clicks particular authorization from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewAuthorization")
	public String viewAuthorization(HttpServletRequest request, ModelMap model)
	{
		confId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		model.addAttribute(TaskPlannerConstants.SELECTED_AUTHORIZATION_ID,confId);
		return TaskPlannerConstants.AUTHORIZATION_VIEW_HTML;
	}
	
	/**
	 * This method will be called when user tries to Add/Update authorization
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUpdateAuthorization")
	public String addUpdateAuthorization(HttpServletRequest request, ModelMap model)
	{
		confId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		if("".equals(confId))
			model.addAttribute(TaskPlannerConstants.SELECTED_AUTHORIZATION_ID,"-1");
		else
			model.addAttribute(TaskPlannerConstants.SELECTED_AUTHORIZATION_ID,confId);
		return TaskPlannerConstants.AUTHORIZATION_EDIT_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		userList = getAllUsersList();
		selectedUsers = new ArrayList<String>();
		
		authorizationList = taskPlannerService.getActiveAuthorizationList(commonError);
		divisionList = taskPlannerService.getActiveDivisionList(commonError);
		confId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_AUTHORIZATION_ID );
		logger.info(confId);
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.AUTHORIZATION_LIST_PAGE) != null ? (List<Boolean>)getSessionObj(TaskPlannerConstants.AUTHORIZATION_LIST_PAGE) : Arrays.asList(false, true, true,true);
		if( confId == null || "".equals(confId) ) return;
		if( "-1".equals(confId) ){
			authorization = new Authorization();
		}
		else{
			authorization = taskPlannerService.getAuthorization(commonError, confId);
			List<String> tempUserList = new ArrayList<String>();
			selectedUsers = Arrays.asList(authorization.getAuthUser().split(","));
			if( authorization == null ) authorization = new Authorization();
		}
	}
			
	/**
	 * This method will get all the active Authorizations of documents
	 * @param event
	 */
	public void showActive(ActionEvent event){
		setAuthorizationList(taskPlannerService.getActiveAuthorizationList( commonError ));
		if( filteredAuthorizationList != null ) filteredAuthorizationList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive Authorizations of documents
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		setAuthorizationList(taskPlannerService.getInactiveAuthorizationList( commonError));
		if( filteredAuthorizationList != null ) filteredAuthorizationList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}

	/**
	 * This method to open new Authorization add page
	 * @param add page
	 * @return 
	 */
	public void addAction(){
		redirector(TaskPlannerConstants.AUTHORIZATION_EDIT_PAGE);
	}

	/**
	 * This method will add / update a ConfidentialityLevel in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		
		String fileNames = String.join(",", selectedUsers);
		authorization.setAuthUser(fileNames);
		if( "".equals(CommonUtils.getValidString(authorization.getId())) ){
			
		//String fileNames = selectedUsers.replace("[", "");
			//fileNames = fileNames.replace("]", "");
			taskPlannerService.addAuthorization( commonError, loginUser, authorization);
		}else{
			taskPlannerService.updateAuthorization( commonError, loginUser, authorization);
		}
		setAuthorizationList(taskPlannerService.getActiveAuthorizationList( commonError));
		if( commonError.getError().isEmpty() ){
			String msg = "".equals(CommonUtils.getValidString(authorization.getId())) ? "confidentiality_added_success" : "confidentiality_update_success";
			customRedirector( msg , TaskPlannerConstants.AUTHORIZATION_LIST_PAGE );
		}
	}
	
	

	/**
	 * This method will navigate to the View page
	 * @param index
	 * @return
	 */
	public void viewAction(String index)
	{
		authorization = taskPlannerService.getAuthorization(commonError, index);
		addSessionObj( TaskPlannerConstants.AUTHORIZATION_LIST_PAGE , togglerList );
		redirector(TaskPlannerConstants.AUTHORIZATION_VIEW_PAGE+"?objectId="+authorization.getId());
	}
	
	/**
	 * This method will navigate to the Edit page
	 * @param index
	 * @return
	 */
	public void editAction( String index ){
		authorization = taskPlannerService.getAuthorization(commonError, index);
		redirector(TaskPlannerConstants.AUTHORIZATION_EDIT_PAGE+"?objectId="+authorization.getId());
	}	
	
	/**
	 * This method to returns to main Authorization list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.AUTHORIZATION_LIST_PAGE);
	}
	
	/**
	 * This method deletes Authorizations
	 * @param list page
	 * @return 
	 */
	public void deleteAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.deleteAuthorization( commonError, loginUser, selectedDocs );
		if( commonError.getError().isEmpty() ){
			customRedirector( "confidentiality_delete_success", TaskPlannerConstants.AUTHORIZATION_LIST_PAGE);
		}
		showInactive(e);
		
	}
	/**
	 * This method locks Authorizations
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.lockAuthorization( commonError, loginUser, selectedDocs );
		showActive(e);
	}
	
	/**
	 * This method unlocks Authorizations
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.unlockAuthorization( commonError, loginUser, selectedDocs );
		showInactive(e);
	}
	
	/**
	 * This method locks viewed authorization
	 * @param list page
	 * @return
	 */
	public void lockViewedAuthorization(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		authorization = taskPlannerService.getAuthorization(commonError, index);
		taskPlannerService.lockViewedAuthorization( commonError, loginUser, authorization );
		customRedirector( "confidentiality_locked_success", TaskPlannerConstants.AUTHORIZATION_LIST_PAGE);
	}

	/**
	 * This method locks viewed authorization
	 * @param list page
	 * @return
	 */
	public void unlockViewedAuthorization(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		authorization = taskPlannerService.getAuthorization(commonError, index);
		taskPlannerService.unlockViewedAuthorization( commonError, loginUser, authorization );
		if( commonError.getError().isEmpty() ){
			customRedirector( "confidentiality_unlocked_success", TaskPlannerConstants.AUTHORIZATION_LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed authorization
	 * @param index
	 */
	public void deleteViewedAuthorization(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		authorization = taskPlannerService.getAuthorization(commonError, index);
		taskPlannerService.deleteViewedAuthorization( commonError, loginUser, authorization );
		if( commonError.getError().isEmpty() ){
			customRedirector( "authorization_delete_success", TaskPlannerConstants.AUTHORIZATION_LIST_PAGE );
			
			
		}
	}
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	public List<String> completeUser(String query){
		query = query.toLowerCase();
		List<String> results = new ArrayList<String>();
		for(User u:getAllUsersList()){
			if(u.getFirstName().toLowerCase().contains(query) || u.getLastName().toLowerCase().contains(query))
				results.add(u.getFirstName()+' '+u.getLastName());
		}
		return results;
	}
	
	
	
	

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}

	
	public List<Boolean> getTogglerList() {
		return togglerList;
	}
	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	

	public Authorization getAuthorization() {
		return authorization;
	}

	public void setAuthorization(Authorization authorization) {
		this.authorization = authorization;
	}

	public List<Authorization> getfilteredAuthorizationList() {
		return filteredAuthorizationList;
	}

	public void setfilteredAuthorizationList(List<Authorization> filteredAuthorizationList) {
		this.filteredAuthorizationList = filteredAuthorizationList;
	}

	

	public List<Authorization> getSelectedDocs() {
		return selectedDocs;
	}

	public void setSelectedDocs(List<Authorization> selectedDocs) {
		this.selectedDocs = selectedDocs;
	}

	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		AuthorizationLevelManagedBean.logger = logger;
	}

	public List<Authorization> getAuthorizationList() {
		return authorizationList;
	}

	public void setAuthorizationList(List<Authorization> authorizationList) {
		this.authorizationList = authorizationList;
	}

	public List<Division> getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(List<Division> divisionList) {
		this.divisionList = divisionList;
	}
	public String getShowActivateBtn() {
		return showActivateBtn;
	}
	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	
	public String getConfId() {
		return confId;
	}

	public void setConfId(String confId) {
		this.confId = confId;
	}

	public List<String> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<String> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	

	
}
