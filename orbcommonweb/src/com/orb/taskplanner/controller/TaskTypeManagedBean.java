package com.orb.taskplanner.controller;

import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.constants.TaskPlannerConstants;

@ManagedBean(name="taskTypeView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class TaskTypeManagedBean  extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(TaskTypeManagedBean.class);
	private TaskType type = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<TaskType> typeList = null;
	private List<TaskType> filteredTaskTypeList = null;
	private List<TaskType> selectedTypes=null;
	private String typeId=null;
	private List<Boolean> togglerList = null;
 
	/**
	 * This Method will be called to view the type list page when use clicks type link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadTaskType")
	public String loadTaskType(ModelMap model){
		clearSessionObjects(model);
		return TaskPlannerConstants.TYPE_LIST_HTML;
	}
	
	/**
	 * This method will be called when user clicks particular type from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewTaskType")
	public String viewTaskType(HttpServletRequest request, ModelMap model)
	{
		String typeId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		model.addAttribute(TaskPlannerConstants.SELECTED_TYPE_ID,typeId);
		return TaskPlannerConstants.TYPE_VIEW_HTML;
	}
	
	/**
	 * This method will be called when user tries to Add/Update type
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUpdateTaskType")
	public String addUpdateTaskType(HttpServletRequest request, ModelMap model)
	{
		String typeId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		if("".equals(typeId))
			model.addAttribute(TaskPlannerConstants.SELECTED_TYPE_ID,"-1");
		else
			model.addAttribute(TaskPlannerConstants.SELECTED_TYPE_ID,typeId);
		return TaskPlannerConstants.TYPE_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		typeList = taskPlannerService.getActiveTypeList(commonError);
		typeId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_TYPE_ID );
		logger.info(typeId);
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.TYPE_LIST_PAGE ) != null ? (List<Boolean>)getSessionObj(TaskPlannerConstants.TYPE_LIST_PAGE) : Arrays.asList(false, true);
		if( typeId == null || "".equals(typeId) ) return;
		if( "-1".equals(typeId) ){
			type = new TaskType();
		}
		else{
			type = taskPlannerService.getTaskType(commonError, typeId);
			if( type == null ) type = new TaskType();
		}
	}
			
	/**
	 * This method will get all the active TaskTypes of documents
	 * @param event
	 */
	public void showActive(ActionEvent event){
		typeList = taskPlannerService.getActiveTypeList(commonError);
		if( filteredTaskTypeList != null ) filteredTaskTypeList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive TaskTypes of documents
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		typeList = taskPlannerService.getInactiveTypeList(commonError);
		if( filteredTaskTypeList != null ) filteredTaskTypeList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}

	/**
	 * This method to open new TaskType add page
	 * @param add page
	 * @return 
	 */
	public void addAction(){
		redirector(TaskPlannerConstants.TYPE_EDIT_PAGE);
	}

	/**
	 * This method will add / update a TaskType in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		if( "".equals(CommonUtils.getValidString(type.getId())) ){
			taskPlannerService.addTaskType( commonError, loginUser, type);
		}else{
			taskPlannerService.updateTaskType( commonError, loginUser, type);
		}
		typeList = taskPlannerService.getActiveTypeList(commonError);
		if( commonError.getError().isEmpty() ){
			if( "".equals(CommonUtils.getValidString(type.getId())) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( TaskPlannerConstants.TYPE_ADDED_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE);
			}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( TaskPlannerConstants.TYPE_UPDATED_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE);
			}
		}
	}
	
	/**
	 * This method will navigate to the Edit page
	 * @param index
	 * @return
	 */
	public void editAction( String index ){
		type = taskPlannerService.getTaskType(commonError, index);
		redirector(TaskPlannerConstants.TYPE_EDIT_PAGE+"?objectId="+type.getId());
	}	
	
	/**
	 * This method will navigate to the View page
	 * @param index
	 * @return
	 */
	public void viewAction(String index)
	{
		type = taskPlannerService.getTaskType(commonError, index);
		addSessionObj( TaskPlannerConstants.TYPE_LIST_PAGE , togglerList );
		redirector(TaskPlannerConstants.TYPE_VIEW_PAGE+"?objectId="+type.getId());
	}
	/**
	 * This method to returns to main TaskType list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.TYPE_LIST_PAGE);
	}
	
	/**
	 * This method deletes TaskTypes
	 * @param list page
	 * @return 
	 */
	public void deleteAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.deleteTaskType( commonError, loginUser, selectedTypes );
		if( commonError.getError().isEmpty() ){
			customRedirector( TaskPlannerConstants.TYPE_DELETE_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE);
		}
		showInactive(e);
		
	}
	/**
	 * This method locks TaskTypes
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.lockTaskType( commonError, loginUser, selectedTypes );
		showActive(e);
	}
	
	/**
	 * This method unlocks TaskTypes
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.unlockTaskType( commonError, loginUser, selectedTypes );
		showInactive(e);
	}
	
	/**
	 * This method locks viewed TaskType
	 * @param list page
	 * @return
	 */
	public void lockViewedTaskType(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		type = taskPlannerService.getTaskType(commonError, index);
		taskPlannerService.lockViewedTaskType( commonError, loginUser, type );
		customRedirector( TaskPlannerConstants.TYPE_LOCK_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE);
	}

	/**
	 * This method locks viewed TaskType
	 * @param list page
	 * @return
	 */
	public void unlockViewedTaskType(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		type = taskPlannerService.getTaskType(commonError, index);
		taskPlannerService.unlockViewedTaskType( commonError, loginUser, type );
		if( commonError.getError().isEmpty() ){
			customRedirector( TaskPlannerConstants.TYPE_UNLOCK_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed TaskType
	 * @param index
	 */
	public void deleteViewedTaskType(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		type = taskPlannerService.getTaskType(commonError, index);
		taskPlannerService.deleteViewedTaskType( commonError, loginUser, type );
		if( commonError.getError().isEmpty() ){
			customRedirector( TaskPlannerConstants.TYPE_DELETE_SUCCESS, TaskPlannerConstants.TYPE_LIST_PAGE );
			
			
		}
	}
	
	public String getUserName(String userId){
		User requiredUser = getUserById(commonError, userId);

		StringBuilder fullName= new StringBuilder();
		if(requiredUser != null ){
			String firstName = CommonUtils.getValidString(requiredUser.getFirstName());
			String lastName = CommonUtils.getValidString(requiredUser.getFirstName());
			if(!"".equals(firstName) && !"".equals(lastName)){
				fullName.append(firstName);fullName.append(" ");
				fullName.append(lastName);
			}
			}
		else
			fullName.append("");
		return fullName.toString();
	}
	
	public String getUserImage(String userId){
		User requiredUser = getUserById(commonError, userId);
		return requiredUser.getUserImage();
	}
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_TYPE_ID );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_TYPE_ID );
	}
	
	public void onToggle(ToggleEvent event) {
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	
	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}	

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	
	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	

	public TaskType getType() {
		return type;
	}

	public void setType(TaskType type) {
		this.type = type;
	}

	public List<TaskType> getFilteredTaskTypeList() {
		return filteredTaskTypeList;
	}

	public void setFilteredTaskTypeList(List<TaskType> filteredTaskTypeList) {
		this.filteredTaskTypeList = filteredTaskTypeList;
	}

	public List<TaskType> getSelectedTypes() {
		return selectedTypes;
	}

	public void setSelectedTypes(List<TaskType> selectedTypes) {
		this.selectedTypes = selectedTypes;
	}

	public List<TaskType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<TaskType> typeList) {
		this.typeList = typeList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	

	
	
}
