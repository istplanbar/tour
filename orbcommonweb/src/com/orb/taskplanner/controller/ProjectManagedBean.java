package com.orb.taskplanner.controller;


import java.util.Arrays;
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.orb.common.client.model.User;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.constants.TaskPlannerConstants;


@ManagedBean(name="projectView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class ProjectManagedBean extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(ProjectManagedBean.class);
	private Project project = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String diviProjectType = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Project> projectList = null;
	private List<Division> divisionList = null;
	private List<Project> filteredProjectList = null;
	private List<Project> selectedDocs=null;
	String projId = null;
	private List<Boolean> togglerList = null;
	
	/**
	 * This Method will be called to view the project list page when use clicks project link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadProject")
	public String loadProject(ModelMap model){
		clearSessionObjects(model);
		return TaskPlannerConstants.PROJECT_LIST_HTML;
	}
	
	/**
	 * This method will be called when user clicks particular project from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewProject")
	public String viewProject(HttpServletRequest request, ModelMap model)
	{
		String projId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		model.addAttribute(TaskPlannerConstants.SELECTED_PROJECT_ID,projId);
		return TaskPlannerConstants.PROJECT_VIEW_HTML;
	}
	
	/**
	 * This method will be called when user tries to Add/Update project
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUpdateProject")
    public String addUpdateProject( HttpServletRequest request, ModelMap model){
		String projId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		if( "".equals(projId) )
			model.addAttribute(TaskPlannerConstants.SELECTED_PROJECT_ID, "-1" );
		else
			model.addAttribute(TaskPlannerConstants.SELECTED_PROJECT_ID, projId );
    	return TaskPlannerConstants.PROJECT_EDIT_HTML;
    }

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		projId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_PROJECT_ID );
		logger.info(projId);
		setProjectList(taskPlannerService.getActiveProjectList(commonError));
		setDivisionList(taskPlannerService.getActiveDivisionList(commonError));
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.PROJECT_LIST_PAGE) != null ? (List<Boolean>)getSessionObj(TaskPlannerConstants.PROJECT_LIST_PAGE) : Arrays.asList(false, true,true,true);
		if( projId == null || "".equals(projId) ) return;
		if( "-1".equals(projId) ){
			project = new Project();
		}
		else{
			project = taskPlannerService.getProject(commonError, projId);
			if( project == null ) project = new Project();
		}
	}
		
	/**
	 * This method will get all the active Projects 
	 * @param event
	 */
	public void showActive(ActionEvent event){
		setProjectList(taskPlannerService.getActiveProjectList( commonError ));
		if( filteredProjectList != null ) filteredProjectList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive Projects 
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		setProjectList(taskPlannerService.getInactiveProjectList( commonError));
		if( filteredProjectList != null ) filteredProjectList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	/**
	 * This method to open new Project add page
	 * @param add page
	 * @return 
	 */
	public void addAction(){
		redirector(TaskPlannerConstants.PROJECT_EDIT_PAGE);
	}

	/**
	 * This method will add / update a Project in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		User loginUser = (User)getSessionObj(CommonConstants.USER_IN_CONTEXT );
		if( "".equals(CommonUtils.getValidString(project.getId()))){
			taskPlannerService.addProject(commonError, loginUser, project);
		}else{
			taskPlannerService.updateProject(commonError, loginUser, project);
		}
		setProjectList(taskPlannerService.getActiveProjectList(commonError));
		if(commonError.getError().isEmpty()){
			if( "".equals(CommonUtils.getValidString(project.getId())) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( "project_added_success", TaskPlannerConstants.PROJECT_LIST_PAGE);
				}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( "project_update_success", TaskPlannerConstants.PROJECT_LIST_PAGE);
			}
		}
	}
	
	/**
	 * This method will navigate to the Edit page
	 * @param index
	 * @return
	 */
	public void editAction(String index ){
		project = taskPlannerService.getProject(commonError, index);
		redirector(TaskPlannerConstants.PROJECT_EDIT_PAGE+"?objectId="+project.getId());
	}	
	
	/**
	 * This method will navigate to the view page
	 * @param index
	 * @return
	 */
	
	public void viewAction(String index)
	{
		project = taskPlannerService.getProject(commonError, index);
		addSessionObj( TaskPlannerConstants.PROJECT_LIST_PAGE , togglerList );
		redirector(TaskPlannerConstants.PROJECT_VIEW_PAGE+"?objectId="+project.getId());
		
	}
	
	/**
	 * This method to returns to main Project list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.PROJECT_LIST_PAGE);
		}
	
	/**
	 * This method deletes Projects
	 * @param list page
	 * @return 
	 */
	public void deleteAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.deleteProject( commonError, loginUser, selectedDocs );
		if( commonError.getError().isEmpty() ){
			customRedirector( "project_delete_success", TaskPlannerConstants.PROJECT_LIST_PAGE);
		}
		showInactive(e);
	}
	
	/**
	 * This method locks Projects
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.lockProject( commonError, loginUser, selectedDocs );
		showActive(e);
	}
	
	/**
	 * This method unlocks Projects
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.unlockProject( commonError, loginUser, selectedDocs );
		showInactive(e);
	}
	
	/**
	 * This method locks viewed Project
	 * @param list page
	 * @return
	 */
	public void lockViewedProject(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		project = taskPlannerService.getProject(commonError, index);
		taskPlannerService.lockViewedProject( commonError, loginUser, project );
		customRedirector( "project_locked_success", TaskPlannerConstants.PROJECT_LIST_PAGE);
	}

	/**
	 * This method locks viewed Project
	 * @param list page
	 * @return
	 */
	public void unlockViewedProject(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		project = taskPlannerService.getProject(commonError, index);
		taskPlannerService.unlockViewedProject( commonError, loginUser, project );
		if( commonError.getError().isEmpty() ){
			customRedirector( "project_unlocked_success", TaskPlannerConstants.PROJECT_LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed project
	 * @param index
	 */
	public void deleteViewedProject(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		project = taskPlannerService.getProject(commonError, index);
		taskPlannerService.deleteViewedProject( commonError, loginUser, project );
		if( commonError.getError().isEmpty() ){
			customRedirector( "project_delete_success", TaskPlannerConstants.PROJECT_LIST_PAGE );
		}
	}
	
	
	
	/**
	 * This method will show division based Projects 
	 * @param event
	 */
	public void changeType(ValueChangeEvent e){
		String value = (String) e.getNewValue();
		if(value.equals(TaskPlannerConstants.PROJECT_TYPE)){
			diviProjectType = CommonConstants.TRUE;
		}else{
			diviProjectType =  CommonConstants.FALSE;
			}
	}	
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_PROJECT_ID );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_PROJECT_ID );
	}

	public String getShowActiveListBtn() {	
		return showActiveListBtn;
	}	

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	
	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Project> getFilteredProjectList() {
		return filteredProjectList;
	}

	public void setFilteredProjectList(List<Project> filteredProjectList) {
		this.filteredProjectList = filteredProjectList;
	}

	

	public String getDiviProjectType() {
		return diviProjectType;
	}

	public void setDiviProjectType(String diviProjectType) {
		this.diviProjectType = diviProjectType;
	}

	public List<Project> getSelectedDocs() {
		return selectedDocs;
	}

	public void setSelectedDocs(List<Project> selectedDocs) {
		this.selectedDocs = selectedDocs;
	}

	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		ProjectManagedBean.logger = logger;
	}

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public List<Division> getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(List<Division> divisionList) {
		this.divisionList = divisionList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	
	
}
