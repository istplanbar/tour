package com.orb.taskplanner.controller;


import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.constants.TaskPlannerConstants;


@ManagedBean(name="categoryView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class TaskCategoryManagedBean extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(TaskCategoryManagedBean.class);
	private TaskCategory category = null;
    private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<TaskCategory> categoryList = null;
	private List<TaskCategory> filteredCategoryList = null;
	private List<TaskCategory> selectedDocs = null;
	String catId = null;
	private List<Boolean> togglerList = null;
 
	/**
	 * This Method will be called to view the category list page when use clicks category link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadTaskCategory")
    public String loadCategory(ModelMap model){
		clearSessionObjects(model);
    	return TaskPlannerConstants.CATEGORY_LIST_HTML;
    }
	
	/**
	 * This method will be called when user clicks particular category from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewCategory")
	public String viewCategory(HttpServletRequest request, ModelMap model)
	{
		String catId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		model.addAttribute(TaskPlannerConstants.SELECTED_CATEGORY_ID,catId);
		return TaskPlannerConstants.CATEGORY_VIEW_HTML;
	}
	
	/**
	 * This method will be called when user tries to Add/Update category
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUpdateCategory")
    public String addUpdateCategory( HttpServletRequest request, ModelMap model){
		String catId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		if( "".equals(catId) )
			model.addAttribute(TaskPlannerConstants.SELECTED_CATEGORY_ID, "-1" );
		else
			model.addAttribute(TaskPlannerConstants.SELECTED_CATEGORY_ID, catId );
    	return TaskPlannerConstants.CATEGORY_EDIT_HTML;
    }
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		setCategoryList(taskPlannerService.getActiveCategoryList(commonError));
		catId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_CATEGORY_ID );
		logger.info(catId);
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.CATEGORY_LIST_PAGE) != null ? (List<Boolean>)getSessionObj(TaskPlannerConstants.CATEGORY_LIST_PAGE) : Arrays.asList(false, true);
		if( catId == null || "".equals(catId) ) return;
		if( "-1".equals(catId) ){
			category = new TaskCategory();
		}
		else{
			category = taskPlannerService.getCatName(commonError, catId);
			if( category == null ) category = new TaskCategory();
		}
	}
		
	
	/**
	 * This method will get all the active Categories from documents
	 * @param event
	 */
	public void showActive(ActionEvent event){
		setCategoryList(taskPlannerService.getActiveCategoryList( commonError ));
		if( filteredCategoryList != null ) filteredCategoryList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive Categories of documents
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		setCategoryList(taskPlannerService.getInactiveCategoryList( commonError));
		if( filteredCategoryList != null ) filteredCategoryList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}

	/**
	 * This method to open new TaskCategory add page
	 * @param add page
	 * @return 
	 */
	public void addAction(){
		redirector(TaskPlannerConstants.CATEGORY_EDIT_PAGE);
	}

	/**
	 * This method will add / update a TaskCategory in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		if( "".equals(CommonUtils.getValidString(category.getId())) ){
			taskPlannerService.add( commonError, loginUser, category);
		}else{
			taskPlannerService.update( commonError, loginUser, category);
		}
		setCategoryList(taskPlannerService.getActiveCategoryList( commonError));
		if( commonError.getError().isEmpty() ){
			if( "".equals(CommonUtils.getValidString(category.getId())) ){
				FacesContext context = FacesContext.getCurrentInstance();
		        context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( "category_added_success", TaskPlannerConstants.CATEGORY_LIST_PAGE);
			}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
		        context.getExternalContext().getFlash().setKeepMessages(true);
		        customRedirector( "category_update_success", TaskPlannerConstants.CATEGORY_LIST_PAGE);
			}
		}
	}
	
	/**
	 * This method will navigate to the Edit page
	 * @param index
	 * @return
	 */
	public void editAction( String index ){
		category = taskPlannerService.getCatName(commonError, index);
		redirector(TaskPlannerConstants.CATEGORY_EDIT_PAGE+"?objectId="+category.getId());
	}	
	
	/**
	 * This method will navigate to the View page
	 * @param index
	 * @return
	 */
	public void viewAction( String index ){
		category = taskPlannerService.getCatName(commonError, index);
		addSessionObj( TaskPlannerConstants.CATEGORY_LIST_PAGE , togglerList );
		redirector(TaskPlannerConstants.CATEGORY_VIEW_PAGE+"?objectId="+category.getId());
	}	
	
	/**
	 * This method to returns to main TaskCategory list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.CATEGORY_LIST_PAGE);
	}
	
	/**
	 * This method deletes Categories
	 * @param list page
	 * @return 
	 */
	public void deleteAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.deleteCategory( commonError, loginUser, selectedDocs );
		if( commonError.getError().isEmpty() ){
			customRedirector( "category_delete_success", TaskPlannerConstants.CATEGORY_LIST_PAGE);
		}
		showInactive(e);
		
	}
	/**
	 * This method locks Categories
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.lockCategory( commonError, loginUser, selectedDocs );
		showActive(e);
	}
	
	/**
	 * This method unlocks Categories
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.unlockCategory( commonError, loginUser, selectedDocs );
		showInactive(e);
	}
	
	/**
	 * This method locks viewed Category
	 * @param list page
	 * @return
	 */
	public void lockViewedCategory(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		category=taskPlannerService.getCatName(commonError, index);
		taskPlannerService.lockViewedCategory( commonError, loginUser, category );
		customRedirector( "category_locked_success", TaskPlannerConstants.CATEGORY_LIST_PAGE);
	}

	/**
	 * This method locks viewed Category
	 * @param list page
	 * @return
	 */
	public void unlockViewedCategory(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		category=taskPlannerService.getCatName(commonError, index);
		taskPlannerService.unlockViewedCategory( commonError, loginUser, category );
		if( commonError.getError().isEmpty() ){
			customRedirector( "category_unlocked_success", TaskPlannerConstants.CATEGORY_LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed Category
	 * @param index
	 */
	public void deleteViewedCategory(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		category=taskPlannerService.getCatName(commonError, index);
		taskPlannerService.deleteViewedCategory( commonError, loginUser, category );
		if( commonError.getError().isEmpty() ){
			customRedirector( "category_delete_success", TaskPlannerConstants.CATEGORY_LIST_PAGE );
			
			
		}
	}
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_CATEGORY_ID );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_CATEGORY_ID );
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	
	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public List<TaskCategory> getfilteredCategoryList() {
		return filteredCategoryList;
	}

	public void setfilteredCategoryList(List<TaskCategory> filteredCategoryList) {
		this.filteredCategoryList = filteredCategoryList;
	}

	
	
			
	public TaskCategory getCategory() {
		return category;
	}

	public void setCategory(TaskCategory category) {
		this.category = category;
	}

	public List<TaskCategory> getSelectedDocs() {
		return selectedDocs;
	}

	public void setSelectedDocs(List<TaskCategory> selectedDocs) {
		this.selectedDocs = selectedDocs;
	}

	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		TaskCategoryManagedBean.logger = logger;
	}

	public List<TaskCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<TaskCategory> categoryList) {
		this.categoryList = categoryList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
}
