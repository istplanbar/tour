package com.orb.taskplanner.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.orb.admin.useradministration.service.UserProfileSettingsService;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.crm.service.CrmService;
import com.orb.facilities.service.FacilitiesService;
import com.orb.taskplanner.service.TaskPlannerService;

abstract public class AbstractTaskPlannerController extends AbstractCommonController{

	@Autowired
	protected TaskPlannerService taskPlannerService;
	
	@Autowired 
	protected UserProfileSettingsService userProfileService;
	
	/*@Autowired
	protected QmsService qmsService;*/
	
	/*@Autowired
	protected ParticipantService participantService;*/
	
	@Autowired
	protected CrmService crmService;
	
	/*@Autowired
	protected CourseService courseService;
	*/
	
	@Autowired
	protected FacilitiesService facilitiesService;
	
}
