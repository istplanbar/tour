package com.orb.taskplanner.controller;


import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.constants.TaskPlannerConstants;

@ManagedBean(name="divisionView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/taskplanner")
public class DivisionManagedBean extends AbstractTaskPlannerController{

	private static Log logger = LogFactory.getLog(DivisionManagedBean.class);
	private Division division = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Division> divisionList = null;
	private List<Division> filteredDivisionList = null;
	private List<Division> selectedDocs=null;
	private String divId=null;
	private List<Boolean> togglerList = null;

	/**
	 * This Method will be called to view the division list page when use clicks division link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadDivision")
	public String loadDivision(ModelMap model){
		clearSessionObjects(model);
		return TaskPlannerConstants.DIVISION_LIST_HTML;
	}
	
	/**
	 * This method will be called when user clicks particular division from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewDivision")
	public String viewDivision(HttpServletRequest request, ModelMap model)
	{
		String divId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		model.addAttribute(TaskPlannerConstants.SELECTED_DIVISION_ID,divId);
		return TaskPlannerConstants.DIVISION_VIEW_HTML;
	}
	
	/**
	 * This method will be called when user tries to Add/Update division
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/addUpdateDivision")
	public String addUpdateDivision(HttpServletRequest request, ModelMap model)
	{
		String divId=CommonUtils.getValidString(request.getParameter(CommonConstants.OBJECT_ID));
		if("".equals(divId))
			model.addAttribute(TaskPlannerConstants.SELECTED_DIVISION_ID,"-1");
		else
			model.addAttribute(TaskPlannerConstants.SELECTED_DIVISION_ID,divId);
		return TaskPlannerConstants.DIVISION_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		setDivisionList(taskPlannerService.getActiveDivisionList(commonError));
		divId = getJSFRequestAttribute( TaskPlannerConstants.SELECTED_DIVISION_ID );
		logger.info(divId);
		togglerList = (List<Boolean>)getSessionObj(TaskPlannerConstants.DIVISION_LIST_PAGE) != null ? (List<Boolean>)getSessionObj(TaskPlannerConstants.DIVISION_LIST_PAGE) : Arrays.asList(false,true);
		
		if( divId == null || "".equals(divId) ) return;
		if( "-1".equals(divId) ){
			division = new Division();
		}
		else{
			division = taskPlannerService.getDivision(commonError, divId);
			if( division == null ) division = new Division();
		}
	}
	
	
	/**
	 * This method will get all the active Divisions of documents
	 * @param event
	 */
	public void showActive(ActionEvent event){
		setDivisionList(taskPlannerService.getActiveDivisionList( commonError ));
		if( filteredDivisionList != null ) filteredDivisionList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will get all the inactive Divisions of documents
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		setDivisionList(taskPlannerService.getInactiveDivisionList( commonError));
		if( filteredDivisionList != null ) filteredDivisionList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}

	/**
	 * This method to open new Division add page
	 * @param add page
	 * @return 
	 */
	public void addAction(){
		redirector(TaskPlannerConstants.DIVISION_EDIT_PAGE);
	}

	/**
	 * This method will add / update a Division in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		if( "".equals(CommonUtils.getValidString(division.getId())) ){
			taskPlannerService.addDivision( commonError, loginUser, division);
		}else{
			taskPlannerService.updateDivision( commonError, loginUser, division);
		}
		setDivisionList(taskPlannerService.getActiveDivisionList( commonError));
		if( commonError.getError().isEmpty() ){
			if( "".equals(CommonUtils.getValidString(division.getId())) ){
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( "division_added_success", TaskPlannerConstants.DIVISION_LIST_PAGE);
			}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
			    context.getExternalContext().getFlash().setKeepMessages(true);
				customRedirector( "division_update_success", TaskPlannerConstants.DIVISION_LIST_PAGE);
			}
		}
	}
	
	/**
	 * This method will navigate to the Edit page
	 * @param index
	 * @return
	 */
	public void editAction( String index ){
		division = taskPlannerService.getDivision(commonError, index);
		redirector(TaskPlannerConstants.DIVISION_EDIT_PAGE+"?objectId="+division.getId());
	}	
	
	/**
	 * This method will navigate to the View page
	 * @param index
	 * @return
	 */
	public void viewAction(String index)
	{
		division = taskPlannerService.getDivision(commonError, index);
		addSessionObj( TaskPlannerConstants.DIVISION_LIST_PAGE , togglerList );
		
		redirector(TaskPlannerConstants.DIVISION_VIEW_PAGE+"?objectId="+division.getId());
	}
	/**
	 * This method to returns to main Division list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(TaskPlannerConstants.DIVISION_LIST_PAGE);
	}
	
	/**
	 * This method deletes Divisions
	 * @param list page
	 * @return 
	 */
	public void deleteAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.deleteDivision( commonError, loginUser, selectedDocs );
		if( commonError.getError().isEmpty() ){
			customRedirector( "division_delete_success", TaskPlannerConstants.DIVISION_LIST_PAGE);
		}
		showInactive(e);
		
	}
	/**
	 * This method locks Divisions
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.lockDivision( commonError, loginUser, selectedDocs );
		showActive(e);
	}
	
	/**
	 * This method unlocks Divisions
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		taskPlannerService.unlockDivision( commonError, loginUser, selectedDocs );
		showInactive(e);
	}
	
	/**
	 * This method locks viewed Division
	 * @param list page
	 * @return
	 */
	public void lockViewedDivision(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		division = taskPlannerService.getDivision(commonError, index);
		taskPlannerService.lockViewedDivision( commonError, loginUser, division );
		customRedirector( "division_locked_success", TaskPlannerConstants.DIVISION_LIST_PAGE);
	}

	/**
	 * This method locks viewed Division
	 * @param list page
	 * @return
	 */
	public void unlockViewedDivision(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		division = taskPlannerService.getDivision(commonError, index);
		taskPlannerService.unlockViewedDivision( commonError, loginUser, division );
		if( commonError.getError().isEmpty() ){
			customRedirector( "division_unlocked_success", TaskPlannerConstants.DIVISION_LIST_PAGE );
		}
	}
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * Delete viewed Division
	 * @param index
	 */
	public void deleteViewedDivision(String index){
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		division = taskPlannerService.getDivision(commonError, index);
		taskPlannerService.deleteViewedDivision( commonError, loginUser, division );
		if( commonError.getError().isEmpty() ){
			customRedirector( "division_delete_success", TaskPlannerConstants.DIVISION_LIST_PAGE );
			
			
		}
	}
	
	/*public String getUserName(String userId){
		User requiredUser = getUserById(commonError, userId);

		StringBuilder fullName= new StringBuilder();
		if(requiredUser != null ){
			String firstName = CommonUtils.getValidString(requiredUser.getFirstName());
			String lastName = CommonUtils.getValidString(requiredUser.getFirstName());
			if(!"".equals(firstName) && !"".equals(lastName)){
				fullName.append(firstName);fullName.append(" ");
				fullName.append(lastName);
			}
			}
		else
			fullName.append("");
		return fullName.toString();
	}*/
	
	/*public String getUserImage(String userId){
		User requiredUser = getUserById(commonError, userId);
		return requiredUser.getUserImage();
	}*/
	
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( TaskPlannerConstants.SELECTED_DIVISION_ID );
		if( id != null ) model.remove( TaskPlannerConstants.SELECTED_DIVISION_ID );
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}	

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	
	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public List<Division> getFilteredDivisionList() {
		return filteredDivisionList;
	}

	public void setFilteredDivisionList(List<Division> filteredDivisionList) {
		this.filteredDivisionList = filteredDivisionList;
	}

	public List<Division> getSelectedDocs() {
		return selectedDocs;
	}

	public void setSelectedDocs(List<Division> selectedDocs) {
		this.selectedDocs = selectedDocs;
	}

	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		DivisionManagedBean.logger = logger;
	}

	public List<Division> getDivisionList() {
		return divisionList;
	}

	public void setDivisionList(List<Division> divisionList) {
		this.divisionList = divisionList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	
}
