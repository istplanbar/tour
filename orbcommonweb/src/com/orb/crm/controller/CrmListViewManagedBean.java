package com.orb.crm.controller;

/**
 * @author IST planbar / MaCh
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.AddNotes;
import com.orb.crm.bean.Crm;
import com.orb.crm.constants.CrmConstants;



@ManagedBean(name="crmView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/crm")
public class CrmListViewManagedBean extends AbstractCrmController{
	
	private static Log logger = LogFactory.getLog(CrmListViewManagedBean.class);
	
	private Crm crm = null;
	private List<Crm> crmList = null;
	private User loginUser = null;
	private List<Crm> filteredCrmList = null;
	private List<Crm> selectedCrmList = null;
	private String crmId = null;
	private List<MultiVal> countryList = null;
	private List<MultiVal> stateList = null;
    private List<Boolean> togglerList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private Boolean type = null;
	private AddNotes addNotes = null;
	private List<AddNotes> addNotesList=null;
	private List<Crm> companyList = null;
	private String currentView = null;
	private Boolean detailView = null;
	private Boolean listView = null;
	private String changeView = null;
	
			
		
	/**
	 * This Method will be called to view the crm list page when use clicks crm link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadCrm")
	public String loadCrm(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		currentView = CommonUtils.getValidString( request.getParameter( CrmConstants.CURRENT_VIEW ) );
		model.addAttribute(CrmConstants.CURRENT_VIEW, currentView);
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return CrmConstants.CRM_DEFAULT_LIST_HTML;
	}
	
	/**
	 * This method will be called when user clicks particular Crm from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewCrm")
	public String viewCrm(HttpServletRequest request, ModelMap model){
		crmId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		currentView = CommonUtils.getValidString( request.getParameter( CrmConstants.CURRENT_VIEW ) );
		model.addAttribute(CrmConstants.CURRENT_VIEW, currentView);
		model.addAttribute( CrmConstants.SELECTED_CRM_ID, crmId );
	    return CrmConstants.CRM_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
        crmList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		crmId = CommonUtils.getValidString(getJSFRequestAttribute( CrmConstants.SELECTED_CRM_ID ));
		currentView = CommonUtils.getValidString(getJSFRequestAttribute( CrmConstants.CURRENT_VIEW ) );
		logger.info("crm id: "+crmId);
		logger.info("current View: "+currentView);
		if(currentView.isEmpty()|| CrmConstants.CRM_DETAIL_VIEW.equals(currentView)){
			detailView = true;
			changeView = CommonConstants.FALSE;
		}else{
			listView = true;
			changeView = CommonConstants.TRUE;
		}
		countryList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_COUNTRY ); 
		stateList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_STATE ); 
		type = true;
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
    	if(CommonConstants.N.equals(showActive)){
			crmList = crmService.getInactiveCrmList(commonError);
			showActivateBtn = CommonConstants.TRUE;
			showActiveListBtn = CommonConstants.TRUE;
		}else{
			crmList=crmService.getActiveCrmList(commonError);
		}	
		companyList = new ArrayList<>(crmList);
		companyList = companyList.stream().filter(c -> CommonConstants.TRUE.equals(c.getType())).collect(Collectors.toList());
		togglerList = (List<Boolean>)getSessionObj(CrmConstants.CRM_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true, true,true);
		}
		if(!crmId.isEmpty()){
		    crm = crmService.getCrm(commonError, crmId);
		    crm.setCompany(crmList.stream().filter(cr -> cr.getId().equals(crm.getCompany())).map(Crm::getFirstName)					
						.findAny().orElse(null));
            addNotes=new AddNotes();
		    addNotes.setCrmIdentifier(crm.getIdentifier());
		    addNotes.setName(crm.getFirstName());
		    addNotesList = crmService.getAddNotes(commonError, crmId);
		 }
	}
	
	/**
	 * This method will sent a addNotes in DB
	 * @param event
	 */
	public void saveNotes(ActionEvent event){
		if(CommonUtils.getValidString(addNotes.getId()).isEmpty()){
			crmService.addAddNotes( commonError, loginUser,addNotes );
		}
		customRedirector(CrmConstants.CRM_ADDNOTES_ADDED_SUCCESS,CrmConstants.CRM_VIEW_PAGE+"?objectId="+crm.getIdentifier() );
	}
		
	/**
	 * This method will navigate to the edit page
	 * @param crmId
	 */
	public void editAction(String crmId ){
		crm = crmService.getCrm( commonError, crmId );
		redirector(CrmConstants.CRM_EDIT_PAGE+"?objectId="+crm.getIdentifier()+"&currentView="+currentView);
	}	
	
	/**
	 * This method will navigate to the view page
	 * @param crmId
	 */
	public void detailedViewAction( String crmId ){
		crm = crmService.getCrm( commonError, crmId );
		addSessionObj( CrmConstants.CRM_TOGGLER_LIST , togglerList );
    	redirector(CrmConstants.CRM_VIEW_PAGE+"?objectId="+crm.getIdentifier()+"&currentView="+CrmConstants.CRM_DETAIL_VIEW);
	}	
	
	/**
	 * This method will navigate to the view page
	 * @param crmId
	 */
	public void listedViewAction( String crmId ){
		crm = crmService.getCrm( commonError, crmId );
		addSessionObj( CrmConstants.CRM_TOGGLER_LIST , togglerList );
    	redirector(CrmConstants.CRM_VIEW_PAGE+"?objectId="+crm.getIdentifier()+"&currentView="+CrmConstants.CRM_LIST_VIEW);
	}	
		
	/**
	 * This method to returns to main Crm list page
	 */
	public void cancelAction(){
		if(CrmConstants.CRM_LIST_VIEW.equals(currentView)){
		  redirector( CrmConstants.CRM_LIST_PAGE+"?activeFlag="+crm.getActive()+"&currentView="+CrmConstants.CRM_LIST_VIEW);
		}else{
    	  redirector( CrmConstants.CRM_LIST_PAGE+"?activeFlag="+crm.getActive()+"&currentView="+CrmConstants.CRM_DETAIL_VIEW);	
		}
	}
	
	/**
	 * This method will get all the active Crm
	 * @param event
	 */
	public void showActive(ActionEvent event){
		crmList = crmService.getActiveCrmList(commonError);
		if( filteredCrmList != null ) filteredCrmList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}
	
	/**
	 * This method will show all the inactive Crm
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		crmList = crmService.getInactiveCrmList(commonError);
		if( filteredCrmList != null ) filteredCrmList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	/**
	 * This method locks crm
	 * @param e
	 */
	public void lockAction(ActionEvent e){
		crmService.lockCrm( commonError, loginUser, selectedCrmList );
		showActive(e);
	}
		
	/**
	 * This method unlocks crm
	 * @param e
	 */
	public void unlockAction(ActionEvent e){
		crmService.unlockCrm( commonError, loginUser, selectedCrmList );
		showInactive(e);
	}
	
	/**
	 * This method will delete the Crm
	 * @param e
	 */
	public void deleteAction(ActionEvent e){
		if(selectedCrmList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(CrmConstants.CRM_DELETE_SUCCESS);
		}else{
			crmService.deleteCrm( commonError, loginUser, selectedCrmList );
			if( commonError.getError().isEmpty() ){
				customRedirector( CrmConstants.CRM_DELETE_SUCCESS, CrmConstants.CRM_LIST_PAGE);
			}
			showInactive(e);
			commonError.clearErrors();
		}
	}
	
	/**
	 * This method locks viewed crm
	 * @param crmId
	 */
	public void lockViewedCrm(String crmId){
		crm = crmService.getCrm(commonError, crmId);
		crmService.lockViewedCrm( commonError, loginUser, crm );
		customRedirector( CrmConstants.CRM_LOCKED_SUCCESS, CrmConstants.CRM_LIST_PAGE);
	}
	
	/**
	 * This method unlocks viewed crm
	 * @param crmId
	 */
	public void unlockViewedCrm(String crmId){
		crm = crmService.getCrm(commonError, crmId);
		crmService.unlockViewedCrm( commonError, loginUser, crm );
		if( commonError.getError().isEmpty() ){
    		customRedirector( CrmConstants.CRM_UNLOCKED_SUCCESS, CrmConstants.CRM_LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed crm
	 * @param crmId
	 */
	public void deleteViewedCrm(String crmId){
		crm = crmService.getCrm(commonError, crmId);
		crmService.deleteViewedCrm( commonError, loginUser, crm);
		if( commonError.getError().isEmpty() ){
			customRedirector( CrmConstants.CRM_DELETE_SUCCESS, CrmConstants.CRM_LIST_PAGE );
		}
	}
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param e
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
		
	/**
	 * To clear the session variables
	 * @param model
	 */	
	@SuppressWarnings("rawtypes")
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( CrmConstants.SELECTED_CRM_ID );
		if( id != null ) model.remove(CrmConstants.SELECTED_CRM_ID);
		List toggleList = (List)model.get( togglerList );
		if( toggleList != null ) model.remove( CrmConstants.CRM_TOGGLER_LIST );
	}
		
	/**
	 * This method will show  type in crm 
	 * @param e
	 */
	public void changeType(AjaxBehaviorEvent  e){
		Boolean value = (Boolean) ((UIOutput) e.getSource()).getValue();
		if(value == null || !value.booleanValue()){
		 type = true;
		}else{
		 type = false;
		}
	}	
	
	/**
	 * This method is used to filter the institute based on the company
	 * @param index
	 * @return
	 */	
    public String filterInsitute(String index){
		String insName = null ;
		insName = companyList.stream().filter(c->c.getId().equals(index)).map(Crm::getFirstName).findAny().orElse(null);
		return insName;
	}
	
	/**
	 * This method is use to toggle crm page between list and detail view
	 */
	public void onChangeView(String boolValue){
		if(CommonConstants.FALSE.equals(boolValue)){
        	listView = true;
			changeView = CommonConstants.TRUE;
			detailView = false;
		}else{
			changeView = CommonConstants.FALSE;
			detailView = true;
			listView = false;
		}
	}
	
	public Crm getCrm() {
		return crm;
	}

	public void setCrm(Crm crm) {
		this.crm = crm;
	}

	public List<Crm> getCrmList() {
		return crmList;
	}

	public void setCrmList(List<Crm> crmList) {
		this.crmList = crmList;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Crm> getFilteredCrmList() {
		return filteredCrmList;
	}

	public void setFilteredCrmList(List<Crm> filteredCrmList) {
		this.filteredCrmList = filteredCrmList;
	}

	public List<MultiVal> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<MultiVal> countryList) {
		this.countryList = countryList;
	}

	public List<MultiVal> getStateList() {
		return stateList;
	}

	public void setStateList(List<MultiVal> stateList) {
		this.stateList = stateList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}

	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public Boolean getType() {
		return type;
	}

	public void setType(Boolean type) {
		this.type = type;
	}

	public List<Crm> getSelectedCrmList() {
		return selectedCrmList;
	}

	public void setSelectedCrmList(List<Crm> selectedCrmList) {
		this.selectedCrmList = selectedCrmList;
	}
	
	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public AddNotes getAddNotes() {
		return addNotes;
	}

	public void setAddNotes(AddNotes addNotes) {
		this.addNotes = addNotes;
	}

	public List<AddNotes> getAddNotesList() {
		return addNotesList;
	}

	public void setAddNotesList(List<AddNotes> addNotesList) {
		this.addNotesList = addNotesList;
	}

	public List<Crm> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Crm> companyList) {
		this.companyList = companyList;
	}

	public String getDefaultView() {
		return currentView;
	}

	public void setDefaultView(String defaultView) {
		this.currentView = defaultView;
	}

	public String getChangeView() {
		return changeView;
	}

	public void setChangeView(String changeView) {
		this.changeView = changeView;
	}

	public Boolean getDetailView() {
		return detailView;
	}

	public void setDetailView(Boolean detailView) {
		this.detailView = detailView;
	}

	public Boolean getListView() {
		return listView;
	}

	public void setListView(Boolean listView) {
		this.listView = listView;
	}

		
	
}
