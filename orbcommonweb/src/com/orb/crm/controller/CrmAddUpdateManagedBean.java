package com.orb.crm.controller;

/**
 * @author IST planbar / MaCh
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.Crm;
import com.orb.crm.constants.CrmConstants;


@ManagedBean(name="crmManipulate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/crm")
public class CrmAddUpdateManagedBean extends AbstractCrmController{
	
	private static Log logger = LogFactory.getLog(CrmAddUpdateManagedBean.class);
	
	private Crm crm = null;
	private String crmId = null;
	private User loginUser = null;
	private List<MultiVal> countryList = null;
	private List<MultiVal> stateList = null;
	private UploadedFile uploadedFile = null;
	private List<Crm> companyList = null;
	private String currentView = null;
	
		
	/**
	 * This method will be called when user clicks Add button / Crm Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateCrm")
    public String manipulateCrm( HttpServletRequest request, ModelMap model){
		crmId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		currentView = CommonUtils.getValidString( request.getParameter( CrmConstants.CURRENT_VIEW ) );
		model.addAttribute(CrmConstants.CURRENT_VIEW, currentView);
		model.addAttribute( CrmConstants.SELECTED_CRM_ID, crmId );
		return CrmConstants.CRM_EDIT_HTML;
    }
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		crmId = CommonUtils.getValidString(getJSFRequestAttribute( CrmConstants.SELECTED_CRM_ID));
		logger.info("crm id: "+crmId);
		currentView = CommonUtils.getValidString(getJSFRequestAttribute( CrmConstants.CURRENT_VIEW ) );
		companyList = crmService.getActiveCrmList(commonError);
		if(companyList != null){
		   companyList = companyList.stream().filter(c -> CommonConstants.TRUE.equals(c.getType())).collect(Collectors.toList());
		}
		countryList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_COUNTRY ); // Country List only required for Add / Update page.
		stateList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_STATE ); // State List only required for Add / Update page.
		if(crmId.isEmpty()){
			crm = new Crm();
		}else{
			crm = crmService.getCrm(commonError, crmId);
		}
	}
	
	
	/**
	 * This method returns to Crm view page
	 */
	public void cancelAction(){
		if(crm.getIdentifier() == null){
			redirector(CrmConstants.CRM_LIST_PAGE+"?view="+currentView);
		}else{
			redirector(CrmConstants.CRM_VIEW_PAGE+"?objectId="+crm.getIdentifier()+"&currentView="+currentView);
		}
	}
	
	/**
	 * This method will add / update a crm in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		if(CommonUtils.getValidString(crm.getId()).isEmpty()){
			crmService.addCrm(commonError, loginUser, crm);
		}else{
			crmService.updateCrm(commonError, loginUser, crm);
		}
		String msg = CommonUtils.getValidString(crm.getId()).isEmpty() ? CrmConstants.CRM_ADDED_SUCCESS : CrmConstants.CRM_UPDATED_SUCCESS ;
		customRedirector( msg, CrmConstants.CRM_LIST_PAGE);
		
	}
	
	/**
	 * This method will set the default country code
	 */
	public void dialogView(){
		crm = new Crm();
		crm.setCountry(CrmConstants.DEFAULT_COUNTRY_CODE);
	}
	
	
	
	/**
	 * This method process the image upload
	 * @param fileUploadEvent
	 * @throws IOException
	 */
	public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException{
		UploadedFile file = fileUploadEvent.getFile();
		uploadedFile = fileUploadEvent.getFile();
		String filePath = null;	
		 byte[] bytes = null;
		 if (file!=null) {
			 filePath = System.getProperty("config.dir") + "moduleUploads/crm/" ;
             bytes = file.getContents();
             String filename = FilenameUtils.getName(file.getFileName());
             crm.setPicture(filename);
             BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
             stream.write(bytes);
             stream.close();
         }
   }
				
	public Crm getCrm() {
		return crm;
	}

	public void setCrm(Crm crm) {
		this.crm = crm;
	}
	
	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<MultiVal> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<MultiVal> countryList) {
		this.countryList = countryList;
	}

	public List<MultiVal> getStateList() {
		return stateList;
	}

	public void setStateList(List<MultiVal> stateList) {
		this.stateList = stateList;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public List<Crm> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Crm> companyList) {
		this.companyList = companyList;
	}

    public String getDefaultView() {
		return currentView;
	}

	public void setDefaultView(String defaultView) {
		this.currentView = defaultView;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

				
}