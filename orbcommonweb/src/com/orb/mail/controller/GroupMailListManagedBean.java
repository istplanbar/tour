package com.orb.mail.controller;




import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.mail.bean.GroupMail;
import com.orb.mail.constants.MailConstants;
import com.orb.mail.controller.AbstractMailController;


@ManagedBean(name="groupMailView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class GroupMailListManagedBean extends AbstractMailController {
	
	private static Log logger = LogFactory.getLog(GroupMailListManagedBean.class);
	private List<GroupMail> groupUserList = null;
	private List<GroupMail> selectedGroupMail = null;
	private List<GroupMail> filteredMailList = null;
	private GroupMail groupMail = null; 
	private String groupMailId = null;
	private User loginUser = null;
	private List<User> userList = null;
	private List<String> selectedUsers = null;
	private List<Boolean> togglerList = null;
	private List<String> grpUserList = null;
	
	
	/**
	 * This method will be called when user clicks particular GroupMail
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadGroupMail")
	public String loadMail(ModelMap model){
		return MailConstants.MAIL_GROUP_HTML;				
	}
	
	/**
	 * This method will be called when user clicks particular GroupMailEdit
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadEditGroupMail") 
	public String manipulateGroupMail( HttpServletRequest request, ModelMap model){
	    groupMailId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( MailConstants.SELECTED_GROUP_MAIL_ID, groupMailId );
	    return MailConstants.MAIL_GROUPEDIT_HTML;				
	}

	
	/**
	 * This method will be called when user clicks particular GroupViewMail
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadGroupViewMail")
	public String viewGroupMail(HttpServletRequest request, ModelMap model){
		groupMailId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( MailConstants.SELECTED_GROUP_MAIL_ID, groupMailId );
		return MailConstants.MAIL_GROUPVIEW_HTML;
	}
	
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		groupUserList = mailService.getGroupUserList(commonError);
		groupUserList = getFilteredGroupUsers(groupUserList);
		togglerList = (List<Boolean>)getSessionObj(MailConstants.GROUPMAIL_TOGGLER_LIST) != null ? (List<Boolean>)getSessionObj(MailConstants.GROUPMAIL_TOGGLER_LIST) : Arrays.asList(false, true, true);
	    groupMailId = CommonUtils.getValidString(getJSFRequestAttribute( MailConstants.SELECTED_GROUP_MAIL_ID ));
	    userList = getAllUsersList();
	    selectedUsers = new ArrayList<String>();
	    if(groupMailId.isEmpty()){
			groupMail = new GroupMail();
	    }else{
	    groupMail = mailService.getGroupMail(commonError, groupMailId);
		    if(!CommonUtils.getValidString(groupMail.getListOfUser()).isEmpty()) {
		    	selectedUsers = Arrays.asList(groupMail.getListOfUser().split(","));
				for (int i = 0; i < selectedUsers.size(); i++) {
					String ns = selectedUsers.get(i).trim();
					selectedUsers.set(i, ns);
				}
				groupMail.setListOfUser(String.join(", ",selectedUsers));
			}
	    }
	}
	
	/**
	 * This method will be called where user can add a Group Mail
	 * @return
	 */
	public void addAction()	{
		redirector(MailConstants.MAIL_GROUPEDIT_PAGE);
	}


	/**
	 * This method will sent a Group mail in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		String fileNames = String.join(",", selectedUsers);
		groupMail.setListOfUser(fileNames);
		if( "".equals(CommonUtils.getValidString(groupMail.getId())) ){
			mailService.addGroupMail( commonError, loginUser, groupMail );
		}else{
			mailService.updateGroupMail( commonError, loginUser, groupMail );
		}
		String msg="".equals(CommonUtils.getValidString(groupMail.getId())) ? MailConstants.MAIL_GROUPADD_SUCCESS : MailConstants.MAIL_GROUPUPDATE_SUCCESS ;
		customRedirector( msg, MailConstants.MAIL_GROUP_PAGE);
		
	}
	
	/**
	 * This method to returns to Group List page
	 * @param list page
	 * @return 
	 */
	public void cancelViewAction(){
		addSessionObj(MailConstants.GROUPMAIL_TOGGLER_LIST , togglerList );
		redirector(MailConstants.MAIL_GROUP_PAGE);
	}
	
	
	/**
	 * This method will Required a Group User List in DB
	 * @param event
	 */
	 public List<String> completeUser(String query){
		query = query.toLowerCase();
		List<String> results = new ArrayList<String>();
		for(User u:getAllUsersList()){
			if(u.getFirstName().toLowerCase().contains(query) || u.getLastName().toLowerCase().contains(query))
				results.add(u.getFirstName()+' '+u.getLastName());
		}
		return results;
	}
	
	
	/**
	 * This method to returns to GroupList page
	 * @param list page
	 * @return 
	 */
    public void cancelEditAction(){
    	if(groupMailId.isEmpty()){
    		redirector(MailConstants.MAIL_GROUP_PAGE);
    	}else{
    		redirector(MailConstants.MAIL_GROUPVIEW_PAGE+"?objectId="+groupMailId);
    	}
    }
    
    /**
	 * This method will navigate to GroupMail View page
	 * @param groupMailId
	 * @return
	 */
	public void viewAction(String groupMailId){
		groupMail = mailService.getGroupMail(commonError, groupMailId);
		selectedUsers = Arrays.asList(groupMail.getListOfUser().split(","));
		addSessionObj(MailConstants.GROUPMAIL_TOGGLER_LIST , togglerList );
		redirector(MailConstants.MAIL_GROUPVIEW_PAGE+"?objectId="+groupMail.getIdentifier());
	}
    
    
   /**
	 * This method will navigate to GroupMail Edit page
	 * @param groupMailId
	 * @return
	 */
	public void editAction(String groupMailId){
		
		groupMail = mailService.getGroupMail(commonError, groupMailId);
		selectedUsers = Arrays.asList(groupMail.getListOfUser().split(","));
		addSessionObj(MailConstants.GROUPMAIL_TOGGLER_LIST , togglerList );
		redirector(MailConstants.MAIL_GROUPEDIT_PAGE+"?objectId="+groupMail.getIdentifier());
	}
    
	/**
	 * This method will delete the GroupMail
	 * @param event
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedGroupMail.isEmpty()){
		mailService.deleteGroupMail( commonError, loginUser, selectedGroupMail );
		if( commonError.getError().isEmpty() ){
			customRedirector( MailConstants.GROUP_DELETE_SUCCESS, MailConstants.MAIL_GROUP_PAGE);
		}
		}	
	}
	
	private List<GroupMail> getFilteredGroupUsers(List<GroupMail> groupList){
		for(GroupMail gp:groupList){
			if(gp != null && gp.getListOfUser() != null) {
				grpUserList = Arrays.asList(gp.getListOfUser().split(","));
				for (int i = 0; i < grpUserList.size(); i++) {
					String ns = grpUserList.get(i).trim();
					grpUserList.set(i, ns);
				}
				gp.setListOfUser(String.join(", ",grpUserList));
			}
		}
		return groupList;
	}
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param list page
	 * @return
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	
	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		GroupMailListManagedBean.logger = logger;
	}

	public List<GroupMail> getSelectedGroupMail() {
		return selectedGroupMail;
	}

	public void setSelectedGroupMail(List<GroupMail> selectedGroupMail) {
		this.selectedGroupMail = selectedGroupMail;
	}

	public List<GroupMail> getGroupUserList() {
		return groupUserList;
	}

	public void setGroupUserList(List<GroupMail> groupUserList) {
		this.groupUserList = groupUserList;
	}

	public GroupMail getGroupMail() {
		return groupMail;
	}

	public void setGroupMail(GroupMail groupMail) {
		this.groupMail = groupMail;
	}

	public String getGroupMailId() {
		return groupMailId;
	}

	public void setGroupMailId(String groupMailId) {
		this.groupMailId = groupMailId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<GroupMail> getFilteredMailList() {
		return filteredMailList;
	}

	public void setFilteredMailList(List<GroupMail> filteredMailList) {
		this.filteredMailList = filteredMailList;
	}

	public List<String> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<String> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public List<String> getGrpUserList() {
		return grpUserList;
	}

	public void setGrpUserList(List<String> grpUserList) {
		this.grpUserList = grpUserList;
	}

}