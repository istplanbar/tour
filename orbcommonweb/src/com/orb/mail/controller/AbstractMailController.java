package com.orb.mail.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.mail.service.MailService;

public abstract class AbstractMailController extends AbstractCommonController{
	
	@Autowired
	protected MailService mailService;

}
