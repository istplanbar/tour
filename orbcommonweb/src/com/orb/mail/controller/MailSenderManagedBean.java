package com.orb.mail.controller;



import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.mail.bean.GroupMail;
import com.orb.mail.bean.Mail;
import com.orb.mail.constants.MailConstants;


@ManagedBean(name="mailSenderView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")
public class MailSenderManagedBean extends AbstractMailController {
	
	private static Log logger = LogFactory.getLog(MailSenderManagedBean.class);
	private String mailId = null;
	private Mail mail = null;
	private List<Mail> mailSenderList = null;
	private List<Mail> mailReceiverList = null;
	private User loginUser = null;
	private List<Mail> filteredMailList = null;
	private List<Mail> selectedMail = null;
	private List<User> userList;
	private List<String> mailMenu = null;
	private String selectedMenu = null;
	private List<String> selectedUsers = null;
	private UploadedFile file;
	private List<String> uploadedFiles = new ArrayList<String>();
	private List<Boolean> togglerList = null;
	private long inboxCount;
    private List<Mail> deletedMailList = null;
    private List<String> mailList=new ArrayList<String>();
    private List<GroupMail> groupUserList = null;
    private List<Mail> filteredMailReceiverList = null;
    

	/**
	 * This method will be called when user clicks particular OutboxMail
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadOutboxMail")
	public String loadOutboxMail(ModelMap model){
				return MailConstants.MAIL_OUTBOX_HTML;
	}

	/**
	 * This method will be called when user clicks particular PostMail
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadPostMail")
	public String loadNewMessageMail( HttpServletRequest request,ModelMap model){
		String mailId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( MailConstants.SELECTED_MAIL_ID, mailId );
		return MailConstants.MAIL_POST_HTML;
	}


	/**
	 * This method will be called when user clicks particular OutboxViewmail from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadOutboxViewMail")
	public String viewoutboxMail(HttpServletRequest request, ModelMap model){
			mailId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
			model.addAttribute( MailConstants.SELECTED_MAIL_ID, mailId );
			return MailConstants.MAIL_OUTBOXVIEW_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){

		mailMenu = mailService.getMailMenu(1);
		selectedMenu = MailConstants.MAIL_INBOX;
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		mailSenderList = mailService.getMailSenderList(commonError);
		mailSenderList = mailSenderList.stream().filter(m -> loginUser.getUserId().equals(m.getSender())).collect(Collectors.toList());
		groupUserList = mailService.getGroupUserList(commonError);
		mailReceiverList = mailService.getMailReceiverList(commonError);
	    mailReceiverList = mailReceiverList.stream().filter(m -> m.getReceiver().equals(getUserFullName(loginUser.getUserId()))).collect(Collectors.toList());
		inboxCount = mailReceiverList.stream().filter(mr -> CommonConstants.FALSE.equals(mr.getFlag())).count();
		addSessionObj(MailConstants.MAIL_INBOX_COUNT, inboxCount);
		mailId = CommonUtils.getValidString(getJSFRequestAttribute( MailConstants.SELECTED_MAIL_ID ));
		togglerList = (List<Boolean>)getSessionObj(MailConstants.MAIL_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true, true,true);
		}
		if(mailId.isEmpty()){
			 mail = new Mail();
		}else{
			 mail = mailService.getSenderMail(commonError, mailId);
			 if(!"".equals(mail.getAttachment())){
				mailList = new ArrayList<String>(Arrays.asList(mail.getAttachment().split(",")));
			 }else{
			    mailList = new ArrayList<String>();
			 }
			 Set<String> hs = new HashSet<>();
			 hs.addAll(mailList);
			 mailList.clear();
			 mailList.addAll(hs);
			 mail.setAttachmentList(mailList);
			 filteredMailReceiverList = mailService.getFilteredMailReceiverList(commonError,mail.getIdentifier());
		}
	if(!CommonUtils.getValidString(mailId).isEmpty()){
			//mail = mailService.getSenderMail(commonError, mailId);
			if(mail.getCreateTime() != null){
				
				mail.setCreateTime(CommonUtils.convertFromUTC(loginUser.getTimeZone(), mail.getCreateTime()));
			}
		}
		
		if(!mailSenderList.isEmpty()){
			for(Mail mail : mailSenderList){
				if(mail.getCreateTime() != null){
					
					mail.setCreateTime(CommonUtils.convertFromUTC(loginUser.getTimeZone(), mail.getCreateTime()));
				}
			}
		}
	}


	/**
	 * This method will sent a mail in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		List<String> selectedReceivers = new ArrayList<>();
		Set<String> selectedReceiverList = new HashSet<>();
		String selectedUser = String.join(",", selectedUsers);
		String[] user=selectedUser.split(",");
		for(String s: user){
			String flag=CommonConstants.FALSE;
			for(GroupMail groupMail:getGroupUserList()){
				if(s.equals(groupMail.getGroupName())){
					selectedReceivers.addAll(Arrays.asList(groupMail.getListOfUser().split("\\s*,\\s*")));
					flag=CommonConstants.TRUE;
			    }
			}
			if(flag!=CommonConstants.TRUE){
			    selectedReceivers.add(s);
			}
	    }
		selectedReceiverList.addAll(selectedReceivers);
		selectedReceivers.clear();
		selectedReceivers.addAll(selectedReceiverList);
		mail.setMailReceiverList(selectedReceivers);
		String fileNames1 = String.join(",", uploadedFiles);
		mail.setAttachment(fileNames1);
		if( "".equals(CommonUtils.getValidString(mail.getId())) ){
			mailService.add( commonError, loginUser, mail );
		}
		customRedirector( MailConstants.MAIL_SENT_SUCCESS, MailConstants.MAIL_INBOX_PAGE);
	}


	/**
	 * This method will navigate to OutboxMail View page
	 * @param mailId
	 */
	public void senderViewAction(String mailId){
		addSessionObj(MailConstants.MAIL_TOGGLER_LIST , togglerList );
		redirector(MailConstants.MAIL_OUTBOXVIEW_PAGE+"?objectId="+mailId);
	}

	/**
	 * This method will delete the Mail
	 * @param event
	 */
	public void senderDeleteAction(ActionEvent e){
		if(!selectedMail.isEmpty()){
		    mailService.deleteSenderMail( commonError, loginUser, selectedMail );
			if( commonError.getError().isEmpty() ){
				selectedMenu = MailConstants.MAIL_OUTBOX;
				customRedirector( MailConstants.MAIL_DELETE_SUCCESS, MailConstants.MAIL_OUTBOX_PAGE);
			}
		}
	}
	
	/**
	 * Delete viewed Sender Mail
	 * @param mailId
	 */
	public void deleteSenderViewedMail(String mailId){
		mail = mailService.getSenderMail(commonError, mailId);
		mailService.deleteViewedSenderMail( commonError, loginUser, mail);
		if( commonError.getError().isEmpty() ){
			selectedMenu = MailConstants.MAIL_OUTBOX;
			customRedirector( MailConstants.MAIL_DELETE_SUCCESS, MailConstants.MAIL_OUTBOX_PAGE );
		}
	}
	
	/**
     * This method to returns to inbox page
	 * @param list page
	 * @return
	*/
    public void senderCancelAction(){
    	addSessionObj(MailConstants.MAIL_TOGGLER_LIST , togglerList );
    	selectedMenu = MailConstants.MAIL_OUTBOX;
    	redirector(MailConstants.MAIL_OUTBOX_PAGE);
    }
    
    /**
     * This method to returns to inbox page
	 * @param list page
	 * @return
	*/
    public void cancelAction(){
    	addSessionObj(MailConstants.MAIL_TOGGLER_LIST , togglerList );
    	redirector(MailConstants.MAIL_INBOX_PAGE);
    }
    
    /**
	 * This method process the row toggler
	 * @param pageId
	 */	
	public void onRowToggle(String mailId){
		filteredMailReceiverList = mailService.getFilteredMailReceiverList(commonError,mailId);
	}


   /**
    * This method to returns to userlist
    * @param query
    * @return
    */
    public List<String> completeUser(String query){
		query = query.toLowerCase();
		List<String> results = new ArrayList<String>();
		for(User u:getAllUsersList()){
			if(u.getFirstName().toLowerCase().contains(query) || u.getLastName().toLowerCase().contains(query))
				results.add(u.getFirstName()+' '+u.getLastName());
		}
		if(!groupUserList.isEmpty()){
			for(GroupMail groupMail:getGroupUserList()){
				results.add(groupMail.getGroupName());
			}
		}
		return results;
	}



	/**
	 * This method process the image upload
	 * @param file
	 * @param fileName
	 */
	public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException{
		UploadedFile uploadedFile = fileUploadEvent.getFile();
		file = fileUploadEvent.getFile();
		String filePath = null;
		 byte[] bytes = null;
		 String dirName = null;
		 
		 if(mail.getIdentifier() != null){
				if(mail.getDirName() != null){
					dirName = mail.getDirName();
				}else{
					dirName = mailService.getNextDir(mail.getId());
				}
		    }else{
				dirName = mailService.getNextDir(null);
			
		    }
		 mail.setDirName(dirName);
			
		 if (null != uploadedFile) {
			 filePath = System.getProperty("upload.dir") + "mail/" +dirName+"/";
		        File dir = new File(filePath);
		        if(!dir.exists())  dir.mkdir();;
             bytes = uploadedFile.getContents();
             String filename = FilenameUtils.getName(uploadedFile.getFileName());
             uploadedFiles.add(filename);
             BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
             stream.write(bytes);
             stream.close();
         }
    }
		
	/**
	 * Delete the file which is selected
	 * @param file
	 * @throws IOException
	 */
	public void deleteFile(String file) throws IOException{
		String path = System.getProperty("upload.dir") + "mail/" + mail.getDirName()+"/" +file +"/";
		File f = new File(path);
		if(f.exists()){
			f.delete();
			uploadedFiles.remove(file);
		}else{
		commonError.addError("err_docNotFound");
	    }
	}


	/**
	 * This method helps to download the Document
	 * @param model
	 */
	public void downloadFile(String fileVersion) throws IOException{

		String path = System.getProperty("upload.dir") + "mail/"+ mail.getDirName()+"/"+ fileVersion.replaceAll("^\\s+", "");;
		File downFile = new File(path);
		Path filePath = Paths.get(path);
		if(Files.exists(filePath) && !"".equals(fileVersion)){
			String fileName = downFile.getName();
			InputStream fis = new FileInputStream(downFile);

			byte[] buf = new byte[fis.available()];
			int offset = 0;
			int numRead = 0;
			while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length -offset)) >= 0))
				offset += numRead;
			fis.close();
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			response.setContentType(contentType);
			response.setHeader( "Content-Disposition", "attachment;filename=" + fileName );
			response.getOutputStream().write(buf);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			FacesContext.getCurrentInstance().responseComplete();
			commonError.clearErrors();
		}else{
			commonError.addError("err_docNotFound");
		}
	}


	/**
	 * This method will be called when user click the toggle to hide the column
	 * @param list page
	 * @return
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }

	public String getCriteriaDateFormat(String date){
		SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormat = new SimpleDateFormat(CommonConstants.MAIL_DATE_FORMAT);
		String reformattedStr = null;
		try {
		    reformattedStr = myFormat.format(fromUser.parse(date));
		} catch (ParseException e) {
		   logger.info(e.getMessage());
		}
		return reformattedStr;
		
	}
	
	public String getReceiverReadTime(String date){
		String reformattedStr = null;
		SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		if(!CommonUtils.getValidString(date).isEmpty()){
			try {
			    reformattedStr = myFormat.format(fromUser.parse(date));
			} catch (Exception e) {
			    logger.info(e.getMessage());
			}
		}
		return reformattedStr;
	}
	

	public static Log getLogger() {
		return logger;
	}

	public static void setLogger(Log logger) {
		MailSenderManagedBean.logger = logger;
	}

	public Mail getMail() {
		return mail;
	}

	public void setMail(Mail mail) {
		this.mail = mail;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Mail> getFilteredMailList() {
		return filteredMailList;
	}

	public void setFilteredMailList(List<Mail> filteredMailList) {
		this.filteredMailList = filteredMailList;
	}

	public List<Mail> getSelectedMail() {
		return selectedMail;
	}

	public void setSelectedMail(List<Mail> selectedMail) {
		this.selectedMail = selectedMail;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<Mail> getMailSenderList() {
		return mailSenderList;
	}

	public void setMailSenderList(List<Mail> mailSenderList) {
		this.mailSenderList = mailSenderList;
	}

	public List<Mail> getMailReceiverList() {
		return mailReceiverList;
	}

	public void setMailReceiverList(List<Mail> mailReceiverList) {
		this.mailReceiverList = mailReceiverList;
	}

	public List<String> getMailMenu() {
		return mailMenu;
	}

	public void setMailMenu(List<String> mailMenu) {
		this.mailMenu = mailMenu;
	}

	public String getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(String selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	public List<String> getSelectedUsers() {
		return selectedUsers;
	}


	public void setSelectedUsers(List<String> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}


	public UploadedFile getFile() {
		return file;
	}


	public void setFile(UploadedFile file) {
		this.file = file;
	}


	public List<String> getUploadedFiles() {
		return uploadedFiles;
	}


	public void setUploadedFiles(List<String> uploadedFiles) {
		this.uploadedFiles = uploadedFiles;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public List<Mail> getDeletedMailList() {
		return deletedMailList;
	}

	public void setDeletedMailList(List<Mail> deletedMailList) {
		this.deletedMailList = deletedMailList;
	}

	public List<String> getMailList() {
		return mailList;
	}

	public void setMailList(List<String> mailList) {
		this.mailList = mailList;
	}

	public List<GroupMail> getGroupUserList() {
		return groupUserList;
	}

	public void setGroupUserList(List<GroupMail> groupUserList) {
		this.groupUserList = groupUserList;
	}

	public long getInboxCount() {
		return inboxCount;
	}

	public void setInboxCount(long inboxCount) {
		this.inboxCount = inboxCount;
	}

	public List<Mail> getFilteredMailReceiverList() {
		return filteredMailReceiverList;
	}

	public void setFilteredMailReceiverList(List<Mail> filteredMailReceiverList) {
		this.filteredMailReceiverList = filteredMailReceiverList;
	}


}
