
package com.orb.admin.roleadministration.controller;

import java.util.ArrayList;



import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.admin.roleadministration.constants.RoleManagementConstants;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.roleadministration.service.RoleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;


@ManagedBean(name="roleManagementListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class RoleManagementBeanListView extends AbstractCommonController {
	
	private static Log logger = LogFactory.getLog(RoleManagementBeanListView.class);
	
	@Autowired
	private RoleManagementService roleManagementService;
	
	@Autowired
	private AdminService adminService;
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
		
	private List<Role> rolesList= null;
	private String roleId = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private User loginUser=null;
	private List<Boolean> togglerList = null;
	private List<Role> filteredRoleList = null;
	private List<Role> selectedRole = null;
	private Role role = null;
	private List<Role> roles = null;
	private List<Role> roleAcvtMapList=null;
	private List<Role> roleModMapList=null;
	private List<Map<String,List<MetaTable>>> roleModuleMapList = new ArrayList<>();

	
	
	/**
	 * This Method will be called to view the Layout list page when use clicks Role link on Top/Side Menu
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/loadRoleManagementList")
	public String loadRoleManagementList(HttpServletRequest httpRequest, ModelMap model){
		clearSessionObjects(model);
		return RoleManagementConstants.LIST_PAGE; 
	}
	
	
	/**
	 * This method will be called when user clicks particular Layout from list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/loadRoleManagementView")
	public String loadRoleManagementView(HttpServletRequest httpRequest, ModelMap model){
		roleId = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute(  CommonConstants.OBJECT_ID, roleId );
		return RoleManagementConstants.VIEW_PAGE; 
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		String showActive = CommonUtils.getValidString((String) getSessionObj( RoleManagementConstants.SHOW_ACTIVE_ROLE ));
		roleId = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.OBJECT_ID ));
		logger.info("Role Id: "+roleId);
		if(CommonConstants.N.equals(showActive)){
			rolesList=roleManagementService.getInactiveRoleList(commonError);
			showActivateBtn = CommonConstants.TRUE;
			showActiveListBtn = CommonConstants.TRUE;
		}else{
			rolesList=roleManagementService.getActiveRoleList(commonError);	
		}
		togglerList = (List<Boolean>)getSessionObj(RoleManagementConstants.ROLE_TOGGLER_LIST);
		if(togglerList==null){
		   togglerList = Arrays.asList(false, true, true, true);
		}
		if(!roleId.isEmpty()){
		   role = roleManagementService.getRole(commonError,roleId);
		   roleModMapList = roleManagementService.getRoleModMapList(commonError,roleId);
		   List<MetaTable> metaTableList = moduleManagementService.getAllMetaTableList(commonError);
		   roleModMapList.forEach(item->{item.setPageDesc(metaTableList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(MetaTable::getPageDesc)					
					.findAny().orElse(null));
					});
		   roleModMapList.forEach(item->{item.setParentPage(metaTableList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(MetaTable::getParentPage)					
					.findAny().orElse(null));
					});
		   roleModMapList.forEach(item->{item.setParentPageDesc(metaTableList.stream()
					.filter(x -> item.getParentPage()==x.getPageId()).map(MetaTable::getPageDesc)					
					.findAny().orElse(null));
					});
		}	   
	} 
		
	
	/**
	 * This method will navigate to the role management view page
	 * @param roleId
	 */
	public void viewAction(String roleId) {
		redirector(RoleManagementConstants.VIEW_PAGE_REDIR + "?objectId=" + roleId);
	}
	
	/**
	 * This method will navigate to the role management add page
	 */
	public void addAction(){
		redirector(RoleManagementConstants.ADD_UPDATE_PAGE_REDIR);
	}
	
	/**
	 * This method will navigate to the role management edit page
	 * @param roleId
	 */
	public void editAction(String roleId){
		redirector(RoleManagementConstants.ADD_UPDATE_PAGE_REDIR + "?objectId=" + roleId);
		
	}
	
	/**
	 * This method will navigate to the role management list page
	 */
	public void listAction(){
		redirector(RoleManagementConstants.LIST_PAGE_REDIR);
	}
	
	/**
	 * This method calls service for delete role
	 * @param identifier
	 */
	public void deleteRole(String identifier) {
		roleManagementService.deleteRole(commonError,loginUser,identifier);
		customRedirector( RoleManagementConstants.ROLE_DELETE_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR);
		
	}
	
	/**
	 * This method will get all the active Role
	 * @param event
	 */
	public void showActive(ActionEvent event){
		rolesList=roleManagementService.getActiveRoleList(commonError);
		addSessionObj(RoleManagementConstants.SHOW_ACTIVE_ROLE, CommonConstants.Y);
		if( filteredRoleList != null ) filteredRoleList.clear();
		  showActivateBtn = CommonConstants.FALSE;
		  showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will show all the inactive Role
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		rolesList=roleManagementService.getInactiveRoleList(commonError);
		if( filteredRoleList != null ) filteredRoleList.clear();
		  showActivateBtn = CommonConstants.TRUE;
		  showActiveListBtn = CommonConstants.TRUE;
	}
	
	/**
	 * This method locks Role
	 * @param e 
	 */
	public void lockAction(ActionEvent e){
		roleManagementService.lockRole( commonError, loginUser, selectedRole );
		showActive(e);
		customRedirector( RoleManagementConstants.ROLE_LOCKED_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR);
	}
		
	/**
	 * This method unlocks Role
	 * @param e
	 */
	public void unlockAction(ActionEvent e){
		roleManagementService.unlockRole( commonError, loginUser, selectedRole );
		showInactive(e);
		customRedirector( RoleManagementConstants.ROLE_UNLOCKED_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR );
	}
	
	/**
	 * This method locks viewed Role
	 * @param index
	 */
	public void lockViewedRole(String index){
		roleManagementService.lockViewedRole( commonError, loginUser, index );
		customRedirector( RoleManagementConstants.ROLE_LOCKED_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR);
	}

	/**
	 * This method unlocks viewed Role
	 * @param e
	 */
	public void unlockViewedRole(String index){
		roleManagementService.unlockViewedRole( commonError, loginUser, index );
		if( commonError.getError().isEmpty() ){
			customRedirector( RoleManagementConstants.ROLE_UNLOCKED_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR );
		}
	}
	
	
	/**
	 * This method will delete the Role
	 * @param e
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedRole.isEmpty()){
		   roleManagementService.delete( commonError, loginUser, selectedRole );
		   if( commonError.getError().isEmpty() ){
			 customRedirector( RoleManagementConstants.ROLE_DELETE_SUCCESS, RoleManagementConstants.LIST_PAGE_REDIR);
		   }
		   showInactive(e);
		   commonError.clearErrors();
		}
		else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(RoleManagementConstants.ROLE_DELETE_SUCCESS);
		}
			
	}
	
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param e
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	
	/**
	 * To clear the session variables
	 * @param model
	 */
   	private void clearSessionObjects( ModelMap model ){
    	String id = (String)model.get( CommonConstants.OBJECT_ID);
    	String showActive = (String)model.get( RoleManagementConstants.SHOW_ACTIVE_ROLE ); 
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( RoleManagementConstants.SHOW_ACTIVE_ROLE );
		if( showActive != null ) 
			model.remove( RoleManagementConstants.SHOW_ACTIVE_ROLE );
		if( toggleList != null ) model.remove( RoleManagementConstants.ROLE_TOGGLER_LIST );
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public List<Role> getRolesList() {
		return rolesList;
	}
	public void setRolesList(List<Role> rolesList) {
		this.rolesList = rolesList;
	}
		
	public RoleManagementService getRoleManagementService() {
		return roleManagementService;
	}

	public void setRoleManagementService(RoleManagementService roleManagementService) {
		this.roleManagementService = roleManagementService;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public User getLoginUser() {
		return loginUser;
	}
 

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public List<Role> getFilteredRoleList() {
		return filteredRoleList;
	}


	public void setFilteredRoleList(List<Role> filteredRoleList) {
		this.filteredRoleList = filteredRoleList;
	}


	public List<Role> getSelectedRole() {
		return selectedRole;
	}


	public void setSelectedRole(List<Role> selectedRole) {
		this.selectedRole = selectedRole;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}

	public List<Role> getRoles() {
		return roles;
	}


	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


	public List<Role> getRoleAcvtMapList() {
		return roleAcvtMapList;
	}


	public void setRoleAcvtMapList(List<Role> roleAcvtMapList) {
		this.roleAcvtMapList = roleAcvtMapList;
	}


	public List<Role> getRoleModMapList() {
		return roleModMapList;
	}


	public void setRoleModMapList(List<Role> roleModMapList) {
		this.roleModMapList = roleModMapList;
	}


	public List<Map<String,List<MetaTable>>> getRoleModuleMapList() {
		return roleModuleMapList;
	}


	public void setRoleModuleMapList(List<Map<String,List<MetaTable>>> roleModuleMapList) {
		this.roleModuleMapList = roleModuleMapList;
	}


	
}