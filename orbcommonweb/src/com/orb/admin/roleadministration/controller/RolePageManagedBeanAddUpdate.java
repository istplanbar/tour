
package com.orb.admin.roleadministration.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.admin.roleadministration.constants.RoleManagementConstants;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.roleadministration.service.RoleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;


@ManagedBean(name="roleManagementAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")


public class RolePageManagedBeanAddUpdate extends AbstractCommonController {
	
	private static Log logger = LogFactory.getLog(RolePageManagedBeanAddUpdate.class);
	
	@Autowired
	private RoleManagementService roleManagementService;
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
	
	private String roleName = null;
	private DualListModel<MetaTable> mainModules = null;
	private List<Role> subModuleLists = null;
	private boolean skip = false;
	private List<Role> selectedSubModules = null;
	private String roleId = null;
	private String roleTitle = null;
	private User loginUser = null;
	private Role role = null;
	private List<Role> roleModMapList = null;
	private List<MetaTable> filteredMetaTableList = null;
		
	
	
	

	/**
	 * This method will be called when user clicks Add button / Role Id link on list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadRoleManagementAddUpdate")
	public String loadAdminPage(HttpServletRequest httpRequest, ModelMap model){
		roleId = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( CommonConstants.OBJECT_ID, roleId );
		return RoleManagementConstants.ADD_UPDATE_PAGE;
	}
	
	
			
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		List<MetaTable> mainModuleList = new ArrayList<>();
		List<MetaTable> mainModulesTarget = new ArrayList<>();
		roleId =  CommonUtils.getValidString(getJSFRequestAttribute( CommonConstants.OBJECT_ID ));
		logger.info(roleId);
		mainModuleList = roleManagementService.getMainMenuList(commonError);
		List<MetaTable> metaTableList = moduleManagementService.getAllMetaTableList(commonError);
		if (roleId.isEmpty()){
			role = new Role();
			roleTitle = CommonConstants.ROLE_TITLE_CREATION;
		}else{
			role = roleManagementService.getRole(commonError,roleId);
			roleName=role.getRoleName();
		   	roleTitle = CommonConstants.ROLE_TITLE_UPDATE;
		   	roleModMapList = roleManagementService.getRoleModMapList(commonError,roleId);
		    roleModMapList.forEach(item->{item.setPageDesc(metaTableList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(MetaTable::getPageDesc)					
					.findAny().orElse(null));
					});
		   roleModMapList.forEach(item->{item.setParentPage(metaTableList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(MetaTable::getParentPage)					
					.findAny().orElse(null));
					});
		   roleModMapList.forEach(item->{item.setParentPageDesc(metaTableList.stream()
					.filter(x -> item.getParentPage()==x.getPageId()).map(MetaTable::getPageDesc)					
					.findAny().orElse(null));
					});
		   roleModMapList.forEach(item->{item.setMainModule(metaTableList.stream()
					.filter(x -> item.getParentPage()==x.getPageId()).map(MetaTable::getPageId)					
					.findAny().orElse(null));
					});
		   List<MetaTable> roleMappingMod = metaTableList.stream().filter(e -> (roleModMapList.stream()
                   .filter(d -> d.getMainModule() == e.getPageId() && d.getMainModule()!=1 && CommonConstants.Y.equals(e.getActive())).count())>0).collect(Collectors.toList());
		    mainModulesTarget.addAll(roleMappingMod);
		    mainModuleList = mainModuleList.stream().filter(e -> (mainModulesTarget.stream()
                    .filter(d -> d.getPageId()==e.getPageId() && CommonConstants.Y.equals(e.getActive())).count())<1).collect(Collectors.toList());
		    	  
		}
		mainModules = new DualListModel<>(mainModuleList, mainModulesTarget);
		
	}
	
	
	/**
	 * This method will navigate to the Role management list page
	 */
	public void listAction(){
		String msg="".equals(CommonUtils.getValidString(roleId)) ? RoleManagementConstants.ROLE_ADDED_SUCCESS : RoleManagementConstants.ROLE_UPDATED_SUCCESS ;
		customRedirector(msg,RoleManagementConstants.LIST_PAGE_REDIR);
		
	}
	
	/**
	 * This method will navigate to the Role management list page
	 */
	public void cancelAction(){
		if(roleId.isEmpty()){
			redirector(RoleManagementConstants.LIST_PAGE_REDIR);
		}else{
			redirector(RoleManagementConstants.VIEW_PAGE_REDIR + "?objectId=" + roleId);	
		}
		
	}
	
	 /**
	 * This method calls service to get the submodule list
	 */
	public void getSubModuleList(){
		List<MetaTable> targets = mainModules.getTarget();
		if (null != targets) {
			subModuleLists = roleManagementService.getSubModuleList(commonError, targets);
		}
		if(!roleId.isEmpty()){
			
			/*subModuleLists.forEach(item->{item.setActivities(roleModMapList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(Role::getActivities)					
					.findAny().orElse(null));
					});*/
			subModuleLists.forEach(item->{item.setActivities(roleModMapList.stream()
					.filter(x -> item.getPageId()==x.getPageId()).map(Role::getActivities)					
					.findAny().orElse(null));
					});
			if(roleModMapList!=null){
				selectedSubModules = new ArrayList<>();
				for(Role r:roleModMapList){
					selectedSubModules.addAll((List<Role>)subModuleLists.stream().filter(i -> i.getPageId()==r.getPageId()).collect(Collectors.toList()));
					for(Role role: subModuleLists){
						if(role.getPageId()==r.getPageId()){
							role.setChecked(true);
							break;
						}
					}
				}
				Collections.sort(subModuleLists, new Comparator<Role>() {
			        @Override
			        public int compare(Role abc1, Role abc2) {
			            return Boolean.compare(abc2.isChecked(),abc1.isChecked());
			        }
			    });
			}
			
		}
	}
		


	/**
	 * This method calls service for insert/update role
	 */
	public void createRole(ActionEvent event) {
		    if (selectedSubModules != null && ! selectedSubModules.isEmpty()){
		    	if(!commonError.getError().isEmpty()) commonError.clearErrors();
				if (roleId.isEmpty()) {
					roleManagementService.createRoles(commonError,selectedSubModules,role,loginUser);
				}else{
					role.setIdentifier(roleId);
					roleManagementService.updateRole(commonError,selectedSubModules,role,loginUser);
					addSessionObj(CommonConstants.LAST_LOGGEDIN_USER, null);
				}
				listAction();
			 }else{
			   commonError.addError(CommonConstants.ERR_MESSAGE_SUBMODULE);
			   return;
			 }
		
	}
	
	
	

	/**
	 * Wizard
	 * @param event
	 * @return
	 */
	public String onFlowRoleCreationProcess(FlowEvent event) {
		if(skip) {
			return CommonConstants.CONFIRM;
		}else {
			String nextStep = event.getNewStep();
			if (CommonConstants.SUB_MODULE_CREATION.equals(nextStep)){
				getSubModuleList();
			} else {
				getMainModules();
				
			}
			return event.getNewStep();
		}
	}
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public DualListModel<MetaTable> getMainModules() {
		return this.mainModules;
	}
	public void setMainModules(DualListModel<MetaTable> mainModules) {
		this.mainModules = mainModules;
	}
	public List<Role> getSubModuleLists() {
		return subModuleLists;
	}
	public void setSubModuleLists(List<Role> subModuleLists) {
		this.subModuleLists = subModuleLists;
	}
	public boolean isSkip() {
		return skip;
	}
	public void setSkip(boolean skip) {
		this.skip = skip;
	}
	
	
	public List<Role> getSelectedSubModules() {
		return selectedSubModules;
	}
	public void setSelectedSubModules(List<Role> selectedSubModules) {
		this.selectedSubModules = selectedSubModules;
	}
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public RoleManagementService getRoleManagementService() {
		return roleManagementService;
	}
	public void setRoleManagementService(RoleManagementService roleManagementService) {
		this.roleManagementService = roleManagementService;
	}
		
	public String getRoleTitle() {
		return roleTitle;
	}
	
	public void setRoleTitle(String roleTitle) {
		this.roleTitle = roleTitle;
	}

	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}

	public List<Role> getRoleModMapList() {
		return roleModMapList;
	}



	public void setRoleModMapList(List<Role> roleModMapList) {
		this.roleModMapList = roleModMapList;
	}



	public List<MetaTable> getFilteredMetaTableList() {
		return filteredMetaTableList;
	}



	public void setFilteredMetaTableList(List<MetaTable> filteredMetaTableList) {
		this.filteredMetaTableList = filteredMetaTableList;
	}

	
	
	
}