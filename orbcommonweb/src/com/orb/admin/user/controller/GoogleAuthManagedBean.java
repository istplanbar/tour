package com.orb.admin.user.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.useradministration.service.UserProfileSettingsService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="googleAuthManageView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/googleAuth")
public class GoogleAuthManagedBean extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(GoogleAuthManagedBean.class);
	
	@Autowired
	private UserProfileSettingsService userProfileService;
	
	@Autowired
	protected AdminService adminService;
	
	private User appUser = null;
	private static Map<String, String> adminConfMap = null;
	private String givenUserId = null;
	private String authCode = null;
	private List<User> googleAuthUserList = null;
	private String googleAuthUser = CommonConstants.FALSE;
	private boolean authBoolValue = false;
	private String secretKey = null;
	private String regPageView = null;
	private String registrationPin = null;
	
	/**
	 * This method will be called when user want to do Google Auth Registration
	 * @param request
	 * @return
	 */
	@RequestMapping("/openGoogleAuth")
    public String openGoogleAuth(HttpServletRequest httpRequest, ModelMap model, Locale locale, String language){
		adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	model.addAttribute( CommonConstants.USER_LOCALE, locale );
		return CommonConstants.GOOGLE_AUTH_OPEN_PAGE;
	}
	
	
	/**
	 * This method will be called to do Registration
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/regGoogleAuth")
	public String regGoogleAuth(HttpServletRequest httpRequest, ModelMap model, Locale locale, String language){
		adminConfMap = adminService.getAdminConfMap(commonError);
		String userId = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		regPageView = CommonUtils.getValidString( httpRequest.getParameter("view") );
		model.addAttribute( "USER_ID", userId );
		model.addAttribute( CommonConstants.GOOGLE_AUTH_REGISTRATION_PAGE, regPageView );
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	model.addAttribute( "google_Auth_Msg", CommonConstants.GOOGLE_AUTH_MAIL_CHECK );
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	model.addAttribute( CommonConstants.USER_LOCALE, locale );
		return CommonConstants.GOOGLE_AUTH_REG_PAGE;
	}
	
	
	/**
	 * This method will be called when user is redirect to Google Page Entry page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/validateGoogleAuthEntry")
	public String validateGoogleAuthEntry(HttpServletRequest httpRequest, ModelMap model, Locale locale, String language){
		HttpSession session = httpRequest.getSession();
		User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
		String userId = null;
		if(user != null) userId = user.getUserId();
		
		model.addAttribute( "USER_ID", userId );
		return CommonConstants.GOOGLE_AUTH_ENTRY_PAGE;
	}
	
	/**
	 * on load action
	 */
	public void onLoadAction(){
		adminConfMap = adminService.getAdminConfMap(commonError);
		givenUserId = CommonUtils.getValidString(getJSFRequestAttribute( "USER_ID" ));
		googleAuthUserList = userProfileService.getGoogleAuthUserList(commonError);
		regPageView = CommonUtils.getValidString(getJSFRequestAttribute( CommonConstants.GOOGLE_AUTH_REGISTRATION_PAGE ));
		logger.info(regPageView);
		if(CommonConstants.GOOGLE_AUTH_REGISTRATION_PAGE.equals(regPageView)){
			User appUser = userProfileService.getLoginUser(givenUserId);
			String googleAuthPin = googleAuthUserList.stream().filter(googleAuth -> googleAuth.getUserId().equals(givenUserId)).map(g -> g.getRegistrationPin()).findAny().orElse(null);
			if(!CommonUtils.getValidString(googleAuthPin).isEmpty()){
				infoMessage(getJSFRequestAttribute( "google_Auth_Msg" ));
			}
			genRandomSecretKey(appUser);
		}
		
	}
	
	public void enableGoogleAuthenticator(){
		appUser = userProfileService.getLoginUser(givenUserId);
		if(appUser == null){
			errorMessage(CommonConstants.INVALID_USERNAME);
		}else{
			try{
				adminService.sendGoogleAuthPin(commonError, appUser);
				redirector(CommonConstants.GOOGLE_AUTH_REG+"?objectId="+givenUserId+"&view="+CommonConstants.GOOGLE_AUTH_REGISTRATION_PAGE);
			}catch(Exception e){
				logger.info("Exception while generating google authentication barcode"+e.getMessage());
			}
		}
	}
	
	
	public void googleAuthAction(){
		/*googleAuthUser = googleAuthUserList.stream().filter(user -> user.getUserId().equals(givenUserId)).map(u -> CommonConstants.TRUE).findAny().orElse(null);
		if(CommonConstants.TRUE.equals(googleAuthUser)){
			RequestContext.getCurrentInstance().execute("confirmRegister.show()");
		}else{*/
		enableGoogleAuthenticator();
		/*}*/
	}
	
	private void genRandomSecretKey(User appUser){
		try{
			SecureRandom random = new SecureRandom();
		    byte[] bytes = new byte[20];
		    random.nextBytes(bytes);
		    Base32 base32 = new Base32();
		    secretKey = base32.encodeToString(bytes);
		    secretKey = secretKey.toLowerCase().replaceAll("(.{4})(?=.{4})", "$1 ");
		    logger.info("Secrete: "+secretKey);
		    getGoogleAuthenticatorBarCode(secretKey, appUser.getEmail(), adminConfMap.get("companyName"), appUser);
		}catch(Exception e){
			logger.info(e.getMessage());
		}
	}
	
	public void reRegisterAction(String userId){
		givenUserId = userId;
		userProfileService.updateGoogleAuthUser(commonError ,userId);
		
	}
	
	public void closeDialog(){
		RequestContext.getCurrentInstance().execute("confirmRegister.hide()");
	}
	public void validateToken(){
		try {
			User user = userProfileService.getLoginUser(givenUserId);
			String secretKey = adminService.getGoogleAuthSecrets(user);
			String url = null;
			String lastCode = null;
			//while (true) {
			logger.info("Given Code = " + authCode);
			String code = getTOTPCode(secretKey);
			logger.info("Generated Code =" + code);
			if (code.equals(authCode)) {
				//infoMessage(CommonConstants.VALID_AUTH_TOKEN);
				redirector(CommonConstants.DASHBOARD);
			}else{
			    errorMessage(CommonConstants.INVALID_AUTH_TOKEN);
			}
			
			//Thread.sleep(100);
			} catch (Exception e) {
			   	logger.info(e.getMessage());
			   	commonError.addError(CommonConstants.INVALID_AUTH_TOKEN);
			};
		//}
	}
	
	private String getTOTPCode(String secretKey) {
	    String normalizedBase32Key = secretKey.replace(" ", "").toUpperCase();
	    Base32 base32 = new Base32();
	    byte[] bytes = base32.decode(normalizedBase32Key);
	    String hexKey = Hex.encodeHexString(bytes);
	    long time = (System.currentTimeMillis() / 1000) / 30;
	    String hexTime = Long.toHexString(time);
	    return TOTPAuthTokenGenerator.generateTOTP(hexKey, hexTime, "6");
	}
	
	
	private void getGoogleAuthenticatorBarCode(String secretKey, String account, String issuer, User appUser) {
	    String normalizedBase32Key = secretKey.replace(" ", "").toUpperCase();
	    try {
	    	String path = System.getProperty("upload.dir") + "googleauthqr/" + appUser.getUserId()+"_qr.png";
	        String encodedStr = "otpauth://totp/"
	        + URLEncoder.encode(issuer + ":" + account, "UTF-8").replace("+", "%20")
	        + "?secret=" + URLEncoder.encode(normalizedBase32Key, "UTF-8").replace("+", "%20")
	        + "&issuer=" + URLEncoder.encode(issuer, "UTF-8").replace("+", "%20");
	        logger.info("Secrete: "+encodedStr);
	        createQRCode(encodedStr, path, 150, 150);
	        
	    } catch (WriterException | IOException e) {
	    	logger.info("Error: "+e.getMessage());
	    	commonError.addError("Error while enabling google authentication");
	        throw new IllegalStateException(e);
	    }
	}
	
	private void createQRCode(String barCodeData, String filePath, int height, int width) throws WriterException, IOException {
		BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE, width, height);
		try (FileOutputStream out = new FileOutputStream(filePath)) {
			MatrixToImageWriter.writeToStream(matrix, "png", out);
		}catch (IOException e) {
			logger.info("Error: "+e.getMessage());
	    	commonError.addError("Error while enabling google authentication");
		}
	}
	
	public void validateRegPin(String regPin){
		String googleAuthPin = googleAuthUserList.stream().filter(googleAuth -> googleAuth.getUserId().equals(givenUserId)).map(g -> g.getRegistrationPin()).findAny().orElse(null);
		if(regPin.equals(googleAuthPin)){
			authBoolValue = true;
		}else{
			authBoolValue = false;
			commonError.addError(CommonConstants.INVALID_REG_PIN);
		}
		
	}
	
	
	public void cancelAction(ActionEvent e){
		redirector(ForgotPasswordConstant.LOGIN_PAGE);
	}
	
	public void addSecretKey(ActionEvent e){
		User user = userProfileService.getLoginUser(givenUserId);
		adminService.addGoogleAuthSecrets(commonError, user, secretKey);
		redirector(ForgotPasswordConstant.LOGIN_PAGE);
	}
	
	public Map<String, String> getAdminConfMap() {
		return adminConfMap;
	}

	public void setAdminConfMap(Map<String, String> adminConfMap) {
		this.adminConfMap = adminConfMap;
	}

	public String getGivenUserId() {
		return givenUserId;
	}

	public void setGivenUserId(String givenUserId) {
		this.givenUserId = givenUserId;
	}


	public String getAuthCode() {
		return authCode;
	}


	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}


	public List<User> getGoogleAuthUserList() {
		return googleAuthUserList;
	}


	public void setGoogleAuthUserList(List<User> googleAuthUserList) {
		this.googleAuthUserList = googleAuthUserList;
	}


	public String getGoogleAuthUser() {
		return googleAuthUser;
	}


	public void setGoogleAuthUser(String googleAuthUser) {
		this.googleAuthUser = googleAuthUser;
	}


	public boolean isAuthBoolValue() {
		return authBoolValue;
	}


	public void setAuthBoolValue(boolean authBoolValue) {
		this.authBoolValue = authBoolValue;
	}


	public String getSecretKey() {
		return secretKey;
	}


	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}


	public String getRegPageView() {
		return regPageView;
	}


	public void setRegPageView(String regPageView) {
		this.regPageView = regPageView;
	}


	public String getRegistrationPin() {
		return registrationPin;
	}


	public void setRegistrationPin(String registrationPin) {
		this.registrationPin = registrationPin;
	}

	
}
