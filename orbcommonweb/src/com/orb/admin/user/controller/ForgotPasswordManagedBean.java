package com.orb.admin.user.controller;

import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.model.ForgotPass;
import com.orb.admin.forgotpassword.service.ForgotPassService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="forgotPassManageView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/resetpass")
@SessionAttributes({CommonConstants.USER_LOCALE})
public class ForgotPasswordManagedBean extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(ForgotPasswordManagedBean.class);
	
	@Autowired
	private ForgotPassService forgotPassService;
	
	@Autowired
	protected AdminService adminService;

	private String givenUserId = null;
	private ForgotPass forgotPass = null;
	private String tempPass = null;
	private String savedTempPass = null;
	private String newPassword = null;
	private String confirmPassword = null;
	private String linkForEmail = null;
	private String passPreMessage = null;
	Map<String, String> adminConfMap = null;
	private String passwordExpiredMsg = null;

	/**
	 * 
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/forgotPass")
	public String forgotPasswordView(HttpServletRequest httpRequest, ModelMap model, Locale locale, String language){
		clearSessionObjects(model);
    	
    	adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	model.addAttribute( CommonConstants.USER_LOCALE, locale );
    	
		int port = httpRequest.getServerPort();
		StringBuffer baseURL = new StringBuffer();
        if( port > 1000 ){
        	baseURL.append( "http://" );
        	baseURL.append( httpRequest.getServerName() );
        	baseURL.append( ":" );
        	baseURL.append( port );
        }else{
        	baseURL.append( "https://" );
        	baseURL.append( httpRequest.getServerName() );
        }
		model.addAttribute( ForgotPasswordConstant.URLForForgotPass, baseURL.toString() );
		return ForgotPasswordConstant.FORGOT_PASS_PAGE;
	}

	/**
	 * 
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/resetPass")
	public String resetPasswordView(HttpServletRequest httpRequest, ModelMap model, Locale locale, String language){
		clearSessionObjects(model);
    	
    	adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	model.addAttribute( CommonConstants.USER_LOCALE, locale );
   		String hash = CommonUtils.getValidString( httpRequest.getParameter( ForgotPasswordConstant.USER_ACTION ) );

		model.addAttribute( ForgotPasswordConstant.HASH_VALUE, hash );
		return ForgotPasswordConstant.RESET_PASS_PAGE;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		adminConfMap = adminService.getAdminConfMap(commonError);
	     String passwordValidation = adminConfMap.get("passwordChangeValid");
		linkForEmail = CommonUtils.getValidString(getJSFRequestAttribute(( ForgotPasswordConstant.URLForForgotPass )));
		if(CommonConstants.TRUE.equals((String)getSessionObj(CommonConstants.PASSWORD_EXP_MSG )) && passwordValidation.equals("Yes")){
			   passwordExpiredMsg = getJSFMessage(CommonConstants.USER_CREDENTIAL_EXPIRED);
		}
		if(linkForEmail.isEmpty()){
			String hashValue = CommonUtils.getValidString(getJSFRequestAttribute(( ForgotPasswordConstant.HASH_VALUE )));
			if(hashValue.isEmpty()){
				errorMessage(ForgotPasswordConstant.INVALID_URL);
			} else {
				
				forgotPass = forgotPassService.fetchForgotPassDetails(commonError, hashValue);
				if (null != forgotPass){
					savedTempPass = forgotPass.getTempPassword();
					passPreMessage = (getJSFMessage(ForgotPasswordConstant.PASSWORD_REGEX_MESSAGE));
				}else {
					errorMessage(ForgotPasswordConstant.URL_EXPIRED);
				}
			}
		}
	}
	
	
	/**
	 * This method will call the services to check the userId, password generation and send email
	 */
	public void forgotPassword(){
		String returnValue = forgotPassService.forgotPasswordService(commonError, givenUserId, linkForEmail, adminConfMap);
		if(null != returnValue){
			errorMessage(returnValue);
		} else {
			cancelAction(ForgotPasswordConstant.CHECK_MAIL);
		}
	}

	/**
	 * This method will call the reset password service to insert new password
	 */
	public void resetPassword(){
		
		Map<String, String> adminConfMap = null;
		adminConfMap = adminService.getAdminConfMap(commonError);
		String passwordValidation = adminConfMap.get("passwordValidation");
		
		if (newPassword.equals(confirmPassword)){
			String pattern;
			if(passwordValidation.equals("No")){
				pattern = ForgotPasswordConstant.PASSWORD_REGEX; 
			}else{
				pattern = newPassword;
			}
		    if ( newPassword.matches(pattern)) {
		    	if (tempPass.equals(forgotPass.getTempPassword())){
		    		logger.info(tempPass);
		    		logger.info(forgotPass.getTempPassword());
					if (forgotPassService.resetPassword(commonError, newPassword, forgotPass)){
						infoMessage(ForgotPasswordConstant.RESET_DONE);
						cancelAction(ForgotPasswordConstant.RESET_DONE);
					}else{
						// Error message when previous passwords match with new password
						errorMessage(ForgotPasswordConstant.UNIQUE_PASSWORD);
					}
				}else{
					// Error message when token is wrong
					errorMessage(ForgotPasswordConstant.WRONG_TEMP_PASS);
				}
		    }else {
		    	errorMessage(ForgotPasswordConstant.PASSWORD_PATTERN);
		    }
		}else{
			errorMessage(ForgotPasswordConstant.PASSWORD_MATCH);
		}
	}
	
	/**
	 * Redirect to login page
	 */
	public void cancelAction(String redirectAction){
		if (redirectAction.isEmpty()) {
			redirector(ForgotPasswordConstant.LOGIN_PAGE);
		}else if (redirectAction.equals(ForgotPasswordConstant.CHECK_MAIL)){
			redirector(ForgotPasswordConstant.LOGIN_PAGE_FORGOT);
		}else{
			redirector(ForgotPasswordConstant.LOGIN_PAGE_RESET_SUCCESS);
		}
	}

	/**
	 * To clear the session variables
	 * @param model
	 */
	private void clearSessionObjects( ModelMap model ){
		String hash = (String)model.get( ForgotPasswordConstant.HASH_VALUE);
		String url = (String)model.get( ForgotPasswordConstant.URLForForgotPass );

		if( hash != null ) model.remove( ForgotPasswordConstant.HASH_VALUE );
		if( url != null ) model.remove( ForgotPasswordConstant.URLForForgotPass );
	}

	public String getGivenUserId() {
		return givenUserId;
	}

	public void setGivenUserId(String givenUserId) {
		this.givenUserId = givenUserId;
	}

	public CommonError getCommonError() {
		return commonError;
	}

	public void setCommonError(CommonError commonError) {
		this.commonError = commonError;
	}

	public ForgotPass getForgotPass() {
		return forgotPass;
	}

	public void setForgotPass(ForgotPass forgotPass) {
		this.forgotPass = forgotPass;
	}
	
	public String getTempPass() {
		return tempPass;
	}

	public void setTempPass(String tempPass) {
		this.tempPass = tempPass;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getLinkForEmail() {
		return linkForEmail;
	}

	public void setLinkForEmail(String linkForEmail) {
		this.linkForEmail = linkForEmail;
	}

	public String getSavedTempPass() {
		return savedTempPass;
	}

	public void setSavedTempPass(String savedTempPass) {
		this.savedTempPass = savedTempPass;
	}
	
	public String getPassPreMessage() {
		return passPreMessage;
	}

	public void setPassPreMessage(String passPreMessage) {
		this.passPreMessage = passPreMessage;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public Map<String, String> getAdminConfMap() {
		return adminConfMap;
	}

	public void setAdminConfMap(Map<String, String> adminConfMap) {
		this.adminConfMap = adminConfMap;
	}

	public String getPasswordExpiredMsg() {
		return passwordExpiredMsg;
	}

	public void setPasswordExpiredMsg(String passwordExpiredMsg) {
		this.passwordExpiredMsg = passwordExpiredMsg;
	}

}