package com.orb.admin.user.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.useradministration.service.UserProfileSettingsService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.security.encrypt.EncryptionUtil;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.service.TaskPlannerService;

@ManagedBean(name="userProfileView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")
public class UserProfileSettings  extends AbstractCommonController {

	private static Log logger = LogFactory.getLog(UserProfileSettings.class);
	private User loginUser = null;
	private String ingePwd = null;
	private UploadedFile file = null;
	private User user = null;
	private List<User> userList = null;
	private List<Task> taskList = null;
	private int taskListSize;
	private String secondLevelAuthDisable = CommonConstants.DISABLE;
	
	@Autowired
	private UserProfileSettingsService userProfileService;
	
	@Autowired
	protected TaskPlannerService taskPlannerService;

	
	private Set<String> userDateFormats = CommonConstants.DATE_FORMAT;
	/**
	 * This method laod user profile settings page
	 * @param httpRequest
	 * @param model
	 * @param locale
	 * @return
	 */
	@RequestMapping("/loadUserProfile")
	public String loadUserProfile(HttpServletRequest httpRequest){
		return CommonConstants.USER_PROFILE_PAGE;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		if(commonError != null) commonError.clearErrors();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		user = userProfileService.getLoginUser( loginUser.getUserId());
		ingePwd = user.getWsPassword();
		if(!"".equals(CommonUtils.getValidString(user.getWsPassword()))){
			user.setWsPassword(EncryptionUtil.decrypt(user.getWsPassword()));
		}
		userList = getAllUsersList();
		if(userList != null)
			userList = userList.stream().filter(p -> !p.getUserId().equals(loginUser.getUserId())).collect(Collectors.toList());
	}
	@SuppressWarnings("unchecked")
	@PostConstruct
    public void init(){
		user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		if(user != null){
			taskList = taskPlannerService.getUserTaskList(commonError, user);
			if(taskList !=  null)
				taskListSize = taskList.size();

		}
	}
	
	
	/**
	 * This methods save user profile settings to DB
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public void saveProfile() throws InterruptedException, IOException{
		if( commonError.getError().isEmpty() ){
			try {
				if(!user.getWsPassword().isEmpty())
					user.setWsPassword(EncryptionUtil.encrypt(user.getWsPassword()));
				else
					user.setWsPassword(ingePwd);
			} catch (Exception e) {
					logger.info(e.getMessage());
			}
			userProfileService.saveProfileSettings( commonError, loginUser.getUserId(), user);
			setSessionObject(user);
			infoMessage("msg_settingsUploaded");
		} else 
			return;
			
	}
	
	/**
	 * This method initiates the  user image upload
	 */
	
	public void uploadImage() throws InterruptedException, IOException{
		if( commonError.getError().isEmpty() ){
			if(!"".equals(file.getFileName())){
			String ext = getExtension(file.getFileName());
			String fileName = String.format(loginUser.getUserId() + "." + ext.toLowerCase());
			loginUser.setUserImage(fileName);
			fileUpload(file,fileName);
			userProfileService.saveProfileSettings( commonError, loginUser.getUserId(), loginUser);
			}
			
		}else return;
	}

	/**
	 * This methods redirects to dashboard page
	 */
	public void cancelAction(){
		redirector("/common/process/dashboard/loadDashboard.html");
	}
	
	/**
	 * This method process the image upload
	 * @param file
	 * @param fileName
	 */
	public void fileUpload(UploadedFile file, String fileName) {
		InputStream input = null;
		OutputStream output = null;
	    try {
	    	input = file.getInputstream();
		    output = new FileOutputStream(new File(System.getProperty("config.dir") + "images/", fileName));
		    File path = new File(System.getProperty("config.dir") + "images/", fileName);
		   if(path.exists()){
			   path.delete();
		    }
	        IOUtils.copy(input, output);
	        infoMessage("msg_fotoUpload");
	    } catch (IOException e) {
		} finally {
	        IOUtils.closeQuietly(input);
	        IOUtils.closeQuietly(output);
	    }
	    this.file = file;
	   
	}
	
	/**
	 * This method validates the input image format 
	 * @throws ValidatorException
	 */
	@SuppressWarnings("null")
	public void fileFormatValidation(FacesContext ctx, UIComponent component, Object value) throws ValidatorException{
		UploadedFile uploadedFile = (UploadedFile)value;
		if(!"".equals(uploadedFile.getFileName())){
		String name = uploadedFile.getFileName();
		String ext = getExtension(name);
		if( "".equals(name) || CommonConstants.IMAGE_FORMAT.contains(ext.toLowerCase())){
			commonError.clearErrors();
		}else{
			commonError.addError("err_invalidImageFormat");
		}			
		}
	}
	
	public void fileUploadListener(FileUploadEvent e) throws IOException{
		try {
		UploadedFile uploadedPhoto=e.getFile();
		String filePath = System.getProperty("config.dir") + "images/";
        byte[] bytes=null;
        if (null!=uploadedPhoto) {
    	   	bytes = uploadedPhoto.getContents();
    	   	String ext = getExtension(uploadedPhoto.getFileName());
   		  	String userImage = String.format(loginUser.getUserId() + "." + ext.toLowerCase());
   		  	
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+userImage)));
            stream.write(bytes);
            stream.close();
			 user.setUserImage(userImage);
            loginUser.setUserImage(userImage);
		 }
       	} catch (Exception exception) {
    			logger.info("Error upload");
    }
      
    }
	
	/**
	 * This method helps to change user image to default image
	 * @return
	 */
	public void setDefaultImage(){
		loginUser.setUserImage("");
		user.setUserImage("");
	}
		 
	/**
	 * This method helps to re-register google authentication 
	 */
	public void deRegisterAction(ActionEvent actionEvent) {
		userProfileService.deRegisterGoogleAuth(commonError, loginUser.getUserId());
		customRedirector( CommonConstants.DE_REGISTER_GOOGLE_AUTH, CommonConstants.USER_PROFILE_HTML);
		
    }
		
	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Set<String> getUserDateFormats() {
		return userDateFormats;
	}

	public void setUserDateFormats(Set<String> userDateFormats) {
		this.userDateFormats = userDateFormats;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}
	
	public int getTaskListSize() {
		return taskListSize;
	}

	public void setTaskListSize(int taskListSize) {
		this.taskListSize = taskListSize;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public String getSecondLevelAuthDisable() {
		return secondLevelAuthDisable;
	}

	public void setSecondLevelAuthDisable(String secondLevelAuthDisable) {
		this.secondLevelAuthDisable = secondLevelAuthDisable;
	}

}



