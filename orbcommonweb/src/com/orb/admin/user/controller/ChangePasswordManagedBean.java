package com.orb.admin.user.controller;

import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.model.ChangePassword;
import com.orb.admin.forgotpassword.service.ChangePasswordService;
import com.orb.admin.forgotpassword.service.ForgotPassService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.EncryptionUtil;

@ManagedBean(name="changePassView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")
public class ChangePasswordManagedBean  extends AbstractCommonController {

	private static Log logger = LogFactory.getLog(ChangePasswordManagedBean.class);
	
	private ChangePassword changePassword = new ChangePassword();
	private static EncryptionUtil encrypt = null;
	
	@Autowired 
	private ChangePasswordService changePassService;
	
	@Autowired
	private ForgotPassService forgotPassService;
	
	@Autowired
	private AdminService adminService;
	
	@RequestMapping("/loadChangePassPage")
    public String loadChangePassPage(HttpServletRequest httpRequest, Locale locale){
		String mylang = locale.getDisplayLanguage();
			
    	return "admin/changePassword.xhtml";
    }
	
	public void changePass() throws NullPointerException {
		
		Map<String, String> adminConfMap = null;
		User loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		String curPass = changePassword.getCurrentPass();
		adminConfMap = adminService.getAdminConfMap(commonError);
		String passwordValidation = adminConfMap.get("passwordValidation");
		
		try{
			if(encrypt.matchPassword(curPass,loginUser.getPassword())){
				if((changePassword.getNewPass()).equals(changePassword.getRepeatPass())){
					String pattern;
					if(passwordValidation.equals("No")){
					
						 pattern = ForgotPasswordConstant.PASSWORD_REGEX;
					} else{
						pattern=changePassword.getNewPass();
					}
										
					if ( changePassword.getNewPass().matches(pattern)) {
						if(changePassService.matchPreviousPasswords(commonError,loginUser.getUserId(),changePassword.getNewPass())){
							String encryptPass = encrypt.encryptBCrypt(changePassword.getNewPass());
							String user = loginUser.getUserId();
							changePassService.updatePassword(encryptPass,user);
							infoMessage("success_password"); //Return success when condition is true
						}else
							errorMessage(ForgotPasswordConstant.UNIQUE_PASSWORD);//error out when matches with last 3 passwords.
					}else
						errorMessage(ForgotPasswordConstant.PASSWORD_PATTERN); //error out when password is not matching
				}else 
					errorMessage("err_match_password"); //error when new and repeat password does not match
			}else
				errorMessage("err_wrongCurrentPass"); //when current password is wrong
		
		}catch(Exception e){
			logger.error("Error : "+e.getMessage());
		}
		
	}
	
	public void cancelAction(){
		redirector(CommonConstants.DASHBOARD);
	}
	
	public ChangePassword getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(ChangePassword changePassword) {
		this.changePassword = changePassword;
	}

	
	
	
}


