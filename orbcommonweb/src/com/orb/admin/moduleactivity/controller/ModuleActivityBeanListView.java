package com.orb.admin.moduleactivity.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.constants.ModuleActivityConstants;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.moduleactivity.service.ModuleActivityService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="moduleActivityListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class ModuleActivityBeanListView extends AbstractCommonController {
	
	private static Log logger = LogFactory.getLog(ModuleActivityBeanListView.class);
	
	@Autowired 
	private ModuleActivityService moduleActivityService;
	
	private ModuleActivity moduleActivity = null;
	private String modActivityId = null;
	private List<ModuleActivity> filteredActivityList = null;
	private List<ModuleActivity> selectedActivity = null;
	private List<Boolean> togglerList = null;
	private List<ModuleActivity> moduleActivityList = null;
	private String activityTitle = null;
	private DualListModel<String> modSelectionList = null;
	private List<MetaTable> moduleList = null;
	private List<String> moduleNameList = null;
	private User loginUser = null;
	private List<String> pageIdList = null;
	private List<String> moduleNameSelectionList = null;
	private List<String> activityTargetList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;

	/**
	 * This Method will be called to view the ModuleActivity list page when use clicks User link on Top/Side Menu
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleActivity")
	public String loadModuleActivityList(HttpServletRequest request, ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return ModuleActivityConstants.LIST_HTML; 
	}
	
	
	/**
	 * This method will be called when ModuleActivity clicks particular User from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleActivityView")
	public String loadModuleView(HttpServletRequest request, ModelMap model){
		modActivityId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( ModuleActivityConstants.SELECTED_ACTIVITY_ID, modActivityId );
		return ModuleActivityConstants.VIEW_HTML; 
	}
	
	/**
	 * This method will be called when ModuleActivity add button is clicked
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleActivityAddUpdate")
	public String loadModuleActivityAddUpdate(HttpServletRequest request, ModelMap model){
		modActivityId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( ModuleActivityConstants.SELECTED_ACTIVITY_ID, modActivityId );
		return ModuleActivityConstants.EDIT_HTML; 
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	
	public void onLoadAction(){
		pageIdList = new ArrayList<>();
		moduleNameSelectionList = new ArrayList<>();
		activityTargetList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		modActivityId = CommonUtils.getValidString(getJSFRequestAttribute( ModuleActivityConstants.SELECTED_ACTIVITY_ID ));
		logger.info(modActivityId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		
		if(CommonConstants.N.equals(showActive)){
			moduleActivityList=moduleActivityService.getInactiveModuleActivityList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE); 
		}else{
			moduleActivityList=moduleActivityService.getActiveModuleActivityList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(ModuleActivityConstants.ACTIVITY_TOGGLER_LIST);
		  if(togglerList==null){
			togglerList = Arrays.asList(false, true, true, true,true);
		  } 
		if(modActivityId.isEmpty()){
			moduleActivity = new ModuleActivity();
			activityTitle = ModuleActivityConstants.ACTIVITY_TITLE_CREATION;
		}else{
			moduleActivity = moduleActivityService.getModuleActivity(commonError, modActivityId);
			activityTitle = ModuleActivityConstants.ACTIVITY_TITLE_UPDATE;
		}
		
	}
	
	/**
	 * This method will navigate to the module activity add page
	 */
	
	public void addAction(){
		redirector(ModuleActivityConstants.EDIT_PAGE);
    }
	
	/**
	 * This method will navigate to the module activity view page
	 * @param activityId
	 */
	public void viewAction(String activityId){
			redirector(ModuleActivityConstants.VIEW_PAGE+"?objectId="+activityId);
	}
	
	/**
	 * This method will navigate to the module activity edit page
	 * @param activityId
	 */
	public void editAction(String activityId){
		redirector(ModuleActivityConstants.EDIT_PAGE+"?objectId="+activityId);
	}
	
	/**
	 * This method will either call add or update action
	 */
    public void saveAction(){
		if("".equals(CommonUtils.getValidString(moduleActivity.getId()))){
			moduleActivityService.add( commonError,loginUser, moduleActivity );
		}else{
			moduleActivityService.update( commonError, loginUser, moduleActivity );
		}
		String msg="".equals(CommonUtils.getValidString(moduleActivity.getId())) ? ModuleActivityConstants.ACTIVITY_ADDED_SUCCESS : ModuleActivityConstants.ACTIVITY_UPDATED_SUCCESS ;
		customRedirector( msg, ModuleActivityConstants.LIST_PAGE);
		
	}
    

	/**
	 * This method will navigate to the module activity list page directly from add page
	 */
    public void cancelAction(){
		redirector(ModuleActivityConstants.LIST_PAGE +"?activeFlag="+moduleActivity.getActive());
	}
    
    /**
	 * This method will navigate from module activity add page to view page then to list page
	 */
	public void backView(){
		if(modActivityId.isEmpty()){
			redirector(ModuleActivityConstants.LIST_PAGE);
		}else{
			redirector(ModuleActivityConstants.VIEW_PAGE+"?objectId="+modActivityId);	
		}
	}
	
	/**
	 * This method will delete the Activity
	 * @param e
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedActivity.isEmpty()){
			moduleActivityService.deleteActivity( commonError, loginUser, selectedActivity );
			if( commonError.getError().isEmpty() ){
				customRedirector( ModuleActivityConstants.ACTIVITY_DELETE_SUCCESS, ModuleActivityConstants.LIST_PAGE);
				commonError.clearErrors();
			}
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(ModuleActivityConstants.ACTIVITY_DELETE_SUCCESS);
		}
			
	}
	
	/** This method will get all the active Module Activity
	 * @param event
	*/
	public void showActive(ActionEvent event){
		moduleActivityList=moduleActivityService.getActiveModuleActivityList(commonError);
		addSessionObj(ModuleActivityConstants.SHOW_ACTIVE_MODULE_ACTIVITY, CommonConstants.Y);
		if( filteredActivityList != null ) filteredActivityList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will show all the inactive Module Activity
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		moduleActivityList=moduleActivityService.getInactiveModuleActivityList(commonError);
		if( filteredActivityList != null ) filteredActivityList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	
	/**
	 * This method locks Module Activity
	 * @param e
	 */
	public void lockAction(ActionEvent e){
		moduleActivityService.lockModuleActivity( commonError, loginUser, selectedActivity );
		showActive(e);
		customRedirector( ModuleActivityConstants.MODULE_ACTIVITY_LOCKED_SUCCESS, ModuleActivityConstants.LIST_PAGE);
	}
	
	
	/**
	 * This method unlocks Module Activity
	 * @param e
	 */
	public void unlockAction(ActionEvent e){
		moduleActivityService.unlockModuleActivity( commonError, loginUser, selectedActivity );
		showInactive(e);
		customRedirector( ModuleActivityConstants.MODULE_ACTIVITY_UNLOCKED_SUCCESS, ModuleActivityConstants.LIST_PAGE );
	}
	
	/**
	 * This method locks viewed Module Activity
	 * @param index
	 */
	public void lockViewedModuleActivity(String index){
		moduleActivity = moduleActivityService.getModuleActivity(commonError, index);
		moduleActivityService.lockViewedModuleActivity( commonError, loginUser, moduleActivity );
		customRedirector( ModuleActivityConstants.MODULE_ACTIVITY_LOCKED_SUCCESS, ModuleActivityConstants.LIST_PAGE);
	}

	/**
	 * This method unlocks viewed Module Activity
	* @param index
	 */
	public void unlockViewedModuleActivity(String index){
		moduleActivity = moduleActivityService.getModuleActivity(commonError, index);
		moduleActivityService.unlockViewedModuleActivity( commonError, loginUser, moduleActivity );
		if( commonError.getError().isEmpty() ){
			customRedirector( ModuleActivityConstants.MODULE_ACTIVITY_UNLOCKED_SUCCESS, ModuleActivityConstants.LIST_PAGE );
		}
	}
	
	/**
	 * Delete viewed Module Activity
	 * @param index
	 */
	public void deleteViewedModuleActivity(String index){
		moduleActivity = moduleActivityService.getModuleActivity(commonError, index);
		moduleActivityService.deleteViewedModuleActivity( commonError, loginUser, moduleActivity);
		if( commonError.getError().isEmpty() ){
			customRedirector( ModuleActivityConstants.MODULE_ACTIVITY_DELETE_SUCCESS, ModuleActivityConstants.LIST_PAGE );
		}
	}
	
	/**
	 * To clear the session variables
	 * @param model
	 */
	
    @SuppressWarnings("rawtypes")
    private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( ModuleActivityConstants.SELECTED_MODULE_ACTIVITY_ID);
		String showActive = (String)model.get( ModuleActivityConstants.SHOW_ACTIVE_MODULE_ACTIVITY ); 
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( ModuleActivityConstants.SHOW_ACTIVE_MODULE_ACTIVITY );
		if( showActive != null ) 
			model.remove( ModuleActivityConstants.SHOW_ACTIVE_MODULE_ACTIVITY );
		if( toggleList != null ) model.remove( ModuleActivityConstants.ACTIVITY_TOGGLER_LIST );
	}
	
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	public ModuleActivity getModuleActivity() {
		return moduleActivity;
	}

	public void setModuleActivity(ModuleActivity moduleActivity) {
		this.moduleActivity = moduleActivity;
	}

	public String getModActivityId() {
		return modActivityId;
	}

	public void setModActivityId(String modActivityId) {
		this.modActivityId = modActivityId;
	}


	public List<ModuleActivity> getFilteredActivityList() {
		return filteredActivityList;
	}


	public void setFilteredActivityList(List<ModuleActivity> filteredActivityList) {
		this.filteredActivityList = filteredActivityList;
	}


	public List<ModuleActivity> getSelectedActivity() {
		return selectedActivity;
	}


	public void setSelectedActivity(List<ModuleActivity> selectedActivity) {
		this.selectedActivity = selectedActivity;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public List<ModuleActivity> getModuleActivityList() {
		return moduleActivityList;
	}


	public void setModuleActivityList(List<ModuleActivity> moduleActivityList) {
		this.moduleActivityList = moduleActivityList;
	}
	
	public ModuleActivityService getModuleActivityService() {
		return moduleActivityService;
	}


	public void setModuleActivityService(ModuleActivityService moduleActivityService) {
		this.moduleActivityService = moduleActivityService;
	}


	public String getActivityTitle() {
		return activityTitle;
	}


	public void setActivityTitle(String activityTitle) {
		this.activityTitle = activityTitle;
	}


	public DualListModel<String> getModSelectionList() {
		return modSelectionList;
	}


	public void setModSelectionList(DualListModel<String> modSelectionList) {
		this.modSelectionList = modSelectionList;
	}


	public List<MetaTable> getModuleList() {
		return moduleList;
	}


	public void setModuleList(List<MetaTable> moduleList) {
		this.moduleList = moduleList;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<String> getPageIdList() {
		return pageIdList;
	}


	public void setPageIdList(List<String> pageIdList) {
		this.pageIdList = pageIdList;
	}


	public List<String> getModuleNameSelectionList() {
		return moduleNameSelectionList;
	}


	public void setModuleNameSelectionList(List<String> moduleNameSelectionList) {
		this.moduleNameSelectionList = moduleNameSelectionList;
	}


	public List<String> getActivityTargetList() {
		return activityTargetList;
	}


	public void setActivityTargetList(List<String> activityTargetList) {
		this.activityTargetList = activityTargetList;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<String> getModuleNameList() {
		return moduleNameList;
	}


	public void setModuleNameList(List<String> moduleNameList) {
		this.moduleNameList = moduleNameList;
	}


}
