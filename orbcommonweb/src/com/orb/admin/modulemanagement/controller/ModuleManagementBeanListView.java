package com.orb.admin.modulemanagement.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.moduleactivity.service.ModuleActivityService;
import com.orb.admin.modulemanagement.constants.ModuleManagementConstants;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="moduleManagementListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class ModuleManagementBeanListView extends AbstractCommonController {
	
	private static Log logger = LogFactory.getLog(ModuleManagementBeanListView.class);
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
	@Autowired 
	private ModuleActivityService moduleActivityService;
	
	private MetaTable metaTable = null;
	private List<MetaTable> mainModules = null;
	private List<MetaTable> subModules = null;
	private List<MetaTable> childModules = null;
	private List<MetaTable> metaTableList = null;	
	private User user = null;
	private String moduleId = null;
	private List<MetaTable> selectedModules=null;
	private List<Boolean> togglerList = null;
	private List<MetaTable> filteredModuleList = null;
	private List<MetaTable> allMetaTableList = null;
	private String parentPageName = null;
	private String subParentPageDesc = null;
	private String childParentPageDesc = null;
	private String parentPageDesc = null;
	private User loginUser=null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private String rowExpansion = CommonConstants.FALSE;
	private List<ModuleActivity> activityNameList = null;
	
	/**
	 * To access modules list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleManagement")
	public String loadModuleManagementList(HttpServletRequest request, ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return ModuleManagementConstants.LIST_HTML; 
	}
	
	/**
	 * To access modules view page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleManagementView")
	public String loadModuleView(HttpServletRequest request, ModelMap model){
		moduleId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( ModuleManagementConstants.SELECTED_MODULE_ID, moduleId );
		return ModuleManagementConstants.VIEW_HTML; 
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		allMetaTableList = moduleManagementService.getAllMetaTableList(commonError);
	    activityNameList =new ArrayList<>();
		user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		moduleId = CommonUtils.getValidString(getJSFRequestAttribute( ModuleManagementConstants.SELECTED_MODULE_ID ));
		logger.info("Module Id" +moduleId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		togglerList = (List<Boolean>)getSessionObj(ModuleManagementConstants.MODULES_TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList(false, true, true, true, true);
		}
		mainModules=moduleManagementService.getParentPageList(commonError);
		if(CommonConstants.N.equals(showActive)){
			metaTableList=moduleManagementService.getInactiveMetaTableList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			metaTableList=moduleManagementService.getActiveMetaTableList(commonError);
		}
		if("".equals(CommonUtils.getValidString(moduleId))){
			for(MetaTable metaTable:allMetaTableList){
				for(MetaTable mT:metaTableList)
				if(metaTable.getPageId()==mT.getParentPage()){
					setParentPageName("root");
					break;
				}
			}
			metaTable = new MetaTable();
		}else{
			activityNameList=moduleActivityService.getActiveModuleActivityList(commonError);
			metaTable = moduleManagementService.getMetaTable(commonError, moduleId);
		}
	}
	
	/**
	 * This method will navigate to the module management view page
	 * @param pageId
	 */
	public void viewAction(String pageId){
		redirector(ModuleManagementConstants.VIEW_PAGE+"?objectId="+pageId);
	}
	
	/**
	 * This method will navigate to the module management edit page
	 * @param pageId
	 */
	public void editAction(String pageId){
		redirector(ModuleManagementConstants.EDIT_PAGE+"?objectId="+pageId);
	}
	
	/**
	 * This method to returns to modules list page
	 */
	public void cancelAction(){
		redirector(ModuleManagementConstants.LIST_PAGE +"?activeFlag="+metaTable.getActive());
	}

	/**
	 * This method will get all the active module
	 * @param event
	 */
	public void showActive(ActionEvent event){
		metaTableList=moduleManagementService.getActiveMetaTableList(commonError);
		addSessionObj(ModuleManagementConstants.SHOW_ACTIVE_MODULE, CommonConstants.Y);
		if( filteredModuleList != null ) filteredModuleList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}

	/**
	 * This method will show all the inactive Modules
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		metaTableList=moduleManagementService.getInactiveMetaTableList(commonError);
		if( filteredModuleList != null ) filteredModuleList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}
	
	/**
	 * This method locks Module
	 * @param e
	 */
	public void lockAction(ActionEvent e){
		getSelectedFilModules(selectedModules);		
		moduleManagementService.lockMetaTable( commonError, loginUser, selectedModules );
		getTaskPlannerStatus();
		showActive(e);
		if( commonError.getError().isEmpty() ){
		  customRedirector( ModuleManagementConstants.MODULE_LOCKED_SUCCESS, ModuleManagementConstants.LIST_PAGE);
		}
	}
	
	/**
	 * This method locks Sub Module
	 * @param e
	 */
	public void subModLockAction(MetaTable m){
		List<MetaTable> subModList = new ArrayList<>();
		subModList.add(m);
		List<MetaTable> childModList = allMetaTableList.stream().filter(mt-> mt.getParentPage() == m.getPageId()).collect(Collectors.toList());
		if(!childModList.isEmpty()){
			subModList.addAll(childModList);
		}
		moduleManagementService.lockMetaTable( commonError, loginUser, subModList );
		getTaskPlannerStatus();
		allMetaTableList = moduleManagementService.getAllMetaTableList(commonError);
		infoMessage(ModuleManagementConstants.SUB_MODULE_LOCKED_SUCCESS);
	}
	
	/**
	 * This method locks Sub Module
	 * @param m
	 */
	public void subModUnLockAction(MetaTable m){
		List<MetaTable> subModList = new ArrayList<>();
		subModList.add(m);
		List<MetaTable> childModList = allMetaTableList.stream().filter(mt-> mt.getParentPage() == m.getPageId()).collect(Collectors.toList());
		if(!childModList.isEmpty()){
			subModList.addAll(childModList);
		}
		moduleManagementService.unlockMetaTable( commonError, loginUser, subModList );
		getTaskPlannerStatus();
		allMetaTableList = moduleManagementService.getAllMetaTableList(commonError);
		infoMessage(ModuleManagementConstants.SUB_MODULE_UNLOCKED_SUCCESS);
	}
		
	/**
	 * This method unlocks Modules
	 * @param e
	 */
	public void unlockAction(ActionEvent e){
		getSelectedFilModules(selectedModules);
		moduleManagementService.unlockMetaTable( commonError, loginUser, selectedModules );
		getTaskPlannerStatus();
		showInactive(e);
		if( commonError.getError().isEmpty() ){
		  customRedirector( ModuleManagementConstants.MODULE_UNLOCKED_SUCCESS, ModuleManagementConstants.LIST_PAGE );
		}
	}
	
	/**
	 * This method calls service for delete module
	 * @param e
	 */
	public void deleteAction(ActionEvent e){
		getSelectedFilModules(selectedModules);
		moduleManagementService.delete( commonError, loginUser, selectedModules );	
		getTaskPlannerStatus();
 		customRedirector( ModuleManagementConstants.MODULE_DELETE_SUCCESS, ModuleManagementConstants.LIST_PAGE );
	}
	
	/**
	 * This method calls service for delete module
	 * @param e
	 */
	public void subModdeleteAction(String index){
		metaTable = moduleManagementService.getMetaTable(commonError, index);
		List<MetaTable> subModList = new ArrayList<>();
		List<MetaTable> childModList = new ArrayList<>();
		subModList.add(metaTable);
		for(MetaTable mt:subModList){
			for(MetaTable metaTable:allMetaTableList){
				if(mt.getPageId()==metaTable.getParentPage()){
					childModList.add(metaTable);
				}
			}
		}
		subModList.addAll(childModList);
		moduleManagementService.delete( commonError, loginUser, subModList );
		getTaskPlannerStatus();
		if( commonError.getError().isEmpty() ){
			customRedirector( ModuleManagementConstants.SUB_MODULE_DELETE_SUCCESS, ModuleManagementConstants.LIST_PAGE );
			
		}
	}
	
	/**
	 * This method locks viewed Module
	 * @param index
	 */
	public void lockViewedMetaTable(String index){
		metaTable = moduleManagementService.getMetaTable(commonError, index);
		List<MetaTable> metaTableList = new ArrayList<>();
		if(metaTable.getParentPage() == 1){
			metaTableList.add(metaTable);
			getSelectedFilModules(metaTableList);
			moduleManagementService.lockMetaTable( commonError, loginUser, metaTableList );
		}else{
		  moduleManagementService.lockViewedMetaTable( commonError, loginUser, metaTable );
		}
		getTaskPlannerStatus();
		customRedirector( ModuleManagementConstants.MODULE_LOCKED_SUCCESS, ModuleManagementConstants.LIST_PAGE);
	}

	/**
	 * This method unlocks viewed Module
	 * @param index
	 */
	public void unlockViewedMetaTable(String index){
		metaTable = moduleManagementService.getMetaTable(commonError, index);
		List<MetaTable> metaTableList = new ArrayList<>();
		if(metaTable.getParentPage() == 1){
			metaTableList.add(metaTable);
			getSelectedFilModules(metaTableList);
			moduleManagementService.unlockMetaTable( commonError, loginUser, metaTableList );
		}else{
		     moduleManagementService.unlockViewedMetaTable( commonError, loginUser, metaTable );
		}
		getTaskPlannerStatus();
		if( commonError.getError().isEmpty() ){
			customRedirector( ModuleManagementConstants.MODULE_UNLOCKED_SUCCESS, ModuleManagementConstants.LIST_PAGE );
		}
	}

	/**
	 * Delete viewed Module
	 * @param index
	 */
	public void deleteViewedMetaTable(String index){
		metaTable = moduleManagementService.getMetaTable(commonError, index);
		List<MetaTable> metaTableList = new ArrayList<>();
		if(metaTable.getParentPage() == 1){
			metaTableList.add(metaTable);
			getSelectedFilModules(metaTableList);
			moduleManagementService.delete( commonError, loginUser, metaTableList );
		}else{
		   moduleManagementService.deleteViewedMetaTable( commonError, loginUser, metaTable);
		}
		getTaskPlannerStatus();
		if( commonError.getError().isEmpty() ){
			customRedirector( ModuleManagementConstants.MODULE_DELETE_SUCCESS, ModuleManagementConstants.LIST_PAGE );
			
		}
	}
	
    /**
	 * This method process the row toggler
	 * @param pageId
	 */
	public void onRowToggle(String pageId){
		parentPageDesc = null;
		subModules = moduleManagementService.getSubModuleList( commonError, pageId );
		subModules.forEach(item->{item.setParentPageDesc(allMetaTableList.stream()
				.filter(x -> item.getPageId()==x.getParentPage()).map(i -> CommonConstants.TRUE)					
				.findAny().orElse(null));
				});
		setSubParentPageDesc(allMetaTableList.stream().filter(mt -> Integer.parseInt(pageId) == mt.getPageId()).map(MetaTable::getPageDesc)					
				.findAny().orElse(null));
		setParentPageDesc(allMetaTableList.stream().filter(mt -> Integer.parseInt(pageId) == mt.getParentPage()).map(i -> subParentPageDesc)					
				.findAny().orElse(null));
		
		subModules.forEach(item->{item.setSubLock(allMetaTableList.stream()
				.filter(x -> item.getPageId()==x.getPageId() && x.getActive().equals(CommonConstants.Y)).map(i -> true)					
				.findAny().orElse(false));
				});
	}
	
	/**
	 * This method process the row toggler
	 * @param pageId
	 */	
	public void onRowToggle1(String pageId){
		parentPageDesc = null;
		for(MetaTable metaTable:subModules){
			int i=Integer.parseInt(pageId);
			for(MetaTable mT:allMetaTableList){
				if(metaTable.getPageId()==mT.getParentPage()){
					rowExpansion = CommonConstants.TRUE;	
				}else{
					rowExpansion = CommonConstants.FALSE;	
				}
			}
			if(i==metaTable.getPageId()){
				childModules = moduleManagementService.getSubModuleList( commonError, pageId );
			}
			
		}
		int i=Integer.parseInt(pageId);
		for(MetaTable metaTable:allMetaTableList){
			if(i==metaTable.getPageId()){
				setChildParentPageDesc(metaTable.getPageDesc());
				setParentPageDesc(childParentPageDesc);
				break;
			}
		}
		childModules.forEach(item->{item.setSubLock(allMetaTableList.stream()
				.filter(x -> item.getPageId()==x.getPageId() && x.getActive().equals(CommonConstants.Y)).map(k -> true)					
				.findAny().orElse(false));
				});
	}
	
	private List<MetaTable> getSelectedFilModules(List<MetaTable> metaTableList){
		List<MetaTable> subModuleList = new ArrayList<>();
		List<MetaTable> childModuleList = new ArrayList<>();
		if(!metaTableList.isEmpty()){
			for(MetaTable m:metaTableList){
				for(MetaTable metaTable:allMetaTableList){
					if(m.getPageId()==metaTable.getParentPage()){
						subModuleList.add(metaTable);
					}
				}
			}
			metaTableList.addAll(subModuleList);
			for(MetaTable m:subModuleList){
				for(MetaTable metaTable:allMetaTableList){
					if(m.getPageId()==metaTable.getParentPage()){
						childModuleList.add(metaTable);
					}
				}
			}
			metaTableList.addAll(childModuleList);
	    }
		return metaTableList;
		
	}
	
	/**
	 * This method will navigate to the module management add page
	 */
	public void addAction(){
			redirector(ModuleManagementConstants.EDIT_PAGE);
	}
	
	private void getTaskPlannerStatus(){
		List<MetaTable> mainModules =  moduleManagementService.getAllMetaTableList(commonError);
        addSessionObj(CommonConstants.TASK_PLANNER_ACTIVE, mainModules.stream().filter(x -> x.getPageId() == 33 && CommonConstants.Y.equals(x.getActive())).map(i -> CommonConstants.TRUE).findAny().orElse(null));
        addSessionObj(CommonConstants.LAST_LOGGEDIN_USER, null);
	}
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param list page
	 * @return
	 */
    public void onToggle(ToggleEvent e) {
	        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
    
    /**
	 * To clear the session variables
	 * @param model
	 */
   	private void clearSessionObjects( ModelMap model ){
    	String id = (String)model.get( ModuleManagementConstants.SELECTED_MODULE_ID);
    	String showActive = (String)model.get( ModuleManagementConstants.SHOW_ACTIVE_MODULE ); 
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( ModuleManagementConstants.SHOW_ACTIVE_MODULE );
		if( showActive != null ) 
			model.remove( ModuleManagementConstants.SHOW_ACTIVE_MODULE );
		if( toggleList != null ) model.remove( ModuleManagementConstants.MODULES_TOGGLER_LIST );
	}

	public ModuleManagementService getModuleManagementService() {
			return moduleManagementService;
	}

	public void setModuleManagementService(ModuleManagementService moduleManagementService) {
			this.moduleManagementService = moduleManagementService;
	}
	public List<MetaTable> getSelectedModules() {
			return selectedModules;
	}

	public void setSelectedModules(List<MetaTable> selectedModules) {
			this.selectedModules = selectedModules;
	}

	public MetaTable getMetaTable() {
			return metaTable;
	}

	public void setMetaTable(MetaTable metaTable) {
			this.metaTable = metaTable;
	}

	public List<MetaTable> getMainModules() {
			return mainModules;
		}

	public void setMainModules(List<MetaTable> mainModules) {
			this.mainModules = mainModules;
	}

	public User getUser() {
			return user;
	}

	public void setUser(User user) {
			this.user = user;
	}
	public String getModuleId() {
			return moduleId;
	}

	public void setModuleId(String moduleId) {
			this.moduleId = moduleId;
	}

	public List<MetaTable> getMetaTableList() {
			return metaTableList;
	}

	public void setMetaTableList(List<MetaTable> metaTableList) {
			this.metaTableList = metaTableList;
	}
	public List<MetaTable> getSubModules() {
			return subModules;
	}
	public void setSubModules(List<MetaTable> subModules) {
			this.subModules = subModules;
	}
	
	public List<Boolean> getTogglerList() {
			return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
			this.togglerList = togglerList;
	}

	public List<MetaTable> getFilteredModuleList() {
		return filteredModuleList;
	}

	public void setFilteredModuleList(List<MetaTable> filteredModuleList) {
		this.filteredModuleList = filteredModuleList;
	}

	public List<MetaTable> getAllMetaTableList() {
		return allMetaTableList;
	}

	public void setAllMetaTableList(List<MetaTable> allMetaTableList) {
		this.allMetaTableList = allMetaTableList;
	}

	public String getParentPageName() {
		return parentPageName;
	}

	public void setParentPageName(String parentPageName) {
		this.parentPageName = parentPageName;
	}

	public String getSubParentPageDesc() {
		return subParentPageDesc;
	}

	public void setSubParentPageDesc(String subParentPageDesc) {
		this.subParentPageDesc = subParentPageDesc;
	}

	public String getChildParentPageDesc() {
		return childParentPageDesc;
	}

	public void setChildParentPageDesc(String childParentPageDesc) {
		this.childParentPageDesc = childParentPageDesc;
	}

	public String getParentPageDesc() {
		return parentPageDesc;
	}

	public void setParentPageDesc(String parentPageDesc) {
		this.parentPageDesc = parentPageDesc;
	}

	public String getRowExpansion() {
		return rowExpansion;
	}

	public void setRowExpansion(String rowExpansion) {
		this.rowExpansion = rowExpansion;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<ModuleActivity> getActivityNameList() {
		return activityNameList;
	}


	public void setActivityNameList(List<ModuleActivity> activityNameList) {
		this.activityNameList = activityNameList;
	}

	public ModuleActivityService getModuleActivityService() {
		return moduleActivityService;
	}


	public void setModuleActivityService(ModuleActivityService moduleActivityService) {
		this.moduleActivityService = moduleActivityService;
	}

	public List<MetaTable> getChildModules() {
		return childModules;
	}

	public void setChildModules(List<MetaTable> childModules) {
		this.childModules = childModules;
	}


			
}