package com.orb.admin.modulemanagement.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.moduleactivity.service.ModuleActivityService;
import com.orb.admin.modulemanagement.constants.ModuleManagementConstants;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="moduleManagementAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")
public class ModuleManagementAddUpdateManagedBean extends AbstractCommonController {
	
	private static Log logger = LogFactory.getLog(ModuleManagementAddUpdateManagedBean.class);
     
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
	@Autowired 
	private ModuleActivityService moduleActivityService; 
	
	private MetaTable metaTable = null;
	private User loginUser=null;
	private String moduleId = null;
	private List<ModuleActivity> activityNameList = null;
	private DualListModel<ModuleActivity> activityList = null;
	private String subModuleValue = null;
	private List<MetaTable> subModules = null;
	private  UploadedFile file = null;
	private List<MetaTable> allMetaTableList = null;
	private String moduleTitle = null;
	private List<MetaTable> mainModules = null;
	private String showChildCheckBox = CommonConstants.FALSE;
	
	/**
	 * To access modules add/update page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadModuleManagementAddUpdate")
	public String loadAddUpdate(HttpServletRequest request, ModelMap model){
		moduleId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( ModuleManagementConstants.SELECTED_MODULE_ID, moduleId );
		return ModuleManagementConstants.EDIT_HTML; 
	}
	
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		activityNameList =new ArrayList<>();
		List<ModuleActivity> activitySource = new ArrayList<>();
	    List<ModuleActivity> activityTarget = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		moduleId = CommonUtils.getValidString(getJSFRequestAttribute( ModuleManagementConstants.SELECTED_MODULE_ID ));
	    logger.info(moduleId);
		mainModules=moduleManagementService.getParentPageList(commonError);
		allMetaTableList = moduleManagementService.getAllMetaTableList(commonError);
	    if(moduleId.isEmpty()){
	    	metaTable = new MetaTable();
	    	moduleTitle = ModuleManagementConstants.MODULE_TITLE_CREATION;
	    }else{
	    	activityNameList=moduleActivityService.getActiveModuleActivityList(commonError);
	    	metaTable = moduleManagementService.getMetaTable(commonError, moduleId);
	    	if(metaTable.getParentPage()!=1){
				  showChildCheckBox = CommonConstants.TRUE;  
			}
	    	moduleTitle = ModuleManagementConstants.MODULE_TITLE_UPDATE;
	    	if(CommonConstants.TRUE.equals(metaTable.getChildModuleConfig())){
				metaTable.setParentPage(allMetaTableList.stream()
						.filter(x -> metaTable.getParentPage()==x.getPageId()).map(MetaTable::getParentPage)					
						.findAny().orElse(null));
				subModuleList(String.valueOf(metaTable.getParentPage()));
				Integer i = allMetaTableList.stream()
						.filter(x -> metaTable.getParentPage()==x.getPageId()).map(MetaTable::getPageId)					
						.findAny().orElse(null);
				subModuleValue = String.valueOf(i);
			}
	    	activityTarget.addAll(metaTable.getActivityList());
			activityNameList = activityNameList.stream().filter(e-> (metaTable.getActivityList().stream()
                    			.filter(d -> d.getIdentifier().equals(e.getIdentifier()))
                    			.count())<1).collect(Collectors.toList());
			activitySource.addAll(activityNameList);
			activityList = new DualListModel<>(activitySource, activityTarget);
	    }
	}
	
	/**
	 * This method will save modules in DB
	 **/
	public void saveAction(){
		if(CommonConstants.TRUE.equals(metaTable.getChildModuleConfig()) && subModuleValue!=null ){
			metaTable.setParentPage(Integer.valueOf(subModuleValue));
		}else{
			metaTable.setChildModuleConfig(CommonConstants.FALSE);
		}
		if(moduleId.isEmpty()){
			if(!activityNameList.isEmpty())
				metaTable.setActivityList(activityNameList);
			moduleManagementService.add( commonError, metaTable, loginUser);
		}else{
			metaTable.setActivityList(activityList.getTarget());
			moduleManagementService.update( commonError, metaTable,moduleId, loginUser );
		}
		String msg="".equals(CommonUtils.getValidString(moduleId)) ? ModuleManagementConstants.MODULE_ADDED_SUCCESS : ModuleManagementConstants.MODULE_UPDATED_SUCCESS ;
		customRedirector( msg, ModuleManagementConstants.LIST_PAGE);
		
	}
	
	/**
	 * This method to returns to modules list/view page
	 */
	public void backView(){
		if("".equals(CommonUtils.getValidString(moduleId))){
			redirector(ModuleManagementConstants.LIST_PAGE);
		}else{
			redirector(ModuleManagementConstants.VIEW_PAGE+"?objectId="+moduleId);	
		}
	}
	
	/**
	 * This method to get sub modules list
	 */
	public void subModuleList(String pageId){
		  if(!"1".equals(pageId)){
			  showChildCheckBox = CommonConstants.TRUE;  
		  }
		  subModules = moduleManagementService.getSubModuleList( commonError, pageId );
		  if(CommonConstants.TRUE.equals(metaTable.getChildModuleConfig()) && subModules.isEmpty() ){
			  subModuleCheck();  
		  }
	}
	
	public void subModuleCheck(){
		if(subModules.isEmpty()){
			if(CommonConstants.TRUE.equals(metaTable.getChildModuleConfig())){
				FacesContext.getCurrentInstance().addMessage("warnMessage",new FacesMessage(FacesMessage.SEVERITY_WARN,getJSFMessage("unfilledDatawarning"), ""));
				customWarnMessage(getJSFMessage("warn_subModule"));
			    return; 
			}
		}
		
	}
	
	/**
	 * This method process the image upload
	 * @param fileUploadEvent
	 */
	public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException{
		UploadedFile uploadedPhoto = fileUploadEvent.getFile();
		setFile(fileUploadEvent.getFile());
		String filePath = null;	
		byte[] bytes = null;
		if (null != uploadedPhoto) {
		   filePath = System.getProperty("config.dir") + "images/" ;
		   bytes = uploadedPhoto.getContents();
		   String filename = FilenameUtils.getName(uploadedPhoto.getFileName());
		             
		   metaTable.setImageName(filename);
		   BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+filename)));
		   stream.write(bytes);
		   stream.close();
		}
	}	
	
	
	/**
	 * Set default profile Image
	 */
	public void setDefaultImage(){
		metaTable.setImageName("");
	}
	
	public MetaTable getMetaTable() {
		return metaTable;
	}
	public void setMetaTable(MetaTable metaTable) {
		this.metaTable = metaTable;
	}
	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}


	public ModuleManagementService getModuleManagementService() {
		return moduleManagementService;
	}


	public void setModuleManagementService(ModuleManagementService moduleManagementService) {
		this.moduleManagementService = moduleManagementService;
	}


	public ModuleActivityService getModuleActivityService() {
		return moduleActivityService;
	}


	public void setModuleActivityService(ModuleActivityService moduleActivityService) {
		this.moduleActivityService = moduleActivityService;
	}


	public List<ModuleActivity> getActivityNameList() {
		return activityNameList;
	}


	public void setActivityNameList(List<ModuleActivity> activityNameList) {
		this.activityNameList = activityNameList;
	}


	public DualListModel<ModuleActivity> getActivityList() {
		return activityList;
	}


	public void setActivityList(DualListModel<ModuleActivity> activityList) {
		this.activityList = activityList;
	}


	public String getSubModuleValue() {
		return subModuleValue;
	}


	public void setSubModuleValue(String subModuleValue) {
		this.subModuleValue = subModuleValue;
	}


	public List<MetaTable> getSubModules() {
		return subModules;
	}


	public void setSubModules(List<MetaTable> subModules) {
		this.subModules = subModules;
	}


	public UploadedFile getFile() {
		return file;
	}


	public void setFile(UploadedFile file) {
		this.file = file;
	}


	public String getModuleTitle() {
		return moduleTitle;
	}


	public void setModuleTitle(String moduleTitle) {
		this.moduleTitle = moduleTitle;
	}


	public List<MetaTable> getAllMetaTableList() {
		return allMetaTableList;
	}


	public void setAllMetaTableList(List<MetaTable> allMetaTableList) {
		this.allMetaTableList = allMetaTableList;
	}


	public List<MetaTable> getMainModules() {
		return mainModules;
	}


	public void setMainModules(List<MetaTable> mainModules) {
		this.mainModules = mainModules;
	}


	public String getShowChildCheckBox() {
		return showChildCheckBox;
	}


	public void setShowChildCheckBox(String showChildCheckBox) {
		this.showChildCheckBox = showChildCheckBox;
	}
	
	
}
