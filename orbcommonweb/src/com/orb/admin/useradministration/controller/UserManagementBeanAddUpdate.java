package com.orb.admin.useradministration.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.service.ForgotPassService;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.constants.UserManagementConstants;
import com.orb.admin.useradministration.service.UserManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="userManagementAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class UserManagementBeanAddUpdate extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(UserManagementBeanAddUpdate.class);
	
	@Autowired 
	private UserManagementService userManagementService;
	
	@Autowired
	private ForgotPassService forgotPassService;
	
	@Autowired
	protected AdminService adminService;
	
	private List<User> selectedUsers = null;
	private User user = null;
	private Map<String, String> language = null;
	private DualListModel<Role> roles = null;
	private String id=null;
	private String userTitle = null;
	private UploadedFile file = null;
	private boolean resetPass = false;
	private Set<String> userDateFormats = CommonConstants.DATE_FORMAT;
	private Map<String, String> locales= null;
	private String timeZone = null;
	private List<String> timeZoneList = null;
	private String linkForEmail= null;
	private User loginUser=null;
	private String secondLevelAuthDisable = CommonConstants.DISABLE;
	private String secondLevelAuthEmail = CommonConstants.MAIL;
	private String secondLevelAuthSMS = CommonConstants.SMS;
	private List<Role> rolesSource = null;
	private List<Role> rolesTarget = null;
	private List<Role> roleList = null;
	Map<String, String> adminConfMap = null;
	private String googleAuthentication = CommonConstants.GOOGLE_AUTH;
	
	/**
	 * This method will be called when user clicks Add button / User Id link on list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadUserManagementAddUpdate")
	public String loadUserManagementAddUpdate(HttpServletRequest httpRequest, ModelMap model){
		id = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute(CommonConstants.OBJECT_ID, id );
		int port = httpRequest.getServerPort();
		StringBuffer baseURL = new StringBuffer();
        if( port > 1000 ){
        	baseURL.append( "http://" );
        	baseURL.append( httpRequest.getServerName() );
        	baseURL.append( ":" );
        	baseURL.append( port );
        }else{
        	baseURL.append( "https://" );
        	baseURL.append( httpRequest.getServerName() );
        }
        
		model.addAttribute( ForgotPasswordConstant.URLForForgotPass, baseURL.toString() );
		
		return UserManagementConstants.ADD_UPDATE_PAGE;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 * @throws NoSuchPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws BadPaddingException 
	 * @throws InvalidKeyException 
	 */
	public void onLoadAction() throws NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException{
		
		timeZoneList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		linkForEmail = CommonUtils.getValidString(getJSFRequestAttribute(( ForgotPasswordConstant.URLForForgotPass )));
		id = CommonUtils.getValidString(getJSFRequestAttribute( CommonConstants.OBJECT_ID ));
		adminConfMap = adminService.getAdminConfMap(commonError);
		if (id.isEmpty()){
			user = new User();
			user.setDisplayRowsSize(UserManagementConstants.DEFAULT_USER_LIST_SIZE);
			user.setTimeZone(UserManagementConstants.DEFAULT_TIME_ZONE);
			rolesSource = userManagementService.fetchRole(commonError);
			rolesTarget = new ArrayList<>();
			userTitle = CommonConstants.USER_TITLE_CREATION;
		} else {
			user = userManagementService.getUser(commonError,id);
			user.setIsPasswordChecked(null);
			rolesTarget = userManagementService.getUserRoles(commonError, id);
			rolesSource = userManagementService.getRole(commonError, rolesTarget);
			userTitle = CommonConstants.USER_TITLE_UPDATE;
		} 
		roleList = userManagementService.fetchRole(commonError);
		roles = new DualListModel<Role>(rolesSource,rolesTarget);
		language = CommonConstants.LANGUAGES;
		String[] ids = TimeZone.getAvailableIDs();
		timeZoneList = Arrays.stream(ids).map(x -> displayTimeZone(TimeZone.getTimeZone(x))).collect(Collectors.toList());
	}
	
	/**
	 * This method call create user service
	 */
	public void createUpdateUser() {
		logger.info("User Creation or Update is called");
		if(user.getIsPasswordChecked() != null){
		 if(true == user.getIsPasswordChecked().booleanValue() && user.getId() == null){
			if(user.getNewPassword().equals(user.getConfirmPassword())){
				String pattern = ForgotPasswordConstant.PASSWORD_REGEX;
				if(user.getNewPassword().matches(pattern)){
					logger.info("Password is done");
				}else{
					errorMessage(ForgotPasswordConstant.PASSWORD_PATTERN);
					return;
				}
			}else{
				errorMessage(ForgotPasswordConstant.PASSWORD_MATCH);
				return;
			}
		  }
		}
		String pattern = CommonConstants.EMAIL_REGEX;
		if ( user.getEmail().matches(pattern)) {
			List<Role> roleTargetLists = new ArrayList<Role>();
			Role r = roleList.stream().filter(obj -> obj.getRoleId().equals(user.getRoleId())).findAny().orElse(null);
			if(r != null) roleTargetLists.add(r);
			user.setRoleId(r.getRoleId());
			if (null != roleTargetLists && null != user) {
				if ( null == user.getId()) {
					userManagementService.createUser(commonError, user, roleTargetLists, linkForEmail, loginUser);
					logger.info("User Created Succesfully");
				} else {
					userManagementService.updateUser(commonError, user, resetPass, roleTargetLists, loginUser);
					logger.info("User Updated Sucesfully");
					if(loginUser.getUserId().equals(user.getUserId())){
						List<Role> userRoles = userManagementService.getUserRoles(null,String.valueOf(loginUser.getId()));
				        IntStream roleIdStream = userRoles.stream().mapToInt(Role::getRoleId);
				        List<Integer> roleIdList = roleIdStream.boxed().collect(Collectors.toList());
						if(roleIdList == null) roleIdList = new ArrayList<>();
						if(roleIdList.stream().anyMatch(role -> role == 1)){
							addSessionObj(CommonConstants.SHOW_ADMIN_BUTTON , CommonConstants.TRUE);
						}
						setSessionObject(user);
					}					
				}
			}
			listAction();
		}else{
			errorMessage(CommonConstants.EMAIL_REGEX_MESSAGE);
		}
	}
	
	/**
	 * This method will navigate to the user management list page
	 */
	public void listAction(){
		String msg="".equals(CommonUtils.getValidString(id)) ? UserManagementConstants.USER_ADDED_SUCCESS : UserManagementConstants.USER_UPDATED_SUCCESS ;
		customRedirector(msg,UserManagementConstants.LIST_PAGE_REDIR);
	}
	
	
	/**
	 * This method will navigate to the user management list page
	 */
	public void cancelAction(){
		if(id.isEmpty()){
			redirector(UserManagementConstants.LIST_PAGE_REDIR);
		}else{
			redirector(UserManagementConstants.VIEW_PAGE_REDIR + "?objectId=" + id);	
		}
	}
	
	/**
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void fileUploadListener(FileUploadEvent event){
		try {
			UploadedFile uploadedPhoto=event.getFile();
		    String filePath = System.getProperty("config.dir") + "images/";
			          	        	
		    byte[] bytes=null;
		   	
		    if (null!=uploadedPhoto) {
			   	bytes = uploadedPhoto.getContents();
			   	String ext = getExtension(uploadedPhoto.getFileName());
				String userImage = String.format(user.getUserId() + "." + ext.toLowerCase());
			    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+userImage)));
			    stream.write(bytes);
			    stream.close();
			    user.setUserImage(userImage);
				if(loginUser.getUserId().equals(user.getUserId())){
			       loginUser.setUserImage(userImage);
			    }
		   }
		} catch (Exception e) {
			logger.info("Error upload");
	    }
	}
	
	
	public void setTempPass(){
		String returnValue = forgotPassService.forgotPasswordService(commonError, user.getUserId(), linkForEmail, adminConfMap);
		if(null != returnValue){
			errorMessage(returnValue);
		}else{
			infoMessage(UserManagementConstants.TEMPORARY_PASSWORD_SENT_SUCCESSFULLY);
		}
	}
	
	public Map<String, String> getLocales() {
		if(locales == null){
			locales = new HashMap<String, String>();
			locales.put("Deutsch", "de");
			locales.put("Englisch", "en");
		}
		return locales;
	}

	public void setLocales(Map<String, String> locales) {
		this.locales = locales;
	}
	
	
	private static String displayTimeZone(TimeZone tz) {

		long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
		long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                                  - TimeUnit.HOURS.toMinutes(hours);
		
		minutes = Math.abs(minutes);

		String result = "";
		if (hours > 0) {
			result = String.format("%s (GMT+%d:%02d)", tz.getID(),hours, minutes);
		} else {
			result = String.format("%s (GMT%d:%02d)", tz.getID(),hours, minutes );
		}
		return result;

	}
	
	/**
	 * Set default profile Image
	 */
	public void setDefaultImage(){
	   user.setUserImage("");
	   if(loginUser.getUserId().equals(user.getUserId())){
		 loginUser.setUserImage("");
	   }
	}
	
	
	public User getUser() {
		return user;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Map<String, String> getLanguage() {
		return language;
	}
	public void setLanguage(Map<String, String> language) {
		this.language = language;
	}
	public DualListModel<Role> getRoles() {
		return roles;
	}
	public void setRoles(DualListModel<Role> roles) {
		this.roles = roles;
	}
	public List<User> getSelectedUsers() {
		return selectedUsers;
	}
	public void setSelectedUsers(List<User> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}
	public UserManagementService getUserManagementService() {
		return userManagementService;
	}
	public void setUserManagementService(UserManagementService userManagementService) {
		this.userManagementService = userManagementService;
	}
	public String getUserTitle() {
		return userTitle;
	}
	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}
	public UploadedFile getFile() {
		return file;
	}
	public void setFile(UploadedFile file) {
		this.file = file;
	}
	public boolean isResetPass() {
		return resetPass;
	}
	public void setResetPass(boolean resetPass) {
		this.resetPass = resetPass;
	}

	public Set<String> getUserDateFormats() {
		return userDateFormats;
	}

	public void setUserDateFormats(Set<String> userDateFormats) {
		this.userDateFormats = userDateFormats;
	}

	public List<String> getTimeZoneList() {
		return timeZoneList;
	}

	public void setTimeZoneList(List<String> timeZoneList) {
		this.timeZoneList = timeZoneList;
	}

	public User getLoginUser() {
		return loginUser;
	}
	
	public String getSecondLevelAuthEmail() {
		return secondLevelAuthEmail;
	}

	public void setSecondLevelAuthEmail(String secondLevelAuthEmail) {
		this.secondLevelAuthEmail = secondLevelAuthEmail;
	}

	public String getSecondLevelAuthSMS() {
		return secondLevelAuthSMS;
	}

	public void setSecondLevelAuthSMS(String secondLevelAuthSMS) {
		this.secondLevelAuthSMS = secondLevelAuthSMS;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getSecondLevelAuthDisable() {
		return secondLevelAuthDisable;
	}

	public void setSecondLevelAuthDisable(String secondLevelAuthDisable) {
		this.secondLevelAuthDisable = secondLevelAuthDisable;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public Map<String, String> getAdminConfMap() {
		return adminConfMap;
	}

	public void setAdminConfMap(Map<String, String> adminConfMap) {
		this.adminConfMap = adminConfMap;
	}

	public String getGoogleAuthentication() {
		return googleAuthentication;
	}

	public void setGoogleAuthentication(String googleAuthentication) {
		this.googleAuthentication = googleAuthentication;
	}
	
}
