package com.orb.admin.useradministration.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.constants.UserManagementConstants;
import com.orb.admin.useradministration.service.UserManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="userManagementListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class UserManagementBeanListView extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(UserManagementBeanListView.class);
	
	@Autowired 
	private UserManagementService userManagementService;

	private User viewUser = null;
	private List<User> userList = null;
	private List<User> selectedUsers = null;
	private String id = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<User> filteredUserList = null;
	private List<Boolean> togglerList = null;
	private User loginUser=null;
	private String viewActiveStatus = null;
    private String role = null;
    private String userChange = CommonConstants.FALSE;
 
	
		
	/**
	 * This Method will be called to view the User list page when use clicks User link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadUserManagementList")
	public String loadUserManagementList(HttpServletRequest request, ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return UserManagementConstants.LIST_PAGE;
	}
	
	/**
	 * This method will be called when user clicks particular User from list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadUserManagementView")
	public String loadUserManagementView(HttpServletRequest httpRequest, ModelMap model){
		id = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		userChange = CommonUtils.getValidString( httpRequest.getParameter( UserManagementConstants.USER_CHANGE ) );
		if(CommonConstants.TRUE.equals(userChange)){
			User tempUser = userManagementService.getUser(commonError,id);
			HttpSession session = httpRequest.getSession();
			if(CommonUtils.getValidString(tempUser.getActualDisplayRowsSize()).isEmpty()){
				Set<String> tablePageTemplates = (Set<String>) CommonUtils.getBean("tableDisplayPageTemplates");
				if( tablePageTemplates == null ){
					tempUser.setActualDisplayRowsSize("5,10,25,50,100,250");
				}else{
					StringBuffer tempStr = new StringBuffer();
					if( !"".equals(CommonUtils.getValidString(tempUser.getDisplayRowsSize())) &&  !tablePageTemplates.contains(tempUser.getDisplayRowsSize()) ){
						tempStr.append(tempUser.getDisplayRowsSize());
						tempStr.append(",");
					}
					else if("".equals(CommonUtils.getValidString(tempUser.getDisplayRowsSize()))){
						tempUser.setDisplayRowsSize("5");
					}
					for( String pageTemplate : tablePageTemplates ){
						tempStr.append(pageTemplate);
						tempStr.append(",");
					}
					tempStr.setLength( tempStr.length()-1 );
					tempUser.setActualDisplayRowsSize(tempStr.toString());
				}
			}
			session.setAttribute(CommonConstants.USER_IN_CONTEXT, tempUser);
			if(tempUser.getRoleId() != null && tempUser.getRoleId() != 1){
				session.setAttribute( CommonConstants.SHOW_ADMIN_BUTTON , CommonConstants.FALSE);
			}
			return "dashboard/dashboard.xhtml";
		}
		model.addAttribute( CommonConstants.OBJECT_ID, id );
		return UserManagementConstants.VIEW_PAGE;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	
	public void onLoadAction(){
		List<Role> rolesTarget = null;
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		id =  CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.OBJECT_ID ));
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			userList=userManagementService.getInactiveUsersList(commonError);
			showActivateBtn = CommonConstants.TRUE;
			showActiveListBtn = CommonConstants.TRUE;
		}else{ 
			userList=userManagementService.getActiveUsersList(commonError);	
		}
		togglerList = (List<Boolean>)getSessionObj(UserManagementConstants.USER_TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList(false, true, true, true, true,true);
		} 
       	 if(!id.isEmpty()){
			viewUser = userManagementService.getUser(commonError,id);
			rolesTarget = userManagementService.getUserRoles(commonError, id);
			role = rolesTarget.stream()
					 .map(Role::getRoleName)
					 .collect(Collectors.joining(", "));
		 }
		} 
	
	/**
	 * This method will navigate to the user management view page
	 * @param id
	 */
	public void viewAction(String id ){
		redirector(UserManagementConstants.VIEW_PAGE_REDIR + "?objectId=" + id);
	}
	
	/**
	 * This method will navigate to the user management add page
	 */
	public void addAction(){
		redirector(UserManagementConstants.ADD_UPDATE_PAGE_REDIR);
	}
	
	public void changeUserAction( String userId){
		redirector(UserManagementConstants.VIEW_PAGE_REDIR + "?objectId=" + userId+"&"+UserManagementConstants.USER_CHANGE+"="+CommonConstants.TRUE);	
	}
	
	/**
	 * This method will navigate to the user management edit page
	 * @param id
	 */
	public void editAction(String id ){
		redirector(UserManagementConstants.ADD_UPDATE_PAGE_REDIR + "?objectId=" + id);
	}
	
	/**
	 * This method will delete the user 
	 * @param id
	 */
	public void deleteUser(List<Integer> id ){
		userManagementService.deleteUser(commonError,id,loginUser);
		listAction();
	}
	/**
	 * This method will get all the active User
	 * @param event
	 */
	public void showActive(ActionEvent event){
		userList=userManagementService.getActiveUsersList(commonError);	
		addSessionObj(UserManagementConstants.SHOW_ACTIVE_USER, CommonConstants.Y);
		if( filteredUserList != null ) filteredUserList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will show all the inactive User
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		userList=userManagementService.getInactiveUsersList(commonError);
		if( filteredUserList != null ) filteredUserList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	
	/**
	 * This method locks User
	 * @param e
	 */
	public void lockAction(ActionEvent e){
		userManagementService.lockUser( commonError, loginUser, selectedUsers );
		showActive(e);
		customRedirector( UserManagementConstants.USER_LOCKED_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR);
	}
		
	/**
	 * This method unlocks User
	 * @param e
	 */
	public void unlockAction(ActionEvent e){
		userManagementService.unlockUser( commonError, loginUser, selectedUsers );
		showInactive(e);
		customRedirector( UserManagementConstants.USER_UNLOCKED_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR );
	}
	
	/**
	 * This method will delete the User
	 * @param e
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedUsers.isEmpty()){
			userManagementService.delete( commonError, loginUser, selectedUsers );
		if( commonError.getError().isEmpty() ){
			customRedirector( UserManagementConstants.USER_DELETE_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR);
		}
		showInactive(e);
		commonError.clearErrors();
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(UserManagementConstants.USER_DELETE_SUCCESS);
		}
			
	}
	
	/**
	 * This method locks viewed user
	 * @param id
	 */
	public void lockViewedUser(int id){
		String userId = String.valueOf(id);
		viewUser = userManagementService.getUser(commonError,userId);
		userManagementService.lockViewedUser( commonError, loginUser, viewUser );
		customRedirector( UserManagementConstants.USER_LOCKED_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR);
	}

	/**
	 * This method unlocks viewed user
	 * @param id
	 */
	public void unlockViewedUser(int id){
		String userId = String.valueOf(id);
		viewUser = userManagementService.getUser(commonError,userId);
		userManagementService.unlockViewedUser( commonError, loginUser, viewUser );
		if( commonError.getError().isEmpty() ){
			customRedirector( UserManagementConstants.USER_UNLOCKED_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR );
		}
	}
	
	/**
	 * Delete viewed user
	 * @param id
	 */
	public void deleteViewedUser(int id){
		String userId = String.valueOf(id);
		viewUser = userManagementService.getUser(commonError,userId);
		userManagementService.deleteViewedUser( commonError, loginUser, viewUser);
		if( commonError.getError().isEmpty() ){
			customRedirector( UserManagementConstants.USER_DELETE_SUCCESS, UserManagementConstants.LIST_PAGE_REDIR );
			
		}
	}
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param e
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	
	/**
	 * This method will navigate to the user management list page
	 */
	public void listAction(){
		redirector(UserManagementConstants.LIST_PAGE_REDIR +"?activeFlag="+viewUser.getActive());
	}

	/**
	 * To clear the session variables
	 * @param model
	 */
	
    @SuppressWarnings("rawtypes")
    private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( UserManagementConstants.SELECTED_USER_ID);
		String showActive = (String)model.get( UserManagementConstants.SHOW_ACTIVE_USER ); 
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( UserManagementConstants.SHOW_ACTIVE_USER );
		if( showActive != null ) 
			model.remove( UserManagementConstants.SHOW_ACTIVE_USER );
		if( toggleList != null ) model.remove( UserManagementConstants.USER_TOGGLER_LIST );
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public User getViewUser() {
		return viewUser;
	}

	public void setViewUser(User viewUser) {
		this.viewUser = viewUser;
	}

	public List<User> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<User> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public UserManagementService getUserManagementService() {
		return userManagementService;
	}

	public void setUserManagementService(UserManagementService userManagementService) {
		this.userManagementService = userManagementService;
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}

	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public List<User> getFilteredUserList() {
		return filteredUserList;
	}

	public void setFilteredUserList(List<User> filteredUserList) {
		this.filteredUserList = filteredUserList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getViewActiveStatus() {
		return viewActiveStatus;
	}

	public void setViewActiveStatus(String viewActiveStatus) {
		this.viewActiveStatus = viewActiveStatus;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


}