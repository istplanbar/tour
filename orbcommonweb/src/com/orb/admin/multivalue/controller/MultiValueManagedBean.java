package com.orb.admin.multivalue.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.constants.MultiValueConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.service.CommonService;
@ManagedBean(name="multiValueList")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")
public class MultiValueManagedBean extends AbstractCommonController{
	
	private static Log logger=LogFactory.getLog(MultiValueManagedBean.class);
	
	@Autowired 
	protected CommonService commonService;
	
	@Autowired 
	private ModuleManagementService moduleManagementService;
	
	private User loginUser = null;
	private List<Boolean> togglerList = null;
	private String multiValueField = null;
	private List<MultiVal> multiValList = null;
    private List<MultiVal> selectedMultiValList = null;
    private List<MultiVal> filteredMultiValList = null;
    private int langSize;
    private MultiVal multiVal = new MultiVal();
    private String noOfValue= null;
    private String parentPage=null;
    private List<MetaTable> mainModuleList = null;
    private List<String> multiValIdList = null;
    private List<String> multiValEngList = null;
	private List<String> multiValGerList = null;
    private Map<String,List<String>> multiValMap = null;
    private Map<String,String> multiValMsgMap = null;
	
	/**
	 * This Method will be called to list the multi Values 
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMultiValue")
	public String loadMessages(HttpServletRequest httpRequest, ModelMap model){
		clearSessionObjects(model);
	    return MultiValueConstants.MULTI_VALUE_LIST_HTML; 
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		multiValMap = new HashMap<>();
		multiValMsgMap = new HashMap<>();
		multiValIdList = new ArrayList<>();
		multiValEngList = new ArrayList<>();
		multiValGerList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		togglerList = (List<Boolean>)getSessionObj(MultiValueConstants.MULTI_VALUE_TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList( false,true,true );
		}
		mainModuleList = moduleManagementService.getParentPageList(commonError);
		multiValList=commonService.getMultiValueList(commonError);
		
	}
	
	
	 /**
	  * This method to use add the Age of Kids Text boxes
	  * @param add page
	  * @return 
	  */
	 public void addMultiValues(AjaxBehaviorEvent  e){
			
			String value = (String) ((UIOutput) e.getSource()).getValue();
			if(!value.isEmpty() && multiValIdList.size() == 0){
				int values=Integer.parseInt(value);
				for(int i=1;i<=values;i++){
					multiValIdList.add(String.valueOf(i));
					multiValEngList.add(null);
					multiValGerList.add(null);
				}
			}else if(multiValIdList.size()!=0 && !"".equals(value)){
				int values1=Integer.parseInt(value);
				if(values1 > multiValIdList.size()){
					for(int j=multiValIdList.size();j<values1;j++){
						multiValIdList.add(String.valueOf(j+1));
						multiValEngList.add(null);
						multiValGerList.add(null);
				   }
				}else{
					for(int j=multiValIdList.size();j>values1;j--){
						multiValIdList.remove(multiValIdList.size()-1);
						multiValEngList.remove(multiValEngList.size()-1);
						multiValGerList.remove(multiValGerList.size()-1);
					}
				}
			}
	 }
	
	/**
	 * To clear the session variables
	 * @param model
	 */	
	@SuppressWarnings("rawtypes")
	private void clearSessionObjects( ModelMap model ){
		List toggleList = (List)model.get( togglerList );
		if( toggleList != null ) model.remove(MultiValueConstants.MULTI_VALUE_TOGGLER_LIST);
	}
	
	/**
	 * This method will add / update a select Item dynamic form in DB
	 * @param event
	 */
     public void saveMultiValAndMsg(ActionEvent event){
    	 multiValMap.put("multiValueIdList", multiValIdList);
    	 multiValMap.put("multiValEngList", multiValEngList);
    	 multiValMap.put("multiValGerList", multiValGerList);
    	 multiValMsgMap.put("multiValField", multiVal.getMultiValueField());
    	 multiValMsgMap.put("parentPage", parentPage);
		 commonService.addMultiValAndMsg(commonError,loginUser,multiValMap,multiValMsgMap);
		 customRedirector( MultiValueConstants.MULTI_VALUE_ADDED_SUCCESS , MultiValueConstants.MULTI_VALUE_LIST_PAGE);
	 }
     
     /**
	  * This method to returns to multivalue list page
	  * @param list page
	  * @return 
	  */
	  public void cancelAction(){
		  redirector(MultiValueConstants.MULTI_VALUE_LIST_PAGE);
	  }

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public String getMultiValueField() {
		return multiValueField;
	}

	public void setMultiValueField(String multiValueField) {
		this.multiValueField = multiValueField;
	}

	public List<MultiVal> getMultiValList() {
		return multiValList;
	}

	public void setMultiValList(List<MultiVal> multiValList) {
		this.multiValList = multiValList;
	}

	public List<MultiVal> getSelectedMultiValList() {
		return selectedMultiValList;
	}

	public void setSelectedMultiValList(List<MultiVal> selectedMultiValList) {
		this.selectedMultiValList = selectedMultiValList;
	}

	public List<MultiVal> getFilteredMultiValList() {
		return filteredMultiValList;
	}

	public void setFilteredMultiValList(List<MultiVal> filteredMultiValList) {
		this.filteredMultiValList = filteredMultiValList;
	}

	public int getLangSize() {
		return langSize;
	}

	public void setLangSize(int langSize) {
		this.langSize = langSize;
	}

	public MultiVal getMultiVal() {
		return multiVal;
	}

	public void setMultiVal(MultiVal multiVal) {
		this.multiVal = multiVal;
	}

	public String getNoOfValue() {
		return noOfValue;
	}

	public void setNoOfValue(String noOfValue) {
		this.noOfValue = noOfValue;
	}

	public String getParentPage() {
		return parentPage;
	}

	public void setParentPage(String parentPage) {
		this.parentPage = parentPage;
	}

	public List<MetaTable> getMainModuleList() {
		return mainModuleList;
	}

	public void setMainModuleList(List<MetaTable> mainModuleList) {
		this.mainModuleList = mainModuleList;
	}

	public List<String> getMultiValEngList() {
		return multiValEngList;
	}

	public void setMultiValEngList(List<String> multiValEngList) {
		this.multiValEngList = multiValEngList;
	}

	public List<String> getMultiValGerList() {
		return multiValGerList;
	}

	public void setMultiValGerList(List<String> multiValGerList) {
		this.multiValGerList = multiValGerList;
	}

	public Map<String,List<String>> getMultiValMap() {
		return multiValMap;
	}

	public void setMultiValMap(Map<String,List<String>> multiValMap) {
		this.multiValMap = multiValMap;
	}
	public List<String> getMultiValIdList() {
		return multiValIdList;
	}

	public void setMultiValIdList(List<String> multiValIdList) {
		this.multiValIdList = multiValIdList;
	}

	public Map<String,String> getMultiValMsgMap() {
		return multiValMsgMap;
	}

	public void setMultiValMsgMap(Map<String,String> multiValMsgMap) {
		this.multiValMsgMap = multiValMsgMap;
	}
	
	

}
