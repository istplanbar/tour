package com.orb.admin.layoutadministration.controller;

/**
 * @author IST planbar / SuKu
 * 
 */

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.layoutadministration.constants.LayoutManagementConstants;
import com.orb.admin.layoutadministration.service.LayoutManagementService;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.layoutadmnistration.model.TableDesc;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="layoutManagement")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class LayoutManagementBeanAddUpdate extends AbstractCommonController{
	
	@Autowired
	protected LayoutManagementService layoutManagementService;
	
	private List<MetaTable> fetchMainModules;
	private Map<String,DualListModel<TableDesc>> tableFieldsMap = null;
	private String layoutName = null;
	private Integer selectedMainModule = null;
	private List<MetaTable> fetchSubModules = null;
	private Integer selectedSubModule = null;
	private List<Layout> layoutDescList= null;
	private List<Layout> layoutList = null;
	private String layoutId = null;

	private User loginUser = null;
	private List<TableDesc> selectedTableFields;
	
	/**
	 * This method will be called when user clicks Add button / Layout Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadLayoutManagementAddUpdate")
	public String loadLayoutManagedBeanAddUpdate(HttpServletRequest httpRequest, ModelMap model){
		clearSessionObjects(model);
		layoutId = CommonUtils.getValidString(httpRequest.getParameter(CommonConstants.OBJECT_ID ) );
		model.addAttribute(CommonConstants.OBJECT_ID, layoutId);
		return LayoutManagementConstants.ADD_UPDATE_PAGE;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		layoutId = CommonUtils.getValidString(getJSFRequestAttribute( CommonConstants.OBJECT_ID ));
		fetchMainModules = layoutManagementService.getMainMenuList(commonError);
		if(!layoutId.isEmpty()){
			layoutDescList = layoutManagementService.getLayoutDesc(commonError, layoutId);
			Layout layout = layoutDescList.get(0);
			layoutName = layout.getLayoutName();
			selectedSubModule = layout.getSubmenuId();
			selectedMainModule = layout.getMainMenuId();
			List<Layout> layoutList = layoutManagementService.getLayout(commonError, layoutId);
			getSubModuleList();
			tableFieldsMap = layoutManagementService.getTableFields(commonError, selectedSubModule, layoutList);
		}
	}
	
	/**
	 * To clear the session variables
	 * @param model
	 */
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( CommonConstants.OBJECT_ID);
		if(!CommonUtils.getValidString(id).isEmpty()) model.remove( CommonConstants.OBJECT_ID );
	}
	
	/**
	 * Fetch Sub module list based on main module 
	 */
	public void getSubModuleList(){
		fetchSubModules = layoutManagementService.getSubModuleList(commonError,selectedMainModule);
	}
	
	/**
	 * Fetch table list and fields based on sub module
	 */
	public void fetchTablesList(){
		tableFieldsMap = layoutManagementService.getTableFields( commonError, selectedSubModule, layoutList );
	}
	/**
	 * get table names
	 * @return
	 */
	public Set<String> getTableNameSet(){
		if( tableFieldsMap == null || tableFieldsMap.isEmpty() ) return null;
		return tableFieldsMap.keySet();
	}
	
	/**
	 * This method will add / update a Layout in DB
	 * @param event
	 */
	public void saveAction(){
		Layout layoutTable = new Layout();
		layoutTable.setLayoutName(layoutName);
		layoutTable.setMainMenuId(selectedMainModule);
		layoutTable.setSubmenuId(selectedSubModule);
		if(layoutId.isEmpty()) {
				layoutManagementService.createLayout(commonError,tableFieldsMap,layoutTable,loginUser);
			} else {
				int id = Integer.parseInt(layoutId);
				layoutTable.setLayoutId(id);

				
				layoutManagementService.updateLayout(commonError,tableFieldsMap,layoutTable,layoutId,loginUser);
			}
		tableFieldsMap = null;
		listAction();
	}

	/**
	 * This method to returns to Layout List page
	 * @param list page
	 * @return 
	 */
	public void listAction(){
		redirector(LayoutManagementConstants.REDIR_LIST_PAGE);
	}
	public String getLayoutName() {
		return layoutName;
	}
	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}
	public List<MetaTable> getFetchMainModules() {
		return fetchMainModules;
	}
	public void setFetchMainModules(List<MetaTable> fetchMainModules) {
		this.fetchMainModules = fetchMainModules;
	}
	public List<MetaTable> getFetchSubModules() {
		return fetchSubModules;
	}
	public void setFetchSubModules(List<MetaTable> fetchSubModules) {
		this.fetchSubModules = fetchSubModules;
	}
	public List<Layout> getLayoutDescList() {
		return layoutDescList;
	}
	public void setLayoutDescList(List<Layout> layoutDescList) {
		this.layoutDescList = layoutDescList;
	}
	public List<Layout> getLayoutList() {
		return layoutList;
	}
	public void setLayoutList(List<Layout> layoutList) {
		this.layoutList = layoutList;
	}
	public Integer getSelectedMainModule() {
		return selectedMainModule;
	}
	public void setSelectedMainModule(Integer selectedMainModule) {
		this.selectedMainModule = selectedMainModule;
	}
	public Integer getSelectedSubModule() {
		return selectedSubModule;
	}
	public void setSelectedSubModule(Integer selectedSubModule) {
		this.selectedSubModule = selectedSubModule;
	}
	public Map<String, DualListModel<TableDesc>> getTableFieldsMap() {
		return tableFieldsMap;
	}
	public void setTableFieldsMap(Map<String, DualListModel<TableDesc>> tableFieldsMap) {
		this.tableFieldsMap = tableFieldsMap;
	}
	public String getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(String layoutId) {
		this.layoutId = layoutId;
	}
	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	public List<TableDesc> getSelectedTableFields() {
		return selectedTableFields;
	}
	public void setSelectedTableFields(List<TableDesc> selectedTableFields) {
		this.selectedTableFields = selectedTableFields;
	}
	public LayoutManagementService getLayoutManagementService() {
		return layoutManagementService;
	}
	public void setLayoutManagementService(LayoutManagementService layoutManagementService) {
		this.layoutManagementService = layoutManagementService;
	}
}