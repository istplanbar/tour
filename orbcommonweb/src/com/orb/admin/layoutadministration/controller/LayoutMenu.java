package com.orb.admin.layoutadministration.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ocpsoft.prettytime.PrettyTime;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.service.AdminService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.theme.Message;

@ManagedBean(name = "layoutMenu")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
public class LayoutMenu extends AbstractCommonController {
	private static Log logger = LogFactory.getLog(LayoutMenu.class);
	
	private MenuModel model = null;
	private MenuModel topMenuModel = null;
	private int messages = 7;
	private int notifications = 100;
	private int tasks = 9;
	private List<Message> messageList = null;
	private User user = null;
	private Map<String, String> adminConfMap = null;
		
	@Autowired
	private AdminService adminService;
	
	@PostConstruct
	public void init(){
		user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		String lastLoggedinUser = (String)getSessionObj( CommonConstants.LAST_LOGGEDIN_USER );
		setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
		if( user.getUserId().equals(lastLoggedinUser) ){
			model = (MenuModel)getSessionObj(CommonConstants.LOGGEDIN_USER_MENU);
		}else{
			addSessionObj( CommonConstants.LAST_LOGGEDIN_USER, user.getUserId() );
			model = new DefaultMenuModel();
			addSessionObj(CommonConstants.LOGGEDIN_USER_MENU, model);
			getMenuModel(model);
	        topMenuModel = model;
	        PrettyTime p = new PrettyTime();
	        messageList = new ArrayList<>();
			messageList.add(new Message("User Name", p.format(new Date(System.currentTimeMillis()-48*60*60*1000)), "Message Description Goes here!!!"));
			messageList.add(new Message("User Name", p.format(new Date(System.currentTimeMillis()-7*24*60*60*1000)), "Message Description Goes here!!!"));
		    messageList.add(new Message("User Name", p.format(new Date(System.currentTimeMillis()-60*1000)), "Message Description Goes here!!!"));
		}
    }
 
	public MenuModel getModel() {
        return model;
    }  
    
   public void save() {
       addMessage("Success", "Data saved");
   }
    
   public void update() {
       addMessage("Success", "Data updated");
   }
    
   public void delete() {
       addMessage("Success", "Data deleted");
   }
    
   public void addMessage(String summary, String detail) {
       FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
       FacesContext.getCurrentInstance().addMessage(null, message);
   }
   
   public void saveTheme(AjaxBehaviorEvent  e){
	  String newTheme = (String) ((UIOutput) e.getSource()).getValue();
	  if(newTheme != null){
		  getUser().setTheme(newTheme);
		  addSessionObj(CommonConstants.USER_IN_CONTEXT, getUser());
		  adminService.setTheme(newTheme, getUser().getUserId());
	  }
   }

	public int getMessages() {
		return messages;
	}
	
	public void setMessages(int messages) {
		this.messages = messages;
	}
	
	public int getNotifications() {
		return notifications;
	}
	
	public void setNotifications(int notifications) {
		this.notifications = notifications;
	}
	
	public int getTasks() {
		return tasks;
	}
	
	public void setTasks(int tasks) {
		this.tasks = tasks;
	}
	
	public Map<String, String> getAdminConfMap() {
		return adminConfMap;
	}

	public void setAdminConfMap(Map<String, String> adminConfMap) {
		this.adminConfMap = adminConfMap;
	}
	
	public MenuModel getTopMenuModel() {
		if(user.getTopMenu()){
			return topMenuModel;
		}
		return null;
	}
	
	public void setTopMenuModel(MenuModel topMenuModel) {
		this.topMenuModel = topMenuModel;
	}
	
	public void topMenuBar(){
	  getUser().setTopMenu(true);
	  addSessionObj(CommonConstants.USER_IN_CONTEXT, getUser());
	  adminService.setTopMenu(true, getUser().getUserId());
	}
	
	public void sideMenuBar(){
	  getUser().setTopMenu(false);
	  addSessionObj(CommonConstants.USER_IN_CONTEXT, getUser());
	  adminService.setTopMenu(false, getUser().getUserId());
	}
	
	public List<Message> getMessageList() {
		return messageList;
	}
	
	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}


	
	private void createSubMenuorItem(DefaultSubMenu submenu, List<MetaTable> metaTableList){
		for(MetaTable metaTable : metaTableList){
			if(metaTable.getMetaTableList().size()>0 ){
				DefaultSubMenu subSubmenu;
				subSubmenu = new DefaultSubMenu(getJSFMessage("menuLbl_"+metaTable.getPageDesc()));
				if(metaTable.getIcon() != null ){subSubmenu.setIcon(metaTable.getIcon());}
				subSubmenu.setId(String.valueOf(metaTable.getPageId()));
				subSubmenu.setStyleClass("id_"+metaTable.getPageId());
				
				createSubMenuorItem(subSubmenu, metaTable.getMetaTableList());
				submenu.addElement(subSubmenu);
			}else{
				DefaultMenuItem item = new DefaultMenuItem(getJSFMessage("menuLbl_"+metaTable.getPageDesc()));
				if(metaTable.getIcon() != null ){item.setIcon(metaTable.getIcon());}
				if(metaTable.getUrl() != null ){item.setUrl(metaTable.getUrl());}
				if(metaTable.getPopupMenu() == null || CommonConstants.FALSE.equals(metaTable.getPopupMenu())){
					item.setTarget("_self");
				}else{
					item.setTarget("_blank");
				}
				item.setStyleClass("id_"+metaTable.getPageId());
				item.setId(String.valueOf(metaTable.getPageId()));
				submenu.addElement(item);
			}
		}
	}
	
	private void getMenuModel(MenuModel model) {
		
		List<MetaTable> mainModules = adminService.getMetaTableList(null);
		//List<Integer> filteredModules = adminService.getFilteredModuleList(null,getUser().getRoleId());
		List<Integer> filteredModules = adminService.getFilteredModuleList(null,user);
		DefaultSubMenu submenu;
		for(MetaTable metaTable : mainModules){
			if(filteredModules.contains(metaTable.getPageId())){
				submenu = new DefaultSubMenu(getJSFMessage("menuLbl_"+metaTable.getPageDesc()));
				submenu.setId(String.valueOf(metaTable.getPageId()));
				if(metaTable.getIcon() != null ){submenu.setIcon(metaTable.getIcon());}
				submenu.setStyleClass("id_"+metaTable.getPageId());
				List<MetaTable> subModules = metaTable.getMetaTableList();
				for (Iterator<MetaTable> iterator = subModules.iterator(); iterator.hasNext();) {
					MetaTable subMetaTable = iterator.next();
				    if (!filteredModules.contains(subMetaTable.getPageId())) {
				        // Remove the current element from the iterator and the list.
				        iterator.remove();
				    }
				}
				
				createSubMenuorItem(submenu, subModules);
				model.addElement(submenu);
			}
			
		}
	}

}
    