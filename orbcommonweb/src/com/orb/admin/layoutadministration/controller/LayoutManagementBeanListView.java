package com.orb.admin.layoutadministration.controller;


import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.layoutadministration.constants.LayoutManagementConstants;
import com.orb.admin.layoutadministration.service.LayoutManagementService;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.utils.CommonUtils;

@ManagedBean(name="layoutManagementListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class LayoutManagementBeanListView extends AbstractCommonController {
	
	@Autowired
	protected LayoutManagementService layoutManagementService;

	private List<Layout> layoutList= null;
	private List<Layout> layout=null;
	private String layoutId = null;
	

	/**
	 * This Method will be called to view the Layout list page when use clicks Layout link on Top/Side Menu
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadLayoutManagementList")
	public String loadLayoutManagedBeanList(ModelMap model){
		clearSessionObjects(model);
		return LayoutManagementConstants.LIST_PAGE;
	}
	
	/**
	 * This method will be called when user clicks particular Layout from list page
	 * @param request
	 * @param model
	 * 
	 * @return
	 */

	@RequestMapping("/loadLayoutManagementView")
	public String loadLayoutManagedBeanView(HttpServletRequest request, ModelMap model) {
		clearSessionObjects(model);
		layoutId = CommonUtils.getValidString(request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( CommonConstants.OBJECT_ID, layoutId );
		return LayoutManagementConstants.VIEW_PAGE;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	
	public void onLoadAction(){
		layoutId = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.OBJECT_ID ));
		if(layoutId.isEmpty()){
			layoutList = layoutManagementService.getLayoutList(commonError);
		} else{
			layout = layoutManagementService.getLayout(commonError, layoutId);
		}
	}
	
	/**
	 * To clear the session variables
	 * @param model
	 */
	private void clearSessionObjects( ModelMap model ){
		String id = (String)model.get( CommonConstants.OBJECT_ID);
		if(!CommonUtils.getValidString(id).isEmpty()) model.remove( CommonConstants.OBJECT_ID );
	}
	
	/**
	 * This method will navigate to the page where user can add a Layout
	 * @return
	 */
	public void addAction() {
		redirector(LayoutManagementConstants.ADD_UPDATE_PAGE_REDIR);
	}
			
	/**
	 * This method will navigate to Layout View page
	 * @param index
	 * @return
	 */
	public void viewAction(String layoutId) {
		redirector(LayoutManagementConstants.REDIR_VIEW_PAGE + "?objectId=" + layoutId);
	}
	
	/**
	 * This method will navigate to the Layout Edit page
	 * @param index
	 * @return
	 */
	
	public void editAction(String layoutId){
		redirector(LayoutManagementConstants.ADD_UPDATE_PAGE_REDIR + "?objectId=" +layoutId);
	}	
	
	/**
	 * This method will navigate to the user management list page
	 */
	public void listAction(){
		redirector(LayoutManagementConstants.REDIR_LIST_PAGE);
	}

	/**
	 * This method calls service for delete layout
	 */
	public void deleteLayout(String layoutId) {
		layoutManagementService.deleteLayout(commonError, layoutId);
		listAction();
	}
		
	public List<Layout> getLayoutList() {
		return layoutList;
	}
	public void setLayoutList(List<Layout> layoutList) {
		this.layoutList = layoutList;
	}
	public List<Layout> getLayout() {
		return layout;
	}
	public void setLayout(List<Layout> layout) {
		this.layout = layout;
	}
	public String getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(String layoutId) {
		this.layoutId = layoutId;
	}
	public LayoutManagementService getLayoutManagementService() {
		return layoutManagementService;
	}
	public void setLayoutManagementService(LayoutManagementService layoutManagementService) {
		this.layoutManagementService = layoutManagementService;
	}
}