package com.orb.admin.systemadministration.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;

@ManagedBean(name = "adminView")
@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@ViewScoped
@RequestMapping("/process/admin")
@SuppressWarnings("unchecked")
public class AdminPageManagedBean extends AbstractCommonController {

	private String companyName;
	private UploadedFile companyLogo;
	private UploadedFile favIcon;
	private String version;
	private String defaultLocale;
	private Map<String, String> locales;
	private Map<String, String> adminConfMap;
	private String secondLevelAuth;
	private String ingeConfiguration;
	private String smsUserName;
	private String smsPassword;
	private String secondLevelAuthDisable = CommonConstants.DISABLE;
	private String ipAddressLog;
    private String googleMapConfiguration;
	private String googleApiKey;
	private String qcSurveyURL;
	private List<MetaTable> metaTableList = null;
	private String showSurveyUrl = null;
	private String sevdeskUserName;
	private String sevdeskApiToken;
	private String showSevdesk;
	private String sevdeskAuthentication;
	private String showIngeConfig;
	private String passwordChangeValid;
	private String passwordValidation;
	
	@Autowired
	private AdminService adminService;
	@Autowired 
	private ModuleManagementService moduleManagementService;

	@RequestMapping("/loadSystemAdministration")
	public String loadAdminPage(HttpServletRequest httpRequest) {
		return CommonConstants.SYSTEM_ADMIN;
	}

	@SuppressWarnings("unchecked")
	public void onLoadAction() {
		metaTableList = new ArrayList();
		metaTableList = moduleManagementService.getAllMetaTableList(commonError);
		if(metaTableList != null){
			showSurveyUrl = metaTableList.stream().filter(metaTable -> metaTable.getPageDesc().equals("Survey")).map(MetaTable::getActive).findAny().orElse(null);							
		}
	}
	/*public void preRenderView(ComponentSystemEvent event) throws Exception {

		super.preRenderView(event);
		addErrorMessage(commonError);
		if (commonError != null)
			commonError.clearErrors();
				
	}*/
	

	public void fileUpload(UploadedFile file, String fileName) {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = file.getInputstream();
			output = new FileOutputStream(new File(System.getProperty("config.dir") + "images/", fileName));
			IOUtils.copy(input, output);
			//FacesMessage message = new FacesMessage(getJSFMessage("msg_bild_success"));
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(getJSFMessage("msg_bild_success")));
		} catch (IOException e) {
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
	}

	public void systemAdministration() {
		// Company Name
		getAdminConfMap().put("companyName", getCompanyName());
		adminService.updateAdminConf("companyName", getCompanyName(), null);
		// Version
		getAdminConfMap().put("version", getVersion());
		adminService.updateAdminConf("version", getVersion(), null);
		// Locale
		getAdminConfMap().put("defaultLocale", getDefaultLocale());
		adminService.updateAdminConf("defaultLocale", getDefaultLocale(), null);

		// Second Level Auth
		getAdminConfMap().put("secondLevelAuth", getSecondLevelAuth());
		adminService.updateAdminConf("secondLevelAuth", getSecondLevelAuth(), null);

		// IngeConfiguration
		getAdminConfMap().put("ingeConfiguration", getIngeConfiguration());
		adminService.updateAdminConf("ingeConfiguration", getIngeConfiguration(), null);

		// smsUserName
		getAdminConfMap().put("smsUserName", getSmsUserName());
		adminService.updateAdminConf("smsUserName", getSmsUserName(), null);

		// smsPassword
		getAdminConfMap().put("smsPassword", getSmsPassword());
		adminService.updateAdminConf("smsPassword", getSmsPassword(), null);

		// ipAddressLog
		getAdminConfMap().put("ipAddressLog", getIpAddressLog());
		adminService.updateAdminConf("ipAddressLog", getIpAddressLog(), null);

		//GoogleMapConfiguration
		getAdminConfMap().put("googleAPIKeyConfig",getGoogleMapConfiguration()); 
		adminService.updateAdminConf("googleAPIKeyConfig", getGoogleMapConfiguration(), null);
		
		//GoogleAPIKey
		getAdminConfMap().put("googleAPIKey",getGoogleApiKey()); 
		adminService.updateAdminConf("googleAPIKey", getGoogleApiKey(), null);
		
		// qmsSurveyURL
		getAdminConfMap().put("qcSurveyURL", getQcSurveyURL());
		adminService.updateAdminConf("qcSurveyURL", getQcSurveyURL(), null);
		
		// sevdeskUserName
		getAdminConfMap().put("sevdeskUserName", getSevdeskUserName());
		adminService.updateAdminConf("sevdeskUserName", getSevdeskUserName(), null);

		// sevdeskApiToken
		getAdminConfMap().put("sevdeskApiToken", getSevdeskApiToken());
		adminService.updateAdminConf("sevdeskApiToken", getSevdeskApiToken(), null);
		
		// showSevdesk
		getAdminConfMap().put("showSevdesk", getShowSevdesk());
		adminService.updateAdminConf("showSevdesk", getShowSevdesk(), null);
		
		// SevdeskAuthentication
		getAdminConfMap().put("sevdeskAuthentication", getSevdeskAuthentication());
		adminService.updateAdminConf("sevdeskAuthentication", getSevdeskAuthentication(), null);
		
		//showIngeConfig
		getAdminConfMap().put("showIngeConfig", getShowIngeConfig());
		adminService.updateAdminConf("showIngeConfig", getShowIngeConfig(), null);
		
		//PasswordChangeValid
		getAdminConfMap().put("passwordChangeValid", getPasswordChangeValid()); 
		adminService.updateAdminConf("passwordChangeValid", getPasswordChangeValid(), null);

		//PasswordValidation
		getAdminConfMap().put("passwordValidation", getPasswordValidation()); 
		adminService.updateAdminConf("passwordValidation", getPasswordValidation(), null);

		
        adminConfMap = adminService.getAdminConfMap(null);
		addSessionObj(CommonConstants.ADMIN_CONF, adminConfMap);
		redirector(CommonConstants.LIST_PAGE);

	}

	public void cancelAction() {
		redirector(CommonConstants.DASHBOARD);
	}

	public void handleFileUpload(FileUploadEvent event) {
		String key = (String) event.getComponent().getAttributes().get("param");
		UploadedFile file = event.getFile();
		if (getAdminConfMap() == null) {
			setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
		}
		String fileName = file.getFileName();
		fileUpload(file, fileName);
		getAdminConfMap().put(key, fileName);
		adminService.updateAdminConf(key, fileName, null);
	}

	public String getCompanyName() {
		if (companyName == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setCompanyName(getAdminConfMap().get("companyName"));
		}
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public UploadedFile getCompanyLogo() {
		return companyLogo;
	}

	public void setCompanyLogo(UploadedFile companyLogo) {
		this.companyLogo = companyLogo;
	}

	public UploadedFile getFavIcon() {
		return favIcon;
	}

	public void setFavIcon(UploadedFile favIcon) {
		this.favIcon = favIcon;
	}

	public String getVersion() {
		if (version == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setVersion(getAdminConfMap().get("version"));
		}
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDefaultLocale() {
		if (defaultLocale == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setDefaultLocale(getAdminConfMap().get("defaultLocale"));
		}
		return defaultLocale;
	}

	public void setDefaultLocale(String defaultLocale) {
		this.defaultLocale = defaultLocale;
	}

	public Map<String, String> getAdminConfMap() {
		return adminConfMap;
	}

	public void setAdminConfMap(Map<String, String> adminConfMap) {
		this.adminConfMap = adminConfMap;
	}

	public String getSecondLevelAuth() {
		if (secondLevelAuth == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSecondLevelAuth(getAdminConfMap().get("secondLevelAuth"));
		}
		return secondLevelAuth;
	}

	public void setSecondLevelAuth(String secondLevelAuth) {
		this.secondLevelAuth = secondLevelAuth;
	}

	public Map<String, String> getLocales() {
		if (locales == null) {
			locales = new HashMap<String, String>();
			locales.put("Deutsch", "de");
			locales.put("Englisch", "en");
		}
		return locales;
	}

	public void setLocales(Map<String, String> locales) {
		this.locales = locales;
	}

	public String getIngeConfiguration() {

		if (ingeConfiguration == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setIngeConfiguration(getAdminConfMap().get("ingeConfiguration"));
		}

		return ingeConfiguration;
	}

	public void setIngeConfiguration(String ingeConfiguration) {
		this.ingeConfiguration = ingeConfiguration;
	}

	public String getSmsUserName() {

		if (smsUserName == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSmsUserName(getAdminConfMap().get("smsUserName"));
		}

		return smsUserName;
	}

	public void setSmsUserName(String smsUserName) {
		this.smsUserName = smsUserName;
	}

	public String getSmsPassword() {
		if (smsPassword == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSmsPassword(getAdminConfMap().get("smsPassword"));
		}

		return smsPassword;
	}

	public void setSmsPassword(String smsPassword) {
		this.smsPassword = smsPassword;
	}

	public String getSecondLevelAuthDisable() {
		return secondLevelAuthDisable;
	}

	public void setSecondLevelAuthDisable(String secondLevelAuthDisable) {
		this.secondLevelAuthDisable = secondLevelAuthDisable;
	}

	public String getIpAddressLog() {
		if (ipAddressLog == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setIpAddressLog(getAdminConfMap().get("ipAddressLog"));
		}
		return ipAddressLog;
	}

	public void setIpAddressLog(String ipAddressLog) {
		this.ipAddressLog = ipAddressLog;
	}

	public String getGoogleMapConfiguration() {
		if(googleMapConfiguration == null){
			if(getAdminConfMap() == null){
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
				}
			setGoogleMapConfiguration(getAdminConfMap().get("googleAPIKeyConfig"));
		}
		
		return googleMapConfiguration;
	}

	public void setGoogleMapConfiguration(String googleMapConfiguration) {
		this.googleMapConfiguration = googleMapConfiguration;
	}

	public String getGoogleApiKey() {
		if(googleApiKey == null){
			if(getAdminConfMap() == null){
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
				}
			setGoogleApiKey(getAdminConfMap().get("googleAPIKey"));
		}
		
		return googleApiKey;
	}

	public void setGoogleApiKey(String googleApiKey) {
		this.googleApiKey = googleApiKey;
	}
	
	public String getQcSurveyURL() {
		if (qcSurveyURL == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setQcSurveyURL(getAdminConfMap().get("qcSurveyURL"));
		}
		
		return qcSurveyURL;
	}

	public void setQcSurveyURL(String qcSurveyURL) {
		this.qcSurveyURL = qcSurveyURL;
	}

	
	
	
	public String getSevdeskUserName() {
		
		if (sevdeskUserName == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSevdeskUserName(getAdminConfMap().get("sevdeskUserName"));
		}

		
		return sevdeskUserName;
	}

	public void setSevdeskUserName(String sevdeskUserName) {
		this.sevdeskUserName = sevdeskUserName;
	}

	public String getSevdeskApiToken() {
		
		if (sevdeskApiToken == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSevdeskApiToken(getAdminConfMap().get("sevdeskApiToken"));
		}
		
		return sevdeskApiToken;
	}

	public void setSevdeskApiToken(String sevdeskApiToken) {
		this.sevdeskApiToken = sevdeskApiToken;
	}
	
	

	public String getShowSevdesk() {
		
		if (showSevdesk == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setShowSevdesk(getAdminConfMap().get("showSevdesk"));
		}
		return showSevdesk;
	}

	public void setShowSevdesk(String showSevdesk) {
		this.showSevdesk = showSevdesk;
	}
	
	public String getShowIngeConfig() {
		if (showIngeConfig == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setShowIngeConfig(getAdminConfMap().get("showIngeConfig"));
		}
		return showIngeConfig;
	}

	public void setShowIngeConfig(String showIngeConfig) {
		this.showIngeConfig = showIngeConfig;
	}
	
	

	public String getSevdeskAuthentication() {
		if (sevdeskAuthentication == null) {
			if (getAdminConfMap() == null) {
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
			}
			setSevdeskAuthentication(getAdminConfMap().get("sevdeskAuthentication"));
		}
		return sevdeskAuthentication;
	}

	public void setSevdeskAuthentication(String sevdeskAuthentication) {
		this.sevdeskAuthentication = sevdeskAuthentication;
	}

	public List<MetaTable> getMetaTableList() {
		return metaTableList;
	}

	public void setMetaTableList(List<MetaTable> metaTableList) {
		this.metaTableList = metaTableList;
	}

	public String getShowSurveyUrl() {
		return showSurveyUrl;
	}

	public void setShowSurveyUrl(String showSurveyUrl) {
		this.showSurveyUrl = showSurveyUrl;
	}

	public String getPasswordChangeValid() {
		if(passwordChangeValid == null){
			if(getAdminConfMap() == null){
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
				}
			setPasswordChangeValid(getAdminConfMap().get("passwordChangeValid"));
		}
		return passwordChangeValid;
	}

	public void setPasswordChangeValid(String passwordChangeValid) {
		this.passwordChangeValid = passwordChangeValid;
	}

	public String getPasswordValidation() {
		if(passwordValidation == null){
			if(getAdminConfMap() == null){
				setAdminConfMap((Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF));
				}
			setPasswordValidation(getAdminConfMap().get("passwordValidation"));
		}
		return passwordValidation;
	}

	public void setPasswordValidation(String passwordValidation) {
		this.passwordValidation = passwordValidation;
	}

	
}
