package com.orb.admin.mailtemplates.controller;

import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.mailtemplates.constants.MailTemplatesConstants;
import com.orb.admin.mailtemplates.model.MailTemplates;
import com.orb.admin.mailtemplates.service.MailTemplatesService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;


@ManagedBean(name="mailTemplatesListView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class MailTemplatesBeanListView extends AbstractCommonController{
	
	private static Log logger = LogFactory.getLog(MailTemplatesBeanListView.class);
	
	@Autowired
	private MailTemplatesService mailTemplatesService;
	
	private String id = null;
	private String identifier = null;
	private List<Boolean> togglerList = null;
	private User loginUser=null;
	private List<MailTemplates> templateList=null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<MailTemplates> selectedTemplates = null;
	private List<MailTemplates> filteredTemplateList = null;
	private MailTemplates mailTemplates = null;
	
	
	


	/**
	 * To access modules list page
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMailTemplates")
	public String loadMailTemplatesList(HttpServletRequest request, ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return MailTemplatesConstants.LIST_HTML; 
	}

	/**
	 * This method will be called when user clicks particular User from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMailTemplatesView")
	public String loadMailTemplatesView(HttpServletRequest httpRequest, ModelMap model){
		id = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( MailTemplatesConstants.SELECTED_MAIL_TEMPLATES_ID, id );
		return MailTemplatesConstants.VIEW_HTML;
		
	}
	
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		id = CommonUtils.getValidString(getJSFRequestAttribute(MailTemplatesConstants.SELECTED_MAIL_TEMPLATES_ID ));
		logger.info(id);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			templateList = mailTemplatesService.getInactiveTemplateList(commonError);
			showActivateBtn = CommonConstants.TRUE;
			showActiveListBtn = CommonConstants.TRUE;
		}else{
			templateList = mailTemplatesService.getActiveTemplateList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(MailTemplatesConstants.MAIL_TEMPLATES_TOGGLER_LIST);
		if(togglerList==null){
				togglerList = Arrays.asList(false,true,true, true);
		}
		if(!id.isEmpty()){
			mailTemplates = mailTemplatesService.getMailTemplates(commonError, id);
		}
		
	}
	
	/** This method will get all the active MailTemplates
	 * @param event
	*/
	public void showActive(ActionEvent event){
		templateList = mailTemplatesService.getActiveTemplateList(commonError);
		addSessionObj(MailTemplatesConstants.SHOW_ACTIVE_MAIL_TEMPLATES, CommonConstants.Y);
		if( filteredTemplateList != null ) filteredTemplateList.clear();
		showActivateBtn = CommonConstants.FALSE;
		showActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will show all the inactive MailTemplates
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		templateList = mailTemplatesService.getInactiveTemplateList(commonError);
		if( filteredTemplateList != null ) filteredTemplateList.clear();
		showActivateBtn = CommonConstants.TRUE;
		showActiveListBtn = CommonConstants.TRUE;
	}
	
	
	/**
	 * This method locks MailTemplates
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		mailTemplatesService.lockMailTemplates( commonError, loginUser, selectedTemplates);
		showActive(e);
		customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_LOCKED_SUCCESS, MailTemplatesConstants.LIST_PAGE);
	}
	
	
	/**
	 * This method unlocks MailTemplates
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		mailTemplatesService.unlockMailTemplates( commonError, loginUser, selectedTemplates );
		showInactive(e);
		customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_UNLOCKED_SUCCESS, MailTemplatesConstants.LIST_PAGE );
	}
	
	/**
	 * This method will delete the MailTemplates
	 * @param event
	 */
	public void deleteAction(ActionEvent e){
		if(!selectedTemplates.isEmpty()){
			mailTemplatesService.deleteMailTemplates( commonError, loginUser, selectedTemplates );
		if( commonError.getError().isEmpty() ){
			customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_DELETE_SUCCESS, MailTemplatesConstants.LIST_PAGE);
		}
		showInactive(e);
		commonError.clearErrors();
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_DELETE_SUCCESS, MailTemplatesConstants.LIST_PAGE);
		}

	}
	
	
	/**
	 * This method will navigate to MailTemplates Edit page
	 * @param index
	 * @return
	 */
	public void editAction(String index){
		mailTemplates = mailTemplatesService.getMailTemplates(commonError, index);
		redirector(MailTemplatesConstants.EDIT_PAGE+"?objectId="+mailTemplates.getIdentifier());
	}

	/**
	 * This method will navigate to MailTemplates View page
	 * @param index
	 * @return
	 */
	public void viewAction(String index){
		mailTemplates = mailTemplatesService.getMailTemplates(commonError, index);
		addSessionObj( MailTemplatesConstants.MAIL_TEMPLATES_TOGGLER_LIST , togglerList );
		redirector(MailTemplatesConstants.VIEW_PAGE+"?objectId="+mailTemplates.getIdentifier());
		
	}	
	
	/**
	 * Delete viewed MailTemplates
	 * @param index
	 */
	public void deleteViewedTemplates(String index){
		mailTemplates = mailTemplatesService.getMailTemplates(commonError,index );
		mailTemplatesService.deleteViewedTemplates( commonError, loginUser, mailTemplates);
		if( commonError.getError().isEmpty() ){
			customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_DELETE_SUCCESS, MailTemplatesConstants.LIST_PAGE );
		}
	}
	
	/**
	 * This method to returns to main MailTemplates list page
	 * @param list page
	 * @return 
	 */
	public void cancelAction(){
		redirector(MailTemplatesConstants.LIST_PAGE +"?activeFlag="+mailTemplates.getActive());
	}
	
	/**
	 * This method locks viewed MailTemplates
	 * @param list page
	 * @return
	 */
	public void lockViewedMailTemplates(String index){
		mailTemplates = mailTemplatesService.getMailTemplates(commonError, index);
		mailTemplatesService.lockViewedMailTemplates( commonError, loginUser, mailTemplates );
		customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_LOCKED_SUCCESS, MailTemplatesConstants.LIST_PAGE);
	}

	/**
	 * This method locks viewed MailTemplates
	 * @param list page
	 * @return
	 */
	public void unlockViewedMailTemplates(String index){
		mailTemplates = mailTemplatesService.getMailTemplates(commonError, index);
		mailTemplatesService.unlockViewedMailTemplates( commonError, loginUser, mailTemplates );
		if( commonError.getError().isEmpty() ){
			customRedirector( MailTemplatesConstants.MAIL_TEMPLATES_UNLOCKED_SUCCESS, MailTemplatesConstants.LIST_PAGE );
		}
	}
	
	
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param list page
	 * @return
	 */
	public void onToggle(ToggleEvent e) {
        togglerList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	
	
	/**
	 * To clear the session variables
	 * @param model
	 */
    
	@SuppressWarnings("rawtypes")
   	private void clearSessionObjects( ModelMap model ){
    	String id = (String)model.get( MailTemplatesConstants.SELECTED_MAIL_TEMPLATES_ID);
    	String showActive = (String)model.get( MailTemplatesConstants.SHOW_ACTIVE_MAIL_TEMPLATES ); 
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( MailTemplatesConstants.SHOW_ACTIVE_MAIL_TEMPLATES );
		if( showActive != null ) 
			model.remove( MailTemplatesConstants.SHOW_ACTIVE_MAIL_TEMPLATES );
		if( toggleList != null ) model.remove( MailTemplatesConstants.MAIL_TEMPLATES_TOGGLER_LIST );
	}
	

	
	/** This method will navigate to the page where user can add a MailTemplates
	 * @return
	 */
	public void addAction()	{
		redirector(MailTemplatesConstants.EDIT_PAGE);
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}

	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}

	public String getShowActivateBtn() {
		return showActivateBtn;
	}

	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}

	public List<MailTemplates> getSelectedTemplates() {
		return selectedTemplates;
	}

	public void setSelectedTemplates(List<MailTemplates> selectedTemplates) {
		this.selectedTemplates = selectedTemplates;
	}
    
	public List<MailTemplates> getTemplateList() {
		return templateList;
	}

	public void setTemplateList(List<MailTemplates> templateList) {
		this.templateList = templateList;
	}
    
	public List<MailTemplates> getFilteredTemplateList() {
		return filteredTemplateList;
	}

	public void setFilteredTemplateList(List<MailTemplates> filteredTemplateList) {
		this.filteredTemplateList = filteredTemplateList;
	}

	public MailTemplates getMailTemplates() {
		return mailTemplates;
	}

	public void setMailTemplates(MailTemplates mailTemplates) {
		this.mailTemplates = mailTemplates;
	}

	public MailTemplatesService getMailTemplatesService() {
		return mailTemplatesService;
	}

	public void setMailTemplatesService(MailTemplatesService mailTemplatesService) {
		this.mailTemplatesService = mailTemplatesService;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
}
