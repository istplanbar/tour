package com.orb.admin.mailtemplates.controller;


import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.mailtemplates.constants.MailTemplatesConstants;
import com.orb.admin.mailtemplates.model.MailTemplates;
import com.orb.admin.mailtemplates.service.MailTemplatesService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;



@ManagedBean(name="mailTemplatesAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/admin")

public class MailTemplatesBeanAddUpdate extends AbstractCommonController{
	
	

	private static Log logger = LogFactory.getLog(MailTemplatesBeanAddUpdate.class);
	
	@Autowired 
	private MailTemplatesService mailTemplatesService;
	
	private String id=null;
	private User loginUser = null;
	private String showIdentifier = CommonConstants.TRUE;
	private List<MailTemplates> templateList = null;
	private MailTemplates mailTemplates = null;
		

	/**
	 * This method will be called when user clicks Add button /  Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMailTemplatesAddUpdate")
	public String loadMailTemplatesAddUpdate(HttpServletRequest httpRequest, ModelMap model){
		id = CommonUtils.getValidString( httpRequest.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( MailTemplatesConstants.SELECTED_MAIL_TEMPLATES_ID, id );
		return MailTemplatesConstants.EDIT_HTML;
		
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
    	id = CommonUtils.getValidString(getJSFRequestAttribute( MailTemplatesConstants.SELECTED_MAIL_TEMPLATES_ID ) );
	    logger.info(id);
	    if(id.isEmpty()){
	    	mailTemplates = new MailTemplates();
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			mailTemplates=mailTemplatesService.getMailTemplates(commonError, id);
			if( mailTemplates == null ) mailTemplates = new MailTemplates();
	    }
	}
	
	/**
	 * This method will add / update a mailTemplates in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if( "".equals(CommonUtils.getValidString(mailTemplates.getId())) ){
			mailTemplatesService.addMailTemplates( commonError, loginUser, mailTemplates );
		}else{
			mailTemplatesService.updateMailTemplates( commonError, loginUser, mailTemplates );
		}
		templateList = mailTemplatesService.getActiveTemplateList(commonError);
	 	String msg="".equals(CommonUtils.getValidString(mailTemplates.getId())) ? MailTemplatesConstants.MAIL_TEMPLATES_ADDED_SUCCESS : MailTemplatesConstants.MAIL_TEMPLATES_UPDATED_SUCCESS ;
		customRedirector( msg, MailTemplatesConstants.LIST_PAGE);
	}
	
	/**
	 * This method to returns to mailTemplates view page
	 * @param list page
	 * @return 
	 */
	public void backView(){
		if(mailTemplates.getId()==null){
			redirector(MailTemplatesConstants.LIST_PAGE);
		}else{
		    redirector(MailTemplatesConstants.VIEW_PAGE+"?objectId="+mailTemplates.getIdentifier());
		}
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public List<MailTemplates> getTemplateList() {
		return templateList;
	}

	public void setTemplateList(List<MailTemplates> templateList) {
		this.templateList = templateList;
	}
    
	public MailTemplatesService getMailTemplatesService() {
		return mailTemplatesService;
	}

	public void setMailTemplatesService(MailTemplatesService mailTemplatesService) {
		this.mailTemplatesService = mailTemplatesService;
	}

	public MailTemplates getMailTemplates() {
		return mailTemplates;
	}

	public void setMailTemplates(MailTemplates mailTemplates) {
		this.mailTemplates = mailTemplates;
	}
}
