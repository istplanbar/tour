package com.orb.tour.controller;

import static java.time.temporal.TemporalAdjusters.lastDayOfYear;

import java.io.File;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.IsoFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.examples.vehiclerouting.domain.Customer;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolution;
import org.optaplanner.examples.vehiclerouting.domain.location.RoadLocation;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Vacation;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.optaplanner.rest.domain.bean.JsonCustomer;
import com.orb.optaplanner.rest.domain.bean.JsonMessage;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoute;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.taskplanner.bean.Task;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.bean.TourCluster;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name = "tourClusterView")
@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@ViewScoped
@RequestMapping("/process/tour")
public class TourClusterManagedBean extends AbstractTourController {
	private static Log logger = LogFactory.getLog(TourClusterManagedBean.class);

	private List<Employee> employeesList = null;
	private List<Employee> filteredEmployeeList = null;
	private static List<Employee> availableEmployeeList = null;
	private List<Facilities> facilitiesList = null;
	private static List<Facilities> selectedFacilitiesList = null;
	private List<KorfAddress> korfmannLocationList = null;
	private Map<String, String> monthMap = null;
	private List<String> yearList = null;
	//private DualListModel<Facilities> facilityPickList = null;
	private List<String> facilityPickList = null;
	private TourCluster tourCluster = null;
	private List<Facilities> sourceFacilitiesList = null;
	private List<Facilities> targetFacilitiesList = null;
	private List<Task> tasksList = null;
	private List<Task> filterTaskList = null;
	private static List<Task> tasksToBeDone = null;
	private static JsonVehicleRoutingSolution jsonVehicleRoutingSolution = null;
	private static List<JsonVehicleRoutingSolution> jsonVehicleRoutingSolutionSet = null;
	private List<Tour> createdTourList = null;
	private List<Tour> filteredTourList = null;
	private List<Tour> selectedTourList = null;
	private List<Boolean> togglerList = null;
	private Map<String, String> estimatedWorkHoursMap = null;
	private User loginUser = null;
	private Map<String, String> localAdminConf = null;
	public VehicleRoutingSolution vehicleRoutingSolution = null;
	private static final String SOLVER_CONFIG =  TourConstants.SOLVER_CONFIG_PATH;
	private SolverFactory<VehicleRoutingSolution> solverFactory = null;;
	private static final ExecutorService executor = Executors.newFixedThreadPool(10);
	Map<Long, Map<Long, Long>> travelTimeMap = new LinkedHashMap<>();
	private String mapApiKey = null;
	List<Employee> selectedEmployeesList = null;
	List<Future<?>> futures = new ArrayList<Future<?>>();
	private String dayOfTour = null;
	private static Date tourStartDate = null;
	private Map<String, List<VisitingDayTimeHours>> facilityVisitingDetailsMap = null;
	private static GeoApiContext geoContext;

	

	@RequestMapping(value = "/solve", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody JsonMessage solve() {
		JsonMessage jsonMessage = new JsonMessage(CommonConstants.FALSE);
		if(solve(commonError)){
			jsonMessage = new JsonMessage(CommonConstants.TRUE);
		}
		logger.info("Solve Rest Service is successfully processed");
		return jsonMessage;
	} 

	@RequestMapping(value = "/solution", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody JsonVehicleRoutingSolution getSolution(ModelMap model) {
		logger.info("Optaplanner Solution is Created ");
		jsonVehicleRoutingSolution = new JsonVehicleRoutingSolution();
		if(!jsonVehicleRoutingSolutionSet.isEmpty()){
			List<JsonCustomer> finalCustomerList = new ArrayList<>();
			List<JsonVehicleRoute> finalVehicleRouteList = new ArrayList<>();
			for(JsonVehicleRoutingSolution solution : jsonVehicleRoutingSolutionSet){
				finalCustomerList.addAll(solution.getCustomerList());
				finalVehicleRouteList.addAll(solution.getVehicleRouteList());
				jsonVehicleRoutingSolution.setName(solution.getName());
			}
			jsonVehicleRoutingSolution.setCustomerList(finalCustomerList);
			jsonVehicleRoutingSolution.setVehicleRouteList(finalVehicleRouteList);
		}else{
			jsonVehicleRoutingSolution = optaplannerTourClusterService.loadSolution();
		}
		if(jsonVehicleRoutingSolution == null) jsonVehicleRoutingSolution = new JsonVehicleRoutingSolution();
		logger.info("jsonVehicleRoutingSolutionList size: "+jsonVehicleRoutingSolutionSet.size());
		return jsonVehicleRoutingSolution;
	}

	@RequestMapping(value = "/getEmployeeFullName", method = RequestMethod.GET, produces = "application/json;charset=UTF-8;")
	public @ResponseBody String getEmployeeFullName(@RequestParam("empName") String empName) throws JSONException {
		JSONObject employeeFullName = new JSONObject();
		if (!empName.isEmpty()) {
			employeeFullName.put("empName", getUserFullName(empName));
		}
		return employeeFullName.toString();
	}

	@RequestMapping(value = "/loadSolution", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody JsonVehicleRoutingSolution loadSolution() {
		logger.info("Optaplanner Solution is Created : ");
		JsonVehicleRoutingSolution initialSolution = new JsonVehicleRoutingSolution();
		initialSolution = optaplannerTourClusterService.loadSolution();
		return initialSolution;
	}

	/**
	 * This page will be called when user clicks the Tour link on the menu.
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadTourCluster")
	public String loadTour(HttpServletRequest request, ModelMap model) {
		String activeFlag = CommonUtils.getValidString(request.getParameter(CommonConstants.ACTIVE_FLAG));
		model.addAttribute(CommonConstants.ACTIVE_FLAG, activeFlag);
		return TourConstants.TOUR_CLUSTER_CREATE_VIEW_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction() {
		solverFactory = SolverFactory.createFromXmlFile(new File(SOLVER_CONFIG));
		travelTimeMap = new HashMap<>();
		selectedTourList = new ArrayList<>();
		selectedEmployeesList = new ArrayList<>();
		jsonVehicleRoutingSolution = new JsonVehicleRoutingSolution();
		jsonVehicleRoutingSolutionSet = new ArrayList<>();
		tasksList = new ArrayList<>();
		employeesList = new ArrayList<>();
		korfmannLocationList = new ArrayList<>();
		filteredEmployeeList = new ArrayList<>();
		//facilityPickList = new DualListModel<>();
		
		facilityPickList = new ArrayList<>();
		availableEmployeeList = new ArrayList<>();
		tourCluster = new TourCluster();
		yearList = new ArrayList<>();
		monthMap = new HashMap<>();
		sourceFacilitiesList = new ArrayList<>();
		targetFacilitiesList = new ArrayList<>();
		createdTourList = new ArrayList<>();
		tasksToBeDone = new ArrayList<>();
		facilityVisitingDetailsMap = new HashMap<>();

		tourCluster.setWorkingHours(TourConstants.OFFICE_HOURS);
		tourCluster.setBreakAtWork(TourConstants.BREAK_HOURS);
		tourCluster.setMaintenanceCycleConsideration(CommonConstants.TRUE);
		tourCluster.setConsiderAbsence(CommonConstants.TRUE);
		monthMap = FacilitiesConstants.MAINTENACE_CYCLE_MONTH_NAME;
		loginUser = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		localAdminConf = (Map<String, String>) getSessionObj(CommonConstants.ADMIN_CONF);
		mapApiKey = localAdminConf.get("googleAPIKey");

		estimatedWorkHoursMap = FacilitiesConstants.ESTIMATED_WORK_HOURS;
		togglerList = (List<Boolean>) getSessionObj(TourConstants.TOUR_TOGGLER_LIST);
		korfmannLocationList = tourService.getActiveKorfAddressList(commonError);
		if (togglerList == null) {
			togglerList = Arrays.asList(true, true, true, true, true);
		}

		try {
			//executor = Executors.newFixedThreadPool(10);
			facilitiesList = tourService.getActiveFacilitiesList(commonError).get();
			tasksList = tourService.getActiveTaskList(commonError).get();
			employeesList = tourService.getActiveEmployeeList(commonError).get();
			yearList = tourService.getMaintenanceYearList().get();
			//vacationList = tourService.getActiveVacationList(commonError).get();
		} catch (InterruptedException e) {
			logger.error("Exception in Threads");
		} catch (ExecutionException e) {
			logger.error("Exception in Threads");
		}
		//facilityPickList = new DualListModel<>(sourceFacilitiesList, targetFacilitiesList);
		facilityPickList = new ArrayList<>();
		
	}

	/**
	 * This methods is called to filter out the employees and filter out the
	 * facilities.
	 * 
	 * @param event
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void filterFacilities(AjaxBehaviorEvent event) {
		filterTaskList = new ArrayList<>();
		if(CommonUtils.getValidString(tourCluster.getKorfmannOfficeLocation()).isEmpty()
				|| CommonUtils.getValidString(tourCluster.getYearOfMaintenance()).isEmpty()){
			return;
		}
			
		switch (tourCluster.getPlanningHorizon()) {
		case "1":
			/*filterTaskList = tasksList.stream()
					.filter(t -> t.getDueDate() != null && tourCluster.getMonthOfMaintenance() != null
							&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
									.getMonthValue() == Integer.parseInt(tourCluster.getMonthOfMaintenance())
							&& tourCluster.getYearOfMaintenance() != null
							&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
									.getYear() == Integer.parseInt(tourCluster.getYearOfMaintenance()))
					.collect(Collectors.toList());*/
			
			//Anand modifying conditions for month clustering at 25 sep 2019
			
			filterTaskList = tasksList.stream()
			.filter(t -> t.getDueDate() != null && tourCluster.getMonthOfMaintenance() != null
					&& tourCluster.getYearOfMaintenance() != null
					&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
							.getYear() == Integer.parseInt(tourCluster.getYearOfMaintenance())
							&& getNumberOfDaysOfTheMonth(t.getDueDate(), t.getStartDate(), tourCluster.getMonthOfMaintenance(), tourCluster.getYearOfMaintenance()))
			.collect(Collectors.toList());
			
			
			break;

		case "2":

			/*filterTaskList = tasksList.stream()
					.filter(t -> t.getDueDate() != null && tourCluster.getQuarterlyCycle() != null
							&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
									.get(IsoFields.QUARTER_OF_YEAR) == Integer.parseInt(tourCluster.getQuarterlyCycle())
							&& tourCluster.getYearOfMaintenance() != null
							&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
									.getYear() == Integer.parseInt(tourCluster.getYearOfMaintenance()))
					.collect(Collectors.toList());*/
			
			filterTaskList = tasksList.stream()
			.filter(t -> t.getDueDate() != null && tourCluster.getQuarterlyCycle() != null
					&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
					.getYear() == Integer.parseInt(tourCluster.getYearOfMaintenance())
					&& tourCluster.getYearOfMaintenance() != null
					&& getQuarterOfTheYear(t.getStartDate(), t.getDueDate(), tourCluster.getQuarterlyCycle()))
					.collect(Collectors.toList());
			break;

		case "3":

			filterTaskList = tasksList.stream()
					.filter(t -> t.getDueDate() != null
							&& tourCluster.getYearOfMaintenance() != null
							&& t.getDueDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
									.getYear() == Integer.parseInt(tourCluster.getYearOfMaintenance()))
					.collect(Collectors.toList());
			break;
		}

		sourceFacilitiesList = new ArrayList<>(returnFilteredFacilities().stream().sorted(Comparator.comparing(Facilities::getName)).collect(Collectors.toList()));
		filteredEmployeeList = new ArrayList<>(returnFilteredEmployees());
		targetFacilitiesList = new ArrayList<>();
		//facilityPickList = new DualListModel<>(sourceFacilitiesList, targetFacilitiesList);
		//facilityPickList = new ArrayList<>(sourceFacilitiesList);
	}


	private boolean getQuarterOfTheYear(Date startDate, Date dueDate, String quarterlyCycle) {
		
		if(startDate == null) {
			if(dueDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
		.get(IsoFields.QUARTER_OF_YEAR) >= Integer.parseInt(quarterlyCycle)) {
				return true;
			} else {
				return false;
			}
		} else {
			if(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
					.get(IsoFields.QUARTER_OF_YEAR) <= Integer.parseInt(quarterlyCycle)
					&& dueDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
							.get(IsoFields.QUARTER_OF_YEAR) >= Integer.parseInt(quarterlyCycle)) {
				return true;
			} else {
				return false;
			}
		}		
	}

	/**
	 * This method will return true or false regards the month is possible for clustering
	 * @param dueDate
	 * @param startDate
	 * @param monthOfMaintenance
	 * @param yearOfMaintenance
	 * @return
	 */
	private boolean getNumberOfDaysOfTheMonth(Date dueDate, Date startDate, String monthOfMaintenance, String yearOfMaintenance) {
		
		int monthOfMaintain = Integer.valueOf(monthOfMaintenance);
		int month, monthForDueDate;
		Calendar calendar = new GregorianCalendar();
		Calendar calendarForDueDate = new GregorianCalendar();
		calendarForDueDate.setTime(dueDate);
		monthForDueDate = calendarForDueDate.get(Calendar.MONTH)+1;
		if(startDate == null) {
		    if(monthForDueDate >= monthOfMaintain) {
		    	return true;
		    } else {
		    	return false;
		    }
		} else {
		calendar.setTime(startDate);
	    month = calendar.get(Calendar.MONTH)+1;
	    
	    if(month <= monthOfMaintain && monthForDueDate >= monthOfMaintain) {
	    	return true;
	    } else {
	    	return false;
	    }
		}
		
	}

	/**
	 * This method returns the facilities based on selected criteria and tasks
	 * @return FacilitySet
	 */
	private Set<Facilities> returnFilteredFacilities() {
		Set<Facilities> facilitySet = new HashSet<>();
		if (!filterTaskList.isEmpty()) {
			for (Facilities facility : facilitiesList) {
				List<Task> facilitiesTask = new ArrayList<>();
				for (Task filteredTask : filterTaskList) {
					if (filteredTask.getFacilities() != null && filteredTask.getFacilities().equals(facility.getId())) {
						if (CommonUtils.isNotEmpty(filteredTask.getEstimatedTime())) {
							facilitiesTask.add(filteredTask);
						}
						facility.setTaskAndWorkHoursList(facilitiesTask);
						facilitySet.add(facility);
					}
				}
			}
		}
		logger.info("Facilities List " + facilitySet.size());
		return facilitySet;
	}

	/**
	 * This method returns the filtered employees based on filteredFacilities
	 * @return EmployeeSet
	 */
	private Set<Employee> returnFilteredEmployees() {
		Set<Employee> employeeSet = new HashSet<>();
		List<String> qualificationIdList = new ArrayList<>();
		for (Facilities f : sourceFacilitiesList) {
			qualificationIdList = Arrays.asList(f.getQualificationsId().split(","));
			if (!qualificationIdList.isEmpty()) {
				for (Employee e : employeesList) {
					if (e.getQualificationsMaId() != null) {
						List<String> empQualify = Arrays.asList(e.getQualificationsMaId().split(","));
						for (String eq : empQualify) {
							if (qualificationIdList.contains(eq)) {
								employeeSet.add(e);
							}
						}
					}
				}
			}
		}
		logger.info("Employee List" + employeeSet.size());
		return employeeSet;
	}
	
	/**
	 * This method returns the day of the tour date
	 * @param tourStartDate
	 * @return
	 */
	private int returnDayOftheWeek(Date tourStartDate){
		LocalDate localDate = tourStartDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		localDate = checkWeekendMethod(localDate);
		return localDate.getDayOfWeek().getValue();
	}

	
	/**
	 * This method make sure that the given tour date is not on weekend 
	 * @param localDate
	 * @return
	 */
	private LocalDate checkWeekendMethod(LocalDate localDate) {
		DayOfWeek dayOfWeek = localDate.getDayOfWeek();
		int day = dayOfWeek.getValue();
		if (day == 6) {
			localDate = localDate.plusDays(2);
		} else if (day == 7) {
			localDate = localDate.plusDays(1);
		}
		return localDate;
	}
	
	/**
	 * Generate next date based on tour date
	 * @param tourStartDate
	 * @return
	 */
	private Date returnNextDate(Date tourStartDate){
		LocalDate localDate = tourStartDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		localDate = localDate.plusDays(1);
		localDate = checkWeekendMethod(localDate);
		return  Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	/**
	 * This methods returns only the reamining facilities based on tasks done
	 * @return
	 */
	private List<Facilities> returnRemainingFacilities(){
		Set<Facilities> remainingFacilitySet = new HashSet<>();
		if (!tasksToBeDone.isEmpty()) {
			for (Facilities facility : selectedFacilitiesList) {
				List<Task> facilitiesTask = new ArrayList<>();
				for (Task filteredTask : tasksToBeDone) {
					if (filteredTask.getFacilities() != null && filteredTask.getFacilities().equals(facility.getId())) {
						if (CommonUtils.isNotEmpty(filteredTask.getEstimatedTime())) {
							facilitiesTask.add(filteredTask);
						}
						facility.setTaskAndWorkHoursList(facilitiesTask);
						remainingFacilitySet.add(facility);
					}
				}
			}
		}
		return new ArrayList<>(remainingFacilitySet);
	}
	
	/**
	 * This method creates tour clustering based on filtered data
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public void generateTourClustering(ActionEvent event) throws InterruptedException, ExecutionException {
		logger.info("Generating Tour Cluster is initiated.");
		long start = System.currentTimeMillis();
		targetFacilitiesList = new ArrayList<>();
		if (tourCluster.getEmployeeList() == null || tourCluster.getEmployeeList().isEmpty()
				|| CommonUtils.getValidString(tourCluster.getKorfmannOfficeLocation()).isEmpty()
				//|| facilityPickList.getTarget().isEmpty()) {
				|| facilityPickList.isEmpty()) {
			commonError.addError(getJSFMessage("err_emptyData"));
			RequestContext.getCurrentInstance().execute("PF('blockUIWidget').unblock();");
			return;
		}else{
			
			targetFacilitiesList = facilityPickList.stream()
					.flatMap(em -> sourceFacilitiesList.stream().filter(e -> em.equals(e.getId())))
					.collect(Collectors.toList());
			logger.info("Target Facilities size " +targetFacilitiesList.size());
			
			jsonVehicleRoutingSolution = new JsonVehicleRoutingSolution();
			jsonVehicleRoutingSolutionSet = new ArrayList<>();
			//selectedFacilitiesList = facilityPickList.getTarget();
			selectedFacilitiesList = targetFacilitiesList;
			logger.info("Selected Facilities List " +selectedFacilitiesList.size());
			selectedEmployeesList = tourCluster.getEmployeeList().stream()
					.flatMap(em -> filteredEmployeeList.stream().filter(e -> em.equals(e.getIdentifier())))
					.collect(Collectors.toList());

			//Map<String, List<VisitingDayTimeHours>> facilityVisitingDetailsMap = facilitiesService.getAllFacilitiesVisitingTimeAndHoursMap(commonError, selectedFacilitiesList);
			facilityVisitingDetailsMap = facilitiesService.getAllFacilitiesVisitingTimeAndHoursMap(commonError, selectedFacilitiesList);
			for(Facilities ft : selectedFacilitiesList){
				tasksToBeDone.addAll(ft.getTaskAndWorkHoursList());
			}
			
			tourStartDate =  tourService.getNextPossibleTourDate(commonError, null, tourCluster );

			while(tasksToBeDone.size() > 0){
				logger.info("Tasks To be Done Size "+ tasksToBeDone.size());
				int dayOfTheWeek = returnDayOftheWeek(tourStartDate);
				vehicleRoutingSolution = new VehicleRoutingSolution();
				List<Facilities> selectedDayFacilityList= new ArrayList<>();
				Set<Facilities> selectedOtherFacilitySet = new HashSet<>();
				selectedFacilitiesList = returnRemainingFacilities();
				selectedFacilitiesList.stream()
					.forEach( dayFacility -> {
						if(facilityVisitingDetailsMap.containsKey(dayFacility.getIdentifier())){
							List<VisitingDayTimeHours> visitingDayTimeHoursList = facilityVisitingDetailsMap.get(dayFacility.getIdentifier());
							if(checkDayOfVisit(visitingDayTimeHoursList, dayOfTheWeek)){
								setVisitingHoursToFacility(visitingDayTimeHoursList, dayOfTheWeek, dayFacility);
								//logger.info("Visiting time "+ dayFacility.getVisitingHours() +  " "+dayFacility.getVisitingTime());
								selectedDayFacilityList.add(dayFacility);
							}else if(!checkAnyDayOfVisit(visitingDayTimeHoursList)){
								selectedOtherFacilitySet.add(dayFacility);
							}
						}else{
							selectedOtherFacilitySet.add(dayFacility);
						}
					});
				dayOfTour = String.valueOf(dayOfTheWeek);
				logger.info("Selected Facility on this Date "+selectedDayFacilityList.size());
				logger.info("Other Facility on this Date "+selectedOtherFacilitySet.size());
				
				availableEmployeeList = getOnlyAvailableEmployeesList(tourStartDate);
				while(availableEmployeeList.isEmpty()) {
					tourStartDate = returnNextDate(tourStartDate);
					LocalDate now = LocalDate.now();
					LocalDate lastDay = now.with(lastDayOfYear()); 
					if(tourStartDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate() == lastDay){
						break;
					}
					availableEmployeeList = getOnlyAvailableEmployeesList(tourStartDate);
				}
				logger.info("Available Employee List Size "+availableEmployeeList.size());
				
				if((tasksToBeDone.size() > 0 && selectedDayFacilityList.size() == 0 && selectedOtherFacilitySet.size() == 0) ||
						(tasksToBeDone.size() > 0 && availableEmployeeList.size() == 0)){
					break;
				}
				List<Employee> tempEmpList = new ArrayList<>();
				List<Facilities> selectedFacilityList= new ArrayList<>();
				
				if(!selectedDayFacilityList.isEmpty()){
					tempEmpList = returnAvailableEmpList(selectedDayFacilityList.size()/5);
					if(selectedDayFacilityList.size() <=  4 && !selectedOtherFacilitySet.isEmpty()){
						Collection<Facilities> removeCandidates = new LinkedList<>();

						for(Facilities element : selectedOtherFacilitySet){
							if(selectedDayFacilityList.size() <= 5){
								selectedDayFacilityList.add(element);
								removeCandidates.add(element);
							}
						}

						selectedOtherFacilitySet.removeAll(removeCandidates);
					}

					if(selectedDayFacilityList.size() >= tempEmpList.size() * 30){
						selectedFacilityList = selectedDayFacilityList.stream().limit(tempEmpList.size() * 30).collect(Collectors.toList());
					}else{
						selectedFacilityList.addAll(selectedDayFacilityList);
					}

					vehicleRoutingSolution = optaplannerTourClusterService.generateClusterTours(commonError, tempEmpList,
							selectedFacilityList, tourCluster);
					solveSolution();
					createVehicleRoutes(tempEmpList);
					logger.info("No of Customers solved count on Date: "+vehicleRoutingSolution.getCustomerList().size() + 
							" by Employee Count: "+tempEmpList.size() +
							" on this Date: " +tourStartDate);
				}
				
				Set<Facilities> otherFacilitiesSet = new HashSet<>(); 
				if(!selectedOtherFacilitySet.isEmpty()){
					List<Employee> otherEmpList = returnAvailableEmpList(selectedOtherFacilitySet.size()/5);
					if(otherEmpList.size() > tempEmpList.size()){
						for(Employee oe : tempEmpList){
							otherEmpList.removeIf( e -> e.getIdentifier().equals(oe.getIdentifier()));
						}

						if(selectedOtherFacilitySet.size() >= otherEmpList.size() * 30){
							otherFacilitiesSet = selectedOtherFacilitySet.stream().limit(otherEmpList.size() * 30).collect(Collectors.toSet());
						}else{
							otherFacilitiesSet.addAll(selectedOtherFacilitySet);
						}

						vehicleRoutingSolution = optaplannerTourClusterService.generateClusterTours(commonError, otherEmpList,
								new ArrayList<>(otherFacilitiesSet), tourCluster);
						solveSolution();
						createVehicleRoutes(otherEmpList);
						logger.info("Customer solved count other Date: "+vehicleRoutingSolution.getCustomerList().size() +
								" Employee Count: "+otherEmpList.size() +
								" on this Date: " +tourStartDate);
						
					}
				}
				
				if(tasksToBeDone.size() > 0){
					logger.info("Selected Size "+selectedFacilityList.size());
					logger.info("Other Size "+otherFacilitiesSet.size());
					tourStartDate = returnNextDate(tourStartDate);
				}
			}
			
			logger.info("Generating Tour Cluster is Completed.");
			logger.info("Total Time is taken for Clustering: "+ (System.currentTimeMillis() - start) / 1000 + " sec");
		}
	}

	private void setVisitingHoursToFacility(List<VisitingDayTimeHours> visitingDayTimeHoursList, int dayOfTheWeek,
			Facilities dayFacility) {
		
		for(VisitingDayTimeHours vT : visitingDayTimeHoursList){
			if(vT.getVisitingDays().equals(String.valueOf(dayOfTheWeek))){
				dayFacility.setVisitingHours(!StringUtils.isEmpty(vT.getVisitingHours()) ? vT.getVisitingHours() : "");
				dayFacility.setVisitingTime(!StringUtils.isEmpty(vT.getVisitingTimes()) ? vT.getVisitingTimes() : "");
			}
		}
	}

	private List<Employee> returnAvailableEmpList(int estSize) {
		List<Employee> tempEmpList = new ArrayList<>(availableEmployeeList);
		if(estSize < availableEmployeeList.size()){
			for (int j = 0 ; j < availableEmployeeList.size()-estSize-1; j++) {
				tempEmpList.remove(tempEmpList.size()-1);
			}
		}
		return tempEmpList;
	}

	private boolean checkAnyDayOfVisit(List<VisitingDayTimeHours> visitingDayTimeHoursList) {
		Predicate<VisitingDayTimeHours> pr = vT ->  CommonUtils.isNotEmpty(vT.getVisitingHours()) && CommonUtils.isNotEmpty(vT.getVisitingTimes());
		return visitingDayTimeHoursList.stream().anyMatch(pr);
	}

	/**
	 * This method filters out available employees on particular tour date based on his vacation
	 */
	private List<Employee> getOnlyAvailableEmployeesList(Date tourStartDate) {
		Set<Employee> availableEmployeeSet = new HashSet<>();
		List<Vacation> empVacationList = new ArrayList<>(); 
		List<Vacation> vacationList = new ArrayList<>(); 
		try {
			for(Employee e : selectedEmployeesList){
				vacationList = tourService.getActiveVacationList(commonError).get();
				List<Boolean> checkIfAllDateOk = new ArrayList<>();
				if(vacationList != null){
						empVacationList = vacationList.stream()
											.filter(vacation -> Arrays.asList(vacation.getEmployee().split(",")).stream()
											.map(String::trim).collect(Collectors.toList()).contains(e.getIdentifier()))
											.collect(Collectors.toList());
				}
				if (!empVacationList.isEmpty()) {
					for (Vacation v : empVacationList) {
						boolean checkIfDateLiesInVacation = checkIfDateLiesInVacation(v.getStartDate(), v.getEndDate(), tourStartDate);
						if (CommonConstants.FALSE.equals(tourCluster.getConsiderAbsence())) {
							if (v.getVacationType().equals("0")) {
								if(!checkIfDateLiesInVacation)
									availableEmployeeSet.add(e);
							}
						} else {
							
							if(!checkIfDateLiesInVacation){
								checkIfAllDateOk.add(checkIfDateLiesInVacation);
								if(checkIfAllDateOk.size() == empVacationList.size()){
									availableEmployeeSet.add(e);
								}
							}
						}
					}
				}else{
					availableEmployeeSet.add(e);
				}
			}
		} catch (InterruptedException e1) {
			logger.error(e1.getLocalizedMessage());
		} catch (ExecutionException e1) {
			logger.error(e1.getLocalizedMessage());
		}
		return	new ArrayList<>(availableEmployeeSet);
	}

	/**
	 * This method check if the tour date between or on vacation date 
	 */
	private boolean checkIfDateLiesInVacation(Date startDate, Date endDate, Date tourStartDate) {
		if((endDate != null && tourStartDate.compareTo(startDate) >= 0 && tourStartDate.compareTo(endDate) <= 0) || tourStartDate.equals(startDate)){
			return true;
		}
		return false;
	}

	/**
	 * This methods checks there facility has predefined visiting day time hour
	 */
	private boolean checkDayOfVisit(List<VisitingDayTimeHours> visitingDayTimeHoursList, int val) {
		Predicate<VisitingDayTimeHours> pr = vT -> vT.getVisitingDays().equals(String.valueOf(val)) && CommonUtils.isNotEmpty(vT.getVisitingHours());
		return visitingDayTimeHoursList.stream().anyMatch(pr);
	}

	
	
	
	/**
	 * This method calls the optaplanner solver method and holds the thread until it solves
	 */
	private void solveSolution() {
		if(!vehicleRoutingSolution.getCustomerList().isEmpty()){
			try{
				solve(commonError);

				for(Future<?> future : futures){
					try {
						future.get();
					} catch (Exception e1) {
						logger.info("Thread Error");
					}
				}

				Map<Long, Long> travelTimeEndLocationMap = new LinkedHashMap<>();
				Map<Long, Long> travelTimeNextLocationMap = new LinkedHashMap<>();
				for(Vehicle v : vehicleRoutingSolution.getVehicleList()){
					Customer c = v.getNextCustomer();
					RoadLocation depot = (RoadLocation) v.getDepot().getLocation();
					while(c != null){
						long travelTime = getTravelDistanceTime(commonError, depot.getLatitude(), depot.getLongitude(),
								c.getLocation().getLatitude(), c.getLocation().getLongitude(), mapApiKey);
						travelTimeEndLocationMap.put(c.getId(), travelTime);
						travelTimeMap.put(depot.getId(), travelTimeEndLocationMap);
						travelTimeNextLocationMap = new LinkedHashMap<>();
						if(c.getNextCustomer() != null){
							long travelTime2 = getTravelDistanceTime(commonError, c.getLocation().getLatitude(), c.getLocation().getLongitude(),
									c.getNextCustomer().getLocation().getLatitude(), c.getNextCustomer().getLocation().getLongitude(), mapApiKey);

							travelTimeNextLocationMap.put(c.getNextCustomer().getId(), travelTime2);
							travelTimeMap.put(c.getId(), travelTimeNextLocationMap);
						}
						c = c.getNextCustomer();
					}
				}
				logger.info("Travel Distance Time is found for all customers");
				
			}catch(Exception  | OutOfMemoryError e){
				RequestContext.getCurrentInstance().execute("PF('blockUIWidget').unblock();");
				logger.error("Exception on Clustering either memory error or other exceptions",e);
				commonError.addError("Exception on Clustering either memory error or other exceptions"+e.getMessage());
				geoContext.shutdown();
				executor.shutdown();
				throw new RuntimeException("Exception on Clustering either memory error or other exceptions: "+e.getMessage());
				
			}
		}
	}

	private void createVehicleRoutes(List<Employee> filteredEmployeeList) {
		if(vehicleRoutingSolution.getVehicleList().get(0).getNextCustomer() != null){
			JsonVehicleRoutingSolution jsonSolution = optaplannerTourClusterService.retrieveSolution(commonError, filteredEmployeeList, 
					vehicleRoutingSolution, travelTimeMap, dayOfTour);

			if(jsonSolution.getVehicleRouteList() != null && !jsonSolution.getVehicleRouteList().isEmpty()){
				jsonVehicleRoutingSolutionSet.add(jsonSolution);
				for(JsonVehicleRoute vr : jsonSolution.getVehicleRouteList()){
					vr.setTourDate(tourStartDate);
					for(JsonCustomer co : vr.getCustomerList()){
						for(Task ta : co.getTasksList()){
							if(tasksToBeDone.contains((Task)ta)){
								tasksToBeDone.remove((Task)ta);
								//logger.info("Tasks To be Done Size "+ tasksToBeDone.size());
							}
						}
					}
				}
			}else{
				tasksToBeDone = new ArrayList<>();
			}
		}
	}

	public synchronized boolean solve(CommonError commonError) {
		logger.info("Solver is initialized");

		long start = System.currentTimeMillis();
		final Solver<VehicleRoutingSolution> solver = solverFactory.buildSolver();
		try{
			solver.addEventListener(event -> {
				VehicleRoutingSolution bestSolution = (VehicleRoutingSolution)event.getNewBestSolution();
				synchronized (TourClusterManagedBean.this) {
					vehicleRoutingSolution = bestSolution;
				}
			});
			
			final VehicleRoutingSolution solution = vehicleRoutingSolution;
			Future<?> f = executor.submit((Runnable) () -> {
				VehicleRoutingSolution bestSolution = solver.solve(solution);
				synchronized (TourClusterManagedBean.this) {
					vehicleRoutingSolution = bestSolution;
					logger.info("BestSolution is added again");
				}
			});
			logger.info("Solving Time "+ (System.currentTimeMillis() - start)/1000 + " sec");
			futures.add(f);
		}catch(Exception e){
			commonError.addError("Execution is interrrupted: "+e.getMessage());
			logger.error("Execution is interrrupted: "+ e.getMessage());
			RequestContext.getCurrentInstance().execute("PF('blockUIWidget').unblock();");
			geoContext.shutdown();
			executor.shutdown();
			throw new RuntimeException("Exception on Clustering either memory error or other exceptions: "+e.getMessage());
		}
		logger.info("Solver is successfully processed " );
		return true;
	}

	
	
	/**
	 * The methods creates and stores Tour in Database
	 */
	public void showPossibleTours(TabChangeEvent event) {
		logger.info("Possible Tours Generation is Started.");
		if (!selectedTourList.isEmpty())
			selectedTourList = new ArrayList<>();
		createdTourList = new ArrayList<>();
		try {
			long start = System.currentTimeMillis();
			Integer i = 1;

			for (JsonVehicleRoute vehicleRoute : jsonVehicleRoutingSolution.getVehicleRouteList()) {
				Tour possibleTour = new Tour();
				List<String> facList = new ArrayList<>();
				StringBuilder facilityDetails = new StringBuilder();
				StringBuilder tourName = new StringBuilder();
				StringBuilder tasksName = new StringBuilder();
				possibleTour.setId(String.valueOf(i));

				if (CommonUtils.isNotEmpty(vehicleRoute.getEmployeeName())) {
					tourName.append(getUserFullName(vehicleRoute.getEmployeeName()));
					tourName.append(TourConstants.ROUTE_APPEND_OPERATOR);
					tourName.append(getJSFMessage("lbl_tour"));
					tourName.append(TourConstants.ROUTE_APPEND_OPERATOR);
					tourName.append(String.valueOf(i));
				}

				i++;
				for (JsonCustomer customer : vehicleRoute.getCustomerList()) {
					if (CommonUtils.isNotEmpty(vehicleRoute.getEmployeeName())) {
						possibleTour.setEmployee(vehicleRoute.getEmployeeName());
						if (CommonUtils.isNotEmpty(tourCluster.getKorfmannOfficeLocation())) {
							possibleTour.setKorfmannOfficeLocation(tourCluster.getKorfmannOfficeLocation());
						}
					}
					possibleTour.setTasks("");

					StringBuilder custDetails = new StringBuilder();
					/*facilityDetails.append(facilityPickList.getTarget().stream()
							.filter(f -> f.getId().equals(String.valueOf(customer.getId())))
							.map(Facilities::getIdentifier).collect(Collectors.joining("")));*/
					
					facilityDetails.append(targetFacilitiesList.stream()
							.filter(f -> f.getId().equals(String.valueOf(customer.getId())))
							.map(Facilities::getIdentifier).collect(Collectors.joining("")));
					
					facilityDetails.append(TourConstants.ROUTE_COMMA_OPERATOR);

					custDetails.append(customer.getLocationName());
					custDetails.append(TourConstants.ROUTE_OPERATOR);
					for (Task task : customer.getTasksList()) {
						tasksName.append(task.getId());
						tasksName.append(TourConstants.ROUTE_COMMA_OPERATOR);
						custDetails.append(task.getName());
						custDetails.append(" ["+task.getIdentifier()+"] ");
						custDetails.append(returnWorkHour(task.getEstimatedTime()));
						custDetails.append(",  ");
					}
					custDetails.setLength(custDetails.length() - 3);
					facList.add(custDetails.toString());
					custDetails.setLength(0);
				}

				if (facilityDetails.length() > 1) facilityDetails.setLength(facilityDetails.length() - 1);
				if (tasksName.length() > 1) tasksName.setLength(tasksName.length() - 1);
				possibleTour.setTasks(tasksName.toString());
				possibleTour.setName(tourName.toString());
				possibleTour.setFacilities(facilityDetails.toString());
				possibleTour.setFacilitiesList(facList);
				possibleTour.setDayOfTour(String.valueOf(returnDayOftheWeek(vehicleRoute.getTourDate())));
				possibleTour.setTourDuration(returnStringFormatTime(vehicleRoute.getTourDuration()));
				possibleTour.setTourDate(vehicleRoute.getTourDate());

				if (CommonUtils.isNotEmpty(possibleTour.getEmployee())
						&& CommonUtils.isNotEmpty(possibleTour.getName())) {
					createdTourList.add(possibleTour);
				}
			}
			logger.info("Possible Tours Generationis finished");
		} catch (Exception e) {
			logger.info("Exception on Possible Tours Creation: " + e.getMessage());
			//commonError.addError("Exception on Possible Tours Creation: " + e.getMessage());
		}

	}

	private String returnStringFormatTime(Long tourDuration) {
		
		long hr = (long) Math.floor(tourDuration / 60000);          
   	 	long mins = (long) Math.floor((tourDuration / 1000)%60);
   	    String totalDuration;
   	    if(hr != 0){
   	    	if(mins != 0)
   	    		totalDuration = hr+"h "+mins+" min";
   	    	else
   	    		totalDuration = hr+"h ";
   	    }else{
   	    	totalDuration = mins+" min";
   	    }
   	    return totalDuration;
	}

	/**
	 * This method will add / update a Tour in DB
	 * 
	 * @param event
	 */
	public void saveAction(ActionEvent event) {
		logger.info("Possible Tours are getting saved");
		if (!selectedTourList.isEmpty()) {
			for (Tour tour : selectedTourList) {

				List<String> facilityIds = new ArrayList<>();
				if (CommonUtils.isNotEmpty(tour.getEmployee())) {
					tour.setEmployee(
							filteredEmployeeList.stream().filter(emp -> emp.getFirstName().equals(tour.getEmployee()))
									.findFirst().map(Employee::getIdentifier).get());
				}
				if (CommonUtils.isNotEmpty(tour.getFacilities())) {
					facilityIds = Arrays.asList(tour.getFacilities().split(","));
				}
				taskPlannerService.updateTaskStatusList(Arrays.asList(tour.getTasks().split(",")));
				tourService.add(commonError, loginUser, tour, facilityIds);
			}
			customRedirector(TourConstants.TOUR_ADDED_SUCCESS, TourConstants.TOUR_CLUSTERING_URL);
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column
	 * 
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event) {
		togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	}

	/**
	 * Returns the estimated work hour in time
	 */
	private String returnWorkHour(String value) {
		StringBuffer workHour = new StringBuffer();
		for (String key : estimatedWorkHoursMap.keySet()) {
			if (estimatedWorkHoursMap.get(key).equals(value)) {
				workHour.append(" (");
				workHour.append(key);
				workHour.append(")");
				break;
			}
		}
		return workHour.toString();
	}

	/**
	 * This methods helps to get real traveling time between places
	 * @return
	 */
	private Long getTravelDistanceTime(CommonError commonError, double lat1, double lon1, double lat2, double lon2, String apiKey){
		Long travelTime = 0L; 
		try {
			//GeoApiContext context = new GeoApiContext.Builder().disableRetries().apiKey(apiKey).build();
	        DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(getGeoContext()); 
	        DistanceMatrix trix = req.origins(new LatLng(lat1,lon1))
	                .destinations(new LatLng(lat2,lon2))
	                .mode(TravelMode.DRIVING)
	                .language("de-DE")
	                .await();                                                                                                         
	        travelTime = (trix.rows[0].elements[0].duration.inSeconds)/60;
	    } catch(ApiException | InterruptedException | IOException e){
	    	 RequestContext.getCurrentInstance().execute("PF('blockUIWidget').unblock();");
		     commonError.addError("Google Maps API fehler: "+e.getMessage());
		     logger.info("Google Maps API fehler: "+ e.getMessage());
		     geoContext.shutdown();
	    	 executor.shutdown();
		     redirector(TourConstants.TOUR_CLUSTERING_URL);
		     throw new RuntimeException("Google Maps API fehler: "+e.getMessage());
	    }
	    return travelTime * 1000;
	}
	
	
	private  GeoApiContext getGeoContext() {
	     if(geoContext == null) {
	          geoContext = new GeoApiContext.Builder()
	        		  .disableRetries()
	        		  .apiKey(mapApiKey)
	        		  .readTimeout(10000, TimeUnit.SECONDS)
	                  .build();
	     }
	     return geoContext;
	 }
	public double distance( double lat1, double lon1, double lat2, double lon2, char unit) {
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  if (unit == 'K') {
		    dist = dist * 1.609344;
		  } else if (unit == 'N') {
		  dist = dist * 0.8684;
		    }
		  return (dist);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts decimal degrees to radians             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}

	
	
	public List<Employee> getEmployeeList() {
		return employeesList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeesList = employeeList;
	}

	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}

	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}

	public List<KorfAddress> getKorfmannLocationList() {
		return korfmannLocationList;
	}

	public void setKorfmannLocationList(List<KorfAddress> korfmannLocationList) {
		this.korfmannLocationList = korfmannLocationList;
	}

	public Map<String, String> getMonthMap() {
		return monthMap;
	}

	public void setMonthMap(Map<String, String> monthMap) {
		this.monthMap = monthMap;
	}

	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	/*public DualListModel<Facilities> getFacilityPickList() {
		return facilityPickList;
	}

	public void setFacilityPickList(DualListModel<Facilities> facilityPickList) {
		this.facilityPickList = facilityPickList;
	}*/
	

	public TourCluster getTourCluster() {
		return tourCluster;
	}

	public List<String> getFacilityPickList() {
		return facilityPickList;
	}

	public void setFacilityPickList(List<String> facilityPickList) {
		this.facilityPickList = facilityPickList;
	}

	public void setTourCluster(TourCluster tourCluster) {
		this.tourCluster = tourCluster;
	}

	public List<Employee> getFilteredEmployeeList() {
		return filteredEmployeeList;
	}

	public void setFilteredEmployeeList(List<Employee> filteredEmployeeList) {
		this.filteredEmployeeList = filteredEmployeeList;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}

	public List<Task> getFilterTaskList() {
		return filterTaskList;
	}

	public void setFilterTaskList(List<Task> filterTaskList) {
		this.filterTaskList = filterTaskList;
	}

	public JsonVehicleRoutingSolution getJsonVehicleRoutingSolution() {
		return jsonVehicleRoutingSolution;
	}


	public void setJsonVehicleRoutingSolution(JsonVehicleRoutingSolution jsonVehicleRoutingSolution) {
		this.jsonVehicleRoutingSolution = jsonVehicleRoutingSolution;
	}

	public List<Tour> getFilteredTourList() {
		return filteredTourList;
	}

	public void setFilteredTourList(List<Tour> filteredTourList) {
		this.filteredTourList = filteredTourList;
	}

	public List<Tour> getSelectedTourList() {
		return selectedTourList;
	}

	public void setSelectedTourList(List<Tour> selectedTourList) {
		this.selectedTourList = selectedTourList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public List<Tour> getCreatedTourList() {
		return createdTourList;
	}

	public void setCreatedTourList(List<Tour> createdTourList) {
		this.createdTourList = createdTourList;
	}

	public Map<String, String> getEstimatedWorkHoursMap() {
		return estimatedWorkHoursMap;
	}

	public void setEstimatedWorkHoursMap(Map<String, String> estimatedWorkHoursMap) {
		this.estimatedWorkHoursMap = estimatedWorkHoursMap;
	}

	public Map<String, String> getLocalAdminConf() {
		return localAdminConf;
	}

	public void setLocalAdminConf(Map<String, String> localAdminConf) {
		this.localAdminConf = localAdminConf;
	}

	public VehicleRoutingSolution getVehicleRoutingSolution() {
		return vehicleRoutingSolution;
	}

	public void setVehicleRoutingSolution(VehicleRoutingSolution vehicleRoutingSolution) {
		this.vehicleRoutingSolution = vehicleRoutingSolution;
	}

	public List<Facilities> getSourceFacilitiesList() {
		return sourceFacilitiesList;
	}

	public void setSourceFacilitiesList(List<Facilities> sourceFacilitiesList) {
		this.sourceFacilitiesList = sourceFacilitiesList;
	}
	
}
/*String previousEmployee = null;
if(previousEmployee == null){
previousEmployee = vr.getEmployeeName();
vr.setTourDate(tempDate);
}else if(previousEmployee.equals(vr.getEmployeeName())){
tempDate = returnNextDate(tempDate);
//tourStartDate = tempDate;
vr.setTourDate(tempDate);
}else{
previousEmployee = vr.getEmployeeName();
vr.setTourDate(tempDate);
}*/

//jsonVehicleRoutingSolutionList.add(optaplannerTourClusterService.retrieveSolution(commonError, selectedEmployeesList, vehicleRoutingSolution, travelTimeMap));
/*for(JsonVehicleRoute vr : jsonSolution.getVehicleRouteList()){
	for(JsonCustomer co : vr.getCustomerList()){
		for(Facilities fac : selectedFacilitiesList){
			if(fac.getName().equals(co.getLocationName()) && fac.getTaskAndWorkHoursList().size() == co.getTasksList().size()){
				jsonVehicleRoutingSolutionList.add(jsonSolution);
				logger.info("selected Facilities: "+ co.getLocationName() + " " + fac.getName());
				selectedFacilitiesList.remove((Facilities)fac);
				selectedFacilitiesList.stream().forEach(s -> System.out.print(s.getName()));
			}
		}
	}

}*/
/*IntStream.rangeClosed(1,5).boxed().forEach(val -> {
vehicleRoutingSolution = new VehicleRoutingSolution();
List<Facilities> selectedDayFacilityList= new ArrayList<>();
Set<Facilities> selectedWoDayFacilitySet = new HashSet<>();
selectedFacilitiesList.stream()
	.forEach( vDay -> {
		List<VisitingDayTimeHours> visitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHours(commonError, vDay.getIdentifier());
		if(!visitingDayTimeHoursList.isEmpty()){
			if(checkDayOfVisit(visitingDayTimeHoursList, val))
					selectedDayFacilityList.add(vDay);
		}else{
			selectedWoDayFacilitySet.add(vDay);
		}
	});
dayOfTour = String.valueOf(val);
logger.info("Day "+selectedDayFacilityList.size());
logger.info("WO DAY "+ selectedWoDayFacilitySet.size());
										
vehicleRoutingSolution = optaplannerTourClusterService.generateClusterTours(commonError, selectedEmployeesList,
										selectedDayFacilityList, tourCluster);


solveSolution();
if(val == 5 && !selectedWoDayFacilitySet.isEmpty()){
	dayOfTour = new String();
	vehicleRoutingSolution = optaplannerTourClusterService.generateClusterTours(commonError, selectedEmployeesList,
								new ArrayList<>(selectedWoDayFacilitySet), tourCluster);
	solveSolution();
}
});*/
/*for(Future<?> future : futures){
allDone &= future.isDone(); // check if future is done
logger.info("All threads are done");
}*/