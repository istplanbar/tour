package com.orb.tour.controller;
import javax.faces.bean.ManagedProperty;

/**
 * @author IST planbar / NaKa
 * 
 */
import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.employee.service.EmployeeService;
import com.orb.facilities.service.FacilitiesService;
import com.orb.optaplanner.rest.domain.service.OptaplannerTSPService;
import com.orb.optaplanner.rest.domain.service.OptaplannerTourClusterService;
import com.orb.taskplanner.service.TaskPlannerService;
import com.orb.tour.service.TourService;

public abstract class AbstractTourController extends AbstractCommonController {
	@Autowired
	protected TourService tourService;
	
	@Autowired
	protected FacilitiesService facilitiesService;
	
	@Autowired
	protected EmployeeService employeeService;
	
	@Autowired
	protected TaskPlannerService taskPlannerService;
	
	@Autowired
	protected OptaplannerTourClusterService optaplannerTourClusterService;

}
