package com.orb.tour.controller;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Employee;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.bean.TourCluster;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="tourClusterTaskCreationView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/tour")
public class TourClusteringTaskCreationManagedBean extends AbstractTourController{
	private static Log logger = LogFactory.getLog(TourClusteringTaskCreationManagedBean.class);
	
	private List<Employee> employeesList = null;
	private List<Employee> filteredEmployeeList = null;
	private List<Facilities> facilitiesList = null;
	private List<KorfAddress> korfmannLocationList = null;
	private Map<String, String> monthMap = null;
	private List<String> yearList = null;
	private DualListModel<Facilities> facilityPickList = null;
	private DualListModel<Facilities> taskFacilityPickList  = null;	
	private List<Facilities> filteredFacilitiesList = null;
	private TourCluster tourCluster = null;
	private List<Facilities> sourceFacilitiesList = null;
	private List<Facilities> targetFacilitiesList = null;
	private List<Facilities> sourceTaskFacilitiesList = null;
	private List<Facilities> targetTaskFacilitiesList = null;
	private List<Task> tasksList =  null;
	private List<Task> filterTaskList =  null;
	private JsonVehicleRoutingSolution jsonVehicleRoutingSolution = null;
	private List<Tour> createdTourList = null;
	private List<Tour> filteredTourList = null;
	private List<Tour> selectedTourList = null;
	private List<Boolean> togglerList = null;
	private Map<String, String> estimatedWorkHoursMap = null;
	private User loginUser = null;
	private List<Project> projectList = null;
	private Map<String, String>  localConf = null;
	private Map<Integer, Task>  hmapFacilities = null;
	
	/**
	 * This method redirects to the page where new tasks can be created.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadCreateTask")
	public String loadCreateTask(HttpServletRequest request,ModelMap model){
		return TourConstants.TOUR_CLUSTER_CREATE_TASK_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		selectedTourList = new ArrayList<>();
		jsonVehicleRoutingSolution = new JsonVehicleRoutingSolution();
		tasksList = new ArrayList<>();
		employeesList = new ArrayList<>();
		korfmannLocationList = new ArrayList<>();
		filteredEmployeeList = new ArrayList<>();
		facilityPickList =  new DualListModel<>();
		taskFacilityPickList =  new DualListModel<>();
		
		tourCluster = new TourCluster();
		yearList = new ArrayList<>();
		monthMap = new HashMap<>();
		tourCluster.setWorkingHours("8");
		tourCluster.setBreakAtWork("45");
		filteredFacilitiesList =  new ArrayList<>();
		sourceFacilitiesList = new ArrayList<>();
		targetFacilitiesList = new ArrayList<>();
		sourceTaskFacilitiesList = new ArrayList<>();
		targetTaskFacilitiesList = new ArrayList<>();
		createdTourList = new ArrayList<>();
		projectList = taskPlannerService.getActiveProjectList(commonError);
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		localConf = (Map<String, String>)getSessionObj(CommonConstants.ADMIN_CONF);
		estimatedWorkHoursMap = FacilitiesConstants.ESTIMATED_WORK_HOURS;
		togglerList = (List<Boolean>)getSessionObj(TourConstants.TOUR_TOGGLER_LIST);
		if(togglerList==null){
			togglerList = Arrays.asList(true, true, true, true,true);
		}
		monthMap = FacilitiesConstants.MAINTENACE_CYCLE_MONTH_NAME;
		
		facilitiesList = facilitiesService.getActiveFacilitiesList(commonError);
		korfmannLocationList = tourService.getActiveKorfAddressList(commonError);
		employeesList = employeeService.getActiveEmployeeList(commonError);
		tasksList = taskPlannerService.getActiveTaskList(commonError);
		
		if(!tasksList.isEmpty()){
			tasksList = tasksList.stream().filter(t -> t.getStatus() != null && 
					!t.getStatus().equals(TaskPlannerConstants.TOUR_TASKS_STATUS)).
					collect(Collectors.toList());
		}
		sourceTaskFacilitiesList = facilitiesList;
		facilityPickList = new DualListModel<>( sourceFacilitiesList, targetFacilitiesList );
		taskFacilityPickList = new DualListModel<>( sourceTaskFacilitiesList, targetTaskFacilitiesList );

		int yearNo =  Year.now().getValue();
		List<Integer> range = IntStream.rangeClosed(yearNo , 2030)
				.boxed().collect(Collectors.toList());
		for (Integer yearVal : range) { 
			yearList.add(String.valueOf(yearVal)); 
		}
	}

	/**
	 * This method creates tour clustering based on filtered data
	 */
	public void generateTaskClusters(ActionEvent event){
		hmapFacilities = new HashMap<>();
		int i = 1;
		String nextTaskId = "";
		String parts[];
		String part1 = "", part2;
		int incrementedId = 1;
		switch(tourCluster.getPlanningHorizon()){
		case "1":
			Date date = new GregorianCalendar(Integer.parseInt(tourCluster.getYearOfMaintenance()), Integer.parseInt(tourCluster.getMonthOfMaintenance())-1, 28).getTime();
			tourCluster.setDueDate(date);
			break;

		case "2":
			Date quaerterDate = new GregorianCalendar(Integer.parseInt(tourCluster.getYearOfMaintenance()), (Integer.parseInt(tourCluster.getQuarterlyCycle())*3)-1, 28).getTime();
			tourCluster.setDueDate(quaerterDate);
			break;

		case "3":
			Date yearDate = new GregorianCalendar(Integer.parseInt(tourCluster.getYearOfMaintenance()), 12-1, 28).getTime();
			tourCluster.setDueDate(yearDate);
			break;
		}
		for(Facilities fac : taskFacilityPickList.getTarget()){
			
			Task taskFacility = new Task();
			if(i==1) {
			nextTaskId = taskPlannerService.genNextTaskId( commonError, tourCluster.getProject() );
			taskFacility.setIdentifier(nextTaskId);
			parts = nextTaskId.split("-");
			part1 = parts[0]; 
			part2 = parts[1]; 
			incrementedId = Integer.valueOf(part2);
			} else {
			incrementedId = incrementedId + 1;	
			nextTaskId = part1 + "-" + incrementedId;
			taskFacility.setIdentifier(nextTaskId);
			}
			taskFacility.setName(fac.getName() + "-" +getJSFMessage("lbl_tasks"));
			taskFacility.setProject(tourCluster.getProject());
			taskFacility.setStatus("notYet");
			taskFacility.setFacilities(fac.getId());
			taskFacility.setCategory("1");
			taskFacility.setAttachment("");
			taskFacility.setDueDate(tourCluster.getDueDate());
			taskFacility.setEstimatedTime(tourCluster.getEstimatedTime());
			taskFacility.setAssignTo(loginUser.getUserId());
			taskFacility.setReporter(loginUser.getUserId());
			taskFacility.setPriority(TaskPlannerConstants.PRIORITY);
			taskFacility.setDeputy(loginUser.getUserId());
			hmapFacilities.put(i, taskFacility);
			i++;
		}
		//taskPlannerService.addTask( commonError, loginUser, taskFacility );
		taskPlannerService.addTaskFacilities( commonError, loginUser, hmapFacilities );
		
		
		customRedirector( TaskPlannerConstants.TASK_ADDED_SUCCESS,"/common/process/tour/loadCreateTask.html");	
	}

	public List<Employee> getEmployeesList() {
		return employeesList;
	}

	public void setEmployeesList(List<Employee> employeesList) {
		this.employeesList = employeesList;
	}

	public List<Employee> getFilteredEmployeeList() {
		return filteredEmployeeList;
	}

	public void setFilteredEmployeeList(List<Employee> filteredEmployeeList) {
		this.filteredEmployeeList = filteredEmployeeList;
	}

	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}

	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}

	public List<KorfAddress> getKorfmannLocationList() {
		return korfmannLocationList;
	}

	public void setKorfmannLocationList(List<KorfAddress> korfmannLocationList) {
		this.korfmannLocationList = korfmannLocationList;
	}

	public Map<String, String> getMonthMap() {
		return monthMap;
	}

	public void setMonthMap(Map<String, String> monthMap) {
		this.monthMap = monthMap;
	}

	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	public DualListModel<Facilities> getFacilityPickList() {
		return facilityPickList;
	}

	public void setFacilityPickList(DualListModel<Facilities> facilityPickList) {
		this.facilityPickList = facilityPickList;
	}

	public DualListModel<Facilities> getTaskFacilityPickList() {
		return taskFacilityPickList;
	}

	public void setTaskFacilityPickList(DualListModel<Facilities> taskFacilityPickList) {
		this.taskFacilityPickList = taskFacilityPickList;
	}

	public List<Facilities> getFilteredFacilitiesList() {
		return filteredFacilitiesList;
	}

	public void setFilteredFacilitiesList(List<Facilities> filteredFacilitiesList) {
		this.filteredFacilitiesList = filteredFacilitiesList;
	}

	public TourCluster getTourCluster() {
		return tourCluster;
	}

	public void setTourCluster(TourCluster tourCluster) {
		this.tourCluster = tourCluster;
	}

	public List<Facilities> getSourceFacilitiesList() {
		return sourceFacilitiesList;
	}

	public void setSourceFacilitiesList(List<Facilities> sourceFacilitiesList) {
		this.sourceFacilitiesList = sourceFacilitiesList;
	}

	public List<Facilities> getTargetFacilitiesList() {
		return targetFacilitiesList;
	}

	public void setTargetFacilitiesList(List<Facilities> targetFacilitiesList) {
		this.targetFacilitiesList = targetFacilitiesList;
	}

	public List<Facilities> getSourceTaskFacilitiesList() {
		return sourceTaskFacilitiesList;
	}

	public void setSourceTaskFacilitiesList(List<Facilities> sourceTaskFacilitiesList) {
		this.sourceTaskFacilitiesList = sourceTaskFacilitiesList;
	}

	public List<Facilities> getTargetTaskFacilitiesList() {
		return targetTaskFacilitiesList;
	}

	public void setTargetTaskFacilitiesList(List<Facilities> targetTaskFacilitiesList) {
		this.targetTaskFacilitiesList = targetTaskFacilitiesList;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}

	public List<Task> getFilterTaskList() {
		return filterTaskList;
	}

	public void setFilterTaskList(List<Task> filterTaskList) {
		this.filterTaskList = filterTaskList;
	}

	public JsonVehicleRoutingSolution getJsonVehicleRoutingSolution() {
		return jsonVehicleRoutingSolution;
	}

	public void setJsonVehicleRoutingSolution(JsonVehicleRoutingSolution jsonVehicleRoutingSolution) {
		this.jsonVehicleRoutingSolution = jsonVehicleRoutingSolution;
	}

	public List<Tour> getCreatedTourList() {
		return createdTourList;
	}

	public void setCreatedTourList(List<Tour> createdTourList) {
		this.createdTourList = createdTourList;
	}

	public List<Tour> getFilteredTourList() {
		return filteredTourList;
	}

	public void setFilteredTourList(List<Tour> filteredTourList) {
		this.filteredTourList = filteredTourList;
	}

	public List<Tour> getSelectedTourList() {
		return selectedTourList;
	}

	public void setSelectedTourList(List<Tour> selectedTourList) {
		this.selectedTourList = selectedTourList;
	}

	public List<Boolean> getTogglerList() {
		return togglerList;
	}

	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}

	public Map<String, String> getEstimatedWorkHoursMap() {
		return estimatedWorkHoursMap;
	}

	public void setEstimatedWorkHoursMap(Map<String, String> estimatedWorkHoursMap) {
		this.estimatedWorkHoursMap = estimatedWorkHoursMap;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public Map<String, String> getLocalConf() {
		return localConf;
	}

	public void setLocalConf(Map<String, String> localConf) {
		this.localConf = localConf;
	}
	
}
