package com.orb.tour.controller;
/**
 * @author IST planbar / KaSe
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.modena.domain.Theme;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.roleadministration.model.Role;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.facilities.bean.Facilities;
import com.orb.taskplanner.bean.Task;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="tourAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/tour")
public class TourAddUpdateManagedBean extends AbstractTourController{
	private static Log logger = LogFactory.getLog(TourAddUpdateManagedBean.class);
	
	private Tour tour =null;
	private List<Tour> tourList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String tourId = null;
	private User loginUser = null;
	private List<Facilities> facilitiesList = null;
	private List<Facilities> filteredFacilitiesList = null;
	private List<Task> tasksList =  null;
	private List<String> taskIdList = null;
	private List<Task> filterTaskList =  null;
	private List<Employee> employeeList = null;
	private DualListModel<Facilities> facilityPickList = null;
	private List<Facilities> sourceFacilities = null;
	private List<Facilities> targetFacilities = null;
	private List<String> selectedFacilitiesIds = null;
	private List<KorfAddress> korfmannLocationList = null;
	private Employee employee;
	/**
	 * This method will be called when user clicks Add button / Tour Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateTour")
	public String manipulateTour( HttpServletRequest request, ModelMap model){
		tourId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( TourConstants.SELECTED_TOUR_ID, tourId );
		return TourConstants.TOUR_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		tasksList = new ArrayList<>();
		employeeList = new ArrayList<>();
		employee = new Employee(); 
		selectedFacilitiesIds = new ArrayList<>();
		facilitiesList = new ArrayList<>();
		filteredFacilitiesList = new ArrayList<>();
		sourceFacilities = new ArrayList<>();
		targetFacilities = new ArrayList<>();
		facilityPickList =  new DualListModel<>();
		korfmannLocationList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		tourId = getJSFRequestAttribute( TourConstants.SELECTED_TOUR_ID );
		logger.info(" Tour Id :" +tourId);
		tourList = tourService.getActiveTourList(commonError);
		facilitiesList = facilitiesService.getActiveFacilitiesList(commonError);
		korfmannLocationList = tourService.getActiveKorfAddressList(commonError);
		tasksList = taskPlannerService.getActiveTaskList(commonError);
		employeeList= employeeService.getActiveEmployeeList(commonError);
		//facilityPickList = new DualListModel<>( sourceFacilities, targetFacilities );
		if(CommonUtils.getValidString(tourId).isEmpty()){
			tour = new Tour();
			sourceFacilities = filteredFacilitiesList;
			targetFacilities = new ArrayList<>();
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			tour = tourService.getTour(commonError, tourId);
			generatePickList();
			getTargetFaciltiesList();
			getSourceFaciltiesList();
		 }
		facilityPickList = new DualListModel<>( sourceFacilities, targetFacilities );
	}
	
	private void generatePickList(){
		logger.info(tour.getEmployee());
		Set<Facilities> facilitySet = new HashSet<>();
		List<String> qualificationIdList = new ArrayList<>();
		employee = employeeService.getEmployee(commonError, tour.getEmployee());
		if(employee.getQualificationsMaId() != null){
			qualificationIdList = new ArrayList(Arrays.asList(employee.getQualificationsMaId().split(",")));
			if(!qualificationIdList.isEmpty()){
				facilitySet = qualificationIdList.stream().flatMap(q -> facilitiesList.stream()
											.filter(f -> f.getQualificationsId() != null && Arrays.asList(f.getQualificationsId().split(",")).contains(q)))
											.collect(Collectors.toSet());
			}
		}	
		filteredFacilitiesList = new ArrayList<>(facilitySet);
		getTargetFaciltiesList();
		getSourceFaciltiesList();
		facilityPickList = new DualListModel<>( sourceFacilities, targetFacilities );
	}

	/**
	 * Returns the Facilities which requires the qualifications matching this requirement
	 */
	public void getFilteredFaciltiesList(AjaxBehaviorEvent event){
		generatePickList();
	}
	
	/**
	 * This method returns the facilities which is mapped for the tour
	 */
	private void getTargetFaciltiesList(){
		logger.info("Get Target Facilities for already saved tour");
		if(!facilitiesList.isEmpty()){
			selectedFacilitiesIds = tourService.getTourFacilitiesList(commonError, tourId);
			targetFacilities = filteredFacilitiesList.stream()
						   					   .filter(e -> selectedFacilitiesIds.contains(e.getIdentifier()))
						   					   .collect(Collectors.toList());
		}
	}
	
	/**
	 * This method returns the facilities which is mapped for the tour
	 */
	private void getSourceFaciltiesList(){
		logger.info("Get Source Facilities for already saved tour");
		if(!facilitiesList.isEmpty()){
			selectedFacilitiesIds = tourService.getTourFacilitiesList(commonError, tourId);
			sourceFacilities = filteredFacilitiesList.stream()
						   					   .filter(e -> !selectedFacilitiesIds.contains(e.getIdentifier()))
						   					   .collect(Collectors.toList());
		}
	}
	
	
	
	/**
	 * This method will add / update a Tour in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		List<String> resultFacilitiesIds = new ArrayList<>();
		System.out.println(facilityPickList.getTarget().size());
		if(targetFacilities != null){
			resultFacilitiesIds = facilityPickList.getTarget().stream().map(Facilities::getIdentifier).collect(Collectors.toList());
		} 
			
		if(CommonUtils.getValidString(tour.getId()).isEmpty()){
			tourService.add( commonError, loginUser, tour, resultFacilitiesIds);
		}else{
			tourService.update( commonError, loginUser, tour, resultFacilitiesIds );
		}
		tourList = tourService.getActiveTourList(commonError);
		String msg=CommonUtils.getValidString(tour.getId()).isEmpty() ? TourConstants.TOUR_ADDED_SUCCESS : TourConstants.TOUR_UPDATED_SUCCESS ;
		String page = TourConstants.TOUR_LIST_PAGE;
		if(!CommonUtils.getValidString(tour.getId()).isEmpty()) {
			page = TourConstants.TOUR_VIEW_PAGE+"?objectId="+tourId; 
		}
		customRedirector( msg, page);		
	}

	/**
	 * This method to returns to disposals view page
	 * @return 
	 */
	public void cancelAction(){
		if(tour.getIdentifier()==null){
			redirector(TourConstants.TOUR_LIST_PAGE);
		}else{
			redirector(TourConstants.TOUR_VIEW_PAGE+"?objectId="+tour.getIdentifier());
		}
	}
	
	/**
	 * This method is used to filter task name 
	 * @param id
	 * @return
	 */	
	public void filterTask(String id){
		filterTaskList = new ArrayList<>();
		if(tasksList!=null){
			for(Task task:tasksList){
			   if(id.equals(task.getFacilities())){
				   filterTaskList.add(task);
			    }
			}
			//String x = tasksList.stream().filter(t->t.getFacilities().equals(id)).map(Task::getName).findAny().orElse(null);
		}
	}
	
	public Tour getTour() {
		return tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}

	public List<Tour> getTourList() {
		return tourList;
	}

	public void setTourList(List<Tour> tourList) {
		this.tourList = tourList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public String getTourId() {
		return tourId;
	}

	public void setTourId(String tourId) {
		this.tourId = tourId;
	}

	public List<Task> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}

	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}

	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}

	public List<String> getTaskIdList() {
		return taskIdList;
	}

	public void setTaskIdList(List<String> taskIdList) {
		this.taskIdList = taskIdList;
	}

	public List<Task> getFilterTaskList() {
		return filterTaskList;
	}

	public void setFilterTaskList(List<Task> filterTaskList) {
		this.filterTaskList = filterTaskList;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public DualListModel<Facilities> getFacilityPickList() {
		return facilityPickList;
	}

	public void setFacilityPickList(DualListModel<Facilities> facilityPickList) {
		this.facilityPickList = facilityPickList;
	}

	public List<KorfAddress> getKorfmannLocationList() {
		return korfmannLocationList;
	}

	public void setKorfmannLocationList(List<KorfAddress> korfmannLocationList) {
		this.korfmannLocationList = korfmannLocationList;
	}
	
	
}
