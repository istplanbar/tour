package com.orb.tour.controller;
/**
 * @author IST planbar / NaKa
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.qualifications.bean.Qualifications;
import com.orb.taskplanner.bean.Task;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="tourView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/tour")
public class TourListViewManagedBean extends AbstractTourController{
	private static Log logger = LogFactory.getLog(TourListViewManagedBean.class);
	
	private User loginUser=null;
	private List<Boolean> togglerList = null;
	private Tour tour = null;
	private List<Tour> tourList = null;
	private List<Tour> filteredTourList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Tour> selectedTourList = null;
	private String tourId = null;
	private String showIdentifier = CommonConstants.TRUE;
	private List<Employee> employeeList = null;
	private List<Facilities> facilitiesList = null;
	private List<Facilities> selectedFacilitysList = null;
	private List<MaintenanceAct> maintenanceActList = null;
	private List<KorfAddress> korfmannLocationList = null;
	private List<Task> tasksList =  null;
	private List<Task> filterTaskList =  null;
	private Map<String, String> estimatedWorkHoursList = null;
	
	 /**
	 * This page will be called when user clicks the Tour link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadTour")
    public String loadTour(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return TourConstants.TOUR_LIST_HTML;
    }
	
	/**
	 * Showing Route Map To Particular Tour
	 * @param request
	 * @return
	 */
	@RequestMapping("/loadGoogleMaps")
	public String loadGoogleMaps(HttpServletRequest request, ModelMap model){
		tourId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	    model.addAttribute( TourConstants.SELECTED_TOUR_ID, tourId );
		return TourConstants.TOUR_MAP_PAGE_URL;
	}
	
	/**
	 * This method will be called when user clicks particular Tour from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewTour")
	public String viewTour(HttpServletRequest request, ModelMap model){
		 tourId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( TourConstants.SELECTED_TOUR_ID, tourId );
		 return TourConstants.TOUR_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		estimatedWorkHoursList = (Map<String,String>)FacilitiesConstants.ESTIMATED_WORK_HOURS;
		tasksList = new ArrayList<>();
		employeeList = new ArrayList<>();
		tourList = new ArrayList<>();
		maintenanceActList = new ArrayList<>();
		korfmannLocationList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		tourId = CommonUtils.getValidString(getJSFRequestAttribute(TourConstants.SELECTED_TOUR_ID) );
		logger.info(" Tour Id :" +tourId);
		facilitiesList =facilitiesService.getActiveFacilitiesList(commonError);
		korfmannLocationList = tourService.getActiveKorfAddressList(commonError);
		employeeList= employeeService.getActiveEmployeeList(commonError);
		tasksList = taskPlannerService.getActiveTaskList(commonError);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			tourList=tourService.getInactiveTourList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			tourList=tourService.getActiveTourList(commonError);
			for(Tour t : tourList){
				List<String> filteredFaciltiyList = new ArrayList<>();
				List<String> selectedFacilitiesIds = tourService.getTourFacilitiesList(commonError, t.getIdentifier());
				if(!selectedFacilitiesIds.isEmpty()){
					filteredFaciltiyList = facilitiesList.stream().filter(fa -> selectedFacilitiesIds.contains(fa.getIdentifier())).map(Facilities::getName).collect(Collectors.toList());
					t.setFacilities(String.join(", ", filteredFaciltiyList));
				}
			}
		}
		togglerList = (List<Boolean>)getSessionObj(TourConstants.TOUR_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true, true, true, true);
		}
		if(!CommonUtils.getValidString(tourId).isEmpty()){
			tour = tourService.getTour(commonError, tourId);
			List<String> selectedFacilitiesIds = tourService.getTourFacilitiesList(commonError, tourId);
			
			selectedFacilitysList = new ArrayList<>();
			if(!selectedFacilitiesIds.isEmpty()){
				selectedFacilitysList = facilitiesList.stream().filter(fa -> selectedFacilitiesIds.contains(fa.getIdentifier())).collect(Collectors.toList());
			}
			for(Facilities f : selectedFacilitysList){
				List<String> filteredMaintenanceIds = new ArrayList<>();
				filteredMaintenanceIds = tasksList.stream().filter(t -> t.getFacilities() != null && t.getFacilities().equals(f.getId()))
														   .map(t -> t.getIdentifier()+returnWorkHour(t.getEstimatedTime())).collect((Collectors.toList()));
														   //.map(t -> t.getIdentifier() + t.getEstimatedTime() != null ? " ("+returnWorkHour(t.getEstimatedTime())+")" : "").collect((Collectors.toList()));
				f.setMaintenanceActId(String.join(", ", filteredMaintenanceIds));
				
			}
			
		}
		
	}
	
	
	private String returnWorkHour(String value){
		String workHour = "";
		for (String key : estimatedWorkHoursList.keySet()) {
	        if (estimatedWorkHoursList.get(key).equals(value)) {
	        	workHour = " ("+key+")";
	        	break;
	        }
	    }
		return workHour;
	}
	
	/**
	 * Return Employee Name for mapped tour
	 */
	public String returnKorfmannLocationName(String id){
		String locationName = null;
		if(!korfmannLocationList.isEmpty()){
			locationName = korfmannLocationList.stream().filter(i -> i.getIdentifier().equals(id)).findFirst().get().getName();
		}
		return locationName;
	}
	
	/**
	 * Return Employee Name for mapped tour
	 */
	public String returnEmployeeName(String id){
		String empName = null;
		if(!employeeList.isEmpty()){
			empName = employeeList.stream().filter(i -> i.getIdentifier().equals(id)).findFirst().get().getFirstName();
		}
		return getUserFullName(CommonUtils.getValidString(empName));
	}
	
	/**
	 * This method will get all the active Tour
	 * @param event
	 */
	public void showActive(ActionEvent event){
		tourList = tourService.getActiveTourList( commonError );
		if( filteredTourList != null ) filteredTourList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Tour
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		tourList = tourService.getInactiveTourList(commonError);
		if( filteredTourList != null ) filteredTourList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks Tour
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		tourService.lockTour( commonError, loginUser, selectedTourList );
		showActive(event);
		customRedirector( TourConstants.TOUR_LOCKED_SUCCESS,TourConstants.TOUR_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Tour
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		tourService.unlockTour( commonError, loginUser, selectedTourList );
		showInactive(event);
		customRedirector( TourConstants.TOUR_UNLOCKED_SUCCESS, TourConstants.TOUR_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the Tour
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(selectedTourList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(TourConstants.TOUR_DELETE_SUCCESS);
		}else{	
			tourService.deleteTour( commonError, loginUser, selectedTourList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( TourConstants.TOUR_DELETE_SUCCESS, TourConstants.TOUR_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will navigate to the page where user can add a Tour
	 */
	public void addAction(){
		addSessionObj( TourConstants.TOUR_TOGGLER_LIST , togglerList );
		redirector(TourConstants.TOUR_EDIT_PAGE);
	}
	/**
	 * This method to returns to main Tour list page
	 */
	public void cancelAction(){
		redirector(TourConstants.TOUR_LIST_PAGE +"?activeFlag="+tour.getActive());
	}
	
	/**
	 * This method will navigate to Tour Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(TourConstants.TOUR_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to Tour Edit page
	 * @param index
	 */
	public void showMapTourAction(String index){
		redirector(TourConstants.TOUR_MAP_PAGE+"?objectId="+index);
	}
	
	
	/**
	 * This method will navigate to Tour View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( TourConstants.TOUR_TOGGLER_LIST , togglerList );
		redirector(TourConstants.TOUR_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed Tour
	 * @param index
	 */
	public void deleteViewedTour(String index){
		tour = tourService.getTour(commonError, index);
		tourService.deleteViewedTour( commonError, loginUser, tour);
		if( commonError.getError().isEmpty() ){
			customRedirector( TourConstants.TOUR_DELETE_SUCCESS, TourConstants.TOUR_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed Tour
	 */
	public void lockViewedTour(String index){
		tour = tourService.getTour(commonError, index);
		tourService.lockViewedTour( commonError, loginUser, tour );
		customRedirector( TourConstants.TOUR_LOCKED_SUCCESS, TourConstants.TOUR_LIST_PAGE);
	}

	/**
	 * This method locks viewed Tour
	 * @param index
	 * @return
	 */
	public void unlockViewedTour(String index){
		tour = tourService.getTour(commonError, index);
		tourService.unlockViewedTour( commonError, loginUser, tour );
		if( commonError.getError().isEmpty() ){
			customRedirector( TourConstants.TOUR_UNLOCKED_SUCCESS, TourConstants.TOUR_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( TourConstants.SELECTED_TOUR_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( TourConstants.SELECTED_TOUR_ID );
		if( toggleList != null ) model.remove( TourConstants.TOUR_TOGGLER_LIST );		
    }
	
	
	
	/**
	 * This method is used to get facilities name
	 * @param index
	 * @return
	 */	
    public String  getFacilitiesName(String index){
		String facilitiesName = null ;
		facilitiesName = facilitiesList.stream().filter(f->f.getIdentifier().equals(index)).map(Facilities::getName).findAny().orElse(null);
		return facilitiesName;
	}
    
    /**
	 * This method is used to get task name
	 * @param index
	 * @return
	 */	
    /*public String getTaskName(String taskId){
    	StringBuffer stringBuffer = new StringBuffer();
    	List<String> taskIds = new ArrayList<String>(Arrays.asList(taskId.split(",")));
    	for(String s:taskIds){
    	 for(Task task:tasksList){
    		if(task.getId().equals(s)){
    			stringBuffer.append(task.getName());
    			stringBuffer.append(",");
    		}
    	 }
    	}
    	//logger.info(stringBuffer);
    	if(!stringBuffer.toString().equals("")){
    		stringBuffer.setLength(stringBuffer.length()-1);
    	}
		return stringBuffer.toString();
    	
    	
    }*/
    
	/**
	 * This method will navigate to Facilities View page
	 * @param index
	 */
	
	public void viewFacilityAction(String index){
		redirector(FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+index);
	}
    public void createPDF(String tourId){
    	tourService.createPDF(commonError, tourId);
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public Tour getTour() {
		return tour;
	}


	public void setTour(Tour tour) {
		this.tour = tour;
	}


	public List<Tour> getTourList() {
		return tourList;
	}


	public void setTourList(List<Tour> tourList) {
		this.tourList = tourList;
	}


	public List<Tour> getFilteredTourList() {
		return filteredTourList;
	}


	public void setFilteredTourList(List<Tour> filteredTourList) {
		this.filteredTourList = filteredTourList;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<Tour> getSelectedTourList() {
		return selectedTourList;
	}


	public void setSelectedTourList(List<Tour> selectedTourList) {
		this.selectedTourList = selectedTourList;
	}


	public String getTourId() {
		return tourId;
	}


	public void setTourId(String tourId) {
		this.tourId = tourId;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}


	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}

	public List<Facilities> getSelectedFacilitysList() {
		return selectedFacilitysList;
	}

	public void setSelectedFacilitysList(List<Facilities> selectedFacilitysList) {
		this.selectedFacilitysList = selectedFacilitysList;
	}

	public List<KorfAddress> getKorfmannLocationList() {
		return korfmannLocationList;
	}

	public void setKorfmannLocationList(List<KorfAddress> korfmannLocationList) {
		this.korfmannLocationList = korfmannLocationList;
	}


	public List<Task> getTasksList() {
		return tasksList;
	}

	public List<Task> getFilterTaskList() {
		return filterTaskList;
	}

	public void setFilterTaskList(List<Task> filterTaskList) {
		this.filterTaskList = filterTaskList;
	}

	public void setTasksList(List<Task> tasksList) {
		this.tasksList = tasksList;
	}


}
