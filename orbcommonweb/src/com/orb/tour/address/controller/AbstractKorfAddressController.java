package com.orb.tour.address.controller;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.utils.CommonUtils;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.service.TourService;

public class AbstractKorfAddressController extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(AbstractKorfAddressController.class);

	
	@Autowired
	protected TourService tourService;
	
	public KorfAddress convertAddressToLatLng(KorfAddress korfAddress){
		try{
			 StringBuffer address = new StringBuffer(korfAddress.getStreet());
			 String key = "&key="+getJSFMessage("google_geocode_api_key");
			 
			 if(!CommonUtils.getValidString(korfAddress.getHouseNumber()).isEmpty()){
				 address.append(" "+korfAddress.getHouseNumber());
			 }
			 if(!CommonUtils.getValidString(korfAddress.getPlace()).isEmpty()){
				 address.append(";"+korfAddress.getPlace());
			 }
			 if(!CommonUtils.getValidString(korfAddress.getZipCode()).isEmpty()){
				 address.append(";"+korfAddress.getZipCode());
			 }
			 /*if(!CommonUtils.getValidString(korfAddress.getState()).isEmpty()){
				 address.append(";"+korfAddress.getState());
			 }*/
			 //System.out.println(getJSFMessage("google_map_api_key"));
			 //String addr = "Sonnenweg 6;99425;";
			 String s = "https://maps.google.com/maps/api/geocode/json?" + "sensor=false&language=de&&components=country:DE&address=";
			 s += URLEncoder.encode(address.toString(), "UTF-8");
			 s += key;
			 URL url = new URL(s);
			
			 // read from the URL
			 Scanner scan = new Scanner(url.openStream());
			 String str = new String();
			 while (scan.hasNext()){
			     str += scan.nextLine();
			 }
			 scan.close();
			
			 // build a JSON object
			 JSONObject obj = new JSONObject(str);
			 if (! obj.getString("status").equals("OK")){
				 logger.info("Error on location to latlng Conversion"+obj.toString());
				 commonError.addError(getJSFMessage("error_on_address_conversion"));
			    }
			
			 // get the first result
			 JSONObject res = obj.getJSONArray("results").getJSONObject(0);
			 //System.out.println(res.getString("formatted_address"));
			 JSONObject loc = res.getJSONObject("geometry").getJSONObject("location");
			 //System.out.println("lat: " + loc.getDouble("lat") + ", lng: " + loc.getDouble("lng"));
			 korfAddress.setFacilityLatitude(loc.getDouble("lat"));
			 korfAddress.setFacilityLongitude(loc.getDouble("lng"));
			 
		 }catch(Exception e){
			 logger.info("Error on converting address to Lat and Long."+e.getMessage());
			 commonError.addError(getJSFMessage("error_on_address_conversion"));
		 }
		 
	return korfAddress;
	}
	
	
}
