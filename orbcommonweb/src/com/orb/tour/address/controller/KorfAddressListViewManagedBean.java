package com.orb.tour.address.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="korfAddressView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/tour")
public class KorfAddressListViewManagedBean extends AbstractKorfAddressController {
	private static Log logger = LogFactory.getLog(KorfAddressListViewManagedBean.class);
	
		private User loginUser=null;
		private List<Boolean> startPointTogglerList = null;
		private KorfAddress korfAddress =null;
		private List<KorfAddress> korfAddressList = null;
		private List<KorfAddress> filteredKorfAddressList = null;
		private String showActiveListBtn = CommonConstants.FALSE;
		private String showActivateBtn = CommonConstants.FALSE;
		private List<KorfAddress> selectedKorfAddressList = null;
		private String korfAddressId = null;
		private String showIdentifier = CommonConstants.TRUE;
		private List<MultiVal> countryList = null;
		private List<MultiVal> stateList = null;


	    /**
		 * This page will be called when user clicks the korfAddress link on the menu.
		 * @param request
		 * @param model
		 * @return
		 */
		@RequestMapping("/loadAddress")
	    public String loadAddress(HttpServletRequest request,ModelMap model){
			clearSessionObjects(model);
			String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
			model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
			return TourConstants.ADDRESS_LIST_HTML;
	    }
		
		
		/**
		 * This method will be called when user clicks particular KorfAddress from list page
		 * @param request
		 * @param model
		 * @return
		 */
		@RequestMapping("/viewAddress")
		public String viewAddress(HttpServletRequest request, ModelMap model){
			korfAddressId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		    model.addAttribute( TourConstants.SELECTED_ADDRESS_ID, korfAddressId );
			return TourConstants.ADDRESS_VIEW_HTML;
		}
		
		/**
		 * This method will be called when the XHTML loaded from Spring call.,
		 */
		@SuppressWarnings("unchecked")
		public void onLoadAction(){
			korfAddressList = new ArrayList<>();
			loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
			korfAddressId = CommonUtils.getValidString(getJSFRequestAttribute(TourConstants.SELECTED_ADDRESS_ID) );
			logger.info(" KorfAddress Id :" +korfAddressId);
			String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
			countryList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_COUNTRY ); 
			stateList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_STATE ); 
			if(CommonConstants.N.equals(showActive)){
				korfAddressList = tourService.getInactiveKorfAddressList(commonError);
				setShowActivateBtn(CommonConstants.TRUE);
				setShowActiveListBtn(CommonConstants.TRUE);
			}else{
				korfAddressList = tourService.getActiveKorfAddressList(commonError);
			}
			startPointTogglerList = (List<Boolean>)getSessionObj(TourConstants.ADDRESS_TOGGLER_LIST);
			if(startPointTogglerList==null){
				 startPointTogglerList = Arrays.asList(false, true, true, true, true, true);
			}
			if(!CommonUtils.getValidString(korfAddressId).isEmpty()){
				korfAddress = tourService.getKorfAddress(commonError, korfAddressId);
			}
			
		}
		
		

		/**
		 * This method will get all the active KorfAddress
		 * @param event
		 */
		public void showActive(ActionEvent event){
			korfAddressList = tourService.getActiveKorfAddressList( commonError );
			if( filteredKorfAddressList != null ) filteredKorfAddressList.clear();
			setShowActivateBtn(CommonConstants.FALSE);
			setShowActiveListBtn(CommonConstants.FALSE);
		}
		
		
		/**
		 * This method will show all the inactive KorfAddress
		 * @param event
		 */
		public void showInactive(ActionEvent event){
			korfAddressList = tourService.getInactiveKorfAddressList(commonError);
			if( filteredKorfAddressList != null ) filteredKorfAddressList.clear();
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}

		
		/**
		 * This method locks KorfAddress
		 * @param list page
		 * @return 
		 */
		public void lockAction(ActionEvent e){
			tourService.lockKorfAddress( commonError, loginUser, selectedKorfAddressList );
			showActive(e);
			customRedirector( TourConstants.ADDRESS_LOCKED_SUCCESS,TourConstants.ADDRESS_LIST_PAGE);
		}
		
		/**
		 * This method unlocks KorfAddress
		 * @param list page
		 * @return 
		 */
		public void unlockAction(ActionEvent e){
			tourService.unlockKorfAddress( commonError, loginUser, selectedKorfAddressList );
			showInactive(e);
			customRedirector( TourConstants.ADDRESS_UNLOCKED_SUCCESS, TourConstants.ADDRESS_LIST_PAGE);
		}
		
		
		/**
		 * This method will delete the KorfAddress
		 * @param event
		 */
		public void deleteAction(ActionEvent e){
			if(selectedKorfAddressList.isEmpty()){
				FacesContext context = FacesContext.getCurrentInstance();
				context.getExternalContext().getFlash().setKeepMessages(true);
				errorMessage(TourConstants.ADDRESS_DELETE_SUCCESS);
			}else{	
				tourService.deleteKorfAddress( commonError, loginUser, selectedKorfAddressList);
			    if( commonError.getError().isEmpty() ){
			        customRedirector( TourConstants.ADDRESS_DELETE_SUCCESS, TourConstants.ADDRESS_LIST_PAGE);
			    }
			    showInactive(e);
			    commonError.clearErrors();
		    }
		}
		
		/**
		 * This method will navigate to the page where user can add a korfAddress
		 */
		public void addAction(){
			addSessionObj( TourConstants.ADDRESS_TOGGLER_LIST , startPointTogglerList );
			redirector(TourConstants.ADDRESS_EDIT_PAGE);
		}
		/**
		 * This method to returns to main korfAddress list page
		 */
		public void cancelAction(){
			redirector(TourConstants.ADDRESS_LIST_PAGE +"?activeFlag="+korfAddress.getActive());
		}
		
		/**
		 * This method will navigate to korfAddress Edit page
		 * @param index
		 */
		public void editAction(String index){

			redirector(TourConstants.ADDRESS_EDIT_PAGE+"?objectId="+index);
		}
		
		
		/**
		 * This method will navigate to korfAddress View page
		 * @param index
		 */
		public void viewAction(String index){
		    addSessionObj( TourConstants.ADDRESS_TOGGLER_LIST , startPointTogglerList );
			redirector(TourConstants.ADDRESS_VIEW_PAGE+"?objectId="+index);
		}
		
		/**
		 * Delete viewed korfAddress
		 * @param index
		 */
		public void deleteViewedKorfAddress(String index){
			korfAddress = tourService.getKorfAddress(commonError, index);
			tourService.deleteViewedKorfAddress( commonError, loginUser, korfAddress);
			if( commonError.getError().isEmpty() ){
				customRedirector( TourConstants.ADDRESS_DELETE_SUCCESS, TourConstants.ADDRESS_LIST_PAGE );
			}
		}
		
		/**
		 * This method locks viewed korfAddress
		 */
		public void lockViewedKorfAddress(String index){
			korfAddress = tourService.getKorfAddress(commonError, index);
			tourService.lockViewedKorfAddress( commonError, loginUser, korfAddress );
			customRedirector( TourConstants.ADDRESS_LOCKED_SUCCESS, TourConstants.ADDRESS_LIST_PAGE);
		}

		/**
		 * This method locks viewed korfAddress
		 * @param index
		 * @return
		 */
		public void unlockViewedKorfAddress(String index){
			korfAddress = tourService.getKorfAddress(commonError, index);
			tourService.unlockViewedKorfAddress( commonError, loginUser, korfAddress );
			if( commonError.getError().isEmpty() ){
				customRedirector( TourConstants.ADDRESS_UNLOCKED_SUCCESS, TourConstants.ADDRESS_LIST_PAGE );
			}
		}

		/**
		 * This method will be called when user click the toggle to hide the column  
		 * @param event
		 * @return
		 */
		
		public void onToggle(ToggleEvent event){
	        startPointTogglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
	    }
		
		/**
		 * This method called to clear the session when application exit
		 * @param model
		 */
		private void clearSessionObjects(ModelMap model) {
			String id = (String)model.get( TourConstants.SELECTED_ADDRESS_ID );
			List toggleList = (List)model.get( startPointTogglerList );
			if( id != null ) model.remove( TourConstants.SELECTED_ADDRESS_ID );
			if( toggleList != null ) model.remove( TourConstants.ADDRESS_TOGGLER_LIST );		
	    }
		
		public User getLoginUser() {
			return loginUser;
		}


		public void setLoginUser(User loginUser) {
			this.loginUser = loginUser;
		}


		public List<Boolean> getStartPointTogglerList() {
			return startPointTogglerList;
		}


		public void setStartPointTogglerList(List<Boolean> startPointTogglerList) {
			this.startPointTogglerList = startPointTogglerList;
		}


		public KorfAddress getKorfAddress() {
			return korfAddress;
		}


		public void setKorfAddress(KorfAddress korfAddress) {
			this.korfAddress = korfAddress;
		}


		public List<KorfAddress> getKorfAddressList() {
			return korfAddressList;
		}


		public void setKorfAddressList(List<KorfAddress> korfAddressList) {
			this.korfAddressList = korfAddressList;
		}


		public List<KorfAddress> getFilteredKorfAddressList() {
			return filteredKorfAddressList;
		}


		public void setFilteredKorfAddressList(List<KorfAddress> filteredKorfAddressList) {
			this.filteredKorfAddressList = filteredKorfAddressList;
		}


		public String getShowActiveListBtn() {
			return showActiveListBtn;
		}


		public void setShowActiveListBtn(String showActiveListBtn) {
			this.showActiveListBtn = showActiveListBtn;
		}


		public String getShowActivateBtn() {
			return showActivateBtn;
		}


		public void setShowActivateBtn(String showActivateBtn) {
			this.showActivateBtn = showActivateBtn;
		}


		public List<KorfAddress> getSelectedKorfAddressList() {
			return selectedKorfAddressList;
		}


		public void setSelectedKorfAddressList(List<KorfAddress> selectedKorfAddressList) {
			this.selectedKorfAddressList = selectedKorfAddressList;
		}


		public String getKorfAddressId() {
			return korfAddressId;
		}


		public void setKorfAddressId(String korfAddressId) {
			this.korfAddressId = korfAddressId;
		}


		public String getShowIdentifier() {
			return showIdentifier;
		}


		public void setShowIdentifier(String showIdentifier) {
			this.showIdentifier = showIdentifier;
		}


		public List<MultiVal> getCountryList() {
			return countryList;
		}


		public void setCountryList(List<MultiVal> countryList) {
			this.countryList = countryList;
		}


		public List<MultiVal> getStateList() {
			return stateList;
		}


		public void setStateList(List<MultiVal> stateList) {
			this.stateList = stateList;
		}



		
		
}
