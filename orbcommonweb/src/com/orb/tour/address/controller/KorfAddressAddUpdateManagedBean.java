package com.orb.tour.address.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="korfAddressAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/tour")
public class KorfAddressAddUpdateManagedBean extends AbstractKorfAddressController {
	private static Log logger = LogFactory.getLog(KorfAddressAddUpdateManagedBean.class);
	
	private KorfAddress korfAddress =null;
	private List<KorfAddress> korfAddressList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String korfAddressId = null;
	private User loginUser = null;
	private List<MultiVal> countryList = null;
	private List<MultiVal> stateList = null;

	/**
	 * This method will be called when user clicks Add button / korfAddress Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateAddress")
	public String manipulateAddress( HttpServletRequest request, ModelMap model){
		korfAddressId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( TourConstants.SELECTED_ADDRESS_ID, korfAddressId );
		return TourConstants.ADDRESS_EDIT_HTML;
	}	

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		korfAddressId = getJSFRequestAttribute( TourConstants.SELECTED_ADDRESS_ID );
		logger.info(" KorfAddress Id :" +korfAddressId);
		countryList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_COUNTRY ); // Country List only required for Add / Update page.
		stateList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_STATE ); // State List only required for Add / Update page.
		if(CommonUtils.getValidString(korfAddressId).isEmpty()){
			korfAddress = new KorfAddress();
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			korfAddress=tourService.getKorfAddress(commonError, korfAddressId);
		 }
	}

	/**
	 * This method will add / update a KorfAddress in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		convertAddressToLatLng(korfAddress);
		if(CommonUtils.getValidString(korfAddress.getIdentifier()).isEmpty()){
			tourService.addAddress( commonError, loginUser, korfAddress );
		}else{
			tourService.updateAddress( commonError, loginUser, korfAddress );
		}
		korfAddressList = tourService.getActiveKorfAddressList(commonError);
		String msg=CommonUtils.getValidString(korfAddress.getIdentifier()).isEmpty() ? TourConstants.ADDRESS_ADDED_SUCCESS : TourConstants.ADDRESS_UPDATED_SUCCESS ;
		customRedirector( msg, TourConstants.ADDRESS_LIST_PAGE);		
	}

	/**
	 * This method to returns to KorfAddress view page
	 * @return 
	 */
	public void cancelAction(){
		if(CommonUtils.getValidString(korfAddress.getIdentifier()).isEmpty() ){
			redirector(TourConstants.ADDRESS_LIST_PAGE);
		}else{
			redirector(TourConstants.ADDRESS_VIEW_PAGE+"?objectId="+korfAddress.getIdentifier());
		}
	}

	public KorfAddress getKorfAddress() {
		return korfAddress;
	}

	public void setKorfAddress(KorfAddress korfAddress) {
		this.korfAddress = korfAddress;
	}

	public List<KorfAddress> getKorfAddressList() {
		return korfAddressList;
	}

	public void setKorfAddressList(List<KorfAddress> korfAddressList) {
		this.korfAddressList = korfAddressList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public String getKorfAddressId() {
		return korfAddressId;
	}

	public void setKorfAddressId(String korfAddressId) {
		this.korfAddressId = korfAddressId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<MultiVal> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<MultiVal> countryList) {
		this.countryList = countryList;
	}

	public List<MultiVal> getStateList() {
		return stateList;
	}

	public void setStateList(List<MultiVal> stateList) {
		this.stateList = stateList;
	}

	
	

}
