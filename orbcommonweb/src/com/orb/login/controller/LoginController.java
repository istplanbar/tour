package com.orb.login.controller;

import java.security.cert.CertificateException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.orb.admin.client.model.UserUSBTokenAuthentication;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.yubico.u2f.U2F;
import com.yubico.u2f.attestation.MetadataService;
import com.yubico.u2f.data.DeviceRegistration;
import com.yubico.u2f.data.messages.AuthenticateRequestData;
import com.yubico.u2f.data.messages.AuthenticateResponse;
import com.yubico.u2f.data.messages.RegisterRequestData;
import com.yubico.u2f.data.messages.RegisterResponse;
import com.yubico.u2f.exceptions.DeviceCompromisedException;
import com.yubico.u2f.exceptions.NoEligibleDevicesException;
import com.yubico.u2f.exceptions.U2fBadInputException;


@ManagedBean (name="loginControl")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/init")
public class LoginController extends AbstractCommonController{
    
	@Autowired 
	protected AdminService adminService;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	UserDAO userDAO;
	
	private UserUSBTokenAuthentication usbTokenDetails = null;
	private String givenUserId = null;
	
	
	//public static final String APP_ID = "https://localhost:8443";
	//public static String APP_ID = null;
	private final U2F u2f = new U2F();
	private final MetadataService metadataService = new MetadataService();
	
    @RequestMapping("/validateAuthentication")
	public String validateAuthentication(HttpServletRequest httpRequest, ModelMap model, Authentication authentication, @RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "error", required = false) String error) throws ParseException{
		Locale locale;
		HttpSession session = httpRequest.getSession();
		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	
    	
    	String userStartDate = session.getAttribute(CommonConstants.USER_START_DATE).toString().substring(0,11);
    	populateResourceBundleinMap(model, locale);
    	    	
    	if (error != null) {
    		StringBuffer errorMsg = new StringBuffer(getMessage(CommonConstants.USER_NOT_STARTED, locale));
    		errorMsg.append(" " + userStartDate +" ");
    		errorMsg.append(getMessage(CommonConstants.USER_NOT_STARTED_LOGIN, locale));
    		
    		model.addAttribute("error", errorMsg.toString());
		}
    	session.removeAttribute(CommonConstants.USER_START_DATE);
		return CommonConstants.LOGIN_PAGE;
		 
	}
	
	@RequestMapping("/login")
    public String login(HttpServletRequest httpRequest, ModelMap model, @RequestParam(value = "language", required = false) String language,
    		@RequestParam(value = "error", required = false) String error,
    		@RequestParam(value = "logout", required = false) String logout,
    		@RequestParam(value = "forgot", required = false) String forgot,
    		@RequestParam(value = "resetSuccess", required = false) String resetSuccess){
    	Locale locale;
    	
    	Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	if (error != null) {
    		model.addAttribute("error", 
                    getErrorMessage(httpRequest, locale ));
		}
    	populateResourceBundleinMap(model, locale);
		if (logout != null) {
			try {
				httpRequest.logout();
				model.addAttribute("msg", getMessage( CommonConstants.USER_LOGGED_OUT, locale));
			} catch (ServletException e) {
				// TODO Auto-generated catch block
			}
		}
		
		if (forgot != null) {
				model.addAttribute("msg", getMessage( ForgotPasswordConstant.CHECK_MAIL, locale));
		}
		
		if (resetSuccess != null) {
			model.addAttribute("msg", getMessage( ForgotPasswordConstant.RESET_DONE, locale));
		}
    	
		HttpSession session = httpRequest.getSession();
		 Map<String, String> adminConfMapForPass = null;
		 adminConfMapForPass = adminService.getAdminConfMap(commonError);
	     String passwordValidation = adminConfMapForPass.get("passwordChangeValid");
		
	     if(getMessage( CommonConstants.USER_CREDENTIAL_EXPIRED, locale).equals(model.get("error")) && passwordValidation.equals("Yes")){
    		model.remove("error");
    		model.remove( CommonConstants.USER_LOCALE);
            session.setAttribute(CommonConstants.PASSWORD_EXP_MSG, CommonConstants.TRUE);
    		return "redirect:"+ForgotPasswordConstant.FORGOT_PASS_REDIR;
    	}else{
            session.setAttribute(CommonConstants.PASSWORD_EXP_MSG, CommonConstants.FALSE);
    		return CommonConstants.LOGIN_PAGE;
    	}
    }
    
	
	/**
	 * This method will be called when user has second level authentication & get mTan   
	 * @param request
	  * @return
	 */
	@RequestMapping("/secondLevelAuth")
    public String secondLevelAuth(HttpServletRequest httpRequest, ModelMap model){
		HttpSession session = httpRequest.getSession();
	Map<String, String> adminConfMap = (Map<String, String>)session.getAttribute(CommonConstants.ADMIN_CONF);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	adminService.sendMTAN(commonError, user);
    	populateResourceBundleinMap(model, locale);
    	model.addAttribute("msg", getMessage("info_mtan", locale));
    	return CommonConstants.SECOND_LEVEL_AUTH_PAGE;
	}
	
	/**
	 * This method will be called for authentication page submission   
	 * @param request
	  * @return
	 */
	
	@RequestMapping("/secondLevelAuthSubmit")
    public String secondLevelAuthSubmit(HttpServletRequest httpRequest, ModelMap model, @RequestParam(value = "mtan", required = false) String mtan){
		HttpSession session = httpRequest.getSession();
	Map<String, String> adminConfMap = (Map<String, String>)session.getAttribute(CommonConstants.ADMIN_CONF);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	String targetUrl = null;
    	populateResourceBundleinMap(model, locale);
       	if (adminService.checkMTAN(commonError, user.getUserId(), mtan)){
    		targetUrl = "redirect:"+(String)session.getAttribute(CommonConstants.CACHED_URL);
		}else{
    		model.addAttribute("error", getMessage("invalid_mtan", locale));
			targetUrl = CommonConstants.SECOND_LEVEL_AUTH_PAGE;
		}
    	return targetUrl;
	}
	
	
	/**
	 * 
	 * @param httpRequest
	 * @param model
	 * @return
	 */
	@RequestMapping("/secondLevelAuthTokenEntryView")
	public String secondLevlAuth(HttpServletRequest httpRequest, ModelMap model){
		HttpSession session = httpRequest.getSession();
		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	populateResourceBundleinMap(model, locale);
		return CommonConstants.SECOND_LEVEL_AUTH_TOKEN_ENTRY_PAGE;
	}
	
	/**
	 * This method will be invoked when user tries to register the token.   
	 * @param request
	  * @return
	 */
	@RequestMapping("/secondLevelAuthTokenUserValidation")
    public String  secondLevelAuthReg(HttpServletRequest httpRequest, ModelMap model, @RequestParam(value = "username", required = false) String userId){
		HttpSession session = httpRequest.getSession();
		Map<String, String> requestStorage = new HashMap<String, String>();
		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	//User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	String targetUrl = null;
    	populateResourceBundleinMap(model, locale);
    	if (getAllUsersList().stream().filter(x -> x.getUserId().equals(userId)).findFirst().isPresent()){
    		List<DeviceRegistration> registerationData = (List<DeviceRegistration>) getRegistrations(userId);
    		if(!registerationData.isEmpty()){    
    			RegisterRequestData registerRequestData = u2f.startRegistration(generateAppId(httpRequest), registerationData);
	            requestStorage.put(registerRequestData.getRequestId(), registerRequestData.toJson());
	        	model.addAttribute("msg", getMessage("info_mtan", locale));
	        	model.addAttribute("data", registerRequestData.toJson());
	        	model.addAttribute("username", userId); 
	        	session.setAttribute("requestStorage", requestStorage);
	        	System.out.println("Data"+registerRequestData.toJson());
	        	targetUrl = CommonConstants.SECOND_LEVEL_AUTH_TOKEN_REDIRECT_TO_LOGIN_PAGE;
	        }else{
	        	RegisterRequestData registerRequestData = u2f.startRegistration(generateAppId(httpRequest),registerationData);
	        	requestStorage.put(registerRequestData.getRequestId(), registerRequestData.toJson());
	        	session.setAttribute("requestStorage", requestStorage);
	        	model.addAttribute("username", userId); 
	        	model.addAttribute("data", registerRequestData.toJson());
	        	targetUrl = CommonConstants.SECOND_LEVEL_AUTH_TOKEN_REGISTRATION_PAGE;
	        }
	        
		}else{
    		model.addAttribute("error", getMessage(CommonConstants.INVALID_USERNAME, locale));
			targetUrl = CommonConstants.SECOND_LEVEL_AUTH_TOKEN_ENTRY_PAGE;
		}
    	return targetUrl;
    }
	/**
	 * This method will be called when user did click the Register button.   
	 * @param request
	  * @return
	 */
	@RequestMapping("/secondLevelAuthTokenRegistration")
    public String  secondLevelAuthTokenRegistration(HttpServletRequest httpRequest, ModelMap model){
		 try {
			 
			Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
		    Locale locale = new Locale( adminConfMap.get("defaultLocale") );
		    populateResourceBundleinMap(model, locale);
		    model.addAttribute("msg", "Token Registered Successfully");
			HttpSession session = httpRequest.getSession();
			Map<String, String> requestStorage = (Map<String, String>)session.getAttribute("requestStorage");
			String response = httpRequest.getParameter("tokenResponse");
			String userId = httpRequest.getParameter("username");
			User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
			RegisterResponse registerResponse = RegisterResponse.fromJson(response);
	        RegisterRequestData registerRequestData = RegisterRequestData.fromJson(requestStorage.remove(registerResponse.getRequestId()));
	        DeviceRegistration registration = u2f.finishRegistration(registerRequestData, registerResponse);
	        metadataService.getAttestation(registration.getAttestationCertificate());
			addRegistration(userId, registration);
		} catch (CertificateException | NoSuchFieldException e) {
			System.out.println("Invalid request");
		} catch (U2fBadInputException e) {
			System.out.println("Bad Input");
		}
        return CommonConstants.LOGIN_PAGE;
	}
	
	

	/**
	 * This method will be called when user has second level authentication & get mTan   
	 * @param request
	  * @return
	 */
	@RequestMapping("/secondLevelAuthTokenStartAuthentication")
    public String  secondLevelAuthTokenStartAuthentication(HttpServletRequest httpRequest, ModelMap model){
		HttpSession session = httpRequest.getSession();
		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	
    	//adminService.sendMTAN(commonError, user);
    	populateResourceBundleinMap(model, locale);

    	AuthenticateRequestData authenticateRequestData;
		try {
			Map<String, String> requestStorage = (Map<String, String>)session.getAttribute("requestStorage");
			if( requestStorage == null ) {
				requestStorage = new HashMap<>();
				session.setAttribute("requestStorage", requestStorage);
			}
			authenticateRequestData = u2f.startAuthentication(generateAppId(httpRequest), getRegistrations(user.getUserId()));
			requestStorage.put(authenticateRequestData.getRequestId(), authenticateRequestData.toJson());
	    	model.addAttribute("msg", getMessage("info_mtan", locale));
	    	model.addAttribute("data", authenticateRequestData.toJson());    	
		} catch (U2fBadInputException e) {
			//return "redirect:" + CommonConstants.SECOND_LEVEL_REG;
			return CommonConstants.LOGIN_PAGE;
		} catch (NoEligibleDevicesException e) {
			//return "redirect:" + CommonConstants.SECOND_LEVEL_REG;
			return CommonConstants.LOGIN_PAGE;
		}
    	
    	return CommonConstants.SECOND_LEVEL_AUTH_TOKEN_AUTHENTICATION_PAGE;
	}
	
	/**
	 * This method will be called for authentication page submission   
	 * @param request
	  * @return
	 */
	
	@RequestMapping("/secondLevelAuthTokenFinishAuthentication")
    public String secondLevelAuthTokenFinishAuthentication(HttpServletRequest httpRequest, ModelMap model ){
		String tokenResponse = httpRequest.getParameter("tokenResponse");
		HttpSession session = httpRequest.getSession();
		Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	Locale locale = new Locale( adminConfMap.get("defaultLocale") );
    	User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
    	String targetUrl = null;
    	AuthenticateResponse authenticateResponse = null;
    	populateResourceBundleinMap(model, locale);
    	Map<String, String> requestStorage = (Map<String, String>)session.getAttribute("requestStorage");
    	if(!"".equals(CommonUtils.getValidString(tokenResponse)))
    		authenticateResponse = AuthenticateResponse.fromJson(tokenResponse);
    	
        
    	AuthenticateRequestData authenticateRequest = AuthenticateRequestData.fromJson(requestStorage.remove(authenticateResponse.getRequestId()));
        DeviceRegistration registration = null;
        try {
            registration = u2f.finishAuthentication(authenticateRequest, authenticateResponse, getRegistrations(user.getUserId()));
        } catch (DeviceCompromisedException e) {
            registration = e.getDeviceRegistration();
            return CommonConstants.LOGIN_PAGE;
        } 
       /*	if (adminService.checkMTAN(commonError, user.getUserId(), mtan)){
    		targetUrl = "redirect:"+(String)session.getAttribute(CommonConstants.CACHED_URL);
		}else{
    		model.addAttribute("error", getMessage("invalid_mtan", locale));
			targetUrl = CommonConstants.SECOND_LEVEL_AUTH_PAGE;

		}*/
        targetUrl = "redirect:"+(String)session.getAttribute(CommonConstants.CACHED_URL);
    	return targetUrl;
	}
	
	/**
	 * This method will be called when user forgot their password  
	 * @param request
	  * @return
	 */

    @RequestMapping("/forgotPassword")
    public String forgotPassword(HttpServletRequest httpRequest, ModelMap model, @RequestParam(value = "language", required = false) String language, Locale locale,
    		@RequestParam(value = "error", required = false) String error){
    	
    	Map<String, String> adminConfMap = adminService.getAdminConfMap(commonError);
    	model.addAttribute(CommonConstants.ADMIN_CONF, adminConfMap);
    	
    	if( language == null ){
    		locale = new Locale( adminConfMap.get("defaultLocale") );
    	}else{
    		locale = new Locale(language);
    	}
    	if (error != null) {
    		model.addAttribute("error", getErrorMessage(httpRequest, locale ));
		}
    	
    	populateResourceBundleinMap(model, locale);
    	
    	return CommonConstants.FORGOT_PAGE;
    }

  //customize the error message
  	private String getErrorMessage( HttpServletRequest httpRequest, Locale locale ){
  	
  		Exception exception = 
                     (Exception) httpRequest.getSession().getAttribute(CommonConstants.SPRING_SECURITY_LAST_EXCEPTION);
  		
  		String error = "";
  		String userName = (String) httpRequest.getSession().getAttribute(CommonConstants.SPRING_SECURITY_LAST_USERNAME);
        com.orb.common.client.model.User loginUser = userDAO.getLoginUser(userName);
		commonDAO.loginAuditTrail(loginUser, CommonConstants.LOGIN_FAILED);
		Map<String, String> adminConfMapForPass = null;
		 adminConfMapForPass = adminService.getAdminConfMap(commonError);
	     String passwordValidation = adminConfMapForPass.get("passwordChangeValid");
		
		if(loginUser == null){
			return getMessage(CommonConstants.USER_LOCKED, locale);
		}
		
  		if (exception instanceof BadCredentialsException || exception instanceof AuthenticationServiceException) {
  			//error = "Invalid username or password!";
  			return getMessage(CommonConstants.INVALID_USER, locale);
  		}else if(exception instanceof DisabledException) {
  			//error = "User Account is Disabled! Please contact Administrator";
  			return getMessage(CommonConstants.USER_DISABLED, locale);
  		}else if(exception instanceof AccountExpiredException) {
  			//error = "User Account is Expired! Please contact Administrator";
  			return getMessage(CommonConstants.USER_EXPIRED, locale);
  			
  		}else if(exception instanceof CredentialsExpiredException && passwordValidation.equals("Yes")) {
  			//error = "User Credentials is Expired! Please contact Administrator";
  			return getMessage(CommonConstants.USER_CREDENTIAL_EXPIRED, locale);
  		}else if(exception instanceof LockedException) {
  			//error = "User Account is Locked! Please contact Administrator";
  			return getMessage(CommonConstants.USER_LOCKED, locale);
  		}else{
  			return exception.getMessage();
  		}
  		
  	}
  	
  	private void populateResourceBundleinMap( ModelMap model, Locale locale ){
  		Map<String,String> resBundle = new HashMap<String,String>();
  		resBundle.put("hdr_login", getMessage("hdr_login", locale));
  		resBundle.put("lbl_userName", getMessage("lbl_userName", locale));
  		resBundle.put("lbl_password", getMessage("lbl_password", locale));
  		resBundle.put("lbl_login", getMessage("lbl_login", locale));
  		resBundle.put("lbl_forgotPassword", getMessage("lbl_forgotPassword", locale));
  		resBundle.put("lbl_rememberMe", getMessage("lbl_rememberMe", locale));
  		resBundle.put("btn_back", getMessage("btn_back", locale));
  		resBundle.put("btn_sendPassword", getMessage("btn_sendPassword", locale));
  		resBundle.put("lbl_email", getMessage("lbl_email", locale));
		resBundle.put("btn_confirmation", getMessage("btn_confirmation", locale));
  		resBundle.put("btn_cancel", getMessage("btn_cancel", locale));
  		resBundle.put("hdr_mTAN", getMessage("hdr_mTAN", locale));
  		resBundle.put("lbl_mTAN", getMessage("lbl_mTAN", locale));
  		resBundle.put("error_emailNotValid", getMessage("error_emailNotValid", locale));
  		resBundle.put("hdr_registration", getMessage("hdr_registration", locale));
  		resBundle.put("btn_submit", getMessage("btn_submit", locale));
  		resBundle.put("btn_gotoGoogleAuthOpenPage", getMessage("btn_gotoGoogleAuthOpenPage", locale));
  		
		model.addAttribute( CommonConstants.LOGIN_RES_BUN, resBundle );
  	}
  	
  	public void cancelAction(ActionEvent e){
  		redirector( CommonConstants.LOGIN_PAGE );
  	}
   
  	private Iterable<DeviceRegistration> getRegistrations(String userName) {
        List<DeviceRegistration> registrations = new ArrayList<DeviceRegistration>();

        usbTokenDetails = adminService.getUserTokenAuthentication(commonError, userName);
        //String jsonString = "{\"keyHandle\":\"yU6BB3rwxSzGnDG_reUNDEin93Jp90GneaCemltSCsspWyKfWd2PGYY3I1PTA9NsmM4_yheuWPpTpkVL7ZHBaA\",\"publicKey\":\"BOh7L5J-kGTaY2mh_euNOSC9xivmjIXZYLh2VGIYR6_i2TrrqbvRJAACr_-5N3W3dnpd0_b4lszOdnCh4x4O1cE\",\"counter\":17,\"compromised\":false}";
        //registrations.add(DeviceRegistration.fromJson(jsonString));
        
        //The JSON Object should be generated based on the user date from DB for the current user.

		//jsonObj.put("keyHandle","yU6BB3rwxSzGnDG_reUNDEin93Jp90GneaCemltSCsspWyKfWd2PGYY3I1PTA9NsmM4_yheuWPpTpkVL7ZHBaA");
		//jsonObj.put("publicKey", "BOh7L5J-kGTaY2mh_euNOSC9xivmjIXZYLh2VGIYR6_i2TrrqbvRJAACr_-5N3W3dnpd0_b4lszOdnCh4x4O1cE");
        if(usbTokenDetails  != null) {
        	String jsonString = null;
        	JSONObject jsonObj = new JSONObject();
        	jsonObj.put("keyHandle", usbTokenDetails.getKeyHandle());
    		jsonObj.put("publicKey", usbTokenDetails.getPublicKey());
    		jsonObj.put("counter", usbTokenDetails.getCounter());
    		jsonObj.put("compromised", usbTokenDetails.getCompromised());
    		jsonString = jsonObj.toString();
            registrations.add(DeviceRegistration.fromJson(jsonString));
        }

        return registrations;
    }
  	
    private void addRegistration(String username, DeviceRegistration registration) {
    	System.out.println("registration.toJson() ==>" + registration.toJson());
    	//String jsonStr = "{\"keyHandle\":\"yU6BB3rwxSzGnDG_reUNDEin93Jp90GneaCemltSCsspWyKfWd2PGYY3I1PTA9NsmM4_yheuWPpTpkVL7ZHBaA\",\"publicKey\":\"BOh7L5J-kGTaY2mh_euNOSC9xivmjIXZYLh2VGIYR6_i2TrrqbvRJAACr_-5N3W3dnpd0_b4lszOdnCh4x4O1cE\",\"counter\":17,\"compromised\":false}";
    	usbTokenDetails = new UserUSBTokenAuthentication();
    	String jsonStr = registration.toJson();
		JSONObject jsonObj = new JSONObject(jsonStr);
		usbTokenDetails.setUserId(username);
		usbTokenDetails.setKeyHandle(String.valueOf(jsonObj.get("keyHandle")));
		usbTokenDetails.setPublicKey(String.valueOf(jsonObj.get("publicKey")));
		usbTokenDetails.setCounter(String.valueOf(jsonObj.get("counter")));
		usbTokenDetails.setCompromised(String.valueOf(jsonObj.get("compromised")));
		adminService.addUserTokenAuthentication(commonError, usbTokenDetails);
		
		//System.out.println("keyHandle ==> " + jsonObj.get("keyHandle"));
		//System.out.println("publicKey ==> " + jsonObj.get("publicKey"));
		//System.out.println("counter ==> " + jsonObj.get("counter"));
		//System.out.println("compromised ==> " + jsonObj.get("compromised"));
		
		//The above sysout values should be strod in DB at the time of registration for the current user.
    }
    
    public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}


	public String getGivenUserId() {
		return givenUserId;
	}

	public void setGivenUserId(String givenUserId) {
		this.givenUserId = givenUserId;
	}
	
	private String generateAppId( HttpServletRequest httpRequest ){
		StringBuffer baseURL = new StringBuffer();
        baseURL.append( "https://" );
        baseURL.append( httpRequest.getServerName() );
        //baseURL.append( ":8453" );
        return baseURL.toString();
	}
	
	
}