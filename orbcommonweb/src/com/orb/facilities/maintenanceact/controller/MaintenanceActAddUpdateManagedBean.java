package com.orb.facilities.maintenanceact.controller;
/**
 * @author IST planbar / KaSe
 * 
 */
import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.facilities.controller.AbstractFacilitiesController;


@ManagedBean(name="maintenanceActAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/facilities")
public class MaintenanceActAddUpdateManagedBean extends AbstractFacilitiesController{
    private static Log logger = LogFactory.getLog(MaintenanceActAddUpdateManagedBean.class);
	
	private MaintenanceAct maintenanceAct =null;
	private MaintenanceControl maintenanceControl =null;
	private MaintenanceSubstance maintenanceSubstance =null;
	private List<MaintenanceAct> facilitiesList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String maintenanceActId = null;
	private User loginUser = null;
	
	/**
	 * This method will be called when user clicks Add button / MaintenanceAct Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateMaintenanceAct")
	public String manipulateMaintenanceAct( HttpServletRequest request, ModelMap model){
		maintenanceActId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID, maintenanceActId );
		return FacilitiesConstants.MAINTENANCE_ACT_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		maintenanceActId = getJSFRequestAttribute( FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID );
		logger.info(" Maintenance Act Id :" +maintenanceActId);
		if(CommonUtils.getValidString(maintenanceActId).isEmpty()){
			maintenanceAct = new MaintenanceAct();
			maintenanceControl =new MaintenanceControl();
			maintenanceSubstance =new MaintenanceSubstance();
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			maintenanceAct=facilitiesService.getMaintenanceAct(commonError, maintenanceActId);
			maintenanceControl =facilitiesService.getMaintenanceControl(commonError, maintenanceActId);
			maintenanceSubstance =facilitiesService.getMaintenanceSubstance(commonError, maintenanceActId);
		 }
	}

	/**
	 * This method will add / update a maintenanceAct in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(CommonUtils.getValidString(maintenanceAct.getId()).isEmpty()){
			//facilitiesService.add( commonError, loginUser, maintenanceAct,maintenanceControl,maintenanceSubstance );
		}else{
			facilitiesService.update( commonError, loginUser, maintenanceAct,maintenanceControl,maintenanceSubstance );
		}
		
		String msg=CommonUtils.getValidString(maintenanceAct.getId()).isEmpty() ? FacilitiesConstants.MAINTENANCE_ACT_ADDED_SUCCESS : FacilitiesConstants.MAINTENANCE_ACT_UPDATED_SUCCESS ;
		customRedirector( msg, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);		
	}


	/**
	 * This method to returns to maintenanceAct view page
	 * @return 
	 */
	public void cancelAction(){
		if(maintenanceAct.getId()==null){
			redirector(FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
		}else{
			redirector(FacilitiesConstants.MAINTENANCE_ACT_VIEW_PAGE+"?objectId="+maintenanceAct.getIdentifier());
		}
	}
	
	public MaintenanceAct getMaintenanceAct() {
		return maintenanceAct;
	}
	public void setMaintenanceAct(MaintenanceAct maintenanceAct) {
		this.maintenanceAct = maintenanceAct;
	}
	public MaintenanceControl getMaintenanceControl() {
		return maintenanceControl;
	}
	public void setMaintenanceControl(MaintenanceControl maintenanceControl) {
		this.maintenanceControl = maintenanceControl;
	}
	public MaintenanceSubstance getMaintenanceSubstance() {
		return maintenanceSubstance;
	}
	public void setMaintenanceSubstance(MaintenanceSubstance maintenanceSubstance) {
		this.maintenanceSubstance = maintenanceSubstance;
	}
	public List<MaintenanceAct> getFacilitiesList() {
		return facilitiesList;
	}
	public void setFacilitiesList(List<MaintenanceAct> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}
	public String getShowIdentifier() {
		return showIdentifier;
	}
	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}
	public String getMaintenanceActId() {
		return maintenanceActId;
	}
	public void setMaintenanceActId(String maintenanceActId) {
		this.maintenanceActId = maintenanceActId;
	}
	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	
	

}
