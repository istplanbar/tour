package com.orb.facilities.maintenanceact.controller;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * @author IST planbar / KaSe
 * 
 */
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.facilities.controller.AbstractFacilitiesController;

@ManagedBean(name="maintenanceActView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/facilities")
public class MaintenanceActListViewManagedBean extends AbstractFacilitiesController{
	private static Log logger = LogFactory.getLog(MaintenanceActListViewManagedBean.class);
	
	private User loginUser=null;
	private List<Boolean> togglerList = null;
	private MaintenanceAct maintenanceAct =null;
	private MaintenanceControl maintenanceControl =null;
	private MaintenanceSubstance maintenanceSubstance =null;
	private List<MaintenanceAct> maintenanceActList = null;
	private List<MaintenanceAct> filteredMaintenanceActList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<MaintenanceAct> selectedMaintenanceActList = null;
	private String maintenanceActId = null;
	private String showIdentifier = CommonConstants.TRUE;
	
	/**
	 * This page will be called when user clicks the maintenanceAct link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadMaintenanceAct")
    public String loadMaintenanceAct(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return FacilitiesConstants.MAINTENANCE_ACT_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular MaintenanceAct from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewMaintenanceAct")
	public String viewMaintenanceAct(HttpServletRequest request, ModelMap model){
		 maintenanceActId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID, maintenanceActId );
		 return FacilitiesConstants.MAINTENANCE_ACT_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		maintenanceActList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		maintenanceActId = CommonUtils.getValidString(getJSFRequestAttribute(FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID) );
		logger.info(" MaintenanceActId :" +maintenanceActId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			//maintenanceActList=facilitiesService.getInactiveMaintenanceActList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			//maintenanceActList=facilitiesService.getActiveMaintenanceActList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true);
		}
		if(!CommonUtils.getValidString(maintenanceActId).isEmpty()){
			maintenanceAct = facilitiesService.getMaintenanceAct(commonError, maintenanceActId);
			maintenanceControl =facilitiesService.getMaintenanceControl(commonError, maintenanceActId);
			maintenanceSubstance =facilitiesService.getMaintenanceSubstance(commonError, maintenanceActId);
		}
	}
	
	/**
	 * This method will get all the active maintenanceAct
	 * @param event
	 */
	public void showActive(ActionEvent event){
		//maintenanceActList = facilitiesService.getActiveMaintenanceActList( commonError );
		if( filteredMaintenanceActList != null ) filteredMaintenanceActList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive MaintenanceAct
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		//maintenanceActList = facilitiesService.getInactiveMaintenanceActList(commonError);
		if( filteredMaintenanceActList != null ) filteredMaintenanceActList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks maintenanceAct
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		facilitiesService.lockMaintenanceAct( commonError, loginUser, selectedMaintenanceActList );
		showActive(event);
		customRedirector( FacilitiesConstants.MAINTENANCE_ACT_LOCKED_SUCCESS,FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
	}
	
	/**
	 * This method unlocks MaintenanceAct
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		facilitiesService.unlockMaintenanceAct( commonError, loginUser, selectedMaintenanceActList );
		showInactive(event);
		customRedirector( FacilitiesConstants.MAINTENANCE_ACT_UNLOCKED_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the MaintenanceAct
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(selectedMaintenanceActList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS);
		}else{	
		    facilitiesService.deleteMaintenanceAct( commonError, loginUser, selectedMaintenanceActList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	
	/**
	 * This method will navigate to the page where user can add a maintenanceAct
	 */
	public void addAction(){
		addSessionObj( FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST , togglerList );
		redirector(FacilitiesConstants.MAINTENANCE_ACT_EDIT_PAGE);
	}
	
	/**
	 * This method to returns to main maintenanceAct list page
	 */
	public void cancelAction(){
		redirector(FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE +"?activeFlag="+maintenanceAct.getActive());
	}
	
	/**
	 * This method will navigate to maintenanceAct Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(FacilitiesConstants.MAINTENANCE_ACT_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to maintenanceAct View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST , togglerList );
		redirector(FacilitiesConstants.MAINTENANCE_ACT_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed maintenanceAct
	 * @param index
	 */
	public void deleteViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
		facilitiesService.deleteViewedMaintenanceAct( commonError, loginUser, maintenanceAct);
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed maintenanceAct
	 */
	public void lockViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
	    facilitiesService.lockViewedMaintenanceAct( commonError, loginUser, maintenanceAct );
		customRedirector( FacilitiesConstants.MAINTENANCE_ACT_LOCKED_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
	}

	/**
	 * This method locks viewed maintenanceAct
	 * @param index
	 * @return
	 */
	public void unlockViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
		facilitiesService.unlockViewedMaintenanceAct( commonError, loginUser, maintenanceAct );
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.MAINTENANCE_ACT_UNLOCKED_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( FacilitiesConstants.SELECTED_MAINTENANCE_ACT_ID );
		if( toggleList != null ) model.remove( FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST );		
    }


	
	
	public List<MaintenanceAct> getSelectedMaintenanceActList() {
		return selectedMaintenanceActList;
	}


	public void setSelectedMaintenanceActList(List<MaintenanceAct> selectedMaintenanceActList) {
		this.selectedMaintenanceActList = selectedMaintenanceActList;
	}


	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	public List<Boolean> getTogglerList() {
		return togglerList;
	}
	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	public MaintenanceAct getMaintenanceAct() {
		return maintenanceAct;
	}
	public void setMaintenanceAct(MaintenanceAct maintenanceAct) {
		this.maintenanceAct = maintenanceAct;
	}
	public MaintenanceControl getMaintenanceControl() {
		return maintenanceControl;
	}
	public void setMaintenanceControl(MaintenanceControl maintenanceControl) {
		this.maintenanceControl = maintenanceControl;
	}
	public MaintenanceSubstance getMaintenanceSubstance() {
		return maintenanceSubstance;
	}
	public void setMaintenanceSubstance(MaintenanceSubstance maintenanceSubstance) {
		this.maintenanceSubstance = maintenanceSubstance;
	}
	public List<MaintenanceAct> getMaintenanceActList() {
		return maintenanceActList;
	}
	public void setMaintenanceActList(List<MaintenanceAct> maintenanceActList) {
		this.maintenanceActList = maintenanceActList;
	}
	public List<MaintenanceAct> getFilteredMaintenanceActList() {
		return filteredMaintenanceActList;
	}
	public void setFilteredMaintenanceActList(List<MaintenanceAct> filteredMaintenanceActList) {
		this.filteredMaintenanceActList = filteredMaintenanceActList;
	}
	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}
	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	public String getShowActivateBtn() {
		return showActivateBtn;
	}
	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}
	public String getMaintenanceActId() {
		return maintenanceActId;
	}
	public void setMaintenanceActId(String maintenanceActId) {
		this.maintenanceActId = maintenanceActId;
	}
	public String getShowIdentifier() {
		return showIdentifier;
	}
	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}
	
	

}
