package com.orb.facilities.controller;
/**
 * @author IST planbar / KaSe
 * 
 */

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.Crm;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.qualifications.bean.Qualifications;


@ManagedBean(name="facilitiesAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/facilities")
public class FacilitiesAddUpdateManagedBean extends AbstractFacilitiesController{
	private static Log logger = LogFactory.getLog(FacilitiesAddUpdateManagedBean.class);
	
	private Facilities facilities =null;
	private List<Facilities> facilitiesList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String facilitiesId = null;
	private User loginUser = null;
	private List<Qualifications> qualificationsList = null;
	private List<Crm> crmList = null;
	private List<MaintenanceAct> maintenanceActList = null;
	private List<String> yearList = null;
	private List<String> qualificationsNameList = null;
	private List<MultiVal> weekList = null;
	private List<String> visitingDayList = null;
	private boolean latitudeBool = false;
	private boolean longitudeBool = false;
	private Map<String, String> monthMap = null;
	private List<String> monthMapList = null;
	private List<MultiVal> timeList = null;
	private VisitingDayTimeHours dayTimeHours = null;
	private List<VisitingDayTimeHours> visitingDayTimeHoursList = null;
		
	

	/**
	 * This method will be called when user clicks Add button / facilities Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateFacilities")
	public String manipulateFacilities( HttpServletRequest request, ModelMap model){
		facilitiesId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( FacilitiesConstants.SELECTED_FACILITIES_ID, facilitiesId );
		return FacilitiesConstants.FACILITIES_EDIT_HTML;
	}

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		maintenanceActList = new ArrayList<>();
		qualificationsNameList = new ArrayList<>();
		visitingDayList = new ArrayList<>();
		monthMap = new HashMap<>();
		setYearList(new ArrayList<>());
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		facilitiesId = getJSFRequestAttribute( FacilitiesConstants.SELECTED_FACILITIES_ID );
		weekList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_WEEK_DAY );// Week List only required for Add / Update page.
		
		//Selva Added for visitngTime at 04/06/2018
		
		timeList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_TIME );
		
		logger.info(" Facilities Id :" +facilitiesId);
		qualificationsList = qualificationsService.getActiveQualificationsList(commonError);
		crmList = crmService.getActiveCrmList(commonError);
		//maintenanceActList=facilitiesService.getActiveMaintenanceActList(commonError);
		
		if("".equals(CommonUtils.getValidString(facilitiesId))){
			facilities = new Facilities();
			dayTimeHours = new VisitingDayTimeHours();
			visitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHoursList(dayTimeHours, 7);
			setShowIdentifier(CommonConstants.FALSE);
		}else{
			visitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHours(commonError, facilitiesId);
			if(visitingDayTimeHoursList.isEmpty() || visitingDayTimeHoursList == null){
				visitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHoursList(dayTimeHours, 7);
			}
			if(facilities == null) facilities = new Facilities();
			if(dayTimeHours == null) dayTimeHours = new VisitingDayTimeHours();
			
		}
		
		
		monthMap = (Map<String,String>)FacilitiesConstants.MAINTENACE_CYCLE_MONTH_NAME;
		int yearNo =  Year.now().getValue();
		List<Integer> range = IntStream.rangeClosed(yearNo-48, yearNo)
			    .boxed().collect(Collectors.toList());
		for (Integer yearVal : range) { 
			yearList.add(String.valueOf(yearVal)); 
		}
		if( maintenanceActList == null) maintenanceActList = new ArrayList<>();
		if(crmList == null) crmList = new ArrayList<>();
    	if(CommonUtils.getValidString(facilitiesId).isEmpty()){
			facilities = new Facilities();
			setShowIdentifier(CommonConstants.FALSE);
			monthMapList = new ArrayList<>();
		}else{
			facilities=facilitiesService.getFacilities(commonError, facilitiesId);
			if(!CommonUtils.getValidString(facilities.getMonthOfMaintenance()).isEmpty()){
				monthMapList = Arrays.asList(facilities.getMonthOfMaintenance().split(","));
			}
			if(!CommonUtils.getValidString(facilities.getQualificationsId()).isEmpty()){
			  qualificationsNameList = Arrays.asList(facilities.getQualificationsId().split(","));
			}
			if(!CommonUtils.getValidString(facilities.getVisitingDay()).isEmpty()){
				visitingDayList = Arrays.asList(facilities.getVisitingDay().split(","));
			 
			}
		}
	}

	/**
	 * This method will add / update a Facilities in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(!commonError.getError().isEmpty()){
			commonError.clearErrors();
		}
		if(!CommonUtils.getValidString(facilities.getDegreeOfLatitude()).isEmpty()
	    		|| !CommonUtils.getValidString(facilities.getDegreeOfLongitude()).isEmpty()){
			if(!CommonUtils.getValidString(facilities.getDegreeOfLatitude()).isEmpty()){
				String latitude = facilities.getDegreeOfLatitude().replaceAll("''", String.valueOf((char)34));
				facilities.setDegreeOfLatitude(latitude);
			}
			if(!CommonUtils.getValidString(facilities.getDegreeOfLongitude()).isEmpty()){
				String longitude = facilities.getDegreeOfLongitude().replaceAll("''", String.valueOf((char)34));
				facilities.setDegreeOfLongitude(longitude);
			}
		}
		if(!CommonUtils.getValidString(facilities.getMaintenanceCycle()).isEmpty()){
			if(Integer.parseInt(facilities.getMaintenanceCycle()) != monthMapList.size()){
				commonError.addError(getJSFMessage("err_invalid_month_selection"));
				return;
			}
			if(!monthMapList.isEmpty()){
				facilities.setMonthOfMaintenance(monthMapList.stream().map(m -> m).collect(Collectors.joining(",")));
			}
		}
		String qualificationName = qualificationsNameList.stream().map(n -> n).collect(Collectors.joining(","));
		facilities.setQualificationsId(qualificationName);
		
		if((!CommonUtils.getValidString(facilities.getLocationStreet()).isEmpty() && 
			!CommonUtils.getValidString(facilities.getLocationPlace()).isEmpty()) || 
			(!CommonUtils.getValidString(facilities.getLocationStreet()).isEmpty() 
			&& !CommonUtils.getValidString(facilities.getLocationPostCode()).isEmpty())){
				convertAddressToLatLng(facilities);
		}
		
		if(!visitingDayTimeHoursList.isEmpty()){
			for(VisitingDayTimeHours vtime : visitingDayTimeHoursList){
				vtime.setVisitingDays(weekList.stream().filter(w -> w.getValue().equals(vtime.getVisitingDays())).findAny().map(MultiVal::getId).get());
			}
		}
		
		if(CommonUtils.getValidString(facilities.getId()).isEmpty()){
			facilitiesService.add( commonError, loginUser, facilities ,visitingDayTimeHoursList);
		}else{
			facilitiesService.update( commonError, loginUser, facilities, visitingDayTimeHoursList);
		}
		facilitiesList=facilitiesService.getActiveFacilitiesList(commonError);
		String msg=CommonUtils.getValidString(facilities.getId()).isEmpty() ? FacilitiesConstants.FACILITIES_ADDED_SUCCESS : FacilitiesConstants.FACILITIES_UPDATED_SUCCESS ;
		customRedirector( msg, FacilitiesConstants.FACILITIES_LIST_PAGE);		
	}

	/**
	 * This method to returns to facilities view page
	 * @return 
	 */
	public void cancelAction(){
		if(facilities.getId()==null){
			redirector(FacilitiesConstants.FACILITIES_LIST_PAGE);
		}else{
			redirector(FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier());
		}
	}
	
	/**
	 * Filter company name based on company ID
	 * @param index
	 * @return
	 */
	public String filterClients(String index){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(index)){
					insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName());
					if(!"".equals(CommonUtils.getValidString(c.getCompany())) && CommonConstants.FALSE.equals(c.getType())){
						//insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName())+" ("+getCompanyName(c.getCompany())+")";
						insName = insName+ " "+" ("+getCompanyName(c.getCompany())+")";
					}
			break;	
			}
		}
		return insName;
	}

	private String getCompanyName(String id){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(id)){
				insName = c.getFirstName();
			break;	
			}
		}
		return insName;
	}
	
	public Facilities getFacilities() {
		return facilities;
	}


	public void setFacilities(Facilities facilities) {
		this.facilities = facilities;
	}


	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}


	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


	public String getFacilitiesId() {
		return facilitiesId;
	}


	public void setFacilitiesId(String facilitiesId) {
		this.facilitiesId = facilitiesId;
	}


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}

	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}

	public List<Crm> getCrmList() {
		return crmList;
	}

	public void setCrmList(List<Crm> crmList) {
		this.crmList = crmList;
	}

	public List<MaintenanceAct> getMaintenanceActList() {
		return maintenanceActList;
	}

	public void setMaintenanceActList(List<MaintenanceAct> maintenanceActList) {
		this.maintenanceActList = maintenanceActList;
	}

	public List<String> getYearList() {
		return yearList;
	}

	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	public List<String> getQualificationsNameList() {
		return qualificationsNameList;
	}

	public void setQualificationsNameList(List<String> qualificationsNameList) {
		this.qualificationsNameList = qualificationsNameList;
	}

	public List<MultiVal> getWeekList() {
		return weekList;
	}

	public void setWeekList(List<MultiVal> weekList) {
		this.weekList = weekList;
	}

	public List<String> getVisitingDayList() {
		return visitingDayList;
	}

	public void setVisitingDayList(List<String> visitingDayList) {
		this.visitingDayList = visitingDayList;
	}

	public boolean isLatitudeBool() {
		return latitudeBool;
	}

	public void setLatitudeBool(boolean latitudeBool) {
		this.latitudeBool = latitudeBool;
	}

	public boolean isLongitudeBool() {
		return longitudeBool;
	}

	public void setLongitudeBool(boolean longitudeBool) {
		this.longitudeBool = longitudeBool;
	}

	public Map<String, String> getMonthMap() {
		return monthMap;
	}

	public void setMonthMap(Map<String, String> monthMap) {
		this.monthMap = monthMap;
	}

	public List<String> getMonthMapList() {
		return monthMapList;
	}

	public void setMonthMapList(List<String> monthMapList) {
		this.monthMapList = monthMapList;
	}
	
	//Selva added for visitingTime at 04/06/18
	public List<MultiVal> getTimeList() {
		return timeList;
	}

	public void setTimeList(List<MultiVal> timeList) {
		this.timeList = timeList;
	}

	public VisitingDayTimeHours getDayTimeHours() {
		return dayTimeHours;
	}

	public void setDayTimeHours(VisitingDayTimeHours dayTimeHours) {
		this.dayTimeHours = dayTimeHours;
	}

	public List<VisitingDayTimeHours> getVisitingDayTimeHoursList() {
		return visitingDayTimeHoursList;
	}

	public void setVisitingDayTimeHoursList(List<VisitingDayTimeHours> visitingDayTimeHoursList) {
		this.visitingDayTimeHoursList = visitingDayTimeHoursList;
	}

		
	
}

/*if(!CommonUtils.getValidString(facilities.getDegreeOfLatitude()).isEmpty()
|| !CommonUtils.getValidString(facilities.getDegreeOfLongitude()).isEmpty()){
String regex = "^([0-8]?[0-9]|90)�(\\s[0-5]?[0-9]')?(\\s[0-5]?[0-9](,[0-9])?"+(char)34+")?$";
Pattern compiledPatternLat = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
Pattern compiledPatternLon = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
Matcher matcherLat = compiledPatternLat.matcher(CommonUtils.getValidString(facilities.getDegreeOfLatitude()));
Matcher matcherLon = compiledPatternLon.matcher(CommonUtils.getValidString(facilities.getDegreeOfLongitude()));
if(!CommonUtils.getValidString(facilities.getDegreeOfLatitude()).isEmpty()){
if(!matcherLat.find()){
   latitudeBool = true;
}else{
   latitudeBool = false;
}
}else{
latitudeBool = false;
}
if(!CommonUtils.getValidString(facilities.getDegreeOfLongitude()).isEmpty()){
if(!matcherLon.find()){
   longitudeBool = true;
}else{
   longitudeBool = false;
}
}else{
longitudeBool = false;
}
}
if(latitudeBool && longitudeBool){
commonError.addError(getJSFMessage("invalid_latlong_value"));
return;
}else if(latitudeBool){
commonError.addError(getJSFMessage("invalid_latitude_value"));
return;
}else if(longitudeBool){
commonError.addError(getJSFMessage("invalid_longitude_value"));
return;
}*/
