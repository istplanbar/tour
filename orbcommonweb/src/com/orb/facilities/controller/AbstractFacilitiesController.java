package com.orb.facilities.controller;
/**
 * @author IST planbar / KaSe
 * 
 */

import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.service.CrmService;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.service.FacilitiesService;
import com.orb.qualifications.service.QualificationsService;

public class AbstractFacilitiesController extends AbstractCommonController{
	private static Log logger = LogFactory.getLog(AbstractFacilitiesController.class);

	@Autowired
	protected FacilitiesService facilitiesService;
	
	@Autowired
	protected QualificationsService qualificationsService;
	
	@Autowired 
	protected CrmService crmService;
	
	private Map<String, String>  localConf = null;
	
	public Facilities convertAddressToLatLng(Facilities facility){
		try{
			localConf = (Map<String, String>)getSessionObj(CommonConstants.ADMIN_CONF);
			 StringBuffer address = new StringBuffer(facility.getLocationStreet());
			 String key = "&key="+localConf.get("googleAPIKey");
			 
			 if(!CommonUtils.getValidString(facility.getLocationHouseNumber()).isEmpty()){
				 address.append(" "+facility.getLocationHouseNumber());
			 }
			 if(!CommonUtils.getValidString(facility.getLocationPlace()).isEmpty()){
				 address.append(";"+facility.getLocationPlace());
			 }
			 if(!CommonUtils.getValidString(facility.getLocationPostCode()).isEmpty()){
				 address.append(";"+facility.getLocationPostCode());
			 }
			/* if(!CommonUtils.getValidString(facility.getLocationDistrict()).isEmpty()){
				 address.append(";"+facility.getLocationDistrict());
			 }*/
			 
			 //String addr = "Sonnenweg 6;99425;";
			 String s = "https://maps.google.com/maps/api/geocode/json?" + "sensor=false&language=de&components=country:DE&address=";
			 s += URLEncoder.encode(address.toString(), "UTF-8");
			 s += key;
			 URL url = new URL(s);
			
			 // read from the URL
			 Scanner scan = new Scanner(url.openStream());
			 String str = new String();
			 while (scan.hasNext()){
			     str += scan.nextLine();
			 }
			 scan.close();
			
			 // build a JSON object
			 JSONObject obj = new JSONObject(str);
			 if (! obj.getString("status").equals("OK")){
				 logger.info("Error on location to latlng Conversion"+obj.toString());
				 commonError.addError(getJSFMessage("error_on_address_conversion"));
			    }
			
			 // get the first result
			 JSONObject res = obj.getJSONArray("results").getJSONObject(0);
			 //System.out.println(res.getString("formatted_address"));
			 JSONObject loc = res.getJSONObject("geometry").getJSONObject("location");
			 //System.out.println("lat: " + loc.getDouble("lat") + ", lng: " + loc.getDouble("lng"));
			 facility.setFacilityLatitude(loc.getDouble("lat"));
			 facility.setFacilityLongitude(loc.getDouble("lng"));
			 
		 }catch(Exception e){
			 logger.info("Error on converting address to Lat and Long."+e.getMessage());
			 commonError.addError(getJSFMessage("error_on_address_conversion"));
		 }
		 
	return facility;
	}
	
	
}
