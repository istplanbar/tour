package com.orb.facilities.controller;
/**
 * @author IST planbar / KaSe
 * 
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.crm.bean.Crm;
import com.orb.facilities.bean.Document;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.bean.TourFacilities;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.facilities.constants.FacilitiesConstants;
import com.orb.qualifications.bean.Qualifications;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.tour.bean.Tour;
import com.orb.tour.constants.TourConstants;

@ManagedBean(name="facilitiesView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/facilities")
public class FacilitiesListViewManagedBean extends AbstractFacilitiesController{
	private static Log logger = LogFactory.getLog(FacilitiesListViewManagedBean.class);
	
	private User loginUser=null;
	private List<Boolean> togglerList = null;
	private List<Boolean> maintenanceTogglerList = null;
	private Facilities facilities = null;
	private List<Facilities> facilitiesList = null;
	private List<Facilities> filteredFacilitiesList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
	private String showActivateBtn = CommonConstants.FALSE;
	private List<Facilities> selectedFacilitiesList = null;
	private List<Qualifications> qualificationsList = null;
	private String facilitiesId = null;
	private String showIdentifier = CommonConstants.TRUE;
	private List<Crm> crmList = null;
	private List<MaintenanceAct> maintenanceActList = null;
	private List<MultiVal> weekList = null;
	private MaintenanceAct maintenanceAct =null;
	private MaintenanceControl maintenanceControl =null;
	private MaintenanceSubstance maintenanceSubstance =null;
	private List<MaintenanceAct> filteredMaintenanceActList = null;
	private List<MaintenanceAct> selectedMaintenanceActList = null;
	private String showMaintenanceActiveListBtn = CommonConstants.FALSE;
	private String showMaintenanceActivateBtn = CommonConstants.FALSE;
	private Map<String, String> monthMap = null;
	private String showDocumentActiveListBtn = CommonConstants.FALSE;
	private String showDocumentActivateBtn = CommonConstants.FALSE;
	private UploadedFile file;
	private List<String> uploadedFiles = new ArrayList<String>();
	private List<Boolean> togglerDocumentList = null;
	private Document document = null;
	private List<Document> documentList = null;
	private List<Document> selectedDocument = null;
	private List<Document> filteredDocumentList = null;
	private String editChange = CommonConstants.FALSE;
	//private Map<String, String> estimatedWorkHoursList = null;
	private VisitingDayTimeHours dayTimeHours = null;
	private List<VisitingDayTimeHours> visitingDayTimeHoursList = null;
	private List<VisitingDayTimeHours> subVisitingDayTimeHoursList = null;
	private List<TourFacilities> facilityTourList = null;
	private List<Task> facilityTaskList = null;
	 /**
	 * This page will be called when user clicks the facilities link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadFacilities")
    public String loadFacilities(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return FacilitiesConstants.FACILITIES_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular Facilities from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewFacilities")
	public String viewFacilities(HttpServletRequest request, ModelMap model){
		 facilitiesId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( FacilitiesConstants.SELECTED_FACILITIES_ID, facilitiesId );
		 return FacilitiesConstants.FACILITIES_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		//estimatedWorkHoursList = (Map<String,String>)FacilitiesConstants.ESTIMATED_WORK_HOURS;
		monthMap = new HashMap<>();
		facilitiesList = new ArrayList<>();
		maintenanceActList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		facilitiesId = CommonUtils.getValidString(getJSFRequestAttribute(FacilitiesConstants.SELECTED_FACILITIES_ID) );
		logger.info(" Facilities Id :" +facilitiesId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		String showMaintenanceActive = CommonUtils.getValidString(getJSFRequestAttribute(FacilitiesConstants.MAINTENANCE_ACTIVE_FLAG));
		String showDocumentActive = CommonUtils.getValidString((String) getSessionObj( FacilitiesConstants.SHOW_ACTIVE_DOCUMENT ));
		crmList = crmService.getActiveCrmList(commonError);
		qualificationsList = qualificationsService.getActiveQualificationsList(commonError);
		subVisitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHours(commonError, facilitiesId);
		
		
		
		visitingDayTimeHoursList = facilitiesService.getVisitingTimeAndHoursList(dayTimeHours, 7);
		weekList = getMultiValues( commonError, CommonConstants.MULTI_VAL_FILED_WEEK_DAY );
		if(qualificationsList == null) qualificationsList = new ArrayList<>();
		if(crmList == null) crmList = new ArrayList<>();
		monthMap = (Map<String,String>)FacilitiesConstants.MAINTENACE_CYCLE_MONTH_NAME;
		//maintenanceActList=facilitiesService.getActiveMaintenanceActList(commonError);
		
		//maintenanceAct = facilitiesService.getMaintenanceAct(commonError, facilitiesId);
		//maintenanceControl =facilitiesService.getMaintenanceControl(commonError, facilitiesId);
		//maintenanceSubstance =facilitiesService.getMaintenanceSubstance(commonError, facilitiesId);
		
		if(CommonConstants.N.equals(showActive)){
			facilitiesList=facilitiesService.getInactiveFacilitiesList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			facilitiesList=facilitiesService.getActiveFacilitiesList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(FacilitiesConstants.FACILITIES_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true, true, true, true, true, true, false);
		}
		if(!CommonUtils.getValidString(facilitiesId).isEmpty()){
			maintenanceAct = new MaintenanceAct();
			maintenanceControl =new MaintenanceControl();
			maintenanceSubstance =new MaintenanceSubstance();
			facilities = facilitiesService.getFacilities(commonError, facilitiesId);
			facilityTourList = facilitiesService.getTourDataList(commonError, facilitiesId);
			facilityTaskList = facilitiesService.getTaskDataList(commonError, facilities.getId());
			
			if(!CommonUtils.getValidString(facilities.getMaintenanceCycle()).isEmpty() && 
					!CommonUtils.getValidString(facilities.getMonthOfMaintenance()).isEmpty()){
				facilities.setMonthOfMaintenance(getMaintenanceMonths(facilities.getMonthOfMaintenance()));
			}
			
			if(CommonConstants.N.equals(showMaintenanceActive)){
				maintenanceActList = facilitiesService.getInactiveMaintenanceActList(commonError, facilitiesId);
				showMaintenanceActivateBtn = CommonConstants.TRUE;
				showMaintenanceActiveListBtn = CommonConstants.TRUE;
			}else{
				maintenanceActList=facilitiesService.getActiveMaintenanceActList(commonError, facilitiesId);
			}
			
			if(CommonConstants.N.equals(showDocumentActive)){
				documentList = facilitiesService.getInactiveDocumentList(commonError, facilitiesId);
				showDocumentActivateBtn = CommonConstants.TRUE;
				showDocumentActiveListBtn = CommonConstants.TRUE;
			}else{
				documentList = facilitiesService.getActiveDocumentList(commonError, facilitiesId);
			}
			
			maintenanceTogglerList = (List<Boolean>)getSessionObj(FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST);
			if(maintenanceTogglerList==null){ maintenanceTogglerList = Arrays.asList(false, true, true); }
			togglerDocumentList = (List<Boolean>)getSessionObj(FacilitiesConstants.DOCUMENT_TOGGLER_LIST) != null ? 
					(List<Boolean>)getSessionObj(FacilitiesConstants.DOCUMENT_TOGGLER_LIST) : Arrays.asList(false, true, true, true,true);
			
			if( maintenanceActList == null) maintenanceActList = new ArrayList<>();
			if( documentList == null) documentList = new ArrayList<>();
		}
		
	}
	
	
	private String getMaintenanceMonths(String monthOfMaintenance) {
		List<String> monthList = Arrays.asList(monthOfMaintenance.split(","));
		StringBuilder months = new StringBuilder(); 
		for(String key: monthMap.keySet()){
			if(monthList.contains(key)){
				months.append(monthMap.get(key));
				months.append(",");
			}
		}
		months.setLength(months.length()-1);
		return months.toString();
	}


	/**
	 * This method will get all the active maintenanceAct
	 * @param event
	 */
	public void showMaintenanceActActive(ActionEvent event){
		maintenanceActList = facilitiesService.getActiveMaintenanceActList( commonError, facilitiesId);
		addSessionObj(FacilitiesConstants.MAINTENANCE_ACTIVE_FLAG, CommonConstants.Y);
		if( filteredMaintenanceActList != null ) filteredMaintenanceActList.clear();
		showMaintenanceActivateBtn = CommonConstants.FALSE;
		showMaintenanceActiveListBtn = CommonConstants.FALSE;
	}
	
	
	/**
	 * This method will show all the inactive MaintenanceAct
	 * @param event
	 */
	public void showMaintenanceActInactive(ActionEvent event){
		maintenanceActList = facilitiesService.getInactiveMaintenanceActList(commonError, facilitiesId);
		if( filteredMaintenanceActList != null ) filteredMaintenanceActList.clear();
		showMaintenanceActivateBtn = CommonConstants.TRUE;
		showMaintenanceActiveListBtn = CommonConstants.TRUE;
	}
	
	
	
	/**
	 * This method will get all the active Facilities
	 * @param event
	 */
	public void showActive(ActionEvent event){
		facilitiesList = facilitiesService.getActiveFacilitiesList( commonError );
		if( filteredFacilitiesList != null ) filteredFacilitiesList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Facilities
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		facilitiesList = facilitiesService.getInactiveFacilitiesList(commonError);
		if( filteredFacilitiesList != null ) filteredFacilitiesList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks Facilities
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent e){
		facilitiesService.lockFacilities( commonError, loginUser, selectedFacilitiesList );
		showActive(e);
		customRedirector( FacilitiesConstants.FACILITIES_LOCKED_SUCCESS,FacilitiesConstants.FACILITIES_LIST_PAGE);
	}
	
	/**
	 * This method locks maintenanceAct
	 * @param list page
	 * @return 
	 */
	public void lockMaintenanceActAction(ActionEvent event){
		facilitiesService.lockMaintenanceAct( commonError, loginUser, selectedMaintenanceActList );
		showMaintenanceActActive(event);
		if(selectedMaintenanceActList != null) selectedMaintenanceActList.clear();
		//customRedirector( FacilitiesConstants.MAINTENANCE_ACT_LOCKED_SUCCESS,FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
	}
	
	/**
	 * This method unlocks MaintenanceAct
	 * @param list page
	 * @return 
	 */
	public void unlockMaintenanceActAction(ActionEvent event){
		facilitiesService.unlockMaintenanceAct( commonError, loginUser, selectedMaintenanceActList );
		showMaintenanceActInactive(event);
		if(selectedMaintenanceActList != null) selectedMaintenanceActList.clear();
		//customRedirector( FacilitiesConstants.MAINTENANCE_ACT_UNLOCKED_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Facilities
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent e){
		facilitiesService.unlockFacilities( commonError, loginUser, selectedFacilitiesList );
		showInactive(e);
		customRedirector( FacilitiesConstants.FACILITIES_UNLOCKED_SUCCESS, FacilitiesConstants.FACILITIES_LIST_PAGE);
	}
	
	/**
	 * This method will delete the Facilities
	 * @param event
	 */
	public void deleteAction(ActionEvent e){
		if(selectedFacilitiesList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(FacilitiesConstants.FACILITIES_DELETE_SUCCESS);
		}else{	
			facilitiesService.deleteFacilities( commonError, loginUser, selectedFacilitiesList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( FacilitiesConstants.FACILITIES_DELETE_SUCCESS, FacilitiesConstants.FACILITIES_LIST_PAGE);
		    }
		    showInactive(e);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will delete the MaintenanceAct
	 * @param event
	 */
	public void deleteMaintenanceActAction(ActionEvent event){
		if(selectedMaintenanceActList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS);
		}else{
		    facilitiesService.deleteMaintenanceAct( commonError, loginUser, selectedMaintenanceActList);
		    /*if( commonError.getError().isEmpty() ){
		        customRedirector( FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS, FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE);
		    }*/
		    showMaintenanceActInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	/**
	 * This method will get all the active Document
	 * @param event
	 */
	public void showDocumentActive(ActionEvent event){
		documentList=facilitiesService.getActiveDocumentList(commonError,facilitiesId);
		addSessionObj(FacilitiesConstants.SHOW_ACTIVE_DOCUMENT, CommonConstants.Y);
		if( filteredDocumentList != null ) filteredDocumentList.clear();
		showDocumentActivateBtn = CommonConstants.FALSE;
		showDocumentActiveListBtn = CommonConstants.FALSE;
	}

	/**
	 * This method will show all the inactive Document
	 * @param event
	 */
	public void showDocumentInactive(ActionEvent event){
		documentList=facilitiesService.getInactiveDocumentList(commonError,facilitiesId);
		if( filteredDocumentList != null ) filteredDocumentList.clear();
		showDocumentActivateBtn = CommonConstants.TRUE;
		showDocumentActiveListBtn = CommonConstants.TRUE;
	}
	
	/**
	 * This method locks Document
	 * @param list page
	 * @return 
	 */
	public void lockDocumentAction(ActionEvent e){
		facilitiesService.lockDocument( commonError, loginUser, selectedDocument );
		showDocumentActive(e);
	}
		
	/**
	 * This method unlocks Document
	 * @param list page
	 * @return 
	 */
	public void unlockDocumentAction(ActionEvent e){
		facilitiesService.unlockDocument( commonError, loginUser, selectedDocument );
		showDocumentInactive(e);
	}
	
	/**
	 * This method will delete the document
	 * @param event
	 * @throws IOException 
	 */
	public void deleteDocumentAction(ActionEvent e) throws IOException{
		
		deleteFileList(selectedDocument);
		if(!selectedDocument.isEmpty()){
			for(Document d : selectedDocument){
				try {
					deleteFile(d.getAttachment());
				} catch (IOException ex) {
					commonError.addError("err_docNotFound");
					logger.info("Erro not found"+ex.getMessage());
				}
			}
			facilitiesService.deleteDocument( commonError, loginUser, selectedDocument );
			if( commonError.getError().isEmpty() ){
				customRedirector(FacilitiesConstants.DOCUMENT_DELETE_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier()  );
			}
			showDocumentInactive(e);
			commonError.clearErrors();
		}else{
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(FacilitiesConstants.DOCUMENT_DELETE_SUCCESS);
		}
	}
	
	
	/**
	 * This method locks viewed document
	 * @param list page
	 * @return
	 */
	public void lockViewedDocument(String index){
		document = facilitiesService.getDocument(commonError, index);
		facilitiesService.lockViewedDocument( commonError, loginUser, document );
		customRedirector( FacilitiesConstants.DOCUMENT_LOCKED_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier());
	}

	/**
	 * This method unlocks viewed document
	 * @param list page
	 * @return
	 */
	public void unlockViewedDocument(String index){
		document = facilitiesService.getDocument(commonError, index);
		facilitiesService.unlockViewedDocument( commonError, loginUser, document );
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.DOCUMENT_UNLOCKED_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier() );
		}
	}

	/**
	 * Delete viewed document
	 * @param index
	 * @throws IOException 
	 */
	public void deleteViewedDocument(String index) throws IOException{
		document = facilitiesService.getDocument(commonError, index);
		if(document.getAttachment() != null){
			try {
				deleteFile(document.getAttachment());
			} catch (IOException e) {
				commonError.addError("err_docNotFound");
				logger.info("Erro not found"+e.getMessage());
			}
		}
		facilitiesService.deleteViewedDocument( commonError, loginUser, document);
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.DOCUMENT_DELETE_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier() );
		}
	}
	
	public void onDocumentToggle(ToggleEvent e) {
		togglerDocumentList.set((Integer) e.getData(), e.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method will add a document in DB
	 * @param event
	 */
	public void saveDocument(ActionEvent event){
		if(uploadedFiles.isEmpty()){
			commonError.addError(getJSFMessage("Upload_file_required"));
			return;
		}
		String fileNames = String.join(",", uploadedFiles);
		
		document.setAttachment(fileNames);
		if( "".equals(CommonUtils.getValidString(document.getId())) ){
			facilitiesService.addDocument( commonError, loginUser,document,facilitiesId );
		}else{
			facilitiesService.updateDocument( commonError, loginUser,document,facilitiesId );
		}
		String msg = "".equals(CommonUtils.getValidString(document.getId())) ? FacilitiesConstants.DOCUMENT_ADDED_SUCCESS : FacilitiesConstants.DOCUMENT_UPDATED_SUCCESS;
		customRedirector(msg,FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilities.getIdentifier() );
	}
	
	/**
	 * Adding new document dialog
	 */
	public void addDocument(){
		document = new Document();
		uploadedFiles = new ArrayList<>();
		RequestContext.getCurrentInstance().execute("createDocumentDialog.show()");
	}
	
	/**
	 * view the already saved document
	 * @param index
	 */
	public void documentView(String index){
		document = facilitiesService.getDocument(commonError, index);
		if(!"".equals(document.getAttachment())){
			uploadedFiles = new ArrayList<String>(Arrays.asList(document.getAttachment().split(",")));
		}else
			uploadedFiles = new ArrayList<String>();
		
		RequestContext.getCurrentInstance().execute("updateDocumentDialog.show()");
	}
	
	/**
	 * Delete the file which is selected
	 * @param file
	 * @throws IOException
	 */
	public void deleteFile(String file) throws IOException{
		String path = System.getProperty("upload.dir") + "facilityDocuments/" + facilities.getIdentifier()+"/" +file +"/";
		File f = new File(path);
		if(f.exists()){
			f.delete();
			uploadedFiles.remove(file);
		}else{
		commonError.addError("err_docNotFound");
	    }	
	}
	
	/**
	 * Delete the file which is selected
	 * 
	 * @param file
	 * @throws IOException
	 */
	public void deleteFileList(List<Document> docs) throws IOException {
		
		for (Document d : docs) {
		String path = System.getProperty("upload.dir") + "facilityDocuments/"+ facilities.getIdentifier()+"/" +d.getAttachment() +"/";
		File f = new File(path);
		if (f.exists()) {
			f.delete();
		} else {
			commonError.addError("err_docNotFound");
		}
		}
	}
	
	/**
	 * This method process the file upload
	 * @param file
	 * @param fileName
	 */
	public void fileUploadListener(FileUploadEvent fileUploadEvent) throws IOException{
		UploadedFile uploadedFile = fileUploadEvent.getFile();
		file = fileUploadEvent.getFile();
		String filePath = null;	
		 byte[] bytes = null;
		 uploadedFiles = new ArrayList<>();
		 if (null != uploadedFile) {
			 filePath = System.getProperty("upload.dir") + "facilityDocuments/"  + facilities.getIdentifier()+"/" ;
			 File dir = new File(filePath);
		     if(!dir.exists())  dir.mkdir();;
             bytes = uploadedFile.getContents();
             String fileName = new String(uploadedFile.getFileName().getBytes("iso-8859-1"),"UTF-8");
             uploadedFiles.add(fileName);
             BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath+fileName)));
             stream.write(bytes);
             stream.close();
         }
   }
	
	/**
	 * This method helps to download the Document
	 * @param model
	 */
	public void downloadFile(String fileVersion) throws IOException{

		String path = System.getProperty("upload.dir") + "facilityDocuments/" + facilities.getIdentifier()+ "/" + fileVersion;
		File downFile = new File(path);
		Path filePath = Paths.get(path);
		if(Files.exists(filePath) && !"".equals(fileVersion)){
			String fileName = downFile.getName();
			InputStream fis = new FileInputStream(downFile);
			
			byte[] buf = new byte[fis.available()];
			int offset = 0;
			int numRead = 0;
			while ((offset < buf.length) && ((numRead = fis.read(buf, offset, buf.length -offset)) >= 0)) 
				offset += numRead;
			fis.close();
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
			String contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(path);
			response.setContentType(contentType);
			response.setHeader( "Content-Disposition", "attachment;filename=\"" + fileName + "\"" );
			response.getOutputStream().write(buf);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			FacesContext.getCurrentInstance().responseComplete();
			commonError.clearErrors();
		}else{
			commonError.addError("err_docNotFound");
		}			
		
		
	}
	
	
	/**
	 * This method will clear a document from file list
	 * @param event
	 */
	public void clearDocumentList(ActionEvent event){
		uploadedFiles.clear();
	}
	
	/**
	 * This method will show  type in residence
	 * @param event
	 */
	public void changeType(ActionEvent e){
		//String value = (String) ((UIOutput)e.getSource()).getValue();
		String value = CommonConstants.FALSE;
		if(CommonConstants.FALSE.equals(value)){
		 editChange = CommonConstants.TRUE;
		}else{
		 editChange = CommonConstants.FALSE;
		}
	}	
	
	/**
	 * This method will navigate to the page where user can add a facilities
	 */
	public void addAction(){
		addSessionObj( FacilitiesConstants.FACILITIES_TOGGLER_LIST , togglerList );
		redirector(FacilitiesConstants.FACILITIES_EDIT_PAGE);
	}
	/**
	 * This method to returns to main facilities list page
	 */
	public void cancelAction(){
		redirector(FacilitiesConstants.FACILITIES_LIST_PAGE +"?activeFlag="+facilities.getActive());
	}
	
	/**
	 * This method will navigate to facilities Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(FacilitiesConstants.FACILITIES_EDIT_PAGE+"?objectId="+index);
	}
	
	
	/**
	 * This method will navigate to facilities View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( FacilitiesConstants.FACILITIES_TOGGLER_LIST , togglerList );
	    addSessionObj( FacilitiesConstants.MAINTENANCE_ACT_TOGGLER_LIST , maintenanceTogglerList );
		redirector(FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed facilities
	 * @param index
	 */
	public void deleteViewedFacilities(String index){
		facilities = facilitiesService.getFacilities(commonError, index);
		facilitiesService.deleteViewedFacilities( commonError, loginUser, facilities);
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.FACILITIES_DELETE_SUCCESS, FacilitiesConstants.FACILITIES_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed facilities
	 */
	public void lockViewedFacilities(String index){
		facilities = facilitiesService.getFacilities(commonError, index);
		facilitiesService.lockViewedFacilities( commonError, loginUser, facilities );
		customRedirector( FacilitiesConstants.FACILITIES_LOCKED_SUCCESS, FacilitiesConstants.FACILITIES_LIST_PAGE);
	}

	/**
	 * This method locks viewed facilities
	 * @param index
	 * @return
	 */
	public void unlockViewedFacilities(String index){
		facilities = facilitiesService.getFacilities(commonError, index);
		facilitiesService.unlockViewedFacilities( commonError, loginUser, facilities );
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.FACILITIES_UNLOCKED_SUCCESS, FacilitiesConstants.FACILITIES_LIST_PAGE );
		}
	}

	/**
	 * Filter company name based on company ID
	 * @param index
	 * @return
	 */
	public String filterClients(String index){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(index)){
					insName = CommonUtils.getValidString(c.getTitle())+" "+c.getFirstName()+" "+CommonUtils.getValidString(c.getSurName());
					if(!"".equals(CommonUtils.getValidString(c.getCompany())) && CommonConstants.FALSE.equals(c.getType())){
						insName = insName+" ("+getCompanyName(c.getCompany())+")";
					}
			break;	
			}
		}
		return insName;
	}

	private String getCompanyName(String id){
		String insName= "" ;
		for(Crm c :crmList){
			if(c.getId().equals(id)){
				insName = c.getFirstName();
			break;	
			}
		}
		return insName;
	}
	
	
	public String filterMaintenanceAct(String maintainId){
		String maintenanceName= "" ;
		for(MaintenanceAct m :maintenanceActList){
			if(m.getId().equals(maintainId)){
				maintenanceName = m.getReferenceNumber();
			break;	
			}
		}
		return maintenanceName;
	}
	
	
	public String getQualificationName(String id){
		String tempName = null;
		if(id!=null){
		  List<String> tempQualificationList = qualificationsList.stream().filter(q -> Arrays.asList(id.split(",")).contains(q.getId())).map(n -> n.getQualificationName()).collect(Collectors.toList());
		  tempName = tempQualificationList.stream().map(x -> x).collect(Collectors.joining(", "));
		}
		return tempName;
	} 
	
	public String getVisitingDay(String id){
		String tempDay = null;
		if(id!=null){
		  List<String> tempWeekDayList = weekList.stream().filter(q -> id.contains(q.getId())).map(n -> n.getValue()).collect(Collectors.toList());
		  tempDay = tempWeekDayList.stream().map(x -> x).collect(Collectors.joining(", "));
		}
		return tempDay;
	}
	
	/* This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	
	public void onToggleMaintenance(ToggleEvent event){
		maintenanceTogglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method will add / update a maintenanceAct in DB
	 * @param event
	 */
	public void saveMainteanceActAction(ActionEvent event){
		if(CommonUtils.getValidString(maintenanceAct.getId()).isEmpty()){
			facilitiesService.add( commonError, loginUser, facilitiesId, maintenanceAct,maintenanceControl,maintenanceSubstance );
		}else{
			facilitiesService.update( commonError, loginUser, maintenanceAct,maintenanceControl,maintenanceSubstance );
		}
		
		String msg=CommonUtils.getValidString(maintenanceAct.getId()).isEmpty() ? FacilitiesConstants.MAINTENANCE_ACT_ADDED_SUCCESS : FacilitiesConstants.MAINTENANCE_ACT_UPDATED_SUCCESS ;
		customRedirector( msg, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilitiesId);		
	}
	
	/**
	 * This method to returns to main maintenanceAct list page
	 */
	public void cancelMaintenanceAction(){
		redirector(FacilitiesConstants.MAINTENANCE_ACT_LIST_PAGE +"?activeFlag="+maintenanceAct.getActive());
	}
	
	/**
	 * This method will navigate to maintenanceAct Edit page
	 * @param index
	 */
	public void editMaintenanceAction(String index){
		maintenanceAct=facilitiesService.getMaintenanceAct(commonError, index);
		maintenanceControl =facilitiesService.getMaintenanceControl(commonError, index);
		maintenanceSubstance =facilitiesService.getMaintenanceSubstance(commonError, index);
	    RequestContext.getCurrentInstance().execute("createMaintenanceDialog.show()");
	}
	
	/**
	 * This method will navigate to maintenanceAct View page
	 * @param index
	 */
	public void viewMaintenanceAction(String index){
		maintenanceAct=facilitiesService.getMaintenanceAct(commonError, index);
		maintenanceControl =facilitiesService.getMaintenanceControl(commonError, index);
		maintenanceSubstance =facilitiesService.getMaintenanceSubstance(commonError, index);
	    RequestContext.getCurrentInstance().execute("updateMaintenanceDialog.show()");
		//redirector(FacilitiesConstants.MAINTENANCE_ACT_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed maintenanceAct
	 * @param index
	 */
	public void deleteViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
		facilitiesService.deleteViewedMaintenanceAct( commonError, loginUser, maintenanceAct);
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.MAINTENANCE_ACT_DELETE_SUCCESS,  FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilitiesId );
		}
	}
	
	/**
	 * This method locks viewed maintenanceAct
	 */
	public void lockViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
	    facilitiesService.lockViewedMaintenanceAct( commonError, loginUser, maintenanceAct );
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.MAINTENANCE_ACT_LOCKED_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilitiesId);
		}
	}

	/**
	 * This method locks viewed maintenanceAct
	 * @param index
	 * @return
	 */
	public void unlockViewedMaintenanceAct(String index){
		maintenanceAct = facilitiesService.getMaintenanceAct(commonError, index);
		facilitiesService.unlockViewedMaintenanceAct( commonError, loginUser, maintenanceAct );
		if( commonError.getError().isEmpty() ){
			customRedirector( FacilitiesConstants.MAINTENANCE_ACT_UNLOCKED_SUCCESS, FacilitiesConstants.FACILITIES_VIEW_PAGE+"?objectId="+facilitiesId);
		}
	}

	public void clearView(){
		maintenanceAct = new MaintenanceAct();
		RequestContext.getCurrentInstance().execute("createMaintenanceDialog.show()");
	}
	
	
	/**
	 * This method will navigate to tour View page
	 * @param index
	 */
	public void viewTourAction(String index){
		redirector(TourConstants.TOUR_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to tour View page
	 * @param index
	 */
	public void viewTaskAction(String index){
		redirector(TaskPlannerConstants.TASK_VIEW_PAGE + "?objectId=" + index+"&view=" +TaskPlannerConstants.TASK_LIST_VIEW);
	}
	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( FacilitiesConstants.SELECTED_FACILITIES_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( FacilitiesConstants.SELECTED_FACILITIES_ID );
		if( toggleList != null ) model.remove( FacilitiesConstants.FACILITIES_TOGGLER_LIST );		
    }
	
	
	public User getLoginUser() {
		return loginUser;
	}
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}
	public List<Boolean> getTogglerList() {
		return togglerList;
	}
	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}
	public Facilities getFacilities() {
		return facilities;
	}
	public void setFacilities(Facilities facilities) {
		this.facilities = facilities;
	}
	public List<Facilities> getFacilitiesList() {
		return facilitiesList;
	}
	public void setFacilitiesList(List<Facilities> facilitiesList) {
		this.facilitiesList = facilitiesList;
	}
	public List<Facilities> getFilteredFacilitiesList() {
		return filteredFacilitiesList;
	}
	public void setFilteredFacilitiesList(List<Facilities> filteredFacilitiesList) {
		this.filteredFacilitiesList = filteredFacilitiesList;
	}
	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}
	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}
	public String getShowActivateBtn() {
		return showActivateBtn;
	}
	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}
	public List<Facilities> getSelectedFacilitiesList() {
		return selectedFacilitiesList;
	}
	public void setSelectedFacilitiesList(List<Facilities> selectedFacilitiesList) {
		this.selectedFacilitiesList = selectedFacilitiesList;
	}
	public String getFacilitiesId() {
		return facilitiesId;
	}
	public void setFacilitiesId(String facilitiesId) {
		this.facilitiesId = facilitiesId;
	}
	public String getShowIdentifier() {
		return showIdentifier;
	}
	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}
	public List<Crm> getCrmList() {
		return crmList;
	}
	public void setCrmList(List<Crm> crmList) {
		this.crmList = crmList;
	}


	public List<MultiVal> getWeekList() {
		return weekList;
	}


	public void setWeekList(List<MultiVal> weekList) {
		this.weekList = weekList;
	}


	public List<Boolean> getMaintenanceTogglerList() {
		return maintenanceTogglerList;
	}


	public void setMaintenanceTogglerList(List<Boolean> maintenanceTogglerList) {
		this.maintenanceTogglerList = maintenanceTogglerList;
	}


	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}


	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}


	public List<MaintenanceAct> getMaintenanceActList() {
		return maintenanceActList;
	}


	public void setMaintenanceActList(List<MaintenanceAct> maintenanceActList) {
		this.maintenanceActList = maintenanceActList;
	}


	public MaintenanceAct getMaintenanceAct() {
		return maintenanceAct;
	}


	public void setMaintenanceAct(MaintenanceAct maintenanceAct) {
		this.maintenanceAct = maintenanceAct;
	}


	public MaintenanceControl getMaintenanceControl() {
		return maintenanceControl;
	}


	public void setMaintenanceControl(MaintenanceControl maintenanceControl) {
		this.maintenanceControl = maintenanceControl;
	}


	public MaintenanceSubstance getMaintenanceSubstance() {
		return maintenanceSubstance;
	}


	public void setMaintenanceSubstance(MaintenanceSubstance maintenanceSubstance) {
		this.maintenanceSubstance = maintenanceSubstance;
	}


	public List<MaintenanceAct> getFilteredMaintenanceActList() {
		return filteredMaintenanceActList;
	}


	public void setFilteredMaintenanceActList(List<MaintenanceAct> filteredMaintenanceActList) {
		this.filteredMaintenanceActList = filteredMaintenanceActList;
	}


	public List<MaintenanceAct> getSelectedMaintenanceActList() {
		return selectedMaintenanceActList;
	}


	public void setSelectedMaintenanceActList(List<MaintenanceAct> selectedMaintenanceActList) {
		this.selectedMaintenanceActList = selectedMaintenanceActList;
	}


	public String getShowMaintenanceActiveListBtn() {
		return showMaintenanceActiveListBtn;
	}


	public void setShowMaintenanceActiveListBtn(String showMaintenanceActiveListBtn) {
		this.showMaintenanceActiveListBtn = showMaintenanceActiveListBtn;
	}


	public String getShowMaintenanceActivateBtn() {
		return showMaintenanceActivateBtn;
	}


	public void setShowMaintenanceActivateBtn(String showMaintenanceActivateBtn) {
		this.showMaintenanceActivateBtn = showMaintenanceActivateBtn;
	}

	public List<Document> getSelectedDocument() {
		return selectedDocument;
	}

	public void setSelectedDocument(List<Document> selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public List<Document> getFilteredDocumentList() {
		return filteredDocumentList;
	}

	public void setFilteredDocumentList(List<Document> filteredDocumentList) {
		this.filteredDocumentList = filteredDocumentList;
	}

	public List<Document> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<Document> documentList) {
		this.documentList = documentList;
	}
	
	public String getShowDocumentActiveListBtn() {
		return showDocumentActiveListBtn;
	}

	public void setShowDocumentActiveListBtn(String showDocumentActiveListBtn) {
		this.showDocumentActiveListBtn = showDocumentActiveListBtn;
	}

	public String getShowDocumentActivateBtn() {
		return showDocumentActivateBtn;
	}

	public void setShowDocumentActivateBtn(String showDocumentActivateBtn) {
		this.showDocumentActivateBtn = showDocumentActivateBtn;
	}


	public List<String> getUploadedFiles() {
		return uploadedFiles;
	}


	public void setUploadedFiles(List<String> uploadedFiles) {
		this.uploadedFiles = uploadedFiles;
	}


	public List<Boolean> getTogglerDocumentList() {
		return togglerDocumentList;
	}


	public void setTogglerDocumentList(List<Boolean> togglerDocumentList) {
		this.togglerDocumentList = togglerDocumentList;
	}


	public Document getDocument() {
		return document;
	}


	public void setDocument(Document document) {
		this.document = document;
	}


	public String getEditChange() {
		return editChange;
	}


	public void setEditChange(String editChange) {
		this.editChange = editChange;
	}


	public VisitingDayTimeHours getDayTimeHours() {
		return dayTimeHours;
	}


	public void setDayTimeHours(VisitingDayTimeHours dayTimeHours) {
		this.dayTimeHours = dayTimeHours;
	}



	public List<VisitingDayTimeHours> getSubVisitingDayTimeHoursList() {
		return subVisitingDayTimeHoursList;
	}


	public void setSubVisitingDayTimeHoursList(List<VisitingDayTimeHours> subVisitingDayTimeHoursList) {
		this.subVisitingDayTimeHoursList = subVisitingDayTimeHoursList;
	}


	public List<VisitingDayTimeHours> getVisitingDayTimeHoursList() {
		return visitingDayTimeHoursList;
	}


	public void setVisitingDayTimeHoursList(List<VisitingDayTimeHours> visitingDayTimeHoursList) {
		this.visitingDayTimeHoursList = visitingDayTimeHoursList;
	}


	public List<TourFacilities> getFacilityTourList() {
		return facilityTourList;
	}


	public void setFacilityTourList(List<TourFacilities> facilityTourList) {
		this.facilityTourList = facilityTourList;
	}


	public List<Task> getFacilityTaskList() {
		return facilityTaskList;
	}


	public void setFacilityTaskList(List<Task> facilityTaskList) {
		this.facilityTaskList = facilityTaskList;
	}


	 

}
