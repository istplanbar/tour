package com.orb.blank.controller;

import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.controller.AbstractCommonController;

@ManagedBean(name="blankView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/blank")
public class BlankPageManagedBean extends AbstractCommonController{

	@RequestMapping("/loadBlankPage")
    public String startCalendar(HttpServletRequest httpRequest, ModelMap model, Locale locale){
    	return "blank/blank.xhtml";
    }
}
