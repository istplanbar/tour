package com.orb.qualifications.controller;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.qualifications.bean.Qualifications;
import com.orb.qualifications.constants.QualificationsConstants;


@ManagedBean(name="qualificationsView")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/qualifications")
public class QualificationsListViewManagedBean extends AbstractQualificationsController {
	private static Log logger = LogFactory.getLog(QualificationsListViewManagedBean.class);
	
	private User loginUser=null;
    private List<Boolean> togglerList = null;
    private Qualifications qualifications = null;
    private List<Qualifications> qualificationsList = null;
	private List<Qualifications> filteredQualificationsList = null;
	private String showActiveListBtn = CommonConstants.FALSE;
    private String showActivateBtn = CommonConstants.FALSE;
	private List<Qualifications> selectedQualificationsList = null;
	private String qualificationsId = null;
	private String showIdentifier = CommonConstants.TRUE;
		 
    /**
	 * This page will be called when user clicks the qualifications link on the menu.
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/loadQualifications")
    public String loadQualifications(HttpServletRequest request,ModelMap model){
		clearSessionObjects(model);
		String activeFlag = CommonUtils.getValidString( request.getParameter(CommonConstants.ACTIVE_FLAG) );
		model.addAttribute( CommonConstants.ACTIVE_FLAG, activeFlag );
		return QualificationsConstants.QUALIFICATION_LIST_HTML;
    }
	
	
	/**
	 * This method will be called when user clicks particular Qualifications from list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/viewQualifications")
	public String viewQualifications(HttpServletRequest request, ModelMap model){
		 qualificationsId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
	     model.addAttribute( QualificationsConstants.SELECTED_QUALIFICATION_ID, qualificationsId );
		 return QualificationsConstants.QUALIFICATION_VIEW_HTML;
	}
	
	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	@SuppressWarnings("unchecked")
	public void onLoadAction(){
		qualificationsList = new ArrayList<>();
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		qualificationsId = CommonUtils.getValidString(getJSFRequestAttribute(QualificationsConstants.SELECTED_QUALIFICATION_ID) );
		logger.info(" QualificationsId :" +qualificationsId);
		String showActive = CommonUtils.getValidString(getJSFRequestAttribute(CommonConstants.ACTIVE_FLAG));
		if(CommonConstants.N.equals(showActive)){
			qualificationsList=qualificationsService.getInactiveQualificationsList(commonError);
			setShowActivateBtn(CommonConstants.TRUE);
			setShowActiveListBtn(CommonConstants.TRUE);
		}else{
			qualificationsList=qualificationsService.getActiveQualificationsList(commonError);
		}
		togglerList = (List<Boolean>)getSessionObj(QualificationsConstants.QUALIFICATION_TOGGLER_LIST);
		if(togglerList==null){
			 togglerList = Arrays.asList(false, true, true);
		}
		if(!CommonUtils.getValidString(qualificationsId).isEmpty()){
			qualifications = qualificationsService.getQualifications(commonError, qualificationsId);
		}
	}
	
	/**
	 * This method will get all the active qualifications
	 * @param event
	 */
	public void showActive(ActionEvent event){
		qualificationsList = qualificationsService.getActiveQualificationsList( commonError );
		if( filteredQualificationsList != null ) filteredQualificationsList.clear();
		setShowActivateBtn(CommonConstants.FALSE);
		setShowActiveListBtn(CommonConstants.FALSE);
	}
	
	
	/**
	 * This method will show all the inactive Qualifications
	 * @param event
	 */
	public void showInactive(ActionEvent event){
		qualificationsList = qualificationsService.getInactiveQualificationsList(commonError);
		if( filteredQualificationsList != null ) filteredQualificationsList.clear();
		setShowActivateBtn(CommonConstants.TRUE);
		setShowActiveListBtn(CommonConstants.TRUE);
	}

	
	/**
	 * This method locks qualifications
	 * @param list page
	 * @return 
	 */
	public void lockAction(ActionEvent event){
		qualificationsService.lockQualifications( commonError, loginUser, selectedQualificationsList );
		showActive(event);
		customRedirector( QualificationsConstants.QUALIFICATION_LOCKED_SUCCESS,QualificationsConstants.QUALIFICATION_LIST_PAGE);
	}
	
	/**
	 * This method unlocks Qualifications
	 * @param list page
	 * @return 
	 */
	public void unlockAction(ActionEvent event){
		qualificationsService.unlockQualifications( commonError, loginUser, selectedQualificationsList );
		showInactive(event);
		customRedirector( QualificationsConstants.QUALIFICATION_UNLOCKED_SUCCESS, QualificationsConstants.QUALIFICATION_LIST_PAGE);
	}
	
	
	/**
	 * This method will delete the Qualifications
	 * @param event
	 */
	public void deleteAction(ActionEvent event){
		if(selectedQualificationsList.isEmpty()){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			errorMessage(QualificationsConstants.QUALIFICATION_DELETE_SUCCESS);
		}else{	
			qualificationsService.deleteQualifications( commonError, loginUser, selectedQualificationsList);
		    if( commonError.getError().isEmpty() ){
		        customRedirector( QualificationsConstants.QUALIFICATION_DELETE_SUCCESS, QualificationsConstants.QUALIFICATION_LIST_PAGE);
		    }
		    showInactive(event);
		    commonError.clearErrors();
	    }
	}
	
	
	/**
	 * This method will navigate to the page where user can add a qualifications
	 */
	public void addAction(){
		addSessionObj( QualificationsConstants.QUALIFICATION_TOGGLER_LIST , togglerList );
		redirector(QualificationsConstants.QUALIFICATION_EDIT_PAGE);
	}
	
	/**
	 * This method to returns to main qualifications list page
	 */
	public void cancelAction(){
		redirector(QualificationsConstants.QUALIFICATION_LIST_PAGE +"?activeFlag="+qualifications.getActive());
	}
	
	/**
	 * This method will navigate to qualifications Edit page
	 * @param index
	 */
	public void editAction(String index){
		redirector(QualificationsConstants.QUALIFICATION_EDIT_PAGE+"?objectId="+index);
	}
	
	/**
	 * This method will navigate to qualifications View page
	 * @param index
	 */
	public void viewAction(String index){
	    addSessionObj( QualificationsConstants.QUALIFICATION_TOGGLER_LIST , togglerList );
		redirector(QualificationsConstants.QUALIFICATION_VIEW_PAGE+"?objectId="+index);
	}
	
	/**
	 * Delete viewed qualifications
	 * @param index
	 */
	public void deleteViewedQualifications(String index){
		qualifications = qualificationsService.getQualifications(commonError, index);
		qualificationsService.deleteViewedQualifications( commonError, loginUser, qualifications);
		if( commonError.getError().isEmpty() ){
			customRedirector( QualificationsConstants.QUALIFICATION_DELETE_SUCCESS, QualificationsConstants.QUALIFICATION_LIST_PAGE );
		}
	}
	
	/**
	 * This method locks viewed qualifications
	 */
	public void lockViewedQualifications(String index){
		qualifications = qualificationsService.getQualifications(commonError, index);
		qualificationsService.lockViewedQualifications( commonError, loginUser, qualifications );
		customRedirector( QualificationsConstants.QUALIFICATION_LOCKED_SUCCESS, QualificationsConstants.QUALIFICATION_LIST_PAGE);
	}

	/**
	 * This method locks viewed qualifications
	 * @param index
	 * @return
	 */
	public void unlockViewedQualifications(String index){
		qualifications = qualificationsService.getQualifications(commonError, index);
		qualificationsService.unlockViewedQualifications( commonError, loginUser, qualifications );
		if( commonError.getError().isEmpty() ){
			customRedirector( QualificationsConstants.QUALIFICATION_UNLOCKED_SUCCESS, QualificationsConstants.QUALIFICATION_LIST_PAGE );
		}
	}

	/**
	 * This method will be called when user click the toggle to hide the column  
	 * @param event
	 * @return
	 */
	public void onToggle(ToggleEvent event){
        togglerList.set((Integer) event.getData(), event.getVisibility() == Visibility.VISIBLE);
    }
	
	/**
	 * This method called to clear the session when application exit
	 * @param model
	 */
	private void clearSessionObjects(ModelMap model) {
		String id = (String)model.get( QualificationsConstants.SELECTED_QUALIFICATION_ID );
		List toggleList = (List)model.get( togglerList );
		if( id != null ) model.remove( QualificationsConstants.SELECTED_QUALIFICATION_ID );
		if( toggleList != null ) model.remove( QualificationsConstants.QUALIFICATION_TOGGLER_LIST );		
    }


	public User getLoginUser() {
		return loginUser;
	}


	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}


	public List<Boolean> getTogglerList() {
		return togglerList;
	}


	public void setTogglerList(List<Boolean> togglerList) {
		this.togglerList = togglerList;
	}


	public Qualifications getQualifications() {
		return qualifications;
	}


	public void setQualifications(Qualifications qualifications) {
		this.qualifications = qualifications;
	}


	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}


	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}


	public List<Qualifications> getFilteredQualificationsList() {
		return filteredQualificationsList;
	}


	public void setFilteredQualificationsList(List<Qualifications> filteredQualificationsList) {
		this.filteredQualificationsList = filteredQualificationsList;
	}


	public String getShowActiveListBtn() {
		return showActiveListBtn;
	}


	public void setShowActiveListBtn(String showActiveListBtn) {
		this.showActiveListBtn = showActiveListBtn;
	}


	public String getShowActivateBtn() {
		return showActivateBtn;
	}


	public void setShowActivateBtn(String showActivateBtn) {
		this.showActivateBtn = showActivateBtn;
	}


	public List<Qualifications> getSelectedQualificationsList() {
		return selectedQualificationsList;
	}


	public void setSelectedQualificationsList(List<Qualifications> selectedQualificationsList) {
		this.selectedQualificationsList = selectedQualificationsList;
	}


	public String getQualificationsId() {
		return qualificationsId;
	}


	public void setQualificationsId(String qualificationsId) {
		this.qualificationsId = qualificationsId;
	}


	public String getShowIdentifier() {
		return showIdentifier;
	}


	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}


	
		 
}
