package com.orb.qualifications.controller;
/**
 * @author Manikandan Chithirangan
 * 
 */
import org.springframework.beans.factory.annotation.Autowired;

import com.orb.common.client.controller.AbstractCommonController;
import com.orb.qualifications.service.QualificationsService;

public abstract class AbstractQualificationsController extends AbstractCommonController {
	@Autowired
	protected QualificationsService qualificationsService;
	

}
