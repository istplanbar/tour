package com.orb.qualifications.controller;
/**
 * @author Manikandan Chithirangan
 * 
 */
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.qualifications.bean.Qualifications;
import com.orb.qualifications.constants.QualificationsConstants;

@ManagedBean(name="qualificationsAddUpdate")
@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@RequestMapping("/process/qualifications")
public class QualificationsAddUpdateManagedBean extends AbstractQualificationsController {
	private static Log logger = LogFactory.getLog(QualificationsAddUpdateManagedBean.class);
	  
	private Qualifications qualifications =null;
	private List<Qualifications> qualificationsList = null;
	private String showIdentifier = CommonConstants.TRUE;
	private String qualificationsId = null;
	private User loginUser = null;
	
	/**
	 * This method will be called when user clicks Add button / qualifications Id link on list page
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/manipulateQualifications")
	public String manipulateQualifications( HttpServletRequest request, ModelMap model){
		qualificationsId = CommonUtils.getValidString( request.getParameter( CommonConstants.OBJECT_ID ) );
		model.addAttribute( QualificationsConstants.SELECTED_QUALIFICATION_ID, qualificationsId );
		return QualificationsConstants.QUALIFICATION_EDIT_HTML;
	}	

	/**
	 * This method will be called when the XHTML loaded from Spring call.,
	 */
	public void onLoadAction(){
		loginUser = (User)getSessionObj( CommonConstants.USER_IN_CONTEXT );
		qualificationsId = getJSFRequestAttribute( QualificationsConstants.SELECTED_QUALIFICATION_ID );
		logger.info(" Qualifications Id :" +qualificationsId);
		if(CommonUtils.getValidString(qualificationsId).isEmpty()){
			qualifications = new Qualifications();
			showIdentifier=CommonConstants.FALSE;
		}else{
			qualifications=qualificationsService.getQualifications(commonError, qualificationsId);
		 }
	}

	/**
	 * This method will add / update a qualifications in DB
	 * @param event
	 */
	public void saveAction(ActionEvent event){
		if(CommonUtils.getValidString(qualifications.getId()).isEmpty()){
			qualificationsService.add( commonError, loginUser, qualifications );
		}else{
			qualificationsService.update( commonError, loginUser, qualifications );
		}
		
		String msg=CommonUtils.getValidString(qualifications.getId()).isEmpty() ? QualificationsConstants.QUALIFICATION_ADDED_SUCCESS : QualificationsConstants.QUALIFICATION_UPDATED_SUCCESS ;
		customRedirector( msg, QualificationsConstants.QUALIFICATION_LIST_PAGE);		
	}


	/**
	 * This method to returns to qualifications view page
	 * @return 
	 */
	public void cancelAction(){
		if(qualifications.getId()==null){
			redirector(QualificationsConstants.QUALIFICATION_LIST_PAGE);
		}else{
			redirector(QualificationsConstants.QUALIFICATION_VIEW_PAGE+"?objectId="+qualifications.getIdentifier());
		}
	}

	public Qualifications getQualifications() {
		return qualifications;
	}

	public void setQualifications(Qualifications qualifications) {
		this.qualifications = qualifications;
	}

	public List<Qualifications> getQualificationsList() {
		return qualificationsList;
	}

	public void setQualificationsList(List<Qualifications> qualificationsList) {
		this.qualificationsList = qualificationsList;
	}

	public String getShowIdentifier() {
		return showIdentifier;
	}

	public void setShowIdentifier(String showIdentifier) {
		this.showIdentifier = showIdentifier;
	}

	public String getQualificationsId() {
		return qualificationsId;
	}

	public void setQualificationsId(String qualificationsId) {
		this.qualificationsId = qualificationsId;
	}

	public User getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	

}
