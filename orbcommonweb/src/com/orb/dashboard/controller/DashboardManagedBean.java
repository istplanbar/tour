package com.orb.dashboard.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DashboardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orb.admin.client.error.AdminError;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.service.UserManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.controller.AbstractCommonController;
import com.orb.common.client.model.User;

@Controller
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
@ViewScoped
@ManagedBean(name = "dashboardMBean")
@RequestMapping("/process/dashboard")
public class DashboardManagedBean extends AbstractCommonController {
	private static Log logger = LogFactory.getLog(DashboardManagedBean.class);
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private AdminError adminError;
	
	@Autowired 
	private UserManagementService userManagementService;
	
	private DashboardModel model;
	private List<MetaTable> mainModules = null;
	private User user;
	private List<Map<String,List<MetaTable>>> roleModMapList = null;
	private List<Role> userRoleList = null;

	@RequestMapping("/loadDashboard")
	public String loadDashboard(){
		return "dashboard/dashboard.xhtml";
	}
	
	public void onLoadAction(){
		mainModules = new ArrayList<>(); 
		roleModMapList = new ArrayList<>();
		userRoleList = new ArrayList<>();
		user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		if(user != null)
    		adminService.sendNotificationsToUsers(commonError,user);
		mainModules = adminService.getMainModuleList( adminError, user );
	}

	@Override
	public void preRenderView(ComponentSystemEvent event)throws Exception{
		super.preRenderView(event);
		addErrorMessage(adminError);
		if( adminError != null ) adminError.clearErrors();
	}
	
	public void handleReorder(DashboardReorderEvent event) {
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		message.setSummary("Reordered: " + event.getWidgetId());
		message.setDetail("Item index: " + event.getItemIndex() + ", Column index: " + event.getColumnIndex() + ", Sender index: " + event.getSenderColumnIndex());

		addMessage(message);
	}

	public void handleClose(CloseEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Panel Closed", "Closed panel id:'" + event.getComponent().getId() + "'");

		addMessage(message);
	}

	public void handleToggle(ToggleEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());

		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public DashboardModel getModel() {
		return model;
	}

	public List<MetaTable> getMainModules() {
		return mainModules;
	}

	public void setMainModules(List<MetaTable> mainModules) {
		this.mainModules = mainModules;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Map<String, List<MetaTable>>> getRoleModMapList() {
		return roleModMapList;
	}

	public void setRoleModMapList(List<Map<String, List<MetaTable>>> roleModMapList) {
		this.roleModMapList = roleModMapList;
	}

	public List<Role> getUserRoleList() {
		return userRoleList;
	}

	public void setUserRoleList(List<Role> userRoleList) {
		this.userRoleList = userRoleList;
	}
	

}