package org.owasp.esapi.reference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.SecurityConfiguration;
import org.owasp.esapi.errors.ConfigurationException;

public class DefaultSecurityConfiguration implements SecurityConfiguration {
	
	private static volatile SecurityConfiguration instance;
	private Properties properties = null;
	private String cipherXformFromESAPIProp = null;
	private String cipherXformCurrent = null;
	public static final String RESOURCE_FILE = "ESAPI.properties";
	public static final String REMEMBER_TOKEN_DURATION = "Authenticator.RememberTokenDuration";
	public static final String IDLE_TIMEOUT_DURATION = "Authenticator.IdleTimeoutDuration";
	public static final String ABSOLUTE_TIMEOUT_DURATION = "Authenticator.AbsoluteTimeoutDuration";
	public static final String ALLOWED_LOGIN_ATTEMPTS = "Authenticator.AllowedLoginAttempts";
	public static final String USERNAME_PARAMETER_NAME = "Authenticator.UsernameParameterName";
	public static final String PASSWORD_PARAMETER_NAME = "Authenticator.PasswordParameterName";
	public static final String MAX_OLD_PASSWORD_HASHES = "Authenticator.MaxOldPasswordHashes";
	public static final String ALLOW_MULTIPLE_ENCODING = "Encoder.AllowMultipleEncoding";
	public static final String ALLOW_MIXED_ENCODING = "Encoder.AllowMixedEncoding";
	public static final String CANONICALIZATION_CODECS = "Encoder.DefaultCodecList";
	public static final String DISABLE_INTRUSION_DETECTION = "IntrusionDetector.Disable";
	public static final String MASTER_KEY = "Encryptor.MasterKey";
	public static final String MASTER_SALT = "Encryptor.MasterSalt";
	public static final String KEY_LENGTH = "Encryptor.EncryptionKeyLength";
	public static final String ENCRYPTION_ALGORITHM = "Encryptor.EncryptionAlgorithm";
	public static final String HASH_ALGORITHM = "Encryptor.HashAlgorithm";
	public static final String HASH_ITERATIONS = "Encryptor.HashIterations";
	public static final String CHARACTER_ENCODING = "Encryptor.CharacterEncoding";
	public static final String RANDOM_ALGORITHM = "Encryptor.RandomAlgorithm";
	public static final String DIGITAL_SIGNATURE_ALGORITHM = "Encryptor.DigitalSignatureAlgorithm";
	public static final String DIGITAL_SIGNATURE_KEY_LENGTH = "Encryptor.DigitalSignatureKeyLength";
	public static final String PREFERRED_JCE_PROVIDER = "Encryptor.PreferredJCEProvider";
	public static final String CIPHER_TRANSFORMATION_IMPLEMENTATION = "Encryptor.CipherTransformation";
	public static final String CIPHERTEXT_USE_MAC = "Encryptor.CipherText.useMAC";
	public static final String PLAINTEXT_OVERWRITE = "Encryptor.PlainText.overwrite";
	public static final String IV_TYPE = "Encryptor.ChooseIVMethod";
	public static final String FIXED_IV = "Encryptor.fixedIV";
	public static final String COMBINED_CIPHER_MODES = "Encryptor.cipher_modes.combined_modes";
	public static final String ADDITIONAL_ALLOWED_CIPHER_MODES = "Encryptor.cipher_modes.additional_allowed";
	public static final String KDF_PRF_ALG = "Encryptor.KDF.PRF";
	public static final String PRINT_PROPERTIES_WHEN_LOADED = "ESAPI.printProperties";
	public static final String WORKING_DIRECTORY = "Executor.WorkingDirectory";
	public static final String APPROVED_EXECUTABLES = "Executor.ApprovedExecutables";
	public static final String FORCE_HTTPONLYSESSION = "HttpUtilities.ForceHttpOnlySession";
	public static final String FORCE_SECURESESSION = "HttpUtilities.SecureSession";
	public static final String FORCE_HTTPONLYCOOKIES = "HttpUtilities.ForceHttpOnlyCookies";
	public static final String FORCE_SECURECOOKIES = "HttpUtilities.ForceSecureCookies";
	public static final String MAX_HTTP_HEADER_SIZE = "HttpUtilities.MaxHeaderSize";
	public static final String UPLOAD_DIRECTORY = "HttpUtilities.UploadDir";
	public static final String UPLOAD_TEMP_DIRECTORY = "HttpUtilities.UploadTempDir";
	public static final String APPROVED_UPLOAD_EXTENSIONS = "HttpUtilities.ApprovedUploadExtensions";
	public static final String MAX_UPLOAD_FILE_BYTES = "HttpUtilities.MaxUploadFileBytes";
	public static final String RESPONSE_CONTENT_TYPE = "HttpUtilities.ResponseContentType";
	public static final String HTTP_SESSION_ID_NAME = "HttpUtilities.HttpSessionIdName";
	public static final String APPLICATION_NAME = "Logger.ApplicationName";
	public static final String LOG_LEVEL = "Logger.LogLevel";
	public static final String LOG_FILE_NAME = "Logger.LogFileName";
	public static final String MAX_LOG_FILE_SIZE = "Logger.MaxLogFileSize";
	public static final String LOG_ENCODING_REQUIRED = "Logger.LogEncodingRequired";
	public static final String LOG_APPLICATION_NAME = "Logger.LogApplicationName";
	public static final String LOG_SERVER_IP = "Logger.LogServerIP";
	public static final String VALIDATION_PROPERTIES = "Validator.ConfigurationFile";
	public static final String ACCEPT_LENIENT_DATES = "Validator.AcceptLenientDates";
	public static final int DEFAULT_MAX_LOG_FILE_SIZE = 10000000;
	protected final int MAX_REDIRECT_LOCATION = 1000;

	/** @deprecated */
	protected final int MAX_FILE_NAME_LENGTH = 1000;
	public static final String LOG_IMPLEMENTATION = "ESAPI.Logger";
	public static final String AUTHENTICATION_IMPLEMENTATION = "ESAPI.Authenticator";
	public static final String ENCODER_IMPLEMENTATION = "ESAPI.Encoder";
	public static final String ACCESS_CONTROL_IMPLEMENTATION = "ESAPI.AccessControl";
	public static final String ENCRYPTION_IMPLEMENTATION = "ESAPI.Encryptor";
	public static final String INTRUSION_DETECTION_IMPLEMENTATION = "ESAPI.IntrusionDetector";
	public static final String RANDOMIZER_IMPLEMENTATION = "ESAPI.Randomizer";
	public static final String EXECUTOR_IMPLEMENTATION = "ESAPI.Executor";
	public static final String VALIDATOR_IMPLEMENTATION = "ESAPI.Validator";
	public static final String HTTP_UTILITIES_IMPLEMENTATION = "ESAPI.HTTPUtilities";
	public static final String DEFAULT_LOG_IMPLEMENTATION = "org.owasp.esapi.reference.JavaLogFactory";
	public static final String DEFAULT_AUTHENTICATION_IMPLEMENTATION = "org.owasp.esapi.reference.FileBasedAuthenticator";
	public static final String DEFAULT_ENCODER_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultEncoder";
	public static final String DEFAULT_ACCESS_CONTROL_IMPLEMENTATION = "org.owasp.esapi.reference.accesscontrol.DefaultAccessController";
	public static final String DEFAULT_ENCRYPTION_IMPLEMENTATION = "org.owasp.esapi.reference.crypto.JavaEncryptor";
	public static final String DEFAULT_INTRUSION_DETECTION_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultIntrusionDetector";
	public static final String DEFAULT_RANDOMIZER_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultRandomizer";
	public static final String DEFAULT_EXECUTOR_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultExecutor";
	public static final String DEFAULT_HTTP_UTILITIES_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultHTTPUtilities";
	public static final String DEFAULT_VALIDATOR_IMPLEMENTATION = "org.owasp.esapi.reference.DefaultValidator";
	private static final Map<String, Pattern> patternCache;
	private static final String userHome;
	private static String customDirectory;
	private String resourceDirectory = ".esapi";

	public static SecurityConfiguration getInstance() {
		if (instance == null) {
			synchronized (DefaultSecurityConfiguration.class) {
				if (instance == null) {
					instance = new DefaultSecurityConfiguration();
				}
			}
		}
		return instance;
	}

	public DefaultSecurityConfiguration() {
		try {
			loadConfiguration();
			setCipherXProperties();
		} catch (IOException e) {
			logSpecial("Failed to load security configuration", e);
			throw new ConfigurationException(
					"Failed to load security configuration", e);
		}
	}

	public DefaultSecurityConfiguration(Properties properties) {
		this.properties = properties;
		setCipherXProperties();
	}

	private void setCipherXProperties() {
		this.cipherXformFromESAPIProp = getESAPIProperty(
				"Encryptor.CipherTransformation", "AES/CBC/PKCS5Padding");

		this.cipherXformCurrent = this.cipherXformFromESAPIProp;
	}

	public String getApplicationName() {
		return getESAPIProperty("Logger.ApplicationName", "DefaultName");
	}

	public String getLogImplementation() {
		return getESAPIProperty("ESAPI.Logger",
				"org.owasp.esapi.reference.JavaLogFactory");
	}

	public String getAuthenticationImplementation() {
		return getESAPIProperty("ESAPI.Authenticator",
				"org.owasp.esapi.reference.FileBasedAuthenticator");
	}

	public String getEncoderImplementation() {
		return getESAPIProperty("ESAPI.Encoder",
				"org.owasp.esapi.reference.DefaultEncoder");
	}

	public String getAccessControlImplementation() {
		return getESAPIProperty("ESAPI.AccessControl",
				"org.owasp.esapi.reference.accesscontrol.DefaultAccessController");
	}

	public String getEncryptionImplementation() {
		return getESAPIProperty("ESAPI.Encryptor",
				"org.owasp.esapi.reference.crypto.JavaEncryptor");
	}

	public String getIntrusionDetectionImplementation() {
		return getESAPIProperty("ESAPI.IntrusionDetector",
				"org.owasp.esapi.reference.DefaultIntrusionDetector");
	}

	public String getRandomizerImplementation() {
		return getESAPIProperty("ESAPI.Randomizer",
				"org.owasp.esapi.reference.DefaultRandomizer");
	}

	public String getExecutorImplementation() {
		return getESAPIProperty("ESAPI.Executor",
				"org.owasp.esapi.reference.DefaultExecutor");
	}

	public String getHTTPUtilitiesImplementation() {
		return getESAPIProperty("ESAPI.HTTPUtilities",
				"org.owasp.esapi.reference.DefaultHTTPUtilities");
	}

	public String getValidationImplementation() {
		return getESAPIProperty("ESAPI.Validator",
				"org.owasp.esapi.reference.DefaultValidator");
	}

	public byte[] getMasterKey() {
		byte[] key = getESAPIPropertyEncoded("Encryptor.MasterKey", null);
		if ((key == null) || (key.length == 0)) {
			throw new ConfigurationException(
					"Property 'Encryptor.MasterKey' missing or empty in ESAPI.properties file.");
		}

		return key;
	}

	public void setResourceDirectory(String dir) {
		this.resourceDirectory = dir;
		logSpecial("Reset resource directory to: " + dir, null);
		try {
			loadConfiguration();
		} catch (IOException e) {
			logSpecial("Failed to load security configuration from " + dir, e);
		}
	}

	public int getEncryptionKeyLength() {
		return getESAPIProperty("Encryptor.EncryptionKeyLength", 128);
	}

	public byte[] getMasterSalt() {
		byte[] salt = getESAPIPropertyEncoded("Encryptor.MasterSalt", null);
		if ((salt == null) || (salt.length == 0)) {
			throw new ConfigurationException(
					"Property 'Encryptor.MasterSalt' missing or empty in ESAPI.properties file.");
		}

		return salt;
	}

	public List<String> getAllowedExecutables() {
		String def = "";
		String[] exList = getESAPIProperty("Executor.ApprovedExecutables", def)
				.split(",");
		return Arrays.asList(exList);
	}

	public List<String> getAllowedFileExtensions() {
		String def = ".zip,.pdf,.tar,.gz,.xls,.properties,.txt,.xml";
		String[] extList = getESAPIProperty(
				"HttpUtilities.ApprovedUploadExtensions", def).split(",");
		return Arrays.asList(extList);
	}

	public int getAllowedFileUploadSize() {
		return getESAPIProperty("HttpUtilities.MaxUploadFileBytes", 5000000);
	}

	private Properties loadPropertiesFromStream(InputStream is, String name)
			throws IOException {
		Properties config = new Properties();
		try {
			config.load(is);
			logSpecial("Loaded '" + name + "' properties file", null);
		} finally {
			if (is != null)
				try {
					is.close();
				} catch (Exception e) {
				}
		}
		return config;
	}

	protected void loadConfiguration() throws IOException {
		try {
			logSpecial("Attempting to load ESAPI.properties via file I/O.");
			this.properties = loadPropertiesFromStream(
					getResourceStream("ESAPI.properties"), "ESAPI.properties");
		} catch (Exception iae) {
			logSpecial("Loading ESAPI.properties via file I/O failed. Exception was: "
					+ iae);
			logSpecial("Attempting to load ESAPI.properties via the classpath.");
			try {
				this.properties = loadConfigurationFromClasspath("ESAPI.properties");
			} catch (Exception e) {
				logSpecial(
						"ESAPI.properties could not be loaded by any means. Fail.",
						e);
				throw new ConfigurationException(
						"ESAPI.properties could not be loaded by any means. Fail.",
						e);
			}

		}

		if (this.properties == null)
			return;
		String validationPropFileName = getESAPIProperty(
				"Validator.ConfigurationFile", "validation.properties");
		Properties validationProperties = null;

		patternCache.clear();
		try {
			logSpecial("Attempting to load " + validationPropFileName
					+ " via file I/O.");
			validationProperties = loadPropertiesFromStream(
					getResourceStream(validationPropFileName),
					validationPropFileName);
		} catch (Exception iae) {
			logSpecial("Loading " + validationPropFileName
					+ " via file I/O failed.");
			logSpecial("Attempting to load " + validationPropFileName
					+ " via the classpath.");
			try {
				validationProperties = loadConfigurationFromClasspath(validationPropFileName);
			} catch (Exception e) {
				logSpecial(validationPropFileName
						+ " could not be loaded by any means. fail.", e);
			}
		}

		if (validationProperties != null) {
			Iterator i = validationProperties.keySet().iterator();
			while (i.hasNext()) {
				String key = (String) i.next();
				String value = validationProperties.getProperty(key);
				this.properties.put(key, value);
			}
		}

		if (!(shouldPrintProperties()))
			return;
	}

	public InputStream getResourceStream(String filename) throws IOException {
		if (filename == null) {
			return null;
		}
		try {
			File f = getResourceFile(filename);
			if ((f != null) && (f.exists()))
				return new FileInputStream(f);
		} catch (Exception e) {
		}
		throw new FileNotFoundException();
	}

	public File getResourceFile(String filename) {
		logSpecial("Attempting to load " + filename
				+ " as resource file via file I/O.");

		if (filename == null) {
			logSpecial("Failed to load properties via FileIO. Filename is null.");
			return null;
		}

		File f = null;

		f = new File(customDirectory, filename);
		if ((customDirectory != null) && (f.canRead())) {
			logSpecial("Found in 'org.owasp.esapi.resources' directory: "
					+ f.getAbsolutePath());
			return f;
		}
		logSpecial("Not found in 'org.owasp.esapi.resources' directory or file not readable: "
				+ f.getAbsolutePath());

		URL fileUrl = ClassLoader.getSystemResource(this.resourceDirectory
				+ "/" + filename);
		if (fileUrl == null) {
			fileUrl = ClassLoader.getSystemResource("esapi/" + filename);
		}

		if (fileUrl != null) {
			String fileLocation = fileUrl.getFile();
			f = new File(fileLocation);
			if (f.exists()) {
				logSpecial("Found in SystemResource Directory/resourceDirectory: "
						+ f.getAbsolutePath());
				return f;
			}
			logSpecial("Not found in SystemResource Directory/resourceDirectory (this should never happen): "
					+ f.getAbsolutePath());
		} else {
			logSpecial("Not found in SystemResource Directory/resourceDirectory: "
					+ this.resourceDirectory + File.separator + filename);
		}

		String homeDir = userHome;
		if (homeDir == null) {
			homeDir = "";
		}

		f = new File(homeDir + "/.esapi", filename);
		if (f.canRead()) {
			logSpecial("[Compatibility] Found in 'user.home' directory: "
					+ f.getAbsolutePath());
			return f;
		}

		f = new File(homeDir + "/esapi", filename);
		if (f.canRead()) {
			logSpecial("Found in 'user.home' directory: " + f.getAbsolutePath());
			return f;
		}
		logSpecial("Not found in 'user.home' (" + homeDir + ") directory: "
				+ f.getAbsolutePath());

		return null;
	}

	private Properties loadConfigurationFromClasspath(String fileName)
    throws IllegalArgumentException
  {
    Properties result = null;
    InputStream in = null;

    ClassLoader[] loaders = { Thread.currentThread().getContextClassLoader(), ClassLoader.getSystemClassLoader(), super.getClass().getClassLoader() };

    String[] classLoaderNames = { "current thread context class loader", "system class loader", "class loader for DefaultSecurityConfiguration class" };

    ClassLoader currentLoader = null;
    for (int i = 0; i < loaders.length; ++i)
      if (loaders[i] != null) {
        currentLoader = loaders[i];
        try
        {
          String currentClasspathSearchLocation = "/ (root)";
          in = loaders[i].getResourceAsStream(fileName);

          if (in == null) {
            currentClasspathSearchLocation = this.resourceDirectory + "/";
            in = currentLoader.getResourceAsStream(this.resourceDirectory + "/" + fileName);
          }

          if (in == null) {
            currentClasspathSearchLocation = ".esapi/";
            in = currentLoader.getResourceAsStream(".esapi/" + fileName);
          }

          if (in == null) {
            currentClasspathSearchLocation = "esapi/";
            in = currentLoader.getResourceAsStream("esapi/" + fileName);
          }

          if (in == null) {
            currentClasspathSearchLocation = "resources/";
            in = currentLoader.getResourceAsStream("resources/" + fileName);
          }

          if (in != null) {
            result = new Properties();
            result.load(in);
            logSpecial("SUCCESSFULLY LOADED " + fileName + " via the CLASSPATH from '" + currentClasspathSearchLocation + "' using " + classLoaderNames[i] + "!");

           // jsr 30; break label375:
          }
        } catch (Exception e) {
          result = null;
        }
        finally {
          try {
            in.close();
          }
          catch (Exception e)
          {
          }
        }
      }
    if (result == null)
    {
      label375: throw new IllegalArgumentException("Failed to load ESAPI.properties as a classloader resource.");
    }

    return result;
  }

	private void logSpecial(String message, Throwable e) {
		StringBuffer msg = new StringBuffer(message);
		if (e != null) {
			msg.append(" Exception was: ").append(e.toString());
		}
	}

	private void logSpecial(String message) {
	}

	public String getPasswordParameterName() {
		return getESAPIProperty("Authenticator.PasswordParameterName",
				"password");
	}

	public String getUsernameParameterName() {
		return getESAPIProperty("Authenticator.UsernameParameterName",
				"username");
	}

	public String getEncryptionAlgorithm() {
		return getESAPIProperty("Encryptor.EncryptionAlgorithm", "AES");
	}

	public String getCipherTransformation() {
		assert (this.cipherXformCurrent != null) : "Current cipher transformation is null";
		return this.cipherXformCurrent;
	}

	public String setCipherTransformation(String cipherXform) {
		String previous = getCipherTransformation();
		if (cipherXform == null) {
			this.cipherXformCurrent = this.cipherXformFromESAPIProp;
		} else {
			assert (!(cipherXform.trim().equals(""))) : "Cipher transformation cannot be just white space or empty string";
			this.cipherXformCurrent = cipherXform;
		}
		return previous;
	}

	public boolean useMACforCipherText() {
		return getESAPIProperty("Encryptor.CipherText.useMAC", true);
	}

	public boolean overwritePlainText() {
		return getESAPIProperty("Encryptor.PlainText.overwrite", true);
	}

	public String getIVType() {
		String value = getESAPIProperty("Encryptor.ChooseIVMethod", "random");
		if ((value.equalsIgnoreCase("fixed"))
				|| (value.equalsIgnoreCase("random")))
			return value;
		if (value.equalsIgnoreCase("specified")) {
			throw new ConfigurationException(
					"'Encryptor.ChooseIVMethod=specified' is not yet implemented. Use 'fixed' or 'random'");
		}

		throw new ConfigurationException(value + " is illegal value for "
				+ "Encryptor.ChooseIVMethod"
				+ ". Use 'random' (preferred) or 'fixed'.");
	}

	public String getFixedIV() {
		if (getIVType().equalsIgnoreCase("fixed")) {
			String ivAsHex = getESAPIProperty("Encryptor.fixedIV", "");
			if ((ivAsHex == null) || (ivAsHex.trim().equals(""))) {
				throw new ConfigurationException(
						"Fixed IV requires property Encryptor.fixedIV to be set, but it is not.");
			}

			return ivAsHex;
		}

		throw new ConfigurationException("IV type not 'fixed' (set to '"
				+ getIVType() + "'), so no fixed IV applicable.");
	}

	public String getHashAlgorithm() {
		return getESAPIProperty("Encryptor.HashAlgorithm", "SHA-512");
	}

	public int getHashIterations() {
		return getESAPIProperty("Encryptor.HashIterations", 1024);
	}

	public String getKDFPseudoRandomFunction() {
		return getESAPIProperty("Encryptor.KDF.PRF", "HmacSHA256");
	}

	public String getCharacterEncoding() {
		return getESAPIProperty("Encryptor.CharacterEncoding", "UTF-8");
	}

	public boolean getAllowMultipleEncoding() {
		return getESAPIProperty("Encoder.AllowMultipleEncoding", false);
	}

	public boolean getAllowMixedEncoding() {
		return getESAPIProperty("Encoder.AllowMixedEncoding", false);
	}

	public List<String> getDefaultCanonicalizationCodecs() {
		List def = new ArrayList();
		def.add("org.owasp.esapi.codecs.HTMLEntityCodec");
		def.add("org.owasp.esapi.codecs.PercentCodec");
		def.add("org.owasp.esapi.codecs.JavaScriptCodec");
		return getESAPIProperty("Encoder.DefaultCodecList", def);
	}

	public String getDigitalSignatureAlgorithm() {
		return getESAPIProperty("Encryptor.DigitalSignatureAlgorithm",
				"SHAwithDSA");
	}

	public int getDigitalSignatureKeyLength() {
		return getESAPIProperty("Encryptor.DigitalSignatureKeyLength", 1024);
	}

	public String getRandomAlgorithm() {
		return getESAPIProperty("Encryptor.RandomAlgorithm", "SHA1PRNG");
	}

	public int getAllowedLoginAttempts() {
		return getESAPIProperty("Authenticator.AllowedLoginAttempts", 5);
	}

	public int getMaxOldPasswordHashes() {
		return getESAPIProperty("Authenticator.MaxOldPasswordHashes", 12);
	}

	public File getUploadDirectory() {
		String dir = getESAPIProperty("HttpUtilities.UploadDir", "UploadDir");
		return new File(dir);
	}

	public File getUploadTempDirectory() {
		String dir = getESAPIProperty("HttpUtilities.UploadTempDir",
				System.getProperty("java.io.tmpdir", "UploadTempDir"));

		return new File(dir);
	}

	public boolean getDisableIntrusionDetection() {
		String value = this.properties.getProperty("IntrusionDetector.Disable");
		return ("true".equalsIgnoreCase(value));
	}

	public SecurityConfiguration.Threshold getQuota(String eventName) {
		int count = getESAPIProperty("IntrusionDetector." + eventName
				+ ".count", 0);
		int interval = getESAPIProperty("IntrusionDetector." + eventName
				+ ".interval", 0);
		List actions = new ArrayList();
		String actionString = getESAPIProperty("IntrusionDetector." + eventName
				+ ".actions", "");
		if (actionString != null) {
			String[] actionList = actionString.split(",");
			actions = Arrays.asList(actionList);
		}
		if ((count > 0) && (interval > 0) && (actions.size() > 0)) {
			return new SecurityConfiguration.Threshold(eventName, count,
					interval, actions);
		}
		return null;
	}

	public int getLogLevel() {
		String level = getESAPIProperty("Logger.LogLevel", "WARNING");

		if (level.equalsIgnoreCase("OFF"))
			return 2147483647;
		if (level.equalsIgnoreCase("FATAL"))
			return 1000;
		if (level.equalsIgnoreCase("ERROR"))
			return 800;
		if (level.equalsIgnoreCase("WARNING"))
			return 600;
		if (level.equalsIgnoreCase("INFO"))
			return 400;
		if (level.equalsIgnoreCase("DEBUG"))
			return 200;
		if (level.equalsIgnoreCase("TRACE"))
			return 100;
		if (level.equalsIgnoreCase("ALL")) {
			return -2147483648;
		}

		logSpecial(
				"The LOG-LEVEL property in the ESAPI properties file has the unrecognized value: "
						+ level + ". Using default: WARNING", null);
		return 600;
	}

	public String getLogFileName() {
		return getESAPIProperty("Logger.LogFileName", "ESAPI_logging_file");
	}

	public int getMaxLogFileSize() {
		return getESAPIProperty("Logger.MaxLogFileSize", 10000000);
	}

	public boolean getLogEncodingRequired() {
		return getESAPIProperty("Logger.LogEncodingRequired", false);
	}

	public boolean getLogApplicationName() {
		return getESAPIProperty("Logger.LogApplicationName", true);
	}

	public boolean getLogServerIP() {
		return getESAPIProperty("Logger.LogServerIP", true);
	}

	public boolean getForceHttpOnlySession() {
		return getESAPIProperty("HttpUtilities.ForceHttpOnlySession", true);
	}

	public boolean getForceSecureSession() {
		return getESAPIProperty("HttpUtilities.SecureSession", true);
	}

	public boolean getForceHttpOnlyCookies() {
		return getESAPIProperty("HttpUtilities.ForceHttpOnlyCookies", true);
	}

	public boolean getForceSecureCookies() {
		return getESAPIProperty("HttpUtilities.ForceSecureCookies", true);
	}

	public int getMaxHttpHeaderSize() {
		return getESAPIProperty("HttpUtilities.MaxHeaderSize", 4096);
	}

	public String getResponseContentType() {
		return getESAPIProperty("HttpUtilities.ResponseContentType",
				"text/html; charset=UTF-8");
	}

	public String getHttpSessionIdName() {
		return getESAPIProperty("HttpUtilities.HttpSessionIdName", "JSESSIONID");
	}

	public long getRememberTokenDuration() {
		int days = getESAPIProperty("Authenticator.RememberTokenDuration", 14);
		return (86400000 * days);
	}

	public int getSessionIdleTimeoutLength() {
		int minutes = getESAPIProperty("Authenticator.IdleTimeoutDuration", 20);
		return (60000 * minutes);
	}

	public int getSessionAbsoluteTimeoutLength() {
		int minutes = getESAPIProperty("Authenticator.AbsoluteTimeoutDuration",
				20);
		return (60000 * minutes);
	}

	public Pattern getValidationPattern(String key) {
		String value = getESAPIProperty("Validator." + key, "");

		Pattern p = (Pattern) patternCache.get(value);
		if (p != null)
			return p;

		if ((value == null) || (value.equals("")))
			return null;
		try {
			Pattern q = Pattern.compile(value);
			patternCache.put(value, q);
			return q;
		} catch (PatternSyntaxException e) {
//			logSpecial("SecurityConfiguration for " + key
//					+ " not a valid regex in ESAPI.properties. Returning null",
//					null);
		}
		return null;
	}

	public File getWorkingDirectory() {
		String dir = getESAPIProperty("Executor.WorkingDirectory",
				System.getProperty("user.dir"));
		if (dir != null) {
			return new File(dir);
		}
		return null;
	}

	public String getPreferredJCEProvider() {
		return this.properties.getProperty("Encryptor.PreferredJCEProvider");
	}

	public List<String> getCombinedCipherModes() {
		List empty = new ArrayList();
		return getESAPIProperty("Encryptor.cipher_modes.combined_modes", empty);
	}

	public List<String> getAdditionalAllowedCipherModes() {
		List empty = new ArrayList();
		return getESAPIProperty("Encryptor.cipher_modes.additional_allowed",
				empty);
	}

	public boolean getLenientDatesAccepted() {
		return getESAPIProperty("Validator.AcceptLenientDates", false);
	}

	protected String getESAPIProperty(String key, String def) {
		String value = this.properties.getProperty(key);
		if (value == null) {
//			logSpecial("SecurityConfiguration for " + key
//					+ " not found in ESAPI.properties. Using default: " + def,
//					null);
			return def;
		}
		return value;
	}

	protected boolean getESAPIProperty(String key, boolean def) {
		String property = this.properties.getProperty(key);
		if (property == null) {
			logSpecial("SecurityConfiguration for " + key
					+ " not found in ESAPI.properties. Using default: " + def,
					null);
			return def;
		}
		if ((property.equalsIgnoreCase("true"))
				|| (property.equalsIgnoreCase("yes"))) {
			return true;
		}
		if ((property.equalsIgnoreCase("false"))
				|| (property.equalsIgnoreCase("no"))) {
			return false;
		}
//		logSpecial(
//				"SecurityConfiguration for "
//						+ key
//						+ " not either \"true\" or \"false\" in ESAPI.properties. Using default: "
//						+ def, null);
		return def;
	}

	protected byte[] getESAPIPropertyEncoded(String key, byte[] def) {
		String property = this.properties.getProperty(key);
		if (property == null) {
			logSpecial("SecurityConfiguration for " + key
					+ " not found in ESAPI.properties. Using default: " + def,
					null);
			return def;
		}
		try {
			return ESAPI.encoder().decodeFromBase64(property);
		} catch (IOException e) {
//			logSpecial(
//					"SecurityConfiguration for "
//							+ key
//							+ " not properly Base64 encoded in ESAPI.properties. Using default: "
//							+ def, null);
		}
		return null;
	}

	protected int getESAPIProperty(String key, int def) {
		String property = this.properties.getProperty(key);
		if (property == null) {
			logSpecial("SecurityConfiguration for " + key
					+ " not found in ESAPI.properties. Using default: " + def,
					null);
			return def;
		}
		try {
			return Integer.parseInt(property);
		} catch (NumberFormatException e) {
//			logSpecial("SecurityConfiguration for " + key
//					+ " not an integer in ESAPI.properties. Using default: "
//					+ def, null);
		}
		return def;
	}

	protected List<String> getESAPIProperty(String key, List<String> def) {
		String property = this.properties.getProperty(key);
		if (property == null) {
			logSpecial("SecurityConfiguration for " + key
					+ " not found in ESAPI.properties. Using default: " + def,
					null);
			return def;
		}
		String[] parts = property.split(",");
		return Arrays.asList(parts);
	}

	protected boolean shouldPrintProperties() {
		return getESAPIProperty("ESAPI.printProperties", false);
	}

	protected Properties getESAPIProperties() {
		return this.properties;
	}

	static {
		instance = null;

		patternCache = new HashMap();

		userHome = System.getProperty("user.home");

		customDirectory = System.getProperty("org.owasp.esapi.resources");
	}
}