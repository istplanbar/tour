package org.springframework.security.web.csrf;

import javax.servlet.http.HttpServletRequest;

public class CsrfUtil {
	public static final String getCurrentCSRFToken( HttpServletRequest request ){
		
		if(request.getSession() != null && request.getSession().getAttribute("csrfToken") != null){
			return request.getSession().getAttribute("csrfToken").toString();
		}else{
			CsrfTokenRepository csrfTokenRepository = new HttpSessionCsrfTokenRepository();
			CsrfToken csrfToken = csrfTokenRepository.loadToken(request);
			if(csrfToken != null)
				return csrfToken.getToken();
			return "";
		}
	}
}
