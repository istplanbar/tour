/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***/
package org.springframework.security.web.csrf;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.OncePerRequestFilter;

public final class CsrfFilter extends OncePerRequestFilter {
	private final Log logger = LogFactory.getLog(super.getClass());
	private final CsrfTokenRepository tokenRepository;
	private RequestMatcher requireCsrfProtectionMatcher = new DefaultRequiresCsrfMatcher();
	private AccessDeniedHandler accessDeniedHandler = new AccessDeniedHandlerImpl();

	public CsrfFilter(CsrfTokenRepository csrfTokenRepository) {
		Assert.notNull(csrfTokenRepository,
				"csrfTokenRepository cannot be null");
		this.tokenRepository = csrfTokenRepository;
	}

	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		CsrfToken csrfToken = this.tokenRepository.loadToken(request);
		boolean missingToken = csrfToken == null;
		if (missingToken) {
			CsrfToken generatedToken = this.tokenRepository
					.generateToken(request);
			csrfToken = new SaveOnAccessCsrfToken(this.tokenRepository,
					request, response, generatedToken);
		}
		request.setAttribute(CsrfToken.class.getName(), csrfToken);
		request.setAttribute(csrfToken.getParameterName(), csrfToken);
		request.getSession().setAttribute("csrfToken", csrfToken.getToken());
		if(!requireCsrfProtectionMatcher.matches(request) || request.getParameter("SSO") != null) {
			filterChain.doFilter(request, response);
			return;
		}
		String actualToken = request.getHeader(csrfToken.getHeaderName());
		if (actualToken == null) {
			actualToken = request.getParameter(csrfToken.getParameterName());	
		}
		if (actualToken == null) {
			actualToken = request.getParameter("form:_csrf");	
		}
		
		if (!(csrfToken.getToken().equals(actualToken))) {
			if (this.logger.isDebugEnabled()) {
				this.logger.debug("Invalid CSRF token found for "
						+ UrlUtils.buildFullRequestUrl(request));
			}
			if (missingToken)
				this.accessDeniedHandler.handle(request, response,
						new MissingCsrfTokenException(actualToken));
			else {
				this.accessDeniedHandler.handle(request, response,
						new InvalidCsrfTokenException(csrfToken, actualToken));
			}
			return;
		}

		filterChain.doFilter(request, response);
	}

	public void setRequireCsrfProtectionMatcher(
			RequestMatcher requireCsrfProtectionMatcher) {
		Assert.notNull(requireCsrfProtectionMatcher,
				"requireCsrfProtectionMatcher cannot be null");
		this.requireCsrfProtectionMatcher = requireCsrfProtectionMatcher;
	}

	public void setAccessDeniedHandler(AccessDeniedHandler accessDeniedHandler) {
		Assert.notNull(accessDeniedHandler,
				"accessDeniedHandler cannot be null");
		this.accessDeniedHandler = accessDeniedHandler;
	}

	private static final class DefaultRequiresCsrfMatcher implements
			RequestMatcher {
		private Pattern allowedMethods;

		private DefaultRequiresCsrfMatcher() {
			this.allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
		}

		public boolean matches(HttpServletRequest request) {
			return (!(this.allowedMethods.matcher(request.getMethod())
					.matches()));
		}
	}

	private static final class SaveOnAccessCsrfToken implements CsrfToken {
		private transient CsrfTokenRepository tokenRepository;
		private transient HttpServletRequest request;
		private transient HttpServletResponse response;
		private final CsrfToken delegate;

		public SaveOnAccessCsrfToken(CsrfTokenRepository tokenRepository,
				HttpServletRequest request, HttpServletResponse response,
				CsrfToken delegate) {
			this.tokenRepository = tokenRepository;
			this.request = request;
			this.response = response;
			this.delegate = delegate;
		}

		public String getHeaderName() {
			return this.delegate.getHeaderName();
		}

		public String getParameterName() {
			return this.delegate.getParameterName();
		}

		public String getToken() {
			saveTokenIfNecessary();
			return this.delegate.getToken();
		}

		public String toString() {
			return "SaveOnAccessCsrfToken [delegate=" + this.delegate + "]";
		}

		public int hashCode() {
			int prime = 31;
			int result = 1;
			result = 31 * result
					+ ((this.delegate == null) ? 0 : this.delegate.hashCode());

			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (super.getClass() != obj.getClass())
				return false;
			SaveOnAccessCsrfToken other = (SaveOnAccessCsrfToken) obj;
			if (this.delegate == null)
				if (other.delegate != null)
					return false;
				else if (!(this.delegate.equals(other.delegate)))
					return false;
			return true;
		}

		private void saveTokenIfNecessary() {
			if (this.tokenRepository == null) {
				return;
			}

			synchronized (this) {
				if (this.tokenRepository != null) {
					this.tokenRepository.saveToken(this.delegate, this.request,
							this.response);
					this.tokenRepository = null;
					this.request = null;
					this.response = null;
				}
			}
		}
	}
	
}