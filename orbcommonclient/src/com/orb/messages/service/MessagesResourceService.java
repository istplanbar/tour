package com.orb.messages.service;

import java.util.List;

import com.orb.messages.bean.MessageResource;

public interface MessagesResourceService {

	public List<MessageResource> getAllMessageList();
	public void manualMessagesSourceCacheEvict();

}
