package com.orb.messages.service.impl;

/**
 * @author IST planbar / MaCh
 */

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.orb.messages.bean.MessageResource;
import com.orb.messages.dao.MessagesDAO;
import com.orb.messages.service.MessagesResourceService;

public class MessagesResourceServiceImpl implements MessagesResourceService{
	
	private static Log logger = LogFactory.getLog(MessagesResourceServiceImpl.class);
	
	 @Autowired
	 MessagesDAO messagesDAO;
	 
	 @Cacheable(value="messagesSource")
	 public List<MessageResource> getAllMessageList(){
		return messagesDAO.getAllMessageList();
	 }
	
	 @CacheEvict(value="messagesSource", allEntries=true)
	 public void manualMessagesSourceCacheEvict(){
		logger.info("MessagesSource cache evicted manually....");
	 }

}
