package com.orb.messages.constants;

import java.util.Map;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.utils.CommonUtils;

public class MessagesConstants {
	
	public final static String MESSAGES_LIST_PAGE="/common/process/messages/loadMessages.html";
	public final static String MESSAGES_LIST_HTML="messages/messagesView.xhtml";
	public final static String KEY="Key";
	public final static String GERMAN="German";
	public final static String ENGLISH="English";
	public final static String PARENT_PAGE="ParentPage";
	public static final String MESSAGE_DELETE_SUCCESS ="messages_delete_success";
	public static final String MESSAGES_TOGGLER_LIST = "MESSAGES_TOGGLER_LIST";
	public static final String MESSAGE_ADDED_SUCCESS = "messages_added_success";
	public static final String MESSAGE_UPDATED_SUCCESS = "messages_updated_success";
	public final static String SELECTED_DOCUMENT_ID = "SELECTED_DOCUMENT_ID";
	public final static String EDIT = "EDIT";
	public final static String CREATE = "CREATE";
	public final static String VIEW = "VIEW";
	
	public static final Map<String,String> SUPPORTED_FORMAT;
	public static final Map<String,String> MSG_MODULE_ID;
	public static final String MODULE_ID = "MODULE_ID";
	public final static String DOCUMENT_ID = "DOCUMENT_ID";
	public final static String ACTIVITY = "ACTIVITY";
	public final static String DOWNLOAD = "Download";
	public final static String MSG_IS_BROWSWER_EDGE = CommonConstants.FALSE;
	public static final String EXCEL_ADDED_SUCCESS = "excel_added_success";
	
	
	
	static{
		SUPPORTED_FORMAT = (Map<String,String>)CommonUtils.getBean("MSG_FILE_FORMATS");
		MSG_MODULE_ID = (Map<String,String>)CommonUtils.getBean("MSG_MODULE_ID");
	}

}
