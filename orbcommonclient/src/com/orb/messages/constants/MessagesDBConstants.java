package com.orb.messages.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class MessagesDBConstants {
	
	public static final String TBL_MULTI_LANG,TBL_MAIL_LANG_HISTORY;
	public static final Map<String,String> MULTI_LANG_MAP,MULTI_LANG_HISTORY_MAP;
	public static final Map<String,String> multiLanguageDBConfigMap;
	
    static{
		
    	multiLanguageDBConfigMap=(Map<String,String>)CommonUtils.getBean("multiLanguageDBConfig");
			
    	MULTI_LANG_MAP=(Map<String,String>)CommonUtils.getBean("MOD_MULTI_LANG_MSG");
    	MULTI_LANG_HISTORY_MAP=(Map<String,String>)CommonUtils.getBean("MOD_MULTI_LANG_MSG_HISTORY");
		
    	TBL_MULTI_LANG=CommonUtils.getPropertyValue("MULTI_LANG_TABLE_NAMES", "MOD_MULTI_LANG_MSG");
    	TBL_MAIL_LANG_HISTORY=CommonUtils.getPropertyValue("MULTI_LANG_TABLE_NAMES", "MOD_MULTI_LANG_MSG_HISTORY");
    }

}
