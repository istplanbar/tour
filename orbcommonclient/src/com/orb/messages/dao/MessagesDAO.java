package com.orb.messages.dao;

/**
 * @author IST planbar / MaCh
 */
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;



import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.messages.bean.MessageResource;
import com.orb.messages.constants.MessagesDBConstants;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class MessagesDAO extends AbstractDAO{
	
	private static Log logger = LogFactory.getLog(MessagesDAO.class);
	
	/**
	 * This method inserts new message into DB
	 * @param commonError
	 * @param loginUser
	 * @param messageResource
	 */
	public void addMessageResource(CommonError commonError, User loginUser,MessageResource messageResource) {
		logger.info(" ==> Insert New Message Resource START <== ");
		try{
			insertRecord(loginUser, messageResource,MessagesDBConstants.TBL_MULTI_LANG,MessagesDBConstants.multiLanguageDBConfigMap.get("rowPrefix"));	
		}catch( Exception e ){
			logger.info("Error when Insert New Message.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert New Message Resource END <== ");
	}

	/**
     * This method will return list of Messages from DB
     * @param commonError
     * @param parentPage
     * @return
     */
	public List<MessageResource> getMessageResourceList(CommonError commonError, String parentPage) {
		logger.info(" ==> getMessageResourceList START <== ");
		List<MessageResource> messageResourceList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : MessagesDBConstants.MULTI_LANG_MAP.keySet() ){
				qry.append(MessagesDBConstants.MULTI_LANG_MAP.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(MessagesDBConstants.TBL_MULTI_LANG);qry.append(" WHERE ");
			qry.append(MessagesDBConstants.MULTI_LANG_MAP.get("MODULE_ID"));qry.append(" = ? ");
			messageResourceList = getJdbcTemplate().query( qry.toString(),new String[]{parentPage}, new BeanPropertyRowMapper(MessageResource.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Message Resource list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getMessageResourceList END <== ");
		return messageResourceList;
	}
	
    /**
     * This method updates existing Message in DB
     * @param commonError
     * @param loginUser
     * @param messageResource
     * @return
     */
	public void updateMessageResource(CommonError commonError, User loginUser, MessageResource messageResource) {
		 logger.info(" ==> Update MessageResource START <== ");
			
		 try{
			 //for(MessageResource messageResource : messageResource2){
			updateRecordById(loginUser, messageResource,MessagesDBConstants.TBL_MULTI_LANG);
			 //}
			 
		 }catch( Exception e ){
			logger.info("Error when Update Message.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		 }
		 logger.info(" ==>  Update MessageResource END <== ");
	}
	
	/**
	 * This method delete the messages into DB
	 * @param commonError
	 * @param loginUser
	 * @return 
	 */
	public void deleteMessageResource(CommonError commonError, User loginUser, List<String> ids) {
		logger.info(" ==> Delete Messages START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertMessagesHistory( commonError, loginUser,ids,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", ids);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(MessagesDBConstants.TBL_MULTI_LANG);newQry.append(" WHERE ");
			newQry.append( MessagesDBConstants.MULTI_LANG_MAP.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete messages list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Messages END <== ");

	}
	
	private void insertMessagesHistory(CommonError commonError, User loginUser, List<String> ids, String action) {
		logger.info(" ==> Insert Messages History START <== ");
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : MessagesDBConstants.MULTI_LANG_MAP.keySet() ){
		  newQryHist.append( MessagesDBConstants.MULTI_LANG_MAP.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(MessagesDBConstants.TBL_MULTI_LANG);newQryHist.append(" WHERE ");newQryHist.append( MessagesDBConstants.MULTI_LANG_MAP.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<ids.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<MessageResource> messageResourceList = (List<MessageResource>)getJdbcTemplate().query(newQryHist.toString(), ids.toArray(),  new BeanPropertyRowMapper<MessageResource>(MessageResource.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( MessagesDBConstants.TBL_MAIL_LANG_HISTORY);newQry.append("( ");
			for( String column : MessagesDBConstants.MULTI_LANG_HISTORY_MAP.keySet() ){
				if(!column.equals(MessagesDBConstants.MULTI_LANG_HISTORY_MAP.get("ID"))){
				newQry.append( MessagesDBConstants.MULTI_LANG_HISTORY_MAP.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?) ");
			
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, messageResourceList.get(i).getIdentifier());ps.setInt(2, messageResourceList.get(i).getModuleId());
					ps.setString(3, messageResourceList.get(i).getCode());ps.setString(4, messageResourceList.get(i).getEnglish());
					ps.setString(5, messageResourceList.get(i).getGerman());ps.setString(6, loginUser.getUserId());
					ps.setTimestamp(7, date);ps.setString(8, loginUser.getUserId());
					ps.setTimestamp(9, date);ps.setString(10, action);
				}		
				public int getBatchSize() {
					return messageResourceList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when Insert Messages History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Messages History END <== ");
	}

	/**
     * This method will return list of All Messages from DB
     * @param commonError
     * @return
     */
	public List<MessageResource> getAllMessageList() {
		logger.info(" ==> getMessageResourceList START <== ");
		List<MessageResource> messageResourceList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : MessagesDBConstants.MULTI_LANG_MAP.keySet() ){
				qry.append(MessagesDBConstants.MULTI_LANG_MAP.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(MessagesDBConstants.TBL_MULTI_LANG);
			messageResourceList = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(MessageResource.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Message Resource list. Exception ==> " + e.getMessage() );
		}
		logger.info(" ==> getMessageResourceList END <== ");
		return messageResourceList;
	}

	public void updateMessageResourceFromExcel(CommonError commonError, User loginUser, MessageResource messageResource, String parentPage,
			List<String> messageCodes, List<MessageResource> msgResourceList) {
		
		logger.info(" ==> Excel Upload Messages START <== ");
		
		try{
			
			if(msgResourceList == null || msgResourceList.isEmpty()){
				msgResourceList = new ArrayList<>();
				messageCodes = new ArrayList<>();
				msgResourceList = getMessageResourceList(commonError, parentPage);
				messageCodes = msgResourceList.stream().map(s1 -> s1.getCode()).collect(Collectors.toList());
						
			}
			
			if(!messageCodes.contains(messageResource.getCode())){
				int moduleId = Integer.valueOf(parentPage);
				messageResource.setModuleId(moduleId);
				insertRecord(loginUser, messageResource,MessagesDBConstants.TBL_MULTI_LANG,MessagesDBConstants.multiLanguageDBConfigMap.get("rowPrefix"));
			}
			java.util.Date  date = CommonUtils.getCurrentUTCDate();
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( MessagesDBConstants.TBL_MULTI_LANG );qry.append(" SET ");
			
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("ENGLISH") );qry.append( "=?, ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("GERMAN") );qry.append( "=?, ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("CREATE_BY") );qry.append( "=?, ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("CREATE_TIME") );qry.append( "=?, ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("UPDATE_BY") );qry.append( "=?, ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("UPDATE_TIME") );qry.append( "=? ");
			qry.append(" WHERE ");
			qry.append( MessagesDBConstants.MULTI_LANG_MAP.get("CODE") );qry.append("=? ");
			
			
			List<Object> args = new ArrayList<Object>();
			args.add(messageResource.getEnglish());args.add(messageResource.getGerman());
			args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(messageResource.getCode());
			
			getJdbcTemplate().update( qry.toString(),args.toArray());
			args.clear();
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to Excel Upload messages list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Excel Upload Messages END <== ");
		
	}
    
}
