package com.orb.messages.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.service.CommonService;
import com.orb.messages.bean.MessageResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.UploadedFile;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class ExcelUploadDAO {

private static Log logger = LogFactory.getLog(ExcelUploadDAO.class);
	

	@Autowired
	CommonService commonService;
	
	@Autowired
	MessagesDAO messagesDAO;
	
	@Autowired 
	protected CommonError commonError;
	
	MessageResource msgRes = null;
	List<MessageResource> arrMessageList = new ArrayList<>();
	FileInputStream newFile = null;
	List<String> messageCodes = null; 
	List<MessageResource> msgResourceList = null;		
	public void addTranslationsFromExcel(String file, User loginUser, String parentPage) throws IOException{
		try{
			
			
			File excelFile = new File(file);
			newFile = new FileInputStream(excelFile);
			
			HSSFWorkbook workbook = new HSSFWorkbook(newFile);
            HSSFSheet sheet = workbook.getSheetAt(0);
            
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
               
                Iterator<Cell> cellIterator = row.cellIterator();
                int i=1;
                String label = "";
                msgRes = new MessageResource();
                //hmMessageList = new HashMap<>();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch(i){
                    case 1:
                    	msgRes.setCode(cell.getStringCellValue().toString());  
                    	break;
                    case 2:
                    	msgRes.setEnglish(cell.getStringCellValue().toString());  
                        break;
                    case 3:
                    	msgRes.setGerman(cell.getStringCellValue().toString());
                        break;
                    }
                    i++; 
                }   
                arrMessageList.add(msgRes);
            }
            commonService.updateMessageResourceFromExcel(commonError, loginUser, arrMessageList, parentPage, messageCodes, msgResourceList);
            /*for (Map.Entry<String, MessageResource> entry : hmMessageList.entrySet()) {
                logger.info("Key = " + entry.getKey() + ", Value = " + entry.getValue().getEnglish()+ "," +entry.getValue().getGerman());
                
            }*/
			
		}catch(Exception e){
			logger.info("Error reading excel file" +e);
			e.printStackTrace();
		}
		finally{
			newFile.close();
		}
		
	}
	
	
}
