package com.orb.common.factory;

import java.util.Locale;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.orb.common.client.constants.CommonConstants;

public abstract class AbstractCommonFactory {
	
	/**
	 * This method will return session object for Primefaces
	 * @param key
	 * @return
	 */
	public Object getSessionObj(String key){
		FacesContext fc = FacesContext.getCurrentInstance();
		if( fc != null ){
			ExternalContext externalContext = fc.getExternalContext();
	    	Map<String, Object> sessionMap = externalContext.getSessionMap();
	    	if( sessionMap != null )
	    		return sessionMap.get(key);
		}
		
		return null;
	}
	
	/**
	 * This method will set session object from Primefaces. 
	 * @param key
	 * @param value
	 */
	public void addSessionObj(String key, Object value){
		FacesContext fc = FacesContext.getCurrentInstance();
		if( fc != null ){
			ExternalContext externalContext = fc.getExternalContext();
	    	Map<String, Object> sessionMap = externalContext.getSessionMap();
	    	if( sessionMap != null )
	    		sessionMap.put(key, value);
		}
	}
	
	/**
	 * This method will show the "Info" message to the user
	 * @param bundle
	 * @param key
	 */
	public void infoMessage(ReloadableResourceBundleMessageSource bundle, String key) {
		String value = null;
		try{
			Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
			value = bundle.getMessage(key, null, locale);
		}catch(Exception e){
			value = key;
		}
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", value));
    }
     
	/**
	 * This method will show the "Warning" message to the user
	 * @param bundle
	 * @param key
	 */
    public void warnMessage(ReloadableResourceBundleMessageSource bundle, String key) {
    	String value = null;
		try{
			Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
			value = bundle.getMessage(key, null, locale);
		}catch(Exception e){
			value = key;
		}
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", value));
    }
     
    /**
	 * This method will show the "Error" message to the user
	 * @param bundle
	 * @param key
	 */
    public void errorMessage(ReloadableResourceBundleMessageSource bundle, String key) {
    	Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
    	String value = bundle.getMessage(key, null, locale);
       FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", value));
    }
    
    /**
     * This method will show the "Fatal" message to the user
     * @param bundle
     * @param key
     */
    public void fatalMessage(ReloadableResourceBundleMessageSource bundle, String key) {
    	String value = null;
		try{
			Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
			value = bundle.getMessage(key, null, locale);
		}catch(Exception e){
			value = key;
		}
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", value));
    }
    
    /**
     * Returns the resource bundle value to the given Key. This is applicable only for JSF.
     * @param bundle
     * @param key
     * @return
     */
    public String getJSFMessage( ReloadableResourceBundleMessageSource bundle, String key ){
		try{
			Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
			return bundle.getMessage(key, null, locale);
		}catch(Exception e){
			return key;
		}
	}
    
    /**
	 * Returns resource bundle value to the given Key
	 * @param bundle
	 * @param key
	 * @param locale
	 * @return
	 */
	public String getMessage( ReloadableResourceBundleMessageSource bundle, String key, Locale locale ){
		try{
			return bundle.getMessage(key, null, locale);
		}catch(Exception e){
			return key;
		}
	}
	
    /**
	 * Returns resource bundle value to the given Key. Also appends the place holders
	 * @param bundle
	 * @param key
	 * @param args
	 * @param locale
	 * @return
	 */
	public String getMessage( ReloadableResourceBundleMessageSource bundle, String key, String args[], Locale locale ){
		try{
			return bundle.getMessage(key, args, locale);
		}catch(Exception e){
			return key;
		}
	}
	
}   
	