package com.orb.common.resourcebundle;

import java.util.Locale;

import javax.el.ELContext;
import javax.el.ELException;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.MessageSource;
import org.springframework.web.jsf.el.SpringBeanFacesELResolver;

public class CustomResourceBundle extends SpringBeanFacesELResolver{
	DatabaseDrivenMessageSource databaseDrivenMessageSource = null;
	
	@Override
	public Object getValue(ELContext elContext, Object base, Object property) throws ELException{
		if (base instanceof MessageSource && property instanceof String){
			String result = null;
			try{
				if( databaseDrivenMessageSource == null ){
					BeanFactory beanFactory = getBeanFactory(elContext);
					databaseDrivenMessageSource = (DatabaseDrivenMessageSource)beanFactory.getBean("messageSource");
				}
				result = databaseDrivenMessageSource.getMessage((String) property, null, getLocale());
				if( result == null ){
					result = ((MessageSource) base).getMessage((String) property, null, getLocale());
				}
			}catch( Exception e ){
				result = (String) property;
			}
		if (null != result){
			elContext.setPropertyResolved(true);
		}
		return result;
		}
		return super.getValue(elContext, base, property);
	}
	
	private Locale getLocale(){
		return FacesContext.getCurrentInstance().getViewRoot().getLocale();
	}
}