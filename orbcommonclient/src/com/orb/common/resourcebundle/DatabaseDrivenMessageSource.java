package com.orb.common.resourcebundle;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;


import com.orb.messages.bean.MessageResource;
import com.orb.messages.service.MessagesResourceService;

public class DatabaseDrivenMessageSource extends AbstractMessageSource implements ResourceLoaderAware {

    private Logger log = Logger.getLogger(getClass());
    private ResourceLoader resourceLoader;

    private final Map<String, Map<String,String>> properties = new HashMap<String, Map<String,String>>();

    @Autowired 
    private MessagesResourceService messagesResourceService;

    public DatabaseDrivenMessageSource() {
    }

    public DatabaseDrivenMessageSource(MessagesResourceService messageResourceService) {
        this.messagesResourceService = messageResourceService;
    }
    
    public DatabaseDrivenMessageSource(MessagesResourceService messageResourceService, boolean clearCache) {
        this.messagesResourceService = messageResourceService;
        if( clearCache ){
        	refreshCache();
        }
    }

    @Override
    protected MessageFormat resolveCode(String code, Locale locale) {
        String msg = getText(code, locale);
        MessageFormat result = createMessageFormat(msg, locale);
        return result;
    }

    @Override
    protected String resolveCodeWithoutArguments(String code, Locale locale) {
        return getText(code, locale);
    }

    private String getText(String code, Locale locale) {
        Map<String, String> localized = properties.get(code);
        String textForCurrentLanguage = null;
        if (localized != null) {
            textForCurrentLanguage = localized.get(locale.getLanguage());
            if (textForCurrentLanguage == null) {
                textForCurrentLanguage = localized.get(Locale.ENGLISH.getLanguage());
            }
        }
        if (textForCurrentLanguage == null) {
            //Check parent message
            logger.debug("Fallback to properties message");
            try {
                textForCurrentLanguage = getParentMessageSource().getMessage(code, null, locale);
            } catch (Exception e) {
                logger.error("Cannot find message with code: " + code);
            }
        }
        return textForCurrentLanguage != null ? textForCurrentLanguage : code;
    }

    public void refreshCache() {
    	messagesResourceService.manualMessagesSourceCacheEvict();
        properties.clear();
        properties.putAll(loadTexts());
    }

    protected Map<String, Map<String, String>> loadTexts() {
        log.debug("loadTexts");
        Map<String, Map<String, String>> m = new HashMap<String, Map<String, String>>();
        List<MessageResource> messageResourceList = messagesResourceService.getAllMessageList();
        if(messageResourceList != null){
         for (MessageResource messageResource : messageResourceList) {
            Map<String, String> v = new HashMap<String, String>();
            v.put("en", messageResource.getEnglish());
            v.put("de", messageResource.getGerman());
            m.put(messageResource.getCode(), v);
         }
        }
        return m;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = (resourceLoader != null ? resourceLoader : new DefaultResourceLoader());
    }
}
