package com.orb.common.client.jdbc.code;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.orb.common.client.utils.CommonUtils;


public class CustomBeanPropertyRowMapper {
	private static Log logger = LogFactory.getLog(CustomBeanPropertyRowMapper.class);
	
	/**
	 * This method reads the Bean's property which are matching to the given Table columns.
	 * 
	 * @param modelObject
	 * @param tableName
	 * @return
	 */
	public static <T> Map<String, String> getPropertyColsMap(T modelObject, String tableName) {
		if( CommonUtils.getValidString(tableName).isEmpty()){
			logger.info("Given Table Name is NULL / EMPTY....Unable to generate the Bean's Propery Map...");
			return null;
		}
		if( modelObject == null ){
			logger.info("Given object is NULL....Unable to generate the Bean's Propery with " + tableName + " columns.");
			return null;
		}
		Map<String,String> tableColsMap = (Map<String,String>)CommonUtils.getBean(tableName);
		if( tableColsMap == null || tableColsMap.isEmpty() ){
			logger.info("No Column is available for the table " + tableName);
			return null;
		}
		Class<?> c = modelObject.getClass(); 
		HashSet<Field> fieldSet = getFields(c) ;
		if( fieldSet == null || fieldSet.isEmpty() ){
			logger.info("No Bean Property available for the bean " + c.getName());
			return null;
		}
		Map<String, String> tempTableColsMap = new HashMap<String,String>();
		tableColsMap.forEach((k,v) -> tempTableColsMap.put(v.replace("_", ""),v));
		Map<String, String> propertyColsMap = new TreeMap<String,String>();
		
		fieldSet.forEach((f)->{
			String tempName = f.getName().toUpperCase();
			if(tempTableColsMap.containsKey(tempName)){
				propertyColsMap.put(f.getName(), tempTableColsMap.get(tempName));
			}
		});
		
		return propertyColsMap; 
	} 
	
	private static HashSet<Field> getFields(Class<?> c) { 
	  HashSet<Field> fields = new HashSet<Field>(); 
	  Arrays.stream(c.getDeclaredFields()).forEach(fields::add); 
	  if (c.getSuperclass() != null && c.getSuperclass() != c) 
	   fields.addAll(getFields(c.getSuperclass())); 
	  return fields; 
	 }

	
}
