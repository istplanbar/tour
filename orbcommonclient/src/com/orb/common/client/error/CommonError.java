package com.orb.common.client.error;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class CommonError {
private Set<String> errorSet = new HashSet<>();
	
	public void addError(String errorMsg){
		errorSet.add(errorMsg);
	}
	
	public Set<String> getError(){
		return errorSet;
	}
	
	public void clearErrors(){
		errorSet.clear();
	}
}
