package com.orb.common.client.logger;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * @author 
 * Logging the message
 */
public class OrbPerformanceLogger {
	private static Logger performanceLogger;
	/**
	 * Read log4j properties and initialize the logger 
	 * @param logPropertyPath
	 * @throws OrbWSException
	 */
	public static void init(String logPropertyPath){
		try{
			Properties logProperties = new Properties();
			FileInputStream fin = new FileInputStream(logPropertyPath);
			logProperties.load(fin);      
			PropertyConfigurator.configure(logProperties);
			performanceLogger = Logger.getLogger("PerformanceLog");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Writing the message into logger
	 * @param message
	 */
	public static void debug(String message){
		performanceLogger.debug(": "+message);
	}
	
	/**
	 * Writing the message into logger
	 * @param message
	 */
	public static void info(String message){
		performanceLogger.info(": "+message);
	}
	
	/**
	 * Writing the Exception into logger
	 * @param message
	 * @param ex
	 */
	public static void error(String message,Throwable ex){
		performanceLogger.error(message,ex);
	}
}
