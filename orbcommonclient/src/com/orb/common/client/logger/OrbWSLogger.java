package com.orb.common.client.logger;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * 
 * @author 
 * Logging the message
 */
public class OrbWSLogger {
	private static Logger webServiceLogger;
	
	/**
	 * Read log4j properties and initialize the logger 
	 * @param logPropertyPath
	 * @throws OrbWSException
	 */
	public static void init(String logPropertyPath){
		try{
			Properties logProperties = new Properties();
			FileInputStream fin = new FileInputStream(logPropertyPath);
			logProperties.load(fin);      
			PropertyConfigurator.configure(logProperties);
			webServiceLogger = Logger.getLogger("WebServiceLog");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Writing the message into logger
	 * @param message
	 */
	public static void log(String message){
		webServiceLogger.debug(": "+message);
	}
	
	/**
	 * Writing the Exception into logger
	 * @param message
	 * @param ex
	 */
	public static void logException(String message,Throwable ex){
		webServiceLogger.error(message,ex);
	}
}
