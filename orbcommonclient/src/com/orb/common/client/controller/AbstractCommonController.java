package com.orb.common.client.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.web.csrf.CsrfUtil;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.service.CommonService;
import com.orb.common.client.utils.CommonUtils;
import com.orb.common.factory.AbstractCommonFactory;

@SessionAttributes({CommonConstants.USER_LOCALE})
abstract public class AbstractCommonController extends AbstractCommonFactory implements ApplicationContextAware{
	private static Log logger = LogFactory.getLog(AbstractCommonController.class);
	
	@Autowired 
	protected ReloadableResourceBundleMessageSource bundle;
	
	@Autowired 
	protected CommonService commonService;
	
	@Autowired 
	protected CommonError commonError;
	
	protected String _csrf;
	
	protected ApplicationContext applicationContext;
	
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	/**
     * This method Autowires the current Managed Bean into Spring Context.
     * It is required to access the Module Services without Session Scope. 
     * 
     */
    @PostConstruct
    private void init() {
        FacesContext facesContext =  FacesContext.getCurrentInstance();
        if( facesContext != null ){
            ExternalContext externalContext = facesContext.getExternalContext();
            ServletContext servletContext = (ServletContext) externalContext.getContext();
            
            WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext).
                                       getAutowireCapableBeanFactory().
                                       autowireBean(this);
        }
    }
	
	
	/**
	 * This method will be invoked on each page load of JSF
	 * @param event
	 * @throws Exception
	 */
	public void preRenderView(ComponentSystemEvent event)throws Exception{
		  Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
		  FacesContext fc = FacesContext.getCurrentInstance();
		  fc.getViewRoot().setLocale(locale);
		  addErrorMessage(commonError);
		  if( commonError != null ) commonError.clearErrors();
	}
	/**
	 * This method will return session object for Primefaces
	 * @param key
	 * @return
	 */
	public Object getSessionObj(String key){
		FacesContext fc = FacesContext.getCurrentInstance();
		if( fc != null ){
			ExternalContext externalContext = fc.getExternalContext();
	    	Map<String, Object> sessionMap = externalContext.getSessionMap();
	    	if( sessionMap != null )
	    		return sessionMap.get(key);
		}
		
		return null;
	}
	/**
	 * This method will set session object from Primefaces. 
	 * @param key
	 * @param value
	 */
	public void addSessionObj(String key, Object value){
		FacesContext fc = FacesContext.getCurrentInstance();
		if( fc != null ){
			ExternalContext externalContext = fc.getExternalContext();
	    	Map<String, Object> sessionMap = externalContext.getSessionMap();
	    	if( sessionMap != null )
	    		sessionMap.put(key, value);
		}
		
	}
	
	/**
	 * check the image into the directory
	 */
	public String getDefaultImage(String image){
		String defaultImage = null;
		String path = System.getProperty("config.dir") + "images/" + image;
		File f = new File(path);
		if(!f.exists() || CommonUtils.getValidString(image).isEmpty()){
			defaultImage = "default.jpg";
		}
		return defaultImage;
	}
	/**
	 * This method will set session object from Primefaces. 
	 * @param key
	 * @param value
	 */
	public void setSessionObject(User loginUser){
		User user = (User) getSessionObj(CommonConstants.USER_IN_CONTEXT);
		loginUser.setIpAddress(user.getIpAddress());
		if(!"".equals(CommonUtils.getValidString(loginUser.getTimeZone()))){
        	loginUser.setTimeZone(loginUser.getTimeZone().substring(0, loginUser.getTimeZone().indexOf("(")).trim());
        }
		if(loginUser.getActualDisplayRowsSize() == null){
			loginUser.setActualDisplayRowsSize("5,10,25,50,100,250");
		}
		List<String> tempDisplayRowSize = Arrays.asList(loginUser.getActualDisplayRowsSize());
       	StringBuffer tempStr = new StringBuffer();
        if( !"".equals(CommonUtils.getValidString(loginUser.getDisplayRowsSize())) &&  !tempDisplayRowSize.contains(loginUser.getDisplayRowsSize()) ){
        	tempStr.append(loginUser.getDisplayRowsSize());
    		tempStr.append(",");
        }
        else if("".equals(CommonUtils.getValidString(loginUser.getDisplayRowsSize()))){
        	loginUser.setDisplayRowsSize("5");
        }
        for( String pageTemplate : tempDisplayRowSize ){
    		tempStr.append(pageTemplate);
    		tempStr.append(",");
    	}
        tempStr.setLength( tempStr.length()-1 );
        loginUser.setActualDisplayRowsSize(tempStr.toString());
        Locale locale = new Locale(loginUser.getLanguage());
        addSessionObj(CommonConstants.USER_LOCALE, locale);
        addSessionObj(CommonConstants.USER_IN_CONTEXT, loginUser);
        addSessionObj(CommonConstants.LAST_LOGGEDIN_USER, null);



	}

	



	/**
	 * This method will add the error messages based on the error which are coming from Service layers.
	 * @param key
	 */
    public void addErrorMessage( CommonError error ) {
    	StringBuffer errorMsgs = new StringBuffer();
    	if( error != null && error.getError() != null && !error.getError().isEmpty() ){
    		Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
    		for( String errorCode : error.getError() ){
    			String errorMsg = null;
    			try{
    				errorMsg = bundle.getMessage(errorCode, null, locale);
    			}catch(Exception e){
    				errorMsg = errorCode;
    			}
    			errorMsgs.append(errorMsg);errorMsgs.append("<br/>");
    		}
    		if( errorMsgs.length() > 5 ){
    			FacesContext fc = FacesContext.getCurrentInstance();
    			fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", errorMsgs.substring(0,errorMsgs.length()-5)));
    		}
    	}else{
    		FacesContext fc = FacesContext.getCurrentInstance();
    		if( fc.isValidationFailed() ){
    			errorMessage("required_field");
    		}
    	}
    }
	/**
	 * This method will show the "Info" message to the user
	 * @param key
	 */
	public void infoMessage(String key) {
		super.infoMessage(bundle, key);
    }
     
	/**
	 * This method will show the "Warning" message to the user
	 * @param key
	 */
    public void warnMessage(String key) {
		super.warnMessage(bundle, key);
    }
     
    /**
	 * This method will show the "Error" message to the user
	 * @param key
	 */
    public void errorMessage(String key) {
		super.errorMessage(bundle, key);
    }
    
    public void fatalMessage(String key) {
		super.fatalMessage(bundle, key);
    }
	
	public void updateCsrfInSession(){
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpServletRequest request = (HttpServletRequest) ec.getRequest();
		if(null != request)
			request.getSession().setAttribute("_csrf", CsrfUtil.getCurrentCSRFToken(request));
	}
	public String get_csrf() {
		return _csrf;
	}

	public void set_csrf(String _csrf) {
		this._csrf = _csrf;
	}
	
	public String getJSFMessage( String key ){
		return super.getJSFMessage(bundle, key);
	}
	
	/**
	 * Returns resource bundle value to the given Key
	 * @param key
	 * @param locale
	 * @return
	 */
	public String getMessage( String key, Locale locale ){
		return super.getMessage(bundle, key, locale);
	}
	
	/**
	 * Returns resource bundle value to the given Key. Also appends the place holders
	 * @param key
	 * @param args
	 * @param locale
	 * @return
	 */
	public String getMessage( String key, String args[], Locale locale ){
		return super.getMessage(bundle, key, args, locale);
	}

	/**
	 * 
	 * @param error
	 * @param multiValueField
	 * @return
	 */
	
	protected List<MultiVal> getMultiValues(CommonError error, String multiValueField) {
    	List<String> multiValueList = commonService.getMultiValues(error, multiValueField);
    	
    	List<MultiVal> multiValList = new ArrayList<MultiVal>();
    	
		if( multiValueList != null ){
    		for(String multiValue : multiValueList){
    			MultiVal multiVal = new MultiVal();
    			multiVal.setId(multiValue);
    			multiVal.setValue(getJSFMessage(multiValueField+"."+multiValue));
    			multiValList.add(multiVal);
    		}
    	}
		return multiValList;
	}
	protected void redirector( String url ){
		if( "".equals(CommonUtils.getValidString(url)) ) {
			logger.info("Sorry... " + url + " is invalid URL...");
			return;
		}
		try {
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			ServletContext servletContext= (ServletContext)externalContext.getContext();
			externalContext.redirect( servletContext.getContextPath() + url );
		} catch (IOException e) {
			logger.error("Not able to redirect to ==> " + url + ". Exception ==> " + e.getMessage());
		}
	}
	
	/**
	 * To redirect to particular page and display custom messages.
	 * @param msg
	 * @param targetUrl
	 */
	protected void customRedirector( String msg, String targetUrl ){
		if( commonError.getError().isEmpty() ){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			infoMessage(msg);
			redirector(targetUrl);
		}
	}
	
	/**
	 * To redirect to particular page and display custom messages.
	 * @param msg
	 * @param targetUrl
	 */
	protected void customWarnMessage( String msg){
		if( commonError.getError().isEmpty() ){
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			warnMessage(msg);
		}
	}
	
	/**
	 * 
	 * @param attributeName
	 * @return
	 */
	protected String getJSFRequestAttribute(String attributeName){
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		return (String)req.getAttribute(attributeName);
	}
	public String getExtension(String filename){
		String extension = null;
		int i = filename.lastIndexOf('.');
		if (i >= 0) {
			extension =  filename.substring(i+1);
		}
		return extension;
	}
	
	protected List<User> getAllUsersList(){
		return commonService.getAllUsersList();
	}
	
	/**
	 * Get the full username for information container
	 * @param userId
	 * @return
	 * @throws NullPointerException
	 */
	public String getUserFullName(String userId)throws NullPointerException{
		User requiredUser = null;
		String requiredUserName = null;
		try{
			requiredUser = getUserById(commonError, userId);
			requiredUserName = requiredUser.getFirstName()+ " " + requiredUser.getLastName();
		}
		catch(Exception e){
			logger.error("Gt user error");
		}
		return CommonUtils.getValidString(requiredUserName);
	}
	
	/**
	 * To display user image in information tab for particular task
	 * @param userId
	 * @return
	 */
	public String getUserImage(String userId){
		User requiredUser = getUserById(commonError, userId);
		if( requiredUser == null) requiredUser = new User();
		return CommonUtils.getValidString(requiredUser.getUserImage());
	}
	
	public User getUserById( CommonError error, String userId  ){
		List<User> usersList = getAllUsersList();
		if( usersList == null ) {
			logger.error("All User List is Empty... " );
			return null;
		}
		
		User requiredUser = null;
		try{
			requiredUser = usersList.stream().filter(user -> user.getUserId().equalsIgnoreCase(userId)).findFirst().get();
		}catch(Exception e){
			logger.error("No user is available in the list for User Id => " + userId );
		}
		
		return requiredUser;
	}
	
	
}
