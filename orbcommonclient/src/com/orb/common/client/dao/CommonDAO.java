package com.orb.common.client.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.constants.CommonDBConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.messages.constants.MessagesDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class CommonDAO extends AbstractDAO {
	private static Log logger = LogFactory.getLog(CommonDAO.class);

	/**
	 * This method will return list of multiValue fields.
	 * @param error
	 * @param multiValueField
	 * @return
	 */
	public List<String> getMultiValues(CommonError error, String multiValueField) {
		logger.info(" ==> getMultiValues START <== ");
		List<String> multiValueList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_MULTI_VALUES);
			qry.append(" WHERE ");qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE_FIELD"));qry.append(" = ? ");
			qry.append(" ORDER BY ");qry.append(CommonDBConstants.MULTI_VALUES.get("DISP_ORDER"));
			multiValueList = (List<String>)getJdbcTemplate().queryForList(qry.toString(), new Object[] { multiValueField }, String.class);
		}catch( Exception e ){
			logger.info("Error when retrieve multiValue List.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMultiValues END <== ");
		return multiValueList;
	}
	
	/**
	 * This method will log the user login audit trail in DB
	 * @param userError
	 * @param user
	 * @return 
	 */
	public void loginAuditTrail(User user, String action) {
		logger.info(" ==> loginAuditTrail START <== ");
		if( null == user){
			logger.info("User is null....");
		}else{
			try{
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				List<Object> args = new ArrayList<Object>();
				args.add(user.getUserId());
				args.add(action);
				args.add(date);
				args.add(user.getIpAddress());
				StringBuffer queryInsert = new StringBuffer();
				queryInsert.append("INSERT INTO ");
				queryInsert.append(CommonDBConstants.TBL_LOGIN_AUDITTRAIL);queryInsert.append("( ");
				queryInsert.append(CommonDBConstants.LOGIN_AUDITTRAIL.get("USER_ID"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.LOGIN_AUDITTRAIL.get("ACTION"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.LOGIN_AUDITTRAIL.get("TIME_STAMP"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.LOGIN_AUDITTRAIL.get("IP_ADDRESS"));
				queryInsert.append(") VALUES(?,?,?,?) ");
				getJdbcTemplate().update( queryInsert.toString(), args.toArray());
			}catch( Exception e ){
				logger.info("Error when Insert user audit trail info.  " + e.getMessage());
			}
		}
		logger.info(" ==> loginAuditTrail END <== ");
	}
	
	/**
	 * This method will return list of multiValue fields.
	 * @param error
	 * @return
	 */
	public List<MultiVal> getMultiValueList(CommonError commonError) {
		logger.info(" ==> getMultiValueList START <== ");
		List<MultiVal> multiValList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.MULTI_VALUES.get("ID"));qry.append(" , ");
			qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE_FIELD"));qry.append(" , ");
			qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_MULTI_VALUES);
			multiValList = getJdbcTemplate().query( qry.toString(),new String[]{},new BeanPropertyRowMapper(MultiVal.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch multiValue list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getMultiValueList END <== ");
		return multiValList;
	}
	
	/**
	 * This method inserts new Multivalues into DB
	 * @param commonError
	 * @param loginUser
	 * @param multiValMap
	 * @param multiValMsgMap
	 */
	public void addMultiValAndMsg(CommonError commonError, User loginUser, Map<String, List<String>> multiValMap,
			Map<String, String> multiValMsgMap) {
		logger.info(" ==> Insert MultiValAndMsg START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			qry.append("INSERT INTO ");qry.append( CommonDBConstants.TBL_MULTI_VALUES);qry.append("( ");
			qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE_FIELD"));qry.append(" , ");
			qry.append(CommonDBConstants.MULTI_VALUES.get("MULTI_VALUE"));
			qry.append(") VALUES(?,?) ");
			
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, multiValMsgMap.get("multiValField"));ps.setString(2, multiValMap.get("multiValueIdList").get(i));
					
				}		
				public int getBatchSize() {
					return multiValMap.get("multiValueIdList").size();
				}
			});
			addMsg(commonError,loginUser,multiValMap,multiValMsgMap);	
		}catch( Exception e ){
			logger.info("Error when Insert MultiValAndMsg.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> Insert MultiValAndMsg END <== ");
	
		
	}
	
	/**
	 * This method inserts new Multivalues into DB
	 * @param commonError
	 * @param loginUser
	 * @param multiValMap
	 * @param multiValMsgMap
	 */
	private void addMsg(CommonError commonError, User loginUser, Map<String, List<String>> multiValMap,
			Map<String, String> multiValMsgMap) {
		logger.info(" ==> Insert Msg START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			String nextRowId = getNextRowId( MessagesDBConstants.TBL_MULTI_LANG, MessagesDBConstants.multiLanguageDBConfigMap.get("rowPrefix") );
			qry.append("INSERT INTO ");qry.append( MessagesDBConstants.TBL_MULTI_LANG);qry.append("( ");
			for( String column : MessagesDBConstants.MULTI_LANG_MAP.keySet() ){
				if(!column.equals(MessagesDBConstants.MULTI_LANG_MAP.get("ID"))){
					qry.append( MessagesDBConstants.MULTI_LANG_MAP.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?) ");
			
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {
				
				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, nextRowId);ps.setString(2, multiValMsgMap.get("parentPage"));
					ps.setString(3, multiValMsgMap.get("multiValField")+"."+multiValMap.get("multiValueIdList").get(i));
					ps.setString(4, multiValMap.get("multiValEngList").get(i));
					ps.setString(5, multiValMap.get("multiValGerList").get(i));
					ps.setString(6, loginUser.getUserId());
					ps.setTimestamp(7, date);ps.setString(8, loginUser.getUserId());
					ps.setTimestamp(9, date);ps.setString(10, CommonConstants.Y);
				}		
				public int getBatchSize() {
					return multiValMap.get("multiValueIdList").size();
				}
			});
			
			
		}catch( Exception e ){
			logger.info("Error when Insert Msg.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Msg END <== ");
	}
}
