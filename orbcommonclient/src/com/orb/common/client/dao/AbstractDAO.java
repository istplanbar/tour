package com.orb.common.client.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.constants.CommonDBConstants;
import com.orb.common.client.jdbc.code.CustomBeanPropertyRowMapper;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

abstract public class AbstractDAO implements ApplicationContextAware{
	private static Log logger = LogFactory.getLog(AbstractDAO.class);
	
	protected JdbcTemplate jdbcTemplate;
	protected ApplicationContext applicationContext;
	protected PlatformTransactionManager transactionManager; 
	private DBConnection dbConnection;
	
	@Autowired
	public void setDBConnection( DBConnection dbConnection ){
		this.dbConnection = dbConnection;
		jdbcTemplate = dbConnection.getJdbcTemplate();
		transactionManager = dbConnection.getTransactionManager();
	}
	
	protected JdbcTemplate getJdbcTemplate(){
		if(jdbcTemplate == null ){
			jdbcTemplate = dbConnection.getJdbcTemplate();
			transactionManager = dbConnection.getTransactionManager();
		}
		return jdbcTemplate;
	}
	
	public TransactionStatus getTransactionStatus(){
		return transactionManager.getTransaction(new DefaultTransactionDefinition());
	}

	protected String getSchema(){
		return dbConnection.getSchema();
	}
	
	/**
	* @param tableName
	* @return
	* The get the ID for the module
	*/
	public String getNextRowId( String tableName ) {
		return getNextRowId( tableName, null );
	}
	/**
	* @param tableName
	* @param prefix
	* @return
	* The get the ID for the module
	*/
	public String getNextRowId( String tableName, String prefix ) {
	   try {
	       StringBuffer qry = new StringBuffer("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES ");
	       qry.append("WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?");
	       
	       String schema = getSchema();
	       
	       if( tableName.startsWith(schema) ) tableName = tableName.replace(schema + ".", "" );
	       
	       Integer id = (Integer)getJdbcTemplate().queryForObject(qry.toString(), new String[] { schema, tableName }, Integer.class); 

	       return CommonUtils.getValidString( prefix ) + String.format( CommonConstants.DB_ROW_ID_MAX_LENGTH, id.intValue() );

	   } catch (Exception e) {
		   logger.error(e.getMessage());
	       return "";
	   }
	}
	
	public String getVersion( String tableName, String prefix, String suffix ) {
		   try {
		       StringBuffer qry = new StringBuffer("SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES ");
		       qry.append("WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?");
		       
		       String schema = getSchema();
		       
		       if( tableName.startsWith(schema) ) tableName = tableName.replace(schema + ".", "" );
		       
		       Integer id = (Integer)getJdbcTemplate().queryForObject(qry.toString(), new String[] { schema, tableName }, Integer.class); 

		       return CommonUtils.getValidString( prefix ) + String.format( CommonConstants.DB_ROW_ID_MAX_LENGTH, id.intValue())+ "_" + CommonUtils.getValidString( suffix );

		   } catch (Exception e) {
			   logger.error(e.getMessage());
		       return "";
		   }
		}
	
	public String genNextVersion(String version){
		try{
		String[] temp = version.split("-");
		String nextVersion = null;
		if(version.indexOf("-") >= 0){
			int ver=Integer.parseInt(temp[1]);
			ver++;
			nextVersion = String.format(temp[0] + "-" + ver);
		}else{
			nextVersion = String.format(temp[0] + "-1");
		}
		return nextVersion;
		
		}catch (Exception e) {
			   logger.error(e.getMessage());
		       return "";
		   }
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<User> getAllUsersList(){
		List<User> usersList = null;
		final List<User> finalUserList;
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT ");
			qry.append(CommonDBConstants.USER.get("ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("PASSWORD"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("FIRST_NAME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_NAME"));qry.append(", ");			
			qry.append(CommonDBConstants.USER.get("EMAIL"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LANGUAGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("THEME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("TOP_MENU"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_EXPIRY"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("ROLE_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_IMAGE"));
			qry.append(" FROM ");
			qry.append(CommonDBConstants.TBL_USER);
			
			usersList = getJdbcTemplate().query( qry.toString(), new BeanPropertyRowMapper(User.class) );
			finalUserList = new ArrayList<User>(usersList);
		}catch( Exception e ){
			logger.info("Error when retrieve User Details.  " + e.getMessage());
			return null;
		}
		return finalUserList;
	}
	
	/**
	 * This method will insert the given bean object value into the given database table.
	 * 
	 * @param loginUser
	 * @param modelObject
	 * @param tableName
	 * @param idPrefix
	 * @param identifierProperty
	 * @return
	 */
	protected <T> int insertRecord( User loginUser, T modelObject, String tableName, String idPrefix, String identifierProperty ){
		int insertStatus = -1;
		TransactionStatus transactionStatus = getTransactionStatus();
		try{
			TreeMap<String,String> propertyColsMap = (TreeMap<String,String>)CustomBeanPropertyRowMapper.getPropertyColsMap(modelObject, tableName);
			if( propertyColsMap == null || propertyColsMap.isEmpty() ){
				return insertStatus;
			}
			propertyColsMap.remove("id"); // Id field is not required in the row mapper for insert / update operations.
			BeanWrapperImpl beanWrapper = new BeanWrapperImpl(modelObject);
			includeInsertDefaultValue(loginUser, beanWrapper, tableName, idPrefix, identifierProperty);
			
			List<Object> argList = new ArrayList<Object>();
			StringBuilder query = new StringBuilder();
			StringBuilder preParams = new StringBuilder();
			
			query.append("INSERT INTO ");query.append(CommonDBConstants.DB_SCHEMA);query.append(tableName);query.append(" ( ");
			
			propertyColsMap.forEach((k,v) -> {
				query.append(v);query.append(", ");
				preParams.append("?,");
				argList.add(beanWrapper.getPropertyValue(k));
			});
			query.setLength( query.length()-2 );
			preParams.setLength( preParams.length()-1 );
			query.append(") VALUES ( ");
			query.append(preParams);
			query.append(") ");
			
			logger.info(query.toString());
			
			getJdbcTemplate().update( query.toString(), argList.toArray() );
			
			transactionManager.commit(transactionStatus);
			
		}catch( Exception e ){
			logger.error("Unable to insert record in the table " + tableName + ". Exception ==> " + e.getMessage() );
			transactionManager.rollback(transactionStatus);
		}
		return insertStatus;
	}
	/**
	 * This method will insert the given bean object value into the given database table.
	 * 
	 * @param loginUser
	 * @param modelObject
	 * @param tableName
	 * @param idPrefix
	 * @return
	 */
	protected <T> int insertRecord( User loginUser, T modelObject, String tableName, String idPrefix ){
		return insertRecord( loginUser, modelObject, tableName, idPrefix, "identifier");
	}
	
	/**
	 * This method will insert the given object values into the given table.
	 * 
	 * @param loginUser
	 * @param modelObject
	 * @param tableName
	 * @return
	 */
	protected <T> int updateRecordById( User loginUser, T modelObject, String tableName ){
		int updateStaus = -1;
		TransactionStatus transactionStatus = getTransactionStatus();
		try{
			
			TreeMap<String,String> propertyColsMap = (TreeMap<String,String>)CustomBeanPropertyRowMapper.getPropertyColsMap(modelObject, tableName);
			if( propertyColsMap == null || propertyColsMap.isEmpty() ){
				return updateStaus;
			}
			String idColumn = propertyColsMap.get("id");
			propertyColsMap.remove("id"); // Id field is not required in the row mapper for insert / update operations.
			
			BeanWrapperImpl beanWrapper = new BeanWrapperImpl(modelObject);
			String idValue = (String)beanWrapper.getPropertyValue("id");
			includeUpdateDefaultValue( loginUser, beanWrapper );
			
			// IMPORTANT: The below method should be called after the includeUpdateDefaultValue method. Otherwise Created Date, Created By, Updated date and Updated By will not be updated into Hitory table
			insertHistoryRecord(loginUser, beanWrapper, propertyColsMap, tableName + "_HISTORY", CommonConstants.UPDATED); 
			
			List<Object> argList = new ArrayList<Object>();
			StringBuilder updateQuery = new StringBuilder();
			
			updateQuery.append("UPDATE ");updateQuery.append(tableName);updateQuery.append(" SET ");
			propertyColsMap.forEach((k,v) -> {
				updateQuery.append(v);updateQuery.append(" = ?, ");
				argList.add(beanWrapper.getPropertyValue(k));
			});
			updateQuery.setLength( updateQuery.length()-2 );
			updateQuery.append(" WHERE ");
			updateQuery.append(idColumn);
			updateQuery.append(" = ? ");
			argList.add(idValue);
			
			getJdbcTemplate().update( updateQuery.toString(), argList.toArray() );
			
			transactionManager.commit(transactionStatus);
			
		}catch( Exception e ){
			logger.error("Unable to insert record in the table " + tableName );
			transactionManager.rollback(transactionStatus);
		}
		
		return updateStaus;
	}
	
	/**
	 * This method inserts the history record for the given object.
	 * 
	 * @param loginUser
	 * @param modelObject
	 * @param tableName
	 * @return
	 */
	private void insertHistoryRecord( User loginUser, BeanWrapperImpl beanWrapper, TreeMap<String,String> propertyColsMap, String tableName, String action ) throws Exception{
		String actionCol = CommonUtils.getPropertyValue(tableName, "ACTION");
		List<Object> argList = new ArrayList<Object>();
		StringBuilder query = new StringBuilder();
		StringBuilder preParams = new StringBuilder();
		
		propertyColsMap.remove("active");
		query.append("INSERT INTO ");query.append(tableName);query.append(" ( ");
		propertyColsMap.forEach((k,v) -> {
			query.append(v);query.append(", ");
			preParams.append("?,");
			argList.add(beanWrapper.getPropertyValue(k));
		});
		query.append(actionCol);
		preParams.append("?");
		query.append(") VALUES ( ");
		query.append(preParams);
		query.append(") ");
		argList.add(action);
		
		getJdbcTemplate().update( query.toString(), argList.toArray() );
		
	}
	
	/**
	 * This method will include the default values for common fields. 
	 * 
	 * @param loginUser
	 * @param modelObject
	 */
	protected <T> void includeInsertDefaultValue( User loginUser, T modelObject, String tableName, String idPrefix, String identifierProperty ){
		BeanWrapperImpl beanWrapper = new BeanWrapperImpl(modelObject);
		includeInsertDefaultValue(loginUser, beanWrapper, tableName, idPrefix, identifierProperty);
	}
	
	/**
	 * This method will include the default values for common fields.
	 * 
	 * @param loginUser
	 * @param modelObject
	 * @param tableName
	 * @param idPrefix
	 */
	protected <T> void includeInsertDefaultValue( User loginUser, T modelObject, String tableName, String idPrefix ){
		includeInsertDefaultValue(loginUser, modelObject, tableName, idPrefix, "identifier");
	}
	
	/**
	 * This method will include the default values for common fields. 
	 * 
	 * @param loginUser
	 * @param beanWrapper
	 * @param tableName
	 * @param idPrefix
	 * @param identifierProperty
	 */
	private void includeInsertDefaultValue( User loginUser, BeanWrapperImpl beanWrapper, String tableName, String idPrefix, String identifierProperty ){
		String nextRowId = getNextRowId( tableName, idPrefix );
		PropertyValue identifierValue = new PropertyValue(identifierProperty, nextRowId);
		beanWrapper.setPropertyValue(identifierValue);
		
		Date currentUTCDate = null;
		try{
			currentUTCDate = CommonUtils.getCurrentUTCDate();
		}catch( Exception e){
			
		}
		PropertyValue createdByValue = new PropertyValue("createBy", CommonUtils.getValidString(loginUser.getUserId()));
		beanWrapper.setPropertyValue(createdByValue);
		
		PropertyValue createdDateValue = new PropertyValue("createTime", currentUTCDate);
		beanWrapper.setPropertyValue(createdDateValue);
		
		PropertyValue modifiedByValue = new PropertyValue("updateBy", CommonUtils.getValidString(loginUser.getUserId()));
		beanWrapper.setPropertyValue(modifiedByValue);
		
		PropertyValue modifiedDateValue = new PropertyValue("updateTime", currentUTCDate);
		beanWrapper.setPropertyValue(modifiedDateValue);
		
		PropertyValue activeValue = new PropertyValue("active", CommonConstants.Y);
		beanWrapper.setPropertyValue(activeValue);
	}
	
	/**
	 * This method will include the default values for update operation.
	 * @param loginUser
	 * @param beanWrapper
	 */
	private void includeUpdateDefaultValue( User loginUser, BeanWrapperImpl beanWrapper ){
		Date currentUTCDate = null;
		try{
			currentUTCDate = CommonUtils.getCurrentUTCDate();
		}catch( Exception e){
			
		}
		PropertyValue modifiedByValue = new PropertyValue("updateBy", CommonUtils.getValidString(loginUser.getUserId()));
		beanWrapper.setPropertyValue(modifiedByValue);
		
		PropertyValue modifiedDateValue = new PropertyValue("updateTime", currentUTCDate);
		beanWrapper.setPropertyValue(modifiedDateValue);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}
}
