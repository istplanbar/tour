package com.orb.common.client.dao;

import javax.annotation.PostConstruct;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;

import com.orb.common.client.utils.EncryptionUtil;

@Repository
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class DBConnection {
	@Value("${db.driverName}")
	private String driverName;
	
	@Value("${db.url}")
	private String url;
	
	@Value("${db.userName}")
	private String userName;
	
	@Value("${db.password}")
	private String password;
	
	@Value("${db.schema}")
	private String schema;
	
	private JdbcTemplate jdbcTemplate;
	
	private BasicDataSource dataSource;
	private DataSourceTransactionManager transactionManager; 
	
	@PostConstruct
	public void init(){
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverName);
		dataSource.setUrl(url);
		dataSource.setUsername(userName);
		dataSource.setPassword(EncryptionUtil.decrypt(password));
		
		transactionManager = new DataSourceTransactionManager(dataSource);
		jdbcTemplate = new JdbcTemplate(transactionManager.getDataSource());
	}
	
	public DataSourceTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public JdbcTemplate getJdbcTemplate(){
		return jdbcTemplate;
	}
	public String getSchema(){
		return schema;
	}
	
}
