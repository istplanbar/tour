package com.orb.common.client.model;

public class MultiVal {
	private String id;
	private String value;
	private String multiValueField;
	private String multiValue;
	private String checked;
	
	public String getId() {
		return id;
	}
	public void setId(String multiValue) {
		this.id = multiValue;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String string) {
		this.value = string;
	}
	public String getMultiValueField() {
		return multiValueField;
	}
	public void setMultiValueField(String multiValueField) {
		this.multiValueField = multiValueField;
	}
	public String getMultiValue() {
		return multiValue;
	}
	public void setMultiValue(String multiValue) {
		this.multiValue = multiValue;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	
}