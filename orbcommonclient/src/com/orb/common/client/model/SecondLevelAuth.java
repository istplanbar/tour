package com.orb.common.client.model;

import java.sql.Date;

public class SecondLevelAuth {
	private String userId;
	private String mtan;
	private Date expireTime;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMtan() {
		return mtan;
	}
	public void setMtan(String mtan) {
		this.mtan = mtan;
	}
	public Date getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	
	
	
}