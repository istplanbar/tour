package com.orb.common.client.model;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{

	private static final long serialVersionUID = 8181586307312327150L;
	
	private Integer id;
	private String userId;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String initials;
	private String phoneNumber;
	private String mobile;
	private String smsSignature;
	private String emailSignature;
	private String language;
	private String theme;
	private Boolean topMenu;
	private String timeZone;
	private Date lastPwdChange;
	private Date userExpiry;
	private Date startDate;
	private String authenticateUser;
	private String secondLevelAuthUserPrivilege;
	private Integer roleId;
	private String userImage;
	private String ipAddress;
	private String displayRowsSize; 
	private String actualDisplayRowsSize;
	private String displayDateFormat;
	private Date createdDate;
	private Date lastModifiedDate;
	private Integer loginFailureCount;
	private String wsUserId;
	private String wsPassword;
	private String disable;
	private String deputy;
	private String checked;
	private String active;
    private String createBy;
	private String updateBy;
	private Boolean isPasswordChecked;
	private String newPassword;
	private String confirmPassword;
	private String registrationPin;
	
	
	public String getSmsSignature() {
		return smsSignature;
	}
	public void setSmsSignature(String smsSignature) {
		this.smsSignature = smsSignature;
	}
	public String getEmailSignature() {
		return emailSignature;
	}
	public void setEmailSignature(String emailSignature) {
		this.emailSignature = emailSignature;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getDeputy() {
		return deputy;
	}
	public void setDeputy(String deputy) {
		this.deputy = deputy;
	}
	public String getDisable() {
		return disable;
	}
	public void setDisable(String disable) {
		this.disable = disable;
	}
	public String getAuthenticateUser() {
		return authenticateUser;
	}
	public void setAuthenticateUser(String authenticateUser) {
		this.authenticateUser = authenticateUser;
	}
	public String getWsUserId() {
		return wsUserId;
	}
	public String getSecondLevelAuthUserPrivilege() {
		return secondLevelAuthUserPrivilege;
	}
	public void setSecondLevelAuthUserPrivilege(String secondLevelAuthUserPrivilege) {
		this.secondLevelAuthUserPrivilege = secondLevelAuthUserPrivilege;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setWsUserId(String wsUserId) {
		this.wsUserId = wsUserId;
	}
	public String getWsPassword() {
		return wsPassword;
	}
	public void setWsPassword(String wsPassword) {
		this.wsPassword = wsPassword;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public Boolean getTopMenu() {
		return topMenu;
	}
	public void setTopMenu(Boolean topMenu) {
		this.topMenu = topMenu;
	}
	public Date getLastPwdChange() {
		return lastPwdChange;
	}
	public void setLastPwdChange(Date lastPwdChange) {
		this.lastPwdChange = lastPwdChange;
	}
	public Date getUserExpiry() {
		return userExpiry;
	}
	public void setUserExpiry(Date userExpiry) {
		this.userExpiry = userExpiry;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getDisplayRowsSize() {
		return displayRowsSize;
	}
	public void setDisplayRowsSize(String displayRowsSize) {
		this.displayRowsSize = displayRowsSize;
	}
	public String getActualDisplayRowsSize() {
		return actualDisplayRowsSize;
	}
	public void setActualDisplayRowsSize(String actualDisplayRowsSize) {
		this.actualDisplayRowsSize = actualDisplayRowsSize;
	}
	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public String getDisplayDateFormat() {
		return displayDateFormat;
	}
	public void setDisplayDateFormat(String displayDateFormat) {
		this.displayDateFormat = displayDateFormat;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public Integer getLoginFailureCount() {
		return loginFailureCount;
	}
	public void setLoginFailureCount(Integer loginFailureCount) {
		this.loginFailureCount = loginFailureCount;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
    public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Boolean getIsPasswordChecked() {
		return isPasswordChecked;
	}
	public void setIsPasswordChecked(Boolean isPasswordChecked) {
		this.isPasswordChecked = isPasswordChecked;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getRegistrationPin() {
		return registrationPin;
	}
	public void setRegistrationPin(String registrationPin) {
		this.registrationPin = registrationPin;
	}
	
}

