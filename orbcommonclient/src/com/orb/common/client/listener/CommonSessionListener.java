package com.orb.common.client.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;											 
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;																	
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.model.User;
@Service
public class CommonSessionListener implements HttpSessionListener{
	private static Log logger = LogFactory.getLog(CommonSessionListener.class);
	
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession();
		ServletContext servletContext =   session.getServletContext();
		WebApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		CommonDAO commonDAO = (CommonDAO)appContext.getBean("commonDAO");
		Map<String, String> adminConfMap = (Map<String, String>)session.getAttribute(CommonConstants.ADMIN_CONF);
		User user = (User)session.getAttribute(CommonConstants.USER_IN_CONTEXT);
		if(user != null){
			HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
			String ipAddr = getClientIpAddr(request);				  
		   String ipAddressLog = getFilteredIpAddress(adminConfMap,ipAddr);
		    user.setIpAddress(ipAddressLog);
			commonDAO.loginAuditTrail(user, CommonConstants.LOGGED_OUT);
			logger.info( user.getUserId() + " user session inactivated...");
		}
	}
	
	
	 private String getFilteredIpAddress(Map<String, String> adminConfMap, String ipAddress) {
	    	if("2".equals(adminConfMap.get("ipAddressLog"))){
	    		 int index= ipAddress.lastIndexOf(".");
	    		 ipAddress = ipAddress.substring(0, index)+".*";
	    	}else if("3".equals(adminConfMap.get("ipAddressLog"))){
	    		 ipAddress = "";
	    	}
			return ipAddress;
	}
	 
	 private String getClientIpAddr(HttpServletRequest request) {  
	        String ip = request.getHeader("X-Forwarded-For");  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("Proxy-Client-IP");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("WL-Proxy-Client-IP");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_X_FORWARDED");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_CLIENT_IP");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_FORWARDED_FOR");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_FORWARDED");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("HTTP_VIA");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getHeader("REMOTE_ADDR");  
	        }  
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
	            ip = request.getRemoteAddr();  
	        }  
	        return ip;  
	    }
	 															
}
