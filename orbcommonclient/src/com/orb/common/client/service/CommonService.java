package com.orb.common.client.service;

import java.util.List;
import java.util.Map;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.messages.bean.MessageResource;

public interface CommonService {

	public List<String> getMultiValues( CommonError error, String multiValueField );
	public List<User> getAllUsersList();
	public void addLoginAuditTrail( User user, String action);
	public void addMessageResource(CommonError commonError,User loginUser, Map<String, String> messageResourceMap);
	public List<MessageResource> getMsgResourceList(CommonError commonError, String parentPage);
	public void updateMessageResource(CommonError commonError, User loginUser, MessageResource messageResource);
	public List<MultiVal> getMultiValueList(CommonError commonError);
	public void addMultiValAndMsg(CommonError commonError, User loginUser, Map<String, List<String>> multiValMap,
			Map<String, String> multiValMsgMap);
	public void deleteMessageResource(CommonError commonError, User loginUser,
			List<MessageResource> selectedMsgResourceList);
	
	public void updateMessageResourceFromExcel(CommonError commonError, User loginUser, List<MessageResource> 
	messageResource, String parentPage, List<String> messageCodes, List<MessageResource> msgResourceList);
}
