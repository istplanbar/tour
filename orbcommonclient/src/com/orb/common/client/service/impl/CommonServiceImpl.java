package com.orb.common.client.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.MultiVal;
import com.orb.common.client.model.User;
import com.orb.common.client.service.CommonService;
import com.orb.common.resourcebundle.DatabaseDrivenMessageSource;
import com.orb.messages.bean.MessageResource;
import com.orb.messages.constants.MessagesConstants;
import com.orb.messages.dao.MessagesDAO;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class CommonServiceImpl implements CommonService{
	private static Log logger = LogFactory.getLog(CommonServiceImpl.class);
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	 MessagesDAO messagesDAO;
	 
	@Autowired
	DatabaseDrivenMessageSource databaseDrivenMessageSource;
	
	 /**
	  * This method will interact with DAO to insert a Message in DB
	  */
	@CacheEvict(value="messagesSource", allEntries=true)
	public void addMessageResource(CommonError commonError,User loginUser,Map<String, String> messageResourceMap) {
		MessageResource messageResource = new MessageResource();
		//logger.info("MessageResourceMap: "+messageResourceMap);
		int parentId=Integer.parseInt(messageResourceMap.get(MessagesConstants.PARENT_PAGE));
		messageResource.setModuleId(parentId);
		messageResource.setCode(messageResourceMap.get(MessagesConstants.KEY));
		messageResource.setEnglish(messageResourceMap.get(MessagesConstants.ENGLISH));
		messageResource.setGerman(messageResourceMap.get(MessagesConstants.GERMAN));
		messagesDAO.addMessageResource(commonError, loginUser, messageResource );
		databaseDrivenMessageSource.refreshCache();
		
	}
   
	
	/**
	 * This method will interact with DAO to update a Message in DB
	 */
	@CacheEvict(value="messagesSource", allEntries=true)
	public void updateMessageResource(CommonError commonError, User loginUser, MessageResource messageResource) {
		messagesDAO.updateMessageResource(commonError,loginUser,messageResource);
		 databaseDrivenMessageSource.refreshCache();
	}
	
	/**
	 * This method will interact with DAO to update a Message in DB
	 */
	@CacheEvict(value="messagesSource", allEntries=true)
	public void deleteMessageResource(CommonError commonError, User loginUser,
			List<MessageResource> selectedMsgResourceList) {
		List<String> ids=new ArrayList<String>();
		for( MessageResource messageResource : selectedMsgResourceList ){
			ids.add(messageResource.getId());
		}
		messagesDAO.deleteMessageResource(commonError, loginUser, ids);
		databaseDrivenMessageSource.refreshCache();
	}
	
	
	/**
	 * This method will call DAO to get multiValue to the given Field.
	 */
	@Override
	@Cacheable(value="multiValueList", key="#multiValueField")
	public List<String> getMultiValues(CommonError error, String multiValueField) {
		return commonDAO.getMultiValues(error, multiValueField);
	}
	
	/**
	 * This method will return all the users from DB. 
	 * Note: The user list will be returned from Cache. The Cache will be cleared if any user is added / updated in DB. 
	 */
	@Override
	@Cacheable(value="allUsersList")
	public List<User> getAllUsersList( ){
		return commonDAO.getAllUsersList();
	}
	
	@Override
	public void addLoginAuditTrail( User user, String action ){
		 commonDAO.loginAuditTrail(user, action);
	}
	
	/**
	 * This method will call DAO to get multiValue list from DB.
	 */
	@Override
	public List<MultiVal> getMultiValueList(CommonError commonError) {
		return commonDAO.getMultiValueList(commonError);
	}
	
	/**
	 * This method inserts new Multivalues into DB
	 * @param commonError
	 * @param loginUser
	 * @param multiValMap
	 * @param multiValMsgMap
	 */
	@Override
	public void addMultiValAndMsg(CommonError commonError, User loginUser, Map<String, List<String>> multiValMap,
			Map<String, String> multiValMsgMap) {
		commonDAO.addMultiValAndMsg(commonError, loginUser,multiValMap,multiValMsgMap);
		
	}

	/**
	 * This method calls DAO to get existing Message from DB
	 */
	@Override
	public List<MessageResource> getMsgResourceList(CommonError commonError, String parentPage) {
		return messagesDAO.getMessageResourceList(commonError,parentPage);
	}


	@Override
	public void updateMessageResourceFromExcel(CommonError commonError, User loginUser,
			List<MessageResource> messageResourceFromExcel,String parentPage, List<String> messageCodes, List<MessageResource> msgResourceList) {
		List<String> ids=new ArrayList<String>();
		for( MessageResource messageResource : messageResourceFromExcel ){
			messagesDAO.updateMessageResourceFromExcel(commonError, loginUser, messageResource, parentPage, messageCodes, msgResourceList);
		}
		
		databaseDrivenMessageSource.refreshCache();
		
	}

}
