package com.orb.common.client.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTimeZone;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.model.User;
@Service
public class CommonUtils implements ApplicationContextAware{
	public static ApplicationContext applicationContext;
	
	public static String getValidString(String str ){
		if( str == null || "".equals(str.trim()) || "null".equalsIgnoreCase(str.trim()) ) return "";
		return str;
	}
	
	public static boolean isEmpty(String str ){
		if( getValidString(str).isEmpty() ){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean isNotEmpty(String str ){
		if( !getValidString(str).isEmpty() ){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		applicationContext = arg0;
	}
	
	public static String getPropertyValue( String beanId, String propertyName ){
		if( applicationContext != null ){
			Map<String,String> mapObj = (Map<String,String>)applicationContext.getBean(beanId);
			if( mapObj != null )
				return mapObj.get(propertyName);
		}
		return "";
	}
	
	public static Object getBean( String beanId ){
		if( applicationContext != null ){
			return applicationContext.getBean(beanId);
		}
		return null;
	}
	public static int compareDate( Date fromDate, Date toDate ){
		if( fromDate == null && toDate == null ) return 0;
		if( fromDate == null && toDate != null ) return 1;
		if( fromDate != null && toDate == null ) return -1;
		if( fromDate.equals(toDate) ) return 0;
		if( fromDate.before(toDate) ) return 1;
		if( fromDate.after(toDate) ) return -1;
		
		return 0;
	}
	
	/**
	 * This method helps to get current utc time format
	 * @return
	 * @throws ParseException
	 */
	public static Date getCurrentUTCDate() throws ParseException {

		LocalDateTime t = LocalDateTime.now();
		ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dt = dateFormat.parse(now.format(dtf));
		System.out.println("UTC Time ==> " + dt);
		return dt;
	}
	
	/**
	 * This method helps to convert UTC date to current TimeZone date
	 */
	public static Date convertFromUTC( String timeZone , Date userDate){
		TimeZone now = TimeZone.getTimeZone(timeZone);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss") ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfgmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdfgmt.setTimeZone(now);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(sdfgmt.format(userDate));
		} catch (ParseException e) {
			System.out.println("error on date");
		}
		return convertedDate;
	} 
}
