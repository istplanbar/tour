package com.orb.common.client.constants;

public class MultiValueConstants {

	public static final String MULTI_VALUE_LIST_HTML = "admin/multivalue/multivalueList.xhtml";
	public static final String MULTI_VALUE_TOGGLER_LIST = "MULTI_VALUE_TOGGLER_LIST";
	public static final String MULTI_VALUE_ADDED_SUCCESS = "multi_value_added_success";
	public static final String MULTI_VALUE_LIST_PAGE = "/common/process/admin/loadMultiValue.html";;

}
