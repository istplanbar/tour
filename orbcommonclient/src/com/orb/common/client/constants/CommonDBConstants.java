package com.orb.common.client.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

@SuppressWarnings("unchecked")
public class CommonDBConstants {
	
	public static final String TBL_MULTI_VALUES;
	public static final Map<String, String> MULTI_VALUES;
	public static final String TBL_USER,TBL_USER_HISTORY;
	public static final Map<String,String> USER, USER_HISTORY;
	public static final String TBL_LOGIN_AUDITTRAIL;
	public static final Map<String,String> LOGIN_AUDITTRAIL;
	public static final String TBL_FORGOT_PASS;
	public static final Map<String, String> FORGOT_PASS;
	public static final String TBL_FORGOT_PASS_HISTORY;
	public static final Map<String, String> FORGOT_PASS_HISTORY;
	public static final String DB_SCHEMA;
	public static final String TBL_SECOND_LEVEL_AUTH, TBL_USER_NOTIFICATION, TBL_USER_USB_TOKEN, TBL_GOOGLE_AUTH_SECRETS;
	public static final Map<String, String> SECOND_LEVEL_AUTH, USER_NOTIFICATION , USER_USB_TOKEN, GOOGLE_AUTH_SECRETS;
	
	static{
		
		DB_SCHEMA = CommonUtils.getPropertyValue("COMMON_TABLES", "DB_SCHEMA");
		
		TBL_MULTI_VALUES = CommonUtils.getPropertyValue("MULTI_VALUES", "CORE_ORB_MULTI_VALUES");
		MULTI_VALUES  = (Map<String,String>)CommonUtils.getBean("MULTI_VALUES");
		
		TBL_USER = CommonUtils.getPropertyValue("USER_TABLE", "CORE_ORB_USERS");
		USER  = (Map<String,String>)CommonUtils.getBean("USER_TABLE");
		
		TBL_USER_HISTORY = CommonUtils.getPropertyValue("USER_HIST_TABLE", "CORE_ORB_USERS_HISTORY");
		USER_HISTORY  = (Map<String,String>)CommonUtils.getBean("USER_HIST_TABLE");
		
		TBL_LOGIN_AUDITTRAIL = CommonUtils.getPropertyValue("COMMON_TABLES", "CORE_LOGIN_AUDITTRAIL");
		LOGIN_AUDITTRAIL  = (Map<String,String>)CommonUtils.getBean("CORE_LOGIN_AUDITTRAIL");
		
		TBL_FORGOT_PASS = CommonUtils.getPropertyValue("FORGOT_PASSWORD", "CORE_ORB_FORGOT_PASS");
		FORGOT_PASS  = (Map<String,String>)CommonUtils.getBean("FORGOT_PASSWORD");
		
		TBL_FORGOT_PASS_HISTORY = CommonUtils.getPropertyValue("FORGOT_PASSWORD_HISTORY", "CORE_ORB_FORGOT_PASS_HISTORY");
		FORGOT_PASS_HISTORY  = (Map<String,String>)CommonUtils.getBean("FORGOT_PASSWORD_HISTORY");
		
		TBL_SECOND_LEVEL_AUTH = CommonUtils.getPropertyValue("SECOND_LEVEL_AUTH", "CORE_ORB_SECOND_LEVEL_AUTH");
		SECOND_LEVEL_AUTH  = (Map<String,String>)CommonUtils.getBean("SECOND_LEVEL_AUTH");
		
		TBL_USER_NOTIFICATION = CommonUtils.getPropertyValue("USER_NOTIFICATION", "CORE_ORB_USER_NOTIFICATION");
		USER_NOTIFICATION  = (Map<String,String>)CommonUtils.getBean("USER_NOTIFICATION");
		TBL_USER_USB_TOKEN = CommonUtils.getPropertyValue("USER_USB_TOKEN_DETAILS", "CORE_ORB_USER_USB_TOKEN_DETAILS");
		USER_USB_TOKEN  = (Map<String,String>)CommonUtils.getBean("USER_USB_TOKEN_DETAILS");
		
		TBL_GOOGLE_AUTH_SECRETS = CommonUtils.getPropertyValue("GOOGLE_AUTH_SECRETS", "CORE_ORB_GOOGLE_AUTH_SECRETS");
		GOOGLE_AUTH_SECRETS  = (Map<String,String>)CommonUtils.getBean("GOOGLE_AUTH_SECRETS");
	}
}
