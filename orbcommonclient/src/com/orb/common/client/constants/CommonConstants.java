package com.orb.common.client.constants;

import java.util.HashMap;
import java.util.Set;

import com.orb.common.client.logger.OrbApplicationLogger;
import com.orb.common.client.logger.OrbPerformanceLogger;
import com.orb.common.client.logger.OrbWSLogger;
import com.orb.common.client.utils.CommonUtils;

@SuppressWarnings("unchecked")
public class CommonConstants {
	
	public final static String LOGIN_PAGE = "login/login.jsp";
    public final static String SECOND_LEVEL_AUTH_TOKEN_REGISTRATION_PAGE = "login/secondLevelReg.jsp";
	public final static String SECOND_LEVEL_AUTH_PAGE = "login/secondLevelAuth.jsp";
	public final static String SECOND_LEVEL_AUTH_TOKEN_AUTHENTICATION_PAGE = "login/secondLevelTokenAuth.jsp";
	public final static String SECOND_LEVEL_AUTH_TOKEN_ENTRY_PAGE = "login/secondLevelAuthTokenEntry.jsp";
	public final static String SECOND_LEVEL_AUTH_TOKEN_REDIRECT_TO_LOGIN_PAGE = "login/secondLevelTokenAuthRedirect.jsp";																								   
	public final static String FORGOT_PAGE = "login/forgotPassword.xhtml";
	public final static String GOOGLE_AUTH_OPEN_PAGE = "googleauth/googleAuthOpen.xhtml";
	public final static String GOOGLE_AUTH_REG_PAGE = "googleauth/googleAuthReg.xhtml";
	public final static String GOOGLE_AUTH_REG = "/common/googleAuth/regGoogleAuth.html";
	public final static String GOOGLE_AUTH_ENTRY_PAGE = "googleauth/googleAuthEntry.xhtml";	
	public final static String GOOGLE_AUTH_ENTRY = "/common/googleAuth/validateGoogleAuthEntry.html";
	public final static String DASHBOARD = "/common/process/dashboard/loadDashboard.html";
	public static final String USER_PROFILE_PAGE = "admin/user/userProfileSettings.xhtml";
	public static final String LOGIN_AUDIT_TRAIL_PAGE = "admin/loginaudittrail/loginaudittrailList.xhtml";
	public static final String USER_PROFILE_HTML = "/common/process/admin/loadUserProfile.html";
	public final static String SYSTEM_ADMIN ="admin/systemadmin/systemAdministration.xhtml";
	public final static String LIST_PAGE = "/common/process/admin/loadSystemAdministration.html";
	public final static String LOGIN_VALIDATION_PAGE = "/common/init/validateAuthentication?error";
	public final static String SECOND_LEVEL_AUTH = "/common/init/secondLevelAuth.html";
	public final static String SECOND_LEVEL_REG = "/common/init/secondLevelAuthReg.html";
	public final static String DB_ERROR = "db_error";
	public final static String DB_INSERT_ERROR = "db_insert_error";
	public final static String DB_UPDATE_ERROR = "db_update_error";
	public final static String DB_DELETE_ERROR = "db_delete_error";
	public final static String DB_GETLIST_ERROR = "db_getList_error";
	public final static String DB_GETRECORD_ERROR = "db_getRecord_error";
	public final static String INVALID_USER = "invalid_user";
	public final static String INVALID_USERNAME = "invalid_username";
	public final static String INVALID_AUTH_TOKEN = "invalidAuthenicationToken";
	public final static String VALID_AUTH_TOKEN = "validAuthenicationToken";
	public final static String USER_DISABLED = "user_disabled";
	public final static String USER_EXPIRED = "user_expired";
	public final static String USER_CREDENTIAL_EXPIRED = "user_credential_expired";
	public final static String USER_LOCKED = "user_locked";
	public final static String USER_NOT_STARTED = "user_not_started";
	public final static String USER_NOT_STARTED_LOGIN = "user_not_started_login";
	public final static String USER_START_DATE = "USER_START_DATE";
	public final static String USER_LOGGED_OUT = "loged_out";
	public final static String PAST_DATE_ERROR = "past_date_error";
	public final static String LOGIN_RES_BUN = "LOGIN_RES_BUN";
	public final static String Y = "Y";
	public final static String N = "N";
	public final static String TRUE = "true";
	public final static String FALSE = "false";
	public final static String CURRENT_ACTION = "CURRENT_ACTION";
	public final static String  UPDATED = "Updated";
	public final static String  CREATED = "Created";
	public final static String  DELETED = "Deleted";
	public final static String  VIEWED = "Viewed";
	public final static String  LOCKED = "Locked";
	public final static String  UNLOCKED = "Unlocked";
	public final static String  INGE_APPROVED = "Inge_Approved";
	public final static String  INGE_REJECTED = "Inge_Rejected";
	public final static String INGE_EXECUTE = "Inge_Executed";
	public final static String OBJECT_ID = "objectId";
	public final static String OBJECT_ID1 = "objectId1";
	public final static String OBJECT_ID2 = "objectId2";
	public final static String MAIL ="E-Mail";
	public final static String SMS ="sms";
	public final static String U2F_TOKEN = "Token";
	public final static String GOOGLE_AUTH = "GoogleAuth";
	public final static String MTAN_MAIL_SUB = "mTan_Subject";
	public final static String MTAN_BODY ="mTan_Body";
	public final static String MTAN_THANKS ="mTan_Thanks.";
	public final static String ERROR_OBJ = "ERROR_OBJ"; 
	public static final String RELOAD_PROCESS = "RELOAD_PROCESS_URL";
	public static final String INFO_TO_USER = "INFO_TO_USER";
	public static final String INVALID_INFO = "INVALID_INFO";
	public static final String USER_LOCALE = "USER_LOCALE";
	public static final String USER_IN_CONTEXT = "USER_IN_CONTEXT";
	public static final String SPRING_SECURITY_LAST_EXCEPTION = "SPRING_SECURITY_LAST_EXCEPTION";
	public static final String SPRING_SECURITY_LAST_USERNAME = "SPRING_SECURITY_LAST_USERNAME";
	public static final String ADMIN_CONF = "ADMIN_CONF";
	public static final String META_TABLE = "META_TABLE";
	public static final String LAST_LOGGEDIN_USER = "LAST_LOGGEDIN_USER";
	public static final String LOGGEDIN_USER_MENU = "LOGGEDIN_USER_MENU";
	public static final String SUB_MODULE_MAP = "SUB_MODULE_MAP";
	public static final String CACHED_URL = "CACHED_URL";
	public static final String TECHNICAL_ISSUE = "db_issue";
	public static final String MULTI_VAL_FILED_INSAPP = "insapp";
	public static final String MULTI_VAL_FILED_CAR_TYPE = "car_type";
	public static final String MULTI_VAL_FILED_CAR_APPR = "car_appr";
	public static final String MULTI_VAL_FILED_WEEK_DAY = "weekday";
	public static final String MULTI_VAL_FILED_KURS_TYPE = "kurs_typ";
	public static final String MULTI_VAL_FILED_KURS_SEC = "kurs_sec";
	public static final String MULTI_VAL_FILED_GRUND = "grund";
	public static final String MULTI_VAL_FILED_LITERACY = "literacy";
	public static final String MULTI_VAL_FILED_EDUCATION = "education";
	public static final String MULTI_VAL_FILED_STATE = "state";
	public static final String MULTI_VAL_FILED_COUNTRY = "country";
	public static final String MULTI_VAL_FILED_MATERIALTY = "materialty";
	public static final String MULTI_VAL_FILED_TIME = "time";
	public static final String ACCOUNT_EXPIRY_NOTIFY_WEEK = "ACCOUNT_EXPIRY_WEEK";
	public static final String PASSWORD_EXPIRY_NOTIFY_WEEK = "PASSWORD_EXPIRY_WEEK";
	public static final String ACCOUNT_EXPIRY_NOTIFY_TODAY = "ACCOUNT_EXPIRY_TODAY";
	public static final String PASSWORD_EXPIRY_NOTIFY_TODAY = "PASSWORD_EXPIRY_TODAY";
	public static final String ACCOUNT_EXPIRY_MAIL_SUBJECT = "acccount_expiry_mail_subject";
	public static final String PASSWORD_EXPIRY_MAIL_SUBJECT = "password_expiry_mail_subject";
	public static final String ACCOUNT_EXPIRY_NOTIFICATION_WEEK = "account_expiry_notification_week";
	public static final String PASSWORD_EXPIRY_NOTIFICATION_WEEK = "password_expiry_notification_week";
	public static final String ACCOUNT_EXPIRY_NOTIFICATION_TODAY = "account_expiry_notification_today";
	public static final String PASSWORD_EXPIRY_NOTIFICATION_TODAY = "password_expiry_notification_today";
	public static final Set<String> IMAGE_FORMAT, DATE_FORMAT;
	public static final String MULTI_VAL_FIELD_STATUS = "status";
	public static final String MULTI_VAL_FIELD_PRIORITY = "priority";
	public static final String MULTI_VAL_FIELD_ROLE = "role";
		
	//Ivaf
	public static final String MULTI_VAL_FILED_HOUSING_SITUATION = "housing_situation";
	public static final String MULTI_VAL_FILED_FAMILY_STATUS = "family_status";
	public static final String MULTI_VAL_FILED_LANGUAGE_LEVEL = "max_language_level";
	public static final String MULTI_VAL_FILED_HIGH_EDU_QUAL = "high_edu_qual";
	public static final String MULTI_VAL_FILED_HIGH_PROF_QUAL = "high_prof_qual";
	public static final String MULTI_VAL_FILED_WHEREABOUTS = "whereabouts";
	public static final String MULTI_VAL_FILED_DRIVING_CLASS = "driving_class";
	public static final String MULTI_VAL_FILED_RESIDENCE_STATUS = "residence_status";
	public static final String MULTI_VAL_FILED_REGISTRATION_OFFICE = "registration_office";
	public static final String MULTI_VAL_FILED_OCCASION = "occasion";
	public static final String MULTI_VAL_FILED_SUBJECT = "subject";
	public static final String MULTI_VAL_FILED_LANGUAGES = "mother_tongue";
	public static final String MULTI_VAL_FILED_OCCUPATION_LEARNED = "occupation_learned";
	public static final String APP_DATE_FORMAT;
	public static final String APP_DATE_TIME_FORMAT;
	public static final String DB_ROW_ID_MAX_LENGTH;
	public static final int USER_LOCKED_IN_DAYS;
	public static final int USER_EXPIRY_IN_DAYS;
	public static final int LOGIN_FAILURE_COUNT;
	public static final boolean LOGIN_FAILURE_ENABLE;
	public static final HashMap<String,String> LANGUAGES;
	public static final String CONFIRM = "confirm";
	public static final String SUB_MODULE_CREATION = "subModuleCreation";
	public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String EMAIL_REGEX_MESSAGE = "err_emailValid";
	public static final String ERR_MESSAGE_SUBMODULE = "err_subModule";
	public static final String ROLE_TITLE_CREATION = "lbl_Role_Creation";
	public static final String ROLE_TITLE_UPDATE = "lbl_Role_Update";
	public static final String USER_TITLE_CREATION = "hdr_User_Management_Add";
	public static final String USER_TITLE_UPDATE = "hdr_User_Management_Update";
	public static final String SEND_MAIL_HOST;
	public static final String FROM_MAIL_ID;
	public static final String MAIL_PROTOCOL;
	public static final String MTAN_TYPE;
	public static final String COMPANY_NAME = "COMPANY_NAME";
	public static final int MTAN_LENGTH;
	public static final String A = "A";
	public static final String AN = "AN";
	public static final String NUM = "NUM";
	public static final String DISABLE = "Disable";
	public static final String DISABLE_DE = "Deaktivieren";
	public static final String FORGOT_PASSWORD_SUBJECT = "forgot_password_subject";
	public static final String ACTIVE_FLAG = "activeFlag";
	public static final String LOGGED_IN = "LoggedIn";
	public static final String LOGGED_OUT  = "LoggedOut";
	public static final String LOGIN_FAILED = "LoginFailed";
	public static final String SHOW_ADMIN_BUTTON = "SHOW_ADMIN_BUTTON" ;
	public static final String TASK_PLANNER_ACTIVE = "TASK_PLANNER_ACTIVE";
	public final static String FORGOT_PASS_LINK = "FORGOT_PASS_LINK";
	public static final String PASSWORD_EXP_MSG = "PASSWORD_EXP_MSG";
	public static final String DE_REGISTER_GOOGLE_AUTH = "de_register_google_auth";
	public static final String GOOGLE_AUTH_REGISTRATION_PAGE = "REGISTRATION_PAGE";
	public static final String INVALID_REG_PIN = "invalid_registration_pin";
	public static final String GOOGLE_AUTH_BODY = "google_auth_Body";
	public final static String GOOGLE_AUTH_MAIL_SUB = "google_auth_Subject";
	public static final Object GOOGLE_AUTH_MAIL_CHECK = "google_auth_mail_check";
	public static final String MAIL_DATE_FORMAT = "dd.MM.yyyy hh:mm:ss a";
	
	static{
		APP_DATE_FORMAT = CommonUtils.getPropertyValue("applicationProperties", "dateFormat");
		String temp = CommonUtils.getPropertyValue("applicationProperties", "userLockedInDays");
		int tempInt = 85;
		if( !"".equals(temp) )
			tempInt = Integer.parseInt(temp);
		USER_LOCKED_IN_DAYS = tempInt;
		
		temp = CommonUtils.getPropertyValue("applicationProperties", "userExpiryInDays");
		tempInt = 90;
		if( !"".equals(temp) )
			tempInt = Integer.parseInt(temp);
		USER_EXPIRY_IN_DAYS = tempInt;
		
		temp = CommonUtils.getPropertyValue("applicationProperties", "loginFailureCount");
		tempInt = 3;
		if( !"".equals(temp) )
			tempInt = Integer.parseInt(temp);
		LOGIN_FAILURE_COUNT = tempInt;
		
		temp = CommonUtils.getPropertyValue("applicationProperties", "loginCountEnable");
		boolean tempValue = false;
		if( !"".equals(temp) )
			tempValue = Boolean.parseBoolean(temp);
		LOGIN_FAILURE_ENABLE = tempValue;
		
		APP_DATE_TIME_FORMAT = CommonUtils.getPropertyValue("applicationProperties", "dateTimeFormat");
		DB_ROW_ID_MAX_LENGTH = CommonUtils.getPropertyValue("applicationProperties", "dbRowIdMaxLength");
		IMAGE_FORMAT = (Set<String>)CommonUtils.getBean("IMAGE_FORMATS");
		DATE_FORMAT = (Set<String>)CommonUtils.getBean("DATE_FORMATS");
		
		String loggerPath = CommonUtils.getPropertyValue("applicationProperties", "loggerPath");
		OrbWSLogger.init(loggerPath);
		OrbApplicationLogger.init(loggerPath);
		OrbPerformanceLogger.init(loggerPath);
		
		LANGUAGES = (HashMap<String, String>) CommonUtils.getBean("LANGUAGES");

		SEND_MAIL_HOST = CommonUtils.getPropertyValue("mailSenderValues", "host");
		FROM_MAIL_ID = CommonUtils.getPropertyValue("mailSenderValues", "from");
		MAIL_PROTOCOL = CommonUtils.getPropertyValue("mailSenderValues", "protocol");
		
		
		MTAN_TYPE = CommonUtils.getPropertyValue("applicationProperties","mTanType");
		String mtanlength = CommonUtils.getPropertyValue("applicationProperties", "mTanLength");
		int mTanLength ;
		if(!"".equals(mtanlength))
		{				
		mTanLength = Integer.parseInt(mtanlength);
		MTAN_LENGTH = mTanLength;
		}
		else {
			MTAN_LENGTH =6;
		}
			
	}
	}
