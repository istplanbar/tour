package com.orb.calendar.error;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.error.CommonError;

@Service
@Scope( value="session" )
public class CalendarError extends CommonError{
	
}
