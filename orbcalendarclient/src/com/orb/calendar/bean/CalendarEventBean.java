package com.orb.calendar.bean;

import java.util.Date;

public class CalendarEventBean {
	private String id;
	private Integer relId;	
	private String userId;
	private String title;
	private Date startDate;
	private Date endDate;
	private Boolean allDay;
	private String calId;
	private String type;
	private Integer count;
	private Date tillDate;
	private Integer skip;
	private Date actualStartDate;
	private Date actualEndDate;
	private Date actualTillDate;
	private String actualCalId;
	private Boolean editAll;
	private String actualTitle;
	private String actualType;
	private Integer actualSkip;
	private Integer actualCount;
	private int eventIndex;
	
	public String getActualType() {
		return actualType;
	}
	public void setActualType(String actualType) {
		this.actualType = actualType;
	}
	public String getActualTitle() {
		return actualTitle;
	}
	public void setActualTitle(String actualTitle) {
		this.actualTitle = actualTitle;
	}
	public Integer getSkip() {
		return skip;
	}
	public void setSkip(Integer skip) {
		this.skip = skip;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Date getTillDate() {
		return tillDate;
	}
	public void setTillDate(Date tillDate) {
		this.tillDate = tillDate;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Boolean getAllDay() {
		return allDay;
	}
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}
	public String getCalId() {
		return calId;
	}
	public void setCalId(String calId) {
		this.calId = calId;
	}
	public Integer getRelId() {
		return relId;
	}
	public void setRelId(Integer d) {
		this.relId = d;
	}
	public Date getActualStartDate() {
		return actualStartDate;
	}
	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}
	public Date getActualEndDate() {
		return actualEndDate;
	}
	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}
	
	public Boolean getEditAll() {
		return editAll;
	}
	public void setEditAll(Boolean editAll) {
		this.editAll = editAll;
	}
	public Date getActualTillDate() {
		return actualTillDate;
	}
	public void setActualTillDate(Date actualTillDate) {
		this.actualTillDate = actualTillDate;
	}
	public Integer getActualSkip() {
		return actualSkip;
	}
	public void setActualSkip(Integer actualSkip) {
		this.actualSkip = actualSkip;
	}
	
	public int getEventIndex() {
		return eventIndex;
	}
	public void setEventIndex(int eventIndex) {
		this.eventIndex = eventIndex;
	}
	public Integer getActualCount() {
		return actualCount;
	}
	public void setActualCount(Integer actualCount) {
		this.actualCount = actualCount;
	}
	public String getActualCalId() {
		return actualCalId;
	}
	public void setActualCalId(String actualCalId) {
		this.actualCalId = actualCalId;
	}
	public CalendarEventBean copy(){
		CalendarEventBean copyBean = new CalendarEventBean();
		copyBean.setAllDay(allDay);
		copyBean.setCalId(calId);
		copyBean.setCount(count);
		copyBean.setEndDate(endDate);
		copyBean.setId(id);
		copyBean.setSkip(skip);
		copyBean.setRelId(relId);
		copyBean.setStartDate(startDate);
		copyBean.setTillDate(tillDate);
		copyBean.setTitle(title);
		copyBean.setType(type);
		copyBean.setUserId(userId);
		copyBean.setActualStartDate(actualStartDate);
		copyBean.setActualEndDate(actualEndDate);
		copyBean.setActualTillDate(actualTillDate);
		copyBean.setActualTitle(actualTitle);
		copyBean.setActualCalId(actualCalId);
		copyBean.setActualType(actualType);
		copyBean.setActualSkip(actualSkip);
		copyBean.setEditAll(editAll);
		copyBean.setActualCount(actualCount);
		copyBean.setEventIndex(eventIndex);
		return copyBean;
	}
}