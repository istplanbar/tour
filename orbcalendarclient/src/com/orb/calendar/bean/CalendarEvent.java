package com.orb.calendar.bean;

import java.util.Date;

import org.primefaces.model.DefaultScheduleEvent;

public class CalendarEvent extends DefaultScheduleEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7993139943064407800L;
	
	private String calId;
	private String type;
	private Integer count;
	private Date tillDate;
	private Integer skip;
	private Integer relId;
	private String eventId;
	private Boolean editAll;
	private Date actualStartDate;
	private Date actualEndDate;
	private Date actualTillDate;
	private String actualCalId;
	private String actualType;
	private Integer actualSkip;
	private String actualTitle;
	private Integer actualCount;
	private int eventIndex;
	
	public String getActualType() {
		return actualType;
	}

	public void setActualType(String actualType) {
		this.actualType = actualType;
	}

	public String getActualTitle() {
		return actualTitle;
	}

	public void setActualTitle(String actualTitle) {
		this.actualTitle = actualTitle;
	}

	public Date getActualStartDate() {
		return actualStartDate;
	}

	public void setActualStartDate(Date actualStartDate) {
		this.actualStartDate = actualStartDate;
	}

	public Date getActualEndDate() {
		return actualEndDate;
	}

	public void setActualEndDate(Date actualEndDate) {
		this.actualEndDate = actualEndDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Date getTillDate() {
		return tillDate;
	}

	public void setTillDate(Date tillDate) {
		this.tillDate = tillDate;
	}

	public Integer getSkip() {
		return skip;
	}

	public void setSkip(Integer skip) {
		this.skip = skip;
	}

	public CalendarEvent(){
		super();
	}
	
	public CalendarEvent( String arg0, Date arg1, Date arg2 ){
		super(arg0, arg1, arg2);
	}
	
	public CalendarEvent( String arg0, Date arg1, Date arg2, boolean arg3 ){
		super( arg0, arg1, arg2, arg3 );
	}
	
	public CalendarEvent( String arg0, Date arg1, Date arg2, Object arg3 ){
		super( arg0, arg1, arg2, arg3 );
	}
	
	public CalendarEvent( String arg0, Date arg1, Date arg2, String arg3 ){
		super( arg0, arg1, arg2, arg3 );
	}
	
	public String getCalId() {
		return calId;
	}

	public void setCalId(String calId) {
		this.calId = calId;
	}

	public Integer getRelId() {
		return relId;
	}

	public void setRelId(Integer relId) {
		this.relId = relId;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public Boolean getEditAll() {
		return editAll;
	}

	public void setEditAll(Boolean editAll) {
		this.editAll = editAll;
	}

	public Date getActualTillDate() {
		return actualTillDate;
	}

	public void setActualTillDate(Date actualTillDate) {
		this.actualTillDate = actualTillDate;
	}

	public Integer getActualSkip() {
		return actualSkip;
	}

	public void setActualSkip(Integer actualSkip) {
		this.actualSkip = actualSkip;
	}

	public int getEventIndex() {
		return eventIndex;
	}

	public void setEventIndex(int eventIndex) {
		this.eventIndex = eventIndex;
	}

	public Integer getActualCount() {
		return actualCount;
	}

	public void setActualCount(Integer actualCount) {
		this.actualCount = actualCount;
	}

	public String getActualCalId() {
		return actualCalId;
	}

	public void setActualCalId(String actualCalId) {
		this.actualCalId = actualCalId;
	}
}