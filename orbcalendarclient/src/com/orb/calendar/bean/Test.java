package com.orb.calendar.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author sibim
 *
 */
public class Test {


	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		
		String date1 = "2015-01-01 13:34:56";
		String date2 = "2015-01-15 01:00:00";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date3 = format.parse(date1);
		Date date4 = format.parse(date2);
		Calendar incDate = Calendar.getInstance();
		incDate.setTime(date3);
		/*System.out.println(incDate.get(Calendar.HOUR));
		System.out.println(incDate.get(Calendar.MINUTE));
		System.out.println(incDate.get(Calendar.SECOND));*/
		//System.out.println(getInterval("D",date3,date4  ));
		System.out.println(date3.getDate());
		System.out.println(date4.getDate());
		
	}
	
	private static int getInterval( String type, Date startDate, Date tillDate ){
		//int diffInDays = (int)( (tillDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24)/30 );
		int diffInDays = 0;
		Calendar incDate = Calendar.getInstance();
		int calType = Calendar.DATE;
		int interval = 1;
		if( "M".equals(type) ){
			calType = Calendar.MONTH;
		}else if( "W".equals(type) ){
			interval = 7;
		}
		while( !tillDate.before(startDate) ){
			incDate.setTime(startDate);
			//incDate.add(Calendar.MONTH, interval);
			incDate.add(calType, interval);
			startDate = (incDate.getTime());
			diffInDays++;		
		}
		return diffInDays;
	}

}
