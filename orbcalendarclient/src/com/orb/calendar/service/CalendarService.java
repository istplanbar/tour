package com.orb.calendar.service;

import java.util.List;

import com.orb.calendar.bean.CalendarEventBean;
import com.orb.calendar.model.CalendarVal;
import com.orb.common.client.error.CommonError;

public interface CalendarService {
	public void insertEvent( CommonError error, CalendarEventBean calendarEvent );
	
	public List<CalendarEventBean> getCalendarEvents( CommonError error, List<String> selectedCal );
	
	public void deleteEvent( CommonError error, CalendarEventBean calendarEvent );
	
	public void createCalendar( CommonError error, CalendarVal calAttributes );
	
	public List<CalendarVal> getCalendars( CommonError error );
	
	public List<CalendarVal> getCalendars( CommonError error, String calId );

	public void updateCalendar(CommonError error, CalendarVal calAttributes);

	public void deleteCalendar(CommonError error, CalendarVal calAttributes);

	public void updateEvents(CommonError CommonError, CalendarEventBean calendarEvent);

	public void splitEvents(CommonError CommonError, CalendarEventBean calendarEvent);

	public void splitDeleteEvent(CommonError CommonError, CalendarEventBean calendarEvent);
}
