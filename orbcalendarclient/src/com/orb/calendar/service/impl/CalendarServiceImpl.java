package com.orb.calendar.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.calendar.bean.CalendarEventBean;
import com.orb.calendar.constants.CalendarConstants;
import com.orb.calendar.dao.CalendarDAO;
import com.orb.calendar.model.CalendarVal;
import com.orb.calendar.service.CalendarService;
import com.orb.common.client.error.CommonError;
@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class CalendarServiceImpl implements CalendarService{

	@Autowired
	CalendarDAO calendarDAO;

	/**
	 * This method will call DAO to get calendar events to the given user. 
	 */
	public List<CalendarEventBean> getCalendarEvents( CommonError error, List<String> selectedCal ){
		// Fetch list of events based on calendar
		List<CalendarEventBean> daoCalendarEvents =  calendarDAO.getCalendarEvents(error, selectedCal);
		if( daoCalendarEvents == null ) return null;
		
		// Creating events array list
		List<CalendarEventBean> calendarEvents = new ArrayList<CalendarEventBean>();
		for( CalendarEventBean event : daoCalendarEvents ){
			if( null != event.getType()){
				String type = event.getType();
				int calType = Calendar.DATE;
				// Check monthly events
				if( CalendarConstants.TYPE_MONTH.equals(type) ){
					calType = Calendar.MONTH;
					// Check weekly events
				}else if( CalendarConstants.TYPE_WEEK.equals(type) ){
					calType = Calendar.WEEK_OF_MONTH;
				}
				int skip = event.getSkip() == null || event.getSkip().intValue() == 0 ? 1 : event.getSkip().intValue()+1;
				
				//Check event count
				if( 0 == event.getCount()){
					//If event count is 0 consider single event
					//Add into event array list
					calendarEvents.add(event);
				}else if (null != event.getCount() && 0 != event.getCount() ){
					
					//Setting startdate and enddate
					int noOfEvents = event.getCount().intValue();
					CalendarEventBean tempEvent = event.copy();
					tempEvent.setEventIndex(0);
					calendarEvents.add( tempEvent );

					Date startDate = event.getStartDate();
					Date endDate = event.getEndDate();
					//If repeated events
					for( int i=1; i<=noOfEvents; i++ ){
						CalendarEventBean copyEvent = event.copy();

						Calendar incDate = Calendar.getInstance();
						//Calculating startdate and enddate
						incDate.setTime(startDate);
						incDate.add(calType, skip);
						copyEvent.setStartDate(incDate.getTime());

						incDate.setTime(endDate);
						incDate.add(calType, skip);
						copyEvent.setEndDate(incDate.getTime());

						startDate = copyEvent.getStartDate();
						endDate = copyEvent.getEndDate();

						copyEvent.setEventIndex(i);

						calendarEvents.add(copyEvent);

					}
				}
			}else {
				//Add into event array list
				calendarEvents.add(event.copy());
			}
		}

		return calendarEvents;

	}

	/**
	 * This method will call DAO to insert a calendar event in DB 
	 */
	public void insertEvent( CommonError error, CalendarEventBean calendarEvent ){
		if( null == calendarEvent.getCount() && null !=  calendarEvent.getTillDate()) {
			Integer count = getNoOfEvents(calendarEvent.getType(),calendarEvent.getStartDate(),calendarEvent.getTillDate(), calendarEvent.getSkip());
			calendarEvent.setCount(count);
		}  else if(null != calendarEvent.getCount() && null == calendarEvent.getTillDate()){
			Date tillDate = getTillDate(calendarEvent.getType(),calendarEvent.getStartDate(), calendarEvent.getCount(), calendarEvent.getSkip());
			calendarEvent.setTillDate(tillDate);
		}else{
			calendarEvent.setCount(0);
			calendarEvent.setSkip(0);
		}
		calendarDAO.insertEvent(error, calendarEvent);
	}

	public void updateSplitEvent( CommonError error, CalendarEventBean calendarEvent ){
		if( null == calendarEvent.getCount() && null !=  calendarEvent.getTillDate()) {
			// Calculating events count if till date available
			Integer count = getNoOfEvents(calendarEvent.getType(),calendarEvent.getStartDate(),calendarEvent.getTillDate(), calendarEvent.getSkip());
			calendarEvent.setCount(count);
		}  else if(null != calendarEvent.getCount() && null == calendarEvent.getTillDate()){
			// Calculating till date if count available
			Date tillDate = getTillDate(calendarEvent.getType(),calendarEvent.getStartDate(), calendarEvent.getCount(), calendarEvent.getSkip());
			calendarEvent.setTillDate(tillDate);
		}else{
			calendarEvent.setCount(0);
			calendarEvent.setSkip(0);
		}
		calendarDAO.updateEvent(error, calendarEvent);
	}

	public void deleteEvent( CommonError error, CalendarEventBean calendarEvent ){
		calendarDAO.deleteEvent(error, calendarEvent);
	}

	/**
	 * This method will call DAO to create calendar List to the given user. 
	 */
	public void createCalendar(CommonError error, CalendarVal calAttributes) {
		calendarDAO.createCalendar(error, calAttributes);
	}

	/**
	 * This method will call DAO to get calendar List to the given user. 
	 */
	public List<CalendarVal> getCalendars( CommonError error) {
		return calendarDAO.getCalendars(error, null);
	}
	/**
	 * This method will call DAO to get calendar List to the given user. 
	 */
	public List<CalendarVal> getCalendars( CommonError error, String calId ) {
		return calendarDAO.getCalendars( error, calId );
	}
	/**
	 * This method will call DAO to update calendar List to the given user. 
	 */
	public void updateCalendar(CommonError error, CalendarVal calAttributes) {
		calendarDAO.updateCalendar(error, calAttributes);
	}

	/**
	 * This method will call DAO to delete calendar List to the given user. 
	 */
	public void deleteCalendar(CommonError error, CalendarVal calAttributes) {
		calendarDAO.deleteCalendar(error, calAttributes);
	}

	/**
	 * This method will return the no. of intervals between Start and End Date
	 * @param type
	 * @param startDate
	 * @param tillDate
	 * @return
	 */
	private int getNoOfEvents( String type, Date startDate, Date tillDate, Integer skip ){
		Integer noOfEvents = 0;
		Date cloneStartDate = (Date)startDate.clone();
		Calendar incDate = Calendar.getInstance();
		skip = skip == null || skip == 0 ? 1 : skip+1;

		int calType = Calendar.DATE;
		int increment = 1;
		// If the type is month
		if( CalendarConstants.TYPE_MONTH.equals(type) ){
			calType = Calendar.MONTH;
			// If the type is week
		}else if( CalendarConstants.TYPE_WEEK.equals(type) ){
			increment = 7;
		}
		//interval calculation
		while( !tillDate.before(cloneStartDate) ){
			incDate.setTime(cloneStartDate);
			incDate.add(calType, skip*increment);
			cloneStartDate = (incDate.getTime());
			noOfEvents++;		
		}
		return noOfEvents-1;
	}

	/**
	 * This method will return the till date
	 * @param type
	 * @param startDate
	 * @param interval
	 * @param skip
	 * @return
	 */
	private Date getTillDate( String type, Date startDate, int interval, Integer skip ){
		Date tillDate = (Date)startDate.clone();
		Calendar incDate = Calendar.getInstance();
		int calType = Calendar.DATE;
		skip = skip == null || skip == 0 ? 1 : skip+1;
		int count = 1;
		// If the type is month
		if( CalendarConstants.TYPE_MONTH.equals(type) ){
			calType = Calendar.MONTH;
			// If the type is week
		}else if( CalendarConstants.TYPE_WEEK.equals(type) ){
			count = 7;
		}
		//till date calculation
		for( int i=0; i<=interval; i++ ){
			incDate.setTime(tillDate);
			incDate.add(calType, count*skip);
			tillDate = (incDate.getTime());
			//interval++;		
		}
		return tillDate;
	}

	/**
	 * This method will return the next start date
	 * @param type
	 * @param ActualDate
	 * @param skip
	 * @return
	 */
	private Date getNextDate( String type, Date ActualDate, Integer skip ){
		Date nextDate = (Date)ActualDate.clone();
		Calendar incDate = Calendar.getInstance();
		int calType = Calendar.DATE;
		skip = skip == null || skip == 0 ? 1 : skip+1;
		int count = 1;
		// If the type is month
		if( CalendarConstants.TYPE_MONTH.equals(type) ){
			calType = Calendar.MONTH;
			// If the type is week
		}else if( CalendarConstants.TYPE_WEEK.equals(type) ){
			count = 7;
		}
		//next date calculation
		incDate.setTime(nextDate);
		incDate.add(calType, count*skip);
		nextDate = (incDate.getTime());	

		return nextDate;
	}

	/**
	 * 
	 */
	public void updateEvents(CommonError error, CalendarEventBean calendarEvent) {
		calendarDAO.updateAllEvents(error, calendarEvent);
	}


	/**
	 * This method splits the events for update and calling DAO to insert and update event
	 */
	public void splitEvents(CommonError error, CalendarEventBean calendarEvent) {

		int actualRelId = calendarEvent.getRelId();
		
		// Insert new split event
		calendarEvent.setRelId(null);
		calendarEvent.setType(null);
		calendarEvent.setCount(0);
		calendarEvent.setSkip(0);
		calendarEvent.setTillDate(null);
		calendarDAO.insertEvent(error, calendarEvent);
		
		calendarEvent.setTitle(calendarEvent.getActualTitle());
		calendarEvent.setCalId(calendarEvent.getActualCalId());
		calendarEvent.setType(calendarEvent.getActualType());
		calendarEvent.setSkip(calendarEvent.getActualSkip());
		calendarEvent.setRelId(actualRelId);
		
		// If first event getting split and moving separately
		if (calendarEvent.getEventIndex() == 0){

			Date nextStartDate = getNextDate(calendarEvent.getActualType(),calendarEvent.getActualStartDate(),calendarEvent.getActualSkip());
			Date nextEndDate = getNextDate(calendarEvent.getActualType(),calendarEvent.getActualEndDate(),calendarEvent.getActualSkip());

			calendarEvent = setEventValues(calendarEvent, nextStartDate, nextEndDate, null, calendarEvent.getActualTillDate());
			updateSplitEvent(error, calendarEvent);

		// If last event getting split and moving separately
		} else if (calendarEvent.getActualCount() == calendarEvent.getEventIndex()) {
			int newCount = calendarEvent.getActualCount()-1;

			calendarEvent = setEventValues(calendarEvent, calendarEvent.getActualStartDate(), calendarEvent.getActualEndDate(), newCount, null);
			updateSplitEvent(error, calendarEvent);
			
		// If in between event getting split and moving separately	
		}else if (calendarEvent.getEventIndex() != 0 && calendarEvent.getActualCount() != calendarEvent.getEventIndex()) {
			int newCount = calendarEvent.getEventIndex()-1;

			calendarEvent = setEventValues(calendarEvent, calendarEvent.getActualStartDate(), calendarEvent.getActualEndDate(), newCount, null);
			updateSplitEvent(error, calendarEvent);
			
			newCount = calendarEvent.getEventIndex();
			Date nextStartDate = getTillDate(calendarEvent.getActualType(),calendarEvent.getActualStartDate(), newCount, calendarEvent.getActualSkip());
			Date nextEndDate = getTillDate(calendarEvent.getActualType(),calendarEvent.getActualEndDate(), newCount, calendarEvent.getActualSkip());

			calendarEvent = setEventValues(calendarEvent, nextStartDate, nextEndDate, null, calendarEvent.getActualTillDate());
			insertEvent(error, calendarEvent);
		}

	}

	/**
	 * This method splits the events for deleting and calling DAO to insert and update event
	 */
	public void splitDeleteEvent(CommonError error, CalendarEventBean calendarEvent) {

		int actualRelId = calendarEvent.getRelId();
		
		calendarEvent.setTitle(calendarEvent.getActualTitle());
		calendarEvent.setCalId(calendarEvent.getActualCalId());
		calendarEvent.setType(calendarEvent.getActualType());
		calendarEvent.setSkip(calendarEvent.getActualSkip());
		calendarEvent.setRelId(actualRelId);
		
		// If first event getting delete and moving other events separately
		if (calendarEvent.getEventIndex() == 0){

			Date nextStartDate = getNextDate(calendarEvent.getActualType(),calendarEvent.getActualStartDate(),calendarEvent.getActualSkip());
			Date nextEndDate = getNextDate(calendarEvent.getActualType(),calendarEvent.getActualEndDate(),calendarEvent.getActualSkip());

			calendarEvent = setEventValues(calendarEvent, nextStartDate, nextEndDate, null, calendarEvent.getActualTillDate());
			updateSplitEvent(error, calendarEvent);

		// If last event getting delete and moving other events separately
		} else if (calendarEvent.getActualCount() == calendarEvent.getEventIndex()) {
			int newCount = calendarEvent.getActualCount()-1;

			calendarEvent = setEventValues(calendarEvent, calendarEvent.getActualStartDate(), calendarEvent.getActualEndDate(), newCount, null);
			updateSplitEvent(error, calendarEvent);
			
		// If in between event getting delete and moving other events separately	
		}else if (calendarEvent.getEventIndex() != 0 && calendarEvent.getActualCount() != calendarEvent.getEventIndex()) {
			int newCount = calendarEvent.getEventIndex()-1;

			calendarEvent = setEventValues(calendarEvent, calendarEvent.getActualStartDate(), calendarEvent.getActualEndDate(), newCount, null);
			updateSplitEvent(error, calendarEvent);
			
			newCount = calendarEvent.getEventIndex();
			Date nextStartDate = getTillDate(calendarEvent.getActualType(),calendarEvent.getActualStartDate(), newCount, calendarEvent.getActualSkip());
			Date nextEndDate = getTillDate(calendarEvent.getActualType(),calendarEvent.getActualEndDate(), newCount, calendarEvent.getActualSkip());

			calendarEvent = setEventValues(calendarEvent, nextStartDate, nextEndDate, null, calendarEvent.getActualTillDate());
			insertEvent(error, calendarEvent);
		}
		
	}
	
	/**
	 * This method perform to set the event values on calendarEvent
	 * @param calendarEvent
	 * @param nextStartDate
	 * @param nextEndDate
	 * @param count
	 * @param tilldate
	 * @return
	 */
	@SuppressWarnings("unused")
	private CalendarEventBean setEventValues(CalendarEventBean calendarEvent, Date nextStartDate, Date nextEndDate, Integer count, Date tilldate){
		calendarEvent.setStartDate(nextStartDate);
		calendarEvent.setEndDate(nextEndDate);
		calendarEvent.setCount(count);
		calendarEvent.setTillDate(tilldate);
		return calendarEvent;
		
	}
}
