package com.orb.calendar.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.calendar.bean.CalendarEventBean;
import com.orb.calendar.constants.CalendarDBConstants;
import com.orb.calendar.model.CalendarVal;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class CalendarDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(CalendarDAO.class);

	/**
	 * This method will return list of Calendar Events for the login user.
	 * @param error
	 * @param userId
	 * @return
	 */
	public List<CalendarEventBean> getCalendarEvents( CommonError error, List<String> selectedCal ){
		logger.info(" ==> getCalendarEvents START <== ");
		if( selectedCal == null || selectedCal.isEmpty() ){
			logger.info(" User doesn't provide any search criteria ");
			return null;
		}
		List<CalendarEventBean> calendarEventList = null;

		int size = selectedCal.size();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("REL_ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("USER_ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TITLE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TITLE"));qry.append(" as ACTUAL_TITLE ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));qry.append(" as ACTUAL_START_DATE ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("END_DATE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("END_DATE"));qry.append(" as ACTUAL_END_DATE ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("ALL_DAY"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));qry.append(" as ACTUAL_CAL_ID ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TYPE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TYPE"));qry.append(" as ACTUAL_TYPE ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("COUNT"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("COUNT"));qry.append(" as ACTUAL_COUNT ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TILL_DATE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TILL_DATE"));qry.append(" as ACTUAL_TILL_DATE ");qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("SKIP"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("SKIP"));qry.append(" as ACTUAL_SKIP ");
			qry.append(" FROM ");qry.append(CalendarDBConstants.TBL.get("CALENDAR_EVENT"));
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));
			qry.append(" IN (");
			for ( int i=0; i<size; i++ ) {
				qry.append(" ? ");qry.append(",");
			}
			qry.setLength(qry.length() - 1);
			qry.append(") ORDER BY ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));
			calendarEventList=getJdbcTemplate().query(qry.toString(),selectedCal.toArray(),new BeanPropertyRowMapper<CalendarEventBean>(CalendarEventBean.class));
		}catch( Exception e ){
			logger.info("Error when retrieve Calendar Events.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getCalendarEvents END <== ");
		return calendarEventList;
	}

	/**
	 * This method will insert Calendar Event in DB 
	 * @param error
	 * @param calendarEvent
	 */
	public void insertEvent( CommonError error, CalendarEventBean calendarEvent ){
		logger.info(" ==> insertEvent START <== ");
		try{
			List<Object> args = new ArrayList<Object>();
			if (null == calendarEvent.getRelId()){
				String nextRowId = getNextRowId( CalendarDBConstants.TBL.get("CALENDAR_EVENT") );
				args.add(Integer.valueOf(nextRowId));
			} else {
				args.add(calendarEvent.getRelId());
			}
			args.add(calendarEvent.getUserId());
			args.add(calendarEvent.getTitle());
			args.add(calendarEvent.getStartDate());
			args.add(calendarEvent.getEndDate());
			args.add(calendarEvent.getAllDay());
			args.add(calendarEvent.getType());
			args.add(calendarEvent.getCount());
			args.add(calendarEvent.getTillDate());
			args.add(calendarEvent.getSkip());
			args.add(calendarEvent.getCalId());


			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(CalendarDBConstants.TBL.get("CALENDAR_EVENT"));queryInsert.append("( ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("REL_ID"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("USER_ID"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("TITLE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("END_DATE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("ALL_DAY"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("TYPE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("COUNT"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("TILL_DATE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("SKIP"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));queryInsert.append(") VALUES(?,?,?,?,?,?,?,?,DATE_FORMAT(?,'%Y-%m-%d'),?,?) ");

			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
		}catch( Exception e ){
			logger.info("Error when Insert Calendar Event.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> insertEvent END <== ");
	}

	/**
	 * This method will update Calendar Event in DB
	 * @param error
	 * @param calendarEvent
	 */
	public void updateEvent( CommonError error, CalendarEventBean calendarEvent ){
		logger.info(" ==> updateEvent START <== ");
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");qry.append(CalendarDBConstants.TBL.get("CALENDAR_EVENT"));
			qry.append(" SET ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("USER_ID"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TITLE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("END_DATE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("ALL_DAY"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TYPE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("COUNT"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TILL_DATE"));qry.append(" = DATE_FORMAT(?,'%Y-%m-%d'),");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("SKIP"));qry.append(" = ?");
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("REL_ID"));qry.append(" = ?");
			qry.append(" AND ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("ID"));qry.append(" = ?");

			List<Object> args = new ArrayList<Object>();
			args.add(calendarEvent.getUserId());
			args.add(calendarEvent.getTitle());
			args.add(calendarEvent.getStartDate());
			args.add(calendarEvent.getEndDate());
			args.add(calendarEvent.getAllDay());
			args.add(calendarEvent.getCalId());
			args.add(calendarEvent.getType());
			args.add(calendarEvent.getCount());
			args.add(calendarEvent.getTillDate());
			args.add(calendarEvent.getSkip());
			args.add(calendarEvent.getRelId());
			args.add(calendarEvent.getId());

			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.info("Error when Update Calendar Event.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> updateEvent END <== ");
	}

	/**
	 * This method will delete Calendar Event from DB
	 * @param error
	 * @param calendarEvent
	 */
	public void deleteEvent( CommonError error, CalendarEventBean calendarEvent ){
		if( calendarEvent == null){
			logger.info("Can't delete calendar events");
			return;
		}
		logger.info(" ==> deleteEvent START <== ");
		try{
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(CalendarDBConstants.TBL.get("CALENDAR_EVENT"));
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("REL_ID"));qry.append("=? ");
			getJdbcTemplate().update(qry.toString(),calendarEvent.getRelId());
		}catch( Exception e ){
			logger.info("Error when Delete Calendar Event.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteEvent END <== ");
	}

	/**
	 * This method will create a new calendar room in DB
	 * @param error
	 * @param calendarEvent
	 */

	public void createCalendar(CommonError error, CalendarVal calAttributes) {
		if( calAttributes == null){
			logger.info("Can't create new calendar rooms");
			return;
		}
		// TODO Auto-generated method stub
		logger.info(" ==> insertCalendar START <== ");

		try{
			List<Object> args = new ArrayList<Object>();
			args.add(calAttributes.getValue());
			args.add(calAttributes.getType());
			args.add(calAttributes.getUserId());
			args.add(calAttributes.getFacilities());
			args.add(calAttributes.getTask());
			args.add(calAttributes.getColor());

			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(CalendarDBConstants.TBL.get("CALENDAR"));queryInsert.append("( ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("VALUE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("TYPE"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("USER_ID"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("FACILITIES"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("TASK"));queryInsert.append(", ");
			queryInsert.append(CalendarDBConstants.CALENDARS.get("COLOR"));queryInsert.append(") VALUES(?,?,?,?,?,?) ");

			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
		}catch( Exception e ){
			logger.info("Error when Insert Calendar.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> insertCalendar END <== ");

	}

	/**
	 * This method will fetch calendar rooms from DB
	 * @param error
	 * @param calendarEvent
	 */

	public List<CalendarVal> getCalendars(CommonError error, String calId) {
		logger.info(" ==> getCalendar START <== ");
		List<CalendarVal> calendarList = null;

		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append(CalendarDBConstants.CALENDARS.get("ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("VALUE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("TYPE"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("USER_ID"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("FACILITIES"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("TASK"));qry.append(", ");
			qry.append(CalendarDBConstants.CALENDARS.get("COLOR"));
			qry.append(" FROM ");qry.append(CalendarDBConstants.TBL.get("CALENDAR"));
			if (calId == null) {
				calendarList = getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<CalendarVal>(CalendarVal.class));
			} else {
				qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDARS.get("ID"));qry.append("=? ");
				calendarList = getJdbcTemplate().query(qry.toString(), new Object[]{calId}, new BeanPropertyRowMapper<CalendarVal>(CalendarVal.class));
			}
		}catch( Exception e ){
			logger.info("Error when retrieve Calendar Events.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getCalendar END <== ");
		return calendarList;
	}

	/**
	 * This method will update a existing calendar room in DB
	 * @param error
	 * @param calendarEvent
	 */

	public void updateCalendar(CommonError error, CalendarVal calAttributes) {
		if( calAttributes == null){
			logger.info("Can't update calendar rooms");
			return;
		}
		logger.info(" ==> updateCalendar START <== ");
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");qry.append(CalendarDBConstants.TBL.get("CALENDAR"));
			qry.append(" SET ");
			qry.append(CalendarDBConstants.CALENDARS.get("VALUE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDARS.get("TYPE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDARS.get("FACILITIES"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDARS.get("TASK"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDARS.get("COLOR"));qry.append(" = ?");
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDARS.get("ID"));qry.append(" = ?");

			List<Object> args = new ArrayList<Object>();    
			args.add(calAttributes.getValue());
			args.add(calAttributes.getType());
			args.add(calAttributes.getFacilities());
			args.add(calAttributes.getTask());
			args.add(calAttributes.getColor());
			args.add(calAttributes.getId());

			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.info("Error when Update Calendar.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> updateCalendar END <== ");

	}

	/**
	 * This method will delete calendar rooms from DB
	 * @param error
	 * @param calendarEvent
	 */
	public void deleteCalendar(CommonError error, CalendarVal calAttributes) {
		if( calAttributes == null){
			logger.info("Can't delete calendar rooms");
			return;
		}
		logger.info(" ==> deleteCalendar START <== ");
		try{
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(CalendarDBConstants.TBL.get("CALENDAR"));
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDARS.get("ID"));qry.append("=? ");
			getJdbcTemplate().update(qry.toString(),calAttributes.getId());
		}catch( Exception e ){
			logger.info("Error when Delete Calendar Event.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteCalendar END <== ");
	}

	public void updateAllEvents(CommonError error, CalendarEventBean calendarEvent) {
		if( calendarEvent == null){
			logger.info("Can't update events");
			return;
		}
		logger.info(" ==> updateAllEvents START <== ");
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");qry.append(CalendarDBConstants.TBL.get("CALENDAR_EVENT"));
			qry.append(" SET ");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("TITLE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("START_DATE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("END_DATE"));qry.append(" = ?,");
			qry.append(CalendarDBConstants.CALENDAR_EVENT.get("CAL_ID"));qry.append(" = ?");
			qry.append(" WHERE ");qry.append(CalendarDBConstants.CALENDAR_EVENT.get("REL_ID"));qry.append(" = ?");

			List<Object> args = new ArrayList<Object>();
			args.add(calendarEvent.getTitle());
			args.add(calendarEvent.getActualStartDate());
			args.add(calendarEvent.getActualEndDate());
			args.add(calendarEvent.getCalId());
			args.add(calendarEvent.getRelId());

			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.info("Error when Update Calendar Event.  " + e.getMessage());
			error.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> updateAllEvents END <== ");

	}

}
