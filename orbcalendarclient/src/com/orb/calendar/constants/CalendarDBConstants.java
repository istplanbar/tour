package com.orb.calendar.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class CalendarDBConstants {
	public static final Map<String,String> CALENDAR_EVENT;
	public static final Map<String,String> CALENDARS;
	public static final String TBL_CALENDAR_REPEATED_EVENT;
	public static final Map<String,String> CALENDAR_REPEATED_EVENT;
	public static final Map<String,String> TBL;
	
	static{
		CALENDAR_EVENT  = (Map<String,String>)CommonUtils.getBean("CALENDAR_EVENT_TABLE");
		CALENDARS  = (Map<String,String>)CommonUtils.getBean("CALENDAR_TABLE");
		TBL_CALENDAR_REPEATED_EVENT = CommonUtils.getPropertyValue("CALENDAR_REPEATED_EVENT_TABLE", "TABLE_NAME");
		CALENDAR_REPEATED_EVENT  = (Map<String,String>)CommonUtils.getBean("CALENDAR_REPEATED_EVENT_TABLE");
		TBL  = (Map<String,String>)CommonUtils.getBean("CALENDAR_TABLE_NAMES");
		
	}
}
