package com.orb.calendar.constants;

import com.orb.common.client.utils.CommonUtils;

public class CalendarConstants {
	
	public final static String CALENDAR_MULTI_VALUE_FIELD = "insapp" ;
	public final static String DATE_TIME_PATTERN ;
	public final static String TYPE = "N";
	public final static Integer COUNT = 0;
	public final static String DATETIME = "0000-00-00 00:00:00";
	public final static Integer SKIP = 0;
	public final static String TYPE_DAY = "D";
	public final static String TYPE_WEEK = "W";
	public final static Integer DAY = 1;
	public final static Integer WEEK = 7;
	public final static String TYPE_MONTH = "M";
	
	static{
		DATE_TIME_PATTERN = CommonUtils.getPropertyValue("customMapping", "DATE_TIME_PATTERN");
	}
}
