package com.orb.crm.service.impl;

/**
 * @author IST planbar / MaCh
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.crm.bean.AddNotes;
import com.orb.crm.bean.Crm;
import com.orb.crm.dao.CrmDAO;
import com.orb.crm.service.CrmService;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class CrmServiceImpl implements CrmService{
	
	private static Log logger = LogFactory.getLog(CrmServiceImpl.class);
	
	
	 @Autowired
	 CrmDAO crmDAO;
	 
	   /**
		 * This method calls DAO to get all active crm from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Crm> getActiveCrmList(CommonError commonError) {
			return crmDAO.getCrmList(commonError, CommonConstants.Y);
		}

		/**
		 * This method calls DAO to get all Inactive crm list from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Crm> getInactiveCrmList(CommonError commonError) {
			return crmDAO.getCrmList(commonError,CommonConstants.N);
		}

		/**
		 * This method will interact with DAO to insert a crm in DB
		 * @param commonError
		 * @param loginUser
		 * @param crm
		 */
		public void addCrm(CommonError commonError, User loginUser, Crm crm ) {
			crmDAO.addCrm(commonError, loginUser, crm );
		}
		
		/**
		 * This method will interact with DAO to get  crm in DB
		 * @param commonError
		 * @param crmId
		 * @return
		 */
		public Crm getCrm(CommonError commonError,String crmId){
			return crmDAO.getCrm(commonError, crmId);
		}
		
		/**
		 * This method will interact with DAO to update a crm in DB
		 * @param commonError
		 * @param loginUser
		 * @param crm
		 */
		@Override
		public void updateCrm(CommonError commonError, User loginUser, Crm crm ) {
			crmDAO.updateCrm(commonError, loginUser, crm );
		}
		
		/**
		 * This method will interact with DAO to delete a crm in DB
		 * @param commonError
		 * @param loginUser
		 * @param selectedCrmList
		 */
		@Override
		public void deleteCrm(CommonError commonError, User loginUser, List<Crm>  selectedCrmList ) {
			List<String> idList = new ArrayList<>();
			List<String> identifierList = new ArrayList<>();
			if( selectedCrmList == null || selectedCrmList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Crm crm : selectedCrmList ){
				idList.add(crm.getId());
				identifierList.add(crm.getIdentifier());
			}
			crmDAO.deleteCrm(commonError, loginUser, idList);
			crmDAO.deleteCrmAddNotes(commonError, loginUser, identifierList);
		}
		
		/**
		 * This method calls DAO to de-activate crm
		 * @param commonError
		 * @param loginUser
		 * @param crm
		 */
		@Override
		public void lockViewedCrm(CommonError commonError, User loginUser, Crm crm) {
			List<String> idList = new ArrayList<>();
			idList.add(crm.getId());
			crmDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );

		}
		
		/**
		 * This method calls DAO to activate crm
		 * @param commonError
		 * @param loginUser
		 * @param crm
		 */
		@Override
		public void unlockViewedCrm(CommonError commonError, User loginUser,Crm crm) {
			List<String> idList=new ArrayList<>();
			idList.add(crm.getId());
			crmDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );

		}

		/**
		 * This method calls DAO to deletes particular crm
		 * @param commonError
		 * @param loginUser
		 * @param crm
		 */
		@Override
		public void deleteViewedCrm(CommonError commonError, User loginUser, Crm crm) {
			List<String> idList = new ArrayList<>();
			List<String> identifierList = new ArrayList<>();
			idList.add(crm.getId());
			identifierList.add(crm.getIdentifier());
			crmDAO.deleteCrm(commonError, loginUser, idList);
			crmDAO.deleteCrmAddNotes(commonError, loginUser, identifierList);

		}
		
		/**
		 * This method calls DAO to activate the locked crm
		 * @param commonError
		 * @param loginUser
		 * @param selectedCrmList
		 */
		public void unlockCrm(CommonError commonError, User loginUser, List<Crm> selectedCrmList) {
			List<String> idList=new ArrayList<>();
			if( selectedCrmList == null || selectedCrmList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Crm crm : selectedCrmList ){
				idList.add(crm.getId());
			}
			crmDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);
		}

		/**
		 * This method calls DAO to de-activate crm
		 * @param commonError
		 * @param loginUser
		 * @param selectedCrmList
		 */
		@Override
		public void lockCrm(CommonError commonError, User loginUser, List<Crm> selectedCrmList) {
			List<String> idList=new ArrayList<>();
			if( selectedCrmList == null || selectedCrmList.isEmpty() ){
				return;
			}
			for( Crm crm : selectedCrmList ){
				idList.add(crm.getId());
			}
			crmDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );
		}

		/**
		 * This method will interact with DAO to insert a crm addNotes in DB
		 * @param commonError
		 * @param loginUser
		 * @param addNotes
		 */
		@Override
		public void addAddNotes(CommonError commonError, User loginUser,AddNotes addNotes) {
			crmDAO.addAddNotes(commonError, loginUser,  addNotes);
		}
		
		/**
		 * This method will interact with DAO to get crm addNotes in DB
		 * @param commonError
		 * @param loginUser
		 * @param crmId
		 */
		public List<AddNotes> getAddNotes(CommonError commonError,String crmId){
			return crmDAO.getAddNotes(commonError, crmId);
		}
				
		
}
