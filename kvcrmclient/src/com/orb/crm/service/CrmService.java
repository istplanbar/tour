package com.orb.crm.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.crm.bean.AddNotes;
import com.orb.crm.bean.Crm;

public interface CrmService {
	
	public List<Crm> getActiveCrmList(CommonError commonError);
	public List<Crm> getInactiveCrmList(CommonError commonError);
	public  void addCrm(CommonError commonError, User loginUser, Crm crm);
	public Crm getCrm(CommonError commonError,String crmId);
	public void updateCrm( CommonError commonError, User loginUser, Crm crm );
	public void deleteCrm(CommonError commonError, User loginUser, List<Crm> selectedCrmList);
	public void lockViewedCrm(CommonError commonError, User loginUser, Crm crm);
	public void unlockViewedCrm(CommonError commonError, User loginUser, Crm crm);
	public void deleteViewedCrm(CommonError commonError, User loginUser, Crm crm);
	public void lockCrm(CommonError commonError, User loginUser, List<Crm> selectedCrmList);
	public void unlockCrm(CommonError commonError, User loginUser, List<Crm> selectedCrmList);
	public List<AddNotes> getAddNotes(CommonError commonError,String crmId);
	public void addAddNotes(CommonError commonError, User loginUser, AddNotes addNotes);

}