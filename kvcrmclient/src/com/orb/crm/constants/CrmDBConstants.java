package com.orb.crm.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;





public class CrmDBConstants {
	
	public static final String TBL_CRM,TBL_CRM_HISTORY;
	public static final Map<String,String> CRM,CRM_HISTORY;
	public static final Map<String,String> crmDBConfig;
	
	public static final String TBL_CRM_ADD_NOTES,TBL_CRM_ADD_NOTES_HISTORY;
	public static final Map<String,String> CRM_ADD_NOTES,CRM_ADD_NOTES_HISTORY;
	
	
	
	static{
			
		TBL_CRM=CommonUtils.getPropertyValue("CRM_TABLE_NAMES", "MOD_CRM");
		TBL_CRM_HISTORY=CommonUtils.getPropertyValue("CRM_TABLE_NAMES", "MOD_CRM_HISTORY");
		
		CRM=(Map<String,String>)CommonUtils.getBean("MOD_CRM");
		CRM_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_CRM_HISTORY");
		
		crmDBConfig=(Map<String,String>)CommonUtils.getBean("crmDBConfig");
		
		TBL_CRM_ADD_NOTES=CommonUtils.getPropertyValue("CRM_TABLE_NAMES", "MOD_CRM_ADD_NOTES");
		TBL_CRM_ADD_NOTES_HISTORY=CommonUtils.getPropertyValue("CRM_TABLE_NAMES", "MOD_CRM_ADD_NOTES_HISTORY");
		
		CRM_ADD_NOTES=(Map<String,String>)CommonUtils.getBean("MOD_CRM_ADD_NOTES");
		CRM_ADD_NOTES_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_CRM_ADD_NOTES_HISTORY");
	}
	

}
