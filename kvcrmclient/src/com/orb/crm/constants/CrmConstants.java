package com.orb.crm.constants;

public class CrmConstants {
	
	public final static String CRM_EDIT_PAGE="/common/process/crm/manipulateCrm.html";
	public final static String CRM_VIEW_PAGE="/common/process/crm/viewCrm.html";
	public final static String CRM_LIST_PAGE="/common/process/crm/loadCrm.html";
	public final static String SELECTED_CRM_ID="SELECTED_CRM_ID";
	public final static String CRM_EDIT_HTML="crm/addUpdate.xhtml";
	public final static String CRM_VIEW_HTML="crm/view.xhtml";
	public final static String CRM_ADDED_SUCCESS="crm_added_success";
	public final static String CRM_UPDATED_SUCCESS="crm_update_success";
	public final static String CRM_DELETE_SUCCESS="crm_delete_success";
	public final static String CRM_LOCKED_SUCCESS="crm_locked_success";
	public final static String CRM_UNLOCKED_SUCCESS="crm_unlocked_success";
	public final static String CRM_TOGGLER_LIST = "CRM_TOGGLER_LIST" ;
	public final static String SHOW_ACTIVE_CRM="SHOW_ACTIVE_CRM";
	public final static String CRM_DEFAULT_LIST_HTML="crm/crmList.xhtml";
	public final static String CRM_LIST_VIEW = "LV";
	public final static String CURRENT_VIEW = "currentView" ;
	public final static String CRM_DETAIL_VIEW = "DV";
	public final static String CRM_ADDNOTES_ADDED_SUCCESS="crm_addNotes_added_successfully";
	public final static String DEFAULT_COUNTRY_CODE = "0";

}