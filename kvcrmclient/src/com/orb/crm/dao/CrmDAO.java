package com.orb.crm.dao;

/**
 * @author IST planbar / MaCh
 */

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.crm.bean.AddNotes;
import com.orb.crm.bean.Crm;
import com.orb.crm.constants.CrmDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class CrmDAO extends AbstractDAO{
	
	private static Log logger = LogFactory.getLog(CrmDAO.class);
	
	
	/**
	 * This method will return list of Crm from DB
	 * @param commonError
	 * @param activeFlag
	 * @return
	 */
	public List<Crm> getCrmList(CommonError commonError, String activeFlag) {
		logger.info(" ==> Get CrmList START <== ");
		List<Crm> crmList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : CrmDBConstants.CRM.keySet() ){
				newQry.append( CrmDBConstants.CRM.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" WHERE ");
			newQry.append(CrmDBConstants.CRM.get("ACTIVE"));newQry.append(" = ? ");
			crmList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Crm.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tried to fetch Crm list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get CrmList END <== ");
		return crmList;
	}
	
	/**
	 * This method inserts new crm into DB
	 * @param commonError
	 * @param loginUser
	 * @param crm
	 */
    public void addCrm(CommonError commonError, User loginUser,Crm crm) {
		logger.info(" ==> Insert Crm START <== ");	
		if(crm == null){
			logger.info("Can't insert crm");
		}
		
		try{
			insertRecord(loginUser, crm,CrmDBConstants.TBL_CRM,CrmDBConstants.crmDBConfig.get("rowPrefix"));	
		}catch( Exception e ){
			logger.info("Error when Insert Crm.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Crm END <== ");
	}
	
	/**
	 * This method used to get a particular Crm from DB
	 * @param commonError
	 * @param crmId
	 * @return
	 */
	public Crm getCrm(CommonError commonError, String crmId) {
		logger.info(" ==> Get a Crm START <== ");
		Crm crm = null;
		Crm lockedHist = null;
		Crm unlockedHist = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : CrmDBConstants.CRM.keySet() ){
				newQry.append( CrmDBConstants.CRM.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" WHERE ");
			newQry.append( CrmDBConstants.CRM.get("IDENTIFIER") );newQry.append( " = ? " );
			crm = (Crm)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{crmId}, new BeanPropertyRowMapper(Crm.class) );
			
			lockedHist = getUserAction(commonError, crm.getIdentifier(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				crm.setLockedBy( lockedHist.getUpdateBy());
				crm.setLockedTime(  lockedHist.getUpdateTime());
			}
			
			unlockedHist = getUserAction(commonError, crm.getIdentifier(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				crm.setUnlockedBy( unlockedHist.getUpdateBy());
				crm.setUnlockedTime( unlockedHist.getUpdateTime());
			}	
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Crm. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get a Crm END <== ");
		return crm;
	}
	
	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return
	 */
     private Crm getUserAction(CommonError commonError,String id, String action){
		Crm historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : CrmDBConstants.CRM_HISTORY.keySet() ){
			newQryHist.append( CrmDBConstants.CRM_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(CrmDBConstants.TBL_CRM_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( CrmDBConstants.CRM_HISTORY.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");newQryHist.append(CrmDBConstants.CRM_HISTORY.get("ACTION"));
		newQryHist.append(" =? ");
		newQryHist.append(" ORDER BY ");newQryHist.append(CrmDBConstants.CRM_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Crm)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Crm.class) );
		}
		catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}
	
	/**
	 * This method updates existing Crm in DB
	 * @param commonError
	 * @param loginUser
	 * @param crm
	 */
	public void updateCrm(CommonError commonError, User loginUser, Crm crm) {
		logger.info(" ==> Update Crm START <== ");
		try{
			updateRecordById(loginUser, crm,CrmDBConstants.TBL_CRM);
		}catch( Exception e ){
			logger.info("Error when Update Crm.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Crm END <== ");
    }
	
	/**
	 * This method delete the crm into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteCrm(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete CRM START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertCrmHistory( commonError, loginUser,idList,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" WHERE ");
			newQry.append( CrmDBConstants.CRM.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the crm. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete CRM END <== ");

	}
	
	/**
	 * This method updates Crm into Crm history table
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	public void insertCrmHistory(CommonError commonError, User loginUser,List<String> idList, String action) throws Exception {
		logger.info(" ==> Insert Crm History START <== ");
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : CrmDBConstants.CRM.keySet() ){
		  newQryHist.append( CrmDBConstants.CRM.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(CrmDBConstants.TBL_CRM);newQryHist.append(" WHERE ");newQryHist.append( CrmDBConstants.CRM.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Crm> crmList = (List<Crm>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Crm>(Crm.class));
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( CrmDBConstants.TBL_CRM_HISTORY);newQry.append("( ");
			for( String column : CrmDBConstants.CRM_HISTORY.keySet() ){
				if(!column.equals(CrmDBConstants.CRM_HISTORY.get("ID"))){
				newQry.append( CrmDBConstants.CRM_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, crmList.get(i).getIdentifier());ps.setString(2, crmList.get(i).getPicture());
					ps.setString(3, crmList.get(i).getFirstName());ps.setString(4, crmList.get(i).getSurName());
					ps.setString(5, crmList.get(i).getGender());ps.setString(6, crmList.get(i).getTitle());
					ps.setString(7, crmList.get(i).getTelephone());ps.setString(8, crmList.get(i).getMobile());
					ps.setString(9, crmList.get(i).getMail());ps.setString(10, crmList.get(i).getWebsite());
					ps.setString(11, crmList.get(i).getStreet());ps.setString(12, crmList.get(i).getHouseNumber());
					ps.setString(13, crmList.get(i).getAdditionalAddress());ps.setString(14, crmList.get(i).getCareOf());
					ps.setString(15, crmList.get(i).getZipCode());ps.setString(16, crmList.get(i).getPlace());
					ps.setString(17, crmList.get(i).getCountry());ps.setString(18, crmList.get(i).getOutpatientInpatientFacility());
					ps.setString(19, crmList.get(i).getCooperationPartners());ps.setString(20, crmList.get(i).getCity());
					ps.setString(21, crmList.get(i).getJobPosition());ps.setString(22, crmList.get(i).getFax());
					ps.setString(23, crmList.get(i).getState());ps.setString(24, crmList.get(i).getCompany());
					ps.setString(25, crmList.get(i).getTags());ps.setString(26, crmList.get(i).getType());
					ps.setString(27, crmList.get(i).getBranch());ps.setString(28, loginUser.getUserId());
					ps.setTimestamp(29, date);ps.setString(30, loginUser.getUserId());
					ps.setTimestamp(31, date);ps.setString(32, action);
					
				}		
				public int getBatchSize() {
					return crmList.size();
				}
			});
			
			
		}catch( Exception e ){
			logger.info("Error when Insert Crm History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> Insert Crm History END <== ");
	
	}
	
	
	/**
	 * This method update crm addNotes into crm addNotes history table
	 * @param commonError
	 * @param loginUser
	 * @param identifierList
	 * @param action
	 */
	public void insertCrmAddNotesHistory(CommonError commonError, User loginUser,List<String> identifierList, String action) throws Exception {
		logger.info(" ==> Insert Crm Add Notes History START <== ");
		logger.info("Crm Add Notes Id ==> " + identifierList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : CrmDBConstants.CRM_ADD_NOTES.keySet() ){
		newQryHist.append( CrmDBConstants.CRM_ADD_NOTES.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(CrmDBConstants.TBL_CRM_ADD_NOTES);newQryHist.append(" WHERE ");newQryHist.append( CrmDBConstants.CRM_ADD_NOTES.get("CRM_IDENTIFIER") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<identifierList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<AddNotes> addNotesList = (List<AddNotes>)getJdbcTemplate().query(newQryHist.toString(), identifierList.toArray(),  new BeanPropertyRowMapper<AddNotes>(AddNotes.class));
			
			try{
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("INSERT INTO ");newQry.append( CrmDBConstants.TBL_CRM_ADD_NOTES_HISTORY);newQry.append("( ");
				for( String column : CrmDBConstants.CRM_ADD_NOTES_HISTORY.keySet() ){
					if(!column.equals(CrmDBConstants.CRM_ADD_NOTES_HISTORY.get("ID"))){
					newQry.append( CrmDBConstants.CRM_ADD_NOTES_HISTORY.get(column) );newQry.append( "," );
					}
				}
				newQry.setLength(newQry.length()-1);
				newQry.append(") VALUES(?,?,?,?,?,?,?,?,?) ");
				
				getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, addNotesList.get(i).getIdentifier());ps.setString(2, addNotesList.get(i).getCrmIdentifier());
					ps.setString(3, addNotesList.get(i).getName());ps.setString(4, addNotesList.get(i).getNotes());
					ps.setString(5, loginUser.getUserId());
					ps.setTimestamp(6, date);ps.setString(7, loginUser.getUserId());
					ps.setTimestamp(8, date);ps.setString(9, action);
				}
				
				public int getBatchSize() {
					return addNotesList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when Insert Crm Add Notes History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
	
		
		logger.info(" ==> Insert Crm Add Notes History END <== ");
	
     }
	
	/**
	 * This method will activate or deactivate the crm
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	public void updateActiveStatus( CommonError commonError, User loginUser, List<String> idList, String action ){
		logger.info(" ==> updateActiveStatus START <== ");
		
		try{
			if(CommonConstants.Y.equals(action)){
				insertCrmHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}else{
				insertCrmHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( CrmDBConstants.TBL_CRM );qry.append(" SET ");
			qry.append( CrmDBConstants.CRM.get("ACTIVE") );qry.append( "=?");
			qry.append(" WHERE ");
			qry.append( CrmDBConstants.CRM.get("ID") );qry.append( "=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, action);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
			});
			
		}
		catch( Exception e ){
			logger.info("Error when updateActiveStatus of a crm.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");
	}
	
	
	/**
	 * This method will add Add Notes
	 * @param commonError
	 * @param loginUser
	 * @param addNotes
	 */

	public void addAddNotes(CommonError commonError, User loginUser,AddNotes addNotes) {
		logger.info(" ==> Insert Crm Add Notes START <== ");
		
		try{
			insertRecord(loginUser, addNotes,CrmDBConstants.TBL_CRM_ADD_NOTES,CrmDBConstants.crmDBConfig.get("rowPrefix"));
			
		}catch( Exception e ){
			logger.info("Error when Insert Crm Add Notes.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Crm Add Notes END <== ");
	}
	
	/**
	 * This method used to get a Add Notes from DB
	 * @param commonError
	 * @param crmId
	 * @return
	 */
	public List<AddNotes> getAddNotes(CommonError commonError, String crmId) {
		List<AddNotes> addNotesList = null;
		logger.info(" ==> Get a Crm Add Notes START <== ");
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : CrmDBConstants.CRM_ADD_NOTES.keySet() ){
				newQry.append( CrmDBConstants.CRM_ADD_NOTES.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(CrmDBConstants.TBL_CRM_ADD_NOTES);newQry.append(" WHERE ");
			newQry.append( CrmDBConstants.CRM_ADD_NOTES.get("CRM_IDENTIFIER") );newQry.append( " = ? " );
			newQry.append(" ORDER BY ");newQry.append( CrmDBConstants.CRM_ADD_NOTES.get("ID") );
			newQry.append(" DESC ");
			addNotesList = (List<AddNotes>)getJdbcTemplate().query( newQry.toString(),new String[]{crmId}, new BeanPropertyRowMapper(AddNotes.class) );
			
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Crm Add Notes. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> Get a Crm Add Notes END <== ");
		return addNotesList;
	}
	
	/**
	 * This method delete the crm add notes from DB
	 * @param commonError
	 * @param loginUser
	 * @param identifierList
	 */
	public void deleteCrmAddNotes(CommonError commonError, User loginUser, List<String> identifierList) {
		logger.info(" ==> Delete Crm Add Notes START <== ");
		
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertCrmAddNotesHistory(commonError,loginUser,identifierList,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", identifierList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(CrmDBConstants.TBL_CRM_ADD_NOTES);newQry.append(" WHERE ");
			newQry.append( CrmDBConstants.CRM_ADD_NOTES.get("CRM_IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete crm addNotes. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete CRM Add Notes END <== ");

	}
	

}
