package com.orb.facilities.bean;

import java.util.Date;

public class MaintenanceAct {
	private String id;
	private String identifier;
	private String facilityIdentifier;
	private Date maintenanceDate;
	private String maId;
	private String referenceNumber;
	private String shortComings;
	private String subsequentProblems;
	private String waterDepth;
	private String chamberBs1;
	private String chamberSs1;
	private String chamberBs2;
	private String chamberSs2;
	private String chamberBs3;
	private String chamberSs3;
	private String report;
	private String structuralCondition;
	private String surfaceInspection;
	private String tankInspection;
	private String waterTightness;
	private String waterFilling;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String checked;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	
	public String getFacilityIdentifier() {
		return facilityIdentifier;
	}
	public void setFacilityIdentifier(String facilityIdentifier) {
		this.facilityIdentifier = facilityIdentifier;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public Date getMaintenanceDate() {
		return maintenanceDate;
	}
	public void setMaintenanceDate(Date maintenanceDate) {
		this.maintenanceDate = maintenanceDate;
	}
	public String getMaId() {
		return maId;
	}
	public void setMaId(String maId) {
		this.maId = maId;
	}
	public String getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getShortComings() {
		return shortComings;
	}
	public void setShortComings(String shortComings) {
		this.shortComings = shortComings;
	}
	public String getSubsequentProblems() {
		return subsequentProblems;
	}
	public void setSubsequentProblems(String subsequentProblems) {
		this.subsequentProblems = subsequentProblems;
	}
	public String getWaterDepth() {
		return waterDepth;
	}
	public void setWaterDepth(String waterDepth) {
		this.waterDepth = waterDepth;
	}
	public String getChamberBs1() {
		return chamberBs1;
	}
	public void setChamberBs1(String chamberBs1) {
		this.chamberBs1 = chamberBs1;
	}
	public String getChamberSs1() {
		return chamberSs1;
	}
	public void setChamberSs1(String chamberSs1) {
		this.chamberSs1 = chamberSs1;
	}
	public String getChamberBs2() {
		return chamberBs2;
	}
	public void setChamberBs2(String chamberBs2) {
		this.chamberBs2 = chamberBs2;
	}
	public String getChamberSs2() {
		return chamberSs2;
	}
	public void setChamberSs2(String chamberSs2) {
		this.chamberSs2 = chamberSs2;
	}
	public String getChamberBs3() {
		return chamberBs3;
	}
	public void setChamberBs3(String chamberBs3) {
		this.chamberBs3 = chamberBs3;
	}
	public String getChamberSs3() {
		return chamberSs3;
	}
	public void setChamberSs3(String chamberSs3) {
		this.chamberSs3 = chamberSs3;
	}
	public String getReport() {
		return report;
	}
	public void setReport(String report) {
		this.report = report;
	}
	public String getStructuralCondition() {
		return structuralCondition;
	}
	public void setStructuralCondition(String structuralCondition) {
		this.structuralCondition = structuralCondition;
	}
	public String getSurfaceInspection() {
		return surfaceInspection;
	}
	public void setSurfaceInspection(String surfaceInspection) {
		this.surfaceInspection = surfaceInspection;
	}
	public String getTankInspection() {
		return tankInspection;
	}
	public void setTankInspection(String tankInspection) {
		this.tankInspection = tankInspection;
	}
	public String getWaterTightness() {
		return waterTightness;
	}
	public void setWaterTightness(String waterTightness) {
		this.waterTightness = waterTightness;
	}
	public String getWaterFilling() {
		return waterFilling;
	}
	public void setWaterFilling(String waterFilling) {
		this.waterFilling = waterFilling;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	
	
	
}
