package com.orb.facilities.bean;

import java.util.Date;

public class MaintenanceControl {
	private String id;
	private String identifier;
	private String functionControl;
	private String machineMaintenance;
	private String changeoverTimes;
	private String lastReportTransfer;
	private String companyBookExamination;
	private String duration1;
	private String duration2;
	private String duration3;
	private String duration4;
	private String odor;
	private String temperature;
	private String phValue;
	private String deepView;
	private String oxygenContent;
	private String sampleNumber;
	private String sampleComment;
	private String distributorChannelCleaning;
	private String repairFrom;
	private String powerFailureDetection; 
	private String constructionSiteCompleted;
	private String remarks;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String checked;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getFunctionControl() {
		return functionControl;
	}
	public void setFunctionControl(String functionControl) {
		this.functionControl = functionControl;
	}
	public String getMachineMaintenance() {
		return machineMaintenance;
	}
	public void setMachineMaintenance(String machineMaintenance) {
		this.machineMaintenance = machineMaintenance;
	}
	public String getChangeoverTimes() {
		return changeoverTimes;
	}
	public void setChangeoverTimes(String changeoverTimes) {
		this.changeoverTimes = changeoverTimes;
	}
	public String getLastReportTransfer() {
		return lastReportTransfer;
	}
	public void setLastReportTransfer(String lastReportTransfer) {
		this.lastReportTransfer = lastReportTransfer;
	}
	public String getCompanyBookExamination() {
		return companyBookExamination;
	}
	public void setCompanyBookExamination(String companyBookExamination) {
		this.companyBookExamination = companyBookExamination;
	}
	public String getDuration1() {
		return duration1;
	}
	public void setDuration1(String duration1) {
		this.duration1 = duration1;
	}
	public String getDuration2() {
		return duration2;
	}
	public void setDuration2(String duration2) {
		this.duration2 = duration2;
	}
	public String getDuration3() {
		return duration3;
	}
	public void setDuration3(String duration3) {
		this.duration3 = duration3;
	}
	public String getDuration4() {
		return duration4;
	}
	public void setDuration4(String duration4) {
		this.duration4 = duration4;
	}
	public String getOdor() {
		return odor;
	}
	public void setOdor(String odor) {
		this.odor = odor;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getPhValue() {
		return phValue;
	}
	public void setPhValue(String phValue) {
		this.phValue = phValue;
	}
	public String getDeepView() {
		return deepView;
	}
	public void setDeepView(String deepView) {
		this.deepView = deepView;
	}
	public String getOxygenContent() {
		return oxygenContent;
	}
	public void setOxygenContent(String oxygenContent) {
		this.oxygenContent = oxygenContent;
	}
	public String getSampleNumber() {
		return sampleNumber;
	}
	public void setSampleNumber(String sampleNumber) {
		this.sampleNumber = sampleNumber;
	}
	public String getSampleComment() {
		return sampleComment;
	}
	public void setSampleComment(String sampleComment) {
		this.sampleComment = sampleComment;
	}
	public String getDistributorChannelCleaning() {
		return distributorChannelCleaning;
	}
	public void setDistributorChannelCleaning(String distributorChannelCleaning) {
		this.distributorChannelCleaning = distributorChannelCleaning;
	}
	public String getRepairFrom() {
		return repairFrom;
	}
	public void setRepairFrom(String repairFrom) {
		this.repairFrom = repairFrom;
	}
	public String getPowerFailureDetection() {
		return powerFailureDetection;
	}
	public void setPowerFailureDetection(String powerFailureDetection) {
		this.powerFailureDetection = powerFailureDetection;
	}
	public String getConstructionSiteCompleted() {
		return constructionSiteCompleted;
	}
	public void setConstructionSiteCompleted(String constructionSiteCompleted) {
		this.constructionSiteCompleted = constructionSiteCompleted;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	
	
}
