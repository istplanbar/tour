package com.orb.facilities.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.orb.taskplanner.bean.Task;

public class Facilities {
	private String id;
	private String identifier;
	private String name;
	private String clientId;
	private String customerNumber;
	private String qualificationsId;
	private String sewageTreatmentType;
	private String blowerType;
	private String constructionYear;
	private String buildingSize;
	private String serialNo;
	private String maintenanceCycle;
	private String monthOfMaintenance;
	private String visitingDay;
	private Date lastVisit;
	private String buildingPlan;
	private String primaryTreatment;
	private String costWithoutOffer;
	private String cleaningStandingOrder;
	private String powerFailureDetection;
	private String controlFreeAccess;
	private String announcementToUwbme;
	private String reportsResultsUwb;
	private String reportsResultsCity;
	private String reportsResultsCustomer;
	private String locationOfTheFacility;
	private String facilityOperator;
	private String instructionGiven;
	private String fitter;
	private String connectionSize;
	private String control;
	private String numberOfContainer;
	private String waterDepth;
	private String diameter;
	private String crashInPrimaryTreatment;
	private String otherForms;
	private String remarks;
	private String maintenanceActId;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String checked;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	private String freelyAccessible;
	private String docket;
	private String password;
	private String revival;
	private String buffer;
	private Date waterRightsPermitUntil;
	private String billingMode;
	private String blocked;
	private String naviAddress;
	private String street;
	private String houseNumber;
	private String postCode;
	private String place;
	
	private String locationStreet;
	private String locationHouseNumber;
	private String locationPostCode;
	private String locationPlace;
	private String locationDistrict;
	
	private String easting;
	private String northing;
	private String degreeOfLongitude;
	private String degreeOfLatitude;
	private String areaOfMunicipality;
	private String hall;
	private String parcel;
	
	private String bsb5;
	private String csb;
	private String nges;
	private String nh4;
	private String pges;
	private String afs;
	private String ass;
	private String phValueFrom;
	private String phValueTo;
	private String bacteriaFrom;
	private String bacteriaTo;
	
	private double facilityLatitude;
	private double facilityLongitude;
	private List<Task> taskAndWorkHoursList;
	//Anand added for visiting hours at 02-06-2018
	private String visitingHours;
	
	// Selva Added for visiting time at 04-06-2016
	private String visitingTime;
	
	private String reminder;
	
	
	
	public String getReminder() {
		return reminder;
	}
	public void setReminder(String reminder) {
		this.reminder = reminder;
	}
	
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getMonthOfMaintenance() {
		return monthOfMaintenance;
	}
	public void setMonthOfMaintenance(String monthOfMaintenance) {
		this.monthOfMaintenance = monthOfMaintenance;
	}
	public List<Task> getTaskAndWorkHoursList() {
		return taskAndWorkHoursList;
	}
	public void setTaskAndWorkHoursList(List<Task> taskAndWorkHoursList) {
		this.taskAndWorkHoursList = taskAndWorkHoursList;
	}
	
	public double getFacilityLatitude() {
		return facilityLatitude;
	}
	public void setFacilityLatitude(double facilityLatitude) {
		this.facilityLatitude = facilityLatitude;
	}
	public double getFacilityLongitude() {
		return facilityLongitude;
	}
	public void setFacilityLongitude(double facilityLongitude) {
		this.facilityLongitude = facilityLongitude;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getQualificationsId() {
		return qualificationsId;
	}
	public void setQualificationsId(String qualificationsId) {
		this.qualificationsId = qualificationsId;
	}
	public String getSewageTreatmentType() {
		return sewageTreatmentType;
	}
	public void setSewageTreatmentType(String sewageTreatmentType) {
		this.sewageTreatmentType = sewageTreatmentType;
	}
	public String getBlowerType() {
		return blowerType;
	}
	public void setBlowerType(String blowerType) {
		this.blowerType = blowerType;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getMaintenanceCycle() {
		return maintenanceCycle;
	}
	public void setMaintenanceCycle(String maintenanceCycle) {
		this.maintenanceCycle = maintenanceCycle;
	}
	public String getVisitingDay() {
		return visitingDay;
	}
	public void setVisitingDay(String visitingDay) {
		this.visitingDay = visitingDay;
	}
	public Date getLastVisit() {
		return lastVisit;
	}
	public void setLastVisit(Date lastVisit) {
		this.lastVisit = lastVisit;
	}
	public String getPrimaryTreatment() {
		return primaryTreatment;
	}
	public void setPrimaryTreatment(String primaryTreatment) {
		this.primaryTreatment = primaryTreatment;
	}
	public String getAnnouncementToUwbme() {
		return announcementToUwbme;
	}
	public void setAnnouncementToUwbme(String announcementToUwbme) {
		this.announcementToUwbme = announcementToUwbme;
	}
	public String getLocationOfTheFacility() {
		return locationOfTheFacility;
	}
	public void setLocationOfTheFacility(String locationOfTheFacility) {
		this.locationOfTheFacility = locationOfTheFacility;
	}
	
	public String getFitter() {
		return fitter;
	}
	public void setFitter(String fitter) {
		this.fitter = fitter;
	}
	public String getConnectionSize() {
		return connectionSize;
	}
	public void setConnectionSize(String connectionSize) {
		this.connectionSize = connectionSize;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public String getNumberOfContainer() {
		return numberOfContainer;
	}
	public void setNumberOfContainer(String numberOfContainer) {
		this.numberOfContainer = numberOfContainer;
	}
	public String getWaterDepth() {
		return waterDepth;
	}
	public void setWaterDepth(String waterDepth) {
		this.waterDepth = waterDepth;
	}
	public String getDiameter() {
		return diameter;
	}
	public void setDiameter(String diameter) {
		this.diameter = diameter;
	}
	public String getOtherForms() {
		return otherForms;
	}
	public void setOtherForms(String otherForms) {
		this.otherForms = otherForms;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getMaintenanceActId() {
		return maintenanceActId;
	}
	public void setMaintenanceActId(String maintenanceActId) {
		this.maintenanceActId = maintenanceActId;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getConstructionYear() {
		return constructionYear;
	}
	public void setConstructionYear(String constructionYear) {
		this.constructionYear = constructionYear;
	}
	public String getBuildingSize() {
		return buildingSize;
	}
	public void setBuildingSize(String buildingSize) {
		this.buildingSize = buildingSize;
	}
	public String getBuildingPlan() {
		return buildingPlan;
	}
	public void setBuildingPlan(String buildingPlan) {
		this.buildingPlan = buildingPlan;
	}
	public String getCostWithoutOffer() {
		return costWithoutOffer;
	}
	public void setCostWithoutOffer(String costWithoutOffer) {
		this.costWithoutOffer = costWithoutOffer;
	}
	public String getPowerFailureDetection() {
		return powerFailureDetection;
	}
	public void setPowerFailureDetection(String powerFailureDetection) {
		this.powerFailureDetection = powerFailureDetection;
	}
	public String getReportsResultsUwb() {
		return reportsResultsUwb;
	}
	public void setReportsResultsUwb(String reportsResultsUwb) {
		this.reportsResultsUwb = reportsResultsUwb;
	}
	public String getReportsResultsCity() {
		return reportsResultsCity;
	}
	public void setReportsResultsCity(String reportsResultsCity) {
		this.reportsResultsCity = reportsResultsCity;
	}
	public String getReportsResultsCustomer() {
		return reportsResultsCustomer;
	}
	public void setReportsResultsCustomer(String reportsResultsCustomer) {
		this.reportsResultsCustomer = reportsResultsCustomer;
	}
	public String getFacilityOperator() {
		return facilityOperator;
	}
	public void setFacilityOperator(String facilityOperator) {
		this.facilityOperator = facilityOperator;
	}
	public String getInstructionGiven() {
		return instructionGiven;
	}
	public void setInstructionGiven(String instructionGiven) {
		this.instructionGiven = instructionGiven;
	}
	public String getCrashInPrimaryTreatment() {
		return crashInPrimaryTreatment;
	}
	public void setCrashInPrimaryTreatment(String crashInPrimaryTreatment) {
		this.crashInPrimaryTreatment = crashInPrimaryTreatment;
	}
	public String getCleaningStandingOrder() {
		return cleaningStandingOrder;
	}
	public void setCleaningStandingOrder(String cleaningStandingOrder) {
		this.cleaningStandingOrder = cleaningStandingOrder;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	public String getControlFreeAccess() {
		return controlFreeAccess;
	}
	public void setControlFreeAccess(String controlFreeAccess) {
		this.controlFreeAccess = controlFreeAccess;
	}
	public String getFreelyAccessible() {
		return freelyAccessible;
	}
	public void setFreelyAccessible(String freelyAccessible) {
		this.freelyAccessible = freelyAccessible;
	}
	public String getDocket() {
		return docket;
	}
	public void setDocket(String docket) {
		this.docket = docket;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRevival() {
		return revival;
	}
	public void setRevival(String revival) {
		this.revival = revival;
	}
	public String getBuffer() {
		return buffer;
	}
	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}
	public Date getWaterRightsPermitUntil() {
		return waterRightsPermitUntil;
	}
	public void setWaterRightsPermitUntil(Date waterRightsPermitUntil) {
		this.waterRightsPermitUntil = waterRightsPermitUntil;
	}
	public String getBillingMode() {
		return billingMode;
	}
	public void setBillingMode(String billingMode) {
		this.billingMode = billingMode;
	}
	public String getBlocked() {
		return blocked;
	}
	public void setBlocked(String blocked) {
		this.blocked = blocked;
	}
	public String getNaviAddress() {
		return naviAddress;
	}
	public void setNaviAddress(String naviAddress) {
		this.naviAddress = naviAddress;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getLocationStreet() {
		return locationStreet;
	}
	public void setLocationStreet(String locationStreet) {
		this.locationStreet = locationStreet;
	}
	public String getLocationHouseNumber() {
		return locationHouseNumber;
	}
	public void setLocationHouseNumber(String locationHouseNumber) {
		this.locationHouseNumber = locationHouseNumber;
	}
	public String getLocationPostCode() {
		return locationPostCode;
	}
	public void setLocationPostCode(String locationPostCode) {
		this.locationPostCode = locationPostCode;
	}
	public String getLocationPlace() {
		return locationPlace;
	}
	public void setLocationPlace(String locationPlace) {
		this.locationPlace = locationPlace;
	}
	public String getLocationDistrict() {
		return locationDistrict;
	}
	public void setLocationDistrict(String locationDistrict) {
		this.locationDistrict = locationDistrict;
	}
	public String getEasting() {
		return easting;
	}
	public void setEasting(String easting) {
		this.easting = easting;
	}
	public String getNorthing() {
		return northing;
	}
	public void setNorthing(String northing) {
		this.northing = northing;
	}
	public String getDegreeOfLongitude() {
		return degreeOfLongitude;
	}
	public void setDegreeOfLongitude(String degreeOfLongitude) {
		this.degreeOfLongitude = degreeOfLongitude;
	}
	public String getDegreeOfLatitude() {
		return degreeOfLatitude;
	}
	public void setDegreeOfLatitude(String degreeOfLatitude) {
		this.degreeOfLatitude = degreeOfLatitude;
	}
	public String getAreaOfMunicipality() {
		return areaOfMunicipality;
	}
	public void setAreaOfMunicipality(String areaOfMunicipality) {
		this.areaOfMunicipality = areaOfMunicipality;
	}
	public String getHall() {
		return hall;
	}
	public void setHall(String hall) {
		this.hall = hall;
	}
	public String getParcel() {
		return parcel;
	}
	public void setParcel(String parcel) {
		this.parcel = parcel;
	}
	public String getBsb5() {
		return bsb5;
	}
	public void setBsb5(String bsb5) {
		this.bsb5 = bsb5;
	}
	public String getCsb() {
		return csb;
	}
	public void setCsb(String csb) {
		this.csb = csb;
	}
	public String getNges() {
		return nges;
	}
	public void setNges(String nges) {
		this.nges = nges;
	}
	public String getNh4() {
		return nh4;
	}
	public void setNh4(String nh4) {
		this.nh4 = nh4;
	}
	public String getPges() {
		return pges;
	}
	public void setPges(String pges) {
		this.pges = pges;
	}
	public String getAfs() {
		return afs;
	}
	public void setAfs(String afs) {
		this.afs = afs;
	}
	public String getAss() {
		return ass;
	}
	public void setAss(String ass) {
		this.ass = ass;
	}
	public String getPhValueFrom() {
		return phValueFrom;
	}
	public void setPhValueFrom(String phValueFrom) {
		this.phValueFrom = phValueFrom;
	}
	public String getPhValueTo() {
		return phValueTo;
	}
	public void setPhValueTo(String phValueTo) {
		this.phValueTo = phValueTo;
	}
	public String getBacteriaFrom() {
		return bacteriaFrom;
	}
	public void setBacteriaFrom(String bacteriaFrom) {
		this.bacteriaFrom = bacteriaFrom;
	}
	public String getBacteriaTo() {
		return bacteriaTo;
	}
	public void setBacteriaTo(String bacteriaTo) {
		this.bacteriaTo = bacteriaTo;
	}
	
	//Anand added getter and setter for visitngHours at 02-06-2018
	public String getVisitingHours() {
		return visitingHours;
	}
	public void setVisitingHours(String visitingHours) {
		this.visitingHours = visitingHours;
	}
	// Selva added Getter & Setter for visitingTime at 04-06-2018
	public String getVisitingTime() {
		return visitingTime;
	}
	public void setVisitingTime(String visitingTime) {
		this.visitingTime = visitingTime;
	}
}
