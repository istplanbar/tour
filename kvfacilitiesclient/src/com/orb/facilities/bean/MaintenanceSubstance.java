package com.orb.facilities.bean;

import java.util.Date;

public class MaintenanceSubstance {
	private String id;
	private String identifier;
	private String mudAssessment;
	private String floatingMud;
	private String mudOverflowDrain;
	private String removableSubstances;
	private String mudRemoval;
	private String floatingMudRemoval;
	private String sealedDisposal;
	private String exitOpeningsChanged;
	private String mudStorageVolume;
	private String buffer;
	private String revival;
	private String stability;
	private String soakAway;
	private String testRun;
	private String durationPasswordProgramming;  
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String checked;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getMudAssessment() {
		return mudAssessment;
	}
	public void setMudAssessment(String mudAssessment) {
		this.mudAssessment = mudAssessment;
	}
	public String getFloatingMud() {
		return floatingMud;
	}
	public void setFloatingMud(String floatingMud) {
		this.floatingMud = floatingMud;
	}
	public String getMudOverflowDrain() {
		return mudOverflowDrain;
	}
	public void setMudOverflowDrain(String mudOverflowDrain) {
		this.mudOverflowDrain = mudOverflowDrain;
	}
	public String getRemovableSubstances() {
		return removableSubstances;
	}
	public void setRemovableSubstances(String removableSubstances) {
		this.removableSubstances = removableSubstances;
	}
	public String getMudRemoval() {
		return mudRemoval;
	}
	public void setMudRemoval(String mudRemoval) {
		this.mudRemoval = mudRemoval;
	}
	public String getFloatingMudRemoval() {
		return floatingMudRemoval;
	}
	public void setFloatingMudRemoval(String floatingMudRemoval) {
		this.floatingMudRemoval = floatingMudRemoval;
	}
	public String getSealedDisposal() {
		return sealedDisposal;
	}
	public void setSealedDisposal(String sealedDisposal) {
		this.sealedDisposal = sealedDisposal;
	}
	public String getExitOpeningsChanged() {
		return exitOpeningsChanged;
	}
	public void setExitOpeningsChanged(String exitOpeningsChanged) {
		this.exitOpeningsChanged = exitOpeningsChanged;
	}
	public String getMudStorageVolume() {
		return mudStorageVolume;
	}
	public void setMudStorageVolume(String mudStorageVolume) {
		this.mudStorageVolume = mudStorageVolume;
	}
	public String getBuffer() {
		return buffer;
	}
	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}
	public String getRevival() {
		return revival;
	}
	public void setRevival(String revival) {
		this.revival = revival;
	}
	public String getStability() {
		return stability;
	}
	public void setStability(String stability) {
		this.stability = stability;
	}
	public String getSoakAway() {
		return soakAway;
	}
	public void setSoakAway(String soakAway) {
		this.soakAway = soakAway;
	}
	public String getTestRun() {
		return testRun;
	}
	public void setTestRun(String testRun) {
		this.testRun = testRun;
	}
	public String getDurationPasswordProgramming() {
		return durationPasswordProgramming;
	}
	public void setDurationPasswordProgramming(String durationPasswordProgramming) {
		this.durationPasswordProgramming = durationPasswordProgramming;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	
	

}
