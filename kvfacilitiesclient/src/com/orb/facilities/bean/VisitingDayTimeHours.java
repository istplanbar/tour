package com.orb.facilities.bean;

import java.util.Date;

public class VisitingDayTimeHours {

	private String id;
	private String facilitiesId;
	private String visitingDays;
	private String visitingHours;
	private String visitingTimes;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String identifier;
	
	
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFacilitiesId() {
		return facilitiesId;
	}
	public void setFacilitiesId(String facilitiesId) {
		this.facilitiesId = facilitiesId;
	}
	public String getVisitingDays() {
		return visitingDays;
	}
	public void setVisitingDays(String visitingDays) {
		this.visitingDays = visitingDays;
	}
	public String getVisitingHours() {
		return visitingHours;
	}
	public void setVisitingHours(String visitingHours) {
		this.visitingHours = visitingHours;
	}
	public String getVisitingTimes() {
		return visitingTimes;
	}
	public void setVisitingTimes(String visitingTimes) {
		this.visitingTimes = visitingTimes;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	
}
