package com.orb.facilities.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class FacilitiesConstants {
	
	//Facilities
		public final static String SELECTED_FACILITIES_ID = "SELECTED_FACILITIES_ID";
		public final static String MAINTENANCE_ACTIVE_FLAG = "MAINTENANCE_ACTIVE_FLAG";
		public final static String FACILITIES_LIST_PAGE = "/common/process/facilities/loadFacilities.html";
		public final static String FACILITIES_EDIT_PAGE = "/common/process/facilities/manipulateFacilities.html";
		public final static String FACILITIES_LIST_HTML = "facilities/list.xhtml";
		public final static String FACILITIES_TOGGLER_LIST = "facilities_toggler_list" ;
		public final static String FACILITIES_DELETE_SUCCESS = "facilities_delete_success";
		public final static String FACILITIES_LOCKED_SUCCESS = "facilities_locked_success";
		public final static String FACILITIES_UNLOCKED_SUCCESS = "facilities_unlocked_success";
		public final static String FACILITIES_VIEW_PAGE = "/common/process/facilities/viewFacilities.html";
		public final static String FACILITIES_EDIT_HTML = "facilities/addUpdate.xhtml";
		public final static String FACILITIES_VIEW_HTML = "facilities/view.xhtml";
		public final static String FACILITIES_ADDED_SUCCESS = "facilities_added_success";
		public final static String FACILITIES_UPDATED_SUCCESS = "facilities_update_success";
		
		
		//Maintenance Act
		public final static String SELECTED_MAINTENANCE_ACT_ID = "SELECTED_MAINTENANCE_ACT_ID";
		public final static String MAINTENANCE_ACT_LIST_PAGE = "/common/process/facilities/loadMaintenanceAct.html";
		public final static String MAINTENANCE_ACT_EDIT_PAGE = "/common/process/facilities/manipulateMaintenanceAct.html";
		public final static String MAINTENANCE_ACT_LIST_HTML = "facilities/maintenanceact/list.xhtml";
		public final static String MAINTENANCE_ACT_TOGGLER_LIST = "maintenance_act_toggler_list" ;
		public final static String MAINTENANCE_ACT_DELETE_SUCCESS = "maintenance_act_delete_success";
		public final static String MAINTENANCE_ACT_LOCKED_SUCCESS = "maintenance_act_locked_success";
		public final static String MAINTENANCE_ACT_UNLOCKED_SUCCESS = "maintenance_act_unlocked_success";
		public final static String MAINTENANCE_ACT_VIEW_PAGE = "/common/process/facilities/viewMaintenanceAct.html";
		public final static String MAINTENANCE_ACT_EDIT_HTML = "facilities/maintenanceact/addUpdate.xhtml";
		public final static String MAINTENANCE_ACT_VIEW_HTML = "facilities/maintenanceact/view.xhtml";
		public final static String MAINTENANCE_ACT_ADDED_SUCCESS = "maintenance_act_added_success";
		public final static String MAINTENANCE_ACT_UPDATED_SUCCESS = "maintenance_act_update_success";

		//Document
		public final static String DOCUMENT_ADDED_SUCCESS="document_added_success";
		public final static String DOCUMENT_UPDATED_SUCCESS="document_update_success";
		public final static String SHOW_ACTIVE_DOCUMENT = "SHOW_ACTIVE_DOCUMENT";
		public final static String DOCUMENT_TOGGLER_LIST = "DOCUMENT_TOGGLER_LIST" ;
		public final static String DOCUMENT_DELETE_SUCCESS="document_delete_success";
		public final static String DOCUMENT_LOCKED_SUCCESS="document_locked_success";
		public final static String DOCUMENT_UNLOCKED_SUCCESS="document_unlocked_success";
		
		public final static Map<String,String>  ESTIMATED_WORK_HOURS, MAINTENACE_CYCLE_MONTH_NAME;
	
		static{
			ESTIMATED_WORK_HOURS = (Map<String,String>)CommonUtils.getBean("ESTIMATED_WORK_HOURS");
			MAINTENACE_CYCLE_MONTH_NAME = (Map<String,String>)CommonUtils.getBean("MAINTENACE_CYCLE_MONTH_NAME");
		}
}
