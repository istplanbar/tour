package com.orb.facilities.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class FacilitiesDBConstants {

	// Facilities

	public static final String TBL_FACILITIES, TBL_FACILITIES_HISTORY,TBL_TOUR, TBL_TOUR_FACILITIES;
	public static final Map<String, String> FACILITIES, FACILITIES_HISTORY,TOUR;
	public static final Map<String, String> facilitiesDBConfig;
	

	// Maintenance Visiting Day Time and Hours
	public static final String TBL_MAINTENANCE_VISITING_DAY_TIME, TBL_MAINTENANCE_VISITING_DAY_TIME_HISTORY;
	public static final Map<String, String> MAINTENANCE_DAY_TIME, MAINTENANCE_DAY_TIME_HISTORY;

	// Maintenance Basic Details
	public static final String TBL_MAINTENANCE_BASIC, TBL_MAINTENANCE_BASIC_HISTORY;
	public static final Map<String, String> MAINTENANCE_BASIC, MAINTENANCE_BASIC_HISTORY;
	public static final Map<String, String> maintenanceActDBConfig;

	// Maintenance Control
	public static final String TBL_MAINTENANCE_CONTROL, TBL_MAINTENANCE_CONTROL_HISTORY;
	public static final Map<String, String> MAINTENANCE_CONTROL, MAINTENANCE_CONTROL_HISTORY;

	// Maintenance Substance
	public static final String TBL_MAINTENANCE_SUBSTANCE, TBL_MAINTENANCE_SUBSTANCE_HISTORY;
	public static final Map<String, String> MAINTENANCE_SUBSTANCE, MAINTENANCE_SUBSTANCE_HISTORY;

	// Document Tables and beans
	public static final String TBL_FACILITIES_DOCUMENT;
	public static final String TBL_FACILITIES_DOCUMENT_HISTORY;
	public static final Map<String, String> FACILITIES_DOCUMENT;
	public static final Map<String, String> FACILITIES_DOCUMENT_HISTORY;

	static {

		// Facilities

		TBL_FACILITIES = CommonUtils.getPropertyValue("FACILITIES_TABLE_NAMES", "MOD_FACILITIES");
		TBL_FACILITIES_HISTORY = CommonUtils.getPropertyValue("FACILITIES_TABLE_NAMES", "MOD_FACILITIES_HISTORY");

		FACILITIES = (Map<String, String>) CommonUtils.getBean("MOD_FACILITIES");
		FACILITIES_HISTORY = (Map<String, String>) CommonUtils.getBean("MOD_FACILITIES_HISTORY");
		
		TBL_TOUR=CommonUtils.getPropertyValue("TOUR_TABLE_NAMES", "MOD_TOUR");
		TBL_TOUR_FACILITIES = CommonUtils.getPropertyValue("TOUR_TABLE_NAMES", "MOD_TOUR_FACILITIES");

		
		TOUR=(Map<String,String>)CommonUtils.getBean("MOD_TOUR");
		
		facilitiesDBConfig = (Map<String, String>) CommonUtils.getBean("facilitiesDBConfig");

		// Maintenance Basic Details

		TBL_MAINTENANCE_BASIC = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_BASIC_DETAILS");
		TBL_MAINTENANCE_BASIC_HISTORY = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_BASIC_DETAILS_HISTORY");

		MAINTENANCE_BASIC = (Map<String, String>) CommonUtils.getBean("MOD_FACILITIES_MAINTENANCE_BASIC_DETAILS");
		MAINTENANCE_BASIC_HISTORY = (Map<String, String>) CommonUtils
				.getBean("MOD_FACILITIES_MAINTENANCE_BASIC_DETAILS_HISTORY");

		maintenanceActDBConfig = (Map<String, String>) CommonUtils.getBean("maintenanceActDBConfig");

		// Maintenance Control

		TBL_MAINTENANCE_CONTROL = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_CONTROL");
		TBL_MAINTENANCE_CONTROL_HISTORY = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_CONTROL_HISTORY");

		MAINTENANCE_CONTROL = (Map<String, String>) CommonUtils.getBean("MOD_FACILITIES_MAINTENANCE_CONTROL");
		MAINTENANCE_CONTROL_HISTORY = (Map<String, String>) CommonUtils
				.getBean("MOD_FACILITIES_MAINTENANCE_CONTROL_HISTORY");

		// Maintenance Substance

		TBL_MAINTENANCE_SUBSTANCE = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_SUBSTANCE");
		TBL_MAINTENANCE_SUBSTANCE_HISTORY = CommonUtils.getPropertyValue("MAINTENANCE_ACT_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_SUBSTANCE_HISTORY");

		MAINTENANCE_SUBSTANCE = (Map<String, String>) CommonUtils.getBean("MOD_FACILITIES_MAINTENANCE_SUBSTANCE");
		MAINTENANCE_SUBSTANCE_HISTORY = (Map<String, String>) CommonUtils
				.getBean("MOD_FACILITIES_MAINTENANCE_SUBSTANCE_HISTORY");

		// Document Tables
		TBL_FACILITIES_DOCUMENT = CommonUtils.getPropertyValue("FACILITIES_DOCUMENT_TABLE_NAMES", "FACILITY_DOC_TABLE");
		TBL_FACILITIES_DOCUMENT_HISTORY = CommonUtils.getPropertyValue("FACILITIES_DOCUMENT_TABLE_NAMES",
				"FACILITY_DOC_HISTORY_TABLE");

		FACILITIES_DOCUMENT = (Map<String, String>) CommonUtils.getBean("FACILITY_DOC_TABLE");
		FACILITIES_DOCUMENT_HISTORY = (Map<String, String>) CommonUtils.getBean("FACILITY_DOC_HISTORY_TABLE");

		// Maintenance Visiting Day and Time

		TBL_MAINTENANCE_VISITING_DAY_TIME = CommonUtils.getPropertyValue("FACILITIES_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_VISITING_DAY_TIME");
		TBL_MAINTENANCE_VISITING_DAY_TIME_HISTORY = CommonUtils.getPropertyValue("FACILITIES_TABLE_NAMES",
				"MOD_FACILITIES_MAINTENANCE_VISITING_DAY_TIME_HISTORY");

		MAINTENANCE_DAY_TIME = (Map<String, String>) CommonUtils
				.getBean("MOD_FACILITIES_MAINTENANCE_VISITING_DAY_TIME");
		MAINTENANCE_DAY_TIME_HISTORY = (Map<String, String>) CommonUtils
				.getBean("MOD_FACILITIES_MAINTENANCE_VISITING_DAY_TIME_HISTORY");
		
		

	}
}
