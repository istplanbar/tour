package com.orb.facilities.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.Document;
import com.orb.facilities.constants.FacilitiesDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class DocumentDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(DocumentDAO.class);
	
	
	/**
	 * This method will add the new Document for Participant in DB
	 * @param commonError
	 * @param loginUser
	 * @param re
	 */
	public void addDocument( CommonError commonError, User loginUser, Document doc,String identifier)  {
		logger.info("==> addDocument START <==");
		logger.info("Participant Id ==> " + identifier );
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("INSERT INTO ");qry.append( FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);qry.append("( ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("IDENTIFIER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("NAME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPLOAD_TYPE"));qry.append(",");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("DESCRIPTION"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ATTACHMENT"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ACTIVE"));
		qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?)  ");
		List<Object> args = new ArrayList<Object>();
		args.add(identifier);
		args.add(doc.getName());
		args.add(doc.getUploadType());
		args.add(doc.getDescription());
		args.add(doc.getAttachment());
		args.add(loginUser.getUserId());args.add(date);
		args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> addDocument END <==");
	}
	
	
	/**
	 * This method will Update the new Document for Participant in DB
	 * @param commonError
	 * @param loginUser


	 * @param re
	 */
	public void updateDocument( CommonError commonError, User loginUser, Document doc,String identifier)  {
		logger.info("==>update Document START <==");
		logger.info("Participant Id ==> " + identifier );
        List<String> idList = new ArrayList<>();
        idList.add(doc.getId());
		insertDocumentHistory(commonError,loginUser,idList,CommonConstants.UPDATED);
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("UPDATE ");qry.append( FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);qry.append(" SET ");
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("IDENTIFIER"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("NAME"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPLOAD_TYPE"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("DESCRIPTION"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ATTACHMENT"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_BY"));qry.append( " = ?," );
		qry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_TIME"));qry.append( " = ?" );
		qry.append(" WHERE ");qry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID") );qry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(identifier);
		args.add(doc.getName());
		args.add(doc.getUploadType());
		args.add(doc.getDescription());
		args.add(doc.getAttachment());
		args.add(loginUser.getUserId());args.add(date);args.add(doc.getId());
		getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> update Residence END <==");
	}
	
	
	/**
	 * This method used to get a particular document from DB
	 * @param commonError
	 * @param crmId
	 * @return
	 */
	public List<Document> getDocumentList(CommonError commonError, String identifier,String activeFlag) {
		List<Document> documentList=null;
		logger.info(" ==> Get a Document List START <== ");
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("IDENTIFIER"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("NAME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPLOAD_TYPE"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("DESCRIPTION"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ATTACHMENT"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_BY"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_TIME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_BY"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_TIME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ACTIVE"));
			newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("IDENTIFIER") );newQry.append( " = ? " );
			newQry.append( " AND " );newQry.append( " ACTIVE " );newQry.append( " = ? " );
			newQry.append(" ORDER BY CREATE_TIME DESC ");
			documentList = (List<Document>)getJdbcTemplate().query( newQry.toString(),new String[]{identifier,activeFlag}, new BeanPropertyRowMapper(Document.class) );
			//residenceList = getJdbcTemplate().query( newQry.toString(),new String[]{identifier,activeFlag}, new BeanPropertyRowMapper(Residence.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Document list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Document list END <== ");
		return documentList;
	}
	
	/**
	 * This method will activate or deactivate the document
	 * @param commonError
	 * @param loginUser
	 * @param r
	 * @param status
	 */
	public void updateDocumentActiveStatus( CommonError commonError, User loginUser, List<String> idList, String status ){
		logger.info(" ==> updateDocumentActiveStatus START <== ");
		logger.info("Document Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			if(CommonConstants.Y.equals(status)){
				insertDocumentHistory( commonError, loginUser, idList, CommonConstants.UNLOCKED );
			}else{
				insertDocumentHistory( commonError, loginUser, idList, CommonConstants.LOCKED );
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT );qry.append(" SET ");
			qry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ACTIVE") );qry.append( "=? ");
			qry.append(" WHERE ");
			qry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID") );qry.append( "=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, status);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when updateResidenceActiveStatus of a Residence.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateDocumenteActiveStatus END <== ");
	}

	/**
	 * This method will delete a document in DB
	 * @param commonError
	 * @param loginUser
	 * @param r
	 */
	public void deleteDocument(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> delete START <== ");
		logger.info("Document Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertDocumentHistory(commonError, loginUser, idList, CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
			
			
		}catch( Exception e ){
			logger.info("Error when Delete Document.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> delete END <== ");
	}
	
	/**
	 * This method will insert DocumentHistory for Participant in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	private void insertDocumentHistory( CommonError commonError, User loginUser, List<String> idList, String action ) {
		logger.info(" ==> insertDocumentHistory START <== ");
		logger.info("Participant Id ==> " + idList );
		StringBuffer qryHist = new StringBuffer();
		StringBuffer qry = new StringBuffer();
		qry.append("SELECT ");
		for( String column : FacilitiesDBConstants.FACILITIES_DOCUMENT.keySet() ){
			qry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get(column) );qry.append( "," );
		}
		qry.setLength(qry.length()-1);
		qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);qry.append(" WHERE ");qry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID") );
		qry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			qry.append(" ? ");qry.append(",");
		}
		qry.setLength(qry.length() - 1);
		qry.append(") ");
		List<Document> documentList = (List<Document>)getJdbcTemplate().query(qry.toString(), idList.toArray(),  new BeanPropertyRowMapper<Document>(Document.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			qryHist.append("INSERT INTO ");qryHist.append( FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT_HISTORY);qryHist.append("( ");
			for( String column : FacilitiesDBConstants.FACILITIES_DOCUMENT_HISTORY.keySet() ){
				if(!column.equals(FacilitiesDBConstants.FACILITIES_DOCUMENT_HISTORY.get("ID"))){
					qryHist.append( FacilitiesDBConstants.FACILITIES_DOCUMENT_HISTORY.get(column) );qryHist.append( "," );
				}
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(qryHist.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, documentList.get(i).getIdentifier());ps.setString(2, documentList.get(i).getName());
					ps.setString(3, documentList.get(i).getUploadType());
					ps.setString(4, documentList.get(i).getDescription());ps.setString(5, documentList.get(i).getAttachment());
					ps.setString(6, loginUser.getUserId());
					ps.setTimestamp(7, date);ps.setString(8, loginUser.getUserId());
					ps.setTimestamp(9, date);ps.setString(10, action);
				}		
				public int getBatchSize() {
					return documentList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when Insert DocumentHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert DocumentHistory END <== ");
	
	}
	/**
	 * This method will return the document to the given id with the type. 
	 * @param commonError
	 * @param participantId
	 * @return
	 */
	public Document getDocument( CommonError commonError, String documentId){
		logger.info(" ==> getDocument START <== ");
		Document document = null;
		logger.info("Document Id ==> " + documentId );
		
		if( documentId == null ) return null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("IDENTIFIER"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("NAME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPLOAD_TYPE"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("DESCRIPTION"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ATTACHMENT"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_BY"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("CREATE_TIME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_BY"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("UPDATE_TIME"));newQry.append(",");
			newQry.append(FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ACTIVE"));
			newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_FACILITIES_DOCUMENT);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES_DOCUMENT.get("ID") );newQry.append( " = ? " );
			document = (Document) getJdbcTemplate().queryForObject( newQry.toString(), new String[] { documentId}, new BeanPropertyRowMapper(Document.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch document. Exception ==> " + e.getMessage() );
			
		}
		logger.info(" ==> getDocument END <== ");
		return document;
	}

}
