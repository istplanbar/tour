package com.orb.facilities.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.TourFacilities;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.facilities.constants.FacilitiesDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class FacilitiesDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(FacilitiesDAO.class);

	/**
     * This method will return list of Facilities from DB
     * @param commonError
     * @param activeFlag
     * @return
     */
	public List<Facilities> getFacilitiesList(CommonError commonError, String activeFlag) {
		logger.info(" ==> getFacilitiesList START <== ");
		List<Facilities> facilitiesList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : FacilitiesDBConstants.FACILITIES.keySet() ){
				   qry.append( FacilitiesDBConstants.FACILITIES.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_FACILITIES);qry.append(" WHERE ");
			qry.append(FacilitiesDBConstants.FACILITIES.get("ACTIVE"));qry.append("=? ");
			facilitiesList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag},new BeanPropertyRowMapper(Facilities.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to fetch facilities list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getFacilitiesList END <== ");
		return facilitiesList;
	}

	/**
	 * This method used to get a particular Facilities from DB
	 * @param commonError
	 * @param facilitiesId
	 * @return facilities
	 */
	public Facilities getFacilities(CommonError commonError, String facilitiesId) {
		logger.info(" ==> Get a Facilities START <== ");
		Facilities facilities = null;
		Facilities lockedHist = null;
		Facilities unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.FACILITIES.keySet() ){
				newQry.append( FacilitiesDBConstants.FACILITIES.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_FACILITIES);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES.get("IDENTIFIER") );newQry.append( "=? ");
			facilities = (Facilities)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{facilitiesId}, new BeanPropertyRowMapper(Facilities.class) );
			
		lockedHist = getUserAction(commonError, facilities.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			facilities.setLockedBy( lockedHist.getUpdateBy());
			facilities.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, facilities.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			facilities.setUnlockedBy( unlockedHist.getUpdateBy());
			facilities.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch facilities. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Facilities END <== ");
		return facilities;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */
    private Facilities getUserAction(CommonError commonError,String id, String action){
    	Facilities historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : FacilitiesDBConstants.FACILITIES_HISTORY.keySet() ){
			newQryHist.append( FacilitiesDBConstants.FACILITIES_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(FacilitiesDBConstants.TBL_FACILITIES_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( FacilitiesDBConstants.FACILITIES.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(FacilitiesDBConstants.FACILITIES_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(FacilitiesDBConstants.FACILITIES_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Facilities)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Facilities.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}
    
    /**
	 * This method will activate or deactivate the Facilities
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> updateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertFacilitiesHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertFacilitiesHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( FacilitiesDBConstants.TBL_FACILITIES);qry.append(" SET ");
			qry.append( FacilitiesDBConstants.FACILITIES.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( FacilitiesDBConstants.FACILITIES.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Facilities." + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");
		
		
		
	}

	/**
	 * This method delete the Facilities into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteFacilities(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Facilities START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertFacilitiesHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_FACILITIES);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Facilities list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Facilities END <== ");
		
	}

	/**
	 * This method inserts new Facilities into DB
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */
	public void addFacilities(CommonError commonError, User loginUser, Facilities facilities, List<VisitingDayTimeHours> dayTimeList) {
		logger.info(" ==> Insert Facilities START <== ");
		try{
			//String nextRowId = getNextRowId( FacilitiesDBConstants.TBL_FACILITIES, FacilitiesDBConstants.facilitiesDBConfig.get("rowPrefix") );
			insertRecord(loginUser, facilities,FacilitiesDBConstants.TBL_FACILITIES,FacilitiesDBConstants.facilitiesDBConfig.get("rowPrefix"));  
			if(dayTimeList != null){
			for(VisitingDayTimeHours vtm : dayTimeList){
				vtm.setFacilitiesId(facilities.getIdentifier());
				addVisitingTimeAndHours(commonError, loginUser, vtm);
			}}
		}catch( Exception e ){
			logger.info("Error when Insert Facilities.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Facilities END <== ");
		
	}

	 /**
     * This method updates existing Facilities in DB
     * @param commonError
     * @param loginUser
     * @param software
     */
	public void updateFacilities(CommonError commonError, User loginUser, Facilities facilities, List<VisitingDayTimeHours> dayTimeList) {
		logger.info(" ==> Update Facilities START <== ");
		try{
			updateRecordById(loginUser, facilities,FacilitiesDBConstants.TBL_FACILITIES); 
			if(dayTimeList != null){
				deleteFacilitiesDayAndTime(commonError, loginUser, facilities.getIdentifier());
				for(VisitingDayTimeHours vtm : dayTimeList){
					vtm.setFacilitiesId(facilities.getIdentifier());
					addVisitingTimeAndHours(commonError, loginUser, vtm);
				}
		}
		}catch( Exception e ){ 
			logger.info("Error when Update Facilities.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Facilities END <== ");
		
	}
	
	
	/**
     * This method inserts updated Facilities into Facilities history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	
	
   public void insertFacilitiesHistory(CommonError commonError, User loginUser,List<String> idList, String action) throws Exception {
		logger.info(" ==> Insert Facilities History START <== ");
		logger.info("Facilities Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : FacilitiesDBConstants.FACILITIES.keySet() ){
			newQryHist.append( FacilitiesDBConstants.FACILITIES.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(FacilitiesDBConstants.TBL_FACILITIES);newQryHist.append(" WHERE ");
		newQryHist.append( FacilitiesDBConstants.FACILITIES.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Facilities> facilitiesList = (List<Facilities>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Facilities>(Facilities.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( FacilitiesDBConstants.TBL_FACILITIES_HISTORY);newQry.append("( ");
			for( String column : FacilitiesDBConstants.FACILITIES_HISTORY.keySet() ){
				if(!column.equals(FacilitiesDBConstants.FACILITIES_HISTORY.get("ID"))){
				newQry.append( FacilitiesDBConstants.FACILITIES_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1); 
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, facilitiesList.get(i).getIdentifier());
					ps.setString(2, facilitiesList.get(i).getName());
					ps.setString(3, facilitiesList.get(i).getClientId());
					ps.setString(4, facilitiesList.get(i).getQualificationsId());
					ps.setString(5, facilitiesList.get(i).getSewageTreatmentType());
					ps.setString(6, facilitiesList.get(i).getBlowerType());
					ps.setString(7, facilitiesList.get(i).getConstructionYear());
					ps.setString(8, facilitiesList.get(i).getBuildingSize());
					ps.setString(9, facilitiesList.get(i).getSerialNo());
					ps.setString(10, facilitiesList.get(i).getMaintenanceCycle());
					ps.setString(11, facilitiesList.get(i).getVisitingDay());
					ps.setObject(12, facilitiesList.get(i).getLastVisit());
					ps.setString(13, facilitiesList.get(i).getBuildingPlan());
					ps.setString(14, facilitiesList.get(i).getPrimaryTreatment());
					ps.setString(15, facilitiesList.get(i).getCostWithoutOffer());
					ps.setString(16, facilitiesList.get(i).getCleaningStandingOrder());
					ps.setString(17, facilitiesList.get(i).getPowerFailureDetection());
					ps.setString(18, facilitiesList.get(i).getControlFreeAccess());
					ps.setString(19, facilitiesList.get(i).getAnnouncementToUwbme());
					ps.setString(20, facilitiesList.get(i).getReportsResultsUwb());
					ps.setString(21, facilitiesList.get(i).getReportsResultsCity());
					ps.setString(22, facilitiesList.get(i).getReportsResultsCustomer());
					ps.setString(23, facilitiesList.get(i).getLocationOfTheFacility());
					ps.setString(24, facilitiesList.get(i).getFacilityOperator());
					ps.setString(25, facilitiesList.get(i).getInstructionGiven());
					ps.setString(26, facilitiesList.get(i).getFitter());
					ps.setString(27, facilitiesList.get(i).getConnectionSize());
					ps.setString(28, facilitiesList.get(i).getControl());
					ps.setString(29, facilitiesList.get(i).getNumberOfContainer());
					ps.setString(30, facilitiesList.get(i).getWaterDepth());
					ps.setString(31, facilitiesList.get(i).getDiameter());
					ps.setString(32, facilitiesList.get(i).getCrashInPrimaryTreatment());
					ps.setString(33, facilitiesList.get(i).getOtherForms());
					ps.setString(34, facilitiesList.get(i).getRemarks());
					ps.setString(35, facilitiesList.get(i).getMaintenanceActId());
					ps.setString(36, facilitiesList.get(i).getFreelyAccessible());
					ps.setString(37, facilitiesList.get(i).getDocket());
					ps.setString(38, facilitiesList.get(i).getPassword());
					ps.setString(39, facilitiesList.get(i).getRevival());
					ps.setString(40, facilitiesList.get(i).getBuffer());
					ps.setObject(41, facilitiesList.get(i).getWaterRightsPermitUntil());
					ps.setString(42, facilitiesList.get(i).getBillingMode());
					ps.setString(43, facilitiesList.get(i).getBlocked());
					ps.setString(44, facilitiesList.get(i).getNaviAddress());
					ps.setString(45, facilitiesList.get(i).getStreet());
					ps.setString(46, facilitiesList.get(i).getHouseNumber());
					ps.setString(47, facilitiesList.get(i).getPostCode());
					ps.setString(48, facilitiesList.get(i).getPlace());
					ps.setString(49, facilitiesList.get(i).getLocationStreet());
					ps.setString(50, facilitiesList.get(i).getLocationHouseNumber());
					ps.setString(51, facilitiesList.get(i).getLocationPostCode());
					ps.setString(52, facilitiesList.get(i).getLocationPlace());
					ps.setString(53, facilitiesList.get(i).getLocationDistrict());
					ps.setString(54, facilitiesList.get(i).getEasting());
					ps.setString(55, facilitiesList.get(i).getNorthing());
					ps.setString(56, facilitiesList.get(i).getDegreeOfLongitude());
					ps.setString(57, facilitiesList.get(i).getDegreeOfLatitude());
					ps.setDouble(58, facilitiesList.get(i).getFacilityLatitude());
					ps.setDouble(59, facilitiesList.get(i).getFacilityLongitude());
					ps.setString(60, facilitiesList.get(i).getAreaOfMunicipality());
					ps.setString(61, facilitiesList.get(i).getHall());
					ps.setString(62, facilitiesList.get(i).getParcel());
					ps.setString(63, facilitiesList.get(i).getBsb5());
					ps.setString(64, facilitiesList.get(i).getCsb());
					ps.setString(65, facilitiesList.get(i).getNges());
					ps.setString(66, facilitiesList.get(i).getNh4());
					ps.setString(67, facilitiesList.get(i).getPges());
					ps.setString(68, facilitiesList.get(i).getAfs());
					ps.setString(69, facilitiesList.get(i).getAss());
					ps.setString(70, facilitiesList.get(i).getPhValueFrom());
					ps.setString(71, facilitiesList.get(i).getPhValueTo());
					ps.setString(72, facilitiesList.get(i).getBacteriaFrom());
					ps.setString(73, facilitiesList.get(i).getBacteriaTo());
					ps.setString(74, loginUser.getUserId());
					ps.setTimestamp(75, date);ps.setString(76, loginUser.getUserId());
					ps.setTimestamp(77, date);ps.setString(78, action);ps.setString(79, facilitiesList.get(i).getMonthOfMaintenance());
					ps.setString(80, facilitiesList.get(i).getCustomerNumber());
					//Added a value for visiting hours at 02-06-2018
					ps.setString(81, facilitiesList.get(i).getVisitingHours());
					//Selva a value for visiting hours at 04/06/2018
					ps.setString(82, facilitiesList.get(i).getVisitingTime());
					ps.setString(83, facilitiesList.get(i).getReminder());
					
				}		
				public int getBatchSize() {
					return facilitiesList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when Insert Facilities History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Facilities History END <== ");
	}

   
   /**
	 * This method inserts new VisitingTimeAndHours into DB
	 * @param commonError
	 * @param loginUser
	 * @param ct
	 * @return 
	 */
	public void addVisitingTimeAndHours(CommonError commonError, User loginUser, VisitingDayTimeHours visitingDayTimeHours) throws Exception {
		logger.info("==> addVisitingTimeAndHours START <==");
		logger.info("Facilities Id ==> " +   visitingDayTimeHours.getFacilitiesId() );
		try
		{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());	
		StringBuffer qry = new StringBuffer();
		qry.append("INSERT INTO ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_VISITING_DAY_TIME );qry.append("( ");
		for( String column : FacilitiesDBConstants.MAINTENANCE_DAY_TIME.keySet() ){
			if(!column.equals(FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get("ID"))){
				qry.append( FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get(column) );qry.append( "," );
			}
		}
		qry.setLength(qry.length()-1);
		qry.append(") VALUES(?,?,?,?,?,?,?,?,?)  ");
		List<Object> args = new ArrayList<Object>();
		args.add(visitingDayTimeHours.getFacilitiesId());
		args.add(visitingDayTimeHours.getVisitingDays());args.add(visitingDayTimeHours.getVisitingHours());
		args.add(visitingDayTimeHours.getVisitingTimes());
		args.add(loginUser.getUserId());
		args.add(date);args.add(loginUser.getUserId());
		args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( qry.toString(),args.toArray());
			
		
		}
		catch( Exception e ){
			logger.info("Error when Insert VisitingTimeAndHours.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
		logger.info(" ==> Insert VisitingTimeAndHours END <== ");
		
	}

	/**
	 * This method get the VisitingTimeAndHours into DB
	 * @param commonError
	 * @param FacilitiesId
	 * @return 
	 */
	@SuppressWarnings("unchecked")
	public List<VisitingDayTimeHours> getAllVisitingTimeAndHours( CommonError commonError, List<String> facilityIdList ){
		logger.info(" ==> get AllVisitingTimeAndHours START <== ");
		List<VisitingDayTimeHours> visitingDayTimeList = null;
		try{
			
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_DAY_TIME.keySet() ){
				qry.append( FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get(column) );qry.append( "," );
			}
			qry.append( " IDENTIFIER " );
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_VISITING_DAY_TIME);
			qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get("IDENTIFIER") );
			qry.append(" IN ( ");
			for ( int i=0; i< facilityIdList.size(); i++ ) {
				qry.append(" ? ");qry.append(",");
			}
			qry.setLength(qry.length() - 1);
			qry.append(") ");
			visitingDayTimeList =  getJdbcTemplate().query(qry.toString(), facilityIdList.toArray(), new BeanPropertyRowMapper(VisitingDayTimeHours.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch VisitingTimeAndHours list. Exception ==> " + e.getMessage() );
		}
		logger.info(" ==> get AllVisitingTimeAndHours END <== ");
		return visitingDayTimeList;
	}
	
	
	/**
	 * This method get the VisitingTimeAndHours into DB
	 * @param commonError
	 * @param FacilitiesId
	 * @return 
	 */
	public List<VisitingDayTimeHours> getVisitingTimeAndHours( CommonError commonError, String FacilitiesId ){
		logger.info(" ==> get VisitingTimeAndHours START <== ");
		List<VisitingDayTimeHours> visitingDayTimeList = null;
		logger.info("Facilties Id ==> " + FacilitiesId );
		if( FacilitiesId == null) return null;
		try{
			
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_DAY_TIME.keySet() ){
				qry.append( FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_VISITING_DAY_TIME);
			qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_DAY_TIME.get("IDENTIFIER") );
			qry.append( " = ? " );
			
			visitingDayTimeList = getJdbcTemplate().query( qry.toString(),new String[]{FacilitiesId}, new BeanPropertyRowMapper(VisitingDayTimeHours.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch VisitingTimeAndHours list. Exception ==> " + e.getMessage() );
			//commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> get VisitingTimeAndHours END <== ");
		return visitingDayTimeList;
	}
	
	
	/**
	 * This method will return tour from DB.
	 * @param commonError
	 * @param loginUser
	 * @param ct
	 */
	public List<TourFacilities> getTourDataList(CommonError commonError, String facilityId) {
		logger.info("==> tour retrieval START <==");
		logger.info("tour Id ==> " + facilityId );
		List<String> tourIDList = null;
		List<TourFacilities> tourFacilitiesList = null;
		try{
			
			StringBuffer qry = new StringBuffer(" SELECT IDENTIFIER ");
			
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_TOUR_FACILITIES);
			qry.append(" WHERE ");qry.append( "FACILITIES" );
			qry.append( " = ? " );
			/*qry.append( " AND " );
			qry.append(CommonDBConstants.USER.get("ACTIVE"));
			qry.append(" = ? ");*/
			
			
			tourIDList = getJdbcTemplate().queryForList( qry.toString(),new String[]{facilityId}, String.class);
			logger.info("List of Tours" + Arrays.toString(tourIDList.toArray()));
		    
			NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
			SqlParameterSource namedParameters = new MapSqlParameterSource("tourIds", tourIDList);
			
		   StringBuffer newQry = new StringBuffer("SELECT ");
		   newQry.append( FacilitiesDBConstants.TOUR.get("IDENTIFIER") );newQry.append( "," );
		   newQry.append( FacilitiesDBConstants.TOUR.get("TOUR_DATE") );newQry.append( "," );
		   newQry.append( FacilitiesDBConstants.TOUR.get("NAME") );
		   newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_TOUR);newQry.append(" WHERE ");
		   newQry.append(" ACTIVE = 'Y' AND ");
		   newQry.append(FacilitiesDBConstants.TOUR.get("IDENTIFIER"));newQry.append(" IN ");
		   newQry.append(" (:tourIds) ");
				
			tourFacilitiesList = db.query(newQry.toString(), namedParameters, new BeanPropertyRowMapper(TourFacilities.class));
		}catch( Exception e ){
			logger.info("Error when retrieving tour from Table.  " + e.getMessage());
			//commonError.addError(CommonConstants.DB_SELECT_ERROR);
		}
		logger.info(" ==> Get latest tour date for END <== ");
		return tourFacilitiesList;
	}

	/**
	 * This method delete the Facilities into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteFacilitiesDayAndTime(CommonError commonError, User loginUser, String facilityId) {
		logger.info(" ==> Delete Facilities visiting day and time START <== ");
		try{
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_VISITING_DAY_TIME);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.FACILITIES.get("IDENTIFIER") );newQry.append( " =? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{facilityId} );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete Facilities day and time list. Exception ==> " + e.getMessage() );
			//commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Facilities visiting day and time START <== ");
		
	}
	
}
