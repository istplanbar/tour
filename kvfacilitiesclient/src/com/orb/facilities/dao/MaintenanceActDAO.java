package com.orb.facilities.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.constants.FacilitiesDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class MaintenanceActDAO extends AbstractDAO{
	
	private static Log logger = LogFactory.getLog(FacilitiesDAO.class);
	/**
     * This method will return list of MaintenanceAct from DB
     * @param commonError
     * @param activeFlag
     * @return
     */
	public List<MaintenanceAct> getMaintenanceActList(CommonError commonError, String facilityId, String s) {
		logger.info(" ==> Get MaintenanceActList START <== ");
		List<MaintenanceAct> maintenanceActList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC.keySet() ){
				   qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_BASIC);qry.append(" WHERE ");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("FACILITY_IDENTIFIER"));qry.append("=? AND ");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("ACTIVE"));qry.append("=? ");
			
			maintenanceActList = getJdbcTemplate().query( qry.toString(),new String[]{facilityId, s},new BeanPropertyRowMapper(MaintenanceAct.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to fetch maintenanceact list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> Get MaintenanceActList END <== ");
		return maintenanceActList;		
	}
	
	/**
	 * This method used to get a particular MaintenanceAct from DB
	 * @param commonError
	 * @param maintenanceActId
	 */
	public MaintenanceAct getMaintenanceAct(CommonError commonError, String maintenanceActId) {
		logger.info(" ==> getMaintenanceAct START <== ");
		MaintenanceAct maintenanceAct = null;
		MaintenanceAct lockedHist = null;
		MaintenanceAct unlockedHist = null;
		logger.info("MaintenanceAct Id ==> " + maintenanceActId );
		if( maintenanceActId == null ) return null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC.keySet() ){
				qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_BASIC);
			qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );qry.append( " = ? " );
			maintenanceAct = (MaintenanceAct)getJdbcTemplate().queryForObject( qry.toString(), new String[] { maintenanceActId }, new BeanPropertyRowMapper(MaintenanceAct.class) );
			
			lockedHist = getUserAction(commonError, maintenanceAct.getIdentifier(),CommonConstants.LOCKED);
			if(lockedHist !=  null){

				maintenanceAct.setLockedBy( lockedHist.getUpdateBy());
				maintenanceAct.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, maintenanceAct.getIdentifier(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){

				maintenanceAct.setUnlockedBy( unlockedHist.getUpdateBy() );
				maintenanceAct.setUnlockedTime( unlockedHist.getUpdateTime() );
			}
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch maintenanceAct list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> getMaintenanceAct END <== ");
		return maintenanceAct;
		
	}

	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param identifier
	 * @param action
	 */
	private MaintenanceAct getUserAction(CommonError commonError, String identifier, String action) {
		MaintenanceAct historyInfo = new MaintenanceAct();

		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.keySet() ){
			newQryHist.append( FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(FacilitiesDBConstants.TBL_MAINTENANCE_BASIC_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (MaintenanceAct)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(MaintenanceAct.class) );
		
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}

	/**
	 * This method will activate or deactivate the maintenanceAct
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param status
	 */
	public void updateActiveStatus( CommonError commonError, User loginUser, List<String> idList, String status ){
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("MaintenanceAct Identifier ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		Map<String,List<String>> idMap = new HashMap<>();
		try{
			idMap.put("ids", idList);
			if(status.equals(CommonConstants.Y)){
				insertMaintenanceActHistory( commonError, loginUser, idList, CommonConstants.UNLOCKED );
			    insertMaintenanceControlHistory( commonError, loginUser, idMap.get("ids"), CommonConstants.UNLOCKED );
			    insertMaintenanceSubstanceHistory( commonError, loginUser, idMap.get("ids"), CommonConstants.UNLOCKED );
			    
			}else{
				insertMaintenanceActHistory( commonError, loginUser, idList, CommonConstants.LOCKED );
				insertMaintenanceControlHistory( commonError, loginUser, idMap.get("ids"), CommonConstants.LOCKED );
				insertMaintenanceSubstanceHistory( commonError, loginUser, idMap.get("ids"), CommonConstants.LOCKED );
			}			
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
	        StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_BASIC );qry.append(" SET ");
			qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("ACTIVE") );qry.append( "= ");
			qry.append("'"+status+"'");
			qry.append(" WHERE ");
			qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );qry.append(" IN ");
			qry.append(" (:ids) ");
			db.update(qry.toString(),params);

		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a MaintenanceAct.  " + e.getMessage());

			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");		
	}
	
	/**
	 * This method will insert MaintenanceSubstanceHistory for MaintenanceAct in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	private void insertMaintenanceSubstanceHistory(CommonError commonError, User loginUser,
			List<String> idList, String action) {
		logger.info(" ==> Insert MaintenanceSubstance History START <== ");
		logger.info("MaintenanceSubstance Identifier ==> " + idList );
		StringBuffer qryHist = new StringBuffer();
		StringBuffer qry = new StringBuffer();
		qry.append("SELECT ");
		for( String column : FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.keySet() ){
			qry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get(column) );qry.append( "," );
		}
		qry.setLength(qry.length()-1);
		qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE);qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("ID") );
		qry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			qry.append(" ? ");qry.append(",");
		}
		qry.setLength(qry.length() - 1);
		qry.append(") ");
		List<MaintenanceSubstance> maintenanceSubstanceList = (List<MaintenanceSubstance>)getJdbcTemplate().query(qry.toString(), idList.toArray(),  new BeanPropertyRowMapper<MaintenanceSubstance>(MaintenanceSubstance.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			qryHist.append("INSERT INTO ");qryHist.append( FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE_HISTORY);qryHist.append("( ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_SUBSTANCE_HISTORY.keySet() ){
				if(!column.equals(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE_HISTORY.get("ID"))){
					qryHist.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE_HISTORY.get(column) );qryHist.append( "," );
				}
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(qryHist.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, maintenanceSubstanceList.get(i).getIdentifier());
					ps.setString(2, maintenanceSubstanceList.get(i).getMudAssessment());
					ps.setString(3, maintenanceSubstanceList.get(i).getFloatingMud());ps.setString(4, maintenanceSubstanceList.get(i).getMudOverflowDrain());
					ps.setObject(5, maintenanceSubstanceList.get(i).getRemovableSubstances());ps.setString(6, maintenanceSubstanceList.get(i).getMudRemoval());
					ps.setString(7, maintenanceSubstanceList.get(i).getFloatingMudRemoval());ps.setString(8, maintenanceSubstanceList.get(i).getSealedDisposal());
					ps.setString(9, maintenanceSubstanceList.get(i).getExitOpeningsChanged());ps.setString(10, maintenanceSubstanceList.get(i).getMudStorageVolume());
					ps.setString(11, maintenanceSubstanceList.get(i).getBuffer());ps.setString(12, maintenanceSubstanceList.get(i).getRevival());
					ps.setString(13, maintenanceSubstanceList.get(i).getStability());ps.setString(14, maintenanceSubstanceList.get(i).getSoakAway());
					ps.setString(15, maintenanceSubstanceList.get(i).getTestRun());ps.setString(16, maintenanceSubstanceList.get(i).getDurationPasswordProgramming());
					ps.setString(17, loginUser.getUserId());
					ps.setTimestamp(18, date);ps.setString(19, loginUser.getUserId());
					ps.setTimestamp(20, date);ps.setString(21, action);
				}		
				public int getBatchSize() {
					return maintenanceSubstanceList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when Insert MaintenanceSubstance.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert MaintenanceSubstance END <== ");				
	}
	
	/**
	 * This method will insert MaintenanceControlHistory for MaintenanceAct in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	private void insertMaintenanceControlHistory(CommonError commonError, User loginUser,
			 List<String> idList, String action) {
		logger.info(" ==> Insert MaintenanceControl History START <== ");
		logger.info("MaintenanceControl Identifier ==> " + idList );
		StringBuffer qryHist = new StringBuffer();
		StringBuffer qry = new StringBuffer();
		qry.append("SELECT ");
		for( String column : FacilitiesDBConstants.MAINTENANCE_CONTROL.keySet() ){
			qry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get(column) );qry.append( "," );
		}
		
		qry.setLength(qry.length()-1);
		qry.append(" FROM ");qry.append(FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL);qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get("ID") );
		qry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			qry.append(" ? ");qry.append(",");
		}
		qry.setLength(qry.length() - 1);
		qry.append(") ");
		List<MaintenanceControl> maintenanceControlList = (List<MaintenanceControl>)getJdbcTemplate().query(qry.toString(), idList.toArray(),  new BeanPropertyRowMapper<MaintenanceControl>(MaintenanceControl.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			qryHist.append("INSERT INTO ");qryHist.append( FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL_HISTORY);qryHist.append("( ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_CONTROL_HISTORY.keySet() ){
				if(!column.equals(FacilitiesDBConstants.MAINTENANCE_CONTROL_HISTORY.get("ID"))){
					qryHist.append( FacilitiesDBConstants.MAINTENANCE_CONTROL_HISTORY.get(column) );qryHist.append( "," );
				}
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(qryHist.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, maintenanceControlList.get(i).getIdentifier());
					ps.setString(2, maintenanceControlList.get(i).getFunctionControl());
					ps.setString(3, maintenanceControlList.get(i).getMachineMaintenance());ps.setString(4, maintenanceControlList.get(i).getChangeoverTimes());
					ps.setObject(5, maintenanceControlList.get(i).getLastReportTransfer());ps.setString(6, maintenanceControlList.get(i).getCompanyBookExamination());
					ps.setString(7, maintenanceControlList.get(i).getDuration1());ps.setString(8, maintenanceControlList.get(i).getDuration2());
					ps.setString(9, maintenanceControlList.get(i).getDuration3());ps.setString(10, maintenanceControlList.get(i).getDuration4());
					ps.setString(11, maintenanceControlList.get(i).getOdor());ps.setString(12, maintenanceControlList.get(i).getTemperature());
					ps.setString(13, maintenanceControlList.get(i).getPhValue());ps.setString(14, maintenanceControlList.get(i).getDeepView());
					ps.setString(15, maintenanceControlList.get(i).getOxygenContent());ps.setString(16, maintenanceControlList.get(i).getSampleNumber());
					ps.setString(17, maintenanceControlList.get(i).getSampleComment());ps.setString(18, maintenanceControlList.get(i).getDistributorChannelCleaning());
					ps.setString(19, maintenanceControlList.get(i).getRepairFrom());ps.setString(20, maintenanceControlList.get(i).getPowerFailureDetection());
					ps.setString(21, maintenanceControlList.get(i).getConstructionSiteCompleted());ps.setString(22, maintenanceControlList.get(i).getRemarks());
					ps.setString(23, loginUser.getUserId());
					ps.setTimestamp(24, date);ps.setString(25, loginUser.getUserId());
					ps.setTimestamp(26, date);ps.setString(27, action);
				}		
				public int getBatchSize() {
					return maintenanceControlList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when Insert MaintenanceControl.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert MaintenanceControl END <== ");		
	}
	
	/**
	 * This method will insert MaintenanceActHistory in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	private void insertMaintenanceActHistory(CommonError commonError, User loginUser, List<String> idList,String action) {
		logger.info(" ==> Insert MaintenanceActHistory START <== ");
		logger.info("MaintenanceAct Id ==> " + idList );
		StringBuffer newQryHist = new StringBuffer();
		StringBuffer newQry = new StringBuffer();
		newQry.append("SELECT ");
		for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC.keySet() ){
			newQry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get(column) );newQry.append( "," );
		}
		newQry.setLength(newQry.length()-1);
		newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_BASIC);newQry.append(" WHERE ");newQry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );
		newQry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQry.append(" ? ");newQry.append(",");
		}
		newQry.setLength(newQry.length() - 1);
		newQry.append(") ");
		List<MaintenanceAct> maintenanceActList = (List<MaintenanceAct>)getJdbcTemplate().query(newQry.toString(), idList.toArray(),  new BeanPropertyRowMapper<MaintenanceAct>(MaintenanceAct.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQryHist.append("INSERT INTO ");newQryHist.append( FacilitiesDBConstants.TBL_MAINTENANCE_BASIC_HISTORY);newQryHist.append("( ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.keySet() ){
				if(!column.equals(FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.get("ID"))){
					newQryHist.append( FacilitiesDBConstants.MAINTENANCE_BASIC_HISTORY.get(column) );newQryHist.append( "," );
				}
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQryHist.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					
					ps.setString(1, maintenanceActList.get(i).getIdentifier());
					ps.setString(2,  maintenanceActList.get(i).getFacilityIdentifier());
					ps.setObject(3, maintenanceActList.get(i).getMaintenanceDate());
					ps.setString(4, maintenanceActList.get(i).getMaId());ps.setString(5, maintenanceActList.get(i).getReferenceNumber());
					ps.setString(6, maintenanceActList.get(i).getShortComings());ps.setString(7, maintenanceActList.get(i).getSubsequentProblems());
					ps.setString(8, maintenanceActList.get(i).getWaterDepth());ps.setString(9, maintenanceActList.get(i).getChamberBs1());
					ps.setString(10, maintenanceActList.get(i).getChamberSs1());ps.setString(11, maintenanceActList.get(i).getChamberBs2());
					ps.setString(12, maintenanceActList.get(i).getChamberSs2());ps.setString(13, maintenanceActList.get(i).getChamberBs3());
					ps.setString(14, maintenanceActList.get(i).getChamberSs3());ps.setString(15, maintenanceActList.get(i).getReport());
					ps.setString(16, maintenanceActList.get(i).getStructuralCondition());ps.setString(17, maintenanceActList.get(i).getSurfaceInspection());
					ps.setString(18, maintenanceActList.get(i).getTankInspection());ps.setString(19, maintenanceActList.get(i).getWaterTightness());
					ps.setString(20, maintenanceActList.get(i).getWaterFilling());
					ps.setString(21, loginUser.getUserId());
					ps.setTimestamp(22, date);ps.setString(23, loginUser.getUserId());
					ps.setTimestamp(24, date);ps.setString(25, action);
				}		
				public int getBatchSize() {
					return maintenanceActList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when Insert MaintenanceActHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert MaintenanceActHistory END <== ");		
	}

	/**
	 * This method will insert a MaintenanceAct in DB
	 */
	public void addMaintenanceAct(CommonError commonError, User loginUser,  String facilityId, MaintenanceAct maintenanceAct,
			MaintenanceControl maintenanceControl, MaintenanceSubstance maintenanceSubstance) {
		logger.info(" ==> Add MaintenanceAct START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			String nextRowId =  getNextRowId( FacilitiesDBConstants.TBL_MAINTENANCE_BASIC, FacilitiesDBConstants.maintenanceActDBConfig.get("rowPrefix") );
			maintenanceAct.setIdentifier(nextRowId);
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			qry.append("INSERT INTO ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_BASIC );qry.append("( ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_BASIC.keySet() ){
				if(!column.equals(FacilitiesDBConstants.MAINTENANCE_BASIC.get("ID"))){
					qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(maintenanceAct.getIdentifier());args.add(facilityId);
			args.add(maintenanceAct.getMaintenanceDate());
			args.add(maintenanceAct.getMaId());args.add(maintenanceAct.getReferenceNumber());
			args.add(maintenanceAct.getShortComings());args.add(maintenanceAct.getSubsequentProblems());
			args.add(maintenanceAct.getWaterDepth());args.add(maintenanceAct.getChamberBs1());
			args.add(maintenanceAct.getChamberSs1());args.add(maintenanceAct.getChamberBs2());
			args.add(maintenanceAct.getChamberSs2());args.add(maintenanceAct.getChamberBs3());
			args.add(maintenanceAct.getChamberSs3());args.add(maintenanceAct.getReport());
			args.add(maintenanceAct.getStructuralCondition());args.add(maintenanceAct.getSurfaceInspection());
			args.add(maintenanceAct.getTankInspection());args.add(maintenanceAct.getWaterTightness());
			args.add(maintenanceAct.getWaterFilling());	args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			int rowId = getJdbcTemplate().update( qry.toString(),args.toArray() );
			
			maintenanceAct = getMaintenanceAct(commonError, maintenanceAct.getIdentifier() );
			maintenanceControl.setIdentifier(maintenanceAct.getIdentifier());
			maintenanceSubstance.setIdentifier(maintenanceAct.getIdentifier());
			
			addMaintenanceControl(commonError, loginUser, maintenanceControl);
			addMaintenanceSubstance (commonError, loginUser, maintenanceSubstance); 
			
		}catch( Exception e ){
			logger.info("Error when Insert MaintenanceAct.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Add MaintenanceAct END <== ");
	}

	
	/**
	 * This method will insert a MaintenanceSubstance in DB
	 */
	private void addMaintenanceSubstance(CommonError commonError, User loginUser,MaintenanceSubstance maintenanceSubstance) {
		logger.info("==> Add MaintenanceSubstance START <==");
		logger.info("MaintenanceSubstance Id ==> " + maintenanceSubstance.getIdentifier() );
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("INSERT INTO ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE );qry.append("( ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("IDENTIFIER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_ASSESSMENT"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("FLOATING_MUD"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_OVERFLOW_DRAIN"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("REMOVABLE_SUBSTANCES"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_REMOVAL"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("FLOATING_MUD_REMOVAL"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("SEALED_DISPOSAL"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("EXIT_OPENINGS_CHANGED"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_STORAGE_VOLUME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("BUFFER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("REVIVAL"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("STABILITY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("SOAK_AWAY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("TEST_RUN"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("DURATION_PASSWORD_PROGRAMMING"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("CREATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("CREATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("UPDATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("UPDATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("ACTIVE"));
		qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ");
		List<Object> args = new ArrayList<Object>();
		args.add(maintenanceSubstance.getIdentifier());args.add(maintenanceSubstance.getMudAssessment());args.add(maintenanceSubstance.getFloatingMud());
		args.add(maintenanceSubstance.getMudOverflowDrain());args.add(maintenanceSubstance.getRemovableSubstances());
		args.add(maintenanceSubstance.getMudRemoval());args.add(maintenanceSubstance.getFloatingMudRemoval());args.add(maintenanceSubstance.getSealedDisposal());
		args.add(maintenanceSubstance.getExitOpeningsChanged());args.add(maintenanceSubstance.getMudStorageVolume());
		args.add(maintenanceSubstance.getBuffer());args.add(maintenanceSubstance.getRevival());
		args.add(maintenanceSubstance.getStability());args.add(maintenanceSubstance.getSoakAway());
		args.add(maintenanceSubstance.getTestRun());args.add(maintenanceSubstance.getDurationPasswordProgramming());
		args.add(loginUser.getUserId());args.add(date);
		args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> Add MaintenanceSubstance END <==");		
	}
	
	/**
	 * This method will insert a MaintenanceControl in DB
	 */
	private void addMaintenanceControl(CommonError commonError, User loginUser, MaintenanceControl maintenanceControl) {
		logger.info("==> Add MaintenanceControl START <==");
		logger.info("MaintenanceControl Id ==> " + maintenanceControl.getIdentifier() );
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("INSERT INTO ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL );qry.append("( ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("IDENTIFIER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("FUNCTION_CONTROL"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("MACHINE_MAINTENANCE"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CHANGEOVER_TIMES"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("LAST_REPORT_TRANSFER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("COMPANY_BOOK_EXAMINATION"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION1"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION2"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION3"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION4"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("ODOR"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("TEMPERATURE"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("PH_VALUE"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DEEP_VIEW"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("OXYGEN_CONTENT"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("SAMPLE_NUMBER"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("SAMPLE_COMMENT"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DISTRIBUTOR_CHANNEL_CLEANING"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("REPAIR_FROM"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("POWER_FAILURE_DETECTION"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CONSTRUCTION_SITE_COMPLETED"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("REMARKS"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CREATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CREATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("UPDATE_BY"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("UPDATE_TIME"));qry.append(", ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("ACTIVE"));
		qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ");
		List<Object> args = new ArrayList<Object>();
		args.add(maintenanceControl.getIdentifier());args.add(maintenanceControl.getFunctionControl());args.add(maintenanceControl.getMachineMaintenance());
		args.add(maintenanceControl.getChangeoverTimes());args.add(maintenanceControl.getLastReportTransfer());
		args.add(maintenanceControl.getCompanyBookExamination());args.add(maintenanceControl.getDuration1());args.add(maintenanceControl.getDuration2());
		args.add(maintenanceControl.getDuration3());args.add(maintenanceControl.getDuration4());
		args.add(maintenanceControl.getOdor());args.add(maintenanceControl.getTemperature());
		args.add(maintenanceControl.getPhValue());args.add(maintenanceControl.getDeepView());
		args.add(maintenanceControl.getOxygenContent());args.add(maintenanceControl.getSampleNumber());
		args.add(maintenanceControl.getSampleComment());args.add(maintenanceControl.getDistributorChannelCleaning());
		args.add(maintenanceControl.getRepairFrom());args.add(maintenanceControl.getPowerFailureDetection());
		args.add(maintenanceControl.getConstructionSiteCompleted());args.add(maintenanceControl.getRemarks());
		args.add(loginUser.getUserId());args.add(date);
		args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> Add MaintenanceControl END <==");			
	}

	/**
     * This method will update a participant in DB
	 */
	public void updateMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct,
			MaintenanceControl maintenanceControl, MaintenanceSubstance maintenanceSubstance) {
		logger.info(" ==> Update MaintenanceAct START <== ");
		List<String> ids=new ArrayList<String>();
		StringBuffer qry = new StringBuffer();
		logger.info("MaintenanceAct Id ==> " + maintenanceAct.getIdentifier() );
		try{
			ids.add(maintenanceAct.getIdentifier());
			insertMaintenanceActHistory(commonError, loginUser, ids,CommonConstants.UPDATED);
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			qry.append("UPDATE ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_BASIC );qry.append(" SET ");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );qry.append(" = ?,");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("MAINTENANCE_DATE") );qry.append(" = ?,");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("MA_ID") );qry.append(" = ?,");
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("REFERENCE_NUMBER") );qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("SHORT_COMINGS") );qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("SUBSEQUENT_PROBLEMS"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("WATER_DEPTH"));qry.append(" = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_BS1"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_SS1"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_BS2"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_SS2"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_BS3"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("CHAMBER_SS3"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("REPORT"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("STRUCTURAL_CONDITION"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("SURFACE_INSPECTION"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("TANK_INSPECTION"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("WATER_TIGHTNESS"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("WATER_FILLING"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("UPDATE_BY"));qry.append( " = ?," );
			qry.append(FacilitiesDBConstants.MAINTENANCE_BASIC.get("UPDATE_TIME"));qry.append( " = ?" );
			qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("ID") );qry.append(" = ? ");
			List<Object> args = new ArrayList<Object>();
			args.add(maintenanceAct.getIdentifier());args.add(maintenanceAct.getMaintenanceDate());args.add(maintenanceAct.getMaId());args.add(maintenanceAct.getReferenceNumber());
			args.add(maintenanceAct.getShortComings());args.add(maintenanceAct.getSubsequentProblems());args.add(maintenanceAct.getWaterDepth());
			args.add(maintenanceAct.getChamberBs1());args.add(maintenanceAct.getChamberSs1());args.add(maintenanceAct.getChamberBs2());args.add(maintenanceAct.getChamberSs2());
			args.add(maintenanceAct.getChamberBs3());args.add(maintenanceAct.getChamberSs3());args.add(maintenanceAct.getReport());args.add(maintenanceAct.getStructuralCondition());
			args.add(maintenanceAct.getSurfaceInspection());args.add(maintenanceAct.getTankInspection());
			args.add(maintenanceAct.getWaterTightness());args.add(maintenanceAct.getWaterFilling());
			args.add(loginUser.getUserId());args.add(date);args.add(maintenanceAct.getId());
			getJdbcTemplate().update( qry.toString(),args.toArray() );
			
			updateMaintenanceSubstance( commonError, loginUser, maintenanceSubstance);
			updateMaintenanceControl(commonError, loginUser, maintenanceControl);
						
		}catch( Exception e ){
			logger.info("Error when Insert MaintenanceAct.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> Update MaintenanceAct END <== ");
		
	}
	
	/**
	 * This method will update the MaintenanceSubstance in DB
	 * @param commonError
	 * @param loginUser
	 * @param maintenanceSubstance
	 */
	private void updateMaintenanceSubstance(CommonError commonError, User loginUser, MaintenanceSubstance maintenanceSubstance) {
		logger.info("==> Update MaintenanceSubstance START <==");
		logger.info("Participant Id ==> " + maintenanceSubstance.getIdentifier() );
		List<String> ids=new ArrayList<String>();
		ids.add(maintenanceSubstance.getIdentifier());
		insertMaintenanceSubstanceHistory(commonError, loginUser,ids,CommonConstants.UPDATED);
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("UPDATE ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE );qry.append(" SET ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("IDENTIFIER"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_ASSESSMENT"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("FLOATING_MUD"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_OVERFLOW_DRAIN"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("REMOVABLE_SUBSTANCES"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_REMOVAL"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("FLOATING_MUD_REMOVAL"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("SEALED_DISPOSAL"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("EXIT_OPENINGS_CHANGED"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("MUD_STORAGE_VOLUME"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("BUFFER"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("REVIVAL"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("STABILITY"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("SOAK_AWAY"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("TEST_RUN"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("DURATION_PASSWORD_PROGRAMMING"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("UPDATE_BY"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("UPDATE_TIME"));qry.append(" = ?");
		
		qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("ID") );qry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(maintenanceSubstance.getIdentifier());args.add(maintenanceSubstance.getMudAssessment());args.add(maintenanceSubstance.getFloatingMud());
		args.add(maintenanceSubstance.getMudOverflowDrain());args.add(maintenanceSubstance.getRemovableSubstances());
		args.add(maintenanceSubstance.getMudRemoval());args.add(maintenanceSubstance.getFloatingMudRemoval());args.add(maintenanceSubstance.getSealedDisposal());
		args.add(maintenanceSubstance.getExitOpeningsChanged());args.add(maintenanceSubstance.getMudStorageVolume());
		args.add(maintenanceSubstance.getBuffer());args.add(maintenanceSubstance.getRevival());
		args.add(maintenanceSubstance.getStability());args.add(maintenanceSubstance.getSoakAway());
		args.add(maintenanceSubstance.getTestRun());args.add(maintenanceSubstance.getDurationPasswordProgramming());
		args.add(loginUser.getUserId());args.add(date);
		args.add(maintenanceSubstance.getId());
	    getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> Update MaintenanceSubstance END <==");		
	}

	
	/**
	 * This method will update the MaintenanceControl in DB
	 * @param commonError
	 * @param loginUser
	 * @param maintenanceControl
	 */
	private void updateMaintenanceControl(CommonError commonError, User loginUser,MaintenanceControl maintenanceControl) {
		logger.info("==> Update MaintenanceControl START <==");
		logger.info("MaintenanceControl Id ==> " + maintenanceControl.getIdentifier() );
		List<String> ids=new ArrayList<String>();
		ids.add(maintenanceControl.getIdentifier());
		insertMaintenanceControlHistory(commonError, loginUser,ids,CommonConstants.UPDATED);
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		qry.append("UPDATE ");qry.append( FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL );qry.append(" SET ");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("IDENTIFIER"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("FUNCTION_CONTROL"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("MACHINE_MAINTENANCE"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CHANGEOVER_TIMES"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("LAST_REPORT_TRANSFER"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("COMPANY_BOOK_EXAMINATION"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION1"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION2"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION3"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DURATION4"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("ODOR"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("TEMPERATURE"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("PH_VALUE"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DEEP_VIEW"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("OXYGEN_CONTENT"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("SAMPLE_NUMBER"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("SAMPLE_COMMENT"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("DISTRIBUTOR_CHANNEL_CLEANING"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("REPAIR_FROM"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("POWER_FAILURE_DETECTION"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("CONSTRUCTION_SITE_COMPLETED"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("REMARKS"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("UPDATE_BY"));qry.append(" = ?,");
		qry.append(FacilitiesDBConstants.MAINTENANCE_CONTROL.get("UPDATE_TIME"));qry.append(" = ?");
		
		qry.append(" WHERE ");qry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get("ID") );qry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(maintenanceControl.getIdentifier());args.add(maintenanceControl.getFunctionControl());args.add(maintenanceControl.getMachineMaintenance());
		args.add(maintenanceControl.getChangeoverTimes());args.add(maintenanceControl.getLastReportTransfer());
		args.add(maintenanceControl.getCompanyBookExamination());args.add(maintenanceControl.getDuration1());args.add(maintenanceControl.getDuration2());
		args.add(maintenanceControl.getDuration3());args.add(maintenanceControl.getDuration4());
		args.add(maintenanceControl.getOdor());args.add(maintenanceControl.getTemperature());
		args.add(maintenanceControl.getPhValue());args.add(maintenanceControl.getDeepView());
		args.add(maintenanceControl.getOxygenContent());args.add(maintenanceControl.getSampleNumber());
		args.add(maintenanceControl.getSampleComment());args.add(maintenanceControl.getDistributorChannelCleaning());
		args.add(maintenanceControl.getRepairFrom());args.add(maintenanceControl.getPowerFailureDetection());
		args.add(maintenanceControl.getConstructionSiteCompleted());args.add(maintenanceControl.getRemarks());
		args.add(loginUser.getUserId());args.add(date);
		args.add(maintenanceControl.getId());
	    getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> Update MaintenanceControl END <==");				
	}

	/**
	 * This method will delete a participant in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteMaintenanceAct(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> delete Maintenance Act START <== ");
		logger.info("Maintenance Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertMaintenanceActHistory(commonError, loginUser,idList,CommonConstants.DELETED);
			deleteMaintenanceControl(commonError, loginUser, idList);
			deleteMaintenanceSubstance(commonError, loginUser, idList);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_BASIC);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.MAINTENANCE_BASIC.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		
		}catch( Exception e ){
			logger.info("Error when Delete Participant.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> delete END <== ");		
	}
	
	/**
	 * This method will deleted the MaintenanceSubstance from DB.
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	private void deleteMaintenanceSubstance(CommonError commonError, User loginUser, List<String> idList) {
		logger.info("==> Delete MaintenanceSubstance START <==");
		logger.info("MaintenanceSubstance Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		Map<String,List<String>> idMap = new HashMap<>();
		try{
			insertMaintenanceSubstanceHistory(commonError, loginUser,idList,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when Delete MaintenanceSubstance in Table.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info("==> Delete MaintenanceSubstance END <==");		
	}

	
	/**
	 * This method will deleted the MaintenanceControl from DB.
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	private void deleteMaintenanceControl(CommonError commonError, User loginUser, List<String> idList) {
		logger.info("==> Delete MaintenanceControl START <==");
		logger.info("MaintenanceControl Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertMaintenanceControlHistory(commonError, loginUser,idList,CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when Delete MaintenanceControl in Table.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info("==> Delete MaintenanceControl END <==");		
	}
	
	
	/**
	 * This method used to get a particular MaintenanceControl from DB
	 * @param commonError
	 * @param maintenanceActId
	 * @return maintenanceControl
	 */
	public MaintenanceControl getMaintenanceControl(CommonError commonError, String maintenanceActId) {
		logger.info(" ==> Get a MaintenanceControl START <== ");
		MaintenanceControl maintenanceControl = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_CONTROL.keySet() ){
				newQry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_CONTROL);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.MAINTENANCE_CONTROL.get("IDENTIFIER") );newQry.append( "=? ");
			maintenanceControl = (MaintenanceControl)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{maintenanceActId}, new BeanPropertyRowMapper(MaintenanceControl.class) );
			
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch MaintenanceControl. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a MaintenanceControl END <== ");
		return maintenanceControl;
	}
	
	
	/**
	 * This method used to get a particular MaintenanceSubstance from DB
	 * @param commonError
	 * @param maintenanceActId
	 * @return maintenanceSubstance
	 */
	public MaintenanceSubstance getMaintenanceSubstance(CommonError commonError, String maintenanceActId) {
		logger.info(" ==> Get a MaintenanceSubstance START <== ");
		MaintenanceSubstance maintenanceSubstance = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.keySet() ){
				newQry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(FacilitiesDBConstants.TBL_MAINTENANCE_SUBSTANCE);newQry.append(" WHERE ");
			newQry.append( FacilitiesDBConstants.MAINTENANCE_SUBSTANCE.get("IDENTIFIER") );newQry.append( "=? ");
			maintenanceSubstance = (MaintenanceSubstance)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{maintenanceActId}, new BeanPropertyRowMapper(MaintenanceSubstance.class) );
			
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch MaintenanceSubstance. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a MaintenanceControl END <== ");
		return maintenanceSubstance;
	}

	

}
