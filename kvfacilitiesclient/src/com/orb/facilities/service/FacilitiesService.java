package com.orb.facilities.service;

import java.util.List;
import java.util.Map;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.Document;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.bean.TourFacilities;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.taskplanner.bean.Task;

public interface FacilitiesService {
	
	//Facilities
		public List<Facilities> getActiveFacilitiesList(CommonError commonError);
		public List<Facilities> getInactiveFacilitiesList(CommonError commonError);
		public Facilities getFacilities(CommonError commonError, String facilitiesId);
		public void lockFacilities(CommonError commonError, User loginUser,List<Facilities> selectedFacilitiesList);
		public void unlockFacilities(CommonError commonError, User loginUser,List<Facilities> selectedFacilitiesList);
		public void deleteFacilities(CommonError commonError, User loginUser,List<Facilities> selectedFacilitiesList);
		public void deleteViewedFacilities(CommonError commonError, User loginUser, Facilities facilities);
		public void lockViewedFacilities(CommonError commonError, User loginUser, Facilities facilities);
		public void unlockViewedFacilities(CommonError commonError, User loginUser, Facilities facilities);
		public void add(CommonError commonError, User loginUser, Facilities facilities, List<VisitingDayTimeHours> dayTimeList);
		public void update(CommonError commonError, User loginUser, Facilities facilities, List<VisitingDayTimeHours> dayTimeList);
		public List<VisitingDayTimeHours> getVisitingTimeAndHours(CommonError commonError, String facilitiesId);
		public Map<String, List<VisitingDayTimeHours>> getAllFacilitiesVisitingTimeAndHoursMap(CommonError commonError, List<Facilities> selectedFacilitiesList);
		public List<VisitingDayTimeHours> getVisitingTimeAndHoursList(VisitingDayTimeHours visitingDayTimeHours, int size);
		
	//MaintenanceAct
		public List<MaintenanceAct> getActiveMaintenanceActList(CommonError commonError, String facilityId);
		public List<MaintenanceAct> getInactiveMaintenanceActList(CommonError commonError, String facilityId);
		public MaintenanceAct getMaintenanceAct(CommonError commonError, String maintenanceActId);
		public void lockMaintenanceAct(CommonError commonError, User loginUser,List<MaintenanceAct> selectedMaintenanceActList);
		public void unlockMaintenanceAct(CommonError commonError, User loginUser,List<MaintenanceAct> selectedMaintenanceActList);
		public void deleteMaintenanceAct(CommonError commonError, User loginUser,List<MaintenanceAct> selectedMaintenanceActList);
		public void deleteViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct);
		public void lockViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct);
		public void unlockViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct);
		public void add(CommonError commonError, User loginUser,  String facilityId, MaintenanceAct maintenanceAct , MaintenanceControl maintenanceControl ,MaintenanceSubstance maintenanceSubstance);
		public void update(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct ,MaintenanceControl maintenanceControl ,MaintenanceSubstance maintenanceSubstance );
		public MaintenanceControl getMaintenanceControl(CommonError commonError, String maintenanceActId);
		public MaintenanceSubstance getMaintenanceSubstance(CommonError commonError, String maintenanceActId);
		
		
		//document
		public  void addDocument(CommonError commonError, User loginUser, Document document,String facilitiesId);
		public  void updateDocument(CommonError commonError, User loginUser, Document document,String facilitiesId);
		public List<Document> getActiveDocumentList( CommonError participantError, String facilitiesId);
		public List<Document> getInactiveDocumentList( CommonError participantError, String facilitiesId);
		public void lockDocument(CommonError commonError, User loginUser, List<Document> documentList);
		public void unlockDocument(CommonError commonError, User loginUser, List<Document> documentList);
		public void deleteDocument(CommonError participantError, User loginUser, List<Document> documentList);
		public Document getDocument( CommonError participantError, String documentId );
		public void lockViewedDocument(CommonError commonError, User loginUser, Document document);
		public void unlockViewedDocument(CommonError commonError, User loginUser, Document document);
		public void deleteViewedDocument(CommonError participantError, User loginUser, Document document);
		
		public List<TourFacilities> getTourDataList(CommonError commonError, String facilityId);
		public List<Task> getTaskDataList(CommonError commonError, String id);

}
