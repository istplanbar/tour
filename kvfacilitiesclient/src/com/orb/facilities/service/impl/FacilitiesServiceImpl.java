package com.orb.facilities.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.facilities.bean.Document;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.bean.MaintenanceAct;
import com.orb.facilities.bean.MaintenanceControl;
import com.orb.facilities.bean.MaintenanceSubstance;
import com.orb.facilities.bean.TourFacilities;
import com.orb.facilities.bean.VisitingDayTimeHours;
import com.orb.facilities.dao.DocumentDAO;
import com.orb.facilities.dao.FacilitiesDAO;
import com.orb.facilities.dao.MaintenanceActDAO;
import com.orb.facilities.service.FacilitiesService;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.dao.TaskDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class FacilitiesServiceImpl implements FacilitiesService{
	private static Log logger = LogFactory.getLog(FacilitiesServiceImpl.class);

	@Autowired
	FacilitiesDAO facilitiesDAO;

	@Autowired
	MaintenanceActDAO maintenanceActDAO;

	@Autowired
	DocumentDAO documentDAO;

	@Autowired
	TaskDAO taskDAO;

	//Facilities

	/**
	 * This method calls DAO to get all Inactive Facilities from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Facilities> getInactiveFacilitiesList(CommonError commonError) {
		return  facilitiesDAO.getFacilitiesList(commonError,CommonConstants.N);
	}
	/**
	 * This method calls DAO to get all Inactive Facilities list from DB
	 * @param commonError
	 * @return
	 */
	@Override
	@Cacheable(value="allFacilitiesList")
	public List<Facilities> getActiveFacilitiesList(CommonError commonError) {
		return facilitiesDAO.getFacilitiesList(commonError,CommonConstants.Y);
	}
	/**
	 * This method will interact with DAO to get  Facilities in DB
	 * @param commonError
	 * @param facilitiesId
	 * @return
	 */
	@Override
	public Facilities getFacilities(CommonError commonError, String facilitiesId) {
		return facilitiesDAO.getFacilities(commonError, facilitiesId);
	}
	/**
	 * This method calls DAO to de-activate Facilities
	 * @param commonError
	 * @param loginUser
	 * @param selectedFacilitiesList
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void lockFacilities(CommonError commonError, User loginUser, List<Facilities> selectedFacilitiesList) {
		List<String> idList=new ArrayList<>();
		if( selectedFacilitiesList == null || selectedFacilitiesList.isEmpty() ){
			return;
		}
		for( Facilities facilities : selectedFacilitiesList ){
			idList.add(facilities.getId());
		}
		facilitiesDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );		
	}
	/**
	 * This method calls DAO to activate the locked facilities
	 * @param commonError
	 * @param loginUser
	 * @param selectedFacilitiesList
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void unlockFacilities(CommonError commonError, User loginUser, List<Facilities> selectedFacilitiesList) {
		List<String> idList=new ArrayList<>();
		if( selectedFacilitiesList == null || selectedFacilitiesList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Facilities facilities : selectedFacilitiesList ){
			idList.add(facilities.getId());
		}
		facilitiesDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);			
	}
	/**
	 * This method will interact with DAO to delete a facilities in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedFacilitiesList
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void deleteFacilities(CommonError commonError, User loginUser, List<Facilities> selectedFacilitiesList) {
		List<String> idList=new ArrayList<>();
		if( selectedFacilitiesList == null || selectedFacilitiesList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Facilities facilities : selectedFacilitiesList ){
			idList.add(facilities.getId());
		}
		facilitiesDAO.deleteFacilities(commonError, loginUser, idList);		
	}
	/**
	 * This method calls DAO to deletes particular facilities
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void deleteViewedFacilities(CommonError commonError, User loginUser, Facilities facilities) {
		List<String> idList=new ArrayList<>();
		idList.add(facilities.getId());
		facilitiesDAO.deleteFacilities(commonError, loginUser, idList);
	}
	/**
	 * This method calls DAO to de-activate facilities
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void lockViewedFacilities(CommonError commonError, User loginUser, Facilities facilities) {
		List<String> idList=new ArrayList<>();
		idList.add(facilities.getId());
		facilitiesDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );		
	}
	/**
	 * This method calls DAO to activate facilities
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void unlockViewedFacilities(CommonError commonError, User loginUser, Facilities facilities) {
		List<String> idList=new ArrayList<>();
		idList.add(facilities.getId());
		facilitiesDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );		
	}
	/**
	 * This method will interact with DAO to insert a facilities in DB
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */

	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void add(CommonError commonError, User loginUser, Facilities facilities,List<VisitingDayTimeHours> dayTimeList) {
		facilitiesDAO.addFacilities(commonError, loginUser, facilities, dayTimeList);		
	}
	/**
	 * This method will interact with DAO to update a facilities in DB
	 * @param commonError
	 * @param loginUser
	 * @param facilities
	 */
	@Override
	@CacheEvict(value="allFacilitiesList", allEntries=true)
	public void update(CommonError commonError, User loginUser, Facilities facilities,List<VisitingDayTimeHours> dayTimeList) {
		facilitiesDAO.updateFacilities(commonError, loginUser, facilities, dayTimeList );		
	}


	//MaintenanceAct

	/**
	 * This method will interact with DAO to get all active maintenanceAct from DB
	 */
	@Override
	public List<MaintenanceAct> getActiveMaintenanceActList(CommonError commonError, String facilityId) {
		return maintenanceActDAO.getMaintenanceActList(commonError, facilityId, CommonConstants.Y );
	}

	/**
	 * This method will interact with DAO to get all inactive maintenanceActs from DB
	 */
	@Override
	public List<MaintenanceAct> getInactiveMaintenanceActList(CommonError commonError, String facilityId) {
		return maintenanceActDAO.getMaintenanceActList(commonError,facilityId, CommonConstants.N );
	}

	/**
	 * This method will return the maintenanceAct data from DAO
	 */
	@Override
	public MaintenanceAct getMaintenanceAct(CommonError commonError, String maintenanceActId) {
		return maintenanceActDAO.getMaintenanceAct(commonError, maintenanceActId);
	}

	/**
	 * This method calls DAO to de-activate maintenanceAct
	 */
	@Override
	public void lockMaintenanceAct(CommonError commonError, User loginUser,
			List<MaintenanceAct> selectedMaintenanceActList) {
		List<String> ids=new ArrayList<String>();
		if( selectedMaintenanceActList == null || selectedMaintenanceActList.isEmpty() ){
			return;
		}
		for( MaintenanceAct maintenanceAct : selectedMaintenanceActList ){
			ids.add(maintenanceAct.getIdentifier());	
		}
		maintenanceActDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );			
	}

	/**
	 * This method calls DAO to activate the locked maintenanceAct
	 */
	@Override
	public void unlockMaintenanceAct(CommonError commonError, User loginUser,
			List<MaintenanceAct> selectedMaintenanceActList) {
		List<String> ids=new ArrayList<String>();
		if( selectedMaintenanceActList == null || selectedMaintenanceActList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( MaintenanceAct maintenanceAct : selectedMaintenanceActList ){
			ids.add(maintenanceAct.getIdentifier());	
		}
		maintenanceActDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );			
	}

	/**
	 * This method will interact with DAO to delete a maintenanceAct in DB
	 */
	@Override
	public void deleteMaintenanceAct(CommonError commonError, User loginUser,
			List<MaintenanceAct> selectedMaintenanceActList) {
		List<String> ids=new ArrayList<String>();
		if( selectedMaintenanceActList == null || selectedMaintenanceActList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( MaintenanceAct maintenanceAct : selectedMaintenanceActList ){
			ids.add(maintenanceAct.getIdentifier());
		}
		maintenanceActDAO.deleteMaintenanceAct(commonError, loginUser, ids);			
	}

	/**
	 * This method calls DAO to deletes particular maintenanceAct
	 */
	@Override
	public void deleteViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct) {
		List<String> ids=new ArrayList<String>();
		ids.add(maintenanceAct.getIdentifier());
		maintenanceActDAO.deleteMaintenanceAct(commonError, loginUser, ids);			
	}

	/**
	 * This method calls DAO to de-activate maintenanceAct
	 */
	@Override
	public void lockViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct) {
		List<String> ids=new ArrayList<String>();
		ids.add(maintenanceAct.getIdentifier());	
		maintenanceActDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );			
	}

	/**
	 * This method calls DAO to activate maintenanceAct
	 */
	@Override
	public void unlockViewedMaintenanceAct(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct) {
		List<String> ids=new ArrayList<String>();
		ids.add(maintenanceAct.getIdentifier());
		maintenanceActDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );			
	}

	/**
	 * This method will interact with DAO to insert a maintenanceAct in DB
	 */
	@Override
	public void add(CommonError commonError, User loginUser,  String facilityId, MaintenanceAct maintenanceAct,
			MaintenanceControl maintenanceControl, MaintenanceSubstance maintenanceSubstance) {
		maintenanceActDAO.addMaintenanceAct(commonError, loginUser, facilityId, maintenanceAct, maintenanceControl, maintenanceSubstance);
	}

	/**
	 * This method will interact with DAO to update a maintenanceAct in DB
	 */
	@Override
	public void update(CommonError commonError, User loginUser, MaintenanceAct maintenanceAct,
			MaintenanceControl maintenanceControl, MaintenanceSubstance maintenanceSubstance) {
		maintenanceActDAO.updateMaintenanceAct(commonError, loginUser, maintenanceAct, maintenanceControl, maintenanceSubstance);			
	}

	@Override
	public MaintenanceControl getMaintenanceControl(CommonError commonError, String maintenanceActId) {
		return maintenanceActDAO.getMaintenanceControl(commonError, maintenanceActId);
	}

	@Override
	public MaintenanceSubstance getMaintenanceSubstance(CommonError commonError, String maintenanceActId) {
		return maintenanceActDAO.getMaintenanceSubstance(commonError, maintenanceActId);
	}

	//document
	/**
	 * This method will interact with DAO to insert a document in DB
	 */
	@Override
	public void addDocument(CommonError commonError, User loginUser,Document document,String participantId) {
		documentDAO.addDocument(commonError, loginUser,  document, participantId);
	}

	/**
	 * This method will interact with DAO to update a document in DB
	 */
	@Override
	public void updateDocument(CommonError commonError, User loginUser,Document document,String participantId) {
		documentDAO.updateDocument(commonError, loginUser, document, participantId);
	}

	/**
	 * This method will return the Document data from DAO
	 */
	@Override
	public List<Document> getActiveDocumentList(CommonError commonError, String participantId) {
		return documentDAO.getDocumentList(commonError, participantId,CommonConstants.Y);
	}

	/**
	 * This method will return the Document data from DAO
	 */
	@Override
	public List<Document> getInactiveDocumentList(CommonError commonError, String participantId) {
		return documentDAO.getDocumentList(commonError, participantId,CommonConstants.N);
	}

	/**
	 * This method calls DAO to activate the locked Document
	 */
	public void unlockDocument(CommonError commonError, User loginUser, List<Document> selectedDocument) {
		List<String> idList=new ArrayList<String>();
		if( selectedDocument == null || selectedDocument.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Document document : selectedDocument ){
			idList.add(document.getId());
		}
		documentDAO.updateDocumentActiveStatus(commonError, loginUser, idList, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to de-activate Document
	 */
	@Override
	public void lockDocument(CommonError commonError, User loginUser, List<Document> selectedDocument) {
		List<String> idList=new ArrayList<String>();
		if( selectedDocument == null || selectedDocument.isEmpty() ){
			return;
		}
		for( Document document : selectedDocument ){
			idList.add(document.getId());
		}
		documentDAO.updateDocumentActiveStatus(commonError, loginUser,idList, CommonConstants.N );
	}

	/**
	 * This method will interact with DAO to delete a document in DB
	 */
	@Override
	public void deleteDocument(CommonError commonError, User loginUser, List<Document>  selectedDocument ) {
		List<String> idList=new ArrayList<String>();
		if( selectedDocument == null || selectedDocument.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Document document : selectedDocument ){
			idList.add(document.getId());
		}
		documentDAO.deleteDocument(commonError, loginUser, idList);
	}

	/**
	 * This method will return the document data from DAO
	 */
	@Override
	public Document getDocument(CommonError commonError, String documentId) {
		return documentDAO.getDocument(commonError, documentId);
	}


	/**
	 * This method calls DAO to de-activate document
	 */
	@Override
	public void lockViewedDocument(CommonError commonError, User loginUser, Document document) {
		List<String> idList=new ArrayList<String>();
		idList.add(document.getId());
		documentDAO.updateDocumentActiveStatus(commonError, loginUser,idList, CommonConstants.N );

	}
	/**
	 * This method calls DAO to activate document
	 */
	@Override
	public void unlockViewedDocument(CommonError commonError, User loginUser, Document document) {
		List<String> idList=new ArrayList<String>();
		idList.add(document.getId());
		documentDAO.updateDocumentActiveStatus(commonError, loginUser,idList, CommonConstants.Y );

	}	

	/**
	 * This method calls DAO to deletes particular document
	 */
	@Override
	public void deleteViewedDocument(CommonError commonError, User loginUser, Document document ) {
		List<String> idList=new ArrayList<String>();
		idList.add(document.getId());
		documentDAO.deleteDocument(commonError, loginUser, idList);

	}

	/**
	 * This method will return the visiting time and hours data from DAO
	 */
	@Override
	public List<VisitingDayTimeHours> getVisitingTimeAndHours(CommonError commonError, String facilitiesId) {
		return facilitiesDAO.getVisitingTimeAndHours(commonError, facilitiesId);
	}

	/**
	 * This method will return the visiting data and time list 
	 */
	@Override
	public List<VisitingDayTimeHours> getVisitingTimeAndHoursList(VisitingDayTimeHours visitingDayTimeHours,
			int size) {
		List<VisitingDayTimeHours> list = new ArrayList<VisitingDayTimeHours>();
		for(int i = 0 ; i < 7 ; i++) {
			list.add(new VisitingDayTimeHours());
		}
		return list;
	}
	@Override
	public List<TourFacilities> getTourDataList(CommonError commonError, String facilityId) {		
		return facilitiesDAO.getTourDataList(commonError,facilityId);					
	}
	@Override
	public List<Task> getTaskDataList(CommonError commonError, String id) {
		return taskDAO.getTaskDataList(commonError,id);
	}

	@Override
	public Map<String, List<VisitingDayTimeHours>>  getAllFacilitiesVisitingTimeAndHoursMap(CommonError commonError,
			List<Facilities> selectedFacilitiesList) {
		Map<String, List<VisitingDayTimeHours>> allFacilityVisitingDtlMap = new HashMap<>();
		List<VisitingDayTimeHours> visitingDayTimeList =new ArrayList<>();
		if(!selectedFacilitiesList.isEmpty()){
			List<String> facilityIdList = selectedFacilitiesList.stream().map(Facilities::getIdentifier).collect(Collectors.toList());
			if(!facilityIdList.isEmpty())
				visitingDayTimeList = facilitiesDAO.getAllVisitingTimeAndHours( commonError , facilityIdList);
			
			if(!visitingDayTimeList.isEmpty())
				allFacilityVisitingDtlMap = visitingDayTimeList.stream().collect(Collectors.groupingBy(v -> v.getIdentifier()));
		}
		/*if(!selectedFacilitiesList.isEmpty()){
			for( Facilities facilities : selectedFacilitiesList ){
				visitingDayTimeList = facilitiesDAO.getVisitingTimeAndHours( commonError , facilities.getIdentifier());
				if(!visitingDayTimeList.isEmpty()){
					allFacilityVisitingDtlMap.put(facilities.getIdentifier(), visitingDayTimeList);
				}
			}
		}*/
		return allFacilityVisitingDtlMap;		
	}



}
