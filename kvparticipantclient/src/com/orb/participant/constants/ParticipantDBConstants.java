package com.orb.participant.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;
/**
 * 
 * @author IST planbar / PaPe
 *
 */
public class ParticipantDBConstants {
	
	public static final String TBL_PARTI;
	public static final String TBL_PARTI_HIST;
	public static final String TBL_PARTI_ADDRESS;
	public static final String TBL_PARTI_ADDRESS_HIST;
	public static final String TBL_INGE_TRANS;
	public static final String TBL_INGE_MSG;
	public static final String TBL_CONTACT_PARTI;
	public static final String TBL_CONTACT_PARTI_HIST;
	public static final String TBL_PARTI_ACCESS_HIST;
	public static final String TBL_PARTI_FILTER;
	public static final String TBL_PARTI_FILTER_HIST;
		
	public static final Map<String,String> PARTI_DB_CONFIG;
	public static final Map<String,String> PARTI;
	public static final Map<String,String> PARTI_HIST;
	public static final Map<String,String> PARTI_ADDRESS;
	public static final Map<String,String> PARTI_ADDRESS_HIST;
	public static final Map<String,String> INGE_TRANS;
	public static final Map<String,String> INGE_MSG;
	public static final Map<String,String> CONTACT_PARTI;
	public static final Map<String,String> CONTACT_PARTI_HIST;
	public static final Map<String,String> PARTI_ACCESS_HIST;
	public static final Map<String,String> PARTI_FILTER;
	public static final Map<String,String> PARTI_FILTER_HIST;
	public static final Map<String,String> filterDBConfig;
	
	static{
		TBL_PARTI = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANTS");
		TBL_PARTI_HIST = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANTS_HISTORY");
		TBL_PARTI_ADDRESS = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANTS_ADDRESS");
		TBL_PARTI_ADDRESS_HIST = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANTS_ADDRESS_HISTORY");
		TBL_INGE_TRANS=CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_INGE_WS_TRANS_DETAILS");
		TBL_INGE_MSG=CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_INGE_WS_MSG_DETAILS");
		TBL_CONTACT_PARTI = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_CONTACT_PARTICIPANT");
		TBL_CONTACT_PARTI_HIST = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_CONTACT_PARTICIPANT_HISTORY");
		TBL_PARTI_ACCESS_HIST = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANTS_ACCESS_HISTORY");
		
		TBL_PARTI_FILTER = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANT_CRITERIA");
		TBL_PARTI_FILTER_HIST = CommonUtils.getPropertyValue("PARTI_TABLE_NAMES", "MOD_PARTICIPANT_CRITERIA_HISTORY");
		
		PARTI_DB_CONFIG = (Map<String,String>)CommonUtils.getBean("participantDBConfig");
		PARTI = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANTS");
		PARTI_HIST = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANTS_HISTORY");
		PARTI_ADDRESS = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANTS_ADDRESS");
		PARTI_ADDRESS_HIST = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANTS_ADDRESS_HISTORY");
		INGE_TRANS=(Map<String,String>)CommonUtils.getBean("MOD_INGE_WS_TRANS_DETAILS");
		INGE_MSG=(Map<String,String>)CommonUtils.getBean("MOD_INGE_WS_MSG_DETAILS");
		CONTACT_PARTI = (Map<String,String>)CommonUtils.getBean("MOD_CONTACT_PARTICIPANT");
		CONTACT_PARTI_HIST = (Map<String,String>)CommonUtils.getBean("MOD_CONTACT_PARTICIPANT_HISTORY");
		PARTI_ACCESS_HIST = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANTS_ACCESS_HISTORY");
		
		filterDBConfig = (Map<String,String>)CommonUtils.getBean("filterDBConfig");
		PARTI_FILTER = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANT_CRITERIA");
		PARTI_FILTER_HIST = (Map<String,String>)CommonUtils.getBean("MOD_PARTICIPANT_CRITERIA_HISTORY");
		 
	}
}
