package com.orb.participant.constants;

/**
 * 
 * @author IST planbar / PaPe
 *
 */

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;


public class ParticipantConstants {

	public final static String SELECTED_PARTICIPANT_ID = "SELECTED_PARTICIPANT_ID";
	public final static String SHOW_ACTIVE_PARTICIPANT = "SHOW_ACTIVE_PARTICIPANT";
	public final static String POSTAL = "POSTAL";
	public final static String RESIDENTIAL = "RESIDENTIAL";
	public final static String PARTICIPANT_VIEW_PAGE = "/common/process/participant/viewParticipant.html";
	public final static String VIEW = "VIEW";
	public final static String ACTIVE_TAB_INDEX = "";
	public final static String PARTICIPANT_EDIT_PAGE="/common/process/participant/manipulateParticipant.html";
	public final static String PARTICIPANT_LIST_PAGE="/common/process/participant/loadParticipant.html";
	public final static String MESSAGE = "MESSAGE";
	public final static String TRANSACTION = "TRANSACTION";
	public final static String PARTICIPANT_INFORMATION_VIEW_PAGE = "/common/process/participant/viewParticipantInformation.html";
	public final static String PARTICIPANT_INFORMATION_LIST_PAGE = "/common/process/participant/loadParticipantInformation.html";
	
	public final static String PARTICIPANT_AUSKUNFT_VIEW_PAGE = "/common/process/participant/viewAuskunft.html";
	public final static String PARTICIPANT_AUSKUNFT_LIST_PAGE = "/common/process/participant/loadAuskunft.html";
	
	public final static String PARTICIPANT_LANGINFO_VIEW_PAGE = "/common/process/participant/viewParticipantLangInfo.html";
	public final static String PARTICIPANT_LANGINFO_LIST_PAGE = "/common/process/participant/loadParticipantLangInfo.html";
	
	public final static String PARTICIPANT_INGE_EXEC_VIEW_PAGE = "/common/process/participant/viewIngeExecute.html";
	public final static String PARTICIPANT_INGE_EXEC_LIST_PAGE = "/common/process/participant/loadIngeExecute.html";
	
	public final static String PARTICIPANT_LIST_HTML = "participant/list.xhtml";
	public final static String PARTICIPANT_VIEW_HTML = "participant/view.xhtml";
	public final static String PARTICIPANT_EDIT_HTML = "participant/addUpdate.xhtml";
	public final static String EMAIL_SMS_VALIDATION = "emailSmsValidation";
	public final static String PARTICIPANT_LOCKED_SUCCESS = "participant_locked_success";
	public final static String PARTICIPANT_UNLOCKED_SUCCESS = "participant_unlocked_success";
	public final static String PARTICIPANT_DELETED_SUCCESS = "participant_delete_success";
	public final static String PARTICIPANT_ADDED_SUCCESS = "participant_added_success";
	public final static String PARTICIPANT_UPDATED_SUCCESS = "participant_update_success";
	
	public final static String PARTCIPANT_TOGGLER_LIST = "PARTCIPANT_TOGGLER_LIST" ;
	public final static String PARTICIPANT_REGISTRATION = "READY" ;
	public final static String PARTICIPANT_SPRACHTEST_REGISTRATION = "SPRACHREADY" ;
	
	public final static String REGISTRATION_REQ = "REG" ;
	public final static String RE_REGISTRATION_REQ = "REREG";
	public final static String SPRACH_TEST_REQ = "SPRACH" ;
	public final static String KURS_REQ = "KURS" ;
	public final static String INGE_APPROVED = "Inge_Approved" ;
	public final static String INGE_REJECTED = "Inge_Rejected";
	public final static String INGE_EXECUTE = "Inge_Executed";
	
	public final static String ADDRESS_CO = "address1";
	public final static String ADDRESS_STATE = "address2";
	public final static String ADDRESS_COUNTRY = "address3";
	public final static String PARTCIPANT_TOGGLER_LANGUAGE_LIST = "PARTCIPANT_TOGGLER_LANGUAGE_LIST" ;
	public final static String PARTCIPANT_TOGGLER_APPROVAL_LIST = "PARTCIPANT_TOGGLER_APPROVAL_LIST" ;
	public final static String WARN_MSG = "inge_warn_msg";
	public final static Map<String,String> CARRIER;
	
	
	public final static String PARTICIPANT_SENT_SUCCESS = "message_sent_success";
		
	public static final String MODULE_NAME = "Teilnehmerliste";
	public final static String GREATER_THAN = "gr��er als";
	public final static String LESS_THAN = "kleiner als";
	public final static String EQUALS = "ist gleich";
	public final static String LIKE = "entspricht";
	public final static String STARTS_WITH = "beginnt mit";
	public final static String ENDS_WITH = "endet mit";
	public final static String CONTAINS = "enth�lt";
	public final static String PERCENTAGE = "%";
	public final static String NOT = "NOT";
	public final static Map<String,String> DEFAULT_COLS, PARTICIPANT_MODULE_IDS;
	public static final Map<String,String> FILTER_CRITERIA_LIST;
	public final static String SHOW_EMAIL = "E-Mail";
	public final static String SHOW_SMS = "SMS";
	public final static String WARN_EMPTY_TAG = "warn_empty_tag";
	public static final String CRITERIA_ADDED_SUCCESS = "criteria_added_success";
	
	
	
	static{
		PARTICIPANT_MODULE_IDS = (Map<String,String>)CommonUtils.getBean("PARTICIPANT_MODULE_IDS");
		DEFAULT_COLS = (Map<String,String>)CommonUtils.getBean("DEFAULT_COLS");
		FILTER_CRITERIA_LIST = (Map<String,String>)CommonUtils.getBean("PARTICIPANT_FILTER_CRITERIA_LIST");
		CARRIER = (Map<String,String>) CommonUtils.getBean("CARRIER");
	}
	

}