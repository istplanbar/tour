package com.orb.participant.dao;


/**
 * 
 * @author IST planbar / PaPe
 *
 */
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.layoutadmnistration.model.TableDesc;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.participant.bean.Criteria;
import com.orb.participant.bean.Participant;
import com.orb.participant.bean.ParticipantAddress;
import com.orb.participant.constants.ParticipantConstants;
import com.orb.participant.constants.ParticipantDBConstants;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class ParticipantDAO extends AbstractDAO {
	private static Log logger = LogFactory.getLog(ParticipantDAO.class);
	
	/**
	 * This method will return list of Participants from DB
	 * @param participantError
	 * @return
	 */
	public List<Participant> getParticipantList( CommonError participantError, String activeFlag ) {
		logger.info(" ==> getParticipantList START <== ");
		List<Participant> participantList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.PARTI.keySet() ){
				qry.append( ParticipantDBConstants.PARTI.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI);qry.append(" WHERE ");
			qry.append(ParticipantDBConstants.PARTI.get("ACTIVE"));qry.append(" = ? ");
			participantList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Participant.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch participant list. Exception ==> " + e.getMessage() );
			participantError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getParticipantList END <== ");
		return participantList;
	}
	
	/**
	 * This method will return list of Participants from DB
	 * @param participantError
	 * @return
	 */
	public List<Participant> getSprachParticipantList( CommonError participantError, String activeFlag ) {
		logger.info(" ==> getParticipantList START <== ");
		List<Participant> participantList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.PARTI.keySet() ){
				qry.append( ParticipantDBConstants.PARTI.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI);qry.append(" WHERE ");
			qry.append(ParticipantDBConstants.PARTI.get("ACTIVE"));qry.append(" = ? ");
			qry.append( ParticipantDBConstants.PARTI.get("SPRACH_TEST_REQUIRED") );qry.append(" = ? ");
			participantList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag, CommonConstants.TRUE}, new BeanPropertyRowMapper(Participant.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Participant list. Exception ==> " + e.getMessage() );
			participantError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getParticipantList END <== ");
		return participantList;
	}
	
	
	/**
	 * This method will return the Participant data to the given Id
	 * @param participantError
	 * @param participantId
	 * @return
	 */
	public Participant getParticipant( CommonError commonError,User loginUser, String participantId ){
		logger.info(" ==> getParticipant START <== ");
		Participant participant = null;
		Participant lockedHist = null;
		Participant unlockedHist = null;
		Participant approvedHist = null;
		Participant rejectedHist = null;
		Participant executedHist = null;
		
		logger.info("Participant Id ==> " + participantId );
		if( participantId == null ) return null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.PARTI.keySet() ){
				qry.append( ParticipantDBConstants.PARTI.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI);
			qry.append(" WHERE ");qry.append( ParticipantDBConstants.PARTI.get("IDENTIFIER") );qry.append( " = ? " );
			participant = (Participant)getJdbcTemplate().queryForObject( qry.toString(), new String[] { participantId }, new BeanPropertyRowMapper(Participant.class) );
			addParticipantAccess(commonError,loginUser,participantId,CommonConstants.VIEWED);
			lockedHist = getUserAction(commonError, participant.getIdentifier(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				
				participant.setLockedBy( lockedHist.getUpdateBy());
				participant.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, participant.getIdentifier(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				
				participant.setUnlockedBy( unlockedHist.getUpdateBy() );
				participant.setUnlockedTime( unlockedHist.getUpdateTime() );
			}
			
			approvedHist = getUserAction(commonError, participant.getIdentifier(),CommonConstants.INGE_APPROVED);
			if(approvedHist !=  null){
				
				participant.setApprovedBy( approvedHist.getUpdateBy() );
				participant.setApprovedTime( approvedHist.getUpdateTime() );
			}
			
			rejectedHist = getUserAction(commonError, participant.getIdentifier(),CommonConstants.INGE_REJECTED);
			if(rejectedHist !=  null){
				
				participant.setRejectedBy( rejectedHist.getUpdateBy() );
				participant.setRejectedTime( rejectedHist.getUpdateTime() );
			}
			
			executedHist = getUserAction(commonError, participant.getIdentifier(),CommonConstants.INGE_EXECUTE);
			if(executedHist !=  null){
				
				participant.setExecutedBy( executedHist.getUpdateBy() );
				participant.setExecutedTime( executedHist.getUpdateTime() );
			}
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch participant List. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> getParticipant END <== ");
		return participant;
	}
	
	/**
	 * This method will insert a participant access in DB
	 * @param commonError
	 * @param participant
	 * @param activeFlag
	 * @return 
	 */
	public void addParticipantAccess(CommonError commonError,User loginUser, String participantId, String activeFlag) {
		logger.info(" ==> addParticipantAccess START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			
			newQry.append("INSERT INTO ");newQry.append( ParticipantDBConstants.TBL_PARTI_ACCESS_HIST );newQry.append("( ");
			newQry.append( ParticipantDBConstants.PARTI_ACCESS_HIST.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_ACCESS_HIST.get("ACCESS_TYPE") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_ACCESS_HIST.get("ACCESSED_BY") );
			newQry.append(") VALUES(?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(participantId);
			args.add(activeFlag);
			args.add(loginUser.getUserId());
			getJdbcTemplate().update( newQry.toString(),args.toArray() );
		}catch( Exception e ){
			logger.info("Error when add Participant Access.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> addParticipantAccess END <== ");
			
	}

	/**
	 * This method will return the Participant Address to the given id with the type. 
	 * @param participantError
	 * @param participantId
	 * @param type
	 * @return 
	 */
	public ParticipantAddress getAddress( CommonError participantError, String participantId, String type ){
		logger.info(" ==> getAddress START <== ");
		ParticipantAddress participantAddress = null;
		logger.info("Participant Id ==> " + participantId );
		logger.info("Participant Addres Type ==> " + type );
		if( participantId == null || type == null ) return null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.PARTI_ADDRESS.keySet() ){
				qry.append( ParticipantDBConstants.PARTI_ADDRESS.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI_ADDRESS);
			qry.append(" WHERE ");qry.append( ParticipantDBConstants.PARTI_ADDRESS.get("PARTICIPANT_ID") );qry.append( " = ? AND " );
			qry.append( ParticipantDBConstants.PARTI_ADDRESS.get("ADDRESS_TYPE") );qry.append( " = ? " );
			
			participantAddress = (ParticipantAddress) getJdbcTemplate().queryForObject( qry.toString(), new String[] { participantId, type }, new BeanPropertyRowMapper(ParticipantAddress.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch participant add list. Exception ==> " + e.getMessage() );
			participantError.addError(CommonConstants.DB_GETRECORD_ERROR);
		}
		logger.info(" ==> getAddress END <== ");
		return participantAddress;
	}
	
	/**
	 * This method will insert a participant in DB
	 * @param participantError
	 * @param p
	 */
	public void addParticipant(CommonError participantError, User loginUser, Participant p, ParticipantAddress homeAddress, ParticipantAddress postalAddress ) {
		logger.info(" ==> add START <== ");
		
		try{
			scanParticipant(p);
			insertRecord(loginUser, p,ParticipantDBConstants.TBL_PARTI,ParticipantDBConstants.PARTI_DB_CONFIG.get("rowPrefix"));
			p = getParticipant(participantError,loginUser, p.getIdentifier() );
			homeAddress.setParticipantId( p.getIdentifier() );
			postalAddress.setParticipantId( p.getIdentifier() );
			
			addAddress(participantError, loginUser, homeAddress);
			addAddress(participantError, loginUser, postalAddress);
			
		}catch( Exception e ){
			logger.info("Error when Insert Participant.  " + e.getMessage());
			participantError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> add END <== ");
	}
	
	
	/**
	 * This method will activate or deactivate the participants
	 * @param participantError
	 * @param loginUser
	 * @param idList
	 * @param status
	 */
	public void updateActiveStatus( CommonError participantError, User loginUser, List<String> idList, String status ){
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("Participant Id ==> " + idList );
		try{
			
			if(CommonConstants.Y.equals(status)){
				insertParticipantHistory( participantError, loginUser, idList, CommonConstants.UNLOCKED );
			}else{
				insertParticipantHistory( participantError, loginUser, idList, CommonConstants.LOCKED );
			}
	        StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( ParticipantDBConstants.TBL_PARTI );qry.append(" SET ");
			qry.append( ParticipantDBConstants.PARTI.get("ACTIVE") );qry.append( "=? ");
			qry.append(" WHERE ");
			qry.append( ParticipantDBConstants.PARTI.get("ID") );qry.append("=? ");
			 getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setString(1, status);
						ps.setString(2, idList.get(i));
					}
					
					public int getBatchSize() {
						return idList.size();
					}
				});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Participant.  " + e.getMessage());
			participantError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");
	}

	
	/**
	 * This method will update a participant in DB
	 * @param commonError
	 * @param p
	 */
	public void updateParticipant( CommonError commonError, User loginUser, Participant p, ParticipantAddress homeAddress, ParticipantAddress postalAddress ) {
		
		logger.info(" ==> update START <== ");
		
		logger.info("Participant Id ==> " + p.getId() );
		try{
			
			scanParticipant(p);
			updateRecordById(loginUser, p,ParticipantDBConstants.TBL_PARTI);
			
			String addressTableName=ParticipantDBConstants.TBL_PARTI_ADDRESS;
			updateRecordById(loginUser, homeAddress,addressTableName);
			updateRecordById(loginUser, postalAddress,addressTableName);
			addParticipantAccess(commonError,loginUser,p.getIdentifier(),CommonConstants.UPDATED);
			
		}catch( Exception e ){
			logger.info("Error when Insert Participant.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> update END <== ");
	}
	

	/**
	 * This method will delete a participant in DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param homeAddress
	 * @param postalAddress
	 */
	public void deleteParticipant(CommonError commonError, User loginUser, List<String> idList, ParticipantAddress homeAddress, ParticipantAddress postalAddress) {
		logger.info(" ==> delete START <== ");
		logger.info("Participant Id ==> " + idList);
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertParticipantHistory(commonError, loginUser, idList, CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(ParticipantDBConstants.TBL_PARTI);newQry.append(" WHERE ");
			newQry.append( ParticipantDBConstants.PARTI.get("ID") );newQry.append(" IN ");
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when Delete Participant.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> delete END <== ");
	}
	
	/**
	 * This method will insert Participant History in DB
	 * @param participantError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	private void insertParticipantHistory( CommonError commonError, User loginUser, List<String> idList, String action ) throws Exception {
		logger.info(" ==> insertHistory START <== ");
		logger.info("Participant Id ==> " + idList );
		StringBuffer qry = new StringBuffer();
		StringBuffer qryHist = new StringBuffer();
		qry.append("SELECT ");
		for( String column : ParticipantDBConstants.PARTI.keySet() ){
			qry.append( ParticipantDBConstants.PARTI.get(column) );qry.append( "," );
		}
		qry.setLength(qry.length()-1);
		qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI);qry.append(" WHERE ");qry.append( ParticipantDBConstants.PARTI.get("ID") );
		qry.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			qry.append(" ? ");qry.append(",");
		}
		qry.setLength(qry.length() - 1);
		qry.append(") ");
		List<Participant> participantList = (List<Participant>)getJdbcTemplate().query(qry.toString(), idList.toArray(),  new BeanPropertyRowMapper<Participant>(Participant.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			qryHist.append("INSERT INTO ");qryHist.append( ParticipantDBConstants.TBL_PARTI_HIST);qryHist.append("( ");
			for( String column : ParticipantDBConstants.PARTI_HIST.keySet() ){
				if(!column.equals(ParticipantDBConstants.PARTI_HIST.get("ID"))){
					qryHist.append( ParticipantDBConstants.PARTI_HIST.get(column) );qryHist.append( "," );
				}
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(qryHist.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, participantList.get(i).getSurName());
					ps.setString(2, participantList.get(i).getFirstName());
					ps.setObject(3, participantList.get(i).getDateOfBirth());
					ps.setString(4, participantList.get(i).getGender());
					ps.setString(5, participantList.get(i).getPlaceOfBirth());
					ps.setString(6, participantList.get(i).getNationality());
					ps.setString(7, participantList.get(i).getIdentifier());
					ps.setString(8, participantList.get(i).getTelephone());
					ps.setString(9, participantList.get(i).getMobile());
					ps.setString(10, participantList.get(i).getEmail());
					ps.setString(11, participantList.get(i).getCarrierIdentifier());
					ps.setObject(12, participantList.get(i).getRegistrationDate());
					ps.setString(13, participantList.get(i).getEducation());
					ps.setString(14, participantList.get(i).getNoOfSchoolYears());
					ps.setString(15, participantList.get(i).getLiteracy());
					ps.setString(16, participantList.get(i).getTestRequired());
					ps.setString(17, participantList.get(i).getTestIdentifier());
					ps.setObject(18, participantList.get(i).getTestDate());
					ps.setString(19, participantList.get(i).getCourseRecommended());
					ps.setString(20, participantList.get(i).getSectionRecommended());
					ps.setString(21, participantList.get(i).getMarkForDeletion());
					ps.setString(22, participantList.get(i).getAktenzeichen());
					ps.setString(23, participantList.get(i).getRegistrationType());
					ps.setString(24, participantList.get(i).getSprachTestRequired());
					ps.setString(25, participantList.get(i).getSprachTestCarrier());
					ps.setString(26, participantList.get(i).getAuftragsNummer());
					ps.setString(27, participantList.get(i).getBogenNummer());
					ps.setObject(28, participantList.get(i).getSprachTestDate());
					ps.setString(29, participantList.get(i).getAusweisNummer());
					ps.setString(30, participantList.get(i).getIngeStatus());
					ps.setString(31, participantList.get(i).getLocation());
					ps.setString(32, participantList.get(i).getTags());
					ps.setString(33, loginUser.getUserId());
					ps.setTimestamp(34, date);
					ps.setString(35, loginUser.getUserId());
					ps.setTimestamp(36, date);
					ps.setString(37, action);
				}		
				public int getBatchSize() {
					return participantList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when Insert History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		
	
		logger.info(" ==> insertHistory END <== ");
	
	}
			
	
	/**
	 * Get user actions from table for particular table
	 * @return
	 */
	private Participant getUserAction(CommonError commonError,String identifier, String action){
		Participant historyInfo = new Participant();
		String fullName = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : ParticipantDBConstants.PARTI_HIST.keySet() ){
			newQryHist.append( ParticipantDBConstants.PARTI_HIST.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(ParticipantDBConstants.TBL_PARTI_HIST);newQryHist.append(" WHERE ");newQryHist.append( ParticipantDBConstants.PARTI_HIST.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(ParticipantDBConstants.PARTI_HIST.get(" ACTION "));
		newQryHist.append(" =? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(ParticipantDBConstants.PARTI_HIST.get(" ID "));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Participant)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(Participant.class) );
		
		
		}
		catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
			
		}
		
		return historyInfo;
		
	}
	
	/**
	 * This method will add the new address for Participant in DB
	 * @param participantError
	 * @param loginUser
	 * @param pa
	 */
	private void addAddress( CommonError participantError, User loginUser, ParticipantAddress pa) throws Exception {
		logger.info("==> addAddress START <==");
		logger.info("Participant Id ==> " + pa.getId() );
		StringBuffer qry = new StringBuffer();
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		qry.append("INSERT INTO ");qry.append( ParticipantDBConstants.TBL_PARTI_ADDRESS );qry.append("( ");
		for( String column : ParticipantDBConstants.PARTI_ADDRESS.keySet() ){
			if(!column.equals(ParticipantDBConstants.PARTI_ADDRESS.get("ID"))){
				qry.append( ParticipantDBConstants.PARTI_ADDRESS.get(column) );qry.append( "," );
			}
		}
		qry.setLength(qry.length()-1);
		qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ");
		List<Object> args = new ArrayList<Object>();
		args.add(pa.getParticipantId());args.add(pa.getAddressType());args.add(pa.getStreet());args.add(pa.getHouseNumber());
		args.add(pa.getMailBox());args.add(pa.getCareOf());args.add(pa.getZip());args.add(pa.getCity());
		args.add(pa.getAdditionToAddress());args.add(pa.getState());args.add(pa.getCountry());
		args.add(loginUser.getUserId());
		args.add(date);args.add(loginUser.getUserId());
		args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( qry.toString(),args.toArray() );
		logger.info("==> addAddress END <==");
	}
			
	/**
	 * This method will deleted the Participant address from DB.
	 * @param commonError
	 * @param loginUser
	 * @param participantId
	 */
	public void deleteAddress( CommonError commonError, User loginUser, ParticipantAddress homeAddress, ParticipantAddress postalAddress ){
		logger.info("==> deleteAddress START <==");
		logger.info("Participant Id ==> " + homeAddress.getParticipantId() );
		try{
			addAddressHist(commonError, loginUser, homeAddress.getId());
			addAddressHist(commonError, loginUser, postalAddress.getId());
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE ");qry.append( " FROM ");
			qry.append( ParticipantDBConstants.TBL_PARTI_ADDRESS );qry.append(" WHERE ");
			qry.append( ParticipantDBConstants.PARTI_ADDRESS.get("PARTICIPANT_ID") );qry.append(" = ? ");
			getJdbcTemplate().update(qry.toString(),homeAddress.getParticipantId());
		}catch( Exception e ){
			logger.info("Error when Delete Participant Address in Table.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info("==> deleteAddress END <==");
	}
	
	/**
	 * This method will insert the records in Address History Table for auditing purpose
	 * @param participantError
	 * @param loginUser
	 * @param id
	 * @throws Exception
	 */
	private void addAddressHist( CommonError commonError, User loginUser, String id) throws Exception{
		logger.info("==> addAddress START <==");
		logger.info("Participant Id ==> " + id );
		StringBuffer qry = new StringBuffer();
		StringBuffer qryHist = new StringBuffer();
		qry.append("SELECT ");
		for( String column : ParticipantDBConstants.PARTI_ADDRESS.keySet() ){
			qry.append( ParticipantDBConstants.PARTI_ADDRESS.get(column) );qry.append( "," );
		}
		qry.setLength(qry.length()-1);
		qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI_ADDRESS);qry.append(" WHERE ");
		qry.append( ParticipantDBConstants.PARTI_ADDRESS.get("ID") );qry.append( "= ?" );
		ParticipantAddress pa = (ParticipantAddress) getJdbcTemplate().queryForObject( qry.toString(), new String[] { id }, new BeanPropertyRowMapper(ParticipantAddress.class) );
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			qryHist.append("INSERT INTO ");qryHist.append( ParticipantDBConstants.TBL_PARTI_ADDRESS_HIST);qryHist.append("( ");
			for( String column : ParticipantDBConstants.PARTI_ADDRESS_HIST.keySet() ){
				if(!column.equals(ParticipantDBConstants.PARTI_ADDRESS_HIST.get("ID"))){
					qryHist.append( ParticipantDBConstants.PARTI_ADDRESS_HIST.get(column) );qryHist.append( "," );
				}
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)  ");
			List<Object> args = new ArrayList<Object>();
			args.add(pa.getParticipantId());args.add(pa.getAddressType());args.add(pa.getStreet());args.add(pa.getHouseNumber());
			args.add(pa.getMailBox());args.add(pa.getCareOf());args.add(pa.getZip());args.add(pa.getCity());
			args.add(pa.getAdditionToAddress());args.add(pa.getState());args.add(pa.getCountry());
			args.add(loginUser.getUserId());args.add(date);
			getJdbcTemplate().update(qryHist.toString(),args.toArray() );
			}catch( Exception e ){
				logger.info("Error when Insert Crm History.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_INSERT_ERROR);
			}
			
			logger.info("==> addAddress END <==");
		}
		
	
	public void scanParticipant( Participant p ){
		if( p == null ) return;
		if( "true".equals(p.getTestRequired()) ){
			p.setTestIdentifier(null);
			p.setSectionRecommended(null);
			p.setCourseRecommended(null);
			p.setTestDate(null);
		}
		if( !"true".equals(p.getSprachTestRequired()) ){
			p.setSprachTestCarrier(null);
			p.setAuftragsNummer(null);
			p.setBogenNummer(null);
			p.setSprachTestDate(null);
			p.setAusweisNummer(null);
		}
	}
	
	
	/**
	 * This method update inge status in participant table
	 * @param participantError
	 * @param idList
	 * @param ingeStatus
	 */
	public void updateInge(CommonError participantError, List<String> idList, User loginUser, String ingeStatus, String ingeAction) {
		logger.info(" ==> update Inge Status START <== ");
		logger.info("Participant Id ==> " + idList );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			if(CommonConstants.INGE_APPROVED.equalsIgnoreCase(ingeAction) || CommonConstants.INGE_REJECTED.equalsIgnoreCase(ingeAction) 
					|| CommonConstants.INGE_EXECUTE.equalsIgnoreCase(ingeAction)){
				
				insertParticipantHistory(participantError, loginUser, idList, ingeAction);
			}
					
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", idList);
	        StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( ParticipantDBConstants.TBL_PARTI );qry.append(" SET ");
			qry.append( ParticipantDBConstants.PARTI.get("ACTIVE") );qry.append( "= ");
			qry.append("'"+ingeStatus+"'");
			qry.append(" WHERE ");
			qry.append( ParticipantDBConstants.PARTI.get("ID") );qry.append(" IN ");
			qry.append(" (:ids) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when update  Inge Status of a Participant.  " + e.getMessage());
			participantError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> update Inge Status END <== ");
	}
	
	
	/**
	 * This method will insert a participant in DB
	 * @param participantError
	 * @param p
	 */
	public void addContactParticipant(CommonError commonError, User loginUser, Participant participant, String participantId) {
		logger.info(" ==> add START <== ");
		StringBuffer qry = new StringBuffer();
		try{
			
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			qry.append("INSERT INTO ");qry.append( ParticipantDBConstants.TBL_CONTACT_PARTI );qry.append("( ");
			for( String column : ParticipantDBConstants.CONTACT_PARTI.keySet() ){
				if(!column.equals(ParticipantDBConstants.CONTACT_PARTI.get("ID"))){
					qry.append( ParticipantDBConstants.CONTACT_PARTI.get(column) );qry.append( "," );
				}
			}
			qry.setLength(qry.length()-1);
			qry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(participantId);args.add(participant.getTypeOfContact());
			args.add(participant.getContactThrough());args.add(participant.getSubject());
			args.add(participant.getContent());
			args.add(participant.getMessage());
			args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( qry.toString(),args.toArray() );
		}
			catch( Exception e ){
				logger.info("Error when Insert contactParticipant.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_INSERT_ERROR);
			}
			logger.info(" ==> addContactParticipant END <== ");
	
	}
	
	/**
	 * This method results the mapped tables and values for PDF printing
	 * @param error
	 * @param moduleName
	 * @return
	 */	
	public List<Participant> getMappedTablesAndValues(CommonError error, String moduleName,List<Criteria> criteriaList){

        logger.info(" ==> Get Mapped Tables List START <== ");
        Set<String> mappedTables = new HashSet<String>();
        List<String> mappedTableList = null;
        List<String> valueList = new ArrayList<String>();
        String keyAndValue1 = null;
        List<Map<String, Object>> tableAndValues = null;
        List<Participant> participantList = null;
        StringBuilder builder = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();

       try{
      
        StringBuffer qry = new StringBuffer("SELECT ");
		qry.append(AdminDBConstants.MODULES_INFO.get("NAME_OF_THE_TABLE"));
		qry.append(" FROM ");
		qry.append(AdminDBConstants.TBL_MODULES_INFO);
		qry.append(" WHERE ");
		qry.append(AdminDBConstants.MODULES_INFO.get("MODULE_NAME"));
		qry.append(" =? ");

		mappedTableList = getJdbcTemplate().queryForList( qry.toString(), new String[] { moduleName },String.class);
        if(!mappedTableList.isEmpty()){
            mappedTables.addAll(mappedTableList);
         }

       for(String table : mappedTables){
       if(!table.equals("MOD_PARTICIPANTS_ADDRESS")){
      
           int i = 0;
       
       for(Criteria c:criteriaList){
    	   if(c.getFilterCondition().equals(ParticipantConstants.NOT)){
    		   if( i > 0){
 	         	  stringBuilder.append(" "+criteriaList.get(i-1).getFilterCondition()+" ");
    		   }
    	      if(c.getFilterCriteria().equals(ParticipantConstants.GREATER_THAN)||c.getFilterCriteria().equals(ParticipantConstants.LESS_THAN)||
    	              c.getFilterCriteria().equals(ParticipantConstants.EQUALS)  ){
    		      stringBuilder.append(" NOT "+c.getSelectedInput()+" ");
    		   
    	       }else{
    		     stringBuilder.append(c.getSelectedInput());
    		     stringBuilder.append(" "+c.getFilterCondition()+" ");
    	       }
    	   
    	   }else{
    		   if( i > 0)
    	         	  stringBuilder.append(" "+criteriaList.get(i).getFilterCondition()+" ");
    		   stringBuilder.append(c.getSelectedInput()); 
    	   }
          
    	 
          if(c.getFilterCriteria().equals(ParticipantConstants.GREATER_THAN)){
             stringBuilder.append(" > ");
          }else if(c.getFilterCriteria().equals(ParticipantConstants.LESS_THAN)){
             stringBuilder.append(" < ");
          }else if(c.getFilterCriteria().equals(ParticipantConstants.EQUALS)){
             stringBuilder.append(" = ");
          }else if(c.getFilterCriteria().equals(ParticipantConstants.CONTAINS)||c.getFilterCriteria().equals(ParticipantConstants.STARTS_WITH)||
                   c.getFilterCriteria().equals(ParticipantConstants.ENDS_WITH)||c.getFilterCriteria().equals(ParticipantConstants.LIKE)){
             stringBuilder.append(" LIKE ");
          }if(c.getFilterCriteria().equals(ParticipantConstants.GREATER_THAN)||c.getFilterCriteria().equals(ParticipantConstants.LESS_THAN)||
              c.getFilterCriteria().equals(ParticipantConstants.EQUALS)||c.getFilterCriteria().equals(ParticipantConstants.LIKE)  ){
           stringBuilder.append("'");
           stringBuilder.append(c.getFilterKeyValue());
           stringBuilder.append("' ");
          }else if(c.getFilterCriteria().equals(ParticipantConstants.CONTAINS)){
           stringBuilder.append("'");
           stringBuilder.append(ParticipantConstants.PERCENTAGE);
           stringBuilder.append(c.getFilterKeyValue());
           stringBuilder.append(ParticipantConstants.PERCENTAGE);
           stringBuilder.append("' ");
          }else if(c.getFilterCriteria().equals(ParticipantConstants.STARTS_WITH)){
           stringBuilder.append("'");
           stringBuilder.append(c.getFilterKeyValue());
           stringBuilder.append(ParticipantConstants.PERCENTAGE);
           stringBuilder.append("' ");
         }else if(c.getFilterCriteria().equals(ParticipantConstants.ENDS_WITH)){
           stringBuilder.append("'");
           stringBuilder.append(ParticipantConstants.PERCENTAGE);
           stringBuilder.append(c.getFilterKeyValue());
           stringBuilder.append("' ");
         }
        
        
         
        
          i++;

        }
       stringBuilder.append(" AND ACTIVE = ");stringBuilder.append("'");
       stringBuilder.append(CommonConstants.Y);stringBuilder.append("'");
       participantList = (List<Participant>)getJdbcTemplate().query( "SELECT *"+" FROM "+table+" WHERE "+stringBuilder,new String[]{}, new BeanPropertyRowMapper(Participant.class) );
       logger.info("SELECT *"+" FROM "+table+" WHERE "+stringBuilder);
   
      }
      }

     }catch(Exception e){
        logger.info("Error when Mapped Table List.  " + e.getMessage());
     }
       logger.info(" ==> Get Mapped Tables List STOP <== ");
       return participantList;
    }
	
	/**
	 * This method results the mapped tables for PDF printing
	 * @param error
	 * @param selModules
	 * @return
	 */	
	public Map<String,List<String>> getMappedTables(CommonError error, String selTable){
		logger.info(" ==> Get Mapped Columns START <== ");
		Map<String,List<String>> tableAndFields = new HashMap<String,List<String>>();
		List<String> mappedTableList = null;
		Set<String> mappedTables = new HashSet<String>();
		Set<String> defaultCols = ParticipantConstants.DEFAULT_COLS.keySet();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.MODULES_INFO.get("NAME_OF_THE_TABLE"));
			qry.append(" FROM ");
			qry.append(AdminDBConstants.TBL_MODULES_INFO);
			qry.append(" WHERE ");
			qry.append(AdminDBConstants.MODULES_INFO.get("MODULE_NAME"));
			qry.append(" =? ");
			mappedTableList = getJdbcTemplate().queryForList( qry.toString(), new String[] { selTable },String.class);
			 if(!mappedTableList.isEmpty()){
		            mappedTables.addAll(mappedTableList);
		         }
			 for(String table : mappedTables){
			        if(!table.equals("MOD_PARTICIPANTS_ADDRESS")){
			StringBuffer desc = new StringBuffer("DESC "+table);
			List<TableDesc> tableDesc = null;
			tableDesc = getJdbcTemplate().query(desc.toString(), new Object[]{}, new BeanPropertyRowMapper<TableDesc>(TableDesc.class));
			List<String> cols =  new ArrayList<String>();
			for(TableDesc tabDesc : tableDesc){
				if(!defaultCols.contains(tabDesc.getField()))
						cols.add(tabDesc.getField());
			}
			tableAndFields.put(selTable, cols);	
			        }
			 }
		}catch(Exception e){
			logger.info("Error when Mapped Column List.  " + e.getMessage());
		}
		logger.info(" ==> Get Mapped Columns END <== ");
		return tableAndFields;
	}
    
	/**
	 * This method used to get contact list of particular participant
	 * @param commonError
	 * @param participantId
	 * @return
	 */	
	public List<Participant> getContactList(CommonError commonError, String participantId) {
		logger.info(" ==> getContactList START <== ");
		List<Participant> contactList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.CONTACT_PARTI.keySet() ){
					qry.append( ParticipantDBConstants.CONTACT_PARTI.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_CONTACT_PARTI);qry.append(" WHERE ");
			qry.append(ParticipantDBConstants.CONTACT_PARTI.get("IDENTIFIER"));qry.append(" = ? ");
			contactList = getJdbcTemplate().query( qry.toString(),new String[]{participantId}, new BeanPropertyRowMapper(Participant.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch participant contact list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getContactList END <== ");
		return contactList;
	}
	
	/**
	 * This method used to get filter list of participant
	 * @param commonError
	 * @param participantId
	 * @return
	 */	
	public List<Criteria> getFilterList(CommonError commonError) {
		logger.info(" ==> getFilterList START <== ");
		List<Criteria> criteriaList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			for( String column : ParticipantDBConstants.PARTI_FILTER.keySet() ){
					qry.append( ParticipantDBConstants.PARTI_FILTER.get(column) );qry.append( "," );
			}
			qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(ParticipantDBConstants.TBL_PARTI_FILTER);
			criteriaList = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(Criteria.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch participant filter list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getContactList END <== ");
		return criteriaList;
	}

	public void add(CommonError commonError, User loginUser, List<Criteria> criteriaList) {
		logger.info(" ==> add Criteria START <== ");
		StringBuilder selectedInputBuilder = new StringBuilder();
		StringBuilder filterCriteriaBuilder = new StringBuilder();
		StringBuilder filterKeyBuilder = new StringBuilder();
		StringBuilder filterConditionBuilder = new StringBuilder();
		StringBuffer newQry = new StringBuffer();
		int i=criteriaList.size()-1;
		for(Criteria criteria : criteriaList){
			selectedInputBuilder.append(criteria.getSelectedInput());
			filterCriteriaBuilder.append(criteria.getFilterCriteria());
			filterKeyBuilder.append(criteria.getFilterKeyValue());
			filterConditionBuilder.append(criteria.getFilterCondition());
			
			if(0!=i){
				selectedInputBuilder.append(",");
				filterCriteriaBuilder.append(",");
				filterKeyBuilder.append(",");
				filterConditionBuilder.append(",");
			  }
		   i--;
		}
		
		try{
			String nextRowId = getNextRowId( ParticipantDBConstants.TBL_PARTI_FILTER, ParticipantDBConstants.filterDBConfig.get("rowPrefix") );
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( ParticipantDBConstants.TBL_PARTI_FILTER );newQry.append("( ");
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("SELECTED_INPUT") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("FILTER_CRITERIA") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("FILTER_KEY_VALUE") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("FILTER_CONDITION") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("CREATE_BY") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( ParticipantDBConstants.PARTI_FILTER.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(nextRowId);args.add(selectedInputBuilder.toString());
			args.add(filterCriteriaBuilder.toString());args.add(filterKeyBuilder.toString());
			args.add(filterConditionBuilder.toString());
			args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );
			
		}catch( Exception e ){
			logger.info("Error when Insert Participant Criteria.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> add Criteria END <== ");
		
		
	}
	
	
	
}
