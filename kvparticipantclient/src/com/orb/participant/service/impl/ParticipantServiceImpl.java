package com.orb.participant.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.client.dao.AdminDAO;
import com.orb.admin.client.service.impl.AbstractAdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.participant.bean.Criteria;
import com.orb.participant.bean.Participant;
import com.orb.participant.bean.ParticipantAddress;
import com.orb.participant.constants.ParticipantConstants;
import com.orb.participant.dao.ParticipantDAO;
import com.orb.participant.service.ParticipantService;
import com.textmagic.sms.TextMagicMessageService;
import com.textmagic.sms.exception.ServiceException;



@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )

public class ParticipantServiceImpl extends AbstractAdminService implements ParticipantService{
	private static Log logger = LogFactory.getLog(ParticipantServiceImpl.class);
	
	@Autowired
	ParticipantDAO participantDAO;
	
	@Autowired
	AdminDAO adminDAO;
	
	/**
	 * This method will interact with DAO to get all active participants from DB
	 */
	@Override
	public List<Participant> getActiveParticipantList(CommonError participantError) {
		
		return participantDAO.getParticipantList(participantError,CommonConstants.Y);
	}
	
	/**
	 * This method will interact with DAO to get all active participants from DB
	 */
	@Override
	public List<Participant> getSprachParticipantList(CommonError participantError) {
		
		return participantDAO.getSprachParticipantList(participantError ,CommonConstants.Y);
	}
	
	/**
	 * This method will interact with DAO to get all inactive participants from DB
	 */
	@Override
	public List<Participant> getInactiveParticipantList(CommonError participantError) {
		return participantDAO.getParticipantList(participantError, CommonConstants.N );
	}

	/**
	 * This method will interact with DAO to insert a participant in DB
	 */
	@Override
	public void add(CommonError participantError, User loginUser, Participant participant, ParticipantAddress homeAddress, ParticipantAddress postalAddress ) {
		participantDAO.addParticipant(participantError, loginUser, participant, homeAddress, postalAddress );
	}
	
	
	/**
	 * This method will interact with DAO to update a participant in DB
	 */
	@Override
	public void update(CommonError participantError, User loginUser, Participant participant, ParticipantAddress homeAddress, ParticipantAddress postalAddress ) {
		participantDAO.updateParticipant(participantError, loginUser, participant, homeAddress, postalAddress );
	}

	/**
	 * This method will interact with DAO to delete a participant in DB
	 */
	@Override
	public void delete(CommonError participantError, User loginUser, List<Participant>  selectedDocs , ParticipantAddress homeAddress, ParticipantAddress postalAddress ) {
		List<String> idList=new ArrayList<>();
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Participant participant : selectedDocs ){
			idList.add(participant.getId());	
				
		}
		participantDAO.deleteParticipant(participantError, loginUser, idList, homeAddress, postalAddress);
	}
	
	/**
	 * This method will return the participant data from DAO
	 */
	@Override
	public Participant getParticipant(CommonError participantError,User loginUser, String participantId) {
		return participantDAO.getParticipant(participantError,loginUser, participantId);
	}

	/**
	 * This method will return the participant address data from DAO
	 */
	@Override
	public ParticipantAddress getParticipantAddress(CommonError participantError, String participantId, String type) {
		return participantDAO.getAddress(participantError, participantId, type);
	}
	
			
	/**
	 * This method calls DAO to de-activate participant
	 */
	@Override
	public void lockViewedParticipant(CommonError participantError, User loginUser, Participant participant) {
		List<String> idList=new ArrayList<>();
		idList.add(participant.getId());
		participantDAO.scanParticipant(participant);
		participantDAO.updateActiveStatus(participantError, loginUser,idList, CommonConstants.N );
		
	}
	/**
	 * This method calls DAO to activate participant
	 */
	@Override
	public void unlockViewedParticipant(CommonError participantError, User loginUser, Participant participant) {
		List<String> idList=new ArrayList<>();
		idList.add(participant.getId());
		participantDAO.scanParticipant(participant);
		participantDAO.updateActiveStatus(participantError, loginUser,idList, CommonConstants.Y );
		
	}
	
	/**
	 * This method calls DAO to activate the locked Participant
	 */
	public void unlockParticipant(CommonError participantError, User loginUser, List<Participant> selectedDocs) {
		List<String> idList=new ArrayList<>();
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Participant participant : selectedDocs ){
			idList.add(participant.getId());
			participantDAO.scanParticipant(participant);
		}
		participantDAO.updateActiveStatus(participantError, loginUser,idList, CommonConstants.Y );
	}

	/**
	 * This method calls DAO to de-activate Participant
	 */
	@Override
	public void lockParticipant(CommonError participantError, User loginUser, List<Participant> selectedDocs) {
		List<String> idList=new ArrayList<>();
		if( selectedDocs == null || selectedDocs.isEmpty() ){
		
			return;
		}
		for( Participant participant : selectedDocs ){
			idList.add(participant.getId());
			participantDAO.scanParticipant(participant);
		}
		participantDAO.updateActiveStatus(participantError, loginUser,idList, CommonConstants.N );
	}
	
	/**
	 * This method calls DAO to deletes particular participant
	 */
	@Override
	public void deleteViewedParticipant(CommonError participantError, User loginUser, Participant participant, ParticipantAddress homeAddress, ParticipantAddress postalAddress) {
		List<String> idList=new ArrayList<>();
		idList.add(participant.getId());
		participantDAO.deleteParticipant(participantError, loginUser, idList, homeAddress, postalAddress);
		
	}
	
		
	/**
	 * This method will contact Participant in DB
	 */
	@Override
	public void addContactParticipant(CommonError participantError, User loginUser, Participant participant, String participantId) {
		Map<String, String> adminConfMap = adminDAO.getAdminConfMap(participantError);
		Map<String,String> tokens = new HashMap<>();
		tokens.put("<p>", "");
		tokens.put("</p>", "");
		String patternString = "(" + StringUtils.join(tokens.keySet(), "|") + ")";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(participant.getContent());
		StringBuffer sb = new StringBuffer();
		while(matcher.find()) {
		    matcher.appendReplacement(sb, tokens.get(matcher.group(1)));
		}
		matcher.appendTail(sb);
		participant.setContent(sb.toString());
		participantDAO.addContactParticipant(participantError, loginUser, participant, participantId );
		if( ParticipantConstants.SHOW_EMAIL.equalsIgnoreCase(participant.getTypeOfContact()) ){
			try {
				StringBuilder mailBodyLink = new StringBuilder();
				mailBodyLink.append(getJSFMessage("Welcome_greet"));
				mailBodyLink.append(" "+participant.getSurName()+" "+participant.getFirstName()+",");
				mailBodyLink.append("\n");
				mailBodyLink.append(participant.getContent());
				mailBodyLink.append("\n");
				mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
				
				sendMailToUser( participantError, participant, participant.getSubject(), mailBodyLink.toString() );
	           
			 } catch (Exception e) {
	            logger.info("Error when sending emails.  " + e.getMessage());
	         }
		}else if( ParticipantConstants.SHOW_SMS.equalsIgnoreCase(participant.getTypeOfContact()) ){
			String phoneNo = participant.getContactThrough().isEmpty() ? participant.getMobile() : participant.getContactThrough();
			logger.info(participant.getContactThrough());
			TextMagicMessageService service = 
		             new TextMagicMessageService ("***REMOVED***","***REMOVED***");
			try { 
				StringBuilder msgBodyLink = new StringBuilder();
				msgBodyLink.append(getJSFMessage("Welcome_greet"));
				msgBodyLink.append(" "+participant.getSurName()+" "+participant.getFirstName()+",");
				msgBodyLink.append("\n");
				msgBodyLink.append(participant.getMessage());
				msgBodyLink.append("\n");
				msgBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
				service.send( msgBodyLink.toString(), phoneNo );
				
			} 
			catch(ServiceException e) { 
				logger.info("Text Message Not Sent"+e.getMessage()); 
			} 
		}
	}
	
	/**
	 * This method will send mail to user
	 */
	public void sendMailToUser(CommonError commonError, Participant participant, String mailSubject, String mailBody){
		logger.info("Sending mail to USER regarding "+mailSubject);
		try {
			boolean sessionDebug = false;
	        Properties props = System.getProperties();
	        String companyName = (String)getSessionObj(CommonConstants.COMPANY_NAME);
	        props.put("mail.smtp.host", CommonConstants.SEND_MAIL_HOST);
	        props.put("mail.transport.protocol", CommonConstants.MAIL_PROTOCOL);
	       
	        Session session = Session.getDefaultInstance(props, null);
	       	session.setDebug(sessionDebug);
	        
	       	javax.mail.Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(CommonConstants.FROM_MAIL_ID));
	        InternetAddress[] address = { new InternetAddress(participant.getEmail()) };
	        msg.setRecipients(javax.mail.Message.RecipientType.TO, address);
	        msg.setSentDate(new Date());
	      
	        msg.setSubject(companyName.trim()+" - "+mailSubject);
	        msg.setContent(mailBody,"text/html");
	        Transport.send(msg);
	        
		} catch (MessagingException e) {
	        logger.info("Error when sending emails.  " + e.getMessage());
	        commonError.addError(ForgotPasswordConstant.EMAIL_ERROR);
		
		}
		logger.info("Mail Sent to USER regarding "+mailSubject);
	}
	
	/* WebService implemenation calls */
	
	/**
	 * This method will return the mapped tables and fields from DAO
	 */
	@Override
	public Map<String,List<String>> getMappedTablesAndFields(CommonError commonError, String moduleName) {
		return participantDAO.getMappedTables(commonError, moduleName);
	
	}
	
	/**
	 * This method will return the mapped tables and values data from DAO
	 */
	@Override
	public List<Participant> getMappedTablesAndValues(CommonError commonError, String moduleName,List<Criteria> criteriaList) {
		
		return participantDAO.getMappedTablesAndValues(commonError, moduleName,criteriaList);
	}

    /**
	 * This method used to get contact list of particular participant
	 */
	@Override
	public List<Participant> getContactList(CommonError commonError, String participantId) {
		
		return participantDAO.getContactList(commonError, participantId);
	}
	
	@Override
	public List<Criteria> getFilterList(CommonError commonError) {
		return participantDAO.getFilterList(commonError);
	}

	@Override
	public void add(CommonError commonError, User loginUser, List<Criteria> criteriaList) {
	    participantDAO.add(commonError, loginUser,criteriaList);
		
	}
	
	public String getAuthenticationDetails(CommonError commonError,String user,String pass){
		String authValue = null;
		/*List<Participant> participantList = null;
		participantList = participantDAO.getParticipantList(commonError,CommonConstants.Y);
		if(participantList!=null){
			for(Participant participant:participantList){
				//if(user.equals(participant.getPortalUser()) && pass.equals(participant.getPortalPass())){
				if(user.equals(participant.getAppUser()) && pass.equals(participant.getAppPass())){
					authValue = CommonConstants.TRUE;
				}
			}
		}*/
		return authValue;
	}

}
