package com.orb.participant.service;

import java.util.List;
import java.util.Map;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.participant.bean.Criteria;
import com.orb.participant.bean.Participant;
import com.orb.participant.bean.ParticipantAddress;

public interface ParticipantService {
	public List<Participant> getActiveParticipantList( CommonError participantError);
	public List<Participant> getSprachParticipantList( CommonError participantError);
	
	public List<Participant> getInactiveParticipantList( CommonError participantError);
	public Participant getParticipant( CommonError participantError,User loginUser, String participantId );
	public ParticipantAddress getParticipantAddress( CommonError participantError, String participantId, String type );
	public void add( CommonError participantError, User loginUser, Participant participant, ParticipantAddress homeAddress, ParticipantAddress postalAddress );
	public void update( CommonError participantError, User loginUser, Participant participant, ParticipantAddress homeAddress, ParticipantAddress postalAddress );
	public void unlockViewedParticipant(CommonError participantError, User loginUser, Participant participant);
	public void lockViewedParticipant(CommonError participantError, User loginUser, Participant participant);
	public void lockParticipant(CommonError participantError, User loginUser, List<Participant> participantList);
	public void unlockParticipant(CommonError participantError, User loginUser, List<Participant> participantList);
	public void delete(CommonError participantError, User loginUser, List<Participant> participantList,ParticipantAddress homeAddress, ParticipantAddress postalAddress);
	public void deleteViewedParticipant(CommonError participantError, User loginUser, Participant participant,ParticipantAddress homeAddress, ParticipantAddress postalAddress);
	public void addContactParticipant( CommonError participantError, User loginUser, Participant participant, String participantId );
	public Map<String,List<String>> getMappedTablesAndFields( CommonError commonError, String moduleName );
	public List<Participant> getMappedTablesAndValues( CommonError commonError,String moduleName, List<Criteria> selectedCriteriaList);
    public List<Participant> getContactList(CommonError commonError, String participantId);
    public List<Criteria> getFilterList(CommonError commonError);
	public void add(CommonError commonError, User loginUser, List<Criteria> criteriaList);
	
}
