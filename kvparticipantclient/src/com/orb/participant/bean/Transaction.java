package com.orb.participant.bean;

import java.util.Date;

public class Transaction {
	private String inge_Tran_id;
	private String service_name;
	private String inge_identifier;
	private String file_reference_number;
	private String bamf_Tran_id;
	private Date timestamp;
	
	
	public String getInge_Tran_id() {
		return inge_Tran_id;
	}
	public void setInge_Tran_id(String inge_Tran_id) {
		this.inge_Tran_id = inge_Tran_id;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	public String getInge_identifier() {
		return inge_identifier;
	}
	public void setInge_identifier(String inge_identifier) {
		this.inge_identifier = inge_identifier;
	}
	public String getFile_reference_number() {
		return file_reference_number;
	}
	public void setFile_reference_number(String file_reference_number) {
		this.file_reference_number = file_reference_number;
	}
	public String getBamf_Tran_id() {
		return bamf_Tran_id;
	}
	public void setBamf_Tran_id(String bamf_Tran_id) {
		this.bamf_Tran_id = bamf_Tran_id;
	}
	

}
