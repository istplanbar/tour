package com.orb.participant.bean;


import java.util.Date;


/**
 * 
 * @author IST planbar / PaPe
 *
 */
/**
 * @author Pal
 *
 */
public class Participant {
	
	private String id;
	private String surName;
	private String firstName;
	private Date dateOfBirth; 
	private String gender;
	private String placeOfBirth;
	private String nationality;
	private String identifier;
	private String telephone;
	private String mobile;
	private String email;
	private String carrierIdentifier;
	private Date registrationDate;
	private String education;
	private String noOfSchoolYears;
	private String literacy;
	private String testRequired;
	private String testIdentifier;
	private Date testDate;
	private String courseRecommended;
	private String sectionRecommended;
	private String markForDeletion;
	private String aktenzeichen;
	private String registrationType;
	private String sprachTestRequired;
	private String sprachTestCarrier;
	private String auftragsNummer;
	private String bogenNummer;
	private Date sprachTestDate;
	private String ausweisNummer;
	private String ingeStatus;
	private String createBy;
	private Date createTime; 
	private String updateBy;
	private Date updateTime;
	private boolean checked;
	private String active;
	private String createByName;
	private String updateByName;
	private String lockedBy;
	private Date lockedTime;
	private String modifiedBy;
	private Date modifiedTime;
	private String unlockedBy;
	private Date unlockedTime;
	private String approvedBy;
	private Date approvedTime;
	private String rejectedBy;
	private Date rejectedTime;
	private String executedBy;
	private Date executedTime;
	private String travelCostsPaymentAmount;
	private String location;
	private String typeOfContact;
	private String contactThrough;
	private String subject;
	private String content;
	private String message;
	private String tags;
	private String name;
	

	
	public String getExecutedBy() {
		return executedBy;
	}
	public void setExecutedBy(String executedBy) {
		this.executedBy = executedBy;
	}
	
	public Date getExecutedTime() {
		return executedTime;
	}
	public void setExecutedTime(Date executedTime) {
		this.executedTime = executedTime;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public Date getApprovedTime() {
		return approvedTime;
	}
	public void setApprovedTime(Date approvedTime) {
		this.approvedTime = approvedTime;
	}
	public String getRejectedBy() {
		return rejectedBy;
	}
	public void setRejectedBy(String rejectedBy) {
		this.rejectedBy = rejectedBy;
	}
	public Date getRejectedTime() {
		return rejectedTime;
	}
	public void setRejectedTime(Date rejectedTime) {
		this.rejectedTime = rejectedTime;
	}
	public String getTravelCostsPaymentAmount() {
		return travelCostsPaymentAmount;
	}
	public void setTravelCostsPaymentAmount(String travelCostsPaymentAmount) {
		this.travelCostsPaymentAmount = travelCostsPaymentAmount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCarrierIdentifier() {
		return carrierIdentifier;
	}
	public void setCarrierIdentifier(String carrierIdentifier) {
		this.carrierIdentifier = carrierIdentifier;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getNoOfSchoolYears() {
		return noOfSchoolYears;
	}
	public void setNoOfSchoolYears(String noOfSchoolYears) {
		this.noOfSchoolYears = noOfSchoolYears;
	}
	public String getLiteracy() {
		return literacy;
	}
	public void setLiteracy(String literacy) {
		this.literacy = literacy;
	}
	public String getTestRequired() {
		if( !"false".equals(testRequired)) testRequired = "true";
		return testRequired;
	}
	public void setTestRequired(String testRequired) {
		this.testRequired = testRequired;
	}
	public String getTestIdentifier() {
		return testIdentifier;
	}
	public void setTestIdentifier(String testIdentifier) {
		this.testIdentifier = testIdentifier;
	}
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	
	public String getMarkForDeletion() {
		return markForDeletion;
	}
	public void setMarkForDeletion(String markForDeletion) {
		this.markForDeletion = markForDeletion;
	}
	public String getAktenzeichen() {
		return aktenzeichen;
	}
	public void setAktenzeichen(String aktenzeichen) {
		this.aktenzeichen = aktenzeichen;
	}
	public String getRegistrationType() {
		return registrationType;
	}
	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}
	public String getSprachTestRequired() {
		if( !"true".equals(sprachTestRequired) ) sprachTestRequired = "false";
		return sprachTestRequired;
	}
	public void setSprachTestRequired(String sprachTestRequired) {
		this.sprachTestRequired = sprachTestRequired;
	}
	public String getSprachTestCarrier() {
		return sprachTestCarrier;
	}
	public void setSprachTestCarrier(String sprachTestCarrier) {
		this.sprachTestCarrier = sprachTestCarrier;
	}
	public String getAuftragsNummer() {
		return auftragsNummer;
	}
	public void setAuftragsNummer(String auftragsNummer) {
		this.auftragsNummer = auftragsNummer;
	}
	public String getBogenNummer() {
		return bogenNummer;
	}
	public void setBogenNummer(String bogenNummer) {
		this.bogenNummer = bogenNummer;
	}
	public Date getSprachTestDate() {
		return sprachTestDate;
	}
	public void setSprachTestDate(Date sprachTestDate) {
		this.sprachTestDate = sprachTestDate;
	}
	public String getAusweisNummer() {
		return ausweisNummer;
	}
	public void setAusweisNummer(String ausweisNummer) {
		this.ausweisNummer = ausweisNummer;
	}
	public String getIngeStatus() {
		return ingeStatus;
	}
	public void setIngeStatus(String ingeStatus) {
		this.ingeStatus = ingeStatus;
	}
	
	
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getCreateByName() {
		return createByName;
	}
	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}
	public String getUpdateByName() {
		return updateByName;
	}
	public void setUpdateByName(String updateByName) {
		this.updateByName = updateByName;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	public String getCourseRecommended() {
		return courseRecommended;
	}
	public void setCourseRecommended(String courseRecommended) {
		this.courseRecommended = courseRecommended;
	}
	public String getSectionRecommended() {
		return sectionRecommended;
	}
	public void setSectionRecommended(String sectionRecommended) {
		this.sectionRecommended = sectionRecommended;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTypeOfContact() {
		return typeOfContact;
	}
	public void setTypeOfContact(String typeOfContact) {
		this.typeOfContact = typeOfContact;
	}
	public String getContactThrough() {
		return contactThrough;
	}
	public void setContactThrough(String contactThrough) {
		this.contactThrough = contactThrough;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
	
	
}
