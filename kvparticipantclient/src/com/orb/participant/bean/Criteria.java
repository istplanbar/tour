package com.orb.participant.bean;

import java.util.Date;

public class Criteria {
	private String selectedInput;
	private String filterCriteria;
	private String filterKeyValue;
	private String filterCondition;
	private String id;
	private String identifier;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private boolean checked;
	private String active;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getSelectedInput() {
		return selectedInput;
	}
	public void setSelectedInput(String selectedInput) {
		this.selectedInput = selectedInput;
	}
	public String getFilterCriteria() {
		return filterCriteria;
	}
	public void setFilterCriteria(String filterCriteria) {
		this.filterCriteria = filterCriteria;
	}
	public String getFilterKeyValue() {
		return filterKeyValue;
	}
	public void setFilterKeyValue(String filterKeyValue) {
		this.filterKeyValue = filterKeyValue;
	}
	public String getFilterCondition() {
		return filterCondition;
	}
	public void setFilterCondition(String filterCondition) {
		this.filterCondition = filterCondition;
	}

}
