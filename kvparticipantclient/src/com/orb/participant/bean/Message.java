package com.orb.participant.bean;

import java.util.Date;

public class Message {
	private String service_name;
	private String inge_identifier;
	private String bamf_Tran_id;
	private String seqno;
	private String message_id;
	private String message;
	private String description;
	private Date Timestamp;
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
	public String getInge_identifier() {
		return inge_identifier;
	}
	public void setInge_identifier(String inge_identifier) {
		this.inge_identifier = inge_identifier;
	}
	public String getBamf_Tran_id() {
		return bamf_Tran_id;
	}
	public void setBamf_Tran_id(String bamf_Tran_id) {
		this.bamf_Tran_id = bamf_Tran_id;
	}
	public String getSeqno() {
		return seqno;
	}
	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}
	public String getMessage_id() {
		return message_id;
	}
	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getTimestamp() {
		return Timestamp;
	}
	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

}
