package com.orb.employee.constants;

public class EmployeeConstants {

	//Employee
	public final static String SELECTED_EMPLOYEE_ID = "SELECTED_EMPLOYEE_ID";
	public final static String EMPLOYEE_LIST_PAGE = "/common/process/employee/loadEmployee.html";
	public final static String EMPLOYEE_EDIT_PAGE = "/common/process/employee/manipulateEmployee.html";
	public final static String EMPLOYEE_LIST_HTML = "employee/list.xhtml";
	public final static String EMPLOYEE_TOGGLER_LIST = "employee_toggler_list" ;
	public final static String EMPLOYEE_DELETE_SUCCESS = "employee_delete_success";
	public final static String EMPLOYEE_LOCKED_SUCCESS = "employee_locked_success";
	public final static String EMPLOYEE_UNLOCKED_SUCCESS = "employee_unlocked_success";
	public final static String EMPLOYEE_VIEW_PAGE = "/common/process/employee/viewEmployee.html";
	public final static String EMPLOYEE_EDIT_HTML = "employee/addUpdate.xhtml";
	public final static String EMPLOYEE_VIEW_HTML = "employee/view.xhtml";
	public final static String EMPLOYEE_ADDED_SUCCESS = "employee_added_success";
	public final static String EMPLOYEE_UPDATED_SUCCESS = "employee_update_success";
	
	//Transport
	public final static String SELECTED_TRANSPORT_ID = "SELECTED_TRANSPORT_ID";
	public final static String TRANSPORT_LIST_PAGE = "/common/process/employee/loadTransport.html";
	public final static String TRANSPORT_EDIT_PAGE = "/common/process/employee/manipulateTransport.html";
	public final static String TRANSPORT_LIST_HTML = "employee/transport/list.xhtml";
	public final static String TRANSPORT_TOGGLER_LIST = "transport_toggler_list" ;
	public final static String TRANSPORT_DELETE_SUCCESS = "transport_delete_success";
	public final static String TRANSPORT_LOCKED_SUCCESS = "transport_locked_success";
	public final static String TRANSPORT_UNLOCKED_SUCCESS = "transport_unlocked_success";
	public final static String TRANSPORT_VIEW_PAGE = "/common/process/employee/viewTransport.html";
	public final static String TRANSPORT_EDIT_HTML = "employee/transport/addUpdate.xhtml";
	public final static String TRANSPORT_VIEW_HTML = "employee/transport/view.xhtml";
	public final static String TRANSPORT_ADDED_SUCCESS = "transport_added_success";
	public final static String TRANSPORT_UPDATED_SUCCESS = "transport_update_success";

	//Vacation
	public final static String SELECTED_VACATION_ID = "SELECTED_VACATION_ID";
	public final static String VACATION_LIST_PAGE = "/common/process/employee/loadVacation.html";
	public final static String VACATION_EDIT_PAGE = "/common/process/employee/manipulateVacation.html";
	public final static String VACATION_LIST_HTML = "employee/vacation/list.xhtml";
	public final static String VACATION_TOGGLER_LIST = "vacation_toggler_list" ;
	public final static String VACATION_DELETE_SUCCESS = "vacation_delete_success";
	public final static String VACATION_LOCKED_SUCCESS = "vacation_locked_success";
	public final static String VACATION_UNLOCKED_SUCCESS = "vacation_unlocked_success";
	public final static String VACATION_VIEW_PAGE = "/common/process/employee/viewVacation.html";
	public final static String VACATION_EDIT_HTML = "employee/vacation/addUpdate.xhtml";
	public final static String VACATION_VIEW_HTML = "employee/vacation/view.xhtml";
	public final static String VACATION_ADDED_SUCCESS = "vacation_added_success";
	public final static String VACATION_UPDATED_SUCCESS = "vacation_update_success";	
	
	
	
}
