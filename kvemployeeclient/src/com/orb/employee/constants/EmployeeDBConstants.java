package com.orb.employee.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class EmployeeDBConstants {

	
	//Employee
	
	public static final String TBL_EMPLOYEE,TBL_EMPLOYEE_HISTORY;
	public static final Map<String,String> EMPLOYEE,EMPLOYEE_HISTORY;
	public static final Map<String,String> employeeDBConfig;
	
	//Transport
	
	public static final String TBL_TRANSPORT,TBL_TRANSPORT_HISTORY;
	public static final Map<String,String> TRANSPORT,TRANSPORT_HISTORY;
	public static final Map<String,String> transportDBConfig;
	
	//Employee
	
	public static final String TBL_VACATION,TBL_VACATION_HISTORY;
	public static final Map<String,String> VACATION,VACATION_HISTORY;
	public static final Map<String,String> vacationDBConfig;
	
	static{
		
	   //Employee
		
		TBL_EMPLOYEE=CommonUtils.getPropertyValue("EMPLOYEE_TABLE_NAMES", "MOD_EMPLOYEE");
		TBL_EMPLOYEE_HISTORY=CommonUtils.getPropertyValue("EMPLOYEE_TABLE_NAMES", "MOD_EMPLOYEE_HISTORY");
					
		EMPLOYEE=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE");
		EMPLOYEE_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_HISTORY");
					
		employeeDBConfig=(Map<String,String>)CommonUtils.getBean("employeeDBConfig");
		
		//Transport
		
		TBL_TRANSPORT=CommonUtils.getPropertyValue("TRANSPORT_TABLE_NAMES", "MOD_EMPLOYEE_TRANSPORT");
		TBL_TRANSPORT_HISTORY=CommonUtils.getPropertyValue("TRANSPORT_TABLE_NAMES", "MOD_EMPLOYEE_TRANSPORT_HISTORY");
									
		TRANSPORT=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_TRANSPORT");
		TRANSPORT_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_TRANSPORT_HISTORY");
									
		transportDBConfig=(Map<String,String>)CommonUtils.getBean("transportDBConfig");

	    //Employee
		
		TBL_VACATION=CommonUtils.getPropertyValue("VACATION_TABLE_NAMES", "MOD_EMPLOYEE_VACATION");
	    TBL_VACATION_HISTORY=CommonUtils.getPropertyValue("VACATION_TABLE_NAMES", "MOD_EMPLOYEE_VACATION_HISTORY");
						
		VACATION=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_VACATION");
		VACATION_HISTORY=(Map<String,String>)CommonUtils.getBean("MOD_EMPLOYEE_VACATION_HISTORY");
						
		vacationDBConfig=(Map<String,String>)CommonUtils.getBean("vacationDBConfig");
	
		
   }
}
