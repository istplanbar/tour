package com.orb.employee.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Transport;
import com.orb.employee.bean.Vacation;
import com.orb.employee.dao.EmployeeDAO;
import com.orb.employee.dao.TransportDAO;
import com.orb.employee.dao.VacationDAO;
import com.orb.employee.service.EmployeeService;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class EmployeeServiceImpl implements EmployeeService {
	private static Log logger = LogFactory.getLog(EmployeeServiceImpl.class);

	@Autowired
	EmployeeDAO  employeeDAO;
	
	@Autowired
	TransportDAO transportDAO;
	
	@Autowired
	VacationDAO  vacationDAO;
	
	//Employee
	/**
	 * This method calls DAO to get all Inactive employee from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Employee> getInactiveEmployeeList(CommonError commonError) {
		return  employeeDAO.getEmployeeList(commonError,CommonConstants.N);

	}

	/**
	 * This method calls DAO to get all active employee list from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Employee> getActiveEmployeeList(CommonError commonError) {
		return employeeDAO.getEmployeeList(commonError,CommonConstants.Y);
	}

	/**
	 * This method will interact with DAO to get  employee in DB
	 * @param commonError
	 * @param employeeId
	 * @return
	 */
	@Override
	public Employee getEmployee(CommonError commonError, String employeeId) {
		return employeeDAO.getEmployee(commonError, employeeId);
	}

	/**
	 * This method calls DAO to de-activate employee
	 * @param commonError
	 * @param loginUser
	 * @param selectedEmployeeList
	 */
	@Override
	public void lockEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList) {
	List<String> idList=new ArrayList<>();
	if( selectedEmployeeList == null || selectedEmployeeList.isEmpty() ){
	return;
	}
	for( Employee employee : selectedEmployeeList ){
		idList.add(employee.getId());
	}
	employeeDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );						
	}

	/**
	* This method calls DAO to activate the locked employee
	* @param commonError
    * @param loginUser
	* @param selectedEmployeeList
	*/
	@Override
	public void unlockEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList) {
	List<String> idList=new ArrayList<>();
	if( selectedEmployeeList == null || selectedEmployeeList.isEmpty() ){
		logger.info("No record is available to activate");
		return;
	}
	for( Employee employee : selectedEmployeeList ){
		idList.add(employee.getId());
	}
	employeeDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);		
	}
	
	/**
	 * This method will interact with DAO to delete a employee in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedEmployeeList
	 */
	@Override
	public void deleteEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList) {
		List<String> idList=new ArrayList<>();
		if( selectedEmployeeList == null || selectedEmployeeList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Employee employee : selectedEmployeeList ){
			idList.add(employee.getId());
		}
		employeeDAO.deleteEmployee(commonError, loginUser, idList);			
	}

    /**
	 * This method calls DAO to deletes particular employee
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	@Override
	public void deleteViewedEmployee(CommonError commonError, User loginUser, Employee employee) {
		List<String> idList=new ArrayList<>();
		idList.add(employee.getId());
		employeeDAO.deleteEmployee(commonError, loginUser, idList);			
	}

	/**
	 * This method calls DAO to de-activate employee
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	@Override
	public void lockViewedEmployee(CommonError commonError, User loginUser, Employee employee) {
		List<String> idList=new ArrayList<>();
		idList.add(employee.getId());
		employeeDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );				
	}

	/**
	 * This method calls DAO to Activate employee
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	@Override
	public void unlockViewedEmployee(CommonError commonError, User loginUser, Employee employee) {
		List<String> idList=new ArrayList<>();
		idList.add(employee.getId());
		employeeDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );			
	}

	/**
	 * This method will interact with DAO to insert a employee in DB
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	@Override
	public void addEmployee(CommonError commonError, User loginUser, Employee employee) {
		employeeDAO.addEmployee(commonError, loginUser, employee);		
	}

	/**
	 * This method will interact with DAO to update a employee in DB
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	@Override
	public void updateEmployee(CommonError commonError, User loginUser, Employee employee) {
		employeeDAO.updateEmployee(commonError, loginUser, employee );			
	}
	
	
	//Transport
		/**
		 * This method calls DAO to get all Inactive transport from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Transport> getInactiveTransportList(CommonError commonError) {
			return  transportDAO.getTransportList(commonError,CommonConstants.N);

		}

		/**
		 * This method calls DAO to get all Inactive transport list from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Transport> getActiveTransportList(CommonError commonError) {
			return transportDAO.getTransportList(commonError,CommonConstants.Y);
		}

		/**
		 * This method will interact with DAO to get  transport in DB
		 * @param commonError
		 * @param transportId
		 * @return
		 */
		@Override
		public Transport getTransport(CommonError commonError, String transportId) {
			return transportDAO.getTransport(commonError, transportId);
		}

		/**
		 * This method calls DAO to de-activate transport
		 * @param commonError
		 * @param loginUser
		 * @param selectedTransportList
		 */
		@Override
		public void lockTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList) {
		List<String> idList=new ArrayList<>();
		if( selectedTransportList == null || selectedTransportList.isEmpty() ){
		return;
		}
		for( Transport transport : selectedTransportList ){
			idList.add(transport.getId());
		}
		transportDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );						
		}

			/**
			 * This method calls DAO to activate the locked transport
			 * @param commonError
			 * @param loginUser
			 * @param selectedTransportList
			 */
		@Override
		public void unlockTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList) {
		List<String> idList=new ArrayList<>();
		if( selectedTransportList == null || selectedTransportList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Transport transport : selectedTransportList ){
			idList.add(transport.getId());
		}
		transportDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);		
		}
		
		/**
		 * This method will interact with DAO to delete a transport in DB
		 * @param commonError
		 * @param loginUser
		 * @param selectedTransportList
		 */
		@Override
		public void deleteTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList) {
			List<String> idList=new ArrayList<>();
			if( selectedTransportList == null || selectedTransportList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Transport transport : selectedTransportList ){
				idList.add(transport.getId());
			}
			transportDAO.deleteTransport(commonError, loginUser, idList);			
		}

	    /**
		 * This method calls DAO to deletes particular transport
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		@Override
		public void deleteViewedTransport(CommonError commonError, User loginUser, Transport transport) {
			List<String> idList=new ArrayList<>();
			idList.add(transport.getId());
			transportDAO.deleteTransport(commonError, loginUser, idList);			
		}

		/**
		 * This method calls DAO to de-activate transport
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		@Override
		public void lockViewedTransport(CommonError commonError, User loginUser, Transport transport) {
			List<String> idList=new ArrayList<>();
			idList.add(transport.getId());
			transportDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );				
		}

		/**
		 * This method calls DAO to Activate transport
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		@Override
		public void unlockViewedTransport(CommonError commonError, User loginUser, Transport transport) {
			List<String> idList=new ArrayList<>();
			idList.add(transport.getId());
			transportDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );			
		}

		/**
		 * This method will interact with DAO to insert a transport in DB
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		@Override
		public void addTransport(CommonError commonError, User loginUser, Transport transport) {
			transportDAO.addTransport(commonError, loginUser, transport);		
		}

		/**
		 * This method will interact with DAO to update a transport in DB
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		@Override
		public void updateTransport(CommonError commonError, User loginUser, Transport transport) {
			transportDAO.updateTransport(commonError, loginUser, transport );			
		}

	//Vacation
		/**
		 * This method calls DAO to get all Inactive vacation from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Vacation> getInactiveVacationList(CommonError commonError) {
			return  vacationDAO.getVacationList(commonError,CommonConstants.N);
		}

		/**
		 * This method calls DAO to get all active vacation from DB
		 * @param commonError
		 * @return
		 */
		@Override
		public List<Vacation> getActiveVacationList(CommonError commonError) {
			return vacationDAO.getVacationList(commonError,CommonConstants.Y);
		}
		
		/**
		 * This method will interact with DAO to get  vacation in DB
		 * @param commonError
		 * @param vacationId
		 * @return
		 */
		@Override
		public Vacation getVacation(CommonError commonError, String vacationId) {
			return vacationDAO.getVacation(commonError, vacationId);
		}
		
		/**
		 * This method calls DAO to de-activate vacation
		 * @param commonError
		 * @param loginUser
		 * @param selectedVacationList
		 */
		@Override
		public void lockVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList) {
			List<String> idList=new ArrayList<>();
			if( selectedVacationList == null || selectedVacationList.isEmpty() ){
			return;
			}
			for( Vacation vacation : selectedVacationList ){
				idList.add(vacation.getId());
			}
			vacationDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );	
		}

		/**
		* This method calls DAO to activate the locked vacation
		* @param commonError
	    * @param loginUser
		* @param selectedVacationList
		*/
		@Override
		public void unlockVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList) {
			List<String> idList=new ArrayList<>();
			if( selectedVacationList == null || selectedVacationList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Vacation vacation : selectedVacationList ){
				idList.add(vacation.getId());
			}
			vacationDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);		
		}

		/**
		 * This method will interact with DAO to delete a vacation in DB
		 * @param commonError
		 * @param loginUser
		 * @param selectedVacationList
		 */
		@Override
		public void deleteVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList) {
			List<String> idList=new ArrayList<>();
			if( selectedVacationList == null || selectedVacationList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Vacation vacation : selectedVacationList ){
				idList.add(vacation.getId());
			}
			vacationDAO.deleteVacation(commonError, loginUser, idList);	
		}

		 /**
		 * This method calls DAO to deletes particular vacation
		 * @param commonError
		 * @param loginUser
		 * @param vacation
		 */
		@Override
		public void deleteViewedVacation(CommonError commonError, User loginUser, Vacation vacation) {
			List<String> idList=new ArrayList<>();
			idList.add(vacation.getId());
			vacationDAO.deleteVacation(commonError, loginUser, idList);		
		}

		/**
		 * This method calls DAO to De-activate vacation
		 * @param commonError
		 * @param loginUser
		 * @param vacation
		 */
		@Override
		public void lockViewedVacation(CommonError commonError, User loginUser, Vacation vacation) {
			List<String> idList=new ArrayList<>();
			idList.add(vacation.getId());
			vacationDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );	
		}

		/**
		 * This method calls DAO to Activate vacation
		 * @param commonError
		 * @param loginUser
		 * @param vacation
		 */
		@Override
		public void unlockViewedVacation(CommonError commonError, User loginUser, Vacation vacation) {
			List<String> idList=new ArrayList<>();
			idList.add(vacation.getId());
			vacationDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );	
		}

		/**
		 * This method will interact with DAO to insert a vacation in DB
		 * @param commonError
		 * @param loginUser
		 * @param vacation
		 */
		@Override
		public void addVacation(CommonError commonError, User loginUser, Vacation vacation) {
			vacationDAO.addVacation(commonError, loginUser, vacation);	
		}

		/**
		 * This method will interact with DAO to update a vacation in DB
		 * @param commonError
		 * @param loginUser
		 * @param vacation
		 */
		@Override
		public void updateVacation(CommonError commonError, User loginUser, Vacation vacation) {
			vacationDAO.updateVacation(commonError, loginUser, vacation );		
		}
}
