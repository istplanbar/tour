package com.orb.employee.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Employee;
import com.orb.employee.bean.Transport;
import com.orb.employee.bean.Vacation;

public interface EmployeeService {

	//Employee
	public List<Employee> getInactiveEmployeeList(CommonError commonError);
	public List<Employee> getActiveEmployeeList(CommonError commonError);
	public Employee getEmployee(CommonError commonError, String employeeId);
	public void lockEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList);
	public void unlockEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList);
	public void deleteEmployee(CommonError commonError, User loginUser, List<Employee> selectedEmployeeList);
	public void deleteViewedEmployee(CommonError commonError, User loginUser, Employee employee);
	public void lockViewedEmployee(CommonError commonError, User loginUser, Employee employee);
	public void unlockViewedEmployee(CommonError commonError, User loginUser, Employee employee);
	public void addEmployee(CommonError commonError, User loginUser, Employee employee);
	public void updateEmployee(CommonError commonError, User loginUser, Employee employee);
	
	//Transport
	public List<Transport> getInactiveTransportList(CommonError commonError);
	public List<Transport> getActiveTransportList(CommonError commonError);
	public Transport getTransport(CommonError commonError, String transportId);
	public void lockTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList);
	public void unlockTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList);
	public void deleteTransport(CommonError commonError, User loginUser, List<Transport> selectedTransportList);
	public void deleteViewedTransport(CommonError commonError, User loginUser, Transport transport);
	public void lockViewedTransport(CommonError commonError, User loginUser, Transport transport);
	public void unlockViewedTransport(CommonError commonError, User loginUser, Transport transport);
	public void addTransport(CommonError commonError, User loginUser, Transport transport);
	public void updateTransport(CommonError commonError, User loginUser, Transport transport);
	
	
	//Vacation
	public List<Vacation> getInactiveVacationList(CommonError commonError);
	public List<Vacation> getActiveVacationList(CommonError commonError);
	public Vacation getVacation(CommonError commonError, String vacationId);
	public void lockVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList);
	public void unlockVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList);
	public void deleteVacation(CommonError commonError, User loginUser, List<Vacation> selectedVacationList);
	public void deleteViewedVacation(CommonError commonError, User loginUser, Vacation vacation);
	public void lockViewedVacation(CommonError commonError, User loginUser, Vacation vacation);
	public void unlockViewedVacation(CommonError commonError, User loginUser, Vacation vacation);
	public void addVacation(CommonError commonError, User loginUser, Vacation vacation);
	public void updateVacation(CommonError commonError, User loginUser, Vacation vacation);
}
