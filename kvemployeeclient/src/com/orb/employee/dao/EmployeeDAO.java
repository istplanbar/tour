package com.orb.employee.dao;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Employee;
import com.orb.employee.constants.EmployeeDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class EmployeeDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(EmployeeDAO.class);
	
	
	/**
     * This method will return list of employee from DB
     * @param commonError
     * @param activeFlag
     * @return
     */	
	public List<Employee> getEmployeeList(CommonError commonError, String s) {
		logger.info(" ==> Get Employee List START <== ");
		List<Employee> employeeList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : EmployeeDBConstants.EMPLOYEE.keySet() ){
				   qry.append( EmployeeDBConstants.EMPLOYEE.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(EmployeeDBConstants.TBL_EMPLOYEE);qry.append(" WHERE ");
			qry.append(EmployeeDBConstants.EMPLOYEE.get("ACTIVE"));qry.append("=? ");
			employeeList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(Employee.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to  get employee list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==>  Get Employee List END <== ");
		return employeeList;
	}

	/**
	 * This method used to get a particular employee from DB
	 * @param employeeId
	 * @param commonError
	 * @return employee
	 */
	public Employee getEmployee(CommonError commonError, String employeeId) {
		logger.info(" ==> Get a Employee START <== ");
		Employee employee = null;
		Employee lockedHist = null;
		Employee unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : EmployeeDBConstants.EMPLOYEE.keySet() ){
				newQry.append( EmployeeDBConstants.EMPLOYEE.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(EmployeeDBConstants.TBL_EMPLOYEE);newQry.append(" WHERE ");
			newQry.append( EmployeeDBConstants.EMPLOYEE.get("IDENTIFIER") );newQry.append( "=? ");
			employee = (Employee)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{employeeId}, new BeanPropertyRowMapper(Employee.class) );
			
		lockedHist = getUserAction(commonError, employee.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			employee.setLockedBy( lockedHist.getUpdateBy());
			employee.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, employee.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			employee.setUnlockedBy( unlockedHist.getUpdateBy());
			employee.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch employee. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Employee END <== ");
		return employee;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */	
	private Employee getUserAction(CommonError commonError, String id, String action) {
		Employee historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : EmployeeDBConstants.EMPLOYEE_HISTORY.keySet() ){
			newQryHist.append( EmployeeDBConstants.EMPLOYEE_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_EMPLOYEE_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.EMPLOYEE.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(EmployeeDBConstants.EMPLOYEE_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(EmployeeDBConstants.EMPLOYEE_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Employee)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Employee.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}

	/**
	 * This method will activate or deactivate the Employee
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> UpdateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertEmployeeHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertEmployeeHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( EmployeeDBConstants.TBL_EMPLOYEE );qry.append(" SET ");
			qry.append( EmployeeDBConstants.EMPLOYEE.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( EmployeeDBConstants.EMPLOYEE.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a employee.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> UpdateActiveStatus END <== ");				
	}

	/**
     * This method inserts updated Role into Role history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	private void insertEmployeeHistory(CommonError commonError, User loginUser, List<String> idList,
			String action) {
		logger.info(" ==> Insert Employee History START <== ");
		logger.info("Employee Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : EmployeeDBConstants.EMPLOYEE.keySet() ){
			newQryHist.append( EmployeeDBConstants.EMPLOYEE.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_EMPLOYEE);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.EMPLOYEE.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Employee> employeeList = (List<Employee>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Employee>(Employee.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( EmployeeDBConstants.TBL_EMPLOYEE_HISTORY);newQry.append("( ");
			for( String column : EmployeeDBConstants.EMPLOYEE_HISTORY.keySet() ){
				if(!column.equals(EmployeeDBConstants.EMPLOYEE_HISTORY.get("ID"))){
				newQry.append( EmployeeDBConstants.EMPLOYEE_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, employeeList.get(i).getIdentifier());ps.setString(2, employeeList.get(i).getMaId());
					ps.setString(3, employeeList.get(i).getFirstName());ps.setString(4, employeeList.get(i).getSurName());
					ps.setString(5, employeeList.get(i).getQualificationsMaId());ps.setString(6, employeeList.get(i).getCalendarId());
					ps.setString(7, loginUser.getUserId());ps.setTimestamp(8, date);
					ps.setString(9, loginUser.getUserId());ps.setTimestamp(10, date);ps.setString(11,action);
					
				}		
				public int getBatchSize() {
					return employeeList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when Insert Employee History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Employee History END <== ");				
	}

	/**
	 * This method delete the employee into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteEmployee(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Employee START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertEmployeeHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(EmployeeDBConstants.TBL_EMPLOYEE);newQry.append(" WHERE ");
			newQry.append( EmployeeDBConstants.EMPLOYEE.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch employee list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Employee END <== ");		
	}

	
	/**
	 * This method inserts new employee into DB
	 * @param commonError
	 * @param loginUser
	 * @param employee
	 */
	public void addEmployee(CommonError commonError, User loginUser, Employee employee) {
		logger.info(" ==> Insert Employee START <== ");
		try{
			insertRecord(loginUser, employee,EmployeeDBConstants.TBL_EMPLOYEE,EmployeeDBConstants.employeeDBConfig.get("rowPrefix"));  
		}catch( Exception e ){
			logger.info("Error when Insert Employee.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Employee END <== "); 		
	}

	
	/**
     * This method updates existing employee in DB
     * @param commonError
     * @param loginUser
     * @param employee
     */
	public void updateEmployee(CommonError commonError, User loginUser, Employee employee) {
		logger.info(" ==> Update Employee START <== ");
		try{
			updateRecordById(loginUser, employee,EmployeeDBConstants.TBL_EMPLOYEE); 
		}catch( Exception e ){ 
			logger.info("Error when Update Employee.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Employee END <== ");		
	}

}
