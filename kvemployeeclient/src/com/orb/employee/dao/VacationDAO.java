package com.orb.employee.dao;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Vacation;
import com.orb.employee.constants.EmployeeDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class VacationDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(VacationDAO.class);
	
	
	/**
     * This method will return list of vacation from DB
     * @param commonError
     * @param activeFlag
     * @return
     */	
	public List<Vacation> getVacationList(CommonError commonError, String s) {
		logger.info(" ==> Get Vacation List START <== ");
		List<Vacation> vacationList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : EmployeeDBConstants.VACATION.keySet() ){
				   qry.append( EmployeeDBConstants.VACATION.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(EmployeeDBConstants.TBL_VACATION);qry.append(" WHERE ");
			qry.append(EmployeeDBConstants.VACATION.get("ACTIVE"));qry.append("=? ");
			vacationList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(Vacation.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to  get vacation list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==>  Get Vacation List END <== ");
		return vacationList;
	}

	/**
	 * This method used to get a particular vacation from DB
	 * @param vacationId
	 * @param commonError
	 * @return vacation
	 */
	public Vacation getVacation(CommonError commonError, String vacationId) {
		logger.info(" ==> Get a Vacation START <== ");
		Vacation vacation = null;
		Vacation lockedHist = null;
		Vacation unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : EmployeeDBConstants.VACATION.keySet() ){
				newQry.append( EmployeeDBConstants.VACATION.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(EmployeeDBConstants.TBL_VACATION);newQry.append(" WHERE ");
			newQry.append( EmployeeDBConstants.VACATION.get("IDENTIFIER") );newQry.append( "=? ");
			vacation = (Vacation)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{vacationId}, new BeanPropertyRowMapper(Vacation.class) );
			
		lockedHist = getUserAction(commonError, vacation.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			vacation.setLockedBy( lockedHist.getUpdateBy());
			vacation.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, vacation.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			vacation.setUnlockedBy( unlockedHist.getUpdateBy());
			vacation.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch vacation. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Vacation END <== ");
		return vacation;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */	
	private Vacation getUserAction(CommonError commonError, String id, String action) {
		Vacation historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : EmployeeDBConstants.VACATION_HISTORY.keySet() ){
			newQryHist.append( EmployeeDBConstants.VACATION_HISTORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_VACATION_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.VACATION.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(EmployeeDBConstants.VACATION_HISTORY.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(EmployeeDBConstants.VACATION_HISTORY.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Vacation)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Vacation.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}

	/**
	 * This method will activate or deactivate the Vacation
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> UpdateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertVacationHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertVacationHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( EmployeeDBConstants.TBL_VACATION );qry.append(" SET ");
			qry.append( EmployeeDBConstants.VACATION.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( EmployeeDBConstants.VACATION.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a vacation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> UpdateActiveStatus END <== ");				
	}

	/**
     * This method inserts updated Role into Role history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
	private void insertVacationHistory(CommonError commonError, User loginUser, List<String> idList,
			String action) {
		logger.info(" ==> Insert Vacation History START <== ");
		logger.info("Vacation Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : EmployeeDBConstants.VACATION.keySet() ){
			newQryHist.append( EmployeeDBConstants.VACATION.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_VACATION);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.VACATION.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Vacation> vacationList = (List<Vacation>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Vacation>(Vacation.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( EmployeeDBConstants.TBL_VACATION_HISTORY);newQry.append("( ");
			for( String column : EmployeeDBConstants.VACATION_HISTORY.keySet() ){
				if(!column.equals(EmployeeDBConstants.VACATION_HISTORY.get("ID"))){
				newQry.append( EmployeeDBConstants.VACATION_HISTORY.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, vacationList.get(i).getIdentifier());ps.setString(2, vacationList.get(i).getVacationType());
					ps.setString(3, vacationList.get(i).getEmployee());ps.setObject(4, vacationList.get(i).getStartDate());
					ps.setObject(5, vacationList.get(i).getEndDate());ps.setString(6, vacationList.get(i).getReason());
					ps.setString(7, loginUser.getUserId());ps.setTimestamp(8, date);
					ps.setString(9, loginUser.getUserId());ps.setTimestamp(10, date);ps.setString(11,action);
					
				}		
				public int getBatchSize() {
					return vacationList.size();
				}
				
			});
		}catch( Exception e ){
			logger.info("Error when Insert Vacation History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Vacation History END <== ");				
	}

	/**
	 * This method delete the vacation into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteVacation(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Vacation START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertVacationHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(EmployeeDBConstants.TBL_VACATION);newQry.append(" WHERE ");
			newQry.append( EmployeeDBConstants.VACATION.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch vacation list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Vacation END <== ");		
	}

	
	/**
	 * This method inserts new vacation into DB
	 * @param commonError
	 * @param loginUser
	 * @param vacation
	 */
	public void addVacation(CommonError commonError, User loginUser, Vacation vacation) {
		logger.info(" ==> Insert Vacation START <== ");
		try{
			insertRecord(loginUser, vacation,EmployeeDBConstants.TBL_VACATION,EmployeeDBConstants.vacationDBConfig.get("rowPrefix"));  
		}catch( Exception e ){
			logger.info("Error when Insert Vacation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Vacation END <== "); 		
	}

	
	/**
     * This method updates existing vacation in DB
     * @param commonError
     * @param loginUser
     * @param vacation
     */
	public void updateVacation(CommonError commonError, User loginUser, Vacation vacation) {
		logger.info(" ==> Update Vacation START <== ");
		try{
			updateRecordById(loginUser, vacation,EmployeeDBConstants.TBL_VACATION); 
		}catch( Exception e ){ 
			logger.info("Error when Update Vacation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Vacation END <== ");		
	}

}
