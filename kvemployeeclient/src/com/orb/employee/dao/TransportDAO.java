package com.orb.employee.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.employee.bean.Transport;
import com.orb.employee.constants.EmployeeDBConstants;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class TransportDAO extends AbstractDAO {
	    private static Log logger = LogFactory.getLog(TransportDAO.class);
	
		/**
	     * This method will return list of transport from DB
	     * @param commonError
	     * @param activeFlag
	     * @return
	     */	
		public List<Transport> getTransportList(CommonError commonError, String s) {
			logger.info(" ==> Get Transport List START <== ");
			List<Transport> transportList = null;
			try{
				StringBuffer qry = new StringBuffer("SELECT ");
				 for( String column : EmployeeDBConstants.TRANSPORT.keySet() ){
					   qry.append( EmployeeDBConstants.TRANSPORT.get(column) );qry.append( "," );
					}
				   qry.setLength(qry.length()-1);
				qry.append(" FROM ");qry.append(EmployeeDBConstants.TBL_TRANSPORT);qry.append(" WHERE ");
				qry.append(EmployeeDBConstants.TRANSPORT.get("ACTIVE"));qry.append("=? ");
				transportList = getJdbcTemplate().query( qry.toString(),new String[]{s},new BeanPropertyRowMapper(Transport.class) );
				}
			catch( Exception e ){
				logger.error(" Exception occured when tries to  get transport list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_GETLIST_ERROR);
			}
			logger.info(" ==>  Get Transport List END <== ");
			return transportList;
		}

		/**
		 * This method used to get a particular transport from DB
		 * @param transportId
		 * @param commonError
		 * @return transport
		 */
		public Transport getTransport(CommonError commonError, String transportId) {
			logger.info(" ==> Get a Transport START <== ");
			Transport transport = null;
			Transport lockedHist = null;
			Transport unlockedHist = null; 
			try{
				StringBuffer newQry = new StringBuffer("SELECT ");
				for( String column : EmployeeDBConstants.TRANSPORT.keySet() ){
					newQry.append( EmployeeDBConstants.TRANSPORT.get(column) );newQry.append( "," );
					}
				newQry.setLength(newQry.length()-1);
				newQry.append(" FROM ");newQry.append(EmployeeDBConstants.TBL_TRANSPORT);newQry.append(" WHERE ");
				newQry.append( EmployeeDBConstants.TRANSPORT.get("IDENTIFIER") );newQry.append( "=? ");
				transport = (Transport)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{transportId}, new BeanPropertyRowMapper(Transport.class) );
				
			lockedHist = getUserAction(commonError, transport.getIdentifier(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				transport.setLockedBy( lockedHist.getUpdateBy());
				transport.setLockedTime(  lockedHist.getUpdateTime());
			}
			
			unlockedHist = getUserAction(commonError, transport.getIdentifier(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				transport.setUnlockedBy( unlockedHist.getUpdateBy());
				transport.setUnlockedTime( unlockedHist.getUpdateTime());
			}
		  }catch( Exception e ){
				logger.error(" Exception occured when tries to fetch transport. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
		  }
				logger.info(" ==> Get a Transport END <== ");
			return transport;
		}

		
		/**
		 * Get user actions from table for particular table
		 * @param commonError
		 * @param id
		 * @param action
		 * @return historyInfo
		 */	
		private Transport getUserAction(CommonError commonError, String id, String action) {
			Transport historyInfo = null;
			try{
			StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
			for( String column : EmployeeDBConstants.TRANSPORT_HISTORY.keySet() ){
				newQryHist.append( EmployeeDBConstants.TRANSPORT_HISTORY.get(column) );newQryHist.append( "," );
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_TRANSPORT_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.TRANSPORT.get("IDENTIFIER") );
			newQryHist.append( "=? AND ");
			newQryHist.append(EmployeeDBConstants.TRANSPORT_HISTORY.get("ACTION"));
			newQryHist.append("=? ");
			newQryHist.append(" ORDER BY ");
			newQryHist.append(EmployeeDBConstants.TRANSPORT_HISTORY.get("ID"));
			newQryHist.append(" DESC LIMIT 1 ");
			historyInfo = (Transport)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Transport.class) );
			}catch( Exception e ){
				logger.error(" No user action made before ==> " + e.getMessage() );
			}
			return historyInfo;
		}

		/**
		 * This method will activate or deactivate the Transport
		 * @param commonError
		 * @param loginUser
		 * @param idList
		 * @param s
		 */
		public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
			logger.info(" ==> UpdateActiveStatus START <== ");
			try{
				if(CommonConstants.Y.equals(s)){
					insertTransportHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
				}
				else{
					insertTransportHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
				}
				StringBuffer qry = new StringBuffer();
				qry.append(" UPDATE ");qry.append( EmployeeDBConstants.TBL_TRANSPORT );qry.append(" SET ");
				qry.append( EmployeeDBConstants.TRANSPORT.get("ACTIVE") );qry.append("=? ");
				qry.append(" WHERE ");
				qry.append( EmployeeDBConstants.TRANSPORT.get("ID") );qry.append("=? ");
				getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setString(1, s);
						ps.setString(2, idList.get(i));
					}
					
					public int getBatchSize() {
						return idList.size();
					}
					
				});
			}catch( Exception e ){
				logger.info("Error when updateActiveStatus of a transport.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_UPDATE_ERROR);
			}
			logger.info(" ==> UpdateActiveStatus END <== ");				
		}

		/**
	     * This method inserts updated Role into Role history table
	     * @param commonError
	     * @param loginUser
	     * @param idList
	     * @param action
	     * @return 
	     */
		private void insertTransportHistory(CommonError commonError, User loginUser, List<String> idList,
				String action) {
			logger.info(" ==> Insert Transport History START <== ");
			logger.info("Transport Id ==> " + idList );
			StringBuffer newQry = new StringBuffer();
			StringBuffer newQryHist = new StringBuffer();
			newQryHist.append("SELECT ");
			for( String column : EmployeeDBConstants.TRANSPORT.keySet() ){
				newQryHist.append( EmployeeDBConstants.TRANSPORT.get(column) );newQryHist.append( "," );
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(" FROM ");newQryHist.append(EmployeeDBConstants.TBL_TRANSPORT);newQryHist.append(" WHERE ");newQryHist.append( EmployeeDBConstants.TRANSPORT.get("ID") );
			newQryHist.append(" IN ( ");
			for ( int i=0; i<idList.size(); i++ ) {
				newQryHist.append(" ? ");newQryHist.append(",");
			}
			newQryHist.setLength(newQryHist.length() - 1);
			newQryHist.append(") ");
			List<Transport> transportList = (List<Transport>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Transport>(Transport.class));
			try{
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("INSERT INTO ");newQry.append( EmployeeDBConstants.TBL_TRANSPORT_HISTORY);newQry.append("( ");
				for( String column : EmployeeDBConstants.TRANSPORT_HISTORY.keySet() ){
					if(!column.equals(EmployeeDBConstants.TRANSPORT_HISTORY.get("ID"))){
					newQry.append( EmployeeDBConstants.TRANSPORT_HISTORY.get(column) );newQry.append( "," );
					}
				}
				newQry.setLength(newQry.length()-1);
				newQry.append(") VALUES(?,?,?,?,?,?,?) ");
				getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i) throws SQLException {	
						ps.setString(1, transportList.get(i).getIdentifier());ps.setString(2, transportList.get(i).getName());
						ps.setString(3, loginUser.getUserId());
						ps.setTimestamp(4, date);ps.setString(5, loginUser.getUserId());
						ps.setTimestamp(6, date);ps.setString(7,action);
						
					}		
					public int getBatchSize() {
						return transportList.size();
					}
					
				});
			}catch( Exception e ){
				logger.info("Error when Insert Transport History.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_INSERT_ERROR);
			}
			logger.info(" ==> Insert Transport History END <== ");				
		}

		/**
		 * This method delete the transport into DB
		 * @param commonError
		 * @param loginUser
		 * @param idList
		 */
		public void deleteTransport(CommonError commonError, User loginUser, List<String> idList) {
			logger.info(" ==> Delete Transport START <== ");
			NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
			try{
				insertTransportHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
				Map<String, Object> params = new HashMap<>();
		        params.put("ids", idList);
				StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
				newQry.append(EmployeeDBConstants.TBL_TRANSPORT);newQry.append(" WHERE ");
				newQry.append( EmployeeDBConstants.TRANSPORT.get("ID") );newQry.append( " IN " );
				newQry.append(" (:ids) ");
				db.update(newQry.toString(),params);
			}catch( Exception e ){
				logger.error(" Exception occured when tries to fetch transport list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_DELETE_ERROR);
			}
			logger.info(" ==> Delete Transport END <== ");		
		}

		
		/**
		 * This method inserts new transport into DB
		 * @param commonError
		 * @param loginUser
		 * @param transport
		 */
		public void addTransport(CommonError commonError, User loginUser, Transport transport) {
			logger.info(" ==> Insert Transport START <== ");
			try{
				insertRecord(loginUser, transport,EmployeeDBConstants.TBL_TRANSPORT,EmployeeDBConstants.transportDBConfig.get("rowPrefix"));  
			}catch( Exception e ){
				logger.info("Error when Insert Transport.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_INSERT_ERROR);
			}
			logger.info(" ==> Insert Transport END <== "); 		
		}

		
		/**
	     * This method updates existing transport in DB
	     * @param commonError
	     * @param loginUser
	     * @param transport
	     */
		public void updateTransport(CommonError commonError, User loginUser, Transport transport) {
			logger.info(" ==> Update Transport START <== ");
			try{
				updateRecordById(loginUser, transport,EmployeeDBConstants.TBL_TRANSPORT); 
			}catch( Exception e ){ 
				logger.info("Error when Update Transport.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_UPDATE_ERROR);
			}
			logger.info(" ==>  Update Transport END <== ");		
		}

}


