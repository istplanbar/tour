package com.orb.employee.bean;

import java.util.Date;

public class Employee {
	
	private String id;
	private String identifier;
	private String maId;
	private String firstName;	
	private String surName;
	private String qualificationsMaId;
	private String calendarId;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	private boolean checked;
	private String active;
	private String easting;
	private String northing;
	private String degreeOfLongitude;
	private String degreeOfLatitude;
	
	public String getDegreeOfLongitude() {
		return degreeOfLongitude;
	}
	public void setDegreeOfLongitude(String degreeOfLongitude) {
		this.degreeOfLongitude = degreeOfLongitude;
	}
	public String getEasting() {
		return easting;
	}
	public void setEasting(String easting) {
		this.easting = easting;
	}
	public String getNorthing() {
		return northing;
	}
	public void setNorthing(String northing) {
		this.northing = northing;
	}
	public String getDegreeOfLatitude() {
		return degreeOfLatitude;
	}
	public void setDegreeOfLatitude(String degreeOfLatitude) {
		this.degreeOfLatitude = degreeOfLatitude;
	}
	
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getMaId() {
		return maId;
	}
	public void setMaId(String maId) {
		this.maId = maId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	
	public String getQualificationsMaId() {
		return qualificationsMaId;
	}
	public void setQualificationsMaId(String qualificationsMaId) {
		this.qualificationsMaId = qualificationsMaId;
	}
	public String getCalendarId() {
		return calendarId;
	}
	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}

	
}
