package com.orb.optaplanner.rest.domain.service;

import java.util.List;
import java.util.Map;

import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolution;

import com.orb.common.client.error.CommonError;
import com.orb.employee.bean.Employee;
import com.orb.facilities.bean.Facilities;
import com.orb.optaplanner.rest.domain.bean.JsonMessage;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.tour.bean.TourCluster;

public interface OptaplannerTourClusterService {
	
	//ClusteredTours
		public VehicleRoutingSolution generateClusterTours(CommonError commonError, List<Employee> filteredEmployeeList, List<Facilities> filteredFacilitiesList,
						TourCluster tourCluster);

		public JsonVehicleRoutingSolution retrieveSolution(CommonError commonError, List<Employee> filteredEmployeeList, VehicleRoutingSolution vehicleRoutingSolution, 
											Map<Long, Map<Long, Long>> travelTimeMap, String dayOfTour);

		//public JsonMessage solve(CommonError commonError);

		public JsonVehicleRoutingSolution loadSolution();
}
