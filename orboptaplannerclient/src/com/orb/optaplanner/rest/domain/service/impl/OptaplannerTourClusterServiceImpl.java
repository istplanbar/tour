package com.orb.optaplanner.rest.domain.service.impl;

import java.awt.Color;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.examples.vehiclerouting.domain.Customer;
import org.optaplanner.examples.vehiclerouting.domain.Depot;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolution;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedVehicleRoutingSolution;
import org.optaplanner.examples.vehiclerouting.domain.location.DistanceType;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.location.RoadLocation;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedCustomer;
import org.optaplanner.examples.vehiclerouting.domain.timewindowed.TimeWindowedDepot;
import org.optaplanner.swing.impl.TangoColorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.error.CommonError;
import com.orb.employee.bean.Employee;
import com.orb.employee.dao.EmployeeDAO;
import com.orb.facilities.bean.Facilities;
import com.orb.optaplanner.rest.domain.bean.JsonCustomer;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoute;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.optaplanner.rest.domain.service.OptaplannerTourClusterService;
import com.orb.taskplanner.bean.Task;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.TourCluster;
import com.orb.tour.dao.KorfmannLocationDAO;

@Service
@Scope(	BeanDefinition.SCOPE_PROTOTYPE )
public class OptaplannerTourClusterServiceImpl implements OptaplannerTourClusterService{
	private static Log logger = LogFactory.getLog(OptaplannerTSPServiceImpl.class);

	@Autowired
	EmployeeDAO  employeeDAO;
	
	@Autowired
	KorfmannLocationDAO korfmannLocationDAO;

	private static VehicleRoutingSolution vehicleRoutingSolution;
	private static int vehicleListSize;
	private static Map<Long, Location> locationMap;
	private static Map<Long, Facilities> faciltiesMap;
	private static Map<Long, List<Task>> facilitiesLocationMap;
	private static List<Depot> depotList;
	private static List<Location> customerLocationList;
	//private static final String SOLVER_CONFIG =  System.getProperty("config.dir") + "moduleConfig/optaplanner/vehicleRoutingSolverConfig.xml" ;
	//private SolverFactory<VehicleRoutingSolution> solverFactory;
	private ExecutorService executor;
	private Map<String, VehicleRoutingSolution> sessionSolutionMap;
	private Map<String, Solver<VehicleRoutingSolution>> sessionSolverMap;
	Locale theLocale = Locale.US;
	private NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(theLocale);
	private static JsonVehicleRoutingSolution jsonSolution;

	
	/**
	 * This method initialize all the objects and returns the json solution if already available.
	 */
	@Override
	public JsonVehicleRoutingSolution loadSolution() {
		return jsonSolution;
	}

	/**
	 * This method generates solution object which is feeded to optaplanner map object
	 */
	@Override
	public JsonVehicleRoutingSolution retrieveSolution(CommonError commonError, List<Employee> filteredEmployeeList ,VehicleRoutingSolution vehicleRoutingSolution, 
																Map<Long, Map<Long, Long>> travelTimeMap, String dayOfTour) {
		logger.info("Rest Service is triggered to call the Solution");
		return convertToJsonVRPSolution(commonError, filteredEmployeeList, vehicleRoutingSolution, travelTimeMap, dayOfTour);
	}

	/**
	 * This method generates optaplanner object from the given inputs and passed to the solver
	 */
	@Override
	public VehicleRoutingSolution generateClusterTours(CommonError commonError, List<Employee> filteredEmployeeList, 
															List<Facilities> filteredFacilitiesList, TourCluster tourCluster) {
		logger.info("Generation of Cluster Tour is started");
		vehicleRoutingSolution = new VehicleRoutingSolution();
		locationMap = new LinkedHashMap<>();
		facilitiesLocationMap = new LinkedHashMap<>();
		faciltiesMap = new LinkedHashMap<>();
		customerLocationList = new ArrayList<>();
		if(tourCluster == null) tourCluster = new TourCluster();
		List<Facilities> facilitiesList =  new ArrayList<>();
		KorfAddress korfAddress = new KorfAddress();
		try{
			if(!filteredFacilitiesList.isEmpty()){
				facilitiesList =  new ArrayList<>(filteredFacilitiesList);
			}
			if(!filteredEmployeeList.isEmpty()) {
				vehicleListSize = filteredEmployeeList.size();
			}

			korfAddress = korfmannLocationDAO.getKorfAddress(commonError, tourCluster.getKorfmannOfficeLocation());
			if(korfAddress.getIdentifier() != null){
				if(korfAddress.getFacilityLatitude() != 0 && korfAddress.getFacilityLongitude() != 0){
					Location loc = new RoadLocation(Long.parseLong(korfAddress.getId()), korfAddress.getFacilityLatitude(), korfAddress.getFacilityLongitude() );
					loc.setName(korfAddress.getName());
					customerLocationList.add((Location)loc);
					locationMap.put(0L , loc);
				}
			}
			for(Facilities f : facilitiesList){
				if(f.getFacilityLatitude() != 0 && f.getFacilityLongitude() != 0){
					Location loc = new RoadLocation(Long.parseLong(f.getId()), f.getFacilityLatitude(), f.getFacilityLongitude());
					loc.setName(f.getName());
					if(!customerLocationList.contains((Location)loc)){
						customerLocationList.add((Location)loc);
						locationMap.put(loc.getId() , loc);
						faciltiesMap.put(loc.getId(), f);
						facilitiesLocationMap.put(loc.getId(), f.getTaskAndWorkHoursList());
					}
				}
			}
			if(!customerLocationList.isEmpty()){
				setTravelDistanceMapLocations();
				createCustomerList(tourCluster);
			}

		}catch(Exception e){
			logger.info("Exception on Creatin Solutions" + e.getMessage());
		}
		logger.info("Customer Location List is generated here");
		return vehicleRoutingSolution;
	}

	/**
	 * This method generates the travel Distance between customer location
	 */
	private synchronized void setTravelDistanceMapLocations(){
		logger.info("The distance between Map locations are calculated here");
		for (int i = 0; i < customerLocationList.size(); i++) {
			RoadLocation startLocation = (RoadLocation) customerLocationList.get(i);
			Map<RoadLocation, Double> travelDistanceMap = new LinkedHashMap<>();
			for (int j = 0; j < customerLocationList.size(); j++) {
				RoadLocation endLocation = (RoadLocation) customerLocationList.get(j);
				double travelDistance = distance(startLocation.getLatitude(), startLocation.getLongitude(),
						endLocation.getLatitude(), endLocation.getLongitude(), 'K');
				if (i == j) {
					if(startLocation.getName().equals(endLocation.getName())) travelDistance = 0.0;
					if (travelDistance != 0.0) {
						throw new IllegalStateException("The travelDistance (" + travelDistance + ") should be zero.");
					}
				} else {
					travelDistanceMap.put(endLocation, travelDistance);
				}
			}
			startLocation.setTravelDistanceMap(travelDistanceMap);
		}
	}
	/**
	 * This method generates customer object based on given input
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private synchronized void createCustomerList(TourCluster tourCluster) throws IOException {
		logger.info("Customer List Creation Started");
		depotList = new ArrayList<>();
		List<Customer> customerList = new ArrayList<>();
		try{
		for (int i = 0; i < customerLocationList.size(); i++) {
			long id = customerLocationList.get(i).getId();
			if (i == 0) {
				//Depot depot = new Depot();
				TimeWindowedDepot depot = new TimeWindowedDepot();
				depot.setId(id);
				depot.setReadyTime(0);
				depot.setDueTime((Long.parseLong(tourCluster.getWorkingHours())*60*1000) - (Long.parseLong(tourCluster.getBreakAtWork())*1000));
				Location location = locationMap.get(0L);
				if (location == null) {
					throw new IllegalArgumentException("The depot with id (" + id + ") has no location (" + location + ").");
				}
				depot.setLocation(location);
				depotList.add(depot);
			} else {
				TimeWindowedCustomer customer = new TimeWindowedCustomer();
				//Customer customer = new Customer();
				customer.setId(id);
				Facilities facility = faciltiesMap.get(id); 
				Location location = locationMap.get(id);
				if (location == null) {
					throw new IllegalArgumentException("The customer with id (" + id + ") has no location (" + location + ").");
				}
				Long readyTime = 0L;
				Long dueTime = 68400L;
				if(facility.getVisitingHours() != null && facility.getVisitingTime() != null){
					if(facility.getVisitingHours().equals("2") || facility.getVisitingHours().equals("3")){
						if(!StringUtils.isEmpty(facility.getVisitingTime())){
							readyTime = Long.parseLong(facility.getVisitingTime().split(":")[0])*60*1000;
							/*Double tempTime = Double.parseDouble(facility.getVisitingTime().replace(":","."))*60*1000;
							readyTime = tempTime.longValue();*/
						}else{
							readyTime = 1080000L;
						}
						customer.setReadyTime(readyTime);
						customer.setDueTime(readyTime+60000);
						//logger.info("At or After "+facility.getName()+ " "+ facility.getVisitingTime() + " "+ customer.getReadyTime() + " "+customer.getDueTime());
					}

					if(facility.getVisitingHours().equals("1")){
						if(!StringUtils.isEmpty(facility.getVisitingTime())){
							dueTime = Long.parseLong(facility.getVisitingTime().split(":")[0])*60*1000;
							/*Double tempTime = Double.parseDouble(facility.getVisitingTime().replace(":","."))*60*1000;
							dueTime = tempTime.longValue();*/
						}else{
							dueTime = 1440000L;
						}
						customer.setReadyTime(dueTime-60000);
						customer.setDueTime(dueTime);
						//logger.info("Before "+facility.getName()+ " "+facility.getVisitingTime()+ " "+customer.getReadyTime() + " "+customer.getDueTime());
					}
					
				}else{
					customer.setReadyTime(1080000L);
					customer.setDueTime(1440000L);
				}
				
				
				customer.setLocation(location);
				customer.setDemand(1);
				customerList.add(customer);
			}
		}
		}catch(Exception e){
			logger.info("Error while creating customer list :"+e.getMessage());
		}
		
		/*@SuppressWarnings("rawtypes")
		Comparator custCompare = Comparator.comparingLong(TimeWindowedCustomer::getReadyTime);
		Collections.sort(customerList, custCompare);*/
		vehicleRoutingSolution.setId((long)1);
		vehicleRoutingSolution.setName("Solution Test");
		vehicleRoutingSolution.setDistanceType(DistanceType.ROAD_DISTANCE);
		vehicleRoutingSolution.setDistanceUnitOfMeasurement("km");
		vehicleRoutingSolution.setLocationList(customerLocationList);
		vehicleRoutingSolution.setCustomerList(customerList);
		vehicleRoutingSolution.setDepotList(depotList);
		createVehicleList();
		logger.info("Customer List Creation is finished");
	}


	private synchronized static void createVehicleList() {
		logger.info("Creating Vehicle is Called");
		List<Vehicle> vehicleList = new ArrayList<>();
		for (int i = 0; i < vehicleListSize; i++) {
			Vehicle vehicle = new Vehicle();
			vehicle.setId((long) i);
			vehicle.setCapacity(customerLocationList.size()/vehicleListSize);
			//vehicle.setCapacity(10);
			vehicle.setDepot(depotList.get(i % depotList.size()));
			vehicleList.add(vehicle);
		}
		logger.info("Creating Vehicle is finished");
		vehicleRoutingSolution.setVehicleList(vehicleList);
	}

	/**
	 * This methods convers the normal optaplanner solution into Json solution. 
	 * @param solution
	 * @return Json Solution
	 */
	protected JsonVehicleRoutingSolution convertToJsonVRPSolution(CommonError commonError, List<Employee> employeeList,  
					VehicleRoutingSolution solution, Map<Long, Map<Long, Long>> travelTimeMap, String dayOfTour) {
		logger.info("Converting solution to json is started ");
		try{
		jsonSolution = new JsonVehicleRoutingSolution();
		jsonSolution.setName(solution.getName());
		List<JsonCustomer> jsonCustomerList = new ArrayList<>(solution.getCustomerList().size());
		Comparator custCompare = Comparator.comparingLong(TimeWindowedCustomer::getReadyTime);
		Collections.sort(solution.getCustomerList(), custCompare);
		
		for (Customer customer : solution.getCustomerList()) {
			Location customerLocation = customer.getLocation();
			JsonCustomer eachCustomer = new JsonCustomer(customerLocation.getName(),
					customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand());
			TimeWindowedCustomer timeWinCust = (TimeWindowedCustomer) solution.getCustomerList().stream().filter( c -> c.getId()== customer.getId()).findFirst().get();
			eachCustomer.setReadyTime(timeWinCust.getReadyTime());
			jsonCustomerList.add(eachCustomer);
		}
		
		Comparator jsonCustCompare = Comparator.comparingLong(JsonCustomer::getReadyTime);
		Collections.sort(jsonCustomerList, jsonCustCompare);
		
		//jsonCustomerList.stream().forEach(cu -> System.out.println("Solution Customer "+cu.getLocationName()+ " " + cu.getReadyTime() ));
		
		jsonSolution.setCustomerList(jsonCustomerList);
		List<JsonVehicleRoute> jsonVehicleRouteList = new ArrayList<>();
		TangoColorFactory tangoColorFactory = new TangoColorFactory();
		Depot depot = new TimeWindowedDepot();
		logger.info("Total Customer Size : "+solution.getCustomerList().size());
		logger.info("Vehicle Size: "+ solution.getVehicleList().size());
		int employeeCount = 0;
			for (Vehicle vehicle : solution.getVehicleList()) {
				Color color = tangoColorFactory.pickColor(vehicle);
				depot = vehicle.getDepot();
				Location depotLocation = depot.getLocation();
				Customer customer = vehicle.getNextCustomer();
				int customerCount = 0;
				Long totalWorkHours = ((TimeWindowedDepot) depot).getDueTime();
				Long serviceDuration = 0L;
				Long travelTime = 0L;
				Location previousCustomerLocation = null;
				Set<JsonCustomer> jsonRouteCustomerList = new HashSet<>();
				Map<Long, JsonCustomer> jsonRouteCustomerMap = new HashMap<>();
				while(customer != null){
					customerCount++;
					List<Task> jsonVehicleTaskList = new ArrayList<>();
					Location customerLocation = customer.getLocation();
					
					if(previousCustomerLocation == null){
						previousCustomerLocation = customer.getLocation();
					}
					if(customerCount == 1){
						travelTime = travelTimeMap.get(depotLocation.getId()).get(customerLocation.getId());
					}else{
						travelTime = travelTimeMap.get(previousCustomerLocation.getId()).get(customerLocation.getId());
						previousCustomerLocation = customerLocation;
					}
					serviceDuration += travelTime;
					int taskCount = facilitiesLocationMap.get(customerLocation.getId()).size();
					Long returnTravelTime = 0L;
					//logger.info("Customer Count "+ customerCount + " "+ customer.toString());
					
					try{
						for(Task task : facilitiesLocationMap.get(customerLocation.getId())){
						taskCount--;
						serviceDuration += (Long.parseLong(task.getEstimatedTime())*1000);
						returnTravelTime = travelTimeMap.get(depotLocation.getId()).get(customerLocation.getId());
						serviceDuration += returnTravelTime;
						
						/*if(((totalWorkHours-45000) <= serviceDuration && (totalWorkHours-25000) >= serviceDuration )||  
								(totalWorkHours-45000) <= serviceDuration || customer.getNextCustomer() == null){*/
						
						if(((totalWorkHours) <= serviceDuration && (totalWorkHours-25000) >= serviceDuration )||  
								(totalWorkHours) <= serviceDuration || customer.getNextCustomer() == null) {
							Long custId = customer.getId();
							if(totalWorkHours < serviceDuration){
								jsonVehicleTaskList.remove(task);
								if(jsonRouteCustomerMap.containsKey(customerLocation.getId())){
									
									JsonCustomer jsonCust = returnJsonCustomer(solution, customer, jsonVehicleTaskList,
											customerLocation, custId);
									
									jsonRouteCustomerMap.put(customerLocation.getId(), jsonCust);
								}
							}else{
								jsonVehicleTaskList.add(task);
								
								JsonCustomer jsonCust = returnJsonCustomer(solution, customer, jsonVehicleTaskList,
										customerLocation, custId);
								
								jsonRouteCustomerMap.put(customerLocation.getId(), jsonCust);
								if(customer.getNextCustomer() == null && taskCount != 0){
									continue;
								}
							}
							if(!jsonRouteCustomerMap.isEmpty()){
								for(JsonCustomer cust : jsonRouteCustomerMap.values()){
									//System.out.println("Ready "+cust.getReadyTime());
									jsonRouteCustomerList.add(cust);
								}
								jsonRouteCustomerMap = new HashMap<>();
							}
							if(!jsonRouteCustomerList.isEmpty()){
								JsonVehicleRoute jsonVehicleRoute = new JsonVehicleRoute();
								jsonVehicleRoute.setDepotLocationName(depotLocation.getName());
								jsonVehicleRoute.setDepotLatitude(depotLocation.getLatitude());
								jsonVehicleRoute.setDepotLongitude(depotLocation.getLongitude());
								jsonVehicleRoute.setCapacity(vehicle.getCapacity());
								jsonVehicleRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
								//jsonVehicleRoute.setEmployeeName(employeeList.get(i).getFirstName());
								jsonVehicleRoute.setEmployeeName(employeeList.get(employeeCount).getFirstName());
								List<JsonCustomer> finalRouteList  = jsonRouteCustomerList.stream()
													.sorted(Comparator.comparing(JsonCustomer::getReadyTime)) 
													.collect(Collectors.toList());
								jsonVehicleRoute.setCustomerList(new ArrayList<>(finalRouteList));
								//finalRouteList.stream().forEach(s -> System.out.println(s.getLocationName() + " "+s.getReadyTime()));
								jsonVehicleRoute.setDayOfTour(dayOfTour);
								jsonVehicleRoute.setTourDuration(serviceDuration);
								jsonVehicleTaskList = new ArrayList<>();
								jsonRouteCustomerList = new HashSet<>();
								if(totalWorkHours < serviceDuration){
									serviceDuration = 0L;
									serviceDuration += (Long.parseLong(task.getEstimatedTime())*1000);
									serviceDuration += returnTravelTime;
									jsonVehicleTaskList.add(task);
									
									JsonCustomer jsonCust = returnJsonCustomer(solution, customer, jsonVehicleTaskList,
											customerLocation, custId);
									
									jsonRouteCustomerMap.put(customerLocation.getId(), jsonCust);
								}else{
									serviceDuration = 0L;
								}
								employeeCount++;
								jsonVehicleRouteList.add(jsonVehicleRoute);
								if(jsonVehicleRouteList.size() == solution.getVehicleList().size()){
									break;
								}
							}
						}else{
							jsonVehicleTaskList.add(task);
							serviceDuration -= returnTravelTime;
							if(taskCount == 0){
								
								JsonCustomer jsonCust = returnJsonCustomer(solution, customer, jsonVehicleTaskList,
										customerLocation, customer.getId());
								
								jsonRouteCustomerMap.put(customerLocation.getId(), jsonCust);
							}
						}
					}
				}catch(Exception e){
					logger.error("Error in route Generation ", e);
					commonError.addError("Fehler beim Tour Clustering ");
					throw new Exception();
				}
					
					if(jsonVehicleRouteList.size() == solution.getVehicleList().size()){
						employeeCount = 0;
						break;
					}
					customer = customer.getNextCustomer();
					//logger.info(customerLocation.getName());
				}
			}
			jsonSolution.setVehicleRouteList(jsonVehicleRouteList);
			HardSoftLongScore score = solution.getScore();
			jsonSolution.setFeasible(score != null && score.isFeasible());
			jsonSolution.setDistance(solution.getDistanceString(NUMBER_FORMAT));
			logger.info("Converting solution to json is finished ");
		}catch(Exception e){
			commonError.addError("Fehler beim Tour Clustering ");
			logger.error("Error while clustering" + e.getMessage());
		}
		return jsonSolution;
	}

	private JsonCustomer returnJsonCustomer(VehicleRoutingSolution solution, Customer customer,
			List<Task> jsonVehicleTaskList, Location customerLocation, Long custId) {
		JsonCustomer jsonCust = new JsonCustomer(customerLocation.getId(), customerLocation.getName(),
				customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand(), jsonVehicleTaskList);
		TimeWindowedCustomer timeWinCust = (TimeWindowedCustomer) solution.getCustomerList().stream().filter( c -> c.getId()== custId).findFirst().get();
		
		jsonCust.setReadyTime(timeWinCust.getReadyTime());
		return jsonCust;
	}




	/**
	 * This method retrieves if its not null or creates optaplanner solution
	 * @param sessionId
	 * @return
	 */
	public synchronized VehicleRoutingSolution retrieveOrCreateSolution(String sessionId) {
		logger.info("Retrieve or Create Solution  is called");
		VehicleRoutingSolution tempRoutingSolution = sessionSolutionMap.get(sessionId);
		if (tempRoutingSolution == null) {
			if(vehicleRoutingSolution != null) {
				tempRoutingSolution = vehicleRoutingSolution;
				sessionSolutionMap.put(sessionId, vehicleRoutingSolution);
			}else{
				tempRoutingSolution = new VehicleRoutingSolution();
			}
		}
		logger.info("Retrieve or Create solution is returned");
		return tempRoutingSolution;
	}

	/**
	 * Shutdown the optaplanner solver if running.
	 */
	@PreDestroy
	public synchronized void destroy() {
		for (Solver<VehicleRoutingSolution> solver : sessionSolverMap.values()) {
			solver.terminateEarly();
		}
		executor.shutdown();
	}
	
	
	
	public double distance( double lat1, double lon1, double lat2, double lon2, char unit) {
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  if (unit == 'K') {
		    dist = dist * 1.609344;
		  } else if (unit == 'N') {
		  dist = dist * 0.8684;
		    }
		  return (dist);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts decimal degrees to radians             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}
}

/**
 * This methods helps to get real traveling time between places
 * @return

private Long getTravelDistanceTime(CommonError commonError, double lat1, double lon1, double lat2, double lon2, String apiKey){
	Long travelTime = 0L; 
	try {
		GeoApiContext context = new GeoApiContext.Builder().disableRetries().apiKey(apiKey).build();
        DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(context); 
        DistanceMatrix trix = req.origins(new LatLng(lat1,lon1))
                .destinations(new LatLng(lat2,lon2))
                .mode(TravelMode.DRIVING)
                .language("de-DE")
                .await();                                                                                                         
        travelTime = (trix.rows[0].elements[0].duration.inSeconds)/60;
    } catch(ApiException e){
	     logger.info(e.getMessage());
	     commonError.addError("Google Maps API fehler: "+e.getMessage());
	} catch(Exception e){
	     logger.info(e.getMessage());
	     commonError.addError("Google Maps API fehler: "+e.getMessage());
	}
	
    return travelTime * 1000;
}

//This is the final code to take off

/*if(jsonRouteCustomerList.size() >= 1){
JsonVehicleRoute jsonVehicleRoute = new JsonVehicleRoute();
jsonVehicleRoute.setDepotLocationName(depotLocation.getName());
jsonVehicleRoute.setDepotLatitude(depotLocation.getLatitude());
jsonVehicleRoute.setDepotLongitude(depotLocation.getLongitude());
jsonVehicleRoute.setCapacity(vehicle.getCapacity());
jsonVehicleRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
jsonVehicleRoute.setEmployeeName(employeeList.get(i).getFirstName());
jsonVehicleRoute.setCustomerList(new ArrayList<>(jsonRouteCustomerList));
jsonVehicleRoute.setDayOfTour(dayOfTour);
jsonRouteCustomerList = new HashSet<>();
jsonVehicleRouteList.add(jsonVehicleRoute);
}*/

/**
 * This method calls the solver with current session id to solve the solution.
 
@Override
public JsonMessage solve(CommonError commonError) {
	initializeSolver();
	logger.info("Solver updates the optimal solution");
	//boolean success = solve(commonError, request.getSession().getId(), mapApiKey);
	boolean success = false;
	return new JsonMessage(success ? "Solving started." : "Solver was already running.");
}*/

/**
 * Intializes the solver to define the Routing solution.
 
private void initializeSolver(){
	logger.info("Solver Initialization started");
	solverFactory = SolverFactory.createFromXmlFile(new File(SOLVER_CONFIG));
	executor = Executors.newFixedThreadPool(10); 
    sessionSolutionMap = new ConcurrentHashMap<>();
	sessionSolverMap = new ConcurrentHashMap<>();
	logger.info("Solver Initialized Successfully");
}*/


/*VehicleRoutingSolution solution = null;
JsonVehicleRoutingSolution jsonSolution = null;
if(sessionSolutionMap == null) { 
	initializeSolver(); 
}
 solution = retrieveOrCreateSolution(request.getSession().getId());
if(solution != null){
	jsonSolution = convertToJsonVRPSolution(commonError, employeeList, solution);
}else{
	jsonSolution = new JsonVehicleRoutingSolution();
}
logger.info("Solution generation is started ");
return jsonSolution;*/
//sessionSolutionMap.put(request.getSession().getId(), vehicleRoutingSolution);


/*	*//**
 * This method solves the optaplanner solution. 
 * Once the solution is solved, route is calculated with travel time between facilities.
 * @param sessionId
 * @return
 *//*
public synchronized boolean solve(CommonError commonError, final String sessionId, String apiKey) {
	logger.info("Solver is initialized");
	long start = System.currentTimeMillis();
	final Solver<VehicleRoutingSolution> solver = solverFactory.buildSolver();
	try{
		solver.addEventListener(event -> {
			logger.info("Solution Event listener is initiated");
			VehicleRoutingSolution bestSolution = (VehicleRoutingSolution)event.getNewBestSolution();
			synchronized (OptaplannerTourClusterServiceImpl.this) {
				logger.info("BestSolution is added");
				sessionSolutionMap.put(sessionId, bestSolution);
				vehicleRoutingSolution = bestSolution;
			}

		});
		if (sessionSolverMap.containsKey(sessionId)) {
			return false;
		}
		sessionSolverMap.put(sessionId, solver);
		final VehicleRoutingSolution solution = (VehicleRoutingSolution)retrieveOrCreateSolution(sessionId);
		//final VehicleRoutingSolution solution = vehicleRoutingSolution;
		executor.submit((Runnable) () -> {
			logger.info("Thread Executer is Running");
			VehicleRoutingSolution bestSolution = solver.solve(solution);
			logger.info("Solver is successfully processed with custmer count ");
			synchronized (OptaplannerTourClusterServiceImpl.this) {
				sessionSolutionMap.put(sessionId, bestSolution);
				vehicleRoutingSolution = bestSolution;
				long startTime = System.currentTimeMillis();
				Map<Long, Long> travelTimeEndLocationMap = new LinkedHashMap<>();
				Map<Long, Long> travelTimeNextLocationMap = new LinkedHashMap<>();
				for(Vehicle v : bestSolution.getVehicleList()){
					Customer c = v.getNextCustomer();
					RoadLocation depot = (RoadLocation) v.getDepot().getLocation();
					while(c != null){
						long travelTime = getTravelDistanceTime(commonError, depot.getLatitude(), depot.getLongitude(),
								c.getLocation().getLatitude(), c.getLocation().getLongitude(), apiKey);
						travelTimeEndLocationMap.put(c.getId(), travelTime);
						travelTimeMap.put(depot.getId(), travelTimeEndLocationMap);
						travelTimeNextLocationMap = new LinkedHashMap<>();
						if(c.getNextCustomer() != null){
							long travelTime2 = getTravelDistanceTime(commonError, c.getLocation().getLatitude(), c.getLocation().getLongitude(),
									c.getNextCustomer().getLocation().getLatitude(), c.getNextCustomer().getLocation().getLongitude(), apiKey);
							travelTimeNextLocationMap.put(c.getNextCustomer().getId(), travelTime2);
							travelTimeMap.put(c.getId(), travelTimeNextLocationMap);
						}
						c = c.getNextCustomer();
					}
				}
				System.out.println("Second Time "+ (System.currentTimeMillis() - startTime)/1000 + " sec");
				logger.info("Travel Distance Time is found for all customers");
				logger.info("BestSolution is added again");
				sessionSolverMap.remove(sessionId);
			}
		});
		executor.shutdown();
		executor.awaitTermination(10, TimeUnit.SECONDS);
		logger.info("Executed Time: "+  (System.currentTimeMillis() - start) / 1000 + " sec" +" Travel Time Size " + travelTimeMap.size());
	}catch(InterruptedException ie){
		commonError.addError(ie.getMessage());
		logger.info("Execution is interrrupted: "+ ie.getMessage());
	}catch(Exception e){
		commonError.addError(e.getMessage());
		logger.info("Execution is interrrupted: "+ e.getMessage());
	}
	logger.info("Solver is successfully processed " );
	return true;
}*/

/*for (Vehicle vehicle : solution.getVehicleList()) {
	JsonVehicleRoute jsonVehicleRoute = new JsonVehicleRoute();
	JsonVehicleRoute jsonVehicleExtraRoute = new JsonVehicleRoute();
	Location depotLocation = vehicle.getDepot().getLocation();
	depot = (TimeWindowedDepot) vehicle.getDepot();
	jsonVehicleRoute.setDepotLocationName(depotLocation.getName());
	jsonVehicleRoute.setDepotLatitude(depotLocation.getLatitude());
	jsonVehicleRoute.setDepotLongitude(depotLocation.getLongitude());
	jsonVehicleRoute.setCapacity(vehicle.getCapacity());
	jsonVehicleExtraRoute.setDepotLocationName(depotLocation.getName());
	jsonVehicleExtraRoute.setDepotLatitude(depotLocation.getLatitude());
	jsonVehicleExtraRoute.setDepotLongitude(depotLocation.getLongitude());
	jsonVehicleExtraRoute.setCapacity(vehicle.getCapacity());
	Long totalWorkHours = depot.getDueTime();
	Color color = tangoColorFactory.pickColor(vehicle);
	jsonVehicleRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
	jsonVehicleExtraRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
	TimeWindowedCustomer customer = (TimeWindowedCustomer) vehicle.getNextCustomer();
	//Customer customer = vehicle.getNextCustomer();
	int demandTotal = 0;
	Long serviceDuration = 0L;
	Long travelTime = 0L;
	int customerCount = 0;
	List<JsonCustomer> jsonVehicleCustomerList = new ArrayList<>();
	List<JsonCustomer> jsonVehicleExtraCustomerList = new ArrayList<>();
	List<JsonCustomer> jsonTempExtraCustomerList = new ArrayList<>();
	List<Task> jsonVehicleTaskList = new ArrayList<>();
	List<Task> jsonVehicleExtraTaskList = new ArrayList<>();
	Location previousCustomerLocation = null;
	while (customer != null) {
		customerCount++;
		Location customerLocation = customer.getLocation();
		demandTotal += customer.getDemand();
		if(previousCustomerLocation == null){
			previousCustomerLocation = customer.getLocation();
		}

		if(customerCount == 1){
			travelTime = travelTimeMap.get(depotLocation.getId()).get(customerLocation.getId());
		}else{
			travelTime = travelTimeMap.get(previousCustomerLocation.getId()).get(customerLocation.getId());
			previousCustomerLocation = customerLocation;
		}
		if(travelTime == null) travelTime = 1L;
		serviceDuration += travelTime;
		List<Task> customerTasksList = facilitiesLocationMap.get(customerLocation.getId());
		jsonVehicleTaskList = new ArrayList<>();
		jsonVehicleExtraTaskList = new ArrayList<>();
		int taskCompleted = 0;

		for(Task task : customerTasksList){
			serviceDuration += (Long.parseLong(task.getEstimatedTime())*1000);

			// Long returnTravelTime = travelTimeMap.get(customerLocation.getId()).get(depotLocation.getId());
			Long returnTravelTime = travelTimeMap.get(depotLocation.getId()).get(customerLocation.getId());

			serviceDuration += returnTravelTime;

			if(totalWorkHours <= serviceDuration){
				jsonVehicleExtraTaskList.add(task);
				serviceDuration -= (Long.parseLong(task.getEstimatedTime())*1000);
				serviceDuration -= returnTravelTime;
				if(jsonVehicleExtraTaskList.size() == 1){
					jsonVehicleExtraCustomerList.add(new JsonCustomer(customerLocation.getId(), customerLocation.getName(),
							customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand(), jsonVehicleExtraTaskList));
				}
			}else{
				taskCompleted++;
				jsonVehicleTaskList.add(task);
				serviceDuration -= returnTravelTime;
				if(taskCompleted == 1){
					jsonVehicleCustomerList.add(new JsonCustomer(customerLocation.getId(), customerLocation.getName(),
							customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand(), jsonVehicleTaskList));

				}
			} 
		}
		customer = customer.getNextCustomer();
	}
	logger.info("Solved Route Customer Size : "+customerCount);
	jsonVehicleRoute.setDemandTotal(demandTotal);
	jsonVehicleRoute.setEmployeeName(employeeList.get(i).getFirstName());
	jsonVehicleRoute.setCustomerList(jsonVehicleCustomerList);
	jsonVehicleRouteList.add(jsonVehicleRoute);
	Long extraServiceDuration = 0L;
	int extraCustomerSize = jsonVehicleExtraCustomerList.size();
	for(JsonCustomer c: jsonVehicleExtraCustomerList){
		extraCustomerSize--; 
		for(Task t : c.getTasksList()){
			Long workTime = (Long.parseLong(t.getEstimatedTime())*1000);
			extraServiceDuration += workTime;
			Long returnTravelTime = travelTimeMap.get(depotLocation.getId()).get(c.getId());
			//Long returnTravelTime = travelTimeMap.get(c.getId()).get(depotLocation.getId());
			extraServiceDuration += returnTravelTime;
			//}
			if(totalWorkHours < extraServiceDuration || extraCustomerSize == 0){
				//list.removeIf(s -> s.equals("acbd"));
				if(jsonTempExtraCustomerList.size() >= 1){
					jsonTempExtraCustomerList.remove(jsonTempExtraCustomerList.size()-1);
				}
				jsonVehicleExtraRoute = new JsonVehicleRoute();
				jsonVehicleExtraRoute.setDepotLocationName(depotLocation.getName());
				jsonVehicleExtraRoute.setDepotLatitude(depotLocation.getLatitude());
				jsonVehicleExtraRoute.setDepotLongitude(depotLocation.getLongitude());
				jsonVehicleExtraRoute.setCapacity(vehicle.getCapacity());
				jsonVehicleExtraRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
				jsonVehicleExtraRoute.setDemandTotal(demandTotal);
				jsonVehicleExtraRoute.setEmployeeName(employeeList.get(i).getFirstName());
				jsonVehicleExtraRoute.setCustomerList(jsonTempExtraCustomerList);
				jsonTempExtraCustomerList = new ArrayList<>();
				jsonTempExtraCustomerList.add(c);
				extraServiceDuration = 0L;
				jsonVehicleRouteList.add(jsonVehicleExtraRoute);
			}else{
				jsonTempExtraCustomerList.add(c);
			}
		}		
	}
	i++;
}*/
