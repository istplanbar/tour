package com.orb.optaplanner.rest.domain.service;

import com.orb.common.client.error.CommonError;
import com.orb.optaplanner.rest.domain.bean.JsonMessage;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;


public interface OptaplannerTSPService {
	
	//Creates or retrieves thh solurtion for the particular Tour
	 JsonVehicleRoutingSolution getSolution(CommonError commonError, String tourId);
	
	 //Solve the problem and shows the routes and shortest routes
	// JsonMessage solve(CommonError commonError);
	 
	 JsonMessage solve(CommonError commonError);

	JsonVehicleRoutingSolution loadSolution(CommonError commonError, String tourId);
	 
}
