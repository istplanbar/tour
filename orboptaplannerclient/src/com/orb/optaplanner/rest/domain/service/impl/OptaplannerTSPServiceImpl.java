package com.orb.optaplanner.rest.domain.service.impl;

import java.awt.Color;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.annotation.PreDestroy;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.optaplanner.core.api.score.buildin.hardsoftlong.HardSoftLongScore;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.domain.ScanAnnotatedClassesConfig;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.examples.vehiclerouting.domain.Customer;
import org.optaplanner.examples.vehiclerouting.domain.Depot;
import org.optaplanner.examples.vehiclerouting.domain.Vehicle;
import org.optaplanner.examples.vehiclerouting.domain.VehicleRoutingSolution;
import org.optaplanner.examples.vehiclerouting.domain.location.DistanceType;
import org.optaplanner.examples.vehiclerouting.domain.location.Location;
import org.optaplanner.examples.vehiclerouting.domain.location.RoadLocation;
import org.optaplanner.swing.impl.TangoColorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.utils.CommonUtils;
import com.orb.employee.bean.Employee;
import com.orb.employee.dao.EmployeeDAO;
import com.orb.facilities.bean.Facilities;
import com.orb.facilities.dao.FacilitiesDAO;
import com.orb.optaplanner.rest.domain.bean.JsonCustomer;
import com.orb.optaplanner.rest.domain.bean.JsonMessage;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoute;
import com.orb.optaplanner.rest.domain.bean.JsonVehicleRoutingSolution;
import com.orb.optaplanner.rest.domain.service.OptaplannerTSPService;
import com.orb.tour.bean.KorfAddress;
import com.orb.tour.bean.Tour;
import com.orb.tour.dao.KorfmannLocationDAO;
import com.orb.tour.dao.TourDAO;

@Service
@SessionScoped
public class OptaplannerTSPServiceImpl implements OptaplannerTSPService{
	private static Log logger = LogFactory.getLog(OptaplannerTSPServiceImpl.class);
	
	
	private static VehicleRoutingSolution vehicleRoutingSolution;
    private static int vehicleListSize = 0;
    private static int capacity = 75;
    private static Map<Long, Location> locationMap;
    private static List<Depot> depotList;
    private static List<Location> customerLocationList;
    private static final String SOLVER_CONFIG =  "org/optaplanner/examples/vehiclerouting/solver/vehicleRoutingSolverConfig.xml" ;
	private SolverFactory<VehicleRoutingSolution> solverFactory;
	private ExecutorService executor;
	private Map<String, VehicleRoutingSolution> sessionSolutionMap;
	private Map<String, Solver<VehicleRoutingSolution>> sessionSolverMap;
	
	Locale theLocale = Locale.US;
	//private NumberFormat NUMBER_FORMAT = new DecimalFormat("##.#####");
	private NumberFormat NUMBER_FORMAT = NumberFormat.getInstance(theLocale);
	private static JsonVehicleRoutingSolution jsonSolution;
	private String tourId;
	@Autowired
	EmployeeDAO  employeeDAO;
	
	@Autowired
	FacilitiesDAO facilitiesDAO;
	
	@Autowired
	TourDAO tourDAO;
	
	@Autowired
	KorfmannLocationDAO korfmannLocationDAO;
	
	@Autowired
	private  HttpServletRequest request;
	
	 @PreDestroy
	 public synchronized void destroy() {
	        for (Solver<VehicleRoutingSolution> solver : sessionSolverMap.values()) {
	            solver.terminateEarly();
	        }
	        executor.shutdown();
	   }
	
	/**
	 * Intializes the solver to define the Routing solution.
	 */
	private void initializeSolver(){
		logger.info("Solver Initialization started");
		//solverFactory = SolverFactory.createEmpty();
		solverFactory = SolverFactory.createFromXmlResource(SOLVER_CONFIG);
		SolverConfig solverConfig = solverFactory.getSolverConfig();
        TerminationConfig terminationConfig = new TerminationConfig();
        terminationConfig.setSecondsSpentLimit(5L);
        solverConfig.setTerminationConfig(terminationConfig);
        sessionSolutionMap = new ConcurrentHashMap<>();
		sessionSolverMap = new ConcurrentHashMap<>();
		executor = Executors.newFixedThreadPool(2); 
        logger.info("Solver Initialized Successfully");
        
	}
	
	/**
	 * This method generates solution object which is feeded to optaplanner map object
	 */
	@Override
	public JsonVehicleRoutingSolution getSolution(CommonError error, String tourId) {
		logger.info("Rest Service is triggered to call the Solution");
		if(sessionSolutionMap == null || !CommonUtils.getValidString(this.tourId).equals(tourId)) { 
			this.tourId = tourId;
			initializeSolver(); 
		}
		// Only 2 because the other examples have their own Executor
		//this.tourId = tourId;
		VehicleRoutingSolution solution = retrieveOrCreateSolution(error, this.tourId, request.getSession().getId());
		logger.info("Solution generation is started ");
		destroy();
		return convertToJsonVRPSolution(error, tourId, solution);
	}
	
	/**
	 * When user wants to solve the shortest problem this method is called and returned with solution object
	 */
	@Override
    public JsonMessage solve(CommonError commonError) {
		logger.info("Solver updates the optimal solution");
        boolean success = solve(commonError, this.tourId, request.getSession().getId());
        return new JsonMessage(success ? "Solving started." : "Solver was already running.");
    }
	
	
	@Override
	public JsonVehicleRoutingSolution loadSolution(CommonError commonError, String tourId) {
		initializeSolver();
		try {
			this.tourId = tourId;
			vehicleRoutingSolution = (VehicleRoutingSolution) generateSolution(tourId, commonError);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return convertToJsonVRPSolution(commonError, tourId, vehicleRoutingSolution);
	}
	
	public synchronized VehicleRoutingSolution retrieveOrCreateSolution(CommonError commonError, String tourId, String sessionId) {
		logger.info("Retrieve or Create Solution  is called");
        vehicleRoutingSolution = sessionSolutionMap.get(sessionId);
        if (vehicleRoutingSolution == null) {
        	try {
				vehicleRoutingSolution = (VehicleRoutingSolution) generateSolution(tourId, commonError);
			}catch (IOException e) {
				logger.info("Error on Creating Solution "+ e.getMessage());
			}
            sessionSolutionMap.put(sessionId, vehicleRoutingSolution);
        }
        logger.info("Solution is Retrieved");
        return vehicleRoutingSolution;
    }

    public synchronized boolean solve(CommonError commonError, String tourId, final String sessionId) {
    	logger.info("Solver is initialized");
        final Solver<VehicleRoutingSolution> solver = solverFactory.buildSolver();
        solver.addEventListener(event -> {
            VehicleRoutingSolution bestSolution = event.getNewBestSolution();
            synchronized (OptaplannerTSPServiceImpl.this) {
                sessionSolutionMap.put(sessionId, bestSolution);
            }
        });
        if (sessionSolverMap.containsKey(sessionId)) {
            return false;
        }
        sessionSolverMap.put(sessionId, solver);
        try {
        	final VehicleRoutingSolution solution = (VehicleRoutingSolution) generateSolution(tourId, commonError);
		
        //final VehicleRoutingSolution solution = retrieveOrCreateSolution(commonError, tourId, sessionId);
        executor.submit((Runnable) () -> {
            VehicleRoutingSolution bestSolution = solver.solve(solution);
            synchronized (OptaplannerTSPServiceImpl.this) {
                sessionSolutionMap.put(sessionId, bestSolution);
                sessionSolverMap.remove(sessionId);
            }
        });}catch (IOException e) {
			logger.info("Error on Creating Solution "+ e.getMessage());
		}
        logger.info("Solver is succesfully sent");
        return true;
    }
    
    
    public synchronized VehicleRoutingSolution generateSolution(String tourId, CommonError commonError) throws IOException {
		logger.info("Generation of solution is started");
		vehicleRoutingSolution = new VehicleRoutingSolution();
    	locationMap = new LinkedHashMap<>();
    	customerLocationList = new ArrayList<>();
		List<Facilities> facilitiesList =  new ArrayList<>();
		KorfAddress korfAddress = new KorfAddress();
		Employee employee = new Employee();
		Tour tour = new Tour();
		if(CommonUtils.getValidString(tourId).isEmpty()){
			commonError.addError("Invalid Tour Details, Facility is not found");
		}
		try{
			tour = tourDAO.getTour(commonError, tourId);
			employee = employeeDAO.getEmployee(commonError, tour.getEmployee());
			korfAddress = korfmannLocationDAO.getKorfAddress(commonError, tour.getKorfmannOfficeLocation());
			facilitiesList = facilitiesDAO.getFacilitiesList(commonError, CommonConstants.Y);
			List<String> selectedFacilitiesIds = tourDAO.getTourFacilitiesList(commonError, tour.getIdentifier());
			if(!selectedFacilitiesIds.isEmpty()){
				facilitiesList = facilitiesList.stream().filter(fa -> selectedFacilitiesIds.contains(fa.getIdentifier())).collect(Collectors.toList());
			}
			if(!korfAddress.getIdentifier().isEmpty()){
				if(korfAddress.getFacilityLatitude() != 0 && korfAddress.getFacilityLongitude() != 0){
					Location loc = new RoadLocation(Long.parseLong(korfAddress.getId()), korfAddress.getFacilityLatitude(), korfAddress.getFacilityLongitude() );
					loc.setName(korfAddress.getName());
					vehicleListSize = 1;
					customerLocationList.add((Location)loc);
					locationMap.put(0L , loc);
				}
			}
			for(Facilities f : facilitiesList){
				if(f.getFacilityLatitude() != 0 && f.getFacilityLongitude() != 0){
					Location loc = new RoadLocation(Long.parseLong(f.getId()), f.getFacilityLatitude(), f.getFacilityLongitude());
					loc.setName(f.getName());
					customerLocationList.add((Location)loc);
					locationMap.put(loc.getId() , loc);
				}
			}
					
		}catch(Exception e){
			logger.info("Exception on Creatin Solutions" + e.getMessage());
		}
		logger.info("Customer Location List is generated here");
		setTravelDistanceMapLocations();
		setCustomerList();
		return vehicleRoutingSolution;
	}
	
	private synchronized void setTravelDistanceMapLocations(){
		logger.info("The distance between Map locations are calculated here");
	   for (int i = 0; i < customerLocationList.size(); i++) {
           RoadLocation startLocation = (RoadLocation) customerLocationList.get(i);
           Map<RoadLocation, Double> travelDistanceMap = new LinkedHashMap<>();
           for (int j = 0; j < customerLocationList.size(); j++) {
           	RoadLocation endLocation = (RoadLocation) customerLocationList.get(j);
				double travelDistance = new DistanceCalc().distance(startLocation.getLatitude(), startLocation.getLongitude(),
           			endLocation.getLatitude(), endLocation.getLongitude(), 'K');
               if (i == j) {
               	if(startLocation.getName().equals(endLocation.getName())) travelDistance = 0.0;
               	if (travelDistance != 0.0) {
                       throw new IllegalStateException("The travelDistance (" + travelDistance + ") should be zero.");
                   }
               } else {
                   travelDistanceMap.put(endLocation, travelDistance);
               }
           }
           startLocation.setTravelDistanceMap(travelDistanceMap);
       }
   }
	
	private synchronized void setCustomerList() throws IOException {
		logger.info("Customer List Creation Started");
        depotList = new ArrayList<>();
        List<Customer> customerList = new ArrayList<>();
        for (int i = 0; i < customerLocationList.size(); i++) {
        	long id = customerLocationList.get(i).getId();
            if (i == 0) {
                Depot depot = new Depot();
                depot.setId(id);
                Location location = locationMap.get(0L);
                if (location == null) {
                    throw new IllegalArgumentException("The depot with id (" + id + ") has no location (" + location + ").");
                }
               depot.setLocation(location);
               depotList.add(depot);
             } else {
                Customer customer = new Customer();
                customer.setId(id);
                Location location = locationMap.get(id);
                if (location == null) {
                    throw new IllegalArgumentException("The customer with id (" + id + ") has no location (" + location + ").");
                }
                customer.setLocation(location);
                customer.setDemand(1);
                customerList.add(customer);
            }
        }
        vehicleRoutingSolution.setId((long)1);
        vehicleRoutingSolution.setName("Solution Test");
        vehicleRoutingSolution.setDistanceType(DistanceType.ROAD_DISTANCE);
        vehicleRoutingSolution.setDistanceUnitOfMeasurement("km");
        vehicleRoutingSolution.setLocationList(customerLocationList);
        vehicleRoutingSolution.setCustomerList(customerList);
        vehicleRoutingSolution.setDepotList(depotList);
        createVehicleList();
        logger.info("Customer List Creation is finished");
    }
    
    
    private synchronized static void createVehicleList() {
    	logger.info("Creating Vehicle is Called");
        List<Vehicle> vehicleList = new ArrayList<>();
        for (int i = 0; i < vehicleListSize; i++) {
            Vehicle vehicle = new Vehicle();
            vehicle.setId((long) i);
            vehicle.setCapacity(customerLocationList.size()/vehicleListSize);
            vehicle.setDepot(depotList.get(i % depotList.size()));
            vehicleList.add(vehicle);
        }
        logger.info("Creating Vehicle is finished");
        vehicleRoutingSolution.setVehicleList(vehicleList);
    }
    
    /**
	 * This methods convers the normal optaplanner solution into Json solution. 
	 * @param solution
	 * @return Json Solution
	 */
	protected JsonVehicleRoutingSolution convertToJsonVRPSolution(CommonError commonError, String tourId, VehicleRoutingSolution solution) {
		logger.info("Converting solution to json is started ");
		JsonVehicleRoutingSolution jsonSolution = new JsonVehicleRoutingSolution();
        jsonSolution.setName(solution.getName());
        List<JsonCustomer> jsonCustomerList = new ArrayList<>(solution.getCustomerList().size());
        for (Customer customer : solution.getCustomerList()) {
            Location customerLocation = customer.getLocation();
            jsonCustomerList.add(new JsonCustomer(customerLocation.getName(),
                    customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand()));
        }
        jsonSolution.setCustomerList(jsonCustomerList);
        List<JsonVehicleRoute> jsonVehicleRouteList = new ArrayList<>();
        TangoColorFactory tangoColorFactory = new TangoColorFactory();
        for (Vehicle vehicle : solution.getVehicleList()) {
            JsonVehicleRoute jsonVehicleRoute = new JsonVehicleRoute();
            Location depotLocation = vehicle.getDepot().getLocation();
            jsonVehicleRoute.setDepotLocationName(depotLocation.getName());
            jsonVehicleRoute.setDepotLatitude(depotLocation.getLatitude());
            jsonVehicleRoute.setDepotLongitude(depotLocation.getLongitude());
            jsonVehicleRoute.setCapacity(vehicle.getCapacity());
            Color color = tangoColorFactory.pickColor(vehicle);
            jsonVehicleRoute.setHexColor(String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue()));
            Customer customer = vehicle.getNextCustomer();
            int demandTotal = 0;
            List<JsonCustomer> jsonVehicleCustomerList = new ArrayList<>();
            while (customer != null) {
                Location customerLocation = customer.getLocation();
                demandTotal += customer.getDemand();
                jsonVehicleCustomerList.add(new JsonCustomer(customerLocation.getName(),
                        customerLocation.getLatitude(), customerLocation.getLongitude(), customer.getDemand()));
                customer = customer.getNextCustomer();
            }
            jsonVehicleRoute.setDemandTotal(demandTotal);
            jsonVehicleRoute.setCustomerList(jsonVehicleCustomerList);
            jsonVehicleRouteList.add(jsonVehicleRoute);
        }
        jsonSolution.setVehicleRouteList(jsonVehicleRouteList);
        HardSoftLongScore score = solution.getScore();
        jsonSolution.setFeasible(score != null && score.isFeasible());
        jsonSolution.setDistance(solution.getDistanceString(NUMBER_FORMAT));
        logger.info("Converting solution to json is finished ");
        return jsonSolution;
	}


	
	

}
