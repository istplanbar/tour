package com.orb.optaplanner.rest.domain.service.impl;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.tour.constants.TourConstants;

public class DistanceCalc {
	private static Log logger = LogFactory.getLog(DistanceCalc.class);

	static double lat1;
	static double lon1;
	static double lat2;
	static double lon2;
	private static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#,##0.00");
	
	public Long getTravelDistanceTime(CommonError commonError, double lat1, double lon1, double lat2, double lon2, String apiKey){
		Long travelTime = 0L; 
		try {
			GeoApiContext context = new GeoApiContext.Builder().disableRetries().apiKey(apiKey).build();
	        DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(context); 
	        DistanceMatrix trix = req.origins(new LatLng(lat1,lon1))
	                .destinations(new LatLng(lat2,lon2))
	                .mode(TravelMode.DRIVING)
	                .language("de-DE")
	                .await();                                                                                                         
	        travelTime = (trix.rows[0].elements[0].duration.inSeconds)/60;
	    } catch(ApiException e){
		     logger.info(e.getMessage());
		     commonError.addError("Google Maps API fehler: "+e.getMessage());
		} catch(Exception e){
		     logger.info(e.getMessage());
		     commonError.addError("Google Maps API fehler: "+e.getMessage());
		}
		
	    return travelTime * 1000;
	}
	
	public double distance( double lat1, double lon1, double lat2, double lon2, char unit) {
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  if (unit == 'K') {
		    dist = dist * 1.609344;
		  } else if (unit == 'N') {
		  dist = dist * 0.8684;
		    }
		  return (dist);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts decimal degrees to radians             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		private static double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}
		
	
}
