package com.orb.optaplanner.rest.domain.bean;

import java.util.List;

import com.orb.taskplanner.bean.Task;

public class JsonCustomer {
	 	
		protected long id;
	    protected String locationName;
	    protected double latitude;
	    protected double longitude;
	    protected int demand;
	    protected List<Task> tasksList;
	    private Long readyTime;
	    
	    public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public List<Task> getTasksList() {
			return tasksList;
		}

		public void setTasksList(List<Task> tasksList) {
			this.tasksList = tasksList;
		}

		public JsonCustomer() {
	    }

		public JsonCustomer(String locationName, double latitude, double longitude, int demand) {
	        this.locationName = locationName;
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.demand = demand;
	    }
		
		public JsonCustomer(long id, String locationName, double latitude, double longitude, int demand, List<Task> tasksList) {
			this.id = id;
	        this.locationName = locationName;
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.demand = demand;
	        this.tasksList = tasksList;
	    }
		
	    public JsonCustomer(String locationName, double latitude, double longitude, int demand, List<Task> tasksList) {
	        this.locationName = locationName;
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.demand = demand;
	        this.tasksList = tasksList;
	    }

		public String getLocationName() {
			return locationName;
		}

		public void setLocationName(String locationName) {
			this.locationName = locationName;
		}

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		public int getDemand() {
			return demand;
		}

		public void setDemand(int demand) {
			this.demand = demand;
		}

		public Long getReadyTime() {
			return readyTime;
		}

		public void setReadyTime(Long readyTime) {
			this.readyTime = readyTime;
		}
	    
	      
	    
}
