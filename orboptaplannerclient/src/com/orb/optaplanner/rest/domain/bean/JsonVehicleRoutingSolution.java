package com.orb.optaplanner.rest.domain.bean;


import java.util.List;

import com.orb.taskplanner.bean.Task;


public class JsonVehicleRoutingSolution {

    protected String name;
    protected List<JsonCustomer> customerList;
    protected List<JsonVehicleRoute> vehicleRouteList;
    
	protected Boolean feasible;
    protected String distance;
    
    public List<JsonVehicleRoute> getVehicleRouteList() {
		return vehicleRouteList;
	}

	public void setVehicleRouteList(List<JsonVehicleRoute> vehicleRouteList) {
		this.vehicleRouteList = vehicleRouteList;
	}

	public void setName(String name) {
        this.name = name;
    }

   public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

	public List<JsonCustomer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<JsonCustomer> customerList) {
		this.customerList = customerList;
	}

	public Boolean getFeasible() {
		return feasible;
	}

	public void setFeasible(Boolean feasible) {
		this.feasible = feasible;
	}

	public String getName() {
		return name;
	}
    
    

}
