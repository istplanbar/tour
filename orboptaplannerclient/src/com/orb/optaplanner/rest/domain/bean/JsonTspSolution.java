package com.orb.optaplanner.rest.domain.bean;


import java.util.List;

import org.optaplanner.examples.tsp.domain.Domicile;

public class JsonTspSolution {

    protected String name;
    protected List<JsonVisit> visitList;
    protected Domicile domicile;
    protected String distance;
    protected List<JsonVehicleRoute> vehicleRouteList;
    
    public List<JsonVehicleRoute> getVehicleRouteList() {
		return vehicleRouteList;
	}

	public void setVehicleRouteList(List<JsonVehicleRoute> vehicleRouteList) {
		this.vehicleRouteList = vehicleRouteList;
	}

	public List<JsonVisit> getVisitList() {
		return visitList;
	}

	public void setVisitList(List<JsonVisit> visitList) {
		this.visitList = visitList;
	}

	public Domicile getDomicile() {
		return domicile;
	}

	public void setDomicile(Domicile domicile) {
		this.domicile = domicile;
	}
    
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

   public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

}
