package com.orb.optaplanner.rest.domain.bean;

public class JsonMessage {

    protected String text;

    public JsonMessage() {
    }

    public JsonMessage(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
