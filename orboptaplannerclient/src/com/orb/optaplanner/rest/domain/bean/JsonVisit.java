package com.orb.optaplanner.rest.domain.bean;

public class JsonVisit {
	
	protected String name = null;
    protected double latitude;
    protected double longitude;
	
    
    public JsonVisit(){};
    
    public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public JsonVisit(String name, double latitude, double longitude) {
		super();
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}
    
    

	
}
