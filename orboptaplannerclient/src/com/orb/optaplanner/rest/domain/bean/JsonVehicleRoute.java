package com.orb.optaplanner.rest.domain.bean;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import com.orb.taskplanner.bean.Task;

@XmlRootElement
public class JsonVehicleRoute {

    protected String depotLocationName;
    protected double depotLatitude;
    protected double depotLongitude;
    protected String hexColor;
    protected int capacity;
    protected int demandTotal;
    protected List<JsonCustomer> customerList;
    protected String employeeName;
    protected String dayOfTour;
    protected Date tourDate;
    protected Long tourDuration;
    
    
	public Long getTourDuration() {
		return tourDuration;
	}
	public void setTourDuration(Long tourDuration) {
		this.tourDuration = tourDuration;
	}
	public Date getTourDate() {
		return tourDate;
	}
	public void setTourDate(Date tourDate) {
		this.tourDate = tourDate;
	}
	public String getDayOfTour() {
		return dayOfTour;
	}
	public void setDayOfTour(String dayOfTour) {
		this.dayOfTour = dayOfTour;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public List<JsonCustomer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<JsonCustomer> customerList) {
		this.customerList = customerList;
	}

	public String getDepotLocationName() {
        return depotLocationName;
    }

    public void setDepotLocationName(String depotLocationName) {
        this.depotLocationName = depotLocationName;
    }

    public double getDepotLatitude() {
        return depotLatitude;
    }

    public void setDepotLatitude(double depotLatitude) {
        this.depotLatitude = depotLatitude;
    }

    public double getDepotLongitude() {
        return depotLongitude;
    }

    public void setDepotLongitude(double depotLongitude) {
        this.depotLongitude = depotLongitude;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getDemandTotal() {
		return demandTotal;
	}

	public void setDemandTotal(int demandTotal) {
		this.demandTotal = demandTotal;
	}
    
    
}
