package com.orb.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author
 *
 */
public class ORBUtil {

	/**
	 * @param str_date
	 * @return
	 */
	public static String formatDate(String str_date) {
		try {
			if (null == str_date)
				return null;
			DateFormat oldformatter = new SimpleDateFormat("yyyy-mm-dd");
			DateFormat newformatter = new SimpleDateFormat("dd.mm.yyyy");
			Date date;
			date = (Date) oldformatter.parse(str_date);
			return newformatter.format(date);

		} catch (Exception exp) {
			return str_date;

		}
	}

	/**
	 * @param str_date
	 * @return
	 */
	public static String reformatDate(String str_date) {
		try {
			if (null == str_date)
				return null;
			DateFormat oldformatter = new SimpleDateFormat("dd.mm.yyyy");
			DateFormat newformatter = new SimpleDateFormat("yyyy-mm-dd");
			Date date;
			date = (Date) oldformatter.parse(str_date);
			return "'" + newformatter.format(date) + "'";

		} catch (Exception exp) {
			return str_date;

		}
	}

	
}
