package com.orb.log.appender;



import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.orb.startup.loader.ORBConstants;

/**
 * @author 
 *
 */
public class ORBLogger {

	// Static Block
	static {
		try {		
			String fileName = ORBConstants.ORB_LOG_PROPERTIES;			
			PropertyConfigurator.configure(fileName);
			genericLogger = Logger.getLogger("GenericLog");

		} catch (Exception e) {
		}
	}

	// Logger instances for ORB log
	private static Logger genericLogger;

	
	public static ORBLogger getLogger() {
		ORBLogger log = new ORBLogger();
		return log;
	}


	public static void logException(String userId, Throwable ex) {
		genericLogger.error(userId, ex);
	}

	/**
	 * This method is to throw the error stack trace on to the logger file.
	 * 
	 */
	public static void logException(Throwable ex) {
		try {
			String userId = "";
			genericLogger.error(userId, ex);
		} catch (Exception e) {
		}
	}

	public static void log(String userId,String message){
		genericLogger.debug("[ " + userId + " ] : "+message);
	}
	
	public static void log(String message){
		genericLogger.debug(message);
	}
}