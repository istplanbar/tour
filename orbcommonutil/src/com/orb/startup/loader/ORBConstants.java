package com.orb.startup.loader;

/**
 * @author 
 *
 */
public class ORBConstants implements java.io.Serializable{
	
	private static ConfigFileReader conffile = null;
	public static final String ORB_CONFIG_FILE_NAME = "Config.properties";
	
	public static final String ORB_LOG_PROPERTIES;
	public static final String ORB_DB_JNDINAME;
	
		
	static {
		conffile = ConfigFileReader.getInstance();
		
		ORB_LOG_PROPERTIES=conffile.getConfigDetails("ORB_LOG_PROPERTIES");
		ORB_DB_JNDINAME=conffile.getConfigDetails("ORB_DB_JNDINAME");
		}
	

}
