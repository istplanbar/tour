package com.orb.startup.loader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author 
 *
 */
public class ConfigFileReader {

	private static ConfigFileReader instance = null;
	private Properties ConfigList = null;

	FileInputStream fis = null;

	/**
	 * 
	 */
	private ConfigFileReader() {
		init(ORBConstants.ORB_CONFIG_FILE_NAME);
	}

	/**
	 * Method to call the subfunction to load the configuraiton data in to
	 * property object.
	 * 
	 * @param String
	 *            - Converter configuration file name
	 */
	
	public void init(String ConfigFileName) {
		String BaseConfigPath = System.getProperty("Config.dir");
		if (BaseConfigPath == null)
			BaseConfigPath = "/opt/orb/properties/";
		
		readConfigFile(BaseConfigPath + ConfigFileName);
	}

	/**
	 * Method to load the Converter configuration message into java properties
	 * object.
	 * 
	 * @param String
	 *            - ConfigFileName - Converter configuration file name
	 */
	private void readConfigFile(String ConfigFileName) {
		fis = null;
		ConfigList = new Properties();
		try {
			fis = new FileInputStream(ConfigFileName);
			ConfigList.load(fis);
		} catch (IOException e) {
		} finally {
			closeInputStream(fis);
		}
	}

	/**
	 * Method to close InputStream object.
	 * 
	 * @param InputStream
	 */
	private void closeInputStream(InputStream is) {
		if (is != null) {
			try {
				is.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Method to get the instance of this class.
	 */
	public static ConfigFileReader getInstance() {
		if (instance == null) {
			instance = new ConfigFileReader();
		}
		return instance;
	}

	/**
	 * Method to return converter configuration details
	 * 
	 * @param String
	 *            - Converter config name
	 * @return String - Converter config value
	 */
	public String getConfigDetails(String name) {
		return ConfigList.getProperty(name) == null ? null : ConfigList
				.getProperty(name).trim();
		// return ConfigList.getProperty(name);
	}

}
