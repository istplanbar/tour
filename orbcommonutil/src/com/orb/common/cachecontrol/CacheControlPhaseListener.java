package com.orb.common.cachecontrol;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;
 
public class CacheControlPhaseListener implements PhaseListener{
    /**
	 * 
	 */
	private static final long serialVersionUID = -1069330921678792985L;

	public PhaseId getPhaseId(){
        return PhaseId.RENDER_RESPONSE;
    }
 
    public void afterPhase(PhaseEvent event){
    }
 
    public void beforePhase(PhaseEvent event){
        FacesContext facesContext = event.getFacesContext();
        HttpServletResponse response = (HttpServletResponse) facesContext
                .getExternalContext().getResponse();
        response.addHeader("Pragma", "no-cache");
        response.addHeader("Cache-Control", "no-cache");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "must-revalidate");
        response.setDateHeader("Expires", 0);
    }
}