package com.orb.admin.layoutadmnistration.model;

public class Layout {
	private Integer layoutId;
	private String layoutName;
	private Integer submenuId;
	private String tableName;
	private String fieldName;
	private Integer mainMenuId;
	private String permissionName;
	
	public Integer getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getLayoutName() {
		return layoutName;
	}
	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}
	public Integer getSubmenuId() {
		return submenuId;
	}
	public void setSubmenuId(Integer submenuId) {
		this.submenuId = submenuId;
	}
	public Integer getMainMenuId() {
		return mainMenuId;
	}
	public void setMainMenuId(Integer mainMenuId) {
		this.mainMenuId = mainMenuId;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

}
