package com.orb.admin.mailtemplates.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.mailtemplates.model.MailTemplates;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class MailTemplatesDAO extends AbstractDAO{
	
	private static Log logger = LogFactory.getLog(MailTemplatesDAO.class);
		
		/**
		 * This method will return list of MailTemplates from DB
		 * @param commonError
		 * @param activeFlag
		 * @return
		 */
	   public List<MailTemplates> getTemplateList( CommonError commonError, String activeFlag) {
			logger.info(" ==> getTemplateList START <== ");
			List<MailTemplates> templateList = null;
			try{
				StringBuffer qry = new StringBuffer("SELECT ");
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("ID") );qry.append( "," );
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("IDENTIFIER") );qry.append( "," );
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("NAME") );qry.append( "," );
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("CONTENT") );qry.append( "," );
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("ACTIVE") );
				qry.append(" FROM ");qry.append(AdminDBConstants.TBL_MAIL_TEMPLATES);qry.append(" WHERE ");
				qry.append(AdminDBConstants.MAIL_TEMPLATES.get("ACTIVE"));qry.append(" = ? ");
				templateList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag},new BeanPropertyRowMapper(MailTemplates.class) );
			}
			catch( Exception e ){
				logger.error(" Exception occured when tries to fetch templateList. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> getMailTemplatesList END <== ");
			return templateList;
		}	
		
		/**
		 * This method used to get a particular MailTemplates from DB
		 * @param commonError
		 * @param Id
		 * @return
		 */
		public MailTemplates getMailTemplates(CommonError commonError, String id) {
			logger.info(" ==> Get a MailTemplates START <== ");
			MailTemplates mailTemplates = null;
			MailTemplates lockedHist = null;
			MailTemplates unlockedHist = null;
			try{
				StringBuffer newQry = new StringBuffer("SELECT ");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("ID"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("IDENTIFIER"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("NAME"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("CONTENT"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("CREATE_BY"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("CREATE_TIME"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_BY"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_TIME"));newQry.append(",");
				newQry.append(AdminDBConstants.MAIL_TEMPLATES.get("ACTIVE") );
				newQry.append(" FROM ");newQry.append(AdminDBConstants.TBL_MAIL_TEMPLATES);newQry.append(" WHERE ");
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("IDENTIFIER") );newQry.append( " = ? " );
				mailTemplates = (MailTemplates)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{id}, new BeanPropertyRowMapper(MailTemplates.class) );
				
				lockedHist = getUserAction(commonError, mailTemplates.getIdentifier(),CommonConstants.LOCKED);
				if(lockedHist !=  null){
					mailTemplates.setLockedBy( lockedHist.getUpdateBy());
					mailTemplates.setLockedTime(  lockedHist.getUpdateTime());
				}
				
				unlockedHist = getUserAction(commonError, mailTemplates.getIdentifier(),CommonConstants.UNLOCKED);
				if(unlockedHist !=  null){
					mailTemplates.setUnlockedBy( unlockedHist.getUpdateBy());
					mailTemplates.setUnlockedTime( unlockedHist.getUpdateTime());
				}	
			
			}
			catch( Exception e ){
				logger.error(" Exception occured when tries to fetch MailTemplates list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> Get a MailTemplates END <== ");
			return mailTemplates;
		}
		
		/**
		 * Get user actions from table for particular table
		 * @param commonError
		 * @param id
		 * @param action
		 * @return
		 */
	     private MailTemplates getUserAction(CommonError commonError,String id, String action){
	 		logger.info(" ==> getUserAction START <== ");
			MailTemplates historyInfo = null;
			try{
			StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
			for( String column : AdminDBConstants.MAIL_TEMPLATES_HISTORY.keySet() ){
				if(!column.equals("CORE_ORB_MAIL_TEMPLATES_HISTORY")){
					newQryHist.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get(column) );newQryHist.append( "," );
			    }
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(" FROM ");newQryHist.append(AdminDBConstants.TBL_MAIL_TEMPLATES_HISTORY);newQryHist.append(" WHERE ");newQryHist.append( AdminDBConstants.MAIL_TEMPLATES.get("IDENTIFIER") );
			newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
			historyInfo = (MailTemplates)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(MailTemplates.class) );
			
			}
			catch( Exception e ){
				logger.error(" No user action made before ==> " + e.getMessage() );
			}
			logger.info(" ==> getUserAction END <== ");
			return historyInfo;
			
		}
		
		/**
		 * This method will activate or deactivate the MailTemplates
		 * @param commonError
		 * @param loginUser
		 * @param i
		 * @param s
		 */
		public void updateActiveStatus( CommonError commonError, User loginUser, List<String> ids, String s ){
			logger.info(" ==> updateActiveStatus START <== ");
			NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
			try{
				if(s.equals(CommonConstants.Y))
					insertMailTemplatesHistory( commonError, loginUser,ids, CommonConstants.UNLOCKED);
				else
					insertMailTemplatesHistory( commonError, loginUser,ids, CommonConstants.LOCKED);
				Map<String, Object> params = new HashMap<>();
		        params.put("ids", ids);
				StringBuffer qry = new StringBuffer();
				qry.append(" UPDATE ");qry.append( AdminDBConstants.TBL_MAIL_TEMPLATES );qry.append(" SET ");
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("ACTIVE") );qry.append(" = ");
				qry.append("'"+s+"'");
				qry.append(" WHERE ");
				qry.append( AdminDBConstants.MAIL_TEMPLATES.get("ID") );qry.append(" IN ");
				qry.append(" (:ids) ");
				db.update(qry.toString(),params);
			}
			catch( Exception e ){
				logger.info("Error when updateActiveStatus of a MailTemplates.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> updateActiveStatus END <== ");
		}
		
		/**
		 * This method delete the MailTemplates into DB
		 * @param commonError
		 * @param loginUser
		 * @param i
		 * @return 
		 */
	    public void deleteMailTemplates(CommonError commonError, User loginUser, List<String> ids ) {
			logger.info(" ==> Delete MailTemplates START <== ");
			NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
			try{
				insertMailTemplatesHistory( commonError, loginUser,ids,CommonConstants.DELETED);
				Map<String, Object> params = new HashMap<>();
		        params.put("ids", ids);
				StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
				newQry.append(AdminDBConstants.TBL_MAIL_TEMPLATES);newQry.append(" WHERE ");
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("ID") );newQry.append( " IN " );
				newQry.append(" (:ids) ");
				db.update(newQry.toString(),params);
			}
			catch( Exception e ){
				logger.error(" Exception occured when tries to fetch MailTemplates list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> Delete MailTemplates END <== ");

		}
		
	     /**
	      * This method inserts new MailTemplates into DB
	      * @param commonError
	      * @param loginUser
	      * @param i
	      * @return 
	      */
	    public void addMailTemplates(CommonError commonError, User loginUser,MailTemplates mt) {
			logger.info(" ==> Insert MailTemplates START <== ");
			StringBuffer newQry = new StringBuffer();
			try{
			String nextRowId = getNextRowId( AdminDBConstants.TBL_MAIL_TEMPLATES, AdminDBConstants.mailTemplatesDBConfig.get("rowPrefix") );
			mt.setIdentifier(nextRowId);
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( AdminDBConstants.TBL_MAIL_TEMPLATES );newQry.append("( ");
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("NAME"));newQry.append(",");
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("CONTENT") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("CREATE_BY") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<>();
			args.add(mt.getIdentifier());
			args.add(mt.getName());
			args.add(mt.getContent());args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );
			}catch( Exception e ){
				logger.info("Error when Inserting MailTemplates.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> Insert MailTemplates END <== ");
		}

	      /**
	       * This method updates existing MailTemplates in DB
	       * @param commonError
	       * @param loginUser
	       * @param i
	       * @return
	       */
	    public void updateMailTemplates(CommonError commonError, User loginUser, MailTemplates mt) {
	    	   logger.info(" ==> Update MailTemplates START <== ");
	    	   StringBuffer newQry = new StringBuffer();
	    	   List<String> ids=new ArrayList<>();
			try{
				insertMailTemplatesHistory( commonError, loginUser,ids, CommonConstants.UPDATED );
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("UPDATE ");newQry.append( AdminDBConstants.TBL_MAIL_TEMPLATES );newQry.append(" SET ");
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("NAME") );newQry.append( " = ?," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("CONTENT") );newQry.append( " = ?," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_BY") );newQry.append( " = ?," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("UPDATE_TIME") );newQry.append( " = ?" );
				newQry.append(" WHERE ");newQry.append( AdminDBConstants.MAIL_TEMPLATES.get("ID") );newQry.append(" = ? ");
				List<Object> args = new ArrayList<>();
				args.add(mt.getName());
				args.add(mt.getContent());
				args.add(loginUser.getUserId());
				args.add(date);args.add(mt.getId());
				getJdbcTemplate().update( newQry.toString(),args.toArray() );
			}
			catch( Exception e ){
				logger.info("Error when Updating MailTemplates.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==>  Update MailTemplates END <== ");

		}
		
	       /**
	        * This method inserts updated MailTemplates into MailTemplates history table
	        * @param commonError
	        * @param loginUser
	        * @param id
	        * @param action
	        * @return 
	        */
	    public void insertMailTemplatesHistory(CommonError commonError, User loginUser,List<String> ids, String action) throws Exception {
	        	logger.info(" ==> Insert MailTemplates History START <== ");
	        	for(String id:ids){
			    logger.info("MailTemplates Id ==> " + ids );
			    StringBuffer newQry = new StringBuffer();
			    StringBuffer newQryHist = new StringBuffer("SELECT ");
			    for( String column : AdminDBConstants.MAIL_TEMPLATES.keySet() ){
			    	
			    	if(!column.equals("CORE_ORB_MAIL_TEMPLATES")){
						newQryHist.append( AdminDBConstants.MAIL_TEMPLATES.get(column) );newQryHist.append( "," );
				    }
				
			    }
			    newQryHist.setLength(newQryHist.length()-1);
			    newQryHist.append(" FROM ");newQryHist.append(AdminDBConstants.TBL_MAIL_TEMPLATES);newQryHist.append(" WHERE ");newQryHist.append( AdminDBConstants.MAIL_TEMPLATES.get("ID") );newQryHist.append( "=?" );
			    MailTemplates s = (MailTemplates)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(MailTemplates.class) );
			
			try{
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("INSERT INTO ");newQry.append( AdminDBConstants.TBL_MAIL_TEMPLATES_HISTORY);newQry.append("( ");
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("IDENTIFIER") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("NAME") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("CONTENT") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("CREATE_BY") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("CREATE_TIME") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("UPDATE_BY") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("UPDATE_TIME") );newQry.append( "," );
				newQry.append( AdminDBConstants.MAIL_TEMPLATES_HISTORY.get("ACTION") );
				newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
				List<Object> args = new ArrayList<>();
				args.add(s.getIdentifier());args.add(s.getName());
				args.add(s.getContent());
				args.add(loginUser.getUserId());args.add(date);
				args.add(loginUser.getUserId());args.add(date);args.add(action);
				getJdbcTemplate().update( newQry.toString(),args.toArray() );

			}catch( Exception e ){
				logger.info("Error when Inserting MailTemplatesHistory.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
	    		}
			logger.info(" ==> Insert MailTemplates History END <== ");
		}

		public String genNextRowId(CommonError qmsError) {
			return getNextRowId( AdminDBConstants.TBL_MAIL_TEMPLATES, AdminDBConstants.mailTemplatesDBConfig.get("rowPrefix") );

		}


}


