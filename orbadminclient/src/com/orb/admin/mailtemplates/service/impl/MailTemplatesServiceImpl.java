package com.orb.admin.mailtemplates.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.mailtemplates.dao.MailTemplatesDAO;
import com.orb.admin.mailtemplates.model.MailTemplates;
import com.orb.admin.mailtemplates.service.MailTemplatesService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;



@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )

public class MailTemplatesServiceImpl implements MailTemplatesService {
	private static Log logger = LogFactory.getLog(MailTemplatesServiceImpl.class);
	
	@Autowired
	MailTemplatesDAO mailTemplatesDAO;
    
	/**
	 * This method calls DAO to get all active mail templates level from DB
	 */
	@Override
	public List<MailTemplates> getActiveTemplateList(CommonError commonError) {
		return mailTemplatesDAO.getTemplateList(commonError,CommonConstants.Y);
	}
    
	/**
	 * This method calls DAO to get all Inactive mail templates level from DB
	 */
	@Override
	public List<MailTemplates> getInactiveTemplateList(CommonError commonError) {
		return mailTemplatesDAO.getTemplateList(commonError,CommonConstants.N);
	}
    
	/**
	 * This method calls DAO to get existing mail templates from DB
	 */
	@Override
	public MailTemplates getMailTemplates(CommonError commonError, String id) {
		return mailTemplatesDAO.getMailTemplates(commonError,id);
	}
    
	/**
	 * This method will interact with DAO to de-activate a mail templates in DB
	 */
	@Override
	public void lockMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates) {
		List<String> ids=new ArrayList<>();
		if( selectedTemplates == null || selectedTemplates.isEmpty() ){
			return;
		}
		for( MailTemplates mailTemplates : selectedTemplates ){
			ids.add(mailTemplates.getId());
		}
		mailTemplatesDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
		
	}
    
	/**
	 * This method will interact with DAO to activate a mail templates in DB
	 */
	@Override
	public void unlockMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates) {
		List<String> ids=new ArrayList<>();
		if( selectedTemplates == null || selectedTemplates.isEmpty() ){
			return;
		}
		for( MailTemplates mailTemplates : selectedTemplates ){
			ids.add(mailTemplates.getId());
		}
		mailTemplatesDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );
	}
    
	/**
	 * This method will interact with DAO to delete a mail templates in DB
	 */
	@Override
	public void deleteMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates) {
		List<String> ids=new ArrayList<>();
		if( selectedTemplates == null || selectedTemplates.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( MailTemplates mailTemplates: selectedTemplates ){
			ids.add(mailTemplates.getId());	
		}
		mailTemplatesDAO.deleteMailTemplates(commonError, loginUser, ids);
	}
    
	/**
	 * This method will interact with DAO to insert a mail templates in DB
	 */
	@Override
	public void addMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates) {
		mailTemplatesDAO.addMailTemplates(commonError, loginUser, mailTemplates );
	}
    
	/**
	 * This method will interact with DAO to update a mail templates in DB
	 */
	@Override
	public void updateMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates) {
		mailTemplatesDAO.updateMailTemplates(commonError, loginUser, mailTemplates );
	}
    
	/**
	 * This method calls DAO to de-activate viewed mail templates
	 */
	@Override
	public void lockViewedMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates) {
		List<String> ids=new ArrayList<>();
		ids.add(mailTemplates.getId());
		mailTemplatesDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
	}
    
	/**
	 * This method calls DAO to activate viewed mail templates
	 */
	@Override
	public void unlockViewedMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates) {
		List<String> ids=new ArrayList<>();
		ids.add(mailTemplates.getId());
		mailTemplatesDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );
	}
    
	/**
	 * This method calls DAO to deletes particular viewed mail templates
	 */
	@Override
	public void deleteViewedTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates) {
		List<String> ids=new ArrayList<>();
		ids.add(mailTemplates.getId());
		mailTemplatesDAO.deleteMailTemplates(commonError, loginUser, ids);
	}

}
