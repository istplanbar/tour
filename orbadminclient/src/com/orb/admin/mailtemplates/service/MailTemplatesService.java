package com.orb.admin.mailtemplates.service;


import java.util.List;

import com.orb.admin.mailtemplates.model.MailTemplates;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;



public interface MailTemplatesService {
	
	public List<MailTemplates> getActiveTemplateList(CommonError commonError);
	public List<MailTemplates> getInactiveTemplateList(CommonError commonError);
	public MailTemplates getMailTemplates( CommonError commonError, String id );
	public void lockMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates);
	public void unlockMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates);
	public void deleteMailTemplates(CommonError commonError, User loginUser, List<MailTemplates> selectedTemplates);
	public void addMailTemplates( CommonError commonError, User loginUser, MailTemplates mailTemplates);
	public void updateMailTemplates( CommonError commonError, User loginUser, MailTemplates mailTemplates);
	public void lockViewedMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates);
	public void unlockViewedMailTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates);
	public void deleteViewedTemplates(CommonError commonError, User loginUser, MailTemplates mailTemplates);
}
