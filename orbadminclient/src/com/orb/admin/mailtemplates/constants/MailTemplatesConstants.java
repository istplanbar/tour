package com.orb.admin.mailtemplates.constants;

public class MailTemplatesConstants {
	
	public static final String LIST_PAGE = "/common/process/admin/loadMailTemplates.html";
	public static final String VIEW_PAGE = "/common/process/admin/loadMailTemplatesView.html";
	public static final String EDIT_PAGE = "/common/process/admin/loadMailTemplatesAddUpdate.html";
	public static final String LIST_HTML = "admin/mailtemplates/list.xhtml";
	public static final String EDIT_HTML = "admin/mailtemplates/addUpdate.xhtml";
	public static final String VIEW_HTML = "admin/mailtemplates/view.xhtml";
	public static final String SELECTED_MAIL_TEMPLATES_ID = "SELECTED_MAIL_TEMPLATES_ID";
	public static final String SHOW_ACTIVE_MAIL_TEMPLATES = "SHOW_ACTIVE_MAIL_TEMPLATES";
	public static final String MAIL_TEMPLATES_TOGGLER_LIST = "MAIL_TEMPLATES_TOGGLER_LIST";
	public static final String MAIL_TEMPLATES_DELETE_SUCCESS = "mail_templates_delete_success";
	public static final String MAIL_TEMPLATES_UNLOCKED_SUCCESS = "mail_templates_unlocked_success";
	public static final String MAIL_TEMPLATES_LOCKED_SUCCESS = "mail_templates_locked_success";
	public static final String MAIL_TEMPLATES_ADDED_SUCCESS = "mail_templates_added_success";
	public static final String MAIL_TEMPLATES_UPDATED_SUCCESS = "mail_templates_updated_success";
	
}
