package com.orb.admin.modulemanagement.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.moduleactivity.constants.ModuleActivityDBConstants;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class ModuleManagementDAO extends AbstractDAO {
	
	private static Log logger = LogFactory.getLog(ModuleManagementDAO.class);
	
	/**
	 * Get All Main modules from Table
	 *  @param error
	 *  @param roleId
	 *  @return
	 **/
	public List<MetaTable> getMainModuleList(CommonError error, int roleId) {
		  String qry = "select PAGE_ID,PAGE_DESC,IMAGE_NAME, URL from CORE_ORB_META_TABLE where PARENT_PAGE = 1 and PAGE_ID in (select PARENT_PAGE from CORE_ORB_META_TABLE where PAGE_ID in (select SUBMENU_ID from CORE_ORB_ROLE_DESC where ROLE_ID = ? ) union all select SUBMENU_ID from CORE_ORB_ROLE_DESC where ROLE_ID = ?)";
		  List<MetaTable> filteredMainModuleModuleList = new ArrayList<>();
	      filteredMainModuleModuleList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new Object[]{roleId,roleId},  new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		  return filteredMainModuleModuleList;
	}
	
	/**
	 * Method  for get MetaTable list
	 * @param error
	 * @param activeFlag
	 * @return
	 */
	public List<MetaTable> getMetaTableList(CommonError error,String activeFlag) {
		  logger.info(" ==> getMetaTableList START <== ");
		  List<MetaTable>  metaTableList = null;
		  try{
		       StringBuffer qry = new StringBuffer("SELECT ");
		       qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID") ); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("URL")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("ICON")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("POPUP_MENU")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PRINT_CONFIG"));qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG"));
		       qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			   qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(" = ");qry.append(" 1 ");qry.append(" AND ");qry.append("ACTIVE");qry.append(" = ? ");
		       metaTableList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(MetaTable.class) );
		  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch metaTable list. Exception ==> " + e.getMessage() );
			error.addError(CommonConstants.DB_ERROR);
		  }
		    logger.info(" ==> getMetaTableList END <== ");
		    return metaTableList;
    }
	
	/**
	 * Method  for get All MetaTable list
	 * @param error
	 * @return
	 */
    public List<MetaTable> getAllMetaTableList(CommonError error) {
		  logger.info(" ==> get All MetaTableList START <== ");
		  List<MetaTable>  metaTableList = null;
		  try{
		       StringBuffer qry = new StringBuffer("SELECT ");
		       qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID") ); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("URL")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("ICON")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("POPUP_MENU")); qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("PRINT_CONFIG"));qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG"));qry.append(",");
		       qry.append(AdminDBConstants.META_TABLE.get("ACTIVE"));
		       qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);
			   metaTableList = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(MetaTable.class) );
		  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch metaTable list. Exception ==> " + e.getMessage() );
			error.addError(CommonConstants.DB_ERROR);
		  }
		    logger.info(" ==> get All MetaTableList END <== ");
		    return metaTableList;
    }
	
    /**
	 * Method  for get sub module list
	 * @param error
	 * @param pageId
	 * @return
	 */
    public List<MetaTable> getSubModuleList(CommonError error,String pageId) {
		logger.info(" ==> getSubModuleList START <== ");
		List<MetaTable>  metaTableList = null;
		try{
		     StringBuffer qry = new StringBuffer("SELECT ");
		     qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID") ); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("URL")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("ICON")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("POPUP_MENU")); qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("PRINT_CONFIG"));qry.append(",");
		     qry.append(AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG"));
		     qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			 qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(" =? ");
		     metaTableList = getJdbcTemplate().query( qry.toString(),new String[]{pageId}, new BeanPropertyRowMapper(MetaTable.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch sub module list. Exception ==> " + e.getMessage() );
			error.addError(CommonConstants.DB_ERROR);
		}
		     logger.info(" ==> getSubModuleList END <== ");
		     return metaTableList;
    }
    
    /**
	 * Method  for get  module list
	 * @param commonError
	 * @param pageId
	 * @return
	 */
    public MetaTable getMetaTable(CommonError commonError, String pageId) {
		 logger.info(" ==> Get a PAGE START <== ");
		 logger.info("Page Id ==> " + pageId );
		 MetaTable metaTable = null;
		 try{
			  StringBuffer qry = new StringBuffer("SELECT ");
			  qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("URL"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("POPUP_MENU"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("PRINT_CONFIG"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG"));qry.append(",");
			  qry.append(AdminDBConstants.META_TABLE.get("ACTIVE"));
			  qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			  qry.append( AdminDBConstants.META_TABLE.get("PAGE_ID") );qry.append( " = ? " );
			  metaTable = (MetaTable)getJdbcTemplate().queryForObject( qry.toString(),new String[]{pageId}, new BeanPropertyRowMapper(MetaTable.class) );
		 }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Module list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		 }
		    logger.info(" ==> Get a PAGE END <== ");
		    return metaTable;
	}

     /**
	 * Add new module into DB
	 * @param commonError
	 * @param metaTable
	 * @param loginUser
	 */
	public void addModule(CommonError commonError, MetaTable metaTable, User loginUser) {
		 logger.info(" ==> INSERT A NEW MODULE START <== "); 
		 try{
			  String moduleID = getNextRowId(AdminDBConstants.TBL_META_TABLE);
			  StringBuffer queryInsert = new StringBuffer();
			  java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
	          queryInsert.append("INSERT INTO ");
	          queryInsert.append(AdminDBConstants.TBL_META_TABLE);queryInsert.append("( ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("URL"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("ICON"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("POPUP_MENU"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("PRINT_CONFIG"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG"));queryInsert.append(" , ");
	          queryInsert.append( AdminDBConstants.META_TABLE.get("CREATE_BY"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.META_TABLE.get("CREATE_TIME"));queryInsert.append( "," );
	          queryInsert.append( AdminDBConstants.META_TABLE.get("UPDATE_BY"));queryInsert.append( "," );
	          queryInsert.append( AdminDBConstants.META_TABLE.get("UPDATE_TIME"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.META_TABLE.get("ACTIVE"));
	          queryInsert.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ");
	          List<Object> args = new ArrayList<>();
	          args.add(metaTable.getPageDesc());
	          args.add(metaTable.getParentPage());
	          args.add(metaTable.getUrl());
	          args.add(metaTable.getIcon());
	          args.add(metaTable.getImageName());
	          args.add(metaTable.getPopupMenu());
			  args.add(metaTable.getPrintConfig());
			  args.add(metaTable.getChildModuleConfig());
			  args.add(loginUser.getUserId());args.add(date);
			  args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
			  getJdbcTemplate().update( queryInsert.toString(), args.toArray() );
			  if(commonError.getError().isEmpty()){
				  if(metaTable.getActivityList()!=null)
				      for(ModuleActivity modAct : metaTable.getActivityList()){
						  addModuleActivityMap(commonError, modAct.getIdentifier(), moduleID, loginUser);
			          }
		          }
		 }catch( Exception e ){
		      logger.error(" Exception occured when tried to insert new module into meta table. Exception ==> " + e.getMessage() );
			  commonError.addError(CommonConstants.DB_ERROR);
		 }
		      logger.info(" ==> INSERT A NEW MODULE END <== "); 
    }
	
    /**
	 * Crate module and acitivity mapping then store it.
	 * @param commonError
	 * @param activityIdentifier
	 * @param moduleId
	 * @param loginUser
	 */
	public void addModuleActivityMap(CommonError commonError, String activityIdentifier, String moduleId, User loginUser){
	     logger.info(" ==> Add module and activity Mapping START <== ");
	     try{
		      StringBuffer queryInsert = new StringBuffer();
			  java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
	          queryInsert.append("INSERT INTO ");
	          queryInsert.append(AdminDBConstants.TBL_MOD_ACTIVITY_MAPPING);queryInsert.append("( ");
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("PAGE_ID"));queryInsert.append(" , ");
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ACTIVITY"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("CREATE_BY"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("CREATE_TIME"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("UPDATE_BY"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("UPDATE_TIME"));queryInsert.append( "," );
	          queryInsert.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ACTIVE"));
	          queryInsert.append(") VALUES(?,?,?,?,?,?,?) ");
	          List<Object> args = new ArrayList<>();
	          args.add( moduleId );
	          args.add( activityIdentifier );
	          args.add(loginUser.getUserId());args.add(date);
			  args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
	          getJdbcTemplate().update( queryInsert.toString(), args.toArray() );
	     }catch( Exception e ){
			  logger.error(" Exception occured when tries to insert mapping into Mapping table. Exception ==> " + e.getMessage() );
			  commonError.addError(CommonConstants.DB_ERROR);
		 }
	   logger.info(" ==> Add module and activity Mapping END <== ");
	}
	
	/**
	 * Get Module Activities
	 * @param commonError
	 * @param identifiers
	 * @return
	 */
	public List<ModuleActivity> getModuleActivity(CommonError commonError, List<String> identifiers){
		 logger.info(" ==> Get Module Acitivity START <== ");
		 List<ModuleActivity> moduleActivityIds = new ArrayList<>();
		 NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		 try{
			  Map<String, Object> params = new HashMap<>();
		      params.put("ids", identifiers);
			  StringBuffer qry = new StringBuffer("SELECT ");
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVITY_NAME"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("DESCRIPTION"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_BY"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_TIME"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_BY"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_TIME"));qry.append( "," );
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE"));
			  qry.append(" FROM ");qry.append(ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);qry.append(" WHERE ");
			  qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER"));qry.append(" IN ");
			  qry.append(" (:ids) ");
			  moduleActivityIds = (List<ModuleActivity>) db.query( qry.toString(), params, new BeanPropertyRowMapper(ModuleActivity.class) );
		 }catch( Exception e ){
				logger.error(" Exception occured when tries to get mapping from Mapping table. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
		 }
		  logger.info(" ==> Get Module Acitivity START <== ");
		  return moduleActivityIds;
	}

    /**
	 * Update module into DB
	 * @param commonError
	 * @param metaTable
	 * @param moduleId
	 * @param loginUser
	 **/
	public void update(CommonError commonError, MetaTable metaTable,String moduleId, User loginUser) {
		logger.info(" ==> Update MetaTable START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			  newQry.append("UPDATE ");newQry.append( AdminDBConstants.TBL_META_TABLE);newQry.append(" SET ");
			  newQry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("PARENT_PAGE") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("URL") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("ICON") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("IMAGE_NAME") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("POPUP_MENU") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("PRINT_CONFIG") );newQry.append( " = ?," );
			  newQry.append( AdminDBConstants.META_TABLE.get("CHILD_MODULE_CONFIG") );newQry.append( " = ?" );
			  newQry.append(" WHERE ");newQry.append( AdminDBConstants.META_TABLE.get("PAGE_ID") );newQry.append(" = ? ");
			  List<Object> args = new ArrayList<>();
			  args.add(metaTable.getPageDesc());
			  args.add(metaTable.getParentPage());
		      args.add(metaTable.getUrl());
		      args.add(metaTable.getIcon());
		      args.add(metaTable.getImageName());
		      args.add(metaTable.getPopupMenu());
			  args.add(metaTable.getPrintConfig());
			  args.add(metaTable.getChildModuleConfig());
			  args.add(moduleId);
			  getJdbcTemplate().update( newQry.toString(),args.toArray() );
			  if(commonError.getError().isEmpty()){
				deleteModActMapping(commonError, loginUser, moduleId);
				for(ModuleActivity modAct : metaTable.getActivityList())
						addModuleActivityMap(commonError, modAct.getIdentifier(), moduleId, loginUser);
			  }
		}catch( Exception e ){
			logger.info("Error when Update MetaTable.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		    logger.info(" ==>  Update MetaTable END <== ");
		
	}
	
	/**
	 * To delete mapping between module and activity
	 * @param commonError
	 * @param loginUser
	 * @param moduleId
	 **/
	public void deleteModActMapping(CommonError commonError,User loginUser, String moduleId) {
		logger.info(" ==> Delete Modules START <== ");
		 try{
			  StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			  newQry.append(AdminDBConstants.TBL_MOD_ACTIVITY_MAPPING);newQry.append(" WHERE ");
			  newQry.append( AdminDBConstants.MOD_ACTIVITY_MAPPING.get("PAGE_ID") );newQry.append( " = ? " );
			  getJdbcTemplate().update( newQry.toString(),new Object[]{moduleId} );
		 }catch( Exception e ){
			logger.error(" Exception occured when tries to delete modules. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Modules END <== ");
    }
	
	/**
	 * Delete module into DB
	 * @param commonError
	 * @param loginUser
	 * @param ids
	 **/
	public void delete(CommonError commonError,User loginUser, List<Integer> ids) {
		logger.info(" ==> Delete Modules START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			 insertMetaTableHistory( commonError, loginUser,ids,CommonConstants.DELETED);
			 Map<String, Object> params = new HashMap<>();
		        params.put("ids", ids);
	         StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			 newQry.append(AdminDBConstants.TBL_META_TABLE);newQry.append(" WHERE ");
			 newQry.append( AdminDBConstants.META_TABLE.get("PAGE_ID") );newQry.append( " IN " );
			 newQry.append(" (:ids) ");
				db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete modules. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Modules END <== ");
    }
	
	
	/**
 	 * This method will activate or deactivate the module
 	 * @param commonError
 	 * @param loginUser
 	 * @param ids
 	 * @param s
 	 */
 	public void updateActiveStatus( CommonError commonError, User loginUser, List<Integer> ids, String s ){
 		logger.info(" ==> updateActiveStatus START <== ");
 		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
 		try{
 			 if(s.equals(CommonConstants.Y)){
 				insertMetaTableHistory( commonError, loginUser,ids, CommonConstants.UNLOCKED);
 			 }else{
 				insertMetaTableHistory( commonError, loginUser,ids, CommonConstants.LOCKED);
 			 }
 			 Map<String, Object> params = new HashMap<>();
	         params.put("ids", ids);
 			 StringBuffer qry = new StringBuffer();
 			 qry.append(" UPDATE ");qry.append( AdminDBConstants.TBL_META_TABLE );qry.append(" SET ");
 			 qry.append( AdminDBConstants.META_TABLE.get("ACTIVE") );qry.append(" = ");
 			 qry.append("'"+s+"'");
			 qry.append(" WHERE ");
 			 qry.append( AdminDBConstants.META_TABLE.get("PAGE_ID") );qry.append(" IN ");
 			qry.append(" (:ids) ");
			db.update(qry.toString(),params);
 		}
 		catch( Exception e ){
 			logger.info("Error when updateActiveStatus of a module.  " + e.getMessage());
 			commonError.addError(CommonConstants.DB_ERROR);
 		}
 		logger.info(" ==> updateActiveStatus END <== ");
 	}
 	
 	/**
     * This method inserts updated Material into Module  history table
     * @param courseError
     * @param loginUser
     * @param ids
     * @param action
     * @return 
     */
    public void insertMetaTableHistory(CommonError courseError, User loginUser,List<Integer> ids, String action) throws Exception {
		logger.info(" ==> Insert Module Management History START <== ");
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQry.append("SELECT ");
		for( String column :  AdminDBConstants.META_TABLE.keySet() ){
			if(!column.equals("CORE_ORB_META_TABLE")){
			newQry.append( AdminDBConstants.META_TABLE.get(column) );newQry.append( "," );
			}
		}
		newQry.setLength(newQry.length()-1);
		newQry.append(" FROM ");newQry.append(AdminDBConstants.TBL_META_TABLE );newQry.append(" WHERE ");newQry.append(AdminDBConstants.META_TABLE.get("PAGE_ID") );
		newQry.append(" IN ( ");
		for ( int i=0; i<ids.size(); i++ ) {
			newQry.append(" ? ");newQry.append(",");
		}
		newQry.setLength(newQry.length() - 1);
		newQry.append(") ");
		List<MetaTable> metaTableList = (List<MetaTable>)getJdbcTemplate().query(newQry.toString(), ids.toArray(),  new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		try{ 
			 java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			 newQryHist.append("INSERT INTO ");newQryHist.append( AdminDBConstants.TBL_META_TABLE_HISTORY);newQryHist.append("( ");
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("PAGE_DESC") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("PARENT_PAGE") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("URL") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("ICON") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("IMAGE_NAME") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("POPUP_MENU") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("PRINT_CONFIG") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("CHILD_MODULE_CONFIG") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("CREATE_BY") );newQryHist.append( "," );
			 newQryHist.append(AdminDBConstants.META_TABLE_HISTORY.get("CREATE_TIME") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("UPDATE_BY") );newQryHist.append( "," );
			 newQryHist.append( AdminDBConstants.META_TABLE_HISTORY.get("UPDATE_TIME") );newQryHist.append( "," );
			 newQryHist.append(AdminDBConstants.META_TABLE_HISTORY.get("ACTION") );
			 newQryHist.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			 getJdbcTemplate().batchUpdate(newQryHist.toString(), new BatchPreparedStatementSetter() {
			
			 public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setString(1, metaTableList.get(i).getPageDesc());ps.setInt(2, metaTableList.get(i).getParentPage());
				ps.setString(3, metaTableList.get(i).getUrl());ps.setString(4, metaTableList.get(i).getIcon());
				ps.setString(5, metaTableList.get(i).getImageName());ps.setString(6, metaTableList.get(i).getPopupMenu());
				ps.setString(7, metaTableList.get(i).getPrintConfig());
				ps.setString(8, metaTableList.get(i).getChildModuleConfig());
				ps.setString(9, loginUser.getUserId());
				ps.setTimestamp(10, date);ps.setString(11, loginUser.getUserId());
				ps.setTimestamp(12, date);ps.setString(13, action);
			}
			public int getBatchSize() {
				return metaTableList.size();
			}
		});
 
		}catch( Exception e ){
			logger.info("Error when Insert Module Management History.  " + e.getMessage());
			courseError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Module Management History END <== ");
	}
    
    /**
	 * Get Module Activities Mapping List
	 * @param commonError
	 * @return 
	 */
    public List<MetaTable> getModuleActivityMappingList(CommonError commonError){
		 logger.info(" ==> Get Module and Acitivity Mapping START <== ");
		 List<MetaTable> moduleActivityIds = new ArrayList<>();
		 try{
			  StringBuffer qry = new StringBuffer("SELECT ");
			  qry.append( AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ACTIVITY"));qry.append(",");
			  qry.append( AdminDBConstants.MOD_ACTIVITY_MAPPING.get("PAGE_ID"));
			  qry.append(" FROM ");qry.append(AdminDBConstants.TBL_MOD_ACTIVITY_MAPPING);
			  moduleActivityIds = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(MetaTable.class) );
		 }catch( Exception e ){
				logger.error(" Exception occured when tries to get mapping from Mapping table. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get Module and Acitivity Mapping END <== ");
		return moduleActivityIds;
	}
    
}