package com.orb.admin.modulemanagement.service;

import java.util.List;

import com.orb.admin.client.model.MetaTable;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface ModuleManagementService {

	public List<MetaTable> getMainModuleList(CommonError error, int roleId); 
	public void add(CommonError commonError ,MetaTable metaTable, User loginUser );
	public void update(CommonError commonError, MetaTable metaTable,String moduleId, User loginUser); 
	public List<MetaTable> getAllMetaTableList(CommonError error);
	public List<MetaTable> getSubModuleList(CommonError error, String pageId); 
	public MetaTable getMetaTable(CommonError commonError, String pageId);
	public void delete(CommonError commonError,User loginUser, List<MetaTable> selectedModules);
	public List<MetaTable> getInactiveMetaTableList(CommonError commonError);
	public List<MetaTable> getActiveMetaTableList(CommonError commonError);
	public void lockMetaTable(CommonError commonError, User loginUser, List<MetaTable> selectedModules);
	public void unlockMetaTable(CommonError commonError, User loginUser, List<MetaTable> selectedModules);
	public void lockViewedMetaTable(CommonError commonError, User loginUser, MetaTable metaTable);
	public void unlockViewedMetaTable(CommonError commonError, User loginUser, MetaTable metaTable);
	public void deleteViewedMetaTable(CommonError commonError, User loginUser, MetaTable metaTable);
	public List<MetaTable> getParentPageList(CommonError commonError);
	public void subModdeleteAction(CommonError commonError, User loginUser, MetaTable metaTable);
}
