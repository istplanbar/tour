package com.orb.admin.modulemanagement.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.modulemanagement.dao.ModuleManagementDAO;
import com.orb.admin.modulemanagement.service.ModuleManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;


@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class ModuleManagementServiceImpl implements ModuleManagementService{
	private static Log logger = LogFactory.getLog(ModuleManagementServiceImpl.class);
	
	@Autowired
	ModuleManagementDAO moduleManagementDAO;
	
	/**
	 * This method call DAO to fetch main module list
	 * @param error
	 * @param roleId
	 * @return
	*/
	public List<MetaTable> getMainModuleList(CommonError error, int roleId) {
		return moduleManagementDAO.getMainModuleList(error, roleId);
	}
	
	/**
	 * This method call DAO to fetch submodule list
	 * @param error
	 * @param pageId
	 * @return
	 */
	public List<MetaTable> getSubModuleList(CommonError error, String pageId) {
		return moduleManagementDAO.getSubModuleList(error, pageId);
	}
	
	/**
	 * This method will call the DAO for add
	 * @param commonError
	 * @param metaTable
	 * @param loginUser
	 */
	public void add(CommonError commonError, MetaTable metaTable,User loginUser) {
		moduleManagementDAO.addModule(commonError, metaTable,loginUser);
	}
	
	/**
	 * This method will call the DAO for update
	 * @param commonError
	 * @param metaTable
	 * @param moduleId
	 * @param loginUser
	 */
	public void update(CommonError commonError, MetaTable metaTable,String moduleId, User loginUser){
		moduleManagementDAO.update(commonError, metaTable,moduleId, loginUser);
	}
	
	/**
	 * This method call DAO to fetch All MetaTable list
	 * @param error
	 * @return
	 */
	public List<MetaTable> getAllMetaTableList(CommonError error) {
		return moduleManagementDAO.getAllMetaTableList(error);
	}
	
	/**
	 * This method call DAO to fetch MetaTable
	 * @param commonError
	 * @param pageId
	 * @return
	 */
	public MetaTable getMetaTable(CommonError commonError, String pageId){
		List<String> moduleIds = new ArrayList<>();
		List<MetaTable> metaTableList = moduleManagementDAO.getModuleActivityMappingList(commonError);
		MetaTable moduleName = new MetaTable();
		MetaTable parentPageMetaTable = new MetaTable();
		moduleIds.addAll(metaTableList.stream().filter(metaTable -> Integer.parseInt(pageId) == metaTable.getPageId()).map(MetaTable::getActivity).collect(Collectors.toList()));

		moduleName =moduleManagementDAO.getMetaTable(commonError,pageId);
		if(CommonConstants.TRUE.equals(moduleName.getChildModuleConfig())){
			parentPageMetaTable=moduleManagementDAO.getMetaTable(commonError, String.valueOf(moduleName.getParentPage()));
			moduleName.setChildModParentPage(parentPageMetaTable.getPageDesc());
			parentPageMetaTable=moduleManagementDAO.getMetaTable(commonError, String.valueOf(parentPageMetaTable.getParentPage()));
			moduleName.setParentPageDesc(parentPageMetaTable.getPageDesc());
		}else{
			parentPageMetaTable=moduleManagementDAO.getMetaTable(commonError, String.valueOf(moduleName.getParentPage()));
			moduleName.setParentPageDesc(parentPageMetaTable.getPageDesc());
		}
		moduleName.setActivityList(fetchModActivity(commonError, moduleIds));
	    return moduleName;
	}
	
	/**
	 * This method calls DAO to deletes particular Module
	 * @param commonError
	 * @param loginUser
	 * @param selectedModules
	 */
    public void delete(CommonError commonError,User loginUser,List<MetaTable> selectedModules){
    	List<Integer> ids=new ArrayList<>();
		if( selectedModules == null || selectedModules.isEmpty() ){
			logger.info("No record is available");
			return;
		}
		for( MetaTable metaTable : selectedModules ){
			ids.add(metaTable.getPageId());
		}
			moduleManagementDAO.delete(commonError, loginUser, ids);
	    }
	
	
	/**
	 * This method calls DAO to get all active Module Management from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<MetaTable> getActiveMetaTableList(CommonError commonError) {
		List<MetaTable> metaTableList = moduleManagementDAO.getMetaTableList(commonError, CommonConstants.Y);
		List<MetaTable> metaList = moduleManagementDAO.getModuleActivityMappingList(commonError);
		List<String> moduleIds = new ArrayList<>();
		if(metaTableList != null){
			metaTableList.forEach(k->{
				moduleIds .addAll(metaList.stream().filter(metaTable -> k.getPageId() == metaTable.getPageId()).map(MetaTable::getActivity).collect(Collectors.toList()));

				k.setActivityList(fetchModActivity(commonError, moduleIds));
		 });
			
		}
		return metaTableList;
	}
	
	/**
	 * This method calls DAO to get all active Module from DB
	 * @param error
	 * @param moduleIds
	 * @return
	 */
	public List<ModuleActivity> fetchModActivity(CommonError error, List<String> moduleIds){
		List<ModuleActivity>  moduleActivities = new ArrayList<>();
		if(!moduleIds.isEmpty()){
			moduleActivities.addAll(moduleManagementDAO.getModuleActivity(error,moduleIds));
		}
		return moduleActivities;
		
	}
	
    /**
	 * This method calls DAO to get all Inactive Module Management level from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<MetaTable> getInactiveMetaTableList(CommonError commonError) {
		return moduleManagementDAO.getMetaTableList(commonError,CommonConstants.N);
	}
	
	/**
	 * This method calls DAO to activate the locked Module
	 * @param commonError
	 * @param loginUser
	 * @param selectedModules
	 */
	public void unlockMetaTable(CommonError commonError, User loginUser, List<MetaTable> selectedModules) {
		List<Integer> ids=new ArrayList<>();
		if( selectedModules == null || selectedModules.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( MetaTable metaTable : selectedModules){
			ids.add(metaTable.getPageId());
		}
			moduleManagementDAO.updateActiveStatus(commonError, loginUser, ids, CommonConstants.Y);
		}

	/**
	 * This method calls DAO to de-activate Module
	 * @param commonError
	 * @param loginUser
	 * @param selectedModules
	 */
    @Override
	public void lockMetaTable(CommonError commonError, User loginUser, List<MetaTable> selectedModules) {
    	List<Integer> ids=new ArrayList<>();
		if( selectedModules == null || selectedModules.isEmpty() ){
			return;
		}
		for( MetaTable metaTable : selectedModules){
			ids.add(metaTable.getPageId());
		}
			moduleManagementDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
		}
	
	/**
	 * This method calls DAO to de-activate Module
	 * @param commonError
	 * @param loginUser
	 * @param metaTable
	 */
	@Override
	public void lockViewedMetaTable(CommonError commonError, User loginUser, MetaTable metaTable) {
		List<Integer> ids=new ArrayList<>();
		ids.add(metaTable.getPageId());
		moduleManagementDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
	}
	
	/**
	 * This method calls DAO to activate Module
	 * @param commonError
	 * @param loginUser
	 * @param metaTable
	 */
	@Override
	public void unlockViewedMetaTable(CommonError commonError, User loginUser,MetaTable metaTable) {
		List<Integer> ids=new ArrayList<>();
		ids.add(metaTable.getPageId());
		moduleManagementDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );
	}

	/**
	 * This method calls DAO to deletes particular Module
	 * @param commonError
	 * @param loginUser
	 * @param metaTable
	 */
	@Override
	public void deleteViewedMetaTable(CommonError commonError, User loginUser, MetaTable metaTable) {
		List<Integer> ids=new ArrayList<>();
		ids.add(metaTable.getPageId());
		moduleManagementDAO.delete(commonError, loginUser, ids);
	}
	
	/**
	 * This method calls DAO to get parent page
	 * @param commonError
	 * @return
	 */
	@Override
	public List<MetaTable> getParentPageList(CommonError commonError) {
		return moduleManagementDAO.getMetaTableList(commonError,CommonConstants.Y );
	}

	/**
	 * This method calls DAO to deletes particular sub Module
	 * @param commonError
	 * @param loginUser
	 * @param metaTable
	 */
	@Override
	public void subModdeleteAction(CommonError commonError, User loginUser, MetaTable metaTable) {
		List<Integer> ids=new ArrayList<>();
		ids.add(metaTable.getPageId());
		moduleManagementDAO.delete(commonError, loginUser, ids);
	}
	
}
	
	
	