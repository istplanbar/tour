package com.orb.admin.modulemanagement.constants;

public class ModuleManagementConstants {
	
	public final static String LIST_PAGE = "/common/process/admin/loadModuleManagement.html";
	public final static String LIST_HTML = "admin/modulemanagement/moduleManagementList.xhtml";
	public final static String CODE_MODULES_LIST_HTML = "admin/modulemanagement/coreModules.xhtml";
	public final static String EDIT_PAGE = "/common/process/admin/loadModuleManagementAddUpdate.html";
	public final static String EDIT_HTML = "admin/modulemanagement/moduleManagementAddUpdate.xhtml";
	public final static String VIEW_PAGE = "/common/process/admin/loadModuleManagementView.html";
	public final static String VIEW_HTML = "admin/modulemanagement/moduleManagementView.xhtml";
	public final static String SELECTED_MODULE_ID="SELECTED_MODULE_ID";
	public final static String MODULES_TOGGLER_LIST = "MODULES_TOGGLER_LIST" ;
	public static final String MODULE_TITLE_CREATION = "lbl_module_Creation"; 
	public static final String MODULE_TITLE_UPDATE = "lbl_module_Update";
	public final static String MODULE_ADDED_SUCCESS="module_added_success";
	public final static String MODULE_UPDATED_SUCCESS="module_update_success";
	public static final String SHOW_ACTIVE_MODULE = "Show Active_Module";
	public static final String MODULE_LOCKED_SUCCESS = "module_locked_success";
	public static final String MODULE_UNLOCKED_SUCCESS = "module_unlocked_success";
	public static final String MODULE_DELETE_SUCCESS = "module_deleted_success";
	public static final String SUB_MODULE_UNLOCKED_SUCCESS = "sub_module_unlocked_success";
	public static final String SUB_MODULE_LOCKED_SUCCESS = "sub_module_locked_success";
	public static final String SUB_MODULE_DELETE_SUCCESS = "sub_module_deleted_success";
}
