package com.orb.admin.layoutadministration.dao;

/**
 * @author IST planbar / SuKu
 * 
 */

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.admin.client.dao.AbstractAdminDAO;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.layoutadmnistration.model.ModuleInfo;
import com.orb.admin.layoutadmnistration.model.TableDesc;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class LayoutManagementDAO extends AbstractAdminDAO {
	private static Log logger = LogFactory.getLog(CommonDAO.class);

	/**
	 * This method used to display the List of layout details
	 * @param error
	 * @return
	 */
	public List<Layout> getLayoutList(CommonError commonError) {
		logger.info(" ==> Get LayoutList START <== ");
		List<Layout> layoutList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));newQry.append( "," );
			newQry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_NAME"));newQry.append(",");
			newQry.append(AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID"));
			newQry.append(" FROM ");newQry.append(AdminDBConstants.TBL_LAYOUT_TABLE);
			newQry.append(" ORDER BY ");newQry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));
			layoutList = getJdbcTemplate().query( newQry.toString(),new Object[]{},new BeanPropertyRowMapper<Layout>(Layout.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Layout list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get LayoutList END <== ");
		return layoutList;
	}  
	
	/**
	 * This method used to display the List of table details
	 * @param error
	 * @param subModule
	 * @return
	 */
	public List<ModuleInfo> getTables(CommonError commonError, Integer subModule) {
		logger.info(" ==> getTables START <== ");
		if( subModule == null ){
			logger.error(" User doesn't select any sub modules ");
			return null;
		}
		List<ModuleInfo> layoutList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.MODULES_INFO.get("ID"));
			qry.append(" , ");
			qry.append(AdminDBConstants.MODULES_INFO.get("PAGE_ID"));
			qry.append(" , ");
			qry.append(AdminDBConstants.MODULES_INFO.get("NAME_OF_THE_TABLE"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_MODULES_INFO);
			qry.append(" WHERE ");qry.append(AdminDBConstants.MODULES_INFO.get("PAGE_ID"));qry.append(" = ?");
			layoutList = getJdbcTemplate().query(qry.toString(), new Object[]{subModule}, new BeanPropertyRowMapper<ModuleInfo>(ModuleInfo.class));
		}catch( Exception e ){
			logger.error("Error when retrieve Tables List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == layoutList){
			logger.error("Query returns Null");
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getTables END <== ");
		return layoutList;
	}

	/**
	 * 
	 * @param error
	 * @param table
	 * @return
	 */
	public List<TableDesc> getTableDesc(CommonError commonError, ModuleInfo table) {
		logger.info(" ==> getTableDesc START <== ");
		if( table == null ){
			logger.error(" User doesn't select any modules ");
			return null;
		}
		List<TableDesc> tableDesc = null;
		try{
			StringBuffer qry = new StringBuffer("DESC ");qry.append(table.getTableName());
			tableDesc = getJdbcTemplate().query(qry.toString(), new Object[]{}, new BeanPropertyRowMapper<TableDesc>(TableDesc.class));
		}catch( Exception e ){
			logger.info("Error when getTableDesc.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == tableDesc){
			logger.error("Query returns Null");
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getTableDesc END <== ");
		return tableDesc;
	}

	/**
	 * This method is used to insert a new layout
	 * @param error
	 * @param layout
	 * @return
	 */
	public int insertLayout(CommonError commonError, Layout layout) {
		logger.info(" ==> Insert Layout Creation START <== ");
		if( null == layout){
			logger.error("Can't create new Layout");
			return 0;
		}
		int nextRowId = 0;
		try{
			nextRowId = Integer.parseInt(getNextRowId( AdminDBConstants.TBL_LAYOUT_TABLE));
			List<Object> args = new ArrayList<Object>();
			args.add(layout.getLayoutName());
			args.add(layout.getMainMenuId());
			args.add(layout.getSubmenuId());
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_LAYOUT_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_NAME"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_TABLE.get("MAIN_MENU_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID"));queryInsert.append(") VALUES(?,?,?) ");
			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when Insert Layout Creation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> Insert Layout Creation END <== ");
		return nextRowId;
	}

	/**
	 * This method used to insert a layout description table
	 * @param error
	 * @param tableFields
	 * @param layoutTable
	 */
	public void insertLayoutDesc(CommonError commonError, List<TableDesc> tableFields, Layout layoutTable ) {
		logger.info(" ==> Insert LayoutDecs Creation START <== ");
		if( null == tableFields || null == layoutTable){
			logger.error("Can't create new LayoutDecs");
			return;
		}
		try{
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_LAYOUT_DESC_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("NAME_OF_THE_TABLE"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("FIELD_NAME"));
			queryInsert.append(") VALUES(?,?,?) ");
			getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setLong(1, layoutTable.getLayoutId());
					ps.setString(2, layoutTable.getTableName());
					ps.setObject(3, tableFields.get(i).getField());
				}		
				public int getBatchSize() {
					return tableFields.size();
				}

			});
		}catch( Exception e ){
			logger.error("Error when Insert LayoutDecs Creation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> Insert LayoutDecs Creation END <== ");
	}
	
	/**
	 * This method used to insert on layout history table
	 * @param commonError
	 * @param tableFields
	 * @param layoutTable
	 * @param loginUser
	 */
	public void insertLayoutHistory(CommonError commonError, List<TableDesc> tableFields,Layout layoutTable, User loginUser) {
		logger.info(" ==> Insert Layouthistory Creation START <== ");
		if( null == tableFields || null == layoutTable || null == loginUser){
			logger.error("Can't create layout history");
			return;
		}
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_LAYOUT_HISTORY_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("LAYOUT_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("LAYOUT_NAME"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("SUBMENU_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("NAME_OF_THE_TABLE"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("FIELD_NAME"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("CHANGED_TIME"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.LAYOUT_HISTORY_TABLE.get("CHANGED_BY"));
			queryInsert.append(") VALUES(?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setLong(1, layoutTable.getLayoutId());
					ps.setString(2, layoutTable.getLayoutName());
					ps.setLong(3, layoutTable.getSubmenuId());
					ps.setString(4, tableFields.get(i).getTablename());
					ps.setObject(5, tableFields.get(i).getField());
					ps.setObject(6,date);
					ps.setObject(7,loginUser.getUserId());
				}			
				public int getBatchSize() {
					return tableFields.size();
				}
			});
		}catch( Exception e ){
			logger.error("Error when Insert Layouthistory Creation.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Layouthistory Creation END <== ");
	}
	
	/**
	 * This method used to update the details of Layout table 
	 * @param error
	 * @param layout
	 * @return
	 */

	public void updateLayout(CommonError commonError, Layout layoutTable,User loginUser, String layoutId) {
		logger.info(" ==> Update Layout START <== ");
		if( null == layoutTable || null == loginUser || null == layoutId){
			logger.error("Can't Update Layout");
			return ;
		}
		try{
			StringBuffer updateQry = new StringBuffer();		
			updateQry.append("UPDATE ");updateQry.append( AdminDBConstants.TBL_LAYOUT_TABLE );updateQry.append(" SET ");
			updateQry.append( AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_NAME") );updateQry.append( " = ?," );
			updateQry.append( AdminDBConstants.LAYOUT_TABLE.get("MAIN_MENU_ID") );updateQry.append( " = ?," );
			updateQry.append( AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID") );updateQry.append( " = ?" );
			updateQry.append(" WHERE ");updateQry.append( AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID") );updateQry.append(" = ? ");
			List<Object> args = new ArrayList<Object>();
			args.add(layoutTable.getLayoutName());
			args.add(layoutTable.getMainMenuId());
			args.add(layoutTable.getSubmenuId());
			args.add(layoutTable.getLayoutId());
			getJdbcTemplate().update( updateQry.toString(),args.toArray() );
		}catch( Exception e ){
			logger.error("Error when Update Layout.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==>  Update Layout END <== ");

	}
	
	/** 
	 * This method used to get a particular Layout from DB
	 * @param error
	 * @param layoutId
	 * @return
	 */
	public List<Layout> selectLayoutDescDetails(CommonError commonError, String layoutId) {
		logger.info(" ==> Get Layout Desc Details START <== ");
		if( CommonUtils.getValidString(layoutId).isEmpty() ){
			logger.error("Can't fetch Layout Desc Details");
			return null;
		}
		List<Layout> layoutsDescDetail = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(" L.");qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_NAME"));qry.append(" , ");
			qry.append(" LD.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("NAME_OF_THE_TABLE"));qry.append(" , ");
			qry.append(" LD.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("FIELD_NAME"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_TABLE);qry.append(" AS L ");
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_LAYOUT_DESC_TABLE);qry.append(" AS LD ");
			qry.append(" ON ");qry.append(" LD.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));qry.append(" = ");
			qry.append(" L.");qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));
			qry.append(" WHERE ");qry.append(" L.");qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));qry.append(" = (?) ");
			layoutsDescDetail = (List<Layout>) getJdbcTemplate().query(qry.toString(),new Object[]{layoutId}, new BeanPropertyRowMapper<Layout>(Layout.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Layout list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == layoutsDescDetail){
			logger.error("Query returns Null");
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get Layout Desc Details END <== ");
		return layoutsDescDetail;
	}
	
	/**
	 * This method used to get a details of Layoutdesc table from DB
	 * @param error
	 * @param layoutId
	 * @return
	 */
	public List<Layout> getLayoutDesc(CommonError commonError, String layoutId) {
		logger.info(" ==> select LayoutDesc START <== ");
		if( CommonUtils.getValidString(layoutId).isEmpty() ){
			logger.error(" User doesn't select any layout ");
			return null;
		}
		List<Layout> layouts = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append("*");
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_DESC_TABLE);qry.append(" AS LD ");
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_LAYOUT_TABLE);qry.append(" AS L ");
			qry.append(" ON ");qry.append(" LD.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));qry.append(" = ");
			qry.append(" L.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));
			qry.append(" WHERE ");qry.append(" L.");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));qry.append(" = (?) ");
			layouts = getJdbcTemplate().query(qry.toString(), new Object[]{layoutId}, new BeanPropertyRowMapper<Layout>(Layout.class));
		}catch( Exception e ){
			logger.info("Error when Select LayoutDecs .  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == layouts){
			logger.error("Query returns Null");
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> select LayoutDesc END <== ");
		return layouts;
	}
	
	/**
	 * This method used to get a selected menus from  DB
	 * @param error
	 * @param layoutId
	 * @return
	 */
	public List<MetaTable> getSelectedMenu(CommonError commonError, String layoutId) {
		logger.info(" ==> select getSelectedMenu START <== ");
		if (CommonUtils.getValidString(layoutId).isEmpty()) {
			logger.error("Can't fetch selected sub menu");
			return null;
		}
		List<MetaTable> menuList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" IN (SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" IN (SELECT ");
			qry.append(AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));qry.append("=? ))");
			menuList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new Object[]{layoutId}, new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		} catch( Exception e ){
			logger.info("Error when retrieve metaTable List.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == menuList){
			logger.error("Query returns Null");
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMetaTableList END <== ");
		return menuList;
	}
	
	/**
	 * This method used to delete layout from DB
	 * @param error
	 * @param Id
	 * @return
	 */
	public void deleteLayout(CommonError commonError, Integer id) {
		logger.info(" ==> deleteLayouts START <== ");
		if( id == null){
			logger.error("Can't delete layouts");
			return;
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));qry.append("=? ");

			getJdbcTemplate().update(qry.toString(),id);
		}catch( Exception e ){
			logger.error("Error when Delete Layouts.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteLayouts END <== ");

	}

	/**
	 * This method used to delete layout description from DB
	 * @param error
	 * @param id
	 */
	public void deleteLayoutDesc(CommonError commonError, Integer id) {
		logger.info(" ==> deleteLayouts desc START <== ");
		if( id == null){
			logger.error("Can't delete Layouts desc");
			return;
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_DESC_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("LAYOUT_ID"));qry.append("=? ");

			getJdbcTemplate().update(qry.toString(),id);
		}catch( Exception e ){
			logger.error("Error when Delete Layouts desc.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteLayouts desc END <== ");

	}
	
}		