package com.orb.admin.layoutadministration.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.DualListModel;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.layoutadmnistration.model.TableDesc;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface LayoutManagementService {
	
	public List<MetaTable> getMainMenuList(CommonError commonError);

	public List<MetaTable> getSubModuleList(CommonError commonError, Integer subModuleId);

	public List<Layout> getLayoutList(CommonError commonError);

	public List<Layout> getLayout(CommonError commonError,String layoutId);

	public List<Layout> getLayoutDesc(CommonError commonError, String layoutId);

	public List<MetaTable> getSelectedMenu(CommonError commonError, String layoutId);
	
	public List<MetaTable> getMetaTableList(CommonError commonError, List<MetaTable> mainModulesTarget);
	
	public Map<String,DualListModel<TableDesc>> getTableFields(CommonError commonError, Integer subModuleId, List<Layout> layoutList);

	public void deleteLayout(CommonError commonError, String layoutId);
	
	public void updateLayout(CommonError commonError,Map<String,DualListModel<TableDesc>> selectedTableFields ,Layout layoutTable, String layoutId, User loginUser);

	public void createLayout(CommonError commonError, Map<String, DualListModel<TableDesc>> selectedTableFields, Layout layoutTable, User loginUser);
}
