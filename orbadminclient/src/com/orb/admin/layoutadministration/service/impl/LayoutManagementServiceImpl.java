package com.orb.admin.layoutadministration.service.impl;

/**
 * @author IST planbar / SuKu
 * 
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.DualListModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.layoutadministration.dao.LayoutManagementDAO;
import com.orb.admin.layoutadministration.service.LayoutManagementService;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.layoutadmnistration.model.ModuleInfo;
import com.orb.admin.layoutadmnistration.model.TableDesc;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;


@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class LayoutManagementServiceImpl implements LayoutManagementService{

	@Autowired
	LayoutManagementDAO layoutManagementDAO;
	@Autowired
	UserDAO userDAO;

	@Override
	public List<MetaTable> getMainMenuList(CommonError commonError) {
			return layoutManagementDAO.getMenus(commonError);
	}

	/**
	 * This method call DAO to fetch submodule list
	 */
	public List<MetaTable> getSubModuleList(CommonError commonError, Integer selectedMainModule) {
		return layoutManagementDAO.getSubMenus(commonError, selectedMainModule);
	}

	/**
	 * This method will call the DAO for get the layout based on submodules
	 */
	public Map<String,DualListModel<TableDesc>> getTableFields(CommonError commonError, Integer subModules, List<Layout> layoutList) {
		List<ModuleInfo> tablesList = null;
		tablesList = layoutManagementDAO.getTables(commonError, subModules);
		Map<String,List<TableDesc>> selectedTableDescMap = generateTableDesc(layoutList);
		Map<String,DualListModel<TableDesc>> tableFieldsMap = new HashMap<String,DualListModel<TableDesc>>();
		for(ModuleInfo table : tablesList){
			List<TableDesc> tableDescList = layoutManagementDAO.getTableDesc(commonError, table);
			List<TableDesc> uiTableDescTarget = selectedTableDescMap.get(table.getTableName());
			if( uiTableDescTarget == null )
				uiTableDescTarget = new ArrayList<TableDesc>();
			DualListModel<TableDesc> uiTableDesc = new DualListModel<TableDesc>( tableDescList, uiTableDescTarget );
			tableFieldsMap.put(table.getTableName(), uiTableDesc);
		}
		return tableFieldsMap;

	}
	
	/**
	 * Get table description map
	 * @param layoutList
	 * @return
	 */
	private Map<String,List<TableDesc>> generateTableDesc( List<Layout> layoutList ){
		Map<String,List<TableDesc>> tableDescMap = new HashMap<String,List<TableDesc>>();
		if( layoutList != null ){
			for(Layout layout : layoutList){
				String tableName = layout.getTableName();
				List<TableDesc> tableDescList = tableDescMap.get(tableName);
				if( tableDescList == null ){
					tableDescList = new ArrayList<TableDesc>();
					tableDescMap.put(tableName, tableDescList);
				}
				TableDesc tableDesc = new TableDesc();
				tableDesc.setTablename(tableName);
				tableDesc.setField(layout.getFieldName());
				tableDescList.add(tableDesc);
			}
		}
		return tableDescMap;
	}

	/**
	 * This method will call the DAO for create Layout
	 */
	public void createLayout(CommonError commonError, Map<String,DualListModel<TableDesc>> selectedTableFields, Layout layoutTable, User loginUser) {
		int id = layoutManagementDAO.insertLayout(commonError, layoutTable);
		layoutTable.setLayoutId(id);
		insertLayoutDesc(commonError, selectedTableFields, layoutTable,loginUser);
	}
	
	/**
	 * This method will call the DAO for update layout
	 */
	public  void updateLayout(CommonError commonError, Map<String,DualListModel<TableDesc>> selectedTableFields, Layout layoutTable, String layoutId,User loginUser) {
		int id = Integer.parseInt(layoutId);
		layoutManagementDAO.updateLayout(commonError, layoutTable,loginUser,layoutId);
		layoutManagementDAO.deleteLayoutDesc(commonError, id);
		insertLayoutDesc(commonError, selectedTableFields, layoutTable,loginUser);
	}
	
	/**
	 * This method will call the DAO for insert layout description
	 * @param commonError
	 * @param selectedTableFields
	 * @param layoutTable
	 * @param loginUser
	 */
	private void insertLayoutDesc(CommonError commonError,Map<String,DualListModel<TableDesc>> selectedTableFields, Layout layoutTable,User loginUser ){
		List<TableDesc> selectedFields = null;
		for (String key : selectedTableFields.keySet()) {
			DualListModel<TableDesc> targetFields = selectedTableFields.get(key);
			selectedFields = targetFields.getTarget();
			layoutTable.setTableName(key);
			layoutTable.setFieldName(key);
			layoutManagementDAO.insertLayoutDesc(commonError, selectedFields, layoutTable);
			layoutManagementDAO.insertLayoutHistory(commonError,  selectedFields, layoutTable,  loginUser);		
		}
	}
	
	/**
	 * This method will call the DAO for delete Layouts
	 */
	public void deleteLayout(CommonError commonError, String layoutId) {
		int id = Integer.parseInt(layoutId);
		layoutManagementDAO.deleteLayoutDesc(commonError, id);
		layoutManagementDAO.deleteLayout(commonError, id);
	}
	
	/**
	 * This method calls DAO to get existing layout List from DB
	 */
	public List<Layout> getLayoutList(CommonError commonError) {
		return layoutManagementDAO.getLayoutList(commonError);
	}

	/**
	 * This method calls DAO to get existing layout list from DB
	 */
	public List<Layout> getLayout(CommonError commonError,String layoutId) {
		return layoutManagementDAO.selectLayoutDescDetails(commonError,layoutId);
	}

	/**
	 * This method will call the DAO for get Layout description list
	 */
	public List<Layout> getLayoutDesc(CommonError commonError, String layoutId) {
		return layoutManagementDAO.getLayoutDesc(commonError, layoutId);
	}

	/**
	 * This method will call the DAO for get menu list
	 */
	public List<MetaTable> getSelectedMenu(CommonError commonError, String layoutId) {
		return layoutManagementDAO.getSelectedMenu(commonError, layoutId);
	}

	/**
	 * This method will call the DAO for get meta table list 
	 */
	public List<MetaTable> getMetaTableList(CommonError commonError, List<MetaTable> mainModulesTarget) {
		return layoutManagementDAO.getMainModules(commonError, mainModulesTarget);
	}

}
