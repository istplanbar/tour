package com.orb.admin.layoutadministration.constants;

public class LayoutManagementConstants {
	
	public final static String REDIR_VIEW_PAGE = "/common/process/admin/loadLayoutManagementView.html";
	public final static String SELECTED_LAYOUT_ID = "SELECTED_LAYOUT_ID";
	public final static String SELECTED_ID = "SELECTED_ID";
	public final static String VIEW = "VIEW";
	public final static String LIST = "LIST";
	public final static String ADD = "ADD";
	public final static String UPDATE = "UPDATE";
	public final static String REDIR_LIST_PAGE = "/common/process/admin/loadLayoutManagementList.html";
	public final static String ADD_UPDATE_PAGE_REDIR = "/common/process/admin/loadLayoutManagementAddUpdate.html";
	public final static String LIST_PAGE = "admin/layout/layoutManagementList.xhtml";
	public final static String VIEW_PAGE = "admin/layout/layoutManagementView.xhtml";
	public final static String ADD_UPDATE_PAGE = "admin/layout/layoutManagementAddUpdate.xhtml";

}
