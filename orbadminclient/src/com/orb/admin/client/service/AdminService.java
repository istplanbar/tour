package com.orb.admin.client.service;

import java.util.List;
import java.util.Map;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.model.UserUSBTokenAuthentication;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface AdminService {

	public Map<String, String> getAdminConfMap(CommonError error);
	
	public List<MetaTable> getMetaTableList(CommonError error);
	
	public List<MetaTable> getMainModuleList(CommonError error, User user);

	void setTheme(String newTheme, String userId);

	void setTopMenu(boolean topMenu, String userId);

	public void updateAdminConf(String adminKey, String adminValue, CommonError error);

	public List<Integer> getFilteredModuleList(CommonError error, User user);
	
	public boolean sendMTAN(CommonError commonError, User user);

	public boolean checkMTAN(CommonError commonError, String userId, String mtan);
	
	public void sendNotificationsToUsers(CommonError commonError,User loginUser);
	
	public void addUserTokenAuthentication( CommonError commonError, UserUSBTokenAuthentication usbTokenDetails );
	
	public UserUSBTokenAuthentication getUserTokenAuthentication( CommonError commonError, String userName );
	
	public void addGoogleAuthSecrets( CommonError commonError, User user, String secretKey);
	
	public String getGoogleAuthSecrets( User user );

	public boolean sendGoogleAuthPin(CommonError commonError, User appUser);
	

}
