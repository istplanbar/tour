package com.orb.admin.client.service.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.client.dao.AdminDAO;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.client.model.UserUSBTokenAuthentication;
import com.orb.admin.client.service.AdminService;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.roleadministration.dao.RoleManagementDAO;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.constants.UserManagementConstants;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.textmagic.sms.TextMagicMessageService;
import com.textmagic.sms.exception.ServiceException;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class AdminServiceImpl extends AbstractAdminService implements AdminService{
	private static Log logger = LogFactory.getLog(AdminServiceImpl.class);
	
	@Autowired
	AdminDAO adminDAO;
	@Autowired
	UserDAO userDAO;
	@Autowired
	RoleManagementDAO roleManagementDAO;
	

	@Override
	public void setTheme(String newTheme, String userId) {
		userDAO.setTheme(newTheme, userId);
	}

	@Override
	public void setTopMenu(boolean topMenu, String userId) {
		userDAO.setTopMenu(topMenu, userId);
	}

	@Override
	@Cacheable(value="adminConf")
	public Map<String, String> getAdminConfMap(CommonError error) {
		return adminDAO.getAdminConfMap(error);
	}

	@Override
	public List<MetaTable> getMetaTableList(CommonError error) {
		Map<Integer, MetaTable> metaTableMap = adminDAO.getMetaTableList(error);
		Map<Integer, MetaTable> moduleMap = new HashMap<Integer, MetaTable>();
		List<MetaTable> mainModules = new ArrayList<MetaTable>();
		for(MetaTable metaTable : metaTableMap.values()){
			if(metaTable.getParentPage() == 1 || metaTable.getParentPage() == 0){
				if(metaTable.getParentPage() == 1){
					mainModules.add(metaTable);
				}
			}else if(moduleMap.containsKey(metaTable.getParentPage())){
				moduleMap.get(metaTable.getParentPage()).getMetaTableList().add(metaTable);
			}else{
				metaTableMap.get(metaTable.getParentPage()).getMetaTableList().add(metaTable);
				moduleMap.put(metaTable.getParentPage(), metaTableMap.get(metaTable.getParentPage()));
			}
		}
		return mainModules;
	}

	@Override
	@CacheEvict(value="adminConf", allEntries=true)
	public void updateAdminConf(String adminKey, String adminValue, CommonError error) {
		adminDAO.updateAdminConf(adminKey, adminValue, error);
	}
	
	
	@Override
	public List<Integer> getFilteredModuleList(CommonError error,User user) {
		List<Role> userRoleList = new ArrayList<>();
		List<String> userRoleIdList = new ArrayList<>();
		userRoleList = userDAO.getUserRoles(error, user.getId());
		if(userRoleList != null){
			userRoleIdList = userRoleList.stream().map(i -> i.getIdentifier()).collect(Collectors.toList());
		}
		return adminDAO.getFilteredModuleList(error, userRoleIdList);
	}

	@Override
	public List<MetaTable> getMainModuleList(CommonError commonError, User user) {
		List<Role> userRoleList = new ArrayList<>();
		List<String> userRoleIdList = new ArrayList<>();
		userRoleList = userDAO.getUserRoles(commonError, user.getId());
		if(userRoleList != null){
			userRoleIdList = userRoleList.stream().map(i -> i.getIdentifier()).collect(Collectors.toList());
		}
		return adminDAO.getMainModuleList(commonError, userRoleIdList);
	}
	
	

	/** 
	 *	This method will be called when user requested their mTan type(whether Alphabet or Numeric or AlphaNumeric)
	 *      and mTan receiving type( thru Mail or SMS)
	 */
	public boolean sendMTAN(CommonError commonError, User user){
		String mTan = null;
		if(CommonConstants.A.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
			mTan = RandomStringUtils.randomAlphabetic(CommonConstants.MTAN_LENGTH);
		}else if(CommonConstants.AN.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
		    mTan = RandomStringUtils.randomAlphanumeric(CommonConstants.MTAN_LENGTH);
		}else if(CommonConstants.NUM.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
			mTan = RandomStringUtils.randomNumeric(CommonConstants.MTAN_LENGTH);
		}
		userDAO.insertUpdateSecondLevelAuthDetails(commonError, user, mTan);
		
		Map<String, String> adminConfMap = adminDAO.getAdminConfMap(null);
		
		String mTanType = CommonUtils.getValidString(user.getAuthenticateUser());
		if( mTanType.isEmpty() || "null".equalsIgnoreCase(mTanType) ){
			mTanType = CommonUtils.getValidString(adminConfMap.get("secondLevelAuth"));
		}
		if( CommonConstants.MAIL.equalsIgnoreCase(mTanType) ){
			try {
				StringBuilder mailBodyLink = new StringBuilder();
				mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_BODY));
				mailBodyLink.append(mTan);mailBodyLink.append("\n");
				mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
				sendMailToUser( commonError, user, getJSFMessage(CommonConstants.MTAN_MAIL_SUB), mailBodyLink.toString() );
			 } catch (Exception e) {
	            logger.info("Error when sending emails.  " + e.getMessage());
	            commonError.addError(ForgotPasswordConstant.EMAIL_ERROR);
				return false;
	        }
		}else if( CommonConstants.SMS.equalsIgnoreCase(mTanType) ){
			String phoneNo = user.getPhoneNumber();
			TextMagicMessageService service = 
		             new TextMagicMessageService ("***REMOVED***","***REMOVED***");
			try { 
				StringBuilder msgBodyLink = new StringBuilder();
				msgBodyLink.append(getJSFMessage(CommonConstants.MTAN_BODY));
				msgBodyLink.append(mTan);msgBodyLink.append("\n");
				msgBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
				service.send( msgBodyLink.toString(), phoneNo ); 
			} 
			catch(ServiceException e) { 
				System.out.println("Text Message Not Sent"+e.getMessage()); 
			} 
		}
		return true;
	}
	
	/** mTan verification
	 * 
	 */
	public boolean checkMTAN(CommonError commonError, String userId, String mTan) {
		return userDAO.getSecondLevelAuthSubmit(commonError, userId, mTan);
	}

	public void sendNotificationsToUsers(CommonError commonError, User loginUser){
		Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
        Map<String, String> adminConfMap = null;
        adminConfMap = getAdminConfMap(commonError);
        String passwordValidation = adminConfMap.get("passwordChangeValid");
        Date tempAccountDate = loginUser.getUserExpiry();
		Date tempPasswordDate = loginUser.getLastPwdChange();
        
		
		if( tempAccountDate == null ) tempAccountDate = loginUser.getCreatedDate();
        if( tempPasswordDate == null ) tempPasswordDate = loginUser.getCreatedDate();
        long accountExpiry = ( tempAccountDate.getTime() - today.getTime())/ (1000 * 60 * 60 * 24);
        long passwordExpiry = ( today.getTime() - tempPasswordDate.getTime())/ (1000 * 60 * 60 * 24);
		try{
			if(accountExpiry <= 7 && accountExpiry >= 1){
				String notifiType = userDAO.checkUserNotification(loginUser, CommonConstants.ACCOUNT_EXPIRY_NOTIFY_WEEK);
				if( notifiType == null || "".equals(notifiType)){
					userDAO.insertUserNotification(commonError,CommonConstants.ACCOUNT_EXPIRY_NOTIFY_WEEK, loginUser);
					StringBuilder mailBodyLink = new StringBuilder();
					mailBodyLink.append(getJSFMessage(UserManagementConstants.MAIL_GREETINGS_USER)+" "+loginUser.getFirstName()+" "+loginUser.getLastName()+", \n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.ACCOUNT_EXPIRY_NOTIFICATION_WEEK));mailBodyLink.append("\n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
					sendMailToUser( commonError, loginUser, getJSFMessage(CommonConstants.ACCOUNT_EXPIRY_MAIL_SUBJECT), mailBodyLink.toString() );
				}
			}else if( accountExpiry < 1 ){
				String notifiType = userDAO.checkUserNotification(loginUser, CommonConstants.ACCOUNT_EXPIRY_NOTIFY_TODAY);
				if( notifiType == null || "".equals(notifiType)){
					userDAO.insertUserNotification(commonError,CommonConstants.ACCOUNT_EXPIRY_NOTIFY_TODAY, loginUser);
					StringBuilder mailBodyLink = new StringBuilder();
					mailBodyLink.append(getJSFMessage(UserManagementConstants.MAIL_GREETINGS_USER)+" "+loginUser.getFirstName()+" "+loginUser.getLastName()+", \n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.ACCOUNT_EXPIRY_NOTIFICATION_TODAY));mailBodyLink.append("\n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
					
					sendMailToUser( commonError, loginUser, getJSFMessage(CommonConstants.ACCOUNT_EXPIRY_MAIL_SUBJECT), mailBodyLink.toString() );
				}
			}
			
			if(passwordValidation.equals("Yes")){
			if(passwordExpiry >= 80 && passwordExpiry < 85){
				String notifiType = userDAO.checkUserNotification(loginUser, CommonConstants.PASSWORD_EXPIRY_NOTIFY_WEEK);
				if( notifiType == null || "".equals(notifiType)){
					userDAO.insertUserNotification(commonError,CommonConstants.PASSWORD_EXPIRY_NOTIFY_WEEK, loginUser);
					StringBuilder mailBodyLink = new StringBuilder();
					mailBodyLink.append(getJSFMessage(UserManagementConstants.MAIL_GREETINGS_USER)+" "+loginUser.getFirstName()+" "+loginUser.getLastName()+", \n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.PASSWORD_EXPIRY_NOTIFICATION_WEEK));mailBodyLink.append("\n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
					
					sendMailToUser( commonError, loginUser, getJSFMessage(CommonConstants.PASSWORD_EXPIRY_MAIL_SUBJECT), mailBodyLink.toString() );
				}
			}else if( passwordExpiry >= 85 ){
				
				String notifiType = userDAO.checkUserNotification(loginUser, CommonConstants.PASSWORD_EXPIRY_NOTIFY_TODAY);
				
				if( notifiType == null || "".equals(notifiType)){
					userDAO.insertUserNotification(commonError,CommonConstants.PASSWORD_EXPIRY_NOTIFY_TODAY, loginUser);
					StringBuilder mailBodyLink = new StringBuilder();
					mailBodyLink.append(getJSFMessage(UserManagementConstants.MAIL_GREETINGS_USER)+" "+loginUser.getFirstName()+" "+loginUser.getLastName()+", \n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.PASSWORD_EXPIRY_NOTIFICATION_TODAY));mailBodyLink.append("\n\n");
					mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
					
					sendMailToUser( commonError, loginUser, getJSFMessage(CommonConstants.PASSWORD_EXPIRY_MAIL_SUBJECT), mailBodyLink.toString() );
				}
			}
			}
		}catch(Exception e){
			logger.info("Error on notification"+e.getMessage());
		}
	}
	
	public void sendMailToUser(CommonError commonError, User loginUser, String mailSubject, String mailBody){
		logger.info("Sending mail to USER regarding "+mailSubject);
		try {
			boolean sessionDebug = false;
	        Properties props = System.getProperties();
	        Map<String, String> adminConfMap = adminDAO.getAdminConfMap(null);
	        String nameOfDeployment =  adminConfMap.get("nameOfDeployment");

	        props.put("mail.smtp.host", CommonConstants.SEND_MAIL_HOST);
	        props.put("mail.transport.protocol", CommonConstants.MAIL_PROTOCOL);
	       
	        Session session = Session.getDefaultInstance(props, null);
	       	session.setDebug(sessionDebug);
	        
	        Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(CommonConstants.FROM_MAIL_ID));
	        InternetAddress[] address = { new InternetAddress(loginUser.getEmail()) };
	        msg.setRecipients(Message.RecipientType.TO, address);
	        msg.setSentDate(new Date());
	        
	        msg.setSubject(mailSubject.trim()+" "+"("+nameOfDeployment+")");

	        msg.setText(mailBody);
	        
            Transport.send(msg);
	} catch (MessagingException e) {
        logger.info("Error when sending emails.  " + e.getMessage());
        commonError.addError(ForgotPasswordConstant.EMAIL_ERROR);
		}
		logger.info("Mail Sent to USER regarding "+mailSubject);
	}

	/**
	 * Insert User USB Token Registeration Details
	 */
	@Override
	public void addUserTokenAuthentication(CommonError commonError, UserUSBTokenAuthentication usbTokenDetails) {
		userDAO.addUpdateUserTokenAuthentication(commonError, usbTokenDetails );
		
	}
	
	/**
	 * Insert User USB Token Registeration Details
	 */
	@Override
	public UserUSBTokenAuthentication getUserTokenAuthentication(CommonError commonError, String userName) {
		return userDAO.getUserUSBTokenDetails(commonError, userName );
		
	}
	
	/**
	 * Insert User Google Auth Registeration Details
	 */
	@Override
	public void addGoogleAuthSecrets(CommonError commonError, User user, String secretKey) {
		userDAO.addGoogleAuthSecrets(commonError, user, secretKey );
		
	}
	
	/**
	 * get User Google Auth Registeration Details
	 */
	@Override
	public String getGoogleAuthSecrets(User user) {
		return userDAO.getGoogleAuthSecrets(user);
		
	}
	
	public boolean sendGoogleAuthPin(CommonError commonError, User user){
		String googleAuthPin = null;
		if(CommonConstants.A.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
			googleAuthPin = RandomStringUtils.randomAlphabetic(CommonConstants.MTAN_LENGTH);
		}else if(CommonConstants.AN.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
			googleAuthPin = RandomStringUtils.randomAlphanumeric(CommonConstants.MTAN_LENGTH);
		}else if(CommonConstants.NUM.equalsIgnoreCase(CommonConstants.MTAN_TYPE)){
			googleAuthPin = RandomStringUtils.randomNumeric(CommonConstants.MTAN_LENGTH);
		}
		user.setRegistrationPin(googleAuthPin);
		userDAO.addGoogleAuthSecrets(commonError, user, null);
			try {
				StringBuilder mailBodyLink = new StringBuilder();
				mailBodyLink.append(getJSFMessage(CommonConstants.GOOGLE_AUTH_BODY));
				mailBodyLink.append(googleAuthPin);mailBodyLink.append("\n");
				mailBodyLink.append(getJSFMessage(CommonConstants.MTAN_THANKS));
				
				sendMailToUser( commonError, user, getJSFMessage(CommonConstants.GOOGLE_AUTH_MAIL_SUB), mailBodyLink.toString() );
	           
			 } catch (Exception e) {
	            logger.info("Error when sending emails.  " + e.getMessage());
	            commonError.addError(ForgotPasswordConstant.EMAIL_ERROR);
				return false;
	        }
		return true;
	}
	
}
