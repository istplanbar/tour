package com.orb.admin.client.service.impl;

import java.util.Locale;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.orb.common.client.constants.CommonConstants;

public abstract class AbstractAdminService{
	
	@Autowired 
	ReloadableResourceBundleMessageSource bundle;
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	protected Object getSessionObj(String key){
		FacesContext fc = FacesContext.getCurrentInstance();
		if( fc != null ){
			ExternalContext externalContext = fc.getExternalContext();
	    	Map<String, Object> sessionMap = externalContext.getSessionMap();
	    	if( sessionMap != null )
	    		return sessionMap.get(key);
		}
		return null;
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	protected String getJSFMessage( String key ){
		try{
			Locale locale = (Locale)getSessionObj(CommonConstants.USER_LOCALE);
			return bundle.getMessage(key, null, locale);
		}catch(Exception e){
			return key;
		}
	}
}
