package com.orb.admin.client.model;

import java.util.List;
import java.util.Map;
import com.orb.admin.moduleactivity.model.ModuleActivity;;
public class UsersModuleActivity {

	private String userId;
	private String roles;
	private Map<MetaTable,List<ModuleActivity>> moduleActivities;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoles() {
		return roles;
	}
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public Map<MetaTable, List<ModuleActivity>> getModuleActivities() {
		return moduleActivities;
	}
	public void setModuleActivities(Map<MetaTable, List<ModuleActivity>> moduleActivities) {
		this.moduleActivities = moduleActivities;
	}

}
