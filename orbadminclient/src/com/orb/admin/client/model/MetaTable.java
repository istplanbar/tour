package com.orb.admin.client.model;

import java.util.ArrayList;
import java.util.List;

import com.orb.admin.moduleactivity.model.ModuleActivity;

public class MetaTable {
	
	private int pageId;
	private String pageDesc;
	private int parentPage;
	private String url;
	private String icon;
	private String imageName;
	private String parentPageDesc;
	private String popupMenu;
	private String printConfig;
	private String checked;
	private List<ModuleActivity> activityList;
	private String active;
	private String activityIdentifier;
	private String identifier;
	private String activity;
	private boolean subLock;
	private String childModuleConfig;
	private String childModParentPage;
	List<MetaTable> metaTableList;
	
	public String getPopupMenu() {
		return popupMenu;
	}
	public void setPopupMenu(String popupMenu) {
		this.popupMenu = popupMenu;
	}
	public String getPrintConfig(){
		return printConfig;
	}
	public void setPrintConfig(String printConfig){
		this.printConfig = printConfig;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public String getPageDesc() {
		return pageDesc;
	}
	public void setPageDesc(String pageDesc) {
		this.pageDesc = pageDesc;
	}
	public int getParentPage() {
		return parentPage;
	}
	public void setParentPage(int parentPage) {
		this.parentPage = parentPage;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public List<MetaTable> getMetaTableList() {
		if(metaTableList == null){
			metaTableList = new ArrayList<MetaTable>();
		}
		return metaTableList;
	}
	public void setMetaTableList(List<MetaTable> metaTableList) {
		this.metaTableList = metaTableList;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getParentPageDesc() {
		return parentPageDesc;
	}
	public void setParentPageDesc(String parentPageDesc) {
		this.parentPageDesc = parentPageDesc;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
		
	public List<ModuleActivity> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<ModuleActivity> activityList) {
		this.activityList = activityList;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getActivityIdentifier() {
		return activityIdentifier;
	}
	public void setActivityIdentifier(String activityIdentifier) {
		this.activityIdentifier = activityIdentifier;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public boolean isSubLock() {
		return subLock;
	}
	public void setSubLock(boolean subLock) {
		this.subLock = subLock;
	}
	public String getChildModuleConfig() {
		return childModuleConfig;
	}
	public void setChildModuleConfig(String childModuleConfig) {
		this.childModuleConfig = childModuleConfig;
	}
	public String getChildModParentPage() {
		return childModParentPage;
	}
	public void setChildModParentPage(String childModParentPage) {
		this.childModParentPage = childModParentPage;
	}
	
}
