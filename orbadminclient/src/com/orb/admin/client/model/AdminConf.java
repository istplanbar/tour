package com.orb.admin.client.model;

public class AdminConf {
	
	private String adminKey;
	private String adminValue;
	
	public String getAdminKey() {
		return adminKey;
	}
	public void setAdminKey(String adminKey) {
		this.adminKey = adminKey;
	}
	public String getAdminValue() {
		return adminValue;
	}
	public void setAdminValue(String adminValue) {
		this.adminValue = adminValue;
	}
}
