package com.orb.admin.client.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.roleadministration.model.Role;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class AbstractAdminDAO extends AdminDAO {
	
	private static Log logger = LogFactory.getLog(AbstractAdminDAO.class);
	
	/**
	 * 
	 * @param error
	 * @param mainModules
	 * @return
	 */
	public List<MetaTable> getMainModules(CommonError error, List<MetaTable> mainModulesTarget) {
		logger.info(" ==> get MainModule START <== ");
		if( mainModulesTarget == null || mainModulesTarget.isEmpty() ){
			logger.info(" User doesn't select values ");
			return null;
		}
		
		List<Integer> targetMainMenus = new ArrayList<Integer>();
		for(MetaTable targetMainMenu : mainModulesTarget) {
			targetMainMenus.add(targetMainMenu.getPageId());
		}
		
		List<MetaTable> mainMenuList = null;
		int size = mainModulesTarget.size();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(" =1 ");
			qry.append(" AND ");qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" NOT IN ( ");
			for ( int i=0; i<size; i++ ) {
				qry.append(" ? ");qry.append(",");
			}
			qry.setLength(qry.length() - 1);
			qry.append(") ");
			mainMenuList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), targetMainMenus.toArray(),  new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		}catch( Exception e ){
			logger.info("Error when retrieve metaTable List.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> get MainModule END <== ");
		return mainMenuList;
	}
	
	/**
	 * Get sub menu with permission id
	 * @param error
	 * @param userId
	 * @return
	 */
	public HashMap<Integer, String> getModulePermissions (int userId){
		logger.info(" ==> get sub modules with permission START <== ");
		List<Role> subModuleList = null;
		HashMap<Integer, String> subModuleMap = null;
		try{
			StringBuffer qrySelect = new StringBuffer("SELECT ORD.");
			qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));qrySelect.append(" , OP.");
			qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_NAME"));
			qrySelect.append(" FROM ");qrySelect.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);qrySelect.append(" AS ORD");
			qrySelect.append(" INNER JOIN ");qrySelect.append(AdminDBConstants.TBL_META_TABLE);qrySelect.append(" AS OMT ON");
			qrySelect.append(" (ORD.");qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));qrySelect.append(" = OMT.");
			qrySelect.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qrySelect.append(") INNER JOIN ");qrySelect.append(AdminDBConstants.TBL_PERMISSION_TABLE);qrySelect.append(" AS OP ON");
			qrySelect.append(" (ORD.");qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_ID"));qrySelect.append(" = OP.");
			qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_ID"));
			qrySelect.append(") WHERE ORD.");qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qrySelect.append(" in (");
			qrySelect.append(" SELECT ");qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qrySelect.append(" FROM ");
			qrySelect.append(AdminDBConstants.TBL_USER_ROLE);qrySelect.append(" WHERE ");
			qrySelect.append(AdminDBConstants.USER_ROLE.get("USER_ID"));qrySelect.append(" =? )");
			qrySelect.append(" GROUP BY (ORD.");
			qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));qrySelect.append(") ORDER BY ORD.");
			qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("PERMISSION_ID"));qrySelect.append(" DESC ");
			subModuleList = (List<Role>)getJdbcTemplate().query(qrySelect.toString(), new Object[] { userId }, new BeanPropertyRowMapper<Role>(Role.class));
			subModuleMap = new HashMap<Integer,String>();
			for(Role subModule : subModuleList){
				subModuleMap.put(subModule.getSubmenuId(), subModule.getPermissionName());
			}
		}catch( Exception e ){
			logger.error("Error when retrieve sub menus List with permission.  " + e.getMessage());
		}
		logger.info(" ==> get sub modules with permission  END <== ");
		return subModuleMap;
	}
	
	/**
	 * Get Custom Fields based on the user layout and table
	 * @param error
	 * @param roleValue
	 * @return
	 */
	public HashMap<String,String> getCustomFields (CommonError error, Role roleValue, String tableName){
		logger.info(" ==> getCustom Table Fields START <== ");
		List<Layout> tableFieldList = null;
		HashMap<String,String> tableFieldsMap = new HashMap<String,String>();
		try{
			StringBuffer qrySelect = new StringBuffer("SELECT ");
			qrySelect.append(" REPLACE(CORE_ORB_FIRST_UPPER_CASE(");
			qrySelect.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("FIELD_NAME"));qrySelect.append("),'_','') AS ");
			qrySelect.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("FIELD_NAME"));qrySelect.append(" , ");
			qrySelect.append("( SELECT ");qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_NAME"));
			qrySelect.append(" FROM ");qrySelect.append(AdminDBConstants.TBL_PERMISSION_TABLE);qrySelect.append(" WHERE ");
			qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_ID"));qrySelect.append(" =? )");
			qrySelect.append(" AS ");qrySelect.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_NAME"));
			qrySelect.append(" FROM ");qrySelect.append(AdminDBConstants.TBL_LAYOUT_DESC_TABLE);qrySelect.append(" WHERE ");
			qrySelect.append(AdminDBConstants.LAYOUT_DESC_TABLE.get("NAME_OF_THE_TABLE"));qrySelect.append(" =? ");qrySelect.append(" AND ");
			qrySelect.append(AdminDBConstants.ROLE_DESC_TABLE.get("LAYOUT_ID"));qrySelect.append(" =? ");
			tableFieldList = (List<Layout>)getJdbcTemplate().query(qrySelect.toString(), new Object[] { roleValue.getPermissionId(), tableName, roleValue.getLayoutId() }, new BeanPropertyRowMapper<Layout>(Layout.class));
			for(Layout layout:tableFieldList){
				if(null != layout){
					tableFieldsMap.put(layout.getFieldName(), layout.getPermissionName());
				}
			}
		}catch( Exception e ){
			logger.error("Error when getCustom Table Fields.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getCustom Table Fields END <== ");
		return tableFieldsMap;
		}
	
}
