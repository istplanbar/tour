package com.orb.admin.client.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.orb.admin.client.model.AdminConf;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class AdminDAO extends AbstractDAO {
	private static Log logger = LogFactory.getLog(AdminDAO.class);

	/**
	 * This method returns the admin configuration map.
	 * @param error
	 * @return
	 */
	public Map<String, String> getAdminConfMap(CommonError error) {
		logger.info(" ==> getAdminConfMap START <== ");
		List<AdminConf> adminConfList;
		Map<String, String> adminConfMap = new HashMap<String, String>();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.ADMIN_CONF.get("ADMIN_KEY"));
			qry.append(" , ");
			qry.append(AdminDBConstants.ADMIN_CONF.get("ADMIN_VALUE"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ADMIN_CONF);
			adminConfList = (List<AdminConf>)getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<AdminConf>(AdminConf.class));
			for(AdminConf adminConf : adminConfList){
				adminConfMap.put(adminConf.getAdminKey(), adminConf.getAdminValue());
			}
		}catch( Exception e ){
			logger.info("Error when retrieve AdminConf Map.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getAdminConfMap END <== ");
		return adminConfMap;
	}
	
	public Map<Integer, MetaTable> getMetaTableList(CommonError error){
		logger.info(" ==> getMetaTableList START <== ");
		List<MetaTable> metaTableList = new ArrayList<MetaTable>();
		Map<Integer, MetaTable> metaTableMap = new LinkedHashMap<Integer, MetaTable>();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("URL"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("ICON"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("IMAGE_NAME"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("POPUP_MENU"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);
			qry.append(" WHERE ACTIVE = 'Y'");
			metaTableList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
			for(MetaTable metaTable : metaTableList){
				metaTableMap.put(metaTable.getPageId(), metaTable);
			}
		}catch( Exception e ){
			logger.info("Error when retrieve metaTable List.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMetaTableList END <== ");
		return metaTableMap;
	}

	/**
	 * UPDATE `kurverdb`.`CORE_ORB_ADMIN_CONF` SET `ADMIN_VALUE`='en' WHERE `ADMIN_KEY`='defaultLocale';
	 * @param adminKey
	 * @param adminValue
	 * @param error
	 */
	public void updateAdminConf(String adminKey, String adminValue, CommonError error) {
		logger.info(" ==> updateAdminConf START <== ");
		try{
			StringBuffer qry = new StringBuffer("UPDATE ");
			qry.append(AdminDBConstants.TBL_ADMIN_CONF);
			qry.append(" SET ");
			qry.append(AdminDBConstants.ADMIN_CONF.get("ADMIN_VALUE"));
			qry.append(" = ? WHERE ");
			qry.append(AdminDBConstants.ADMIN_CONF.get("ADMIN_KEY"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), new Object[]{adminValue, adminKey});
		}catch( Exception e ){
			logger.info("Error when updating AdminConf Map.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateAdminConf END <== ");
		
	}

	public List<MetaTable> getMenus(CommonError error) {
		logger.info(" ==> getMetaTableList START <== ");
		List<MetaTable> menuList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("URL"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("ICON"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("ACTIVE"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.META_TABLE.get("ACTIVE"));qry.append(" = ");
			qry.append("'"+CommonConstants.Y+"'");qry.append(" AND ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append("= 1 ");
			menuList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		}catch( Exception e ){
			logger.info("Error when retrieve metaTable List.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMetaTableList END <== ");
		return menuList;
	}

	public List<MetaTable> getSubMenus(CommonError error, int id) {
		logger.info(" ==> getMetaTableList START <== ");
		List<MetaTable> subMenuList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("URL"));
			qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("ICON"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append("=? ");
			subMenuList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new Object[]{id},  new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		}catch( Exception e ){
			logger.info("Error when retrieve metaTable List.  " + e.getMessage());
			if(error != null)
				error.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMetaTableList END <== ");
		return subMenuList;
	}

	public List<Integer> getFilteredModuleList(CommonError error,List<String> roleIdentifier) {
		logger.info(" ==> getFilteredModuleList START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		SqlParameterSource namedParameters = new MapSqlParameterSource("roleIds", roleIdentifier);
		List<Integer> filteredModuleList = new ArrayList<Integer>();
		try{
			String qry = "select distinct PARENT_PAGE from CORE_ORB_META_TABLE where PAGE_ID in (select PAGE_ID from CORE_ORB_ROLE_MOD_MAPPING where IDENTIFIER in (:roleIds) ) union select PAGE_ID from CORE_ORB_ROLE_MOD_MAPPING where IDENTIFIER in (:roleIds)";
			filteredModuleList = (List<Integer>) db.queryForList(qry, namedParameters,Integer.class);
		}catch( Exception e ){
			logger.info("Error when retrieve ModuleList List.  " + e.getMessage());
			filteredModuleList = new ArrayList<Integer>(); 
		}
		logger.info(" ==> getFilteredModuleList END <== ");
		return filteredModuleList;
	}

	public List<MetaTable> getMainModuleList(CommonError error, List<String> roleIdentifier) {
		logger.info(" ==> getMainModuleList START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		SqlParameterSource namedParameters = new MapSqlParameterSource("roleIds", roleIdentifier);
		List<MetaTable> filteredMainModuleModuleList = new ArrayList<MetaTable>();
		try{
		String qry = "select PAGE_ID,PAGE_DESC,IMAGE_NAME, URL from CORE_ORB_META_TABLE where PARENT_PAGE = 1 and ACTIVE = 'Y' and PAGE_ID in (select PARENT_PAGE from CORE_ORB_META_TABLE where PAGE_ID in (select PAGE_ID from CORE_ORB_ROLE_MOD_MAPPING where IDENTIFIER in (:roleIds) ) union all select PAGE_ID from CORE_ORB_ROLE_MOD_MAPPING where IDENTIFIER in (:roleIds))";
		filteredMainModuleModuleList = (List<MetaTable>) db.query(qry, namedParameters, new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		}catch( Exception e ){
			logger.info("Error when retrieve MainModule List.  " + e.getMessage());
			filteredMainModuleModuleList = new ArrayList<MetaTable>(); 
		}
		logger.info(" ==> getMainModuleList END <== ");
		return filteredMainModuleModuleList;
	}
	
	
}
