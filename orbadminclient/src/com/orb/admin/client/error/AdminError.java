package com.orb.admin.client.error;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.error.CommonError;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class AdminError extends CommonError{
	
}
