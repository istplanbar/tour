package com.orb.admin.useradministration.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.admin.client.model.UserUSBTokenAuthentication;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.forgotpassword.model.ForgotPass;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.constants.UserManagementConstants;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.constants.CommonDBConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class UserDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(UserDAO.class);

	/**
	 * Method for get login user details
	 * @param userId
	 * @return
	 */
	public User getLoginUser( String userId ){
		logger.info("==> getLoginUser STARTS <== ");
		if (CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't get login details");
			return null;
		}
		User user = null;
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT ");
			qry.append(CommonDBConstants.USER.get("ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("PASSWORD"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("FIRST_NAME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_NAME"));qry.append(", ");			
			qry.append(CommonDBConstants.USER.get("EMAIL"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("INITIALS"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("PHONE_NUMBER"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LANGUAGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("THEME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("TOP_MENU"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_EXPIRY"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("START_DATE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" , ");			
			qry.append(CommonDBConstants.USER.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("ROLE_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("TIME_ZONE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_IMAGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("CREATED_DATE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LOGIN_FAILURE_COUNT"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("WS_USER_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("WS_PASSWORD"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_MODIFIED_DATE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DEPUTY"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISABLE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("IS_PASSWORD_CHECKED"));
			qry.append(" FROM ");
			qry.append(CommonDBConstants.TBL_USER);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			qry.append(" AND ");
			qry.append(CommonDBConstants.USER.get("ACTIVE"));
			qry.append(" = ?");
			user = (User)getJdbcTemplate().queryForObject(qry.toString(), new Object[] { userId, CommonConstants.Y }, new BeanPropertyRowMapper(User.class));
		}catch( Exception e ){
			logger.info("Error when retrieve User Details.  " + e.getMessage());
		}
		if (null == user) {
			logger.error(" Query returns Null ");
		}
		logger.info(" ==> getLoginUser ENDS <== ");
		return user;
	}
		
	/**
	 * Method for set theme
	 * @param newTheme
	 * @param userId
	 */
	public void setTheme(String newTheme, String userId) {
		logger.info("==> setTheme STARTS <== ");
		if (CommonUtils.getValidString(newTheme).isEmpty() || CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't set Theme");
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");
			qry.append(CommonDBConstants.TBL_USER);
			qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("THEME"));
			qry.append(" = ?");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), new Object[] { newTheme, userId });
		}catch( Exception e ){
			logger.error("Error when Updating User Theme Details.  " + e.getMessage());
		}
		logger.info("==> setTheme ENDS <== ");
	}

	/**
	 * Method for set Top Menu
	 * @param topMenu
	 * @param userId
	 */
	public void setTopMenu(boolean topMenu, String userId) {
		logger.info("==> setTopMenu STARTS <== ");
		if (CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't setTopMenu");
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");
			qry.append(CommonDBConstants.TBL_USER);
			qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("TOP_MENU"));
			qry.append(" = ?");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), new Object[] { topMenu, userId });
		}catch( Exception e ){
			logger.error("Error when setTopMenu details.  " + e.getMessage());
		}
		logger.info("==> setTopMenu ENDS <== ");
	}

	/**
	 * Method for update Password
	 * @param newPass
	 * @param userId
	 */
	public void updatePassword(String newPass, String userId) {
		logger.info("==> updatePassword STARTS <== ");
		if (CommonUtils.getValidString(newPass).isEmpty() || CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't updatePassword");
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");
			qry.append(CommonDBConstants.TBL_USER);
			qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("PASSWORD"));
			qry.append(" = ?");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), new Object[] { newPass, userId });
		}catch( Exception e ){
			logger.error("Error when Updating User Password.  " + e.getMessage());
		}
		logger.info("==> updatePassword ENDS <== ");

	}

	/**
	 * Method for update Profile Settings
	 * @param commonError
	 * @param userId
	 * @param loginUser
	 */
	public void updateProfileSettings(CommonError commonError, String userId, User loginUser) {
		logger.info("==> update user profileSettings STARTS <== ");
		if (CommonUtils.getValidString(userId).isEmpty() || null == loginUser){
			logger.error("Can't updateProfileSettings");
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_USER);
			qry.append(" SET "); qry.append(CommonDBConstants.USER.get("FIRST_NAME")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("LAST_NAME")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("EMAIL")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("INITIALS")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("LANGUAGE")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("USER_IMAGE")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("WS_USER_ID")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("WS_PASSWORD")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DEPUTY")); qry.append(" = ? ");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			List<Object> args = new ArrayList<>();
			args.add( loginUser.getFirstName());args.add(loginUser.getLastName() );args.add( loginUser.getEmail());args.add( loginUser.getInitials());
			args.add(loginUser.getLanguage() );args.add(loginUser.getDisplayRowsSize() );args.add(loginUser.getDisplayDateFormat() );
			args.add(loginUser.getUserImage() );args.add(loginUser.getAuthenticateUser());
			args.add(loginUser.getWsUserId() );args.add(loginUser.getWsPassword() );
			args.add(loginUser.getDeputy());args.add( userId );
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when update User Profile.  " + e.getMessage());
			if(commonError != null)
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info("==> update user profileSettings ENDS <== ");
	}
	
	/**
	 * Method for create Role
	 * @param userError
	 * @return
	 */
	public List<Role> createRole(CommonError userError) {
		logger.info(" ==> Insert Role START <== ");
		List<Role> roleList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);
			qry.append(" ORDER BY ");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));
			roleList = (List<Role>)getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.error("Error when insert a role.  " + e.getMessage());
			if(userError != null)
				userError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Role END <== ");
		return roleList;
	}
	
	/**
	 * Method for fetch Roles
	 * @param commonError
	 * @param roles 
	 * @return
	 */
	public List<Role> fetchRoles(CommonError commonError, List<Integer> roles,String flag) {
		logger.info(" ==> fetchRoles START <== ");
		if (null == roles){
			logger.error("Can't fetchRoles");
		}
		int size = 0;
		List<Role> roleList = null;
		if (null != roles){
			size = roles.size();
		}
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);
			qry.append(" WHERE ");
			if (null != roles) {
				qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(" NOT IN ");
				qry.append(" ( ");
				for ( int i=0; i<size; i++ ) {
					qry.append(" ? ");qry.append(",");
				}
				qry.setLength(qry.length() - 1);
				qry.append(") ");
				qry.append(" AND ");
			}
			qry.append(AdminDBConstants.ROLE_TABLE.get("ACTIVE"));qry.append(" =? ");
			qry.append(" ORDER BY ");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));
			if (null == roles){
				roleList = (List<Role>)getJdbcTemplate().query(qry.toString(),new Object[] { flag }, new BeanPropertyRowMapper<Role>(Role.class));
			}else {
				roleList = (List<Role>)getJdbcTemplate().query(qry.toString(), new Object[] {roles.toArray(), flag }, new BeanPropertyRowMapper<Role>(Role.class));
			}

		}catch( Exception e ){
			logger.error("Error when fetch the roles.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == roleList) {
			logger.error(" Query returns NULL ");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> fetchRoles END <== ");
		return roleList;
	}
	
	/**
	 * Method for create new user
	 * @param commonError
	 * @param user
	 * @param loginUser
	 * @return 
	 */
	public Integer createUser(CommonError commonError, User user, User loginUser) {
		logger.info(" ==> Insert user  START <== ");
		if( null == user){
			logger.error("Can't create new user");
			return null;
		}
		int nextRowId = 0;
		try{
			nextRowId = Integer.parseInt(getNextRowId( CommonDBConstants.TBL_USER));
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			List<Object> args = new ArrayList<>();
			args.add(user.getUserId());
			args.add(user.getPassword());
			args.add(user.getFirstName());
			args.add(user.getLastName());
			args.add(user.getEmail());
			args.add(user.getInitials());
			args.add(user.getPhoneNumber());
			args.add(user.getUserImage());
			args.add(user.getUserExpiry());
			args.add(user.getStartDate());
			args.add(user.getDisplayRowsSize());
			args.add(user.getLanguage());
			args.add(user.getDisplayDateFormat());
			args.add(user.getTimeZone());
			args.add(user.getAuthenticateUser());
			args.add(user.getSecondLevelAuthUserPrivilege());
			args.add(user.getRoleId());
			args.add(date);
			args.add(date);
			args.add(user.getIsPasswordChecked());
			args.add(loginUser.getUserId());args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(CommonConstants.Y);
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(CommonDBConstants.TBL_USER);queryInsert.append("( ");
			queryInsert.append(CommonDBConstants.USER.get("USER_ID"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("PASSWORD"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("FIRST_NAME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("LAST_NAME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("EMAIL"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("INITIALS"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("PHONE_NUMBER"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("USER_IMAGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("USER_EXPIRY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("START_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("LANGUAGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("TIME_ZONE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("ROLE_ID"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("CREATED_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("LAST_MODIFIED_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("IS_PASSWORD_CHECKED"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("CREATE_BY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("CREATE_TIME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("UPDATE_BY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("UPDATE_TIME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("ACTIVE"));
						
			queryInsert.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
			
		}catch( Exception e ){
			if(e instanceof DuplicateKeyException ){
				nextRowId = 0;
				if(e.getMessage().contains("USER_ID_UNIQUE")){
					commonError.addError(UserManagementConstants.USER_ID_UNIQUE_ERROR);
				}else{
					commonError.addError(UserManagementConstants.USER_EMAIL_UNIQUE_ERROR);
				}
			}else{
				logger.error("Error when Insert user Creation.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
		}
		logger.info(" ==> Insert user END <== ");
		return nextRowId;
	}

	/**
	 * Method for insert roles to user
	 * @param commonError
	 * @param roleList
	 * @param userRowId
	 */
	public void addRolesToUser(CommonError commonError, List<Role> roleList, Integer userRowId) {
		logger.info(" ==> Insert UserRole Creation START <== ");
		if( null == roleList || null == userRowId){
			logger.error("Can't create new UserRole");
		}else{
			try{
				StringBuffer queryInsert = new StringBuffer();
				queryInsert.append("INSERT INTO ");
				queryInsert.append(AdminDBConstants.TBL_USER_ROLE);queryInsert.append("( ");
				queryInsert.append(AdminDBConstants.USER_ROLE.get("USER_ID"));queryInsert.append(" , ");
				queryInsert.append(AdminDBConstants.USER_ROLE.get("ROLE_ID"));
				queryInsert.append(") VALUES(?,?) ");
				getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {

					public void setValues(PreparedStatement ps, int i) throws SQLException {	
						ps.setLong(1, userRowId);
						ps.setLong(2, roleList.get(i).getRoleId());
					}		
					public int getBatchSize() {
						return roleList.size();
					}
				});
			}catch( Exception e ){
				logger.error("Error when Insert UserRole Creation.  " + e.getMessage());
				if(commonError != null)
					commonError.addError(CommonConstants.DB_ERROR);
			}
		}
		logger.info(" ==> Insert UserRole Creation END <== ");
	}

	/**
	 * Method for get user list
	 * @param commonError
	 * @param activeFlag
	 * @return
	 */
	public List<User> getUserList(CommonError commonError, String activeFlag) {
		logger.info(" ==> getRoleList START <== ");
		List<User> userList = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(CommonDBConstants.USER.get("ID"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("USER_ID"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("PASSWORD"));query.append(", ");
			query.append(CommonDBConstants.USER.get("FIRST_NAME"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("LAST_NAME"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("EMAIL"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("LANGUAGE"));query.append(", ");
			query.append(CommonDBConstants.USER.get("THEME"));query.append(", ");
			query.append(CommonDBConstants.USER.get("TOP_MENU"));query.append(", ");
			query.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));query.append(", ");
			query.append(CommonDBConstants.USER.get("USER_EXPIRY"));query.append(", ");
			query.append(CommonDBConstants.USER.get("ROLE_ID"));query.append(", ");
			query.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));query.append(", ");
			query.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));query.append(", ");
			query.append(CommonDBConstants.USER.get("USER_IMAGE"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("CREATE_BY"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("CREATE_TIME"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("UPDATE_BY"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("UPDATE_TIME"));query.append(" , ");
			query.append(CommonDBConstants.USER.get("ACTIVE") );
			query.append(" FROM ");query.append(CommonDBConstants.TBL_USER);
			query.append(" WHERE ");query.append(CommonDBConstants.USER.get("ACTIVE"));query.append(" = ? ");
			query.append(" ORDER BY ");query.append(AdminDBConstants.ROLE_TABLE.get("USER_ID"));
			userList = getJdbcTemplate().query( query.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(User.class) );
		}catch( Exception e ){
			logger.info("Error when retrieve getRoleList.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getRoleList END <== ");
		return userList;
	}

	/**
	 * Method for get user roles
	 * @param commonError
	 * @param Id
	 * @return
	 */
	public List<Role> getUserRoles(CommonError commonError, Integer Id ){
		logger.info(" ==> get UserRoles List START <== ");
		if( null == Id){
			logger.error("Can't fetch UserRoles");
			return null;
		}
		List<Role> userRoles = null;
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT ");
			qry.append(" U.");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(", ");
			qry.append(" U.");qry.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));qry.append(", ");
			qry.append(" U.");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));
			qry.append(" FROM ");
			qry.append(AdminDBConstants.TBL_USER_ROLE);qry.append(" AS UR ");
			qry.append(" INNER JOIN ");
			qry.append(AdminDBConstants.TBL_ROLE_TABLE);qry.append(" AS U ");
			qry.append(" ON ( ");
			qry.append(" UR.");qry.append(AdminDBConstants.USER_ROLE.get("ROLE_ID"));qry.append(" = ");
			qry.append(" U.");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(" )");
			qry.append(" WHERE ");qry.append(" UR.");qry.append(AdminDBConstants.USER_ROLE.get("USER_ID"));
			qry.append(" = ?");
			userRoles = (List<Role>)getJdbcTemplate().query(qry.toString(), new Object[] { Id }, new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.info("Error when retrieve get UserRoles List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> get UserRoles List END <== ");
		return userRoles;
	}

	/**
	 * Method for delete user roles
	 * @param commonError
	 * @param id
	 */
	public void deleteUserRole(CommonError commonError, List<Integer> ids) {
		logger.info("ids: "+ids);
		logger.info(" ==> delete User Roles START <== ");
		if( ids == null){
			logger.error("Can't delete user roles");
			return;
		}
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", ids);
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(AdminDBConstants.TBL_USER_ROLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.USER_ROLE.get("USER_ID"));
			qry.append(" IN ");qry.append(" (:ids) ");
			db.update(qry.toString(),params);
			
		}catch( Exception e ){
			logger.info("Error when Delete User Roles.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> delete User Roles END <== ");
	}

	/**
	 * Method for update user
	 * @param commonError
	 * @param user
	 * @param resetPass
	 * @param loginUser
	 */
	public void updateUser(CommonError commonError, User user, boolean resetPass, User loginUser ) {
		logger.info(" ==> update User START <== ");
		List<Integer> ids = new ArrayList<>();
		if( null == user ){
			logger.error("Error when updating user data start");
			return;
		}
		try{
			ids.add(user.getId());
			insertUserHistory( commonError, loginUser,ids, CommonConstants.UPDATED);
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_USER);qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("FIRST_NAME")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("LAST_NAME")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("EMAIL")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("INITIALS")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("PHONE_NUMBER"));qry.append(" = ?, ");
			if(resetPass){
				qry.append(CommonDBConstants.USER.get("PASSWORD"));qry.append(" = ?, ");
				qry.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));qry.append(" = now(), ");
			}
			if(true ==user.getIsPasswordChecked().booleanValue()){
				qry.append(CommonDBConstants.USER.get("PASSWORD"));qry.append(" = ?, ");
			}
			qry.append(CommonDBConstants.USER.get("LANGUAGE")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("USER_IMAGE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("USER_EXPIRY")); qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("START_DATE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("TIME_ZONE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("ROLE_ID"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("DISABLE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("IS_PASSWORD_CHECKED"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("UPDATE_BY"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER.get("UPDATE_TIME"));qry.append(" = ? ");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("ID"));
			qry.append(" = ?");
			List<Object> args = new ArrayList<>();
			args.add(user.getFirstName());
			args.add(user.getLastName());
			args.add(user.getEmail());
			args.add(user.getInitials());
			args.add(user.getPhoneNumber());
			if(resetPass){
				args.add(user.getPassword());
			}
			if(true ==user.getIsPasswordChecked().booleanValue()){
				args.add(user.getPassword());
			}
			args.add(user.getLanguage());
			args.add(user.getDisplayRowsSize());
			args.add(user.getUserImage());
			args.add(user.getUserExpiry());
			args.add(user.getStartDate());
			args.add(user.getDisplayDateFormat());
			args.add(user.getTimeZone());
			args.add(user.getAuthenticateUser());
			args.add(user.getSecondLevelAuthUserPrivilege());
			args.add(user.getRoleId());
			args.add(user.getDisable());
			args.add(user.getIsPasswordChecked());
			args.add(loginUser.getUserId());
			args.add(date);
			args.add(user.getId());
			getJdbcTemplate().update(qry.toString(), args.toArray());

		}catch( Exception e ){
			if(e instanceof DuplicateKeyException ){
				if(e.getMessage().contains("USER_ID_UNIQUE"))
					commonError.addError(UserManagementConstants.USER_ID_UNIQUE_ERROR);
				else
					commonError.addError(UserManagementConstants.USER_EMAIL_UNIQUE_ERROR);
				
			}else{
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info("Error when update User Profile.  " + e.getMessage());
		}
		logger.info(" ==> update User End <== ");
	}

	/**
	 * Method for get user details
	 * @param id
	 * @return
	 */
	public User getUser( String id ){
		logger.info(" ==> Get User START <== ");
		if (CommonUtils.getValidString(id).isEmpty()){
			logger.error("Can't get user details");
			return null;
		}
		User user = null;
		try{
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT ");
			qry.append(CommonDBConstants.USER.get("ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("PASSWORD"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("FIRST_NAME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_NAME"));qry.append(", ");			
			qry.append(CommonDBConstants.USER.get("EMAIL"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("INITIALS"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("PHONE_NUMBER"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LANGUAGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("THEME"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("TOP_MENU"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_EXPIRY"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("START_DATE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("ROLE_ID"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_ROWS_SIZE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISPLAY_DATE_FORMAT"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("TIME_ZONE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("CREATED_DATE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("USER_IMAGE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("DISABLE"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("IS_PASSWORD_CHECKED"));qry.append(", ");
			qry.append(CommonDBConstants.USER.get("CREATE_BY"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("CREATE_TIME"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("UPDATE_BY"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("UPDATE_TIME"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("ACTIVE"));
			qry.append(" FROM ");
			qry.append(CommonDBConstants.TBL_USER);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("ID"));
			qry.append(" = ?");
			user = (User)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{id}, new BeanPropertyRowMapper<User>(User.class));
		}catch( Exception e ){
			logger.info("Error when retrieve User Details.  " + e.getMessage());
		}
		logger.info(" ==> Get User END <== ");
		return user;
	}

	/**
	 * Insert Forgot password Details
	 * @param commonError
	 * @param forgotPass
	 * @return
	 */
	public Boolean insertForgotPassDetails(CommonError commonError, ForgotPass forgotPass) {
		logger.info(" ==> Forgot Password Insert START <== ");
		Boolean successFlag= true;
		if( null == forgotPass ){
			logger.info("Can't insert forgot password values");
			successFlag= false;
		}else{
			try{
				List<Object> args = new ArrayList<>();
				args.add(forgotPass.getUserId());
				args.add(forgotPass.getHashValue());
				args.add(forgotPass.getTempPassword());

				StringBuffer queryInsert = new StringBuffer();
				queryInsert.append("INSERT INTO ");
				queryInsert.append(CommonDBConstants.TBL_FORGOT_PASS);queryInsert.append("( ");
				queryInsert.append(CommonDBConstants.FORGOT_PASS.get("USER_ID"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.FORGOT_PASS.get("HASH_VALUE"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.FORGOT_PASS.get("TEMP_PASSWORD"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.FORGOT_PASS.get("EXPIRE_TIME"));
				queryInsert.append(") VALUES(?,?,?,DATE_ADD(now(), INTERVAL 24 HOUR)) ");

				getJdbcTemplate().update(queryInsert.toString(), args.toArray());
			}catch( Exception e ){
				logger.info("Error when Insert forgot password values.  " + e.getMessage());
				if(commonError != null)
					commonError.addError(CommonConstants.DB_ERROR);
					successFlag = false;
			}
			logger.info(" ==> Forgot Password Insert END <== ");
		}
		return successFlag;

	}

	/**
	 * This method fetch forgot password details based on hash value
	 * @param commonError
	 * @param hashValue
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ForgotPass fetchForgotTable(CommonError commonError, String hashValue) {
		logger.info(" ==> get id, tempPass, userId and hash START <== ");
		if(CommonUtils.getValidString(hashValue).isEmpty()){
			logger.error("Can't select id, tempPass, userId and hash");
			return null;
		}
		ForgotPass ResetDetails = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(CommonDBConstants.FORGOT_PASS.get("ID"));query.append(" , ");
			query.append(CommonDBConstants.FORGOT_PASS.get("USER_ID"));query.append(" , ");
			query.append(CommonDBConstants.FORGOT_PASS.get("HASH_VALUE"));query.append(" , ");
			query.append(CommonDBConstants.FORGOT_PASS.get("TEMP_PASSWORD"));
			query.append(" FROM ");query.append(CommonDBConstants.TBL_FORGOT_PASS);
			query.append(" WHERE ");query.append(CommonDBConstants.FORGOT_PASS.get("HASH_VALUE"));
			query.append(" = ?");query.append(" AND ");query.append(" now() <= ");
			query.append(CommonDBConstants.FORGOT_PASS.get("EXPIRE_TIME"));
			query.append(" AND ");query.append(CommonDBConstants.FORGOT_PASS.get("ACTIVE")); query.append(" = 1 ");

			ResetDetails = (ForgotPass)getJdbcTemplate().queryForObject(query.toString(), new Object[] { hashValue }, new BeanPropertyRowMapper(ForgotPass.class));

		}catch( Exception e ){
			logger.error("Error when retrieve tempPass.  " + e.getMessage());
		}
		logger.info(" ==> get id, tempPass, userId and hash END <== ");
		return ResetDetails;
	}

	/**
	 * This method updates the new password for the corresponding user
	 * @param commonError
	 * @param newPassword
	 * @param userId
	 */
	public void updateUserPassword(CommonError commonError, String newPassword, String userId ) {
		logger.info(" ==> Get update User Password START <== ");
		if(CommonUtils.getValidString(newPassword).isEmpty() || CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't update user new password");
			return;
		}
		try{
			List<Object> args = new ArrayList<>();
			args.add(newPassword);
			args.add(userId);

			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_USER);qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("PASSWORD")); qry.append(" = ? , ");
			qry.append(CommonDBConstants.USER.get("LAST_PWD_CHANGE"));qry.append(" = now() ");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when update user new password field.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get update User Password ENDS <== ");
	}

	/**
	 * This method insert previous password to history table
	 * @param commonError
	 * @param userId
	 */
	public void insertForgotPassHistory(CommonError commonError,String userId){
		logger.info(" ==> Forgot Password Histroy Insert START <== ");
		if(CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't insert forgot password histroy");
			return;
		}
		try{
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(CommonDBConstants.TBL_FORGOT_PASS_HISTORY);queryInsert.append("( ");
			queryInsert.append(CommonDBConstants.FORGOT_PASS_HISTORY.get("USER_ID"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.FORGOT_PASS_HISTORY.get("PASSWORD"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.FORGOT_PASS_HISTORY.get("MODIFIED_DATE"));
			queryInsert.append(") (");queryInsert.append("SELECT ");
			queryInsert.append(CommonDBConstants.USER.get("USER_ID"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER.get("PASSWORD"));queryInsert.append(" , now()");
			queryInsert.append(" FROM ");queryInsert.append(CommonDBConstants.TBL_USER);
			queryInsert.append(" WHERE ");queryInsert.append(CommonDBConstants.USER.get("USER_ID"));queryInsert.append(" = ?)");
			getJdbcTemplate().update(queryInsert.toString(), new Object[] { userId });
		}catch( Exception e ){
			logger.error("Error when Insert forgot password histroy.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Forgot Password Histroy Insert END <== ");
	}

	/**
	 * This method fetch the previous passwords
	 * @param commonError
	 * @param userId
	 * @param oldPassCount 
	 * @return
	 */
	public List<User> fetchPreviousPasswords(CommonError commonError, String userId, int oldPassCount) {
		logger.info(" ==> Get previous three passwords START <== ");
		if( CommonUtils.getValidString(userId).isEmpty() || 0 == oldPassCount){
			logger.error("Can't select previous passwords");
			return null;
		}
		List<User> passwords = null;
		try{
			StringBuffer query = new StringBuffer("( SELECT ");
			query.append(CommonDBConstants.FORGOT_PASS_HISTORY.get("PASSWORD"));
			query.append(" FROM ");query.append(CommonDBConstants.TBL_FORGOT_PASS_HISTORY);
			query.append(" WHERE ");query.append(CommonDBConstants.FORGOT_PASS.get("USER_ID"));query.append(" = ?");
			query.append(" ORDER BY ");query.append(CommonDBConstants.FORGOT_PASS_HISTORY.get("MODIFIED_DATE"));query.append(" DESC LIMIT ? )");
			query.append(" UNION ALL ");query.append("( SELECT ");
			query.append(CommonDBConstants.USER.get("PASSWORD"));
			query.append(" FROM ");query.append(CommonDBConstants.TBL_USER);
			query.append(" WHERE ");query.append(CommonDBConstants.USER.get("USER_ID"));query.append(" = ?) ");
			passwords = (List<User>)getJdbcTemplate().query(query.toString(), new Object[] { userId, oldPassCount, userId }, new BeanPropertyRowMapper<User>(User.class));
		}catch( Exception e ){
			logger.error("Error when retrieve previous passwords.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == passwords){
			logger.error("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get previous three passwords END <== ");
		return passwords;
	}

	/**
	 * This method deactivate the forgot password link
	 * @param commonError
	 * @param hashValue
	 */
	public void deactivateLink(CommonError commonError, String hashValue) {
		logger.info(" ==> Deactive forgotPassword link START <== ");
		if( CommonUtils.getValidString(hashValue).isEmpty()){
			logger.error("Can't update user password");
			return;
		}
		try{
			List<Object> args = new ArrayList<Object>();
			args.add(hashValue);
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_FORGOT_PASS);qry.append(" SET ");
			qry.append(CommonDBConstants.FORGOT_PASS.get("ACTIVE")); qry.append(" = 0 ");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.FORGOT_PASS.get("HASH_VALUE"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when deactive forgotpassword.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Deactive forgotPassword link END <== ");

	}
	
	/**
	 * This method updates the reset count
	 * @param userId
	 */
	public void resetFailureCount(String userId){
		logger.info(" ==> Reset Failure Count START <== ");
		if(CommonUtils.getValidString(userId).isEmpty()){
			logger.info("Can't update login failure count as 0. ");
			return;
		}
		try{
			List<Object> args = new ArrayList<>();
			args.add(userId);
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_USER);qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("LOGIN_FAILURE_COUNT")); qry.append(" = 0");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("login failure count as 0.  " + e.getMessage());
		}
		logger.info(" ==>  Reset Failure Count ENDS <== ");
	}
	
	/**
	 * This method insert failure count
	 * @param userId
	 */
	public void increaseFailureCount(String userId){
		logger.info(" ==> Increase Failure Count START <== ");
		if(CommonUtils.getValidString(userId).isEmpty()){
			logger.error("Can't update login failure count by 1. ");
			return;
		}
		try{
			List<Object> args = new ArrayList<>();
			args.add(userId);
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE "); qry.append(CommonDBConstants.TBL_USER);qry.append(" SET ");
			qry.append(CommonDBConstants.USER.get("LOGIN_FAILURE_COUNT")); qry.append(" = ");
			qry.append(CommonDBConstants.USER.get("LOGIN_FAILURE_COUNT")); qry.append(" + 1 ");
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));
			qry.append(" = ?");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("login failure count by 1.  " + e.getMessage());
		}
		logger.info(" ==> Increase Failure Count ENDS <== ");
	}
	

	/**
	 * This method insert & update the mTan
	 * @param commonError
	 * @param user
	 * @param mTan
	 * @return
	 */
	public Boolean insertUpdateSecondLevelAuthDetails(CommonError commonError, User user, String mTan) {
		logger.info(" ==>Second Level Auth Insert & Update START <== ");
		if( null == user ){
			logger.info("Can't insert or update Second level auth values");
			return false;
		}       
			try{
				List<Object> args = new ArrayList<>();
				args.add(user.getUserId());
				args.add(mTan);
				args.add(mTan);
				StringBuffer queryInsert = new StringBuffer();
				queryInsert.append("INSERT INTO ");queryInsert.append(CommonDBConstants.TBL_SECOND_LEVEL_AUTH);
				queryInsert.append("( ");
				queryInsert.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("USER_ID"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("MTAN"));queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("EXPIRE_TIME"));
				queryInsert.append(") VALUES(?,?,DATE_ADD(NOW(), INTERVAL 2 MINUTE)) ");
				queryInsert.append("ON DUPLICATE KEY UPDATE ");
				queryInsert.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("MTAN"));queryInsert.append(" = ? ");
				queryInsert.append(" , ");
				queryInsert.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("EXPIRE_TIME"));
				queryInsert.append(" = DATE_ADD(NOW(), INTERVAL 2 MINUTE)");
				getJdbcTemplate().update(queryInsert.toString(), args.toArray());
			}
			catch( Exception e ){
				logger.info("Error when Insert Second Level Auth values.  " + e.getMessage());
				if(commonError != null)
					commonError.addError(CommonConstants.DB_ERROR);
				return false;
			}
			logger.info(" ==> Second Level Auth Insert & Update END <== ");
			
		return true;
	}
	
	/**
	 * This method used to Retrieve User Details
	 * @param commonError
	 * @param userId
	 * @param mTan
	 * @return
	 */
	public boolean getSecondLevelAuthSubmit(CommonError commonError, String userId, String mTan){
		logger.info(" ==>mTan verification START <== ");
		if( null == mTan ){
			logger.info("Can't verify the mTan value");
			return false;
		}   
		try{		
			User user = null;
			StringBuffer qry = new StringBuffer();
			qry.append("SELECT ");
			qry.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("USER_ID"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_SECOND_LEVEL_AUTH);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("USER_ID"));qry.append(" = ? ");qry.append(" AND ");
			qry.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("MTAN"));qry.append("= ? ");qry.append(" AND ");
			qry.append(CommonDBConstants.SECOND_LEVEL_AUTH.get("EXPIRE_TIME"));qry.append(" >= now()");
			user = (User)getJdbcTemplate().queryForObject(qry.toString(), new Object[] { userId, mTan }, new BeanPropertyRowMapper<User>(User.class));
		}catch( Exception e ){
				logger.info("Error when retrieve User Details.  " + e.getMessage());
				if(commonError != null)
					commonError.addError(CommonConstants.DB_ERROR);
				return false;
		}
			logger.info(" ==> mTan verification END <== ");
			
		return true;
	}
	
	/**
	 * This method used to Retrieve User Details
	 * @param commonError
	 * @param loginUser
	 * @return
	 */
	public User getSecondLevelType(CommonError commonError, User loginUser){
		logger.info(" ==> getSecondLevelAuthType START <== ");
		User authenticate = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" , ");
			qry.append(CommonDBConstants.USER.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_USER);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER.get("USER_ID"));qry.append(" = ? ");
				
			authenticate = (User)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{loginUser.getUserId()} , new BeanPropertyRowMapper<User>(User.class));
				
		}catch( Exception e ){
			logger.error("Error when retrieve Second Level Auth Type.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getSecondLevelAuthType END <== ");
		return authenticate;
	}
	
	/**
	 * This method insert failure count
	 * @param commonError
	 * @param notifiType
	 * @param user
	 */
	public void insertUserNotification(CommonError commonError, String notifiType, User user){
		logger.info(" ==> Insert User notification START <== ");
		try{
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			List<Object> args = new ArrayList<>();
			args.add(user.getUserId());
			args.add(notifiType);
			args.add(date);
			StringBuffer qry = new StringBuffer();
			qry.append("INSERT INTO "); qry.append(CommonDBConstants.TBL_USER_NOTIFICATION);qry.append("( ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("USER_ID"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFICATION"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFIED_DATE"));
			qry.append(") VALUES(?,?,?) ");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when insert user notification.  " + e.getMessage());
		}
		logger.info(" ==> Insert User notification END <== ");
	}
	
	/**
	 * This method inserts user USB Token Details
	 * @param commonError
	 * @param usbTokenDetails
	 */
	public void addUpdateUserTokenAuthentication( CommonError commonError, UserUSBTokenAuthentication usbTokenDetails ){
		logger.info(" ==> Insert User USB TOKEN START <== ");
		try{
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			List<Object> args = new ArrayList<>();
			args.add(usbTokenDetails.getUserId());
			args.add(usbTokenDetails.getKeyHandle());
			args.add(usbTokenDetails.getPublicKey());
			args.add(usbTokenDetails.getCounter());
			args.add(usbTokenDetails.getCompromised());
			args.add(date);
			StringBuffer qry = new StringBuffer();
			qry.append("INSERT INTO "); qry.append(CommonDBConstants.TBL_USER_USB_TOKEN);qry.append("( ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("USER_ID"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("KEY_HANDLE"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("PUBLIC_KEY"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COUNTER"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COMPROMISED"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("REGISTERED_TIME"));
			qry.append(") VALUES(?,?,?,?,?,?) ");
			qry.append("ON DUPLICATE KEY UPDATE ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("KEY_HANDLE"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("PUBLIC_KEY"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COUNTER"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COMPROMISED"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("REGISTERED_TIME"));qry.append(" = ? ");
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when insert user usb token details. " + e.getMessage());
		}
		logger.info(" ==> Insert User USB TOKEN ENDS <== ");
	}
	
	/**
	 * This method inserts user USB Token Details
	 * @param commonError
	 * @param usbTokenDetails
	 */
	public void addGoogleAuthSecrets(CommonError commonError, User user, String secretKey) {
		logger.info(" ==> Insert User Google Authentication START <== ");
		try{
			java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
			List<Object> args = new ArrayList<>();
			args.add(user.getUserId());
			args.add(secretKey);
			args.add(user.getRegistrationPin());
			args.add(user.getUserId());
			args.add(date);
			args.add(user.getUserId());
			args.add(secretKey);
			args.add(user.getRegistrationPin());
			args.add(user.getUserId());
			args.add(date);
			StringBuffer qry = new StringBuffer();
			qry.append("INSERT INTO "); qry.append(CommonDBConstants.TBL_GOOGLE_AUTH_SECRETS);qry.append("( ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("USER_ID"));qry.append(" , ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("SECRET"));qry.append(" , ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("REGISTRATION_PIN"));qry.append(" , ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("CREATE_BY"));qry.append(" , ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("CREATE_TIME"));
			qry.append(") VALUES(?,?,?,?,?) ");
			qry.append("ON DUPLICATE KEY UPDATE ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("USER_ID"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("SECRET"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("REGISTRATION_PIN"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("CREATE_BY"));qry.append(" = ?, ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("CREATE_TIME"));qry.append(" = ? ");
			
			getJdbcTemplate().update(qry.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when insert Google Authentication details. " + e.getMessage());
		}
		logger.info(" ==> Insert Google Authentication ENDS <== ");
	}
	
	/**
	 * Select user notifications from DB
	 * @param loginUser
	 * @param notifiType
	 * @return
	 */
	public String getGoogleAuthSecrets( User loginUser){
		logger.info(" ==> Get Google Authentication Secretes START <== ");
		String secretCode = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("SECRET"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_GOOGLE_AUTH_SECRETS);
			qry.append(" WHERE ");qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("USER_ID"));qry.append(" = ? ");
			secretCode = (String)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{loginUser.getUserId()} , String.class);
		}catch( Exception e ){
			logger.error("Error when retrieve Google Authentication Secretes.  " + e.getMessage());
		}
		logger.info(" ==> Get Google Authentication Secretes END <== ");
		return secretCode;
	}
	
	/**
	 * Select USB user token details for user
	 * @param commonError
	 * @param userName
	 * @return
	 */
	public UserUSBTokenAuthentication getUserUSBTokenDetails( CommonError commonError, String userName ){
		logger.info(" ==> getUserUSBTokenDetails START <== ");
		UserUSBTokenAuthentication userUSBToken = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("USER_ID"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("KEY_HANDLE"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("PUBLIC_KEY"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COUNTER"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("COMPROMISED"));qry.append(" , ");
			qry.append(CommonDBConstants.USER_USB_TOKEN.get("REGISTERED_TIME"));
			qry.append(" WHERE ");qry.append(CommonDBConstants.USER_USB_TOKEN.get("USER_ID"));	
			userUSBToken = (UserUSBTokenAuthentication)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{userName} , new BeanPropertyRowMapper<UserUSBTokenAuthentication>(UserUSBTokenAuthentication.class));
		}catch( Exception e ){
			logger.error("Error when retrieve user USBToken Details.  " + e.getMessage());
		}
		logger.info(" ==> getUserUSBTokenDetails END <== ");
		return userUSBToken;
	}
	
	/**
	 * Select user notifications from DB
	 * @param loginUser
	 * @param notifiType
	 * @return
	 */
	public String checkUserNotification( User loginUser, String notifiType ){
		logger.info(" ==> Check User Notification START <== ");
		String nofificationType = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFICATION"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_USER_NOTIFICATION);
			qry.append(" WHERE ");qry.append(CommonDBConstants.USER_NOTIFICATION.get("USER_ID"));qry.append(" = ? AND ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFICATION"));qry.append(" = ? ");
				
			nofificationType = (String)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{loginUser.getUserId(), notifiType} , String.class);
		
		}catch( Exception e ){
			logger.error("Error when retrieve user notification.  " + e.getMessage());
		}
		logger.info(" ==> Check User Notification END <== ");
		return nofificationType;
	}
	
	/**
	 * Select user notifications from DB
	 * @param notifiType
	 * @return
	 */
	public String checkUserNotifiedDate( String notifiType ){
		logger.info(" ==> Check User Notification Date START <== ");
		String nofificationDate = null;
		
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFIED_DATE"));
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_USER_NOTIFICATION);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.USER_NOTIFICATION.get("NOTIFICATION"));qry.append(" = ? ");
				
			nofificationDate = (String)getJdbcTemplate().queryForObject(qry.toString(), new Object[]{notifiType} , String.class);
			
		}catch( Exception e ){
			logger.error("Error when retrieving user notification Date.  " + e.getMessage());
		}
		logger.info(" ==> Check User Notification Date END <== ");
		return nofificationDate;
	}
	
	/**
	 * Delete user from application
	 * @param commonError
	 * @param ids
	 * @param loginUser
	 */
	public void deleteUser(CommonError commonError, List<Integer> ids,User loginUser) {
		logger.info(" ==> delete User START <== ");
		if( ids == null){
			logger.error("Can't delete user");
			return;
		}
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertUserHistory( commonError, loginUser,ids, CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", ids);
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(CommonDBConstants.TBL_USER);
			qry.append(" WHERE ");qry.append(CommonDBConstants.USER.get("ID"));qry.append(" IN ");
			qry.append(" (:ids) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){

			logger.info("Error when Delete User Roles.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}

		logger.info(" ==> delete User END <== ");
	}
	
	
	/**
	 * This method will activate or deactivate the user
	 * @param commonError
	 * @param loginUser
	 * @param ids
	 * @param status
	 */
	public void updateActiveStatus( CommonError commonError, User loginUser,  List<Integer> ids, String status ){
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("User Id ==> " + ids );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			if(status.equals(CommonConstants.Y))
				insertUserHistory( commonError, loginUser,ids, CommonConstants.UNLOCKED);
			else
				insertUserHistory( commonError, loginUser,ids, CommonConstants.LOCKED);

			Map<String, Object> params = new HashMap<String, Object>();
	        params.put("ids", ids);
 			StringBuffer qry = new StringBuffer();
 			qry.append(" UPDATE ");qry.append( CommonDBConstants.TBL_USER );qry.append(" SET ");
 			qry.append( CommonDBConstants.USER.get("ACTIVE") );qry.append(" = ");
 			qry.append("'"+status+"'");
			qry.append(" WHERE ");
 			qry.append( CommonDBConstants.USER.get("ID") );qry.append(" IN ");
 			qry.append(" (:ids) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a User.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");

	}
	

	/**

	 * This method inserts updated user into user history table
	 * @param commonError
	 * @param loginUser
	 * @param ids
	 * @param action
	 */
	public void insertUserHistory(CommonError commonError, User loginUser,List<Integer> ids, String action) throws Exception {
		for(Integer id:ids){
		logger.info(" ==> Insert User History START <== ");
		logger.info("User Id ==> " + ids );
		String userId=String.valueOf(id);
		StringBuffer queryInsert = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : CommonDBConstants.USER.keySet() ){
			if(!column.equals("CORE_ORB_USERS")){
			newQryHist.append( CommonDBConstants.USER.get(column) );newQryHist.append( "," );
			}
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(CommonDBConstants.TBL_USER);newQryHist.append(" WHERE ");newQryHist.append( CommonDBConstants.USER.get("ID") );newQryHist.append( "=?" );
		User user = (User)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { userId }, new BeanPropertyRowMapper(User.class) );
		
		try{

			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			queryInsert.append("INSERT INTO ");
			queryInsert.append(CommonDBConstants.TBL_USER_HISTORY);queryInsert.append("( ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("USER_ID"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("PASSWORD"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("FIRST_NAME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("LAST_NAME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("EMAIL"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("INITIALS"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("PHONE_NUMBER"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("USER_IMAGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("USER_EXPIRY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("START_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("DISPLAY_ROWS_SIZE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("LANGUAGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("DISPLAY_DATE_FORMAT"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("TIME_ZONE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("AUTHENTICATE_USER"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("SECOND_LEVEL_AUTH_USER_PRIVILEGE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("CREATED_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("LAST_MODIFIED_DATE"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("CREATE_BY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("CREATE_TIME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("UPDATE_BY"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("UPDATE_TIME"));queryInsert.append(" , ");
			queryInsert.append(CommonDBConstants.USER_HISTORY.get("ACTION"));
			queryInsert.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<>();
			args.add(user.getUserId());
			args.add(user.getPassword());
			args.add(user.getFirstName());
			args.add(user.getLastName());
			args.add(user.getEmail());
			args.add(user.getInitials());
			args.add(user.getPhoneNumber());
			args.add(user.getUserImage());
			args.add(user.getUserExpiry());
			args.add(user.getStartDate());
			args.add(user.getDisplayRowsSize());
			args.add(user.getLanguage());
			args.add(user.getDisplayDateFormat());
			args.add(user.getTimeZone());
			args.add(user.getAuthenticateUser());
			args.add(user.getSecondLevelAuthUserPrivilege());
			args.add(date);args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(action);
			getJdbcTemplate().update( queryInsert.toString(),args.toArray() );

		}catch( Exception e ){
				logger.info("Error when Insert UserHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		}

		logger.info(" ==> Insert User History END <== ");
	}

	/**
	 * This method helps to re-register google authentication
     * @param commonError 
     * @param userId 
	 */
	public void deRegisterGoogleAuth(CommonError commonError, String userId) {
	  logger.info(" ==> De-Register GoogleAuth START <== ");
	  try{
			StringBuffer qry = new StringBuffer("DELETE ");
			qry.append(" FROM ");qry.append(CommonDBConstants.TBL_GOOGLE_AUTH_SECRETS);
			qry.append(" WHERE ");
			qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("USER_ID"));qry.append(" = ? ");
				
			getJdbcTemplate().update( qry.toString(),new Object[]{userId} );
		}catch( Exception e ){
			logger.error("Error when de Register Google Authentication.  " + e.getMessage());
		}
	  logger.info(" ==> De-Register GoogleAuth END <== ");
	}

	/**
	 * This method helps to disable Google Authentication
     * @param commonError 
     * @param userId 
	 */
	public void updateGoogleAuthUser(CommonError commonError, String userId,String authType) {
		logger.info(" ==> Update GoogleAuth User START <== ");
		  try{
				StringBuffer qry = new StringBuffer("UPDATE ");
				qry.append(CommonDBConstants.TBL_USER);qry.append(" SET ");
				qry.append(CommonDBConstants.USER.get("AUTHENTICATE_USER"));qry.append(" = ?");
				qry.append(" WHERE ");
				qry.append(CommonDBConstants.USER.get("USER_ID"));qry.append(" = ? ");
					
				getJdbcTemplate().update( qry.toString(),new String[]{authType ,userId} );
			}catch( Exception e ){
				logger.error("Error when update Google Authentication for particular user.  " + e.getMessage());
			}
		  logger.info(" ==> Update GoogleAuth User END <== ");
		
	}
	
	/**
	 * This method will return list of google authenticator user from DB
	 * @param commonError
	 * @return
	 */
	public List<User> getGoogleAuthUserList(CommonError commonError) {
		logger.info(" ==> Get GoogleAuthUserList START <== ");
		List<User> googleAuthUserList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : CommonDBConstants.GOOGLE_AUTH_SECRETS.keySet() ){
				if(!column.equals("CORE_ORB_GOOGLE_AUTH_SECRETS")){
					newQry.append( CommonDBConstants.GOOGLE_AUTH_SECRETS.get(column) );newQry.append( "," );
				}
				
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(CommonDBConstants.TBL_GOOGLE_AUTH_SECRETS);
			googleAuthUserList = getJdbcTemplate().query( newQry.toString(),new String[]{}, new BeanPropertyRowMapper(User.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch GoogleAuthUser list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> Get GoogleAuthUserList END <== ");
		return googleAuthUserList;
	}

	public void UpdateGoogleAuthPin(CommonError commonError, User user, String googleAuthPin) {
		logger.info(" ==> Update GoogleAuth Pin START <== ");
		  try{
				StringBuffer qry = new StringBuffer("UPDATE ");
				qry.append(CommonDBConstants.TBL_GOOGLE_AUTH_SECRETS);qry.append(" SET ");
				qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("REGISTRATION_PIN"));qry.append(" = ?");
				qry.append(" WHERE ");
				qry.append(CommonDBConstants.GOOGLE_AUTH_SECRETS.get("USER_ID"));qry.append(" = ? ");
					
				getJdbcTemplate().update( qry.toString(),new String[]{googleAuthPin ,user.getUserId()} );
			}catch( Exception e ){
				logger.error("Error when update Google Authentication Pin for particular user.  " + e.getMessage());
			}
		  logger.info(" ==> Update GoogleAuth Pin END <== ");
		
	}
	
   	/**
	 * Method for get user list
	 * @param commonError
	 * @param activeFlag
	 * @return
	 */
	public List<User> getUserRoleList(CommonError commonError) {
		logger.info(" ==> getUserRoleList START <== ");
		List<User> userRoleList = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");query.append("DISTINCT ");
			query.append(AdminDBConstants.USER_ROLE.get("USER_ID"));
			query.append(" FROM ");query.append(AdminDBConstants.TBL_USER_ROLE);query.append(" WHERE ");
			query.append(AdminDBConstants.USER_ROLE.get("ROLE_ID"));query.append(" != ");
			query.append("1");
			userRoleList = getJdbcTemplate().query( query.toString(),new String[]{}, new BeanPropertyRowMapper(User.class) );
            logger.info(userRoleList);
		}catch( Exception e ){
			logger.info("Error when retrieve getUserRoleList.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getUserRoleList END <== ");
		return userRoleList;
	}

}
