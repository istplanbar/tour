package com.orb.admin.useradministration.constants;

import com.orb.common.client.utils.CommonUtils;

public class UserManagementConstants {
	
	public final static String PASSWORD;
	public final static String ADD_UPDATE_PAGE_REDIR = "/common/process/admin/loadUserManagementAddUpdate.xhtml";
	public final static String LIST_PAGE = "admin/user/userManagementList.xhtml";
	public final static String VIEW_PAGE = "admin/user/userManagementView.xhtml";
	public final static String ADD_UPDATE_PAGE = "admin/user/userManagementAddUpdate.xhtml";
	public final static String LIST_PAGE_REDIR = "/common/process/admin/loadUserManagementList.html";
	public final static String VIEW_PAGE_REDIR = "/common/process/admin/loadUserManagementView.html";
	public final static String USER_ADDED_SUCCESS="user_added_success";
	public final static String USER_UPDATED_SUCCESS="user_update_success";
	public final static String USER_ACCOUNT_CREATED="user_account_created";
	public final static String USERNAME="lbl_userName";
	public final static String USER_PASSWORD="lbl_password";
	public final static String USER_START_DATE="lbl_userStartDate";
	public final static String USER_EXPIRED_DATE="lbl_Expiry_Date";
	public final static String USER_EMAIL="lbl_uEmail";
	public final static String MAIL_GREETINGS_USER="mail_greetings_user";
	public final static String USER_CREATION="user_creation";
	public final static String MAIL_THANKS_USER="user_creation_thanks";
	public final static String SHOW_ACTIVE_USER="SHOW_ACTIVE_USER";
	public final static String USER_TOGGLER_LIST = "USER_TOGGLER_LIST" ;
	public final static String USER_DELETE_SUCCESS="user_delete_success";
	public final static String USER_LOCKED_SUCCESS="user_locked_success";
	public final static String USER_EMAIL_UNIQUE_ERROR = "user_email_unique_error";
	public final static String USER_ID_UNIQUE_ERROR = "user_id_unique_error";
	public final static String USER_UNLOCKED_SUCCESS="user_unlocked_success";
	public final static String SELECTED_USER_ID="SELECTED_USER_ID";
	public final static String DEFAULT_USER_LIST_SIZE="100";
	public final static String DEFAULT_TIME_ZONE="Europe/Berlin (GMT+1:00)";
	public final static String TEMPORARY_PASSWORD_SENT_SUCCESSFULLY="temporary_password_sent_successfully";
	public final static String USER_CHANGE = "userChange";
	
	static{
		PASSWORD = CommonUtils.getPropertyValue("TEMP_PASSOWRD", "PASSWORD");
	}

}
