package com.orb.admin.useradministration.service;

import java.util.List;

import com.orb.admin.roleadministration.model.Role;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface UserManagementService {

	public List<Role> fetchRole(CommonError userError);
	public void createUser(CommonError userError, User user, List<Role> roleList, String linkForEmail, User loginUser);
	public List<User> getActiveUsersList(CommonError commonError);
	public List<User> getInactiveUsersList(CommonError commonError);
	public User getUser(CommonError userError, String selectedUserId);
	public List<Role> getUserRoles(CommonError userError, String userId);
	public List<Role> getRole(CommonError userError, List<Role> targetRolesList);
	public void updateUser(CommonError userError, User user, boolean resetPass, List<Role> roleTargetLists,User loginUser);
	public void deleteUser(CommonError commonError, List<Integer> id,User loginUser);
	public void lockUser(CommonError commonError, User loginUser, List<User> userList);
	public void unlockUser(CommonError commonError, User loginUser, List<User> userList);
	public void delete(CommonError commonError, User loginUser, List<User> userList);
	public void lockViewedUser(CommonError commonError, User loginUser, User user);
	public void unlockViewedUser(CommonError commonError, User loginUser, User user);
	public void deleteViewedUser(CommonError commonError, User loginUser, User user);
    public List<User> getUserRoleList(CommonError commonError);
}
