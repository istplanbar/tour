package com.orb.admin.useradministration.service.impl;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.orb.admin.client.dao.AdminDAO;
import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.model.ForgotPass;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.constants.UserManagementConstants;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.admin.useradministration.service.UserManagementService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.EncryptionUtil;
import com.orb.common.factory.AbstractCommonFactory;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class UserManagementServiceImpl extends AbstractCommonFactory implements UserManagementService{
	
	private static Log logger = LogFactory.getLog(UserManagementServiceImpl.class);
	
	@Autowired 
	ReloadableResourceBundleMessageSource bundle;
			
	@Autowired
	UserDAO userDAO;
	@Autowired
	AdminDAO adminDAO;
	
	
	

	/**
	 * This method call DAO and fetch the list of roles
	 * @param commonError
	 * @return
	 */
	public List<Role> fetchRole(CommonError commonError) {
		return userDAO.fetchRoles(commonError, null,CommonConstants.Y);
	}

	/**
	 * This method call DAO to insert new user details
	 * @param commonError
	 * @param user
	 * @param roleList
	 * @param linkForEmail
	 * @param loginUser
	 */
	@CacheEvict(value="allUsersList", allEntries=true)
	public void createUser(CommonError commonError, User user, List<Role> roleList, String linkForEmail, User loginUser) {
		String pw = "";
		final Random RANDOM = new SecureRandom();
		ForgotPass forgotPass = new ForgotPass();
		if (null != user){
			
			forgotPass.setUserId(user.getUserId());
			forgotPass.setHashValue(UUID.randomUUID().toString()+UUID.randomUUID().toString()+UUID.randomUUID().toString());
			pw = new BigInteger(32, RANDOM).toString(32);
			forgotPass.setTempPassword(pw);
			if(!userDAO.insertForgotPassDetails(commonError, forgotPass )){
				logger.info("Db insertion issue.");
			}
		}
	  	  
	    String password = EncryptionUtil.encryptBCrypt(UserManagementConstants.PASSWORD+pw);
	    user.setPassword(password);
	    if(true ==user.getIsPasswordChecked().booleanValue()){
		   String newPassword = EncryptionUtil.encryptBCrypt(user.getNewPassword());
		   user.setPassword(newPassword);
		}
		Integer userRowId = userDAO.createUser(commonError, user, loginUser);
		if(userRowId != 0){
			if(true !=user.getIsPasswordChecked().booleanValue()){
				notifyUser(user, forgotPass, linkForEmail);
			}
			userDAO.addRolesToUser(commonError, roleList, userRowId);
		}else{
			return;
		}
	}

	/**
	 * This method for update User
	 * @param commonError
	 * @param user
	 * @param resetPass
	 * @param roleTargetLists
	 * @param loginUser
	 */
	@Override
	@CacheEvict(value="allUsersList", allEntries=true)
	public void updateUser(CommonError commonError, User user, boolean resetPass, List<Role> roleTargetLists,User loginUser ) {
		if(resetPass){
			String password = EncryptionUtil.encryptBCrypt(UserManagementConstants.PASSWORD+user.getLastName());
			user.setPassword(password);
		}
		if(true ==user.getIsPasswordChecked().booleanValue()){
		       String newPassword = EncryptionUtil.encryptBCrypt(user.getNewPassword());
			   user.setPassword(newPassword);
		}
		List<Integer> ids = new ArrayList<>();
		ids.add(user.getId());
		userDAO.updateUser(commonError, user, resetPass, loginUser );
		userDAO.deleteUserRole(commonError, ids);
		userDAO.addRolesToUser(commonError, roleTargetLists, user.getId());
	}

	/**
	 * Method for get user details
	 * @param commonError
	 * @param id
	 * @return
	 */
	public User getUser(CommonError comminError, String id) {
		return userDAO.getUser(id);
	}

	/**
	 * Method for get user roles
	 * @param commonError
	 * @param id
	 * @return
	 */
	public List<Role> getUserRoles(CommonError commonError, String id) {
		//User user = userDAO.getUser(id);
		//return userDAO.getUserRoles(commonError, user.getId());
        return userDAO.getUserRoles(commonError, Integer.parseInt(id));
	}

	/**
	 * Method for get roles
	 * @param commonError
	 * @param targetRolesList
	 * @return
	 */
	public List<Role> getRole(CommonError commonError, List<Role> targetRolesList) {
		List<Integer> roleIdList = new ArrayList<>();
		if (targetRolesList.isEmpty()){
			roleIdList = null;
		}else{
			for (Role role : targetRolesList) {
				roleIdList.add(role.getRoleId());
			}
		}
		return userDAO.fetchRoles(commonError, roleIdList,CommonConstants.Y);
	}

	/**
	 * Deletes User from db
	 * @param commonError
	 * @param id
	 * @param loginUser
	 */
	public void deleteUser(CommonError commonError, List<Integer> id,User loginUser) {
		userDAO.deleteUser(commonError, id,loginUser);
		userDAO.deleteUserRole(commonError, id);
	}
				
	/**
	 * Notify the user when created
	 * @param user
	 * @param forgotPass
	 * @param currentUrl
	 */
	 public void notifyUser(User user, ForgotPass forgotPass, String currentUrl) {
		       	        			        
		        boolean sessionDebug = false;
		        Properties props = System.getProperties();
		        Map<String, String> adminConfMap = adminDAO.getAdminConfMap(null);
		        String nameOfDeployment =  adminConfMap.get("nameOfDeployment");
		       
		        props.put("mail.smtp.host", CommonConstants.SEND_MAIL_HOST);
		        props.put("mail.transport.protocol", CommonConstants.MAIL_PROTOCOL);
		      	       
		        Session session = Session.getDefaultInstance(props, null);
		       	session.setDebug(sessionDebug);
		        
		       try {
		    	   	Message message = new MimeMessage(session);
	    			message.setFrom(new InternetAddress(CommonConstants.FROM_MAIL_ID));
	 		        InternetAddress[] address = { new InternetAddress(user.getEmail()) };
	 		        message.setRecipients(Message.RecipientType.TO, address);
	 		        message.setSentDate(new Date());
		       		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy"); 
	    			StringBuffer str = new StringBuffer();
					str.append(getJSFMessage(bundle,UserManagementConstants.MAIL_GREETINGS_USER)+" "+user.getFirstName()+" "+user.getLastName()+", \n\n");
		    		str.append(getJSFMessage(bundle,UserManagementConstants.USER_ACCOUNT_CREATED)+". \n");
		    		str.append(getJSFMessage(bundle,UserManagementConstants.USERNAME)+": ");
		    		str.append(user.getUserId()+" \n");
		    		str.append(getJSFMessage(bundle,UserManagementConstants.USER_PASSWORD)+": ");
		    		str.append(forgotPass.getTempPassword()+" \n");
		    		if(user.getStartDate()!=null){
		    		  str.append(getJSFMessage(bundle,UserManagementConstants.USER_START_DATE)+": ");
		    		  String startDate = formatter.format(user.getStartDate()); 
		    		  str.append(startDate+" \n\n");
		    		}
		    		str.append(getJSFMessage(bundle,UserManagementConstants.MAIL_THANKS_USER));
	    			
		    		message.setSubject(getJSFMessage(bundle, UserManagementConstants.USER_CREATION).trim()+" - "+"("+nameOfDeployment+")");
					message.setText(str.toString());

	    			Transport.send(message);

	    			logger.info("User Created");
	    			
	    			//Password change mail notification
	    			
	    			StringBuilder mailBodyLink = new StringBuilder();
	    			String forgotPasswordLink = (String) getSessionObj(CommonConstants.FORGOT_PASS_LINK);
					mailBodyLink.append(getJSFMessage(bundle,ForgotPasswordConstant.WELCOME_GREET));mailBodyLink.append("");
					mailBodyLink.append(user.getFirstName());mailBodyLink.append(" ");
					mailBodyLink.append(user.getLastName());mailBodyLink.append(",");
					mailBodyLink.append(getJSFMessage(bundle,ForgotPasswordConstant.MAIL_BODY_FOR_LINK));
					mailBodyLink.append(currentUrl);mailBodyLink.append(forgotPasswordLink);
					mailBodyLink.append(forgotPass.getHashValue());
	    			
	    			message.setSubject(getJSFMessage(bundle, ForgotPasswordConstant.MAIL_SUBJECT).trim()+" - "+"("+nameOfDeployment+")");
	    			message.setText(mailBodyLink.toString());
            		Transport.send(message);
            		
            		logger.info("User Norified");

	    		} catch (MessagingException e) {
	    			logger.info("Mail Notification not sent"+e.getMessage());
	    		}
	        
	}
	
		
	/**
	 * This method calls DAO to get all active user from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<User> getActiveUsersList(CommonError commonError) {
		return userDAO.getUserList(commonError,CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive user level from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<User> getInactiveUsersList(CommonError commonError) {
		return userDAO.getUserList(commonError,CommonConstants.N);
	}
	
	/**
	 * This method calls DAO to activate the user
	 * @param commonError
	 * @param loginUser
	 * @param selectedUser
	 */
	public void unlockUser(CommonError commonError, User loginUser, List<User> selectedUser) {
		List<Integer> ids = new ArrayList<>();
		if( selectedUser == null || selectedUser.isEmpty() ){
			return;
		}
		for( User user : selectedUser ){
			ids.add(user.getId());
		}
		  userDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );
	}

	/**
	 * This method calls DAO to de-activate User
	 * @param commonError
	 * @param loginUser
	 * @param selectedUser
	 */
	@Override
	public void lockUser(CommonError commonError, User loginUser, List<User> selectedUser) {
		List<Integer> ids = new ArrayList<>();
		if( selectedUser == null || selectedUser.isEmpty() ){
			return;
		}
		for( User user : selectedUser ){
			ids.add(user.getId());
		}
		  userDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
	}

	/**
	 * This method will interact with DAO to delete a User in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedUser
	 */
	@Override
	public void delete(CommonError commonError, User loginUser, List<User>  selectedUser ) {
		List<Integer> ids = new ArrayList<>();
		if( selectedUser == null || selectedUser.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( User user : selectedUser ){
			ids.add(user.getId());
		}
		userDAO.deleteUser(commonError, ids,loginUser);
		userDAO.deleteUserRole(commonError, ids);
	}
	
	/**
	 * This method calls DAO to de-activate user
	 * @param commonError
	 * @param loginUser
	 * @param user
	 */
	@Override
	public void lockViewedUser(CommonError commonError, User loginUser, User user) {
		List<Integer> ids = new ArrayList<>();
		ids.add(user.getId());
		userDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );

	}
	/**
	 * This method calls DAO to activate user
	 * @param commonError
	 * @param loginUser
	 * @param user
	 */
	@Override
	public void unlockViewedUser(CommonError commonError, User loginUser,User user) {
		List<Integer> ids = new ArrayList<>();
		ids.add(user.getId());
		userDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );

	}
	
	/**
	 * This method calls DAO to deletes particular user
	 * @param commonError
	 * @param loginUser
	 * @param user
	 */
	@Override
	public void deleteViewedUser(CommonError commonError, User loginUser, User user) {
		List<Integer> ids = new ArrayList<>();
		ids.add(user.getId());
		userDAO.deleteUser(commonError, ids,loginUser);
		userDAO.deleteUserRole(commonError, ids);
	}
	
    /**
	 * Method for get user role List
	 * @param commonError
	 * @param id
	 * @return
	 */
	@Override
	public List<User> getUserRoleList(CommonError commonError) {
		return userDAO.getUserRoleList(commonError);
	}
}

