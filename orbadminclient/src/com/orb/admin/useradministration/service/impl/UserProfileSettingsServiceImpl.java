package com.orb.admin.useradministration.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.admin.useradministration.service.UserProfileSettingsService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class UserProfileSettingsServiceImpl implements UserProfileSettingsService{

	@Autowired
	UserDAO userDAO;
	
	/**
	 * Get all users list
	 */
	@Override
	@CacheEvict(value="allUsersList", allEntries=true)
	public void saveProfileSettings( CommonError commonError, String userId, User loginUser ) {
		userDAO.updateProfileSettings( commonError, userId, loginUser);
		
	}

	/**
	 * Get second level user list
	 */
	@Override
	public User getSecondLevelType(CommonError commonError , User loginUser ) {
		return userDAO.getSecondLevelType(commonError, loginUser );
	}
	
	
	/**
	 * Get logged user
	 */
	@Override
	public User getLoginUser(String userId ) {
		return userDAO.getLoginUser(userId );
	}
	
	/**
	 * This method helps to re-register google authentication 
	 */
	@Override
	public void deRegisterGoogleAuth(CommonError commonError, String userId) {
		userDAO.deRegisterGoogleAuth(commonError ,userId);
		userDAO.updateGoogleAuthUser(commonError ,userId ,CommonConstants.DISABLE);
	}
	
	/**
	 * This method helps to update a user  
	 */
	@Override
	public void updateGoogleAuthUser(CommonError commonError, String userId) {
		userDAO.updateGoogleAuthUser(commonError ,userId,CommonConstants.GOOGLE_AUTH);
		
	}

	@Override
	public List<User> getGoogleAuthUserList(CommonError commonError) {
		return userDAO.getGoogleAuthUserList(commonError);
	}
	
}
