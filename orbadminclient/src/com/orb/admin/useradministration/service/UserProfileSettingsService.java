package com.orb.admin.useradministration.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface UserProfileSettingsService {

	public void saveProfileSettings(CommonError commonError, String userId, User loginUser);
	
	public User getSecondLevelType(CommonError commonError, User loginUser );
	
	public User getLoginUser( String userId );

	public void deRegisterGoogleAuth(CommonError commonError, String userId);

	public void updateGoogleAuthUser(CommonError commonError, String userId);

	public List<User> getGoogleAuthUserList(CommonError commonError);
}
