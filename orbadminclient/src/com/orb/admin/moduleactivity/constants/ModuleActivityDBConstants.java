package com.orb.admin.moduleactivity.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class ModuleActivityDBConstants {
	
	public static final String TBL_MODULE_ACTIVITY;
	public static final Map<String, String> MODULE_ACTIVITY;
	public static final Map<String,String> moduleActivityDBConfig;
	public static final String TBL_MODULE_ACTIVITY_HISTORY ;
	public static final Map<String, String> MODULE_ACTIVITY_HISTORY;
	
	static{

		TBL_MODULE_ACTIVITY = CommonUtils.getPropertyValue("MODULE_ACTIVITY", "CORE_ORB_MOD_ACTIVITIES");
		MODULE_ACTIVITY  = (Map<String,String>)CommonUtils.getBean("MODULE_ACTIVITY");
		TBL_MODULE_ACTIVITY_HISTORY = CommonUtils.getPropertyValue("MODULE_ACTIVITY_HISTORY", "CORE_ORB_MOD_ACTIVITIES_HISTORY");
		MODULE_ACTIVITY_HISTORY  = (Map<String,String>)CommonUtils.getBean("MODULE_ACTIVITY_HISTORY");
		moduleActivityDBConfig = (Map<String,String>)CommonUtils.getBean("moduleActivityDBConfig");
	}

}
