package com.orb.admin.moduleactivity.constants;

public class ModuleActivityConstants {

	public final static String LIST_PAGE = "/common/process/admin/loadModuleActivity.html";
	public final static String LIST_HTML = "admin/moduleactivity/list.xhtml";
	public final static String SELECTED_ACTIVITY_ID="SELECTED_ACTIVITY_ID";
	public final static String VIEW_PAGE = "/common/process/admin/loadModuleActivityView.html";
	public final static String VIEW_HTML = "admin/moduleactivity/view.xhtml";
	public final static String EDIT_PAGE = "/common/process/admin/loadModuleActivityAddUpdate.html";
	public final static String EDIT_HTML = "admin/moduleactivity/addUpdate.xhtml";
	public static final String ACTIVITY_TITLE_CREATION = "lbl_activity_Creation";
	public static final String ACTIVITY_TITLE_UPDATE = "lbl_activity_Update";
	public final static String ALL_MODULES="AllModules";
	public final static String ACTIVITY_ADDED_SUCCESS="module_activity_added_success";
	public final static String ACTIVITY_UPDATED_SUCCESS="module_activity_updated_success";
	public final static String ACTIVITY_TOGGLER_LIST = "ACTIVITY_TOGGLER_LIST" ;
	public final static String ACTIVITY_DELETE_SUCCESS="module_activity_deleted_success";
	public static final String SHOW_ACTIVE_MODULE_ACTIVITY = "Show Active Module Activity";
	public static final String MODULE_ACTIVITY_LOCKED_SUCCESS = "module_activity_locked_success";
	public static final String MODULE_ACTIVITY_LIST_PAGE = "MODULE ACTIVITY LIST PAGE";
	public static final String MODULE_ACTIVITY_UNLOCKED_SUCCESS = "module_activity_unlocked_success";
	public static final String MODULE_ACTIVITY_DELETE_SUCCESS = "module_activity_deleted_success";
	public final static String SELECTED_MODULE_ACTIVITY_ID = "SELECTED_MODULE_ACTIVITY_ID";
	
}
