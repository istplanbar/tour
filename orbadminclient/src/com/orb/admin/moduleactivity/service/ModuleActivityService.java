package com.orb.admin.moduleactivity.service;

import java.util.List;

import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface ModuleActivityService {

	public void add( CommonError commonError, User loginUser, ModuleActivity moduleActivity);
	public void update( CommonError commonError, User loginUser, ModuleActivity moduleActivity );
	public ModuleActivity getModuleActivity(CommonError commonError, String activityId);
	public void deleteActivity(CommonError commonError, User loginUser, List<ModuleActivity> activityList);
	public List<ModuleActivity> getInactiveModuleActivityList(CommonError commonError);
	public List<ModuleActivity> getActiveModuleActivityList(CommonError commonError);
	public void lockModuleActivity(CommonError commonError, User loginUser, List<ModuleActivity> selectedActivity);
	public void unlockModuleActivity(CommonError commonError, User loginUser, List<ModuleActivity> selectedActivity);
	public void lockViewedModuleActivity(CommonError commonError, User loginUser, ModuleActivity moduleActivity);
	public void unlockViewedModuleActivity(CommonError commonError, User loginUser, ModuleActivity moduleActivity);
	public void deleteViewedModuleActivity(CommonError commonError, User loginUser, ModuleActivity moduleActivity);
	
}
