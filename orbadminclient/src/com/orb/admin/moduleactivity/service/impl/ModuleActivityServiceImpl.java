package com.orb.admin.moduleactivity.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.moduleactivity.dao.ModuleActivityDAO;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.moduleactivity.service.ModuleActivityService;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;


@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class ModuleActivityServiceImpl implements ModuleActivityService{
	
	private static Log logger = LogFactory.getLog(ModuleActivityServiceImpl.class);
	
	@Autowired
	ModuleActivityDAO moduleActivityDAO;
	
	/**
	 * This method will interact with DAO to insert a activity in DB
	 * @param commonError
	 * @param loginUser
	 * @param moduleActivity
	 * @return
	 */
	@Override
	public void add(CommonError commonError, User loginUser, ModuleActivity moduleActivity ) {
		moduleActivityDAO.addActivity(commonError, loginUser, moduleActivity);
	}

	/**
	 * This method will interact with DAO to update a activity in DB
	 * @param commonError
	 * @param loginUser
	 * @param moduleActivity
	 * @return
	 */
	@Override
	public void update(CommonError commonError, User loginUser, ModuleActivity moduleActivity ) {
		moduleActivityDAO.updateActivity(commonError, loginUser, moduleActivity );
	}
	
	/**
	 * This method calls DAO to get all active Module Activity from DB
	 * @param commonError
	 * @param activityId
	 * @return
	 */
	public ModuleActivity getModuleActivity(CommonError commonError, String activityId){
		return moduleActivityDAO.getModuleActivity(commonError,activityId);

	}
	
	/**
	 * This method calls DAO to get all active Module Activity List from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<ModuleActivity> getActiveModuleActivityList(CommonError commonError) {
		return moduleActivityDAO.getModuleActivityList(commonError, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive Module Activity list from DB
	 * @param commonError
	 */
	@Override
	public List<ModuleActivity> getInactiveModuleActivityList(CommonError commonError) {
		return moduleActivityDAO.getModuleActivityList(commonError,CommonConstants.N);
	}
	
	/**
	 * This method calls DAO to activate the locked Material
	 * @param commonError
	 * @param loginUser
	 * @param selectedActivity
	 */
	public void unlockModuleActivity(CommonError CourseError, User loginUser, List<ModuleActivity> selectedActivity) {
		List<String> ids = new ArrayList<>();
		if( selectedActivity == null || selectedActivity.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( ModuleActivity moduleActivity : selectedActivity ){
			ids.add(moduleActivity.getId());
		}
			moduleActivityDAO.updateActiveStatus(CourseError, loginUser, ids, CommonConstants.Y);
	}
	
	/**
	 * This method calls DAO to de-activate Material
	 * @param commonError
	 * @param loginUser
	 * @param selectedActivity
	 */
	@Override
	public void lockModuleActivity(CommonError commonError, User loginUser, List<ModuleActivity> selectedActivity) {
		List<String> ids = new ArrayList<>();
		if( selectedActivity == null || selectedActivity.isEmpty() ){
			return;
		}
		for( ModuleActivity moduleActivity : selectedActivity ){
			ids.add(moduleActivity.getId());
		}
			moduleActivityDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );
	}
		
	/**
	 * This method will interact with DAO to delete a activity in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedActivity
	 */
	@Override
	public void deleteActivity(CommonError commonError, User loginUser, List<ModuleActivity>  selectedActivity ) {
		List<String> ids = new ArrayList<>();
		if( selectedActivity == null || selectedActivity.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( ModuleActivity moduleActivity : selectedActivity ){
			ids.add(moduleActivity.getId());
		}
			moduleActivityDAO.deleteActivity(commonError, loginUser, ids);
	}
		
	/**
	 * This method calls DAO to de-activate material
	 * @param commonError
	 * @param loginUser
	 * @param moduleActivity
	 */
	@Override
	public void lockViewedModuleActivity(CommonError commonError, User loginUser, ModuleActivity moduleActivity) {
		List<String> ids = new ArrayList<>();
		ids.add(moduleActivity.getId());
		moduleActivityDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.N );

	}
	/**
	 * This method calls DAO to activate material
	 * @param commonError
	 * @param loginUser
	 * @param moduleActivity
	 */
	@Override
	public void unlockViewedModuleActivity(CommonError commonError, User loginUser,ModuleActivity moduleActivity) {
		List<String> ids = new ArrayList<>();
		ids.add(moduleActivity.getId());
		moduleActivityDAO.updateActiveStatus(commonError, loginUser,ids, CommonConstants.Y );

	}

	/**
	 * This method calls DAO to deletes particular material
	 * @param commonError
	 * @param loginUser
	 * @param moduleActivity
	 */
	@Override
	public void deleteViewedModuleActivity(CommonError commonError, User loginUser, ModuleActivity moduleActivity) {
		List<String> ids = new ArrayList<>();
		ids.add(moduleActivity.getId());
		moduleActivityDAO.deleteActivity(commonError, loginUser, ids);
	}
	
}

