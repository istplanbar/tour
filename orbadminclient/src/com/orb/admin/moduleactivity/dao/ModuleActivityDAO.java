package com.orb.admin.moduleactivity.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.admin.moduleactivity.constants.ModuleActivityDBConstants;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class ModuleActivityDAO extends AbstractDAO{
	
	private static Log logger = LogFactory.getLog(ModuleActivityDAO.class);
	
	    /**
		 * This method inserts new Activity into DB
		 * @param commonError
		 * @param loginUser
		 * @param m
		 */
		public void addActivity(CommonError commonError, User loginUser,ModuleActivity m) {
			logger.info(" ==> Insert Activity START <== ");
			StringBuffer newQry = new StringBuffer();
			try{
				String nextRowId = getNextRowId( ModuleActivityDBConstants.TBL_MODULE_ACTIVITY, ModuleActivityDBConstants.moduleActivityDBConfig.get("rowPrefix") );
				m.setIdentifier(nextRowId);
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("INSERT INTO ");newQry.append( ModuleActivityDBConstants.TBL_MODULE_ACTIVITY );newQry.append("( ");
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVITY_NAME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("DESCRIPTION") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_BY") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_TIME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_BY") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_TIME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE") );
				newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
				List<Object> args = new ArrayList<Object>();
				args.add(m.getIdentifier());args.add(m.getActivityName());
				args.add(m.getDescription());
				args.add(loginUser.getUserId());
				args.add(date);args.add(loginUser.getUserId());
				args.add(date);args.add(CommonConstants.Y);
				getJdbcTemplate().update( newQry.toString(),args.toArray() );
			}catch( Exception e ){
				logger.info("Error when Insert Activity.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> Insert Activity END <== ");
		}
		
		/**
		 * This method update new Activity into DB
		 * @param commonError
		 * @param loginUser
		 * @param m
		 */
		public void updateActivity(CommonError commonError, User loginUser, ModuleActivity m) {
			logger.info(" ==> Update Activity START <== ");
			StringBuffer newQry = new StringBuffer();
			try{
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("UPDATE ");newQry.append( ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);newQry.append(" SET ");
				newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER") );newQry.append( " = ?," );
				newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVITY_NAME") );newQry.append( " = ?," );
				newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("DESCRIPTION") );newQry.append( " = ?," );
				newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_BY") );newQry.append( " = ?," );
				newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_TIME") );newQry.append( " = ?" );
				newQry.append(" WHERE ");newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );newQry.append(" = ? ");
				List<Object> args = new ArrayList<Object>();
				args.add(m.getIdentifier());
				args.add(m.getActivityName());
			    args.add(m.getDescription());
			    args.add(loginUser.getUserId());
				args.add(date);
				args.add(m.getId());
				getJdbcTemplate().update( newQry.toString(),args.toArray() );
			 }catch( Exception e ){
				logger.info("Error when Update Interruption.  " + e.getMessage());
				commonError.addError(CommonConstants.DB_ERROR);
			 }
			    logger.info(" ==>  Update MetaTable END <== ");
		}
	
		
		/**
		 * This method will return list of Module Activity from DB
		 * @param commonError
		 * @param activeFlag
		 * @return
		 */
	     public List<ModuleActivity> getModuleActivityList( CommonError commonError, String activeFlag) {
			logger.info(" ==> getModuleActivityList START <== ");
			List<ModuleActivity> moduleActivityList = null;
			try{
				StringBuffer qry = new StringBuffer("SELECT ");
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVITY_NAME") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("DESCRIPTION") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_BY") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_TIME") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_BY") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_TIME") );qry.append( "," );
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE") );
				qry.append(" FROM ");qry.append(ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);qry.append(" WHERE ");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE"));qry.append(" = ? ");
				moduleActivityList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag},new BeanPropertyRowMapper(ModuleActivity.class) );
			}
			catch( Exception e ){
				logger.error(" Exception occured when tries to fetch activity list. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			logger.info(" ==> getModuleActivityList END <== ");
			return moduleActivityList;
		}	
	     
	     /**
	      * This method will get list of Module Activity from DB
		  * @param commonError
		  * @param activityId
		  * @return
		  */
	     public ModuleActivity getModuleActivity(CommonError commonError, String activityId) {
			 logger.info(" ==> Get Activity START <== ");
			 logger.info("Activity Id ==> " + activityId );
			 ModuleActivity moduleActivity = null;
			try{
				StringBuffer qry = new StringBuffer("SELECT ");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("IDENTIFIER"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVITY_NAME"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("DESCRIPTION"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_BY"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("CREATE_TIME"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_BY"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("UPDATE_TIME"));qry.append(",");
				qry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE"));
				qry.append(" FROM ");qry.append(ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);qry.append(" WHERE ");
				qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );qry.append( " = ? " );
				moduleActivity = (ModuleActivity)getJdbcTemplate().queryForObject( qry.toString(),new String[]{activityId}, new BeanPropertyRowMapper(ModuleActivity.class) );
			}catch( Exception e ){
				logger.error(" Exception occured when tries to fetch Activity . Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
			}
			    logger.info(" ==> Get Activity END <== ");
			    return moduleActivity;
		}
	     
	     /**
	 	 * This method will activate or deactivate the moduleActivity
	 	 * @param courseError
	 	 * @param loginUser
	 	 * @param ids
	 	 * @param s
	 	 */
	 	public void updateActiveStatus( CommonError commonError, User loginUser, List<String> ids, String s ){
	 		logger.info(" ==> updateActiveStatus START <== ");
	 		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
	 		try{
	 			if(s.equals(CommonConstants.Y))
	 				insertModuleActivityHistory( commonError, loginUser,ids, CommonConstants.UNLOCKED);
	 			else
	 				insertModuleActivityHistory( commonError, loginUser,ids, CommonConstants.LOCKED);
	 			Map<String, Object> params = new HashMap<>();
		        params.put("ids", ids);
	 			StringBuffer qry = new StringBuffer();
	 			qry.append(" UPDATE ");qry.append( ModuleActivityDBConstants.TBL_MODULE_ACTIVITY );qry.append(" SET ");
	 			qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ACTIVE") );qry.append(" = ");
	 			qry.append("'"+s+"'");
				qry.append(" WHERE ");
	 			qry.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );qry.append(" IN ");
	 			qry.append(" (:ids) ");
				db.update(qry.toString(),params);
	 		}
	 		catch( Exception e ){
	 			logger.info("Error when updateActiveStatus of a activity.  " + e.getMessage());
	 			commonError.addError(CommonConstants.DB_ERROR);
	 		}
	 		logger.info(" ==> updateActiveStatus END <== ");
	 	}
	 	
	 	/**
	     * This method inserts Activity into Module Activity history table
	     * @param courseError
	     * @param loginUser
	     * @param ids
	     * @param action
	     */
	    public void insertModuleActivityHistory(CommonError courseError, User loginUser,List<String> ids, String action) throws Exception {
			logger.info(" ==> Insert Module Activity History START <== ");
			for(String id:ids)
			{
			logger.info("Module Activity Id ==> " + id );
			StringBuffer newQry = new StringBuffer();
			StringBuffer newQryHist = new StringBuffer("SELECT ");
			for( String column : ModuleActivityDBConstants.MODULE_ACTIVITY.keySet() ){
				if(!column.equals("CORE_ORB_MOD_ACTIVITIES")){
					newQryHist.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get(column) );newQryHist.append( "," );
				}
			}
			newQryHist.setLength(newQryHist.length()-1);
			newQryHist.append(" FROM ");newQryHist.append(ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);newQryHist.append(" WHERE ");newQryHist.append( ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );newQryHist.append( "=?" );
			ModuleActivity s = (ModuleActivity)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(ModuleActivity.class) );
			
			try{ 
				java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
				newQry.append("INSERT INTO ");newQry.append( ModuleActivityDBConstants.TBL_MODULE_ACTIVITY_HISTORY);newQry.append("( ");
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("IDENTIFIER") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("ACTIVITY_NAME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("DESCRIPTION") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("CREATE_BY") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("CREATE_TIME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("UPDATE_BY") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("UPDATE_TIME") );newQry.append( "," );
				newQry.append( ModuleActivityDBConstants.MODULE_ACTIVITY_HISTORY.get("ACTION") );
				newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
				List<Object> args = new ArrayList<>();
				args.add(s.getIdentifier());args.add(s.getActivityName());
				args.add(s.getDescription());
				args.add(loginUser.getUserId());args.add(date);
				args.add(loginUser.getUserId());args.add(date);args.add(action);
				getJdbcTemplate().update( newQry.toString(),args.toArray() );
			}catch( Exception e ){
				logger.info("Error when Insert ModuleActivityHistory.  " + e.getMessage());
				courseError.addError(CommonConstants.DB_ERROR);
			}
		    }
			logger.info(" ==> Insert ModuleActivity History END <== ");
		}
	 	
	     /**
	 	 * This method delete the activity into DB
	 	 * @param commonError
	 	 * @param loginUser
	 	 * @param ids
	 	 */
	 	public void deleteActivity(CommonError commonError, User loginUser, List<String> ids) {
	 		logger.info(" ==> Delete Activity START <== ");
	 		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
	 		try{
	 			insertModuleActivityHistory( commonError, loginUser,ids,CommonConstants.DELETED);
	 			Map<String, Object> params = new HashMap<>();
		        params.put("ids", ids);
	 			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
	 			newQry.append(ModuleActivityDBConstants.TBL_MODULE_ACTIVITY);newQry.append(" WHERE ");
	 			newQry.append(ModuleActivityDBConstants.MODULE_ACTIVITY.get("ID") );newQry.append( " IN " );
	 			newQry.append(" (:ids) ");
				db.update(newQry.toString(),params);
	 		}catch( Exception e ){
	 			logger.error(" Exception occured when tries to fetch activity list. Exception ==> " + e.getMessage() );
	 			commonError.addError(CommonConstants.DB_ERROR);
	 		}
	 		logger.info(" ==> Delete Activity END <== ");
	 	}
		
}
