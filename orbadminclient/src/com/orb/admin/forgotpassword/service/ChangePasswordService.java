package com.orb.admin.forgotpassword.service;

import com.orb.common.client.error.CommonError;

public interface ChangePasswordService {

	public void updatePassword(String password, String userId);
	public boolean matchPreviousPasswords(CommonError commonError, String userId, String newPass);
}
