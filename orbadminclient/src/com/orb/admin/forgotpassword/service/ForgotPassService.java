package com.orb.admin.forgotpassword.service;

import java.util.Map;

import com.orb.admin.forgotpassword.model.ForgotPass;
import com.orb.common.client.error.CommonError;

public interface ForgotPassService {

	String forgotPasswordService(CommonError commonError, String userId, String currentUrl, Map<String, String> adminConfMap);

	ForgotPass fetchForgotPassDetails(CommonError commonError, String hashValue);

	boolean resetPassword(CommonError commonError, String newPassword, ForgotPass forgetPassDetails);

}
