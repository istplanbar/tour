package com.orb.admin.forgotpassword.service.impl;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.model.ForgotPass;
import com.orb.admin.forgotpassword.service.ForgotPassService;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.EncryptionUtil;
import com.orb.common.factory.AbstractCommonFactory;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )

public class ForgotPassServiceImpl extends AbstractCommonFactory implements ForgotPassService {
	private static Log logger = LogFactory.getLog(ForgotPassServiceImpl.class);

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	ReloadableResourceBundleMessageSource bundle;
	
	@Override
	/**
	 * This method generates hash value and token
	 */
	public String forgotPasswordService(CommonError commonError, String givenUserId, String currentUrl,Map<String, String> adminConfMap) {
		User userDetail = userDAO.getLoginUser(givenUserId);
		if (null != userDetail){
			ForgotPass forgotPass = new ForgotPass();
			forgotPass.setUserId(givenUserId);
			// Generate hash value
			forgotPass.setHashValue(UUID.randomUUID().toString()+UUID.randomUUID().toString()+UUID.randomUUID().toString());
			// Generate token value
			SecureRandom random = new SecureRandom();
			forgotPass.setTempPassword(new BigInteger(32, random).toString(32));
			if(!userDAO.insertForgotPassDetails(commonError, forgotPass )){
				logger.info("Db insertion issue.");
				return ForgotPasswordConstant.TECHNICAL_ISSUE;
			}
			if (!sendEmail(commonError, userDetail, forgotPass, currentUrl, adminConfMap)){
				logger.info("Email issue.");
				return ForgotPasswordConstant.EMAIL_ERROR;
			}
		} else {
		logger.info("Invalid userID");
		return CommonConstants.INVALID_USERNAME;
		}
		return null;
	}

	/**
	 * This method will send email
	 */
	private boolean sendEmail(CommonError commonError, User user, ForgotPass forgotPass, String currentUrl, Map<String, String> adminConfMap){
		try {
	        boolean sessionDebug = false;
	        Properties props = System.getProperties();
	        String nameOfDeployment =  adminConfMap.get("nameOfDeployment");
	       
	        props.put("mail.smtp.host", ForgotPasswordConstant.SEND_MAIL_HOST);
	        props.put("mail.transport.protocol", ForgotPasswordConstant.MAIL_PROTOCOL);
	       
	        Session session = Session.getDefaultInstance(props, null);
	        // Set debug on the Session so we can see what is going on
	        // Passing false will not echo debug info, and passing true
	        // will.
	        session.setDebug(sessionDebug);
	        
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress(ForgotPasswordConstant.FROM_MAIL_ID));
	            InternetAddress[] address = { new InternetAddress(user.getEmail()) };
	            msg.setRecipients(Message.RecipientType.TO, address);
	            msg.setSubject(getJSFMessage(bundle, ForgotPasswordConstant.MAIL_SUBJECT).trim()+" "+"("+nameOfDeployment+")");
	            msg.setSentDate(new Date());
	            
				StringBuilder mailBodyLink = new StringBuilder();
				String forgotPasswordLink = (String) getSessionObj(CommonConstants.FORGOT_PASS_LINK);
				mailBodyLink.append(getJSFMessage(bundle, ForgotPasswordConstant.WELCOME_GREET));mailBodyLink.append("");
				mailBodyLink.append(user.getFirstName());mailBodyLink.append(" ");mailBodyLink.append(user.getLastName());mailBodyLink.append(",");
				mailBodyLink.append(getJSFMessage(bundle, ForgotPasswordConstant.MAIL_BODY_FOR_LINK));
				mailBodyLink.append(currentUrl);mailBodyLink.append(forgotPasswordLink);mailBodyLink.append(forgotPass.getHashValue());
	            msg.setText(mailBodyLink.toString());
	            Transport.send(msg);
	            
				StringBuilder mailBodyPass = new StringBuilder();
				mailBodyPass.append(getJSFMessage(bundle, ForgotPasswordConstant.WELCOME_GREET));mailBodyPass.append("");
				mailBodyPass.append(user.getFirstName());mailBodyPass.append(" ");mailBodyPass.append(user.getLastName());mailBodyPass.append(",");
				mailBodyPass.append(getJSFMessage(bundle, ForgotPasswordConstant.MAIL_BODY_FOR_TEMP_PASS));mailBodyPass.append(" ");
				mailBodyPass.append(forgotPass.getTempPassword());
				msg.setSubject(getJSFMessage( bundle, CommonConstants.FORGOT_PASSWORD_SUBJECT).trim()+" "+"("+nameOfDeployment+")");
				msg.setText(mailBodyPass.toString());
	            Transport.send(msg);
	        } catch (MessagingException e) {
	            logger.info("Error when sending emails.  " + e.getMessage());
				return false;
	        }
		return true;
	}
	
	@Override
	/**
	 * This method checks valid hash value and expire time. 
	 * Based on this, It fetch the Id, userId and token and hash value
	 */
	public ForgotPass fetchForgotPassDetails(CommonError commonError, String hashValue) {
		return userDAO.fetchForgotTable(commonError, hashValue );
	}
	/**
	 * This method compare the previous passwords, insert new password and update password history
	 * Deactivate the link
	 */
	@SuppressWarnings("unused")
	public boolean resetPassword(CommonError commonError, String newPassword, ForgotPass forgotPass) {
		String userId = forgotPass.getUserId();
		// True or false for check the old password
		if (Boolean.parseBoolean(ForgotPasswordConstant.OLD_PASSWORD_CHECK)){
			int oldPassCount = Integer.parseInt(ForgotPasswordConstant.OLD_PASSWORD_CHECK_COUNT);
			List<User> passwordList = userDAO.fetchPreviousPasswords(commonError, userId, oldPassCount);
			if (null != passwordList){
				// Compare previous three passwords with new password
				for (User password : passwordList){
					String pass = EncryptionUtil.decrypt(password.getPassword());
					if (EncryptionUtil.matchPassword(newPassword, password.getPassword())){
						logger.info("Password doesn't unique");
						return false;
					}
				}
				updatePassword(commonError, newPassword, forgotPass);
				unlockUser(forgotPass);
				return true;
			}
		} else {
			updatePassword(commonError, newPassword, forgotPass);
			unlockUser(forgotPass);
			return true;
		}
		return false;
	}
	
	private void updatePassword(CommonError commonError, String newPassword, ForgotPass forgotPass){
		String userId = forgotPass.getUserId();
		String password = EncryptionUtil.encryptBCrypt(newPassword);
		userDAO.insertForgotPassHistory(commonError, userId);
		userDAO.updateUserPassword(commonError, password, userId );
		userDAO.deactivateLink(commonError, forgotPass.getHashValue());
	}
	
	private void unlockUser(ForgotPass forgotPass){
		userDAO.resetFailureCount(forgotPass.getUserId());
	}
	
}
