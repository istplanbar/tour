package com.orb.admin.forgotpassword.service.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.forgotpassword.constant.ForgotPasswordConstant;
import com.orb.admin.forgotpassword.service.ChangePasswordService;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.EncryptionUtil;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class ChangePasswordServiceImpl implements ChangePasswordService{
	private static Log logger = LogFactory.getLog(ForgotPassServiceImpl.class);
	
	@Autowired
	UserDAO userDAO;
	
	@Override
	public void updatePassword(String password, String userId) {
		userDAO.updatePassword(password,userId);
	}
	
	public boolean matchPreviousPasswords(CommonError commonError, String userId, String newPass){
		int oldPassCount = Integer.parseInt(ForgotPasswordConstant.OLD_PASSWORD_CHECK_COUNT);
		List<User> passwordList = userDAO.fetchPreviousPasswords(commonError, userId, oldPassCount);
		if (null != passwordList){
			// Compare previous three passwords with new password
			for (User password : passwordList){
				String pass = EncryptionUtil.decrypt(password.getPassword());
				if (EncryptionUtil.matchPassword(newPass, password.getPassword())){
					logger.info("Password doesn't unique");
					return false;
				}
			}
		}
	return true;
	}
}
