package com.orb.admin.forgotpassword.model;

public class ChangePassword {

	private String currentPass;
	private String newPass;
	private String repeatPass;
	
	public String getCurrentPass() {
		return currentPass;
	}
	public void setCurrentPass(String currentPass) {
		this.currentPass = currentPass;
	}
	public String getNewPass() {
		return newPass;
	}
	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}
	public String getRepeatPass() {
		return repeatPass;
	}
	public void setRepeatPass(String repeatPass) {
		this.repeatPass = repeatPass;
	}
	
}
