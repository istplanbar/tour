package com.orb.admin.forgotpassword.constant;

import com.orb.common.client.utils.CommonUtils;

public class ForgotPasswordConstant {
	
	public static final String HASH_VALUE = "hash_value";
	public static final String FORGOT_PASS = "forgot_pass";
	public static final String RESET_PASS = "reset_pass";
	public static final String MAIL_SUBJECT = "mail_subject";
	public static final String WELCOME_GREET = "Welcome_greet";
	public static final String URLForForgotPass ="url";
	public static final String USER_ACTION = "action";
	public static final String INVALID_URL = "Invalid_url";
	public static final String URL_EXPIRED = "url_expired";
	public static final String MAIL_BODY_FOR_LINK = "body_link";
	public static final String MAIL_BODY_FOR_TEMP_PASS = "body_temp_pass";
	public static final String WRONG_TEMP_PASS = "Invalid_temp_pass";
	public static final String PASSWORD_MATCH = "password_match";
	public static final String PASSWORD_PATTERN = "Password_pattern";
	public static final String UNIQUE_PASSWORD = "Unique_password";
	public static final String EMAIL_ERROR = "email_error";
	public static final String TECHNICAL_ISSUE = "db_issue";
	public static final String CHECK_MAIL = "check_mail";
	public static final String RESET_DONE = "reset_done";
	public final static String RESET_PASS_PAGE = "/forgotpass/resetPassword.html";
	public final static String FORGOT_PASS_PAGE = "/forgotpass/forgotPassword.html";
	public final static String LOGIN_PAGE = "/index.jsp";
	public final static String LOGIN_PAGE_FORGOT = "/common/init/login?forgot";
	public final static String LOGIN_PAGE_RESET_SUCCESS = "/common/init/login?resetSuccess";
	public static final String PASSWORD_REGEX ;
	public static final String OLD_PASSWORD_CHECK;
	public static final String OLD_PASSWORD_CHECK_COUNT;
	public static final String PASSWORD_REGEX_MESSAGE = "passwordRegexMessage";
	public static final String SEND_MAIL_HOST;
	public static final String FROM_MAIL_ID;
	public static final String MAIL_PROTOCOL;
	public final static String FORGOT_PASS_REDIR = "/common/resetpass/forgotPass";
	
	static{
		PASSWORD_REGEX = CommonUtils.getPropertyValue("passwordProperties", "passwordRegex");
		OLD_PASSWORD_CHECK = CommonUtils.getPropertyValue("passwordProperties", "oldPasswordCheck");
		OLD_PASSWORD_CHECK_COUNT = CommonUtils.getPropertyValue("passwordProperties", "oldPasswordCheckCount");
		SEND_MAIL_HOST = CommonUtils.getPropertyValue("mailSenderValues", "host");
		FROM_MAIL_ID = CommonUtils.getPropertyValue("mailSenderValues", "from");
		MAIL_PROTOCOL = CommonUtils.getPropertyValue("mailSenderValues", "protocol");
	}
}
