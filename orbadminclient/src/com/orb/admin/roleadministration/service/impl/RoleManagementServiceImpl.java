
package com.orb.admin.roleadministration.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.modulemanagement.dao.ModuleManagementDAO;
import com.orb.admin.roleadministration.dao.RoleManagementDAO;
import com.orb.admin.roleadministration.model.Permission;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.roleadministration.service.RoleManagementService;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class RoleManagementServiceImpl implements RoleManagementService{
	
	private static Log logger = LogFactory.getLog(RoleManagementServiceImpl.class);

	@Autowired
	ModuleManagementDAO moduleManagementDAO;
	@Autowired  
	RoleManagementDAO roleManagementDAO;
	@Autowired
	UserDAO userDAO;

	/**
	 * This method will get the main menu list for list page.
	 * @param commonError
	 * @return
	 */
	@Override
	public List<MetaTable> getMainMenuList(CommonError commonError) {
		return roleManagementDAO.getMenus(commonError);
	}
	
	/**
	 * This method will get the filtered meta table list for list page.
	 * @param commonError
	 * @param pageIds
	 * @return
	 */
	public List<MetaTable> getFilteredMetaTableList(CommonError commonError,Set<Integer> pageIds) {
		List<MetaTable> filterMetaTableList=new ArrayList<>();
		for(Integer i:pageIds){
			filterMetaTableList.add(moduleManagementDAO.getMetaTable(commonError, String.valueOf(i)));
		}
		return filterMetaTableList;
	}
	
	/**
	 * This method will get the submodule list for list page.
	 * @param commonError
	 * @param mainModules
	 * @return
	 */
	@Override
	public List<Role> getSubModuleList(CommonError commonError, List<MetaTable> mainModules) {
		List<Role> submodules = roleManagementDAO.getSubMenus(commonError, mainModules);
		List<Role> selectedModuleList = new ArrayList<>();
		selectedModuleList = getSelectedModulesMapping(commonError,submodules);
		List<MetaTable> metaTableList = moduleManagementDAO.getModuleActivityMappingList(commonError);
		for(Role role:selectedModuleList){
			List<String> moduleActivityList = metaTableList.stream().filter(metaTable -> role.getPageId() == metaTable.getPageId()).map(MetaTable::getActivity).collect(Collectors.toList());
			if(!moduleActivityList.isEmpty())
			role.setActivityList(moduleManagementDAO.getModuleActivity(commonError, moduleActivityList));
		}
		return selectedModuleList;		
	}
	
	/**
	 * This method will get the selected module list for list page.
	 * @param commonError
	 * @param selectedModules
	 * @return
	 */
	public List<Role> getSelectedModulesMapping(CommonError commonError, List<Role> selectedModules){
		List<String> identifiers = new ArrayList<>();
		List<MetaTable> metaTableList = moduleManagementDAO.getModuleActivityMappingList(commonError);
		for(Role r : selectedModules){
			identifiers = metaTableList.stream().filter(metaTable -> r.getPageId() == metaTable.getPageId()).map(MetaTable::getActivity).collect(Collectors.toList());
			if(!identifiers.isEmpty()){
			   r.setActivityList(moduleManagementDAO.getModuleActivity(commonError, identifiers));	
			   break;
			}
		}
		return selectedModules;
	}
	
	/**
	 * This method will call the DAO for get the permission types
	 * @param commonError
	 * @return
	 */
	public List<Permission> getPermission(CommonError commonError) {
		return roleManagementDAO.getPermission(commonError);
	}

	/**
	 * This method will call the DAO for get the layout based on submodules
	 * @param commonError
	 * @param roleList
	 * @return 
	 */
	public List<Layout> getLayout(CommonError commonError, List<Role> roleList) {
		return roleManagementDAO.getLayout(commonError, roleList);
	}

	/**
	 * This method will call the DAO for create Roles
	 * @param commonError
	 * @param roleList
	 * @param role
	 * @param loginUser
	 */
	public void createRoles(CommonError commonError,List<Role> roleList, Role role,User loginUser) {
		roleManagementDAO.insertRole(commonError,roleList,role,loginUser);
		
	}
	
	/**
	 * This method will call the DAO for select RoleDesc
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleDesc(CommonError commonError, String roleId) {
		return roleManagementDAO.getRoleDesc(commonError, roleId);
	}

	/**
	 * This method will call the DAO for update Roles
	 * @param commonError
	 * @param selectedSubModules
	 * @param role
	 * @param loginUser
	 */
	public void updateRole(CommonError commonError, List<Role> selectedSubModules, Role role, User loginUser) {
		List<String> identifiers=new ArrayList<>();
		identifiers.add(role.getIdentifier());
		roleManagementDAO.deleteRoleActivityMapping(commonError,loginUser, identifiers);
		roleManagementDAO.deleteRoleModMapping(commonError,loginUser, identifiers);
		roleManagementDAO.updateRole(commonError,selectedSubModules, role, loginUser);
	}

	/**
	 * This method will call the DAO for delete Roles
	 * @param commonError
	 * @param loginUser
	 * @param identifier
	 */
	public void deleteRole(CommonError commonError,User loginUser, String identifier) {
		List<String> identifiers=new ArrayList<>();
		identifiers.add(identifier);
		roleManagementDAO.deleteRole(commonError,loginUser, identifiers);
		roleManagementDAO.deleteRoleActivityMapping(commonError,loginUser, identifiers);
		roleManagementDAO.deleteRoleModMapping(commonError,loginUser, identifiers);
	}
	
	/**
	 * This method will call the DAO for Role details
	 * @param commonError
	 * @param roleId 
	 * @return
	 */
	public List<Role> getRoleDetails(CommonError commonError, String roleId) {
		return roleManagementDAO.getRoleDescDetails(commonError, roleId);
	}

	/**
	 * This method will call the DAO for selected Menu
	 * @param commonError
	 * @param roleId 
	 * @return
	 */
	public List<MetaTable> getSelectedMenu(CommonError commonError, String roleId) {
		return roleManagementDAO.getSelectedMenu(commonError, roleId);
	}

	/**
	 * This method will call the DAO for MetaTable list
	 * @param commonError
	 * @param mainModulesTarget 
	 * @return
	 */
	public List<MetaTable> getMetaTableList(CommonError commonError, List<MetaTable> mainModulesTarget) {
		return roleManagementDAO.getMainModules(commonError, mainModulesTarget);
	}
	
	/**
	 * This method calls DAO to get all active role from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Role> getActiveRoleList(CommonError commonError) {
		return roleManagementDAO.getRoles(commonError, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive role level from DB
	 * @param commonError
	 * @return
	 */
	@Override
	public List<Role> getInactiveRoleList(CommonError commonError) {
		return roleManagementDAO.getRoles(commonError,CommonConstants.N);
	}
	
	/**
	 * This method calls DAO to activate the locked Role
	 * @param commonError
	 * @param loginUser
	 * @param selectedRoles
	 * @return
	 */
	public void unlockRole(CommonError commonError, User loginUser, List<Role> selectedRoles) {
		List<String> identifiers=new ArrayList<>();
		if( selectedRoles == null || selectedRoles.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Role role : selectedRoles ){
			identifiers.add(role.getIdentifier());
		}
		roleManagementDAO.updateActiveStatus(commonError, loginUser, identifiers, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to de-activate Role
	 * @param commonError
	 * @param loginUser
	 * @param selectedRoles
	 */
	@Override
	public void lockRole(CommonError commonError, User loginUser, List<Role> selectedRoles) {
		List<String> identifiers=new ArrayList<>();
		if( selectedRoles == null || selectedRoles.isEmpty() ){
			return;
		}
		for( Role role : selectedRoles ){
			identifiers.add(role.getIdentifier());
		}
		logger.info(identifiers);
		roleManagementDAO.updateActiveStatus(commonError, loginUser,identifiers, CommonConstants.N );
	}
	
	/**
	 * This method calls DAO to de-activate role
	 * @param courseError
	 * @param loginUser
	 * @param identifier
	 */
	@Override
	public void lockViewedRole(CommonError courseError, User loginUser, String identifier) {
		List<String> identifiers=new ArrayList<>();
		identifiers.add(identifier);
		roleManagementDAO.updateActiveStatus(courseError, loginUser,identifiers, CommonConstants.N );

	}
	
	/**
	 * This method calls DAO to activate role
	 * @param courseError
	 * @param loginUser
	 * @param identifier
	 */
	@Override
	public void unlockViewedRole(CommonError courseError, User loginUser,String identifier) {
		List<String> identifiers=new ArrayList<String>();
		identifiers.add(identifier);
		roleManagementDAO.updateActiveStatus(courseError, loginUser,identifiers, CommonConstants.Y );

	}
	
	/**
	 * This method will interact with DAO to delete a role in DB
	 * @param commonError
	 * @param loginUser
	 * @param selectedRole
	 */
	@Override
	public void delete(CommonError commonError, User loginUser, List<Role>  selectedRole ) {
		List<String> identifiers=new ArrayList<>();
		if( selectedRole == null || selectedRole.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Role role : selectedRole ){
			identifiers.add(role.getIdentifier());
		}
		roleManagementDAO.deleteRole(commonError,loginUser, identifiers);
		roleManagementDAO.deleteRoleActivityMapping(commonError,loginUser,  identifiers);
		roleManagementDAO.deleteRoleModMapping(commonError,loginUser,  identifiers);
		
		
	}
	
	/**
	 * This method will call the DAO to get role
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public Role getRole(CommonError commonError, String roleId) {
		return roleManagementDAO.getRole(commonError, roleId);
	}
	
	/**
	 * This method will call the DAO to get role list
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoles(CommonError commonError, String roleId) {
		return roleManagementDAO.getRoleList(commonError, roleId);
	}

	/**
	 * This method will call the DAO to get activity mapping list
	 * @param commonError
	 * @return
	 */
	@Override
	public List<MetaTable> getActivityMappingList(CommonError commonError) {
		return roleManagementDAO.getActivityMappingList(commonError);
	}
	
	/**
	 * This method will call the DAO for Role details
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleMapping(CommonError commonError, String roleId) {
		return roleManagementDAO.getRoleMapping(commonError, roleId);
	}
	
	/**
	 * This method will call the DAO to get  Role activity mapping details
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleAcvtMapList(CommonError commonError, String roleId) {
		return roleManagementDAO.getRoleAcvtMapList(commonError, roleId);
	}
	
	/**
	 * This method will call the DAO to get  Role module mapping details
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	
	public List<Role> getRoleModMapList(CommonError commonError, String roleId) {
		List<Role> roleMapList = roleManagementDAO.getRoleModMapList(commonError, roleId);
		List<MetaTable> modActivityList = new  ArrayList<>();
		modActivityList = roleManagementDAO.getRoleActivityMapList(commonError);
		logger.info(modActivityList);
		for(Role role:roleMapList){
			role.setActivities(modActivityList.stream().filter(metaTable -> role.getIdentifier().equals(metaTable.getIdentifier()) && metaTable.getPageId() == role.getPageId()).map(MetaTable::getActivityIdentifier).collect(Collectors.toList()));
			if(!role.getActivities().isEmpty()){
				 role.setActivityList(moduleManagementDAO.getModuleActivity(commonError, role.getActivities()));
			}
			role.setActivity(role.getActivityList().stream()
					 .map(ModuleActivity::getActivityName)
					 .collect(Collectors.joining(", ")));
		}

		return roleMapList; 
	}

}
