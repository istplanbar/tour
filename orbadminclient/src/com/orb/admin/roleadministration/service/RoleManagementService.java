package com.orb.admin.roleadministration.service;

import java.util.List;
import java.util.Set;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.roleadministration.model.Permission;
import com.orb.admin.roleadministration.model.Role;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;

public interface RoleManagementService {

	public List<Role> getSubModuleList(CommonError commonError, List<MetaTable> mainModules);
	
	public List<Role> getSelectedModulesMapping(CommonError commonError, List<Role> selectedModules);
	
	public List<Permission> getPermission(CommonError commonError);

	public List<Layout> getLayout(CommonError commonError, List<Role> subModulesList);

	public void createRoles(CommonError commonError,List<Role> roleList, Role role,User loginUser);

	public List<Role> getRoleDesc(CommonError commonError, String roleId);

	public void updateRole(CommonError commonError,List<Role> selectedSubModule, Role role,User loginUser);

	public void deleteRole(CommonError commonError, User loginUser,String identifier);

	public List<Role> getRoleDetails(CommonError commonError, String roleId);

	public List<MetaTable> getSelectedMenu(CommonError commonError, String roleId);

	public List<MetaTable> getMainMenuList(CommonError commonError);

	public List<MetaTable> getMetaTableList(CommonError commonError, List<MetaTable> mainModulesTarget);
	
	public List<Role> getActiveRoleList(CommonError commonError);
	
	public List<Role> getInactiveRoleList(CommonError commonError);
	
	public void lockRole(CommonError commonError, User loginUser, List<Role> roles);
	
	public void unlockRole(CommonError commonError, User loginUser, List<Role> roles);
	
	public void lockViewedRole(CommonError CourseError, User loginUser, String roleId);
	
	public void unlockViewedRole(CommonError CourseError, User loginUser, String roleId);
	
	public void delete(CommonError commonError, User loginUser, List<Role> roles);
	
	public Role getRole(CommonError commonError, String roleId);
	
	public List<Role> getRoles(CommonError commonError, String roleId);
	
	public List<Role> getRoleAcvtMapList(CommonError commonError, String roleId);
	
	public List<Role> getRoleModMapList(CommonError commonError, String roleId);

	public List<MetaTable> getActivityMappingList(CommonError commonError);
	
	public List<Role> getRoleMapping(CommonError commonError, String roleId);

	public List<MetaTable> getFilteredMetaTableList(CommonError commonError, Set<Integer> ids);
}
