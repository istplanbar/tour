package com.orb.admin.roleadministration.constants;

public class RoleManagementConstants {
	
	public final static String VIEW_PAGE_REDIR = "/common/process/admin/loadRoleManagementView.html";
	public final static String SELECTED_ROLE_ID = "SELECTED_ROLE_ID";
	public final static String SELECTED_ID = "SELECTED_ID";
	public final static String VIEW = "VIEW";
	public final static String LIST = "LIST";
	public final static String ADD = "ADD";
	public final static String UPDATE = "UPDATE";
	public final static String LIST_PAGE_REDIR = "/common/process/admin/loadRoleManagementList.html";
	public final static String ADD_UPDATE_PAGE_REDIR = "/common/process/admin/loadRoleManagementAddUpdate.html";
	public final static String LIST_PAGE = "admin/role/roleManagementList.xhtml";
	public final static String VIEW_PAGE = "admin/role/roleManagementView.xhtml";
	public final static String ADD_UPDATE_PAGE = "admin/role/roleManagementAddUpdate.xhtml";
	public final static String SHOW_ACTIVE_ROLE="SHOW_ACTIVE_ROLE";
	public final static String ROLE_TOGGLER_LIST = "ROLE_TOGGLER_LIST" ;
	public final static String ROLE_LOCKED_SUCCESS="role_locked_success";
	public final static String ROLE_UNLOCKED_SUCCESS="role_unlocked_success";
	public final static String ROLE_DELETE_SUCCESS="role_delete_success";
	public final static String SHOW_ACTIVE_STATUS="SHOW_ACTIVE_STATUS";
	public final static String ROLE_ADDED_SUCCESS="role_added_success";
	public final static String ROLE_UPDATED_SUCCESS="role_update_success";

}
