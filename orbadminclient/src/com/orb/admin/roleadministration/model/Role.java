package com.orb.admin.roleadministration.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.model.ModuleActivity;

public class Role extends MetaTable {
	private Integer id;
	private String identifier;
	private Integer roleId;
	private String roleName;
	private Integer submenuId;
	private String permissionId;
	private String permissionName;
	private Integer layoutId;
	private String layoutName;
	private boolean checked;
	private String active;
	private Set<MetaTable> selectedModules=new HashSet<MetaTable>();
	private List<ModuleActivity> activityList = new ArrayList<ModuleActivity>();
	private int mainModule;
	private int subModule;
	private String activity;
	private List<String> activities;
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getSubmenuId() {
		return submenuId;
	}
	public void setSubmenuId(Integer submenuId) {
		this.submenuId = submenuId;
	}
	public String getPermissionId() {
		return permissionId;
	}
	public String getLayoutName() {
		return layoutName;
	}
	public void setLayoutName(String layoutName) {
		this.layoutName = layoutName;
	}
	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}
	public Integer getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(Integer layoutId) {
		this.layoutId = layoutId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
		
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	public List<ModuleActivity> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<ModuleActivity> activityList) {
		this.activityList = activityList;
	}
	
	public int getMainModule() {
		return mainModule;
	}
	public void setMainModule(int mainModule) {
		this.mainModule = mainModule;
	}
	public int getSubModule() {
		return subModule;
	}
	public void setSubModule(int subModule) {
		this.subModule = subModule;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public Set<MetaTable> getSelectedModules() {
		return selectedModules;
	}
	public void setSelectedModules(Set<MetaTable> selectedModules) {
		this.selectedModules = selectedModules;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public List<String> getActivities() {
		return activities;
	}
	public void setActivities(List<String> activities) {
		this.activities = activities;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
		
		
	
	
}
