
package com.orb.admin.roleadministration.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.admin.client.dao.AbstractAdminDAO;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.db.constants.AdminDBConstants;
import com.orb.admin.layoutadmnistration.model.Layout;
import com.orb.admin.roleadministration.model.Permission;
import com.orb.admin.roleadministration.model.Role;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class RoleManagementDAO extends AbstractAdminDAO {
	private static Log logger = LogFactory.getLog(RoleManagementDAO.class);

	/**
	 * Method  to get permission list
	 * @param commonError
	 * @return
	 */
	public List<Permission> getPermission(CommonError commonError) {
		logger.info(" ==> getPermission START <== ");
		List<Permission> permission = new ArrayList<>();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_NAME"));qry.append(" FROM ");
			qry.append(AdminDBConstants.TBL_PERMISSION_TABLE);
			permission = (List<Permission>)getJdbcTemplate().query(qry.toString(), new BeanPropertyRowMapper<Permission>(Permission.class));

		}catch( Exception e ){
			logger.info("Error when retrieve permission List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getPermission END <== ");
		if (null == permission) {
			logger.error(" Query returns NULL ");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		return permission;
	}

	/**
	 * Method  for get layout list
	 * @param commonError
	 * @param roleList
	 * @return
	 */
	public List<Layout> getLayout(CommonError commonError, List<Role> roleList) {
		logger.info(" ==> getLayout START <== ");
		if( null == roleList || roleList.isEmpty() ){
			logger.error(" Role list is not available ");
			return null;
		}
		List<Integer> roleIdList = new ArrayList<>();
		for(Role subModule : roleList) {
			roleIdList.add(subModule.getPageId());
			}
		List<Layout> layoutList = null;
		int size = roleList.size();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.LAYOUT_TABLE.get("LAYOUT_NAME"));qry.append(" , ");
			qry.append(AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_LAYOUT_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.LAYOUT_TABLE.get("SUBMENU_ID"));qry.append(" IN ( ");
			for ( int i=0; i<size; i++ ) {
				qry.append(" ? ");qry.append(",");
			}
			qry.setLength(qry.length() - 1);
			qry.append(") ");
			layoutList = (List<Layout>)getJdbcTemplate().query(qry.toString(), roleIdList.toArray(),  new BeanPropertyRowMapper<Layout>(Layout.class));
		}catch( Exception e ){
			logger.info("Error when retrieve Layout List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == layoutList) {
			logger.error(" Query returns Null ");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getLayout END <== ");
		return layoutList;
	}

	/**
	 * Method  for insert a role
	 * @param commonError
	 * @param roleList
	 * @param role
	 * @param loginUser
	 * @return
	 */
	public Integer insertRole(CommonError commonError,List<Role> roleList, Role role,User loginUser) {
		logger.info(" ==> Insert Role Creation START <== ");
		if( CommonUtils.getValidString(role.getRoleName()).isEmpty()){
			logger.error("Can't insert Role");
			return null;
		}
		int nextRowId1 = 0;
		try{
			String nextRowId = getNextRowId( AdminDBConstants.TBL_ROLE_TABLE, AdminDBConstants.roleDBConfig.get("rowPrefix") );
			nextRowId1 = Integer.parseInt(getNextRowId( AdminDBConstants.TBL_ROLE_TABLE));
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_ROLE_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("CREATE_BY"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("CREATE_TIME"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_BY"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_TIME"));queryInsert.append(",");
			queryInsert.append(AdminDBConstants.ROLE_TABLE.get("ACTIVE"));
			queryInsert.append(") VALUES(?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<>();
			args.add(nextRowId);args.add(role.getRoleName());
			args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
			addModActivity(commonError, loginUser,nextRowId, roleList);
			for(Role r:roleList){
				r.setIdentifier(nextRowId);
				addRoleActivity(commonError, loginUser,r);
			}
		}catch( Exception e ){
			logger.error("Error when Insert Role Creation.  " + e.getMessage());
			if(commonError != null)
				if(e.getMessage().contains("ROLE_NAME_UNIQUE")){
					commonError.addError("ROLE_NAME_UNIQUE_ERROR");
				}else{
					commonError.addError(CommonConstants.DB_ERROR);
				}
		}
		logger.info(" ==> Insert Role Creation END <== ");
		return nextRowId1;
	}
	
	/**
	 * This method inserts new modActivity into DB
	 * @param commonError
	 * @param loginUser
	 * @param identifier
	 * @param roleList
	 */
	public void addModActivity(CommonError commonError, User loginUser, String identifier, List<Role> roleList) throws Exception {
		logger.info("==> addRole  <==");
		logger.info("Role Id ==> " +   identifier );
		try	{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());	
		StringBuffer queryInsert = new StringBuffer();
		queryInsert.append("INSERT INTO ");queryInsert.append( AdminDBConstants.TBL_ROLE_MOD_MAPPING_TABLE );queryInsert.append("( ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("PAGE_ID"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("CREATE_BY"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("CREATE_TIME"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("UPDATE_BY"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("UPDATE_TIME"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("ACTIVE"));
		queryInsert.append(") VALUES(?,?,?,?,?,?,?)  ");
		getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {

	         public void setValues(PreparedStatement ps, int i) throws SQLException {	
				ps.setString(1, identifier);
				ps.setInt(2, roleList.get(i).getPageId());
				ps.setString(3,loginUser.getUserId());
				ps.setTimestamp(4,date);
				ps.setString(5,loginUser.getUserId());
				ps.setTimestamp(6,date);
				ps.setString(7,CommonConstants.Y);
			}		
			public int getBatchSize() {
				return roleList.size();
			} 
		});
		}catch( Exception e ){
			logger.info("Error when Insert Role.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Role END <== ");
	}
	
	/**
	 * This method inserts new roleActivity into DB
	 * @param commonError
	 * @param loginUser
	 * @param role
	 */
	public void addRoleActivity(CommonError commonError, User loginUser, Role role) throws Exception {
		logger.info("==> addRoleActivity  <==");
		logger.info("Role Id ==> " +   role.getIdentifier() );
		try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());	
		StringBuffer queryInsert = new StringBuffer();
		queryInsert.append("INSERT INTO ");queryInsert.append( AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE );queryInsert.append("( ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("PAGE_ID"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVITY_IDENTIFIER"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("CREATE_BY"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("CREATE_TIME"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("UPDATE_BY"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("UPDATE_TIME"));queryInsert.append(", ");
		queryInsert.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVE"));
		queryInsert.append(") VALUES(?,?,?,?,?,?,?,?)  ");
		getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {

			public void setValues(PreparedStatement ps, int i) throws SQLException {	
				ps.setString(1, role.getIdentifier());
				ps.setInt(2, role.getPageId());
				ps.setString(3, role.getActivities().get(i));
				ps.setString(4,loginUser.getUserId());
				ps.setTimestamp(5,date);
				ps.setString(6,loginUser.getUserId());
				ps.setTimestamp(7,date);
				ps.setString(8,CommonConstants.Y);
				
			}		
			public int getBatchSize() {
				return role.getActivities().size();
			}
		});
		}catch( Exception e ){
			logger.info("Error when Insert RoleActivity.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert RoleActivity END <== ");
	}
	
		
	/**
	 * Method  for insert a role description
	 * @param commonError
	 * @param role
	 * @param id
	 */
	public void insertRoleDesc(CommonError commonError, List<Role> roles, int id) {
		logger.info(" ==> Insert RoleDecs Creation START <== ");
		if( null == roles || 0 == id){
			logger.error("Can't insert Role Desc");
			return;
		}
		try{
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_DESC_TABLE.get("PERMISSION_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_DESC_TABLE.get("LAYOUT_ID"));
			queryInsert.append(") VALUES(?,?,?,?) ");
			getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setLong(1, id);
					ps.setLong(2, roles.get(i).getPageId());
					ps.setObject(3, roles.get(i).getPermissionId());	
					ps.setLong(4, 1);
				}		
				public int getBatchSize() {
					return roles.size();
				}
			});
		}catch( Exception e ){
			logger.error("Error when Insert RoleDecs Creation.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert RoleDecs Creation END <== ");
	}
	
	/**
	 * Method  for insert a user role
	 * @param commonError
	 * @param userId
	 * @param id
	 */
	public void insertUserRole(CommonError commonError, int userId, int id) {
		logger.info(" ==> Insert userRole Creation START <== ");
		if( 0 == id && 0 == userId){
			logger.error("Can't insert new userRole");
			return;
		}
		try{
			List<Object> args = new ArrayList<>();
			args.add(userId);
			args.add(id);
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_USER_ROLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.USER_ROLE.get("USER_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.USER_ROLE.get("ROLE_ID"));queryInsert.append(") VALUES(?,?) ");

			getJdbcTemplate().update( queryInsert.toString(), args.toArray());
		}catch( Exception e ){
			logger.error("Error when Insert userRole Creation.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert userRole Creation END <== ");
		
	}

	/**
	 * Method  for get user role list
	 * @param commonError
	 * @param activeFlag 
	 * @return
	 */
	public List<Role> getRoles(CommonError commonError,String activeFlag) {
		logger.info(" ==> selectRole START <== ");
		List<Role> roles = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append("*");
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.ROLE_TABLE.get("ACTIVE"));qry.append(" = ? ");
			qry.append(" ORDER BY ");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));

			roles = getJdbcTemplate().query(qry.toString(), new Object[]{activeFlag}, new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.info("Error when Select RoleDecs Creation.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> selectRole END <== ");
		if (null == roles){
			logger.info("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		return roles;
	}
	
	/**
	 * Method  for get role description list
	 * @param commonError
	 * @param id
	 * @return
	 */
	public List<Role> getRoleDesc(CommonError commonError, String id) {
		logger.info(" ==> select RoleDesc START <== ");
		if( CommonUtils.getValidString(id).isEmpty() ){
			logger.error(" User doesn't select any role id ");
			return null;
		}
		List<Role> roles = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");qry.append("*");
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);qry.append(" AS RD ");
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);qry.append(" AS R ");
			qry.append(" ON ");qry.append(" RD.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append(" = ");
			qry.append(" R.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));
			qry.append(" WHERE ");qry.append(" R.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append(" = (?) ");
			roles = getJdbcTemplate().query(qry.toString(), new Object[]{id}, new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.error("Error when Select RoleDecs .  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == roles){
			logger.info("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> select RoleDesc END <== ");
		return roles;
	}

	/**
	 * Method  for update role
	 * @param commonError
	 * @param selectedRoles
	 * @param role
	 * @param loginUser
	 */
	public void updateRole(CommonError commonError,List<Role> selectedRoles,Role role,User loginUser) {
		logger.info(" ==> updateRole START <== ");
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			StringBuffer qry = new StringBuffer();
			qry.append("UPDATE ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);
			qry.append(" SET ");
			qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));qry.append(" = ?,");
			qry.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_BY"));qry.append(" = ?,");
			qry.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_TIME"));qry.append(" = ?");
			qry.append(" WHERE ");qry.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));qry.append(" = ?");
			List<Object> args = new ArrayList<>(); 
			args.add(role.getRoleName());
			args.add(loginUser.getUserId());
			args.add(date);
			args.add(role.getIdentifier());
			getJdbcTemplate().update(qry.toString(), args.toArray());
			addModActivity(commonError, loginUser,role.getIdentifier(), selectedRoles);
			for(Role r:selectedRoles){
				r.setIdentifier(role.getIdentifier());
				addRoleActivity(commonError, loginUser,r);
			}
					
			}catch( Exception e ){
			logger.error("Error when Update Role .  " + e.getMessage());
			if(commonError != null)
				if(e.getMessage().contains("ROLE_NAME_UNIQUE")){
					commonError.addError("ROLE_NAME_UNIQUE_ERROR");
				}else{
					commonError.addError(CommonConstants.DB_ERROR);
				}	
			}

		logger.info(" ==> updateRole END <== ");

	}

	/**
	 * Method  for insert role history
	 * @param commonError
	 * @param roles
	 * @param id
	 * @param roleName
	 */
	public void insertRoleHistory(CommonError commonError, List<Role> roles, int id, String roleName) {
		logger.info(" ==> Insert Rolehistory Creation START <== ");
		if( null == roles || 0 == id || CommonUtils.getValidString(roleName).isEmpty()){
			logger.error("Can't create role history");
			return;
		}
		try{
			StringBuffer queryInsert = new StringBuffer();
			queryInsert.append("INSERT INTO ");
			queryInsert.append(AdminDBConstants.TBL_ROLE_HISTORY_TABLE);queryInsert.append("( ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("ROLE_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("SUBMENU_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("PERMISSION_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("LAYOUT_ID"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("ROLE_NAME"));queryInsert.append(" , ");
			queryInsert.append(AdminDBConstants.ROLE_HISTORY_TABLE.get("ACTUAL_TIME"));
			queryInsert.append(") VALUES(?,?,?,?,?,now()) ");
			getJdbcTemplate().batchUpdate(queryInsert.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setLong(1, id);
					ps.setLong(2, roles.get(i).getPageId());
					ps.setString(3, roles.get(i).getPermissionId());
					ps.setLong(4, 1);
					ps.setString(5, roleName);
				}		
				public int getBatchSize() {
					return roles.size();
				}
			});
		}catch( Exception e ){
			logger.error("Error when Insert Rolehistory Creation.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Rolehistory Creation END <== ");
	}

	/**
	 * Method  for delete a role
	 * @param commonError
	 * @param loginUser
	 * @param identifiers
	 */
	public void deleteRole(CommonError commonError,User loginUser, List<String> identifiers) {
		logger.info(" ==> deleteRoles START <== ");
		logger.info("ROLE IDENTIFIER  ==>"+  identifiers);
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertRolesHistory( commonError, loginUser,identifiers, CommonConstants.DELETED);
			Map<String, Object> params = new HashMap<>();
	        params.put("identifiers", identifiers);
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));qry.append(" IN ");
			qry.append(" (:identifiers) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){
			logger.error("Error when Delete Roles.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteRoles END <== ");
	}

	/**
	 * Method  for delete a role description
	 * @param commonError
	 * @param id
	 */
	public void deleteRoleDesc(CommonError commonError, Integer id) {
		logger.info(" ==> deleteRoles desc START <== ");
		if( id == null){
			logger.error("Can't delete roles desc");
			return;
		}
		try{
			StringBuffer qry = new StringBuffer();
			qry.append(" DELETE FROM ");qry.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);
			qry.append(" WHERE ");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append("=? ");
			getJdbcTemplate().update(qry.toString(),id);
		}catch( Exception e ){
			logger.error("Error when Delete Roles desc.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> deleteRoles desc END <== ");

	}
	
	/**
	 * Method  for get role description list
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleDescDetails(CommonError commonError, String roleId) {
		logger.info(" ==> getRoleDescDetails Details START <== ");
		if( CommonUtils.getValidString(roleId).isEmpty()){
			logger.error("Can't fetch roles desc details");
			return null;
		}
		List<Role> rolesDescDetail = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(" RD.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ID"));qry.append(" , ");
			qry.append(" R.");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));qry.append(" , ");
			qry.append(" R.");qry.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));qry.append(" , ");
			qry.append(" MT.");qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));qry.append(" , ");
			qry.append(" (select ");qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));qry.append(" FROM ");
			qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qry.append(" = ");qry.append(" MT.");qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(") AS PARENT_PAGE_DESC");qry.append(" , ");
			qry.append(" P.");qry.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_NAME"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_TABLE);qry.append(" AS R ");
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);qry.append(" AS RD ");
			qry.append(" ON ");qry.append(" RD.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append(" = ");
			qry.append(" R.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" AS MT ");
			qry.append(" ON ");qry.append(" RD.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));qry.append(" = ");
			qry.append(" MT.");qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qry.append(" INNER JOIN ");qry.append(AdminDBConstants.TBL_PERMISSION_TABLE);qry.append(" AS P ");
			qry.append(" ON ");qry.append(" RD.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("PERMISSION_ID"));qry.append(" = ");
			qry.append(" P.");qry.append(AdminDBConstants.PERMISSION_TABLE.get("PERMISSION_ID"));
			qry.append(" WHERE ");qry.append(" R.");qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append(" = (?) ");
			rolesDescDetail = getJdbcTemplate().query(qry.toString(), new Object[]{roleId}, new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.error("Error when Select RoleDecs Details.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == rolesDescDetail){
			logger.error("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getRoleDescDetails Details END <== ");
		return rolesDescDetail;
	}
	
	/**
	 * Method  for get selected menu list
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<MetaTable> getSelectedMenu(CommonError commonError, String roleId) {
		logger.info(" ==> getSelectedMenu START <== ");
		if (CommonUtils.getValidString(roleId).isEmpty()) {
			logger.error("Can't fetch selected sub menu");
			return null;
		}
		List<MetaTable> menuList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" , ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" IN (SELECT ");
			qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" IN (SELECT ");
			qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("SUBMENU_ID"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_DESC_TABLE);qry.append(" WHERE ");
			qry.append(AdminDBConstants.ROLE_DESC_TABLE.get("ROLE_ID"));qry.append("=? ))");
			menuList = (List<MetaTable>)getJdbcTemplate().query(qry.toString(), new Object[]{roleId}, new BeanPropertyRowMapper<MetaTable>(MetaTable.class));
		} catch( Exception e ){
			logger.error("Error when retrieve metaTable List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == menuList){
			logger.error("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getMetaTableList END <== ");
		return menuList;
	}
	
	/**
	 * Method  for get sub menu list
	 * @param commonError
	 * @param mainModules
	 * @return
	 */
	public List<Role> getSubMenus(CommonError commonError, List<MetaTable> mainModules) {
		logger.info(" ==> getSubModule START <== ");
		if( mainModules == null || mainModules.isEmpty() ){
			logger.error(" User doesn't select any main modules ");
			return null;
		}
		List<Integer> mainModuleList = new ArrayList<>();
		for (MetaTable mainModule : mainModules){
			mainModuleList.add(mainModule.getPageId());
		}
		List<Role> subMenuList = null;
		int size = mainModules.size();
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));
			qry.append(" , ");
			qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("PAGE_DESC"));
			qry.append(" , ");
			qry.append(" (");
			qry.append(" SELECT ");qry.append(" B.PAGE_DESC");
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" AS B ");
			qry.append(" WHERE ");qry.append(" B.");qry.append(AdminDBConstants.META_TABLE.get("PAGE_ID"));qry.append(" = ");
			qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));
			qry.append(" )");qry.append(" AS PARENT_PAGE_DESC");
			qry.append(" , ");
			qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("URL"));
			qry.append(" , ");
			qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("ICON"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_META_TABLE);qry.append(" AS A ");
			qry.append(" WHERE ");qry.append(" A.");qry.append(AdminDBConstants.META_TABLE.get("PARENT_PAGE"));qry.append(" IN ( ");
			for ( int i=0; i<size; i++ ) {
				qry.append(" ? ");qry.append(",");
			}
			qry.setLength(qry.length() - 1);
			qry.append(") ");
			subMenuList = (List<Role>)getJdbcTemplate().query(qry.toString(), mainModuleList.toArray(),  new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.error("Error when retrieve metaTable List.  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == subMenuList){
			logger.error("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getSubModule END <== ");
		return subMenuList;
	}
	
	
	/**
	 * This method will activate or deactivate the role
	 * @param commonError
	 * @param loginUser
	 * @param identifiers
	 * @param status
	 */
	public void updateActiveStatus( CommonError commonError, User loginUser, List<String> identifiers, String status ){
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("Role Id ==> " + identifiers);
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			if(status.equals(CommonConstants.Y)){
				insertRolesHistory( commonError, loginUser,identifiers, CommonConstants.UNLOCKED);
			}else{
				insertRolesHistory( commonError, loginUser,identifiers, CommonConstants.LOCKED);
			}
			Map<String, Object> params = new HashMap<>();
	        params.put("identifiers", identifiers);
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( AdminDBConstants.TBL_ROLE_TABLE );qry.append(" SET ");
			qry.append( AdminDBConstants.ROLE_TABLE.get("ACTIVE") );qry.append( "= ");
			qry.append("'"+status+"'");
			qry.append(" WHERE ");
			qry.append( AdminDBConstants.ROLE_TABLE.get("IDENTIFIER") );qry.append(" IN ");
			qry.append(" (:identifiers) ");
			db.update(qry.toString(),params);
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Role.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");
	}
	
	
	/**
	 * This method inserts updated Role into Role history table
	 * @param courseError
	 * @param loginUser
	 * @param idList
	 * @param action
	 */
	public void insertRolesHistory(CommonError courseError, User loginUser,List<String> idList, String action) throws Exception {
		
		logger.info(" ==> Insert Role History START <== ");
		logger.info("Role Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : AdminDBConstants.ROLE_TABLE.keySet() ){
			if(!column.equals("CORE_ORB_ROLES")){
			newQryHist.append( AdminDBConstants.ROLE_TABLE.get(column) );newQryHist.append( "," );
			}
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(AdminDBConstants.TBL_ROLE_TABLE);newQryHist.append(" WHERE ");newQryHist.append( AdminDBConstants.ROLE_TABLE.get("IDENTIFIER") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Role> roleList = (List<Role>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Role>(Role.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( AdminDBConstants.TBL_ROLE_HIST_TABLE);newQry.append("( ");
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("ROLE_NAME") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("CREATE_BY") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( AdminDBConstants.ROLE_HIST_TABLE.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, roleList.get(i).getIdentifier());ps.setString(2, roleList.get(i).getRoleName());
					ps.setString(3, loginUser.getUserId());
					ps.setTimestamp(4, date);ps.setString(5, loginUser.getUserId());
					ps.setTimestamp(6, date);ps.setString(7, action);
					
				}		
				public int getBatchSize() {
					return roleList.size();
				}
			});

		}catch( Exception e ){
			logger.info("Error when Insert RoleHistory.  " + e.getMessage());
			courseError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Role History END <== ");
	}
	
	/**
	 * This method gets  Role from Role table
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public Role getRole(CommonError commonError, String roleId) {
		logger.info(" ==> Get a Role START <== ");
		logger.info("Role Id ==> " + roleId );
		Role role = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("CREATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("CREATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("ACTIVE"));
			query.append(" FROM ");query.append(AdminDBConstants.TBL_ROLE_TABLE);query.append(" WHERE ");
			query.append( AdminDBConstants.ROLE_TABLE.get("IDENTIFIER") );query.append( " = ? " );
			role = (Role)getJdbcTemplate().queryForObject( query.toString(),new String[]{roleId}, new BeanPropertyRowMapper(Role.class) );
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Role. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
	
		logger.info(" ==> Get a Role END <== ");
		return role;
	}
	
	/**
	 * This method gets RoleList from Role table
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleList( CommonError commonError, String roleId ) {
		logger.info(" ==> getRoleList START <== ");
		List<Role> roleList = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(AdminDBConstants.ROLE_TABLE.get("ROLE_ID"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("ROLE_NAME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("CREATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("CREATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("UPDATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_TABLE.get("ACTIVE"));
			query.append(" FROM ");query.append(AdminDBConstants.TBL_ROLE_TABLE);query.append(" WHERE ");
			query.append(AdminDBConstants.ROLE_TABLE.get("IDENTIFIER"));query.append(" = ? ");
			roleList = getJdbcTemplate().query( query.toString(),new String[]{roleId}, new BeanPropertyRowMapper(Role.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch interruption list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getRoleList END <== ");
		return roleList;
	}

	/**
	 * This method gets  Activity mapping list from Role table
	 * @param commonError
	 * @return
	 */
	public List<MetaTable> getActivityMappingList(CommonError commonError) {
		 logger.info(" ==> getActivityMappingList START <== ");
		  List<MetaTable>  activityMappingList = null;
		  try{
		       StringBuffer qry = new StringBuffer("SELECT ");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ID") ); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("PAGE_ID")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ACTIVITY")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("CREATE_BY")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("CREATE_TIME")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("UPDATE_BY")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("UPDATE_TIME")); qry.append(",");
		       qry.append(AdminDBConstants.MOD_ACTIVITY_MAPPING.get("ACTIVE"));
		       qry.append(" FROM ");qry.append(AdminDBConstants.TBL_MOD_ACTIVITY_MAPPING);
			   
			   activityMappingList = getJdbcTemplate().query( qry.toString(), new BeanPropertyRowMapper(MetaTable.class) );
		  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch metaTable list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		  }
		    logger.info(" ==> getActivityMappingList END <== ");
		    return activityMappingList;
	}
	
	
	/**
	 * Method  for get role mapping list
	 * @param commonError
	 * @param identifier
	 * @return
	 */
	public List<Role> getRoleMapping(CommonError commonError, String identifier) {
		logger.info(" ==> select RoleMapping START <== ");
		if( CommonUtils.getValidString(identifier).isEmpty() ){
			logger.error(" User doesn't select any role identifier ");
			return null;
		}
		List<Role> roles = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append("RM.");qry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("PAGE_ID"));qry.append(",");
			qry.append("RM.");qry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER"));qry.append(",");
			qry.append("RM.");qry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("ID"));qry.append(",");
			qry.append("RA.");qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVITY_IDENTIFIER"));qry.append(",");
			qry.append("RA.");qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ID"));qry.append(",");
			qry.append("RA.");qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));
			qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_MOD_MAPPING_TABLE);qry.append(" AS RM");
			qry.append(" LEFT OUTER JOIN ");qry.append(AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE);qry.append(" AS RA");
			qry.append(" ON ");qry.append("RM.");qry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("ID"));qry.append(" = ");
			qry.append("RA.");qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ID"));qry.append(" WHERE "); 
			qry.append("RM.");qry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER"));qry.append(" = (?) ");
			roles = getJdbcTemplate().query(qry.toString(), new Object[]{identifier}, new BeanPropertyRowMapper<Role>(Role.class));
		}catch( Exception e ){
			logger.error("Error when Select RoleDecs .  " + e.getMessage());
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		if (null == roles){
			logger.info("Query returns Null");
			if(commonError != null)
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> select RoleDesc END <== ");
		return roles;
	}
	
	/**
	 * Method  for get role Activity mapping list
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleAcvtMapList( CommonError commonError, String roleId ) {
		logger.info(" ==> getRoleAcvtMapList START <== ");
		List<Role> roleAcvtMapList = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ID"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVITY_IDENTIFIER"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("CREATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("CREATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("UPDATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("UPDATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVE"));
			query.append(" FROM ");query.append(AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE);query.append(" WHERE ");
			query.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));query.append(" = ? ");
			roleAcvtMapList = getJdbcTemplate().query( query.toString(),new String[]{roleId}, new BeanPropertyRowMapper(Role.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch role activity map list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR); 
		}
		logger.info(" ==> getRoleAcvtMapList END <== ");
		return roleAcvtMapList;
	}
	
	/**
	 * Method  for get role module mapping list
	 * @param commonError
	 * @param roleId
	 * @return
	 */
	public List<Role> getRoleModMapList( CommonError commonError, String roleId ) {
		logger.info(" ==> getRoleModMapList START <== ");
		List<Role> roleModMapList = null;
		try{
			StringBuffer query = new StringBuffer("SELECT ");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("ID"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("PAGE_ID"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("CREATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("CREATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("UPDATE_BY"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("UPDATE_TIME"));query.append(",");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("ACTIVE"));
			query.append(" FROM ");query.append(AdminDBConstants.TBL_ROLE_MOD_MAPPING_TABLE);query.append(" WHERE ");
			query.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER"));query.append(" = ? ");
			roleModMapList = getJdbcTemplate().query( query.toString(),new String[]{roleId}, new BeanPropertyRowMapper(Role.class) );
			
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch role module map list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getRoleModMapList END <== ");
		return roleModMapList;
	}
	
	
	
	/**
	 * Method  for get role Activity mapping list for particular module
	 * @param commonError
	 * @param role
	 * @return
	 */
	public List<String> getRoleActivityMappingList(CommonError commonError, Role role){
		 logger.info(" ==> Get Role Activity Mapping START <== ");
		 List<String> moduleActivityIds = new ArrayList<>();
		 try{
			  StringBuffer qry = new StringBuffer("SELECT ");
			  qry.append( AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVITY_IDENTIFIER"));
			  qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE);qry.append(" WHERE ");
			  qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));qry.append(" =? ") ;
			  qry.append(" AND ") ;qry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("PAGE_ID"));
			  qry.append(" =? ") ;
			  moduleActivityIds = getJdbcTemplate().queryForList( qry.toString() ,new Object[]{role.getIdentifier(),role.getPageId()}, String.class );
						
		 }catch( Exception e ){
				logger.error(" Exception occured when tries to get mapping from Role Activity Mapping table. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get Role Activity Mapping END <== ");
		return moduleActivityIds;
	}
	
	/**
	 * Method  for get role Activity mapping list for particular module
	 * @param commonError
	 * @param role
	 * @return
	 */
	public List<MetaTable> getRoleActivityMapList(CommonError commonError){
		 logger.info(" ==> Get Role Activity Map START <== ");
		 List<MetaTable> moduleActivityIds = new ArrayList<>();
		 try{
			  StringBuffer qry = new StringBuffer("SELECT ");
			  qry.append( AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER"));qry.append(",");
			  qry.append( AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("PAGE_ID"));qry.append(",");
			  qry.append( AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("ACTIVITY_IDENTIFIER"));
			  qry.append(" FROM ");qry.append(AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE);
			  moduleActivityIds = getJdbcTemplate().query( qry.toString(),new String[]{}, new BeanPropertyRowMapper(MetaTable.class) );
		 }catch( Exception e ){
				logger.error(" Exception occured when tries to get mapping from Role Activity Map table. Exception ==> " + e.getMessage() );
				commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get Role Activity Map END <== ");
		return moduleActivityIds;
	}
	
	
	/**
	 * This method delete the role module mapping data into DB
	 * @param commonError
	 * @param loginUser
	 * @param identifiers 
	 */
	public void deleteRoleModMapping(CommonError commonError, User loginUser, List<String> identifiers) {
		logger.info(" ==> Delete Role Module Mapping START <== ");
		logger.info("Role Identifier ==> " + identifiers );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			Map<String, Object> params = new HashMap<>();
	        params.put("identifiers", identifiers);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(AdminDBConstants.TBL_ROLE_MOD_MAPPING_TABLE);newQry.append(" WHERE ");
			newQry.append(AdminDBConstants.ROLE_MOD_MAPPING_TABLE.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:identifiers) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the role module mapping. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Role Module Mapping END <== ");
	}
	
	/**
	 * This method delete the role module mapping data into DB
	 * @param commonError
	 * @param loginUser
	 * @param identifiers
	 */
	public void deleteRoleActivityMapping(CommonError commonError, User loginUser, List<String> identifiers) {
		logger.info(" ==> Delete Role Activity Mapping START <== ");
		logger.info("Role Identifier ==> " + identifiers );
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			Map<String, Object> params = new HashMap<>();
	        params.put("identifiers", identifiers);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(AdminDBConstants.TBL_ROLE_ACTIVITY_MAPPING_TABLE);newQry.append(" WHERE ");
			newQry.append(AdminDBConstants.ROLE_ACTIVITY_MAPPING_TABLE.get("IDENTIFIER") );newQry.append(" IN ");
			newQry.append(" (:identifiers) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to delete the role activity mapping. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Role Module Mapping END <== ");
	} 

}

