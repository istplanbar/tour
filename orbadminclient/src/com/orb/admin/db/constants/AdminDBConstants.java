package com.orb.admin.db.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

@SuppressWarnings("unchecked")
public class AdminDBConstants {
	public static final String TBL_ADMIN_CONF;
	public static final Map<String, String> ADMIN_CONF;
	
	public static final String TBL_META_TABLE;
	public static final Map<String, String> META_TABLE;
	
	public static final String TBL_META_TABLE_HISTORY;
	public static final Map<String, String> META_TABLE_HISTORY;
	
	public static final String TBL_PERMISSION_TABLE;
	public static final Map<String, String> PERMISSION_TABLE;
	
	public static final String TBL_LAYOUT_TABLE;
	public static final Map<String, String> LAYOUT_TABLE;
		
	public static final String TBL_LAYOUT_DESC_TABLE;
	public static final Map<String, String> LAYOUT_DESC_TABLE;
	
	public static final String TBL_ROLE_TABLE;
	public static final Map<String, String> ROLE_TABLE;
	
	public static final String TBL_ROLE_HIST_TABLE;
	public static final Map<String, String> ROLE_HIST_TABLE;
	
	public static final String TBL_ROLE_MOD_MAPPING_TABLE;
	public static final Map<String, String> ROLE_MOD_MAPPING_TABLE;
	
	public static final String TBL_ROLE_ACTIVITY_MAPPING_TABLE;
	public static final Map<String, String> ROLE_ACTIVITY_MAPPING_TABLE;
	
	public static final String TBL_ROLE_DESC_TABLE;
	public static final Map<String, String> ROLE_DESC_TABLE;
	
	public static final String TBL_ROLE_HISTORY_TABLE;
	public static final Map<String, String> ROLE_HISTORY_TABLE;
	
	public static final String TBL_MODULES_INFO;
	public static final Map<String, String> MODULES_INFO;
	
	public static final String TBL_USER_ROLE;
	public static final Map<String, String> USER_ROLE;
	
	public static final String TBL_LAYOUT_HISTORY_TABLE;
	public static final Map<String, String> LAYOUT_HISTORY_TABLE;
	
	public static final String TBL_SECOND_LEVEL_AUTH;
	public static final Map<String, String> SECOND_LEVEL_AUTH;
	
	public static final String TBL_MOD_ACTIVITY_MAPPING;
	public static final Map<String, String> MOD_ACTIVITY_MAPPING;
	
	public static final String TBL_MAIL_TEMPLATES;
	public static final Map<String, String> MAIL_TEMPLATES;
	
	public static final String TBL_MAIL_TEMPLATES_HISTORY;
	public static final Map<String, String> MAIL_TEMPLATES_HISTORY;
	
	public static final Map<String,String> roleDBConfig;
	
	public static final Map<String,String> mailTemplatesDBConfig;
	
	static{

		TBL_ADMIN_CONF = CommonUtils.getPropertyValue("ADMIN_CONF", "CORE_ORB_ADMIN_CONF");
		ADMIN_CONF  = (Map<String,String>)CommonUtils.getBean("ADMIN_CONF");
		
		TBL_META_TABLE = CommonUtils.getPropertyValue("META_TABLE", "CORE_ORB_META_TABLE");
		META_TABLE  = (Map<String,String>)CommonUtils.getBean("META_TABLE");
		
		TBL_META_TABLE_HISTORY = CommonUtils.getPropertyValue("META_TABLE_HISTORY", "CORE_ORB_META_TABLE_HISTORY");
		META_TABLE_HISTORY  = (Map<String,String>)CommonUtils.getBean("META_TABLE_HISTORY");
		
		TBL_PERMISSION_TABLE = CommonUtils.getPropertyValue("PERMISSION_TABLE", "CORE_ORB_PERMISSIONS");
		PERMISSION_TABLE  = (Map<String,String>)CommonUtils.getBean("PERMISSION_TABLE");
		
		TBL_LAYOUT_TABLE = CommonUtils.getPropertyValue("LAYOUT_TABLE", "CORE_ORB_LAYOUT");
		LAYOUT_TABLE  = (Map<String,String>)CommonUtils.getBean("LAYOUT_TABLE");
		
		TBL_ROLE_TABLE = CommonUtils.getPropertyValue("ROLE_TABLE", "CORE_ORB_ROLES");
		ROLE_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_TABLE");
		
		TBL_ROLE_HIST_TABLE = CommonUtils.getPropertyValue("ROLE_HIST_TABLE", "CORE_ORB_ROLES_HISTORY");
		ROLE_HIST_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_HIST_TABLE");
		
		TBL_ROLE_MOD_MAPPING_TABLE = CommonUtils.getPropertyValue("ROLE_MOD_MAPPING_TABLE", "CORE_ORB_ROLE_MOD_MAPPING");
		ROLE_MOD_MAPPING_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_MOD_MAPPING_TABLE");
		
		TBL_ROLE_ACTIVITY_MAPPING_TABLE = CommonUtils.getPropertyValue("ROLE_ACTIVITY_MAPPING_TABLE", "CORE_ORB_ROLE_ACTIVITY_MAPPING");
		ROLE_ACTIVITY_MAPPING_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_ACTIVITY_MAPPING_TABLE");
		
		TBL_ROLE_DESC_TABLE = CommonUtils.getPropertyValue("ROLE_DESC_TABLE", "CORE_ORB_ROLE_DESC");
		ROLE_DESC_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_DESC_TABLE");
		
		TBL_ROLE_HISTORY_TABLE = CommonUtils.getPropertyValue("ROLE_HISTORY_TABLE", "CORE_ORB_ROLE_HISTORY");
		ROLE_HISTORY_TABLE  = (Map<String,String>)CommonUtils.getBean("ROLE_HISTORY_TABLE");

		TBL_LAYOUT_HISTORY_TABLE = CommonUtils.getPropertyValue("LAYOUT_HISTORY_TABLE", "CORE_ORB_LAYOUT_HISTORY");
		LAYOUT_HISTORY_TABLE  = (Map<String,String>)CommonUtils.getBean("LAYOUT_HISTORY_TABLE");
		
		TBL_LAYOUT_DESC_TABLE = CommonUtils.getPropertyValue("LAYOUT_DESC_TABLE", "CORE_ORB_LAYOUT_DESC");
		LAYOUT_DESC_TABLE  = (Map<String,String>)CommonUtils.getBean("LAYOUT_DESC_TABLE");
		
		TBL_MODULES_INFO = CommonUtils.getPropertyValue("MODULES_INFO", "CORE_ORB_MODULES_INFO");
		MODULES_INFO  = (Map<String,String>)CommonUtils.getBean("MODULES_INFO");
		
		TBL_MOD_ACTIVITY_MAPPING = CommonUtils.getPropertyValue("MODULE_ACTIVITY_MAPPING", "CORE_ORB_MOD_ACTIVITY_MAPPING");
		MOD_ACTIVITY_MAPPING  = (Map<String,String>)CommonUtils.getBean("MODULE_ACTIVITY_MAPPING");
		
		TBL_USER_ROLE = CommonUtils.getPropertyValue("USER_ROLE", "CORE_ORB_USER_ROLE");
		USER_ROLE  = (Map<String,String>)CommonUtils.getBean("USER_ROLE");
		
		TBL_SECOND_LEVEL_AUTH = CommonUtils.getPropertyValue("SECOND_LEVEL_AUTH", "CORE_ORB_SECOND_LEVEL_AUTH");
		SECOND_LEVEL_AUTH  = (Map<String,String>)CommonUtils.getBean("SECOND_LEVEL_AUTH");
		
		roleDBConfig=(Map<String,String>)CommonUtils.getBean("roleDBConfig");
	    
        mailTemplatesDBConfig=(Map<String,String>)CommonUtils.getBean("mailTemplatesDBConfig");
		
		TBL_MAIL_TEMPLATES = CommonUtils.getPropertyValue("MAIL_TEMPLATES", "CORE_ORB_MAIL_TEMPLATES");
		MAIL_TEMPLATES  = (Map<String,String>)CommonUtils.getBean("MAIL_TEMPLATES");
		
		TBL_MAIL_TEMPLATES_HISTORY = CommonUtils.getPropertyValue("MAIL_TEMPLATES_HISTORY", "CORE_ORB_MAIL_TEMPLATES_HISTORY");
		MAIL_TEMPLATES_HISTORY  = (Map<String,String>)CommonUtils.getBean("MAIL_TEMPLATES_HISTORY");
		
	}
}