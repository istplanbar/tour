package com.orb.common.loginaudittrail.dao;

import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonDBConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.loginaudittrail.model.LoginAuditTrail;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class LoginAuditTrailDAO extends AbstractDAO {

   private static Log logger = LogFactory.getLog(LoginAuditTrailDAO.class);
   
   public List<LoginAuditTrail> getLoginAuditTrailList(CommonError error) {
	logger.info(" ==> getLoginAuditTrailList START <== ");
	List<LoginAuditTrail> loginAuditTrailList = null;
	try{
		StringBuffer qry = new StringBuffer("SELECT ");
		qry.append( CommonDBConstants.LOGIN_AUDITTRAIL.get("USER_ID") );qry.append( "," );
		qry.append( CommonDBConstants.LOGIN_AUDITTRAIL.get("ACTION") );qry.append( "," );
		qry.append( CommonDBConstants.LOGIN_AUDITTRAIL.get("TIME_STAMP") );qry.append( "," );
		qry.append( CommonDBConstants.LOGIN_AUDITTRAIL.get("IP_ADDRESS") );
		
		qry.append(" FROM ");qry.append(CommonDBConstants.TBL_LOGIN_AUDITTRAIL);qry.append(" ORDER BY  ");
		qry.append( CommonDBConstants.LOGIN_AUDITTRAIL.get("TIME_STAMP") );qry.append(" DESC ");
		loginAuditTrailList = getJdbcTemplate().query( qry.toString(),new String[]{},new BeanPropertyRowMapper<LoginAuditTrail>(LoginAuditTrail.class) );
	}
	catch( Exception e ){
		logger.info("Error when Insert user audit trail info.  " + e.getMessage());
	}
	return loginAuditTrailList;

}
}
