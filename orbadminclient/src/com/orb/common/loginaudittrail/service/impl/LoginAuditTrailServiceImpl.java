package com.orb.common.loginaudittrail.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.orb.common.client.error.CommonError;
import com.orb.common.loginaudittrail.dao.LoginAuditTrailDAO;
import com.orb.common.loginaudittrail.model.LoginAuditTrail;
import com.orb.common.loginaudittrail.service.LoginAuditTrailService;

@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class LoginAuditTrailServiceImpl implements LoginAuditTrailService{
	
	@Autowired
	LoginAuditTrailDAO loginAuditTrailDAO;
	
	public List<LoginAuditTrail> getLoginAuditTrailList(CommonError commonError ){
		return loginAuditTrailDAO.getLoginAuditTrailList(commonError);
	}
}
