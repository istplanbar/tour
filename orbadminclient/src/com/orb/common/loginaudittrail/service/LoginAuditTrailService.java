package com.orb.common.loginaudittrail.service;

import java.util.List;

import com.orb.common.client.error.CommonError;
import com.orb.common.loginaudittrail.model.LoginAuditTrail;

public interface LoginAuditTrailService {
	public List<LoginAuditTrail> getLoginAuditTrailList(CommonError error);
}
