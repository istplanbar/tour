package com.orb.taskplanner.service;

import java.util.List;
import java.util.Map;

import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
//import com.orb.common.client.error.CommonError;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.bean.Disposals;


public interface TaskPlannerService {
	
	/**
	 * Service methods for TASK Main Module
	 * @param commonError
	 * @return
	 */
	public List<Task> getActiveTaskList( CommonError commonError );
	public List<Task> getTaskStatusHistoryList( CommonError commonError );
	public List<Task> getUserTaskList( CommonError commonError, User loginUser );
	public List<Task> getInactiveTaskList( CommonError commonError );
	public void addTask( CommonError commonError, User loginUser, Task task );
	public void updateTask( CommonError commonError, User loginUser, Task task );
	public Task getTask( CommonError commonError, String taskId );
	public void lockTask( CommonError commonError, User loginUser, List<Task> taskList );
	public void unlockTask( CommonError commonError, User loginUser, List<Task> taskList );
	public void deleteTask( CommonError commonError, User loginUser, Task task );
	public String getExtension( String filename);
	public String genNextTaskId( CommonError commonError, String projectId );
	public String getNextDir( String id );
	public void addTaskToWatchList( CommonError commonError, String taskId, User loginUser  );
	public void removeTaskFromWatchList( CommonError commonError, String taskId, User loginUser );
	public List<Task> getTaskWatchList( CommonError commonError );
	public String getSubTaskId( CommonError commonError, String taskId);
	
	
	/**
	 * Service methods for Project Sub-Module
	 * @param commonError
	 * @return
	 */
	public List<Project> getActiveProjectList(CommonError commonError);
	public List<Project> getInactiveProjectList(CommonError commonError);
	public Project getProject(CommonError commonError, String projId);
	public void addProject(CommonError commonError, User loginUser, Project project);
	public void updateProject(CommonError commonError, User loginUser, Project project);
	public void deleteProject(CommonError commonError, User loginUser, List<Project> ProjectList);
	public void lockProject(CommonError commonError, User loginUser, List<Project> ProjectList);
	public void unlockProject(CommonError commonError, User loginUser, List<Project> ProjectList);
	public void lockViewedProject(CommonError commonError, User loginUser, Project project);
	public void unlockViewedProject(CommonError commonError, User loginUser, Project project);
	public void deleteViewedProject(CommonError commonError, User loginUser, Project project);
	
	/**
	 * Service methods for Authorization Sub-Module
	 * @param commonError
	 * @return
	 */
	public List<Authorization> getActiveAuthorizationList(CommonError commonError);
	public List<Authorization> getInactiveAuthorizationList(CommonError commonError);
	public void addAuthorization(CommonError commonError, User loginUser, Authorization authorization);
	public void updateAuthorization(CommonError commonError, User loginUser, Authorization authorization);
	public Authorization getAuthorization(CommonError commonError, String authId);
	public void lockAuthorization(CommonError commonError, User loginUser, List<Authorization> authorizationList);
	public void unlockAuthorization(CommonError commonError, User loginUser, List<Authorization> authorizationList);
	public void deleteAuthorization(CommonError commonError, User loginUser, List<Authorization> authorizationList);
	public void lockViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization);
	public void unlockViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization);
	public void deleteViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization);
	
	/**
	 * Service methods for Category Sub-Module
	 * @param commonError
	 * @return
	 */
	public List<TaskCategory> getActiveCategoryList(CommonError commonError);
	public List<TaskCategory> getInactiveCategoryList(CommonError commonError);
	public TaskCategory getCatName(CommonError commonError, String catId);
	public void add(CommonError commonError, User loginUser, TaskCategory categoryName);
	public void update(CommonError commonError, User loginUser, TaskCategory categoryName);
	public void deleteCategory(CommonError commonError, User loginUser, List<TaskCategory> categoryList);
	public void lockCategory(CommonError commonError, User loginUser, List<TaskCategory> categoryList);
	public void unlockCategory(CommonError commonError, User loginUser, List<TaskCategory> categoryList);
	public void lockViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName );
	public void unlockViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName);
	public void deleteViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName);
	
	/**
	 * Service methods for type Sub-Module
	 * @param commonError
	 * @return
	 */
	public List<TaskType> getActiveTypeList(CommonError commonError);
	public List<TaskType> getInactiveTypeList(CommonError commonError);
	public TaskType getTaskType(CommonError commonError, String catId);
	public void addTaskType(CommonError commonError, User loginUser, TaskType type);
	public void updateTaskType(CommonError commonError, User loginUser, TaskType type);
	public void deleteTaskType(CommonError commonError, User loginUser, List<TaskType> typeList);
	public void lockTaskType(CommonError commonError, User loginUser, List<TaskType> typeList);
	public void unlockTaskType(CommonError commonError, User loginUser, List<TaskType> typeList);
	public void lockViewedTaskType(CommonError commonError, User loginUser, TaskType type );
	public void unlockViewedTaskType(CommonError commonError, User loginUser, TaskType type);
	public void deleteViewedTaskType(CommonError commonError, User loginUser, TaskType type);
	
	/**
	 * Service methods for Division Sub-Module
	 * @param commonError
	 * @return
	 */
	public List<Division> getActiveDivisionList(CommonError commonError);
	public List<Division> getInactiveDivisionList(CommonError commonError);
	public Division getDivision(CommonError commonError, String deptId);
	public void addDivision(CommonError commonError, User loginUser, Division DivisionName);
	public void updateDivision(CommonError commonError, User loginUser, Division DivisionName);
	public void deleteDivision(CommonError commonError, User loginUser, List<Division> DivisionList);
	public void lockDivision(CommonError commonError, User loginUser, List<Division> DivisionList);
	public void unlockDivision(CommonError commonError, User loginUser, List<Division> DivisionList);
	public void lockViewedDivision(CommonError commonError, User loginUser, Division DivisionName);
	public void unlockViewedDivision(CommonError commonError, User loginUser, Division DivisionName);
	public void deleteViewedDivision(CommonError commonError, User loginUser, Division DivisionName);
	public void cloneTask(CommonError commonError, User loggedUser, Task task);
	
	
	//Facilities
	public List<Disposals> getActiveDisposalsList(CommonError commonError);
	public List<Disposals> getInactiveDisposalsList(CommonError commonError);
	public Disposals getDisposals(CommonError commonError, String disposalsId);
	public void lockDisposals(CommonError commonError, User loginUser,List<Disposals> selectedDisposalsList);
	public void unlockDisposals(CommonError commonError, User loginUser,List<Disposals> selectedDisposalsList);
	public void deleteDisposals(CommonError commonError, User loginUser,List<Disposals> selectedDisposalsList);
	public void deleteViewedDisposals(CommonError commonError, User loginUser, Disposals disposals);
	public void lockViewedDisposals(CommonError commonError, User loginUser, Disposals disposals);
	public void unlockViewedDisposals(CommonError commonError, User loginUser, Disposals disposals);
	public void add(CommonError commonError, User loginUser, Disposals disposals);
	public void update(CommonError commonError, User loginUser, Disposals disposals);
	public void updateTaskStatusList(List<String> asList);
	public void addTaskFacilities(CommonError commonError, User loginUser, Map<Integer, Task> hmapFacilities);
	
	
			
}
