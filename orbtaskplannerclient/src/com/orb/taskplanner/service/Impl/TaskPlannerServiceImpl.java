package com.orb.taskplanner.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.bean.Disposals;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.dao.AuthorizationDAO;
import com.orb.taskplanner.dao.DisposalsDAO;
import com.orb.taskplanner.dao.DivisionsDAO;
import com.orb.taskplanner.dao.ProjectDAO;
import com.orb.taskplanner.dao.TaskCategoryDAO;
import com.orb.taskplanner.dao.TaskDAO;
import com.orb.taskplanner.dao.TaskTypeDAO;
import com.orb.taskplanner.service.TaskPlannerService;		
@Service
@Scope( BeanDefinition.SCOPE_PROTOTYPE )
public class TaskPlannerServiceImpl implements TaskPlannerService{
	
	private static Log logger = LogFactory.getLog(TaskPlannerServiceImpl.class);
	
	@Autowired
	TaskDAO taskDAO;
	@Autowired
	AuthorizationDAO authorizationDAO;
	@Autowired
	TaskCategoryDAO taskCategoryDAO;
	@Autowired
	DivisionsDAO divisionsDAO;
	@Autowired
	ProjectDAO projectDAO;
	@Autowired
	TaskTypeDAO taskTypeDAO;
	@Autowired
	DisposalsDAO disposalsDAO;
	

	//Document Service	 

	/**
	 * This method calls DAO to get all active tasks from DB
	 */
	public List<Task> getActiveTaskList(CommonError commonError) {
		List<Task> taskList=taskDAO.getTaskList(commonError, CommonConstants.Y, null);
		List<Task> subTaskList=taskDAO.getActiveTaskList(commonError,CommonConstants.Y);
		taskList.forEach(w->{
			w.setSubTasks(new ArrayList<>());
			subTaskList.forEach(s->{
			    if(w.getIdentifier().equals(s.getParentTask())){
				   s.setRelation("Parent of");
				   w.getSubTasks().add(s);
			    }
			});
		});
		return taskList;
		
	}
	
	/**
	 * This method calls DAO to get all active tasks from DB
	 */
	public List<Task> getTaskStatusHistoryList(CommonError commonError) {
		String history = CommonConstants.TRUE;
		return taskDAO.getTaskList(commonError, CommonConstants.Y , history);
	}
	
	/**
	 * This method calls DAO to get all active tasks from DB
	 */
	public List<Task> getTaskWatchList(CommonError commonError) {
		return taskDAO.getWatchList(commonError);
		
	}
	
	public String getSubTaskId(CommonError commonError,String identifier){
		return taskDAO.getSubTaskId(commonError, identifier);
	}

	

	/**
	 * This method calls DAO to get all active tasks from DB
	 */
	public List<Task> getUserTaskList(CommonError commonError, User loginUser) {
		return taskDAO.getUserTaskList(commonError,loginUser, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive tasks level from DB
	 */
	@Override
	public List<Task> getInactiveTaskList(CommonError commonError) {
		return taskDAO.getTaskList(commonError,CommonConstants.N, null);
	}

	/**
	 * This method calls DAO to insert new tasks to DB
	 */
	@Override
	public void addTask(CommonError commonError, User loginUser, Task task) {
		taskDAO.addTask(commonError, loginUser, task);		
	}
	
	/**
	 * This method calls DAO to insert new tasks to DB
	 */
	@Override
	public void addTaskToWatchList(CommonError commonError, String taskId, User loginUser ) {
		taskDAO.addTaskToWatchList(commonError, taskId, loginUser);		
	}
	
	/**
	 * This method calls DAO to insert new tasks to DB
	 */
	@Override
	public void removeTaskFromWatchList(CommonError commonError, String taskId, User loginUser ) {
		taskDAO.removeTaskFromWatchList(commonError, taskId, loginUser);		
	}
	
	@Override
	public void updateTask(CommonError commonError, User loginUser, Task task) {
		taskDAO.updateTask(commonError, loginUser, task);		
		
	}
	/**
	 * This method calls DAO to get existing tasks from DB
	 */
	@Override
	public Task getTask(CommonError commonError, String taskId) {
		Task task=taskDAO.getTask(commonError,taskId);
		List<Task> taskList=taskDAO.getActiveTaskList(commonError,CommonConstants.Y);
		task.setSubTasks(new ArrayList<>());
		if(task.getParentTask()!=null){
			taskList.forEach(s->{
			   if(task.getParentTask().equals(s.getIdentifier())){
				  s.setRelation("Child Of");
				  task.getSubTasks().add(s);
			    }
			 });
			task.setRelation("Parent Of");
			task.getSubTasks().add(task);
		}
		if(taskList!=null){
			taskList.forEach(s->{
				if(task.getIdentifier().equals(s.getParentTask())){
				   s.setRelation("Parent of");
				   task.getSubTasks().add(s);
				}
			});
		 }
	  return task;
	}
	
	
	
	@Override
	public String genNextTaskId( CommonError commonError, String projectId) {
		try{
		Task getTask = taskDAO.getPreviousTask( commonError, projectId );
		String nextTaskId = null;
		if(getTask.getIdentifier().indexOf("-") >= 0 && !("").equals(getTask.getIdentifier())){
			String[] taskId = getTask.getIdentifier().split("-");
			int ver=Integer.parseInt(taskId[1]);
			ver++;
			nextTaskId = String.format(getTask.getProjKey() + "-" + ver);
		}else{
			nextTaskId = String.format(getTask.getProjKey() + "-1");
		}
		return nextTaskId;

		}catch (Exception e) {
			   logger.error(e.getMessage());
		       return "";
		   }
	}

	/**
	 * Generate directory for uploading files
	 */
	public String getNextDir(String rowId){
		logger.info(StringUtils.leftPad("foobar", 10, '*'));
		String dirName = null;
		try{
			if(rowId == null){
				dirName = taskDAO.genNextDirName();
			}else{
				dirName =  String.format("task-dir-"+StringUtils.leftPad(rowId, 5, '0'));
			}
		}catch (Exception e) {
			logger.error(e.getMessage());
		    return "";
		}
		return dirName;
	}
	
	/**
	 * This method calls DAO to de-activate tasks
	 */
	@Override
	public void lockTask(CommonError commonError, User loginUser, List<Task> taskList) {
		if( taskList == null || taskList.isEmpty() ){
			return;
		}
		for( Task task : taskList ){
			if( task.isChecked() )
				taskDAO.updateActiveTask(commonError, loginUser,task, CommonConstants.N );
		}
	}

	/**
	 * This method calls DAO to activate the locked tasks
	 */
	public void unlockTask(CommonError commonError, User loginUser, List<Task> taskList) {
		if( taskList == null || taskList.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Task task : taskList ){
			if( task.isChecked() )
				taskDAO.updateActiveTask(commonError, loginUser,task, CommonConstants.Y );
		}
	}


	/**
	 * This method calls DAO to deletes particular tasks
	 */
	@Override
	public void deleteTask(CommonError commonError, User loginUser, Task task) {
		if( task == null ){
			logger.info("No record is available to delete");
			return;
		}
			taskDAO.deleteTask(commonError, loginUser,task, CommonConstants.N );
	}
	
	public String getExtension(String filename){
		String extension = null;
		int i = filename.lastIndexOf('.');
		if (i >= 0) {
			extension =  filename.substring(i+1);
		}
		return extension;
	}
	
	
	
	@Override
	public void cloneTask(CommonError commonError, User loginUser, Task task ) {
		taskDAO.cloneTask(commonError, loginUser, task );
	}

	//Authorization Service	 

	/**
	 * This method calls DAO to get all active Authorization level from DB
	 */
	public List<Authorization> getActiveAuthorizationList(CommonError commonError) {
		return authorizationDAO.getAuthorizationList(commonError, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive Authorization level from DB
	 */
	@Override
	public List<Authorization> getInactiveAuthorizationList(CommonError commonError) {
		return authorizationDAO.getAuthorizationList(commonError,CommonConstants.N);
	}

	/**
	 * This method calls DAO to insert new Confidentiality level to DB
	 */
	@Override
	public void addAuthorization(CommonError commonError, User loginUser, Authorization authorization) {
		authorizationDAO.addAuthorization(commonError, loginUser, authorization);		
	}

	/**
	 * This method calls DAO to update existing Confidentiality level to DB
	 */
	@Override
	public void updateAuthorization(CommonError commonError, User loginUser, Authorization authorization) {
		authorizationDAO.updateAuthorization(commonError, loginUser, authorization);

	}

	@Override
	public Authorization getAuthorization(CommonError commonError, String confId) {
		return authorizationDAO.getAuthorization(commonError,confId);
	}

	@Override
	public void lockAuthorization(CommonError commonError, User loginUser, List<Authorization> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			return;
		}
		for( Authorization authorization : selectedDocs ){
			authorizationDAO.updateActiveStatus(commonError, loginUser,authorization, CommonConstants.N );
		}


	}

	public void unlockAuthorization(CommonError commonError, User loginUser, List<Authorization> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Authorization authorization : selectedDocs ){
			authorizationDAO.updateActiveStatus(commonError, loginUser,authorization, CommonConstants.Y );
		}
	}

	@Override
	public void deleteAuthorization(CommonError commonError, User loginUser, List<Authorization> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Authorization authorization : selectedDocs ){
			authorizationDAO.deleteAuthorization(commonError, loginUser,authorization, CommonConstants.N );
		}

	}
	
	/**
	 * This method calls DAO to de-activate authorization
	 */
	@Override
	public void lockViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization) {
		authorizationDAO.updateActiveStatus(commonError, loginUser,authorization, CommonConstants.N );
		
	}
	/**
	 * This method calls DAO to activate authorization
	 */
	@Override
	public void unlockViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization) {
		authorizationDAO.updateActiveStatus(commonError, loginUser,authorization, CommonConstants.Y );
		
	}

	/**
	 * This method calls DAO to deletes particular authorization
	 */
	@Override
	public void deleteViewedAuthorization(CommonError commonError, User loginUser, Authorization authorization) {
		authorizationDAO.deleteAuthorization(commonError, loginUser,authorization, CommonConstants.N );
		
	}


	//Category Service	 

	/**
	 * This method calls DAO to get all active categories from DB
	 */
	public List<TaskCategory> getActiveCategoryList(CommonError commonError) {
		return taskCategoryDAO.getCategoryList(commonError, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive categories level from DB
	 */
	@Override
	public List<TaskCategory> getInactiveCategoryList(CommonError commonError) {
		return taskCategoryDAO.getCategoryList(commonError,CommonConstants.N);
	}

	/**
	 * This method calls DAO to insert new category to DB
	 */
	@Override
	public void add(CommonError commonError, User loginUser, TaskCategory categoryName) {
		taskCategoryDAO.addCategory(commonError, loginUser, categoryName);		
	}

	/**
	 * This method calls DAO to update existing category level to DB
	 */
	@Override
	public void update(CommonError commonError, User loginUser, TaskCategory categoryName) {
		taskCategoryDAO.updateCategory(commonError, loginUser, categoryName);

	}

	/**
	 * This method calls DAO to get existing category from DB
	 */
	@Override
	public TaskCategory getCatName(CommonError commonError, String catId) {
		return taskCategoryDAO.getCategoryName(commonError,catId);
	}

	/**
	 * This method calls DAO to de-activate categories
	 */
	@Override
	public void lockCategory(CommonError commonError, User loginUser, List<TaskCategory> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			return;
		}
		for( TaskCategory category : selectedDocs ){
			taskCategoryDAO.updateActiveCat(commonError, loginUser,category, CommonConstants.N );
		}


	}

	/**
	 * This method calls DAO to activate the locked categories
	 */
	public void unlockCategory(CommonError commonError, User loginUser, List<TaskCategory> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( TaskCategory categoryName : selectedDocs ){
			taskCategoryDAO.updateActiveCat(commonError, loginUser,categoryName, CommonConstants.Y );
		}
	}


	/**
	 * This method calls DAO to deletes particular category
	 */
	@Override
	public void deleteCategory(CommonError commonError, User loginUser, List<TaskCategory> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( TaskCategory categoryName : selectedDocs ){
			taskCategoryDAO.deleteCategory(commonError, loginUser,categoryName, CommonConstants.N );
		}
	}
		/**
		 * This method calls DAO to de-activate Category
		 */
		@Override
		public void lockViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName) {
			taskCategoryDAO.updateActiveCat(commonError, loginUser,categoryName, CommonConstants.N );
			
		}
		/**
		 * This method calls DAO to activate Category
		 */
		@Override
		public void unlockViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName) {
			taskCategoryDAO.updateActiveCat(commonError, loginUser,categoryName, CommonConstants.Y );
			
		}

		/**
		 * This method calls DAO to deletes particular Category
		 */
		@Override
		public void deleteViewedCategory(CommonError commonError, User loginUser, TaskCategory categoryName) {
			taskCategoryDAO.deleteCategory(commonError, loginUser,categoryName, CommonConstants.N );
			
		}


	

	//Division Service	 

	/**
	 * This method calls DAO to get all active Divisions from DB
	 */
	public List<Division> getActiveDivisionList(CommonError commonError) {
		return divisionsDAO.getDivisionList(commonError, CommonConstants.Y);
	}

	/**
	 * This method calls DAO to get all Inactive Divisions level from DB
	 */
	@Override
	public List<Division> getInactiveDivisionList(CommonError commonError) {
		return divisionsDAO.getDivisionList(commonError,CommonConstants.N);
	}

	/**
	 * This method calls DAO to insert new Division to DB
	 */
	@Override
	public void addDivision(CommonError commonError, User loginUser, Division DivisionName) {
		divisionsDAO.addDivision(commonError, loginUser, DivisionName);		
	}

	/**
	 * This method calls DAO to update existing Division level to DB
	 */
	@Override
	public void updateDivision(CommonError commonError, User loginUser, Division DivisionName) {
		divisionsDAO.updateDivision(commonError, loginUser, DivisionName);

	}


	/**
	 * This method calls DAO to get existing Division from DB
	 */
	@Override
	public Division getDivision(CommonError commonError, String DivisionId) {
		return divisionsDAO.getDivision(commonError,DivisionId);
	}

	/**
	 * This method calls DAO to de-activate Divisions
	 */
	@Override
	public void lockDivision(CommonError commonError, User loginUser, List<Division> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Division DivisionName : selectedDocs ){
			divisionsDAO.updateActiveDivision(commonError, loginUser,DivisionName, CommonConstants.N );
		}


	}

	/**
	 * This method calls DAO to activate the locked Divisions
	 */
	public void unlockDivision(CommonError commonError, User loginUser, List<Division> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Division DivisionName : selectedDocs ){
			divisionsDAO.updateActiveDivision(commonError, loginUser,DivisionName, CommonConstants.Y );
		}
	}


	/**
	 * This method calls DAO to deletes particular Division
	 */
	@Override
	public void deleteDivision(CommonError commonError, User loginUser, List<Division> selectedDocs) {
		if( selectedDocs == null || selectedDocs.isEmpty() ){
			logger.info("No record is available to activate");
			return;
		}
		for( Division DivisionName : selectedDocs ){
			divisionsDAO.deleteDivision(commonError, loginUser,DivisionName, CommonConstants.N );
		}

	}
	
	/**
	 * This method calls DAO to de-activate Division
	 */
	@Override
	public void lockViewedDivision(CommonError commonError, User loginUser, Division DivisionName) {
		divisionsDAO.updateActiveDivision(commonError, loginUser,DivisionName, CommonConstants.N );
		
	}
	/**
	 * This method calls DAO to activate Division
	 */
	@Override
	public void unlockViewedDivision(CommonError commonError, User loginUser, Division DivisionName) {
		divisionsDAO.updateActiveDivision(commonError, loginUser,DivisionName, CommonConstants.Y );
		
	}

	/**
	 * This method calls DAO to deletes particular division
	 */
	@Override
	public void deleteViewedDivision(CommonError commonError, User loginUser, Division DivisionName) {
		divisionsDAO.deleteDivision(commonError, loginUser,DivisionName, CommonConstants.N );
		
	}


	//Project Service	 

		/**
		 * This method calls DAO to get all active Projects from DB
		 */
		public List<Project> getActiveProjectList(CommonError commonError) {
			return projectDAO.getProjectList(commonError, CommonConstants.Y);
		}

		/**
		 * This method calls DAO to get all Inactive Projects level from DB
		 */
		@Override
		public List<Project> getInactiveProjectList(CommonError commonError) {
			return projectDAO.getProjectList(commonError,CommonConstants.N);
		}

		/**
		 * This method calls DAO to insert new Project to DB
		 */
		@Override
		public void addProject(CommonError commonError, User loginUser, Project project) {
			projectDAO.addProject(commonError, loginUser, project);		
		}

		/**
		 * This method calls DAO to update existing Project level to DB
		 */
		@Override
		public void updateProject(CommonError commonError, User loginUser, Project project) {
			projectDAO.updateProject(commonError, loginUser, project);

		}


		/**
		 * This method calls DAO to get existing Project from DB
		 */
		@Override
		public Project getProject(CommonError commonError, String ProjectId) {
			return projectDAO.getProject(commonError,ProjectId);
		}

		/**
		 * This method calls DAO to de-activate Projects
		 */
		@Override
		public void lockProject(CommonError commonError, User loginUser, List<Project> SelectedDocs) {
			if( SelectedDocs == null || SelectedDocs.isEmpty() ){
				return;
			}
			for( Project project : SelectedDocs ){
				projectDAO.updateActiveProject(commonError, loginUser,project, CommonConstants.N );
			}


		}

		/**
		 * This method calls DAO to activate the locked Projects
		 */
		public void unlockProject(CommonError commonError, User loginUser, List<Project> SelectedDocs) {
			if( SelectedDocs == null || SelectedDocs.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Project project : SelectedDocs ){
				projectDAO.updateActiveProject(commonError, loginUser, project, CommonConstants.Y );
			}
		}


		/**
		 * This method calls DAO to deletes particular Project
		 */
		@Override
		public void deleteProject(CommonError commonError, User loginUser, List<Project> SelectedDocs) {
			if( SelectedDocs == null || SelectedDocs.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Project project : SelectedDocs ){
				 projectDAO.deleteProject(commonError, loginUser,project, CommonConstants.N );
			}

		}
		
		/**
		 * This method calls DAO to de-activate Project
		 */
		@Override
		public void lockViewedProject(CommonError commonError, User loginUser, Project project) {
			projectDAO.updateActiveProject(commonError, loginUser,project, CommonConstants.N );
			
		}
		/**
		 * This method calls DAO to activate Project
		 */
		@Override
		public void unlockViewedProject(CommonError commonError, User loginUser, Project project) {
			projectDAO.updateActiveProject(commonError, loginUser,project, CommonConstants.Y );
			
		}

		/**
		 * This method calls DAO to deletes particular project
		 */
		@Override
		public void deleteViewedProject(CommonError commonError, User loginUser, Project project) {
			projectDAO.deleteProject(commonError, loginUser,project, CommonConstants.N );
			
		}
		
		//TaskType Service	 

		/**
		 * This method calls DAO to get all active Types from DB
		 */
		public List<TaskType> getActiveTypeList(CommonError commonError) {
			return taskTypeDAO.getTaskTypeList(commonError, CommonConstants.Y);
		}

		/**
		 * This method calls DAO to get all Inactive Types level from DB
		 */
		@Override
		public List<TaskType> getInactiveTypeList(CommonError commonError) {
			return taskTypeDAO.getTaskTypeList(commonError,CommonConstants.N);
		}

		/**
		 * This method calls DAO to insert new TaskType to DB
		 */
		@Override
		public void addTaskType(CommonError commonError, User loginUser, TaskType TaskTypeName) {
			taskTypeDAO.addTaskType(commonError, loginUser, TaskTypeName);		
		}

		/**
		 * This method calls DAO to update existing TaskType level to DB
		 */
		@Override
		public void updateTaskType(CommonError commonError, User loginUser, TaskType TaskTypeName) {
			taskTypeDAO.updateTaskType(commonError, loginUser, TaskTypeName);

		}


		/**
		 * This method calls DAO to get existing TaskType from DB
		 */
		@Override
		public TaskType getTaskType(CommonError commonError, String TaskTypeId) {
			return taskTypeDAO.getTaskType(commonError,TaskTypeId);
		}

		/**
		 * This method calls DAO to de-activate Types
		 */
		@Override
		public void lockTaskType(CommonError commonError, User loginUser, List<TaskType> selectedDocs) {
			if( selectedDocs == null || selectedDocs.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( TaskType TaskTypeName : selectedDocs ){
				taskTypeDAO.updateActiveTaskType(commonError, loginUser,TaskTypeName, CommonConstants.N );
			}


		}

		/**
		 * This method calls DAO to activate the locked Types
		 */
		public void unlockTaskType(CommonError commonError, User loginUser, List<TaskType> selectedDocs) {
			if( selectedDocs == null || selectedDocs.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( TaskType TaskTypeName : selectedDocs ){
				taskTypeDAO.updateActiveTaskType(commonError, loginUser,TaskTypeName, CommonConstants.Y );
			}
		}


		@Override
		public void deleteTaskType(CommonError commonError, User loginUser, List<TaskType> typeList) {
			if( typeList == null || typeList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( TaskType type : typeList ){
				taskTypeDAO.deleteTaskType(commonError, loginUser,type, CommonConstants.N );
			}
			
		}


		@Override
		public void lockViewedTaskType(CommonError commonError, User loginUser, TaskType type) {
			taskTypeDAO.updateActiveTaskType(commonError, loginUser,type, CommonConstants.N );
			
		}


		@Override
		public void unlockViewedTaskType(CommonError commonError, User loginUser, TaskType type) {
			taskTypeDAO.updateActiveTaskType(commonError, loginUser,type, CommonConstants.Y );
			
		}


		@Override
		public void deleteViewedTaskType(CommonError commonError, User loginUser, TaskType type) {
			taskTypeDAO.deleteTaskType(commonError, loginUser,type, CommonConstants.Y );
			
		}
		
		
	 //Disposals
		
	   /**
	    * This method calls DAO to get all Inactive Disposals from DB
		* @param commonError
		* @return
		*/
		@Override
		public List<Disposals> getInactiveDisposalsList(CommonError commonError) {
			return  disposalsDAO.getDisposalsList(commonError,CommonConstants.N);
		}
		 
	   /**
		* This method calls DAO to get all Inactive Disposals list from DB
		* @param commonError
		* @return
		*/
		@Override
		public List<Disposals> getActiveDisposalsList(CommonError commonError) {
			return disposalsDAO.getDisposalsList(commonError,CommonConstants.Y);
		}
		 
	   /**
	    * This method will interact with DAO to get  Disposals in DB
		* @param commonError
		* @param facilitiesId
		* @return
		*/
		@Override
		public Disposals getDisposals(CommonError commonError, String disposalsId) {
			return disposalsDAO.getDisposals(commonError, disposalsId);
		}
		
	   /**
		* This method calls DAO to de-activate Disposals
		* @param commonError
		* @param loginUser
		* @param selectedDisposalsList
		*/
		@Override
		public void lockDisposals(CommonError commonError, User loginUser, List<Disposals> selectedDisposalsList) {
			List<String> idList=new ArrayList<>();
			if( selectedDisposalsList == null || selectedDisposalsList.isEmpty() ){
			return;
			}
			for( Disposals disposals : selectedDisposalsList ){
				idList.add(disposals.getId());
			}
			disposalsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );		
		}
		
	   /**
		* This method calls DAO to activate the locked disposals
		* @param commonError
		* @param loginUser
		* @param selectedDisposalsList
		*/
		@Override
		public void unlockDisposals(CommonError commonError, User loginUser, List<Disposals> selectedDisposalsList) {
			List<String> idList=new ArrayList<>();
			if( selectedDisposalsList == null || selectedDisposalsList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Disposals disposals : selectedDisposalsList ){
				idList.add(disposals.getId());
			}
			disposalsDAO.updateActiveStatus(commonError, loginUser, idList, CommonConstants.Y);			
		}
		
	   /**
		* This method will interact with DAO to delete a disposals in DB
		* @param commonError
		* @param loginUser
		* @param selectedDisposalsList
		*/
		@Override
		public void deleteDisposals(CommonError commonError, User loginUser, List<Disposals> selectedDisposalsList) {
			List<String> idList=new ArrayList<>();
			if( selectedDisposalsList == null || selectedDisposalsList.isEmpty() ){
				logger.info("No record is available to activate");
				return;
			}
			for( Disposals disposals : selectedDisposalsList ){
				idList.add(disposals.getId());
			}
			disposalsDAO.deleteDisposals(commonError, loginUser, idList);		
		}
		
	   /**
		* This method calls DAO to deletes particular disposals
		* @param commonError
		* @param loginUser
		* @param disposals
		*/
		@Override
		public void deleteViewedDisposals(CommonError commonError, User loginUser, Disposals disposals) {
			List<String> idList=new ArrayList<>();
			idList.add(disposals.getId());
			disposalsDAO.deleteDisposals(commonError, loginUser, idList);
		}
		
	   /**
		* This method calls DAO to de-activate disposals
		* @param commonError
		* @param loginUser
		* @param disposals
		*/
		@Override
		public void lockViewedDisposals(CommonError commonError, User loginUser, Disposals disposals) {
			List<String> idList=new ArrayList<>();
			idList.add(disposals.getId());
			disposalsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.N );		
		}
		
	   /**
		* This method calls DAO to activate disposals
		* @param commonError
		* @param loginUser
		* @param disposals
		*/
		@Override
		public void unlockViewedDisposals(CommonError commonError, User loginUser, Disposals disposals) {
			List<String> idList=new ArrayList<>();
			idList.add(disposals.getId());
			disposalsDAO.updateActiveStatus(commonError, loginUser,idList, CommonConstants.Y );		
		}
		
	   /**
		* This method will interact with DAO to insert a disposals in DB
		* @param commonError
		* @param loginUser
		* @param disposals
		*/
		@Override
		public void add(CommonError commonError, User loginUser, Disposals disposals) {
			disposalsDAO.addDisposals(commonError, loginUser, disposals );		
		}
		
	   /**
		* This method will interact with DAO to update a disposals in DB
		* @param commonError
		* @param loginUser
		* @param disposals
		*/
		@Override
		public void update(CommonError commonError, User loginUser, Disposals disposals) {
			disposalsDAO.updateDisposals(commonError, loginUser, disposals );		
		}

	@Override
	public void updateTaskStatusList(List<String> taskIdList) {
		if(!taskIdList.isEmpty()){
			taskDAO.updateTaskStatusList(taskIdList);
		}
		
	}

	@Override
	public void addTaskFacilities(CommonError commonError, User loginUser, Map<Integer, Task> hmapFacilities) {
		
		taskDAO.addTaskFacilities(commonError, loginUser, hmapFacilities);
	}


}
