package com.orb.taskplanner.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.TaskType;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class TaskTypeDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(TaskTypeDAO.class);
	
	/**
	 * This method will return list of TaskType levels from DB
	 * @param commonError
	 * @return
	 */
	public List<TaskType> getTaskTypeList(CommonError commonError, String activeFlag ) {
		logger.info(" ==> Get TaskTypeList START <== ");
		List<TaskType> categoryList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.TASKTYPE.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TYPE);newQry.append(" WHERE ACTIVE = ? ");
			categoryList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(TaskType.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch TaskType list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get TaskTypeList END <== ");
		return categoryList;
	}
	
	/**
	 * This method inserts new TaskType into DB
	 * @param TaskTypeLevelError
	 * @return s
	 */
	
	public void addTaskType(CommonError commonError, User loginUser,TaskType s) {
		logger.info(" ==> Insert TaskType START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_TYPE );newQry.append("( ");newQry.append( TaskPlannerDBConstants.TASKTYPE.get("TYPE") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE.get("CREATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.TASKTYPE.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_TIME") );
		newQry.append( "," );newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ACTIVE") );
		newQry.append(") VALUES(?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getType());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert TaskType.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert TaskType END <== ");
	}
	
	/**
	 * This method updates existing TaskType level in DB
	 * @param taskType
	 * @return
	 */
	public void updateTaskType(CommonError commonError, User loginUser, TaskType s) {
		logger.info(" ==> Update TaskType START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		insertTaskTypeHistory(commonError, loginUser, s.getId(),CommonConstants.UPDATED);
		newQry.append("UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_TYPE );newQry.append(" SET ");
		newQry.append( TaskPlannerDBConstants.TASKTYPE.get("TYPE") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_BY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_TIME") );newQry.append( " = ?" );
		newQry.append(" WHERE ");newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getType());args.add(loginUser.getUserId());args.add(date);args.add(s.getId());
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Update TaskType.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==>  Update TaskType END <== ");

	}
	
	/**
	 * This method used to get a particular TaskType from DB
	 * @param taskType
	 * @return
	 */
	public TaskType getTaskType(CommonError commonError, String TaskTypeId) {
		logger.info(" ==> Get a TaskType START <== ");
		TaskType taskType = null;
		TaskType lockedHist = null;
		TaskType unlockedHist = null;
		
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TYPE);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append( " = ? " );
			taskType = (TaskType)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{TaskTypeId}, new BeanPropertyRowMapper(TaskType.class) );
			
			lockedHist = getUserAction(commonError, taskType.getId(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				/*java.util.Date lockedDate = CommonUtils.convertFromUTC(lockedHist.getModifiedTime());*/
				taskType.setLockedBy( lockedHist.getUpdateBy());
				taskType.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, taskType.getId(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				/*java.util.Date unlockedDate = CommonUtils.convertFromUTC(unlockedHist.getModifiedTime());*/
				taskType.setUnlockedBy( unlockedHist.getUpdateBy() );
				taskType.setUnlockedTime( unlockedHist.getUpdateTime() );
			}	
			
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch TaskType list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a TaskType END <== ");
		return taskType;
	}

	/**
	 * Updates task type to active or inactive
	 * @param commonError
	 * @param loginUser
	 * @param s
	 * @param y
	 */
	public void updateActiveTaskType(CommonError commonError, User loginUser, TaskType s, String y) {
		// TODO Auto-generated method stub
		logger.info(" ==> TaskType Status Update Active/Inactive START <== ");
		logger.info("TaskType Id ==> " + s.getId() );
		try{
			if(y.equals(CommonConstants.Y))
				insertTaskTypeHistory(commonError, loginUser, s.getId(),CommonConstants.UNLOCKED);
			else
				insertTaskTypeHistory( commonError, loginUser,s.getId(), CommonConstants.LOCKED);	
			
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_TYPE );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when TaskType Status Update Active/Inactive of a TaskType.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> TaskType Status Update Active/Inactive END <== ");	
		
	}

	/**
	 * Deletes particular task
	 * @param commonError
	 * @param loginUser
	 */
	public void deleteTaskType(CommonError commonError, User loginUser, TaskType s, String n) {
		logger.info(" ==> Delete TaskType START <== ");
		logger.info("TaskType Id ==> " + s.getId() );
		try{
			insertTaskTypeHistory( commonError, loginUser,s.getId(), CommonConstants.DELETED);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_TYPE);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{s.getId()} );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch TaskType list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete TaskType END <== ");
		
		
	}
	
	/**
	 * Insert task type into history table
	 * @param commonError
	 * @param loginUser
	 */
	private void insertTaskTypeHistory(CommonError commonError, User loginUser, String id, String action) {
		
		logger.info(" ==> Insert TASKTYPEHistory START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			logger.info("TASKTYPE Id ==> " + id );
			StringBuffer qryHist = new StringBuffer("SELECT ");
			for( String column : TaskPlannerDBConstants.TASKTYPE.keySet() ){
				qryHist.append( TaskPlannerDBConstants.TASKTYPE.get(column) );qryHist.append( "," );
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(" FROM ");qryHist.append(TaskPlannerDBConstants.TBL_TYPE);qryHist.append(" WHERE ");qryHist.append( TaskPlannerDBConstants.TASKTYPE.get("ID") );qryHist.append( "=?" );
			TaskType s = (TaskType)getJdbcTemplate().queryForObject( qryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(TaskType.class) );
			
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_TYPE_HIST );newQry.append("( ");
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("IDENTIFIER") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("TYPE") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("CREATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("UPDATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("UPDATE_TIME") );
		newQry.append( "," );newQry.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("ACTION") );
		newQry.append(") VALUES(?,?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getId());
		args.add(s.getType());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(action);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert TASKTYPE History.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert TASKTYPE History END <== ");
	}

/**
 * Get user actions from table for particular table
 * @return
 */
private TaskType getUserAction(CommonError courseError,String identifier, String action){
	TaskType historyInfo = null;
	String fullName = null;
	try{
	StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
	for( String column : TaskPlannerDBConstants.TASKTYPE_HIST.keySet() ){
		newQryHist.append( TaskPlannerDBConstants.TASKTYPE_HIST.get(column) );newQryHist.append( "," );
	}
	newQryHist.setLength(newQryHist.length()-1);
	newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_TYPE_HIST);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.TASKTYPE_HIST.get("IDENTIFIER") );
	newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
	historyInfo = (TaskType)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(TaskType.class) );
	
	
	}
	catch( Exception e ){
		logger.error(" No user action made before ==> " + e.getMessage() );
		
	}
	
	return historyInfo;
	
}


}
