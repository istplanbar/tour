package com.orb.taskplanner.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.common.client.utils.CommonUtils;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.bean.Task;
import com.orb.taskplanner.constants.TaskPlannerConstants;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;


@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class TaskDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(TaskDAO.class);

	/**
	 * This method will return list of Task levels from DB
	 * @param commonError
	 * @return
	 */
	public List<Task> getTaskList(CommonError commonError, String activeFlag , String history) {
		logger.info(" ==> Get TaskList START <== ");
		List<Task> taskList = null;
		String tableName = null;
		if( history == null){
			tableName = TaskPlannerDBConstants.TBL_TASK;
		}else{
			tableName = TaskPlannerDBConstants.TBL_TASK_HIST;
		}
		
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append("b.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS PROJECT " ); newQry.append( "," ); 
			newQry.append("c.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS CATEGORY " ); newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			/*newQry.append("d.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS PARTICIPANT_NAME " );newQry.append( "," );
			newQry.append("e.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS COURSE_ID " );newQry.append( "," );
			newQry.append("f.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS CRM_NAME " );newQry.append( "," );*/
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			if( history == null){
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			}else{
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK_HIST.get("ACTION") );
			}
			newQry.append(" FROM ");newQry.append(tableName);newQry.append(" AS a ");
			newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" AS b ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PROJECT"));newQry.append(" = "); newQry.append("b.");newQry.append("ID");
			newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" AS c ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CATEGORY"));newQry.append(" = "); newQry.append("c.");newQry.append("ID");
			/*newQry.append(" INNER JOIN ");newQry.append(ParticipantDBConstants.TBL_PARTI);newQry.append(" AS d ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME"));newQry.append(" = "); newQry.append("d.");newQry.append("IDENTIFIER");
			newQry.append(" INNER JOIN ");newQry.append(CourseDBConstants.TBL_COURSES);newQry.append(" AS e ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("COURSE_ID"));newQry.append(" = "); newQry.append("e.");newQry.append("IDENTIFIER");
			newQry.append(" INNER JOIN ");newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" AS f ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CRM_NAME"));newQry.append(" = "); newQry.append("f.");newQry.append("IDENTIFIER");*/
			if( history == null){
				newQry.append(" WHERE a.ACTIVE = ? AND a.PARENT_TASK IS NULL ORDER BY a.IDENTIFIER");
				taskList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Task.class) );
			}else{
				newQry.append(" WHERE a.ACTION = ? AND a.PARENT_TASK IS NULL ORDER BY  a.UPDATE_TIME DESC");
				taskList = getJdbcTemplate().query( newQry.toString(),new String[]{CommonConstants.UPDATED}, new BeanPropertyRowMapper(Task.class) );
			}
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch task list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get TaskList END <== ");
		return taskList;
	}
	
	/**
	 * This method will return list of Task levels from DB
	 * @param commonError
	 * @return
	 */
	public List<Task> getUserTaskList(CommonError commonError, User loginUser, String activeFlag ) {
		logger.info(" ==> Get TaskList START <== ");
		List<Task> taskList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append("b.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS PROJECT " ); newQry.append( "," ); 
			newQry.append("c.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS CATEGORY " ); newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			/*newQry.append("d.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS PARTICIPANT_NAME " );newQry.append( "," );
			newQry.append("e.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS COURSE_ID " );newQry.append( "," );
			newQry.append("f.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS CRM_NAME " );newQry.append( "," );*/
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" AS a ");
			newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" AS b ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PROJECT"));newQry.append(" = "); newQry.append("b.");newQry.append("ID");
			newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" AS c ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CATEGORY"));newQry.append(" = "); newQry.append("c.");newQry.append("ID");
			/*newQry.append(" INNER JOIN ");newQry.append(ParticipantDBConstants.TBL_PARTI);newQry.append(" AS d ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME"));newQry.append(" = "); newQry.append("d.");newQry.append("IDENTIFIER");
			newQry.append(" INNER JOIN ");newQry.append(CourseDBConstants.TBL_COURSES);newQry.append(" AS e ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("COURSE_ID"));newQry.append(" = "); newQry.append("e.");newQry.append("IDENTIFIER");
			newQry.append(" INNER JOIN ");newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" AS f ");
			newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CRM_NAME"));newQry.append(" = "); newQry.append("f.");newQry.append("IDENTIFIER");*/
			newQry.append(" WHERE a.ASSIGNTO = ? AND a.ACTIVE = ? ORDER BY a.IDENTIFIER");
			taskList = getJdbcTemplate().query( newQry.toString(),new Object[]{loginUser.getUserId(),activeFlag}, new BeanPropertyRowMapper(Task.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch task list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get TaskList END <== ");
		return taskList;
	}

	/**
	 * This method inserts new Task into DB
	 * @param commonError
	 * @return s
	 */

	public void addTask(CommonError commonError, User loginUser,Task s) {
		logger.info(" ==> Insert Task START <== ");
		StringBuffer newQry = new StringBuffer();

		try{
			//java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			Date date = CommonUtils.getCurrentUTCDate();
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_TASK );newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CATEGORY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(s.getIdentifier());args.add(s.getParentTask());
			args.add(s.getName());
			args.add(s.getCategory());args.add(s.getProject());
			args.add(s.getType());
			args.add(s.getPriority());args.add(s.getStatus());args.add(s.getDeputy());
			args.add(s.getProgress());args.add(s.getReporter());
			args.add(s.getAssignTo());args.add(s.getStartDate());
			args.add(s.getDueDate()); args.add(s.getDescription());
			args.add(s.getAttachment());args.add(s.getDirName());
			args.add(s.getParticipantName());args.add(s.getCourseId());
			args.add(s.getCrmName());args.add(s.getFacilities());args.add(s.getEstimatedTime());
			args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert Task.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Task END <== ");
	}

	/**
	 * This method updates existing Task level in DB
	 * @param task
	 * @return
	 */
	public void updateTask(CommonError commonError, User loginUser, Task s) {
		logger.info(" ==> Update Task START <== ");
		try{
			String tableName=TaskPlannerDBConstants.TBL_TASK;
			updateRecordById(loginUser, s,tableName);

		}catch( Exception e ){
			logger.info("Error when Update Task.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==>  Update Task END <== ");

	}

	/**
	 * This method used to get a particular Task from DB
	 * @param Task
	 * @return
	 */
	public Task getTask(CommonError commonError, String taskId) {
		logger.info(" ==> Get a Task START <== ");
		Task task = null;
		logger.info("Task Id ==> " + taskId );
		if( taskId == null ) return null;
		StringBuffer newQry;
		try{
			newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CATEGORY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( " = ? " );
			task = (Task)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{taskId}, new BeanPropertyRowMapper(Task.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch a single task. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Task END <== ");
		return task;
	}
	
	/**
	 * This method used to get a particular previous task for particular project
	 * @param Task
	 * @return
	 */
	public Task getPreviousTask(CommonError commonError, String proejctId) {
		logger.info(" ==> Get a Task START <== ");
		Task task = null;
		logger.info("Project Id ==> " + proejctId );
		if( proejctId == null ) return null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( "a." ); newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( "a." ); newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );newQry.append( "," );
			newQry.append( "b." ); newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append( "b." ); newQry.append( TaskPlannerDBConstants.PROJECT.get("PROJKEY") );
			newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" AS a ");newQry.append(" INNER JOIN "); 
			newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" AS b ");
			newQry.append(" ON "); newQry.append( "a." ); newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );
			newQry.append(" = "); newQry.append("b.");newQry.append("ID");
			newQry.append(" WHERE ");newQry.append( "b." ); newQry.append( TaskPlannerDBConstants.TASK.get("ID") );
			newQry.append( " = ?  ORDER BY a.ID DESC LIMIT 1" );
			task = (Task)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{proejctId}, new BeanPropertyRowMapper(Task.class) );
			if(task == null){
				
			}
		}catch( Exception e ){
			task = new Task();
			StringBuffer newQry1 = new StringBuffer("SELECT ");
			for( String column : TaskPlannerDBConstants.PROJECT.keySet() ){
				newQry1.append( TaskPlannerDBConstants.PROJECT.get(column) );newQry1.append( "," );
			}
			newQry1.setLength(newQry1.length()-1);
			newQry1.append(" FROM ");newQry1.append(TaskPlannerDBConstants.TBL_PROJ);newQry1.append(" WHERE ");
			newQry1.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry1.append( " = ? " );
			Project proj = (Project)getJdbcTemplate().queryForObject( newQry1.toString(),new String[]{proejctId}, new BeanPropertyRowMapper(Project.class) );
			task.setProjKey(proj.getProjKey());
			task.setIdentifier("");
			return task;
		}
		logger.info(" ==> Get a Task END <== ");
		return task;
	}

	public void updateActiveTask(CommonError commonError, User loginUser, Task s, String y) {
		logger.info(" ==> Task Status Update Active/Inactive START <== ");
		logger.info("Task Id ==> " + s.getId() );
		try{
			if(y.equals(CommonConstants.Y)){
				insertTaskHistory( commonError, loginUser,s.getId(), CommonConstants.UNLOCKED);
			}else{
				insertTaskHistory( commonError, loginUser,s.getId(), CommonConstants.LOCKED);
			}
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_TASK );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when Update Active/Inactive status of a Task.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Task Status Update Active/Inactive END <== ");	

	}

	public void deleteTask(CommonError commonError, User loginUser, Task s, String n) {
		logger.info(" ==> Delete Task START <== ");
		logger.info("Task Id ==> " + s.getId() );
		try{
			insertTaskHistory( commonError, loginUser,s.getId(),CommonConstants.DELETED);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{s.getId()} );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch department list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Task END <== ");


	}
	

	/**
	 * This method inserts updated Task  history table
	 * @param commonError
	 * @return s
	 */

	public void insertTaskHistory(CommonError commonError, User loginUser,String id, String action) throws Exception {
		logger.info(" ==> Insert Task History START <== ");
		logger.info("Task Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : TaskPlannerDBConstants.TASK.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.TASK.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_TASK);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.TASK.get("ID") );newQryHist.append( "=?" );
		Task s = (Task)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(Task.class) );
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_TASK_HIST);newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("CATEGORY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("PROJECT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("PRIORITY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("STATUS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("DEPUTY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("PROGRESS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("REPORTER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("ASSIGNTO") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("STARTDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("DUEDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("DESCRIPTION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("ATTACHMENT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("DIR_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("COURSE_ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("CRM_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("FACILITIES") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("ESTIMATED_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK_HIST.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(s.getIdentifier());args.add(s.getName());
			args.add(s.getCategory());args.add(s.getProject());
			args.add(s.getType());
			args.add(s.getPriority());args.add(s.getStatus());args.add(s.getDeputy());
			args.add(s.getProgress());args.add(s.getReporter());
			args.add(s.getAssignTo());
			args.add(s.getStartDate());args.add(s.getDueDate()); 
			args.add(s.getDescription()); args.add(s.getAttachment());
			args.add(s.getDirName());args.add(s.getParticipantName());
			args.add(s.getCourseId());args.add(s.getCrmName());args.add(s.getFacilities());args.add(s.getEstimatedTime());
			args.add(s.getCreateBy());args.add(s.getCreateTime());
			args.add(loginUser.getUserId());args.add(date);args.add(action);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert Task History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Task History END <== ");
	}

	public String genNextRowId(CommonError commonError) {
		return getNextRowId( TaskPlannerDBConstants.TBL_TASK, TaskPlannerDBConstants.TASK_DB_CONFIG.get("rowPrefix") );

	}

	public String genNextDirName(){
		return getNextRowId( TaskPlannerDBConstants.TBL_TASK, TaskPlannerConstants.NEXT_DIR_NAME);
	}
	
	/**
	 * aDD NEW WATCH LIST INTO TABLE
	 * @param commonError
	 * @param taskId
	 * @param loginUser
	 */
	
	public void addTaskToWatchList( CommonError commonError, String taskId, User loginUser){
	logger.info(" ==> Insert Watch List START <== ");
	StringBuffer newQry = new StringBuffer();

	try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_WATCH_LIST);newQry.append("( ");
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("IDENTIFIER") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("WATCH_LIST") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("CREATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("UPDATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("UPDATE_TIME") );
		newQry.append(") VALUES(?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(taskId);args.add(CommonConstants.TRUE);
		args.add(loginUser.getUserId());
		args.add(date);args.add(loginUser.getUserId());
		args.add(date);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );

	}catch( Exception e ){
		logger.info("Error when Insert watch list.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert Watch List END <== ");
	}
	
	/*
	 * THIS Method calls when task is removed from watch list
	 */
	public void removeTaskFromWatchList( CommonError commonError, String taskId, User loginUser){
		logger.info(" ==> Delete watch list START <== ");
		StringBuffer newQry = new StringBuffer();

		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("DELETE FROM ");newQry.append( TaskPlannerDBConstants.TBL_WATCH_LIST);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("IDENTIFIER") );newQry.append( " =? AND ");
			newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get("CREATE_BY") );newQry.append( " =? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{taskId,loginUser.getUserId()} );

		}catch( Exception e ){
			logger.info("Error when Insert Task.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete watch list END <== ");
	}
	
	/**
	 * This method used to get a particular Task from DB
	 * @param Task
	 * @return
	 */
	public List<Task> getWatchList(CommonError commonError) {
		logger.info(" ==> Get a Task START <== ");
		List<Task> taskWatchList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : TaskPlannerDBConstants.TASK_WATCH_LIST.keySet() ){
				newQry.append( TaskPlannerDBConstants.TASK_WATCH_LIST.get(column) );newQry.append( "," );
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_WATCH_LIST);
			taskWatchList = (List<Task>)getJdbcTemplate().query( newQry.toString(),  new BeanPropertyRowMapper(Task.class) );
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch watch List. Exception ==> " + e.getMessage() );
		}
		logger.info(" ==> Get a Task END <== ");
		return taskWatchList;
	}
	
	
	public void cloneTask(CommonError commonError, User loginUser,Task s) {
		logger.info(" ==> Insert Task Clone START <== ");
		StringBuffer newQry = new StringBuffer();

		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_TASK );newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CATEGORY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(s.getIdentifier());args.add(s.getName());
			args.add(s.getCategory());args.add(s.getProject());
			args.add(s.getType());
			args.add(s.getPriority());args.add(s.getStatus());args.add(s.getDeputy());
			args.add(s.getProgress());args.add(s.getReporter());
			args.add(s.getAssignTo());args.add(s.getStartDate());
			args.add(s.getDueDate()); args.add(s.getDescription());
			args.add(s.getAttachment());args.add(s.getDirName());
			args.add(s.getParticipantName());args.add(s.getCourseId());
			args.add(s.getCrmName());args.add(s.getFacilities());args.add(s.getEstimatedTime());
			args.add(loginUser.getUserId());
			args.add(date);args.add(loginUser.getUserId());
			args.add(date);args.add(CommonConstants.Y);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert Task Clone.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Task Clone END <== ");
	}
	
	
	
	/**
	 * This method used to get a sub task list from DB
	 * @param Task
	 * @return
	 */
	public String getSubTaskId(CommonError commonError, String taskId) {
		logger.info(" ==> Get a Sub Task Id START <== ");
		String subTaskId = null;
		logger.info("Task Id ==> " + taskId );
		if( taskId == null ) return null;
		StringBuffer newQry;
		try{
			newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.TASK.get("ID") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " = ? " );
			subTaskId = getJdbcTemplate().queryForObject(newQry.toString(), String.class, taskId);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch a single task. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Sub Task END <== ");
		return subTaskId;
	}
	
	 public List<Task> getActiveTaskList(CommonError error,String activeFlag) {
		  logger.info(" ==> get SubTask List START <== ");
		  List<Task>  taskList = null;
		  try{
			    StringBuffer newQry = new StringBuffer("SELECT ");
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ID") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
				newQry.append("b.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS PROJECT " ); newQry.append( "," ); 
				newQry.append("c.");newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( " AS CATEGORY " ); newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
				/*newQry.append("d.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS PARTICIPANT_NAME " );newQry.append( "," );
				newQry.append("e.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS COURSE_ID " );newQry.append( "," );
				newQry.append("f.");newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( " AS CRM_NAME " );newQry.append( "," );*/
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
				newQry.append("a.");newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
				newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_TASK);newQry.append(" AS a ");
				newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" AS b ");
				newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PROJECT"));newQry.append(" = "); newQry.append("b.");newQry.append("ID");
				newQry.append(" INNER JOIN ");newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" AS c ");
				newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CATEGORY"));newQry.append(" = "); newQry.append("c.");newQry.append("ID");
				/*newQry.append(" INNER JOIN ");newQry.append(ParticipantDBConstants.TBL_PARTI);newQry.append(" AS d ");
				newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME"));newQry.append(" = "); newQry.append("d.");newQry.append("IDENTIFIER");
				newQry.append(" INNER JOIN ");newQry.append(CourseDBConstants.TBL_COURSES);newQry.append(" AS e ");
				newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("COURSE_ID"));newQry.append(" = "); newQry.append("e.");newQry.append("IDENTIFIER");
				newQry.append(" INNER JOIN ");newQry.append(CrmDBConstants.TBL_CRM);newQry.append(" AS f ");
				newQry.append(" ON "); newQry.append("a.");newQry.append(TaskPlannerDBConstants.TASK.get("CRM_NAME"));newQry.append(" = "); newQry.append("f.");newQry.append("IDENTIFIER");*/
				newQry.append(" WHERE a.ACTIVE = ? ");
				taskList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Task.class) );
				logger.info(taskList.size());
			}catch( Exception e ){
				logger.error(" Exception occured when tries to fetch task list. Exception ==> " + e.getMessage() );
				error.addError(CommonConstants.DB_ERROR);
			}
	 
		    logger.info(" ==> get SubTask List END <== ");
		    return taskList;
   }

	/**
	 * Updates the task status to assigned when the task is assigned to Tour
	 * @param taskIdList
	 */
	public void updateTaskStatusList(List<String> taskIdList) {
		logger.info(" ==> Update Task Status for tour START <== ");	
		try{
			StringBuffer qry = new StringBuffer();
			
			qry.append(" UPDATE ");qry.append( TaskPlannerDBConstants.TBL_TASK );qry.append(" SET ");
			qry.append( TaskPlannerDBConstants.TASK.get("STATUS") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( TaskPlannerDBConstants.TASK.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, TaskPlannerConstants.TOUR_TASKS_STATUS);
					ps.setString(2, taskIdList.get(i));
				}
				public int getBatchSize() {
					return taskIdList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when update Task Status on Tour.  " + e.getMessage());
		}
		logger.info(" ==> Update Task Status for tour END <== ");		
	}

	/**
	 * This method will return tasklist from DB.
	 * @param commonError
	 * @param loginUser
	 * @param ct
	 */
	public List<Task> getTaskDataList(CommonError commonError, String id) {
		logger.info(" ==>Return Task Status for tour START <== ");	
		List<Task> taskList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			qry.append("a.");qry.append( TaskPlannerDBConstants.TASK.get("ID") );qry.append( "," );
			qry.append("a.");qry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );qry.append( "," );
			qry.append("a.");qry.append( TaskPlannerDBConstants.TASK.get("NAME") );qry.append( "," );
			qry.append("b.");qry.append( TaskPlannerDBConstants.TASK.get("NAME") );qry.append( " AS PROJECT " ); qry.append( "," );
			qry.append("a.");qry.append( TaskPlannerDBConstants.TASK.get("STATUS") );qry.append( "," );
			qry.append("a.");qry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );
			qry.append(" FROM ");qry.append(TaskPlannerDBConstants.TBL_TASK);qry.append(" AS a ");
			qry.append(" INNER JOIN ");qry.append(TaskPlannerDBConstants.TBL_PROJ);qry.append(" AS b ");
			qry.append(" ON "); qry.append("a.");qry.append(TaskPlannerDBConstants.TASK.get("PROJECT"));
			qry.append(" = "); qry.append("b.");qry.append("ID");
			qry.append(" WHERE ");qry.append(TaskPlannerDBConstants.TASK.get("FACILITIES"));qry.append("=? ");
			taskList = getJdbcTemplate().query( qry.toString() ,new String[]{id}, new BeanPropertyRowMapper(Task.class));
		}catch( Exception e ){
			logger.info("Error when Return Task Status on Tour.  " + e.getMessage());
		}
		logger.info(" ==> Return Task Status for tour END <== ");		
		return taskList;
	}

	public void addTaskFacilities(CommonError commonError, User loginUser, Map<Integer, Task> hmapFacilities) {
		
		logger.info(" ==> Insert Task START <== ");
		StringBuffer newQry = new StringBuffer();

		try{
			//java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			Date date = CommonUtils.getCurrentUTCDate();
			
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_TASK );newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.TASK.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARENT_TASK") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CATEGORY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROJECT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PRIORITY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STATUS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DEPUTY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PROGRESS") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("REPORTER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ASSIGNTO") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("STARTDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DUEDATE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DESCRIPTION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ATTACHMENT") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("DIR_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("PARTICIPANT_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("COURSE_ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CRM_NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("FACILITIES") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ESTIMATED_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.TASK.get("ACTIVE") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			for (Entry<Integer, Task> s : hmapFacilities.entrySet())  {
				 
				List<Object> args = new ArrayList<Object>();
				args.add(s.getValue().getIdentifier());args.add(s.getValue().getParentTask());
				args.add(s.getValue().getName());
				args.add(s.getValue().getCategory());args.add(s.getValue().getProject());
				args.add(s.getValue().getType());
				args.add(s.getValue().getPriority());args.add(s.getValue().getStatus());args.add(s.getValue().getDeputy());
				args.add(s.getValue().getProgress());args.add(s.getValue().getReporter());
				args.add(s.getValue().getAssignTo());args.add(s.getValue().getStartDate());
				args.add(s.getValue().getDueDate()); args.add(s.getValue().getDescription());
				args.add(s.getValue().getAttachment());args.add(s.getValue().getDirName());
				args.add(s.getValue().getParticipantName());args.add(s.getValue().getCourseId());
				args.add(s.getValue().getCrmName());args.add(s.getValue().getFacilities());args.add(s.getValue().getEstimatedTime());
				args.add(loginUser.getUserId());
				args.add(date);args.add(loginUser.getUserId());
				args.add(date);args.add(CommonConstants.Y);
				getJdbcTemplate().update( newQry.toString(),args.toArray() );
			}
			

		}catch( Exception e ){
			logger.info("Error when Insert Task.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Task END <== ");
	}
	
	
}
