package com.orb.taskplanner.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.TaskCategory;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class TaskCategoryDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(TaskCategoryDAO.class);
	
	/**
	 * This method will return list of Category from DB
	 * @param commonError
	 * @return
	 */
	public List<TaskCategory> getCategoryList(CommonError commonError, String activeFlag ) {
		logger.info(" ==> Get CategoryList START <== ");
		List<TaskCategory> categoryList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" WHERE ACTIVE = ? ");
			categoryList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(TaskCategory.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch category list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get CategoryList END <== ");
		return categoryList;
	}
	
	/**
	 * This method inserts new Category into DB
	 * @param sensitivityLevelError
	 * @return s
	 */
	
	public void addCategory(CommonError commonError, User loginUser,TaskCategory s) {
		logger.info(" ==> Insert Category START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_CATY );newQry.append("( ");newQry.append( TaskPlannerDBConstants.CATEGORY.get("NAME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.CATEGORY.get("CREATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_TIME") );
		newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("ACTIVE") );
		newQry.append(") VALUES(?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getName());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert Category.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert Category END <== ");
	}
	
	/**
	 * This method updates existing Category in DB
	 * @param categoryName
	 * @return
	 */
	public void updateCategory(CommonError commonError, User loginUser, TaskCategory c) {
		logger.info(" ==> Update Category START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		insertCategoryHistory(commonError, loginUser, c.getId(),CommonConstants.UPDATED);
		newQry.append("UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_CATY );newQry.append(" SET ");
		newQry.append( TaskPlannerDBConstants.CATEGORY.get("NAME") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_BY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_TIME") );newQry.append( " = ?" );
		newQry.append(" WHERE ");newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(c.getName());args.add(loginUser.getUserId());args.add(date);args.add(c.getId());
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Update Category.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==>  Update Category END <== ");

	}
	/**
	 * Inserting old record in category History
	 * @param commonError
	 * @param loginUser
	 * @param id
	 */
	public void insertCategoryHistory(CommonError commonError, User loginUser,String id, String action) throws Exception {
		logger.info(" ==> Insert Category History START <== ");
		logger.info("Category Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : TaskPlannerDBConstants.CATEGORY.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.CATEGORY.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_CATY);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQryHist.append( "=?" );
		TaskCategory c = (TaskCategory)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(TaskCategory.class) );
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_CATY_HIST);newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY_HIST.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(c.getId());
			args.add(c.getName());args.add(loginUser.getUserId());args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(action);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert CategoryHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Category History END <== ");
	}

	/**
	 * This method used to get a particular Category from DB
	 * @param categoryName
	 * @return
	 */
	public TaskCategory getCategoryName(CommonError commonError, String confId) {
		logger.info(" ==> Get a Category START <== ");
		TaskCategory categoryName = null;
		TaskCategory lockedHist = null;
		TaskCategory unlockedHist = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("NAME") );
			newQry.append( "," );newQry.append( TaskPlannerDBConstants.CATEGORY.get("ACTIVE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("UPDATE_TIME") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append( " = ? " );
			categoryName = (TaskCategory)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{confId}, new BeanPropertyRowMapper(TaskCategory.class) );
			
			lockedHist = getUserAction(commonError, categoryName.getId(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				/*java.util.Date lockedDate = CommonUtils.convertFromUTC(lockedHist.getModifiedTime());*/
				categoryName.setLockedBy( lockedHist.getUpdateBy());
				categoryName.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, categoryName.getId(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				/*java.util.Date unlockedDate = CommonUtils.convertFromUTC(unlockedHist.getModifiedTime());*/
				categoryName.setUnlockedBy( unlockedHist.getUpdateBy() );
				categoryName.setUnlockedTime( unlockedHist.getUpdateTime() );
			}	
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch category list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Category END <== ");
		return categoryName;
	}

	/**
	 * Get user actions from table for particular table
	 * @return
	 */
	private TaskCategory getUserAction(CommonError courseError,String identifier, String action){
		TaskCategory historyInfo = null;
		String fullName = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TaskPlannerDBConstants.CATEGORY_HIST.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.CATEGORY_HIST.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_CATY_HIST);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.CATEGORY_HIST.get("IDENTIFIER") );
		newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
		historyInfo = (TaskCategory)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(TaskCategory.class) );
		
		
		}
		catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
			
		}
		
		return historyInfo;
		
	}
	
	public void updateActiveCat(CommonError commonError, User loginUser, TaskCategory s, String y) {
		// TODO Auto-generated method stub
		logger.info(" ==> Category Status Update Active/Inactive START <== ");
		logger.info("Category Id ==> " + s.getId() );
		try{
			if(y.equals(CommonConstants.Y))
				insertCategoryHistory( commonError, loginUser,s.getId(), CommonConstants.UNLOCKED);
			else
				insertCategoryHistory( commonError, loginUser,s.getId(), CommonConstants.LOCKED);		
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_CATY );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when Category Status Update Active/Inactive of a Category.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Category Status Update Active/Inactive END <== ");	
		
	}

	public void deleteCategory(CommonError commonError, User loginUser, TaskCategory c, String n) {
		logger.info(" ==> Delete Category START <== ");
		logger.info("Category Id ==> " + c.getId() );
		try{
			insertCategoryHistory( commonError, loginUser,c.getId(),CommonConstants.DELETED);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_CATY);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.CATEGORY.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{c.getId()} );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch category list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Category END <== ");
		
		
	}


}
