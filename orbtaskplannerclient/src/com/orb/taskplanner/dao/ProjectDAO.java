package com.orb.taskplanner.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.Project;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class ProjectDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(ProjectDAO.class);
	
	/**
	 * This method will return list of Project levels from DB
	 * @param commonError
	 * @return
	 */
	public List<Project> getProjectList(CommonError commonError, String activeFlag ) {
		logger.info(" ==> Get ProjectList START <== ");
		List<Project> projectList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("PROJKEY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("DIVISION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" WHERE ACTIVE = ? ");
			projectList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Project.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Project list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get ProjectList END <== ");
		return projectList;
	}
	
	/**
	 * This method inserts new Project into DB
	 * @param ProjectLevelError
	 * @return s
	 */
	
	public void addProject(CommonError commonError, User loginUser,Project s) {
		logger.info(" ==> Insert Project START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_PROJ );newQry.append("( ");
		newQry.append( TaskPlannerDBConstants.PROJECT.get("NAME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("PROJKEY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("TYPE") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("DIVISION") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("CREATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.PROJECT.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_TIME") );
		newQry.append( "," );newQry.append( TaskPlannerDBConstants.PROJECT.get("ACTIVE") );
		newQry.append(") VALUES(?,?,?,?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getName());args.add(s.getProjKey());args.add(s.getType());args.add(s.getDivision());
		args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert Project.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert Project END <== ");
	}
	
	/**
	 * This method updates existing Project level in DB
	 * @param project
	 * @return
	 */
	public void updateProject(CommonError commonError, User loginUser, Project p) {
		logger.info(" ==> Update Project START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		insertProjectHistory(commonError, loginUser, p.getId(),CommonConstants.UPDATED);
		newQry.append("UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_PROJ );newQry.append(" SET ");
		newQry.append( TaskPlannerDBConstants.PROJECT.get("NAME") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("PROJKEY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("TYPE") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("DIVISION") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_BY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_TIME") );newQry.append( " = ?" );
		newQry.append(" WHERE ");newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(p.getName());args.add(p.getProjKey());args.add(p.getType());args.add(p.getDivision());args.add(loginUser.getUserId());args.add(date);args.add(p.getId());
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Update Project.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==>  Update Project END <== ");

	}
	
	/**
	 * This method used to get a particular Project from DB
	 * @param project
	 * @return
	 */
	public Project getProject(CommonError commonError, String ProjectId) {
		logger.info(" ==> Get a Project START <== ");
		Project project = null;
		Project lockedHist = null;
		Project unlockedHist = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("PROJKEY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ACTIVE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("CREATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.PROJECT.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.PROJECT.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT.get("DIVISION") );
		    newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append( " = ? " );
			project = (Project)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{ProjectId}, new BeanPropertyRowMapper(Project.class) );
			
			lockedHist = getUserAction(commonError, project.getId(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				/*java.util.Date lockedDate = CommonUtils.convertFromUTC(lockedHist.getModifiedTime());*/
				project.setLockedBy( lockedHist.getUpdateBy());
				project.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, project.getId(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				/*java.util.Date unlockedDate = CommonUtils.convertFromUTC(unlockedHist.getModifiedTime());*/
				project.setUnlockedBy( unlockedHist.getUpdateBy() );
				project.setUnlockedTime( unlockedHist.getUpdateTime() );
			}	
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Project list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Project END <== ");
		return project;
	}

	/**
	 * Get user actions from table for particular table
	 * @return
	 */
	private Project getUserAction(CommonError commonError,String identifier, String action){
		Project historyInfo = null;
		String fullName = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TaskPlannerDBConstants.PROJECT_HIST.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.PROJECT_HIST.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_PROJ_HIST);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.PROJECT_HIST.get("IDENTIFIER") );
		newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
		historyInfo = (Project)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(Project.class) );
		
		
		}
		catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
			
		}
		
		return historyInfo;
		
	}
	
	public void updateActiveProject(CommonError commonError, User loginUser, Project p, String y) {
		// TODO Auto-generated method stub
		logger.info(" ==> Project Status Update Active/Inactive START <== ");
		logger.info("Project Id ==> " + p.getId() );
		try{
			if(y.equals(CommonConstants.Y))
				insertProjectHistory( commonError, loginUser,p.getId(), CommonConstants.UNLOCKED);
			else
				insertProjectHistory( commonError, loginUser,p.getId(), CommonConstants.LOCKED);		
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_PROJ );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, p.getId()} );
		}catch( Exception e ){
			logger.info("Error when Project Status Update Active/Inactive of a Project.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Project Status Update Active/Inactive END <== ");	
		
	}

	public void deleteProject(CommonError commonError, User loginUser, Project p, String n) {
		logger.info(" ==> Delete Project START <== ");
		logger.info("Project Id ==> " + p.getId() );
		try{
			insertProjectHistory( commonError, loginUser,p.getId(),CommonConstants.DELETED);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_PROJ);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{p.getId()} );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Project list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Project END <== ");
		
		
	}
	
	public void insertProjectHistory(CommonError commonError, User loginUser,String id, String action) throws Exception {
		logger.info(" ==> Insert Project History START <== ");
		logger.info("Project Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : TaskPlannerDBConstants.PROJECT.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.PROJECT.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_PROJ);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.PROJECT.get("ID") );newQryHist.append( "=?" );
		Project p = (Project)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(Project.class) );
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_PROJ_HIST);newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("IDENTIFIER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("PROJKEY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("TYPE") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("DIVISION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.PROJECT_HIST.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(p.getId());
			args.add(p.getName());args.add(p.getProjKey());
			args.add(p.getType());args.add(p.getDivision());
			args.add(loginUser.getUserId());args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(action);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert ProjectHistory.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Project History END <== ");
	}


}
