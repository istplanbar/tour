package com.orb.taskplanner.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.Division;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class DivisionsDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(DivisionsDAO.class);
	
	/**
	 * This method will return list of Division levels from DB
	 * @param commonError
	 * @return
	 */
	public List<Division> getDivisionList(CommonError commonError, String activeFlag ) {
		logger.info(" ==> Get DivisionList START <== ");
		List<Division> categoryList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.DIVISION.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_DIVI);newQry.append(" WHERE ACTIVE = ? ");
			categoryList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Division.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Division list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get DivisionList END <== ");
		return categoryList;
	}
	
	/**
	 * This method inserts new Division into DB
	 * @param DivisionLevelError
	 * @return s
	 */
	
	public void addDivision(CommonError commonError, User loginUser,Division s) {
		logger.info(" ==> Insert Division START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_DIVI );newQry.append("( ");newQry.append( TaskPlannerDBConstants.DIVISION.get("NAME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION.get("CREATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.DIVISION.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_BY") );newQry.append( "," );newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_TIME") );
		newQry.append( "," );newQry.append( TaskPlannerDBConstants.DIVISION.get("ACTIVE") );
		newQry.append(") VALUES(?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getName());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert Division.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert Division END <== ");
	}
	
	/**
	 * This method updates existing Division level in DB
	 * @param categoryName
	 * @return
	 */
	public void updateDivision(CommonError commonError, User loginUser, Division s) {
		logger.info(" ==> Update Division START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
		      		
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		insertDivisionHistory(commonError, loginUser, s.getId(),CommonConstants.UPDATED);
		newQry.append("UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_DIVI );newQry.append(" SET ");
		newQry.append( TaskPlannerDBConstants.DIVISION.get("NAME") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_BY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_TIME") );newQry.append( " = ?" );
		newQry.append(" WHERE ");newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getName());args.add(loginUser.getUserId());args.add(date);args.add(s.getId());
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Update Division.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==>  Update Division END <== ");

	}
	
	/**
	 * This method used to get a particular Division from DB
	 * @param categoryName
	 * @return
	 */
	public Division getDivision(CommonError commonError, String DivisionId) {
		logger.info(" ==> Get a Division START <== ");
		Division categoryName = null;
		Division lockedHist = null;
		Division unlockedHist = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("NAME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_DIVI);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append( " = ? " );
			categoryName = (Division)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{DivisionId}, new BeanPropertyRowMapper(Division.class) );
			
			lockedHist = getUserAction(commonError, categoryName.getId(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				/*java.util.Date lockedDate = CommonUtils.convertFromUTC(lockedHist.getModifiedTime());*/
				categoryName.setLockedBy( lockedHist.getUpdateBy());
				categoryName.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, categoryName.getId(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				/*java.util.Date unlockedDate = CommonUtils.convertFromUTC(unlockedHist.getModifiedTime());*/
				categoryName.setUnlockedBy( unlockedHist.getUpdateBy() );
				categoryName.setUnlockedTime( unlockedHist.getUpdateTime() );
			}
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Division list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Get a Division END <== ");
		return categoryName;
	}

	public void updateActiveDivision(CommonError commonError, User loginUser, Division s, String y) {
		// TODO Auto-generated method stub
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("Division Id ==> " + s.getId() );
		try{
			if(y.equals(CommonConstants.Y))
				insertDivisionHistory( commonError, loginUser,s.getId(), CommonConstants.UNLOCKED);
			else
				insertDivisionHistory( commonError, loginUser,s.getId(), CommonConstants.LOCKED);
            StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_DIVI );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when Division Status Update Active/Inactive of a Division.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Division Status Update Active/Inactive END <== ");	
		
	}

	public void deleteDivision(CommonError commonError, User loginUser, Division s, String n) {
		logger.info(" ==> Delete Division START <== ");
		logger.info("Division Id ==> " + s.getId() );
		try{
			insertDivisionHistory( commonError, loginUser,s.getId(),CommonConstants.DELETED);
			StringBuffer qry = new StringBuffer("DELETE "); qry.append(" FROM ");
			qry.append(TaskPlannerDBConstants.TBL_DIVI );qry.append(" WHERE ");
			qry.append( TaskPlannerDBConstants.DIVISION.get("ID") );qry.append( " = ? " );
			getJdbcTemplate().update( qry.toString(),new Object[]{s.getId()} );
			
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_DIVI);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{s.getId()} );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Division list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Delete Division END <== ");
		
		
	}
	
private void insertDivisionHistory(CommonError commonError, User loginUser, String id,String action) {
		
		logger.info(" ==> Insert DIVISIONHistory START <== ");
		logger.info("Course Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : TaskPlannerDBConstants.DIVISION.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.DIVISION.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_DIVI);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.DIVISION.get("ID") );newQryHist.append( "=?" );
		Division c = (Division)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(Division.class) );
		try{
			logger.info("DIVISION Id ==> " + id );
			StringBuffer qryHist = new StringBuffer("SELECT ");
			for( String column : TaskPlannerDBConstants.DIVISION.keySet() ){
				qryHist.append( TaskPlannerDBConstants.DIVISION.get(column) );qryHist.append( "," );
			}
			qryHist.setLength(qryHist.length()-1);
			qryHist.append(" FROM ");qryHist.append(TaskPlannerDBConstants.TBL_DIVI);qryHist.append(" WHERE ");qryHist.append( TaskPlannerDBConstants.DIVISION.get("ID") );qryHist.append( "=?" );
			Division s = (Division)getJdbcTemplate().queryForObject( qryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(Division.class) );
			
		java.sql.Timestamp date = new java.sql.Timestamp(System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_DIVI_HIST );newQry.append("( ");
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("IDENTIFIER") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("NAME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("CREATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("UPDATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("UPDATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.DIVISION_HIST.get("ACTION") );
		newQry.append(") VALUES(?, ?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getId());
		args.add(s.getName());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(action);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert DIVISION History.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> Insert DIVISION History END <== ");
	}


/**
 * Get user actions from table for particular table
 * @return
 */
private Division getUserAction(CommonError commonError,String identifier, String action){
	Division historyInfo = null; 
	String fullName = null;
	try{
	StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
	for( String column : TaskPlannerDBConstants.DIVISION_HIST.keySet() ){
		newQryHist.append( TaskPlannerDBConstants.DIVISION_HIST.get(column) );newQryHist.append( "," );
	}
	newQryHist.setLength(newQryHist.length()-1);
	newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_DIVI_HIST);newQryHist.append(" WHERE ");
	newQryHist.append( TaskPlannerDBConstants.DIVISION_HIST.get("IDENTIFIER") );
	newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
	historyInfo = (Division)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { identifier, action }, new BeanPropertyRowMapper(Division.class) );
	
	
	}catch( Exception e ){
		logger.error(" No user action made before ==> " + e.getMessage() );
		
	}
	
	return historyInfo;
	
}

public String genNextRowId(CommonError qmsError) {
	return getNextRowId( TaskPlannerDBConstants.TBL_DIVI, TaskPlannerDBConstants.TASK_DB_CONFIG.get("rowPrefix") );

}

public static Log getLogger() {
	return logger;
}

public static void setLogger(Log logger) {
	DivisionsDAO.logger = logger;
}
}
