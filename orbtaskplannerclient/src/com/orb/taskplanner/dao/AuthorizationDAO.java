package com.orb.taskplanner.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.Authorization;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)

public class AuthorizationDAO extends AbstractDAO{

	private static Log logger = LogFactory.getLog(AuthorizationDAO.class);
	
	/**
	 * This method will return list of Authorization levels from DB
	 * @param commonError
	 * @return
	 */
	public List<Authorization> getAuthorizationList(CommonError commonError, String activeFlag ) {
		logger.info(" ==> getAuthorizationList START <== ");
		List<Authorization> authorizationList = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("LEVEL") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("DIVISION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("AUTHUSER") );newQry.append( "," );			
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_AUTHORIZE);newQry.append(" WHERE ACTIVE = ? ");
			authorizationList = getJdbcTemplate().query( newQry.toString(),new String[]{activeFlag}, new BeanPropertyRowMapper(Authorization.class) );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Authorization list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getAuthorizationList END <== ");
		return authorizationList;
	}
	
	/**
	 * This method inserts new Authorization level into DB
	 * @param commonError
	 * @return s
	 */
	
	public void addAuthorization(CommonError commonError, User loginUser,Authorization s) {
		logger.info(" ==> insert START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		newQry.append("INSERT INTO ");
		newQry.append( TaskPlannerDBConstants.TBL_AUTHORIZE );newQry.append("( ");
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("LEVEL") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("DIVISION") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("AUTHUSER") );newQry.append( "," );	
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("CREATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("CREATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_BY") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_TIME") );newQry.append( "," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ACTIVE") );
		newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getLevel());args.add(s.getDivision());args.add(s.getAuthUser().toString());args.add(loginUser.getUserId());args.add(date);args.add(loginUser.getUserId());args.add(date);args.add(CommonConstants.Y);
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Insert Authorization.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> add END <== ");
	}
	
	/**
	 * This method updates existing Authorization level in DB
	 * @param AuthorizationLevel
	 * @return
	 */
	public void updateAuthorization(CommonError commonError, User loginUser, Authorization s) {
		logger.info(" ==> update START <== ");
		StringBuffer newQry = new StringBuffer();
		try{
			insertAuthorizationHistory( commonError, loginUser,s.getId(), CommonConstants.UPDATED );	
	        java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
		newQry.append("UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_AUTHORIZE );newQry.append(" SET ");
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("LEVEL") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("DIVISION") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("AUTHUSER") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_BY") );newQry.append( " = ?," );
		newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_TIME") );newQry.append( " = ?" );
		newQry.append(" WHERE ");newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append(" = ? ");
		List<Object> args = new ArrayList<Object>();
		args.add(s.getLevel());args.add(s.getDivision());args.add(s.getAuthUser().toString());args.add(loginUser.getUserId());args.add(date);args.add(s.getId());
		getJdbcTemplate().update( newQry.toString(),args.toArray() );
		
		}catch( Exception e ){
		logger.info("Error when Update Authorization.  " + e.getMessage());
		commonError.addError(CommonConstants.DB_ERROR);
	}
	logger.info(" ==> update END <== ");

	}
	
	/**
	 * This method used to get a particular Authorization level from DB
	 * @param AuthorizationLevel
	 * @return
	 */
	public Authorization getAuthorization(CommonError commonError, String authId) {
		logger.info(" ==> getAuthorizationLevel START <== ");
		Authorization AuthorizationLevel = null;
		Authorization lockedHist = null;
		Authorization unlockedHist = null;
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("LEVEL") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("DIVISION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("AUTHUSER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ACTIVE") );
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_AUTHORIZE);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append( " = ? " );
			AuthorizationLevel = (Authorization)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{authId}, new BeanPropertyRowMapper(Authorization.class) );
		
			lockedHist = getUserAction(commonError, AuthorizationLevel.getId(),CommonConstants.LOCKED);
			if(lockedHist !=  null){
				AuthorizationLevel.setLockedBy( lockedHist.getUpdateBy());
				//java.util.Date lockedDate = CommonUtils.convertFromUTC(loginUser.getTimeZone(),lockedHist.getModifiedTime());
				AuthorizationLevel.setLockedTime(  lockedHist.getUpdateTime() );
			}
			
			unlockedHist = getUserAction(commonError, AuthorizationLevel.getId(),CommonConstants.UNLOCKED);
			if(unlockedHist !=  null){
				//java.util.Date unlockedDate = CommonUtils.convertFromUTC(loginUser.getTimeZone(),unlockedHist.getModifiedTime());
				AuthorizationLevel.setUnlockedBy( unlockedHist.getUpdateBy() );
				AuthorizationLevel.setUnlockedTime( unlockedHist.getUpdateTime() );
			}
		
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Authorization list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> getAuthorizationlevel END <== ");
		return AuthorizationLevel;
	}
	
	private Authorization getUserAction(CommonError commonError,String id, String action){
		Authorization historyInfo = null;
		String fullName = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TaskPlannerDBConstants.AUTHORIZE_HIST.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_AUTHORIZE_HIST);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("ID") );
		newQryHist.append( "=? AND ACTION =? " );newQryHist.append(" ORDER BY ID DESC LIMIT 1 ");
		historyInfo = (Authorization)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Authorization.class) );
		
		
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
			//qmsError.addError(CommonConstants.DB_ERROR);
		}
		
		return historyInfo;
		
	}
	

	public void updateInActiveStatus(CommonError commonError, User loginUser, Authorization s, String n) {
		// TODO Auto-generated method stub
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("AuthorizationLevel Id ==> " + s.getId() );
		try{
					
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_AUTHORIZE );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ n, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Authorization.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");	
	}

	public void updateActiveStatus(CommonError commonError, User loginUser, Authorization s, String y) {
		// TODO Auto-generated method stub
		logger.info(" ==> updateActiveStatus START <== ");
		logger.info("AuthorizationLevel Id ==> " + s.getId() );
		try{
			if(y.equals(CommonConstants.Y))
				insertAuthorizationHistory( commonError, loginUser,s.getId(), CommonConstants.UNLOCKED);
			else
				insertAuthorizationHistory( commonError, loginUser,s.getId(), CommonConstants.LOCKED);
				
			StringBuffer newQry = new StringBuffer();
			newQry.append(" UPDATE ");newQry.append( TaskPlannerDBConstants.TBL_AUTHORIZE );newQry.append(" SET ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ACTIVE") );newQry.append(" = ? WHERE ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append(" = ? ");
			getJdbcTemplate().update( newQry.toString(),new String[]{ y, s.getId()} );
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Authorization.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");	
		
	}

	public void deleteAuthorization(CommonError commonError, User loginUser, Authorization s, String n) {
		logger.info(" ==> DELETE Authorization START <== ");
		logger.info("AuthorizationLevel Id ==> " + s.getId() );
		try{
			insertAuthorizationHistory( commonError, loginUser,s.getId(),CommonConstants.DELETED);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_AUTHORIZE);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQry.append( " = ? " );
			getJdbcTemplate().update( newQry.toString(),new Object[]{s.getId()} );
			}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Authorization list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> DELETE Authorization END <== ");
		
		
	}
	
	public void insertAuthorizationHistory(CommonError courseError, User loginUser,String id, String action) throws Exception {
		logger.info(" ==> Insert Material History START <== ");
		logger.info("AuthorizationLevel Id ==> " + id );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer("SELECT ");
		for( String column : TaskPlannerDBConstants.AUTHORIZE.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.AUTHORIZE.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_AUTHORIZE);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.AUTHORIZE.get("ID") );newQryHist.append( "=?" );
		Authorization s = (Authorization)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id }, new BeanPropertyRowMapper(Authorization.class) );
		
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_AUTHORIZE_HIST);newQry.append("( ");
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("LEVEL") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("DIVISION") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("AUTHUSER") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("CREATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("CREATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("UPDATE_BY") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("UPDATE_TIME") );newQry.append( "," );
			newQry.append( TaskPlannerDBConstants.AUTHORIZE_HIST.get("ACTION") );
			newQry.append(") VALUES(?,?,?,?,?,?,?,?) ");
			List<Object> args = new ArrayList<Object>();
			args.add(s.getLevel());args.add(s.getDivision());
			args.add(s.getAuthUser().toString());
			args.add(loginUser.getUserId());args.add(date);
			args.add(loginUser.getUserId());args.add(date);
			args.add(action);
			getJdbcTemplate().update( newQry.toString(),args.toArray() );

		}catch( Exception e ){
			logger.info("Error when Insert Authorization History.  " + e.getMessage());
			courseError.addError(CommonConstants.DB_ERROR);
		}
		logger.info(" ==> Insert Authorization History END <== ");
	}


}
