package com.orb.taskplanner.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.AbstractDAO;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.model.User;
import com.orb.taskplanner.bean.Disposals;
import com.orb.taskplanner.constants.TaskPlannerDBConstants;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;

@Repository
@Scope ( BeanDefinition.SCOPE_PROTOTYPE)
public class DisposalsDAO extends AbstractDAO{
	private static Log logger = LogFactory.getLog(DisposalsDAO.class);

	/**
     * This method will return list of Disposals from DB
     * @param commonError
     * @param activeFlag
     * @return
     */
	public List<Disposals> getDisposalsList(CommonError commonError, String activeFlag) {
		logger.info(" ==> getDisposalsList START <== ");
		List<Disposals> disposalsList = null;
		try{
			StringBuffer qry = new StringBuffer("SELECT ");
			 for( String column : TaskPlannerDBConstants.DISPOSALS.keySet() ){
				   qry.append( TaskPlannerDBConstants.DISPOSALS.get(column) );qry.append( "," );
				}
			   qry.setLength(qry.length()-1);
			qry.append(" FROM ");qry.append(TaskPlannerDBConstants.TBL_DISPOSALS);qry.append(" WHERE ");
			qry.append(TaskPlannerDBConstants.DISPOSALS.get("ACTIVE"));qry.append("=? ");
			disposalsList = getJdbcTemplate().query( qry.toString(),new String[]{activeFlag},new BeanPropertyRowMapper(Disposals.class) );
			}
		catch( Exception e ){
			logger.error(" Exception occured when tries to fetch disposals list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETLIST_ERROR);
		}
		logger.info(" ==> getDisposalsList END <== ");
		return disposalsList;
	}

	/**
	 * This method used to get a particular Disposals from DB
	 * @param commonError
	 * @param disposalsId
	 * @return disposals
	 */
	public Disposals getDisposals(CommonError commonError, String disposalsId) {
		logger.info(" ==> Get a Disposals START <== ");
		Disposals disposals = null;
		Disposals lockedHist = null;
		Disposals unlockedHist = null; 
		try{
			StringBuffer newQry = new StringBuffer("SELECT ");
			for( String column : TaskPlannerDBConstants.DISPOSALS.keySet() ){
				newQry.append( TaskPlannerDBConstants.DISPOSALS.get(column) );newQry.append( "," );
				}
			newQry.setLength(newQry.length()-1);
			newQry.append(" FROM ");newQry.append(TaskPlannerDBConstants.TBL_DISPOSALS);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.DISPOSALS.get("IDENTIFIER") );newQry.append( "=? ");
			disposals = (Disposals)getJdbcTemplate().queryForObject( newQry.toString(),new String[]{disposalsId}, new BeanPropertyRowMapper(Disposals.class) );
			
		lockedHist = getUserAction(commonError, disposals.getIdentifier(),CommonConstants.LOCKED);
		if(lockedHist !=  null){
			disposals.setLockedBy( lockedHist.getUpdateBy());
			disposals.setLockedTime(  lockedHist.getUpdateTime());
		}
		
		unlockedHist = getUserAction(commonError, disposals.getIdentifier(),CommonConstants.UNLOCKED);
		if(unlockedHist !=  null){
			disposals.setUnlockedBy( unlockedHist.getUpdateBy());
			disposals.setUnlockedTime( unlockedHist.getUpdateTime());
		}
	  }catch( Exception e ){
			logger.error(" Exception occured when tries to fetch disposals. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_GETRECORD_ERROR);
	  }
			logger.info(" ==> Get a Disposals END <== ");
		return disposals;
	}

	
	/**
	 * Get user actions from table for particular table
	 * @param commonError
	 * @param id
	 * @param action
	 * @return historyInfo
	 */
    private Disposals getUserAction(CommonError commonError,String id, String action){
    	Disposals historyInfo = null;
		try{
		StringBuffer newQryHist = new StringBuffer("SELECT DISTINCT ");
		for( String column : TaskPlannerDBConstants.DISPOSALS_HIST.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.DISPOSALS_HIST.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_DISPOSALS_HIST);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.DISPOSALS.get("IDENTIFIER") );
		newQryHist.append( "=? AND ");
		newQryHist.append(TaskPlannerDBConstants.DISPOSALS_HIST.get("ACTION"));
		newQryHist.append("=? ");
		newQryHist.append(" ORDER BY ");
		newQryHist.append(TaskPlannerDBConstants.DISPOSALS_HIST.get("ID"));
		newQryHist.append(" DESC LIMIT 1 ");
		historyInfo = (Disposals)getJdbcTemplate().queryForObject( newQryHist.toString(), new String[] { id, action }, new BeanPropertyRowMapper(Disposals.class) );
		}catch( Exception e ){
			logger.error(" No user action made before ==> " + e.getMessage() );
		}
		return historyInfo;
	}
    
    /**
	 * This method will activate or deactivate the Disposals
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 * @param s
	 */
	public void updateActiveStatus(CommonError commonError, User loginUser, List<String> idList, String s) {
		logger.info(" ==> updateActiveStatus START <== ");
		try{
			if(CommonConstants.Y.equals(s)){
				insertDisposalsHistory( commonError, loginUser,idList, CommonConstants.UNLOCKED);
			}
			else{
				insertDisposalsHistory( commonError, loginUser,idList, CommonConstants.LOCKED);
			}
			StringBuffer qry = new StringBuffer();
			qry.append(" UPDATE ");qry.append( TaskPlannerDBConstants.TBL_DISPOSALS);qry.append(" SET ");
			qry.append( TaskPlannerDBConstants.DISPOSALS.get("ACTIVE") );qry.append("=? ");
			qry.append(" WHERE ");
			qry.append( TaskPlannerDBConstants.DISPOSALS.get("ID") );qry.append("=? ");
			getJdbcTemplate().batchUpdate(qry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ps.setString(1, s);
					ps.setString(2, idList.get(i));
				}
				
				public int getBatchSize() {
					return idList.size();
				}
			});
		}catch( Exception e ){
			logger.info("Error when updateActiveStatus of a Disposals." + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==> updateActiveStatus END <== ");
		
		
		
	}

	/**
	 * This method delete the Disposals into DB
	 * @param commonError
	 * @param loginUser
	 * @param idList
	 */
	public void deleteDisposals(CommonError commonError, User loginUser, List<String> idList) {
		logger.info(" ==> Delete Disposals START <== ");
		NamedParameterJdbcTemplate db = new NamedParameterJdbcTemplate(jdbcTemplate);
		try{
			insertDisposalsHistory( commonError, loginUser,idList,CommonConstants.DELETED);		
			Map<String, Object> params = new HashMap<>();
	        params.put("ids", idList);
			StringBuffer newQry = new StringBuffer("DELETE "); newQry.append(" FROM ");
			newQry.append(TaskPlannerDBConstants.TBL_DISPOSALS);newQry.append(" WHERE ");
			newQry.append( TaskPlannerDBConstants.DISPOSALS.get("ID") );newQry.append( " IN " );
			newQry.append(" (:ids) ");
			db.update(newQry.toString(),params);
		}catch( Exception e ){
			logger.error(" Exception occured when tries to fetch Disposals list. Exception ==> " + e.getMessage() );
			commonError.addError(CommonConstants.DB_DELETE_ERROR);
		}
		logger.info(" ==> Delete Disposals END <== ");
		
	}

	/**
	 * This method inserts new Disposals into DB
	 * @param commonError
	 * @param loginUser
	 * @param disposals
	 */
	public void addDisposals(CommonError commonError, User loginUser, Disposals disposals) {
		logger.info(" ==> Insert Disposals START <== ");
		try{
			insertRecord(loginUser, disposals,TaskPlannerDBConstants.TBL_DISPOSALS,TaskPlannerDBConstants.DISPOSALS_DB_CONFIG.get("rowPrefix"));  
		}catch( Exception e ){
			logger.info("Error when Insert Disposals.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Disposals END <== ");
		
	}

	 /**
     * This method updates existing Disposals in DB
     * @param commonError
     * @param loginUser
     * @param software
     */
	public void updateDisposals(CommonError commonError, User loginUser, Disposals disposals) {
		logger.info(" ==> Update Disposals START <== ");
		try{
			updateRecordById(loginUser, disposals,TaskPlannerDBConstants.TBL_DISPOSALS); 
		}catch( Exception e ){ 
			logger.info("Error when Update Disposals.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_UPDATE_ERROR);
		}
		logger.info(" ==>  Update Disposals END <== ");
		
	}
	
	/**
     * This method inserts updated Disposals into Disposals history table
     * @param commonError
     * @param loginUser
     * @param idList
     * @param action
     * @return 
     */
   public void insertDisposalsHistory(CommonError commonError, User loginUser,List<String> idList, String action) throws Exception {
		logger.info(" ==> Insert Disposals History START <== ");
		logger.info("Disposals Id ==> " + idList );
		StringBuffer newQry = new StringBuffer();
		StringBuffer newQryHist = new StringBuffer();
		newQryHist.append("SELECT ");
		for( String column : TaskPlannerDBConstants.DISPOSALS.keySet() ){
			newQryHist.append( TaskPlannerDBConstants.DISPOSALS.get(column) );newQryHist.append( "," );
		}
		newQryHist.setLength(newQryHist.length()-1);
		newQryHist.append(" FROM ");newQryHist.append(TaskPlannerDBConstants.TBL_DISPOSALS);newQryHist.append(" WHERE ");newQryHist.append( TaskPlannerDBConstants.DISPOSALS.get("ID") );
		newQryHist.append(" IN ( ");
		for ( int i=0; i<idList.size(); i++ ) {
			newQryHist.append(" ? ");newQryHist.append(",");
		}
		newQryHist.setLength(newQryHist.length() - 1);
		newQryHist.append(") ");
		List<Disposals> disposalsList = (List<Disposals>)getJdbcTemplate().query(newQryHist.toString(), idList.toArray(),  new BeanPropertyRowMapper<Disposals>(Disposals.class));
		try{
			java.sql.Timestamp  date = new java.sql.Timestamp (System.currentTimeMillis());
			newQry.append("INSERT INTO ");newQry.append( TaskPlannerDBConstants.TBL_DISPOSALS_HIST);newQry.append("( ");
			for( String column : TaskPlannerDBConstants.DISPOSALS_HIST.keySet() ){
				if(!column.equals(TaskPlannerDBConstants.DISPOSALS_HIST.get("ID"))){
				newQry.append( TaskPlannerDBConstants.DISPOSALS_HIST.get(column) );newQry.append( "," );
				}
			}
			newQry.setLength(newQry.length()-1);
			newQry.append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			getJdbcTemplate().batchUpdate(newQry.toString(), new BatchPreparedStatementSetter() {

				public void setValues(PreparedStatement ps, int i) throws SQLException {	
					ps.setString(1, disposalsList.get(i).getIdentifier());
					ps.setString(2, disposalsList.get(i).getCustomerNumber());ps.setString(3, disposalsList.get(i).getSystemNumber());
					ps.setString(4, disposalsList.get(i).getFirstName());ps.setString(5, disposalsList.get(i).getObject());
					ps.setString(6, disposalsList.get(i).getRoadObject());ps.setString(7, disposalsList.get(i).getPostcodeObject());
					ps.setString(8, disposalsList.get(i).getDistrictObject());ps.setString(9, disposalsList.get(i).getKw());
					ps.setString(10, disposalsList.get(i).getIntervalInKws());ps.setString(11, disposalsList.get(i).getIntervals());
					ps.setObject(12, disposalsList.get(i).getDateIn2012());ps.setString(13, disposalsList.get(i).getDateIn2013());
					ps.setString(14, disposalsList.get(i).getDateIn2014());ps.setString(15, disposalsList.get(i).getDateIn2015());
					ps.setString(16, disposalsList.get(i).getDateIn2016());ps.setString(17, disposalsList.get(i).getDateIn2017());
					ps.setString(18, disposalsList.get(i).getTee());ps.setString(19, disposalsList.get(i).getAmount());
					ps.setString(20, disposalsList.get(i).getWaterSupply());ps.setString(21, disposalsList.get(i).getHoseLength());
					ps.setString(22, disposalsList.get(i).getVehicle());ps.setString(23, disposalsList.get(i).getRemarksExpression());
					ps.setString(24, disposalsList.get(i).getAdditionalInformation());ps.setString(25, disposalsList.get(i).getInstallationSa());
					ps.setString(26, disposalsList.get(i).getTender());ps.setString(27, disposalsList.get(i).getFirstNamePost());
					ps.setString(28, disposalsList.get(i).getNamePost());ps.setString(29, disposalsList.get(i).getStreetPost());
					ps.setString(30, disposalsList.get(i).getPostCode());ps.setString(31, disposalsList.get(i).getPlacePost());
					ps.setString(32, disposalsList.get(i).getIntervalInMeet());ps.setString(33, disposalsList.get(i).getDatePrintPostcard());
					ps.setString(34, loginUser.getUserId());
					ps.setTimestamp(35, date);ps.setString(36, loginUser.getUserId());
					ps.setTimestamp(37, date);ps.setString(38, action);
					
				}		
				public int getBatchSize() {
					return disposalsList.size();
				}
			});
			
		}catch( Exception e ){
			logger.info("Error when Insert Disposals History.  " + e.getMessage());
			commonError.addError(CommonConstants.DB_INSERT_ERROR);
		}
		logger.info(" ==> Insert Disposals History END <== ");
	}


}
