package com.orb.taskplanner.bean;

import java.util.Date;
import java.util.List;

public class Task {
	
	private String id;
	private String identifier;
	private String name;
	private String type;
	private String project;
	private String projKey;
	private String category;
	private String priority;
	private String status;
	private String deputy;
	private String progress;
	private Date startDate;
	private Date dueDate;
	private String description;
	private String assignTo;
	private String reporter;
	private String attachment;
	private String dirName;
	private String createBy;
	private Date createTime; 
	private String updateBy;
	private Date updateTime;
	private String active;
	private boolean checked;
	private List<String> attachmentList;
	private String watchStatus;
	private String qmsDoc;
	private String qmsFile;
	private String parentTask;
	private List<String> subTaskList;
	private List<Task> subTasks;
	private String relation;
	private String participantName;
	private String courseId;
	private String crmName;
	private String facilities;
	
	private String estimatedTime;
	
	public String getEstimatedTime() {
		return estimatedTime;
	}
	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}
	
	public String getDeputy() {
		return deputy;
	}
	public void setDeputy(String deputy) {
		this.deputy = deputy;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getAssignTo() {
		return assignTo;
	}
	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getReporter() {
		return reporter;
	}
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}
	public String getProjKey() {
		return projKey;
	}
	public void setProjKey(String projKey) {
		this.projKey = projKey;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public List<String> getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(List<String> attachmentList) {
		this.attachmentList = attachmentList;
	}
	public String getDirName() {
		return dirName;
	}
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}
	public String getQmsDoc() {
		return qmsDoc;
	}
	public void setQmsDoc(String qmsDoc) {
		this.qmsDoc = qmsDoc;
	}
	public String getQmsFile() {
		return qmsFile;
	}
	public void setQmsFile(String qmsFile) {
		this.qmsFile = qmsFile;
	}
	public String getParentTask() {
		return parentTask;
	}
	public void setParentTask(String parentTask) {
		this.parentTask = parentTask;
	}
	public List<String> getSubTaskList() {
		return subTaskList;
	}
	public void setSubTaskList(List<String> subTaskList) {
		this.subTaskList = subTaskList;
	}
	public List<Task> getSubTasks() {
		return subTasks;
	}
	public void setSubTasks(List<Task> subTasks) {
		this.subTasks = subTasks;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getWatchStatus() {
		return watchStatus;
	}
	public void setWatchStatus(String watchStatus) {
		this.watchStatus = watchStatus;
	}
	public String getParticipantName() {
		return participantName;
	}
	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCrmName() {
		return crmName;
	}
	public void setCrmName(String crmName) {
		this.crmName = crmName;
	}
	public String getFacilities() {
		return facilities;
	}
	public void setFacilities(String facilities) {
		this.facilities = facilities;
	}
			
}
