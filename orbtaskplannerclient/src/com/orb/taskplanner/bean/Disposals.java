package com.orb.taskplanner.bean;

import java.util.Date;

public class Disposals {
	private String id;
	private String identifier;
	private String customerNumber;
	private String systemNumber;
	private String firstName;
	private String object;
	private String roadObject;
	private String postcodeObject;
	private String districtObject;
	private String kw;
	private String intervalInKws;
	private String intervals;
	private String dateIn2012;
	private String dateIn2013;
	private String dateIn2014;
	private String dateIn2015;
	private String dateIn2016;
	private String dateIn2017;
	public String getDateIn2012() {
		return dateIn2012;
	}
	public void setDateIn2012(String dateIn2012) {
		this.dateIn2012 = dateIn2012;
	}
	public String getDateIn2013() {
		return dateIn2013;
	}
	public void setDateIn2013(String dateIn2013) {
		this.dateIn2013 = dateIn2013;
	}
	public String getDateIn2014() {
		return dateIn2014;
	}
	public void setDateIn2014(String dateIn2014) {
		this.dateIn2014 = dateIn2014;
	}
	public String getDateIn2015() {
		return dateIn2015;
	}
	public void setDateIn2015(String dateIn2015) {
		this.dateIn2015 = dateIn2015;
	}
	public String getDateIn2016() {
		return dateIn2016;
	}
	public void setDateIn2016(String dateIn2016) {
		this.dateIn2016 = dateIn2016;
	}
	public String getDateIn2017() {
		return dateIn2017;
	}
	public void setDateIn2017(String dateIn2017) {
		this.dateIn2017 = dateIn2017;
	}
	private String tee;
	private String amount;
	private String waterSupply;
	private String hoseLength;
	private String vehicle;
	private String remarksExpression;
	private String additionalInformation;
	private String installationSa;
	private String tender;
	private String firstNamePost;
	private String namePost;
	private String streetPost;
	private String postCode;
	private String placePost;
	private String intervalInMeet;
	private String datePrintPostcard;
	private String createBy;
	private Date createTime;
	private String updateBy;
	private Date updateTime;
	private String active;
	private String checked;
	private String lockedBy;
	private Date lockedTime;
	private String unlockedBy;
	private Date unlockedTime;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getSystemNumber() {
		return systemNumber;
	}
	public void setSystemNumber(String systemNumber) {
		this.systemNumber = systemNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getRoadObject() {
		return roadObject;
	}
	public void setRoadObject(String roadObject) {
		this.roadObject = roadObject;
	}
	public String getPostcodeObject() {
		return postcodeObject;
	}
	public void setPostcodeObject(String postcodeObject) {
		this.postcodeObject = postcodeObject;
	}
	public String getDistrictObject() {
		return districtObject;
	}
	public void setDistrictObject(String districtObject) {
		this.districtObject = districtObject;
	}
	public String getKw() {
		return kw;
	}
	public void setKw(String kw) {
		this.kw = kw;
	}
	public String getIntervalInKws() {
		return intervalInKws;
	}
	public void setIntervalInKws(String intervalInKws) {
		this.intervalInKws = intervalInKws;
	}
	public String getIntervals() {
		return intervals;
	}
	public void setIntervals(String intervals) {
		this.intervals = intervals;
	}
	public String getTee() {
		return tee;
	}
	public void setTee(String tee) {
		this.tee = tee;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getWaterSupply() {
		return waterSupply;
	}
	public void setWaterSupply(String waterSupply) {
		this.waterSupply = waterSupply;
	}
	public String getHoseLength() {
		return hoseLength;
	}
	public void setHoseLength(String hoseLength) {
		this.hoseLength = hoseLength;
	}
	public String getVehicle() {
		return vehicle;
	}
	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}
	public String getRemarksExpression() {
		return remarksExpression;
	}
	public void setRemarksExpression(String remarksExpression) {
		this.remarksExpression = remarksExpression;
	}
	public String getAdditionalInformation() {
		return additionalInformation;
	}
	public void setAdditionalInformation(String additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
	public String getInstallationSa() {
		return installationSa;
	}
	public void setInstallationSa(String installationSa) {
		this.installationSa = installationSa;
	}
	public String getTender() {
		return tender;
	}
	public void setTender(String tender) {
		this.tender = tender;
	}
	public String getFirstNamePost() {
		return firstNamePost;
	}
	public void setFirstNamePost(String firstNamePost) {
		this.firstNamePost = firstNamePost;
	}
	public String getNamePost() {
		return namePost;
	}
	public void setNamePost(String namePost) {
		this.namePost = namePost;
	}
	public String getStreetPost() {
		return streetPost;
	}
	public void setStreetPost(String streetPost) {
		this.streetPost = streetPost;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getPlacePost() {
		return placePost;
	}
	public void setPlacePost(String placePost) {
		this.placePost = placePost;
	}
	public String getIntervalInMeet() {
		return intervalInMeet;
	}
	public void setIntervalInMeet(String intervalInMeet) {
		this.intervalInMeet = intervalInMeet;
	}
	public String getDatePrintPostcard() {
		return datePrintPostcard;
	}
	public void setDatePrintPostcard(String datePrintPostcard) {
		this.datePrintPostcard = datePrintPostcard;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}
	public Date getLockedTime() {
		return lockedTime;
	}
	public void setLockedTime(Date lockedTime) {
		this.lockedTime = lockedTime;
	}
	public String getUnlockedBy() {
		return unlockedBy;
	}
	public void setUnlockedBy(String unlockedBy) {
		this.unlockedBy = unlockedBy;
	}
	public Date getUnlockedTime() {
		return unlockedTime;
	}
	public void setUnlockedTime(Date unlockedTime) {
		this.unlockedTime = unlockedTime;
	}
	

}
