package com.orb.taskplanner.constants;

import java.util.Map;
import com.orb.common.client.utils.CommonUtils;
public class TaskPlannerDBConstants {
	
	public static final String TBL_TASK,TBL_TASK_HIST;
	public static final String TBL_CATY,TBL_CATY_HIST;
	public static final String TBL_DIVI, TBL_DIVI_HIST;
	public static final String TBL_AUTHORIZE,TBL_AUTHORIZE_HIST;
	public static final String TBL_PROJ,TBL_PROJ_HIST;
	public static final String TBL_TYPE,TBL_TYPE_HIST;
	public static final String TBL_WATCH_LIST;
	
	
	public static final Map<String,String> TASK_DB_CONFIG;
	public static final Map<String,String> TASK,TASK_HIST;
	public static final Map<String,String> AUTHORIZE,AUTHORIZE_HIST;
	public static final Map<String,String> DIVISION , DIVISION_HIST;
	public static final Map<String,String> CATEGORY,CATEGORY_HIST;
	public static final Map<String,String> PROJECT,PROJECT_HIST;
	public static final Map<String,String> TASKTYPE,TASKTYPE_HIST;
	public static final Map<String,String> TASK_WATCH_LIST;
	
	//Disposals
	public static final Map<String,String> DISPOSALS_DB_CONFIG;
	public static final String TBL_DISPOSALS,TBL_DISPOSALS_HIST;
	public static final Map<String,String> DISPOSALS,DISPOSALS_HIST;
	
	
	
	
	static{
		TBL_TASK = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "MOD_TASKPLANNER_TASKS");
		TBL_TASK_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "MOD_TASKPLANNER_TASKS_HISTORY");
		
		TBL_PROJ = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "PROJ_TABLE");
		TBL_PROJ_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "PROJ_HIST_TABLE");
		
		TBL_CATY = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "CATY_TABLE");
		TBL_CATY_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "CATY_HIST_TABLE");
		
		TBL_DIVI = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "DIV_TABLE");
		TBL_DIVI_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "DIV_HIST_TABLE");
		
		TBL_TYPE = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "TYPE_TABLE");
		TBL_TYPE_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "TYPE_HIST_TABLE");
		
		TBL_AUTHORIZE = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "AUTH_TABLE");
		TBL_AUTHORIZE_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "AUTH_HIST_TABLE");
		
		TBL_WATCH_LIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "TASK_WATCH_LIST_TBL");
		TASK_WATCH_LIST = (Map<String,String>)CommonUtils.getBean("TASK_WATCH_LIST_TBL");
		
		TASK_DB_CONFIG = (Map<String,String>)CommonUtils.getBean("taskplannerDBConfig");
		TASK = (Map<String,String>)CommonUtils.getBean("MOD_TASKPLANNER_TASKS");
		TASK_HIST = (Map<String,String>)CommonUtils.getBean("MOD_TASKPLANNER_TASKS_HISTORY");
		PROJECT = (Map<String,String>)CommonUtils.getBean("PROJ_TABLE");
		PROJECT_HIST = (Map<String,String>)CommonUtils.getBean("PROJ_HIST_TABLE");
		CATEGORY = (Map<String,String>)CommonUtils.getBean("CATY_TABLE");
		CATEGORY_HIST = (Map<String,String>)CommonUtils.getBean("CATY_HIST_TABLE");
		DIVISION = (Map<String,String>)CommonUtils.getBean("DIV_TABLE");
		DIVISION_HIST = (Map<String,String>)CommonUtils.getBean("DIV_HIST_TABLE");
		TASKTYPE = (Map<String,String>)CommonUtils.getBean("TYPE_TABLE");
		TASKTYPE_HIST = (Map<String,String>)CommonUtils.getBean("TYPE_HIST_TABLE");
		AUTHORIZE = (Map<String,String>)CommonUtils.getBean("AUTH_TABLE");
		AUTHORIZE_HIST = (Map<String,String>)CommonUtils.getBean("AUTH_HIST_TABLE");
		
		//Disposals
		DISPOSALS_DB_CONFIG = (Map<String,String>)CommonUtils.getBean("disposalsDBConfig");
		TBL_DISPOSALS = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "MOD_DISPOSALS");
		TBL_DISPOSALS_HIST = CommonUtils.getPropertyValue("TASKS_TABLE_NAMES", "MOD_DISPOSALS_HISTORY");
		DISPOSALS = (Map<String,String>)CommonUtils.getBean("MOD_DISPOSALS");
		DISPOSALS_HIST = (Map<String,String>)CommonUtils.getBean("MOD_DISPOSALS_HISTORY");
	}
}
