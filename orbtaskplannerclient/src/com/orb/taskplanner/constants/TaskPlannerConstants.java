package com.orb.taskplanner.constants;

import java.util.Map;

import com.orb.common.client.utils.CommonUtils;

public class TaskPlannerConstants {
	public final static String DASHBOARD_PAGE = "/common/process/dashboard/loadDashboard.html";
	public final static String TASK_LIST_PAGE = "/common/process/taskplanner/loadTaskPlanner.html";
	public final static String TASK_VIEW_PAGE = "/common/process/taskplanner/viewTaskPlanner.html";
	public final static String TASK_ADDUPDATE_PAGE = "/common/process/taskplanner/editTaskPlanner.html";
	public final static String TASK_LIST_HTML = "taskplanner/task/taskList.xhtml";
	public final static String TASK_EDIT_HTML = "taskplanner/task/addUpdateTask.xhtml";
	public final static String TASK_VIEW_HTML = "taskplanner/task/taskView.xhtml";
	public final static String NEXT_DIR_NAME = "task-dir-";
	public final static String TASK_CLONE_SUCCESS="task_clone_success";
	public final static String TASK_ADDUPDATE_CLONE_PAGE = "/common/process/taskplanner/cloneTaskPlanner.html";
	public final static String SELECTED_CLONE_TASK_ID = "SELECTED_CLONE_TASK_ID";
	public final static String CLONE_ADDED_SUCCESS="clone_added_success";
	public final static String TASK_LIST_VIEW = "LV";
	public final static String TASK_DETAIL_VIEW = "DV";
	public final static String DEFAULT_VIEW = "view" ;
	public final static String TOUR_TASKS_STATUS = "inProgress";
	
	public final static String SELECTED_TASK_ID = "SELECTED_TASK_ID";
	public final static String SELECTED_CATEGORY_ID = "SELECTED_CATEGORY_ID";
	public final static String SELECTED_DIVISION_ID = "SELECTED_DIVISION_ID";
	public final static String SELECTED_AUTHORIZATION_ID = "SELECTED_AUTHORIZATION_ID";
	public final static String SELECTED_PROJECT_ID = "SELECTED_PROJECT_ID";
	public final static String SELECTED_TYPE_ID = "SELECTED_TYPE_ID";
	public final static String NEW_TASK_DIALOG = "NEW_TASK_DIALOG" ;
	public final static String LOGGEDIN_USER_TASKS = "LOGGEDIN_USER_TASKS" ;
	public final static String TOGGLER_LIST = "TOGGLER_LIST" ;
	public final static String PRIORITY = "normal";
	
	public static final Map<String,String> SUPPORTED_FORMAT;
	public final static String PROJECT_TYPE = "1";
	public final static String PROJECT_VIEW_PAGE="/common/process/taskplanner/viewProject.html";
	public final static String PROJECT_EDIT_PAGE="/common/process/taskplanner/addUpdateProject.html";
	public final static String PROJECT_LIST_PAGE="/common/process/taskplanner/loadProject.html";
	public final static String DIVISION_VIEW_PAGE="/common/process/taskplanner/viewDivision.html";
	public final static String DIVISION_EDIT_PAGE="/common/process/taskplanner/addUpdateDivision.html";
	public final static String DIVISION_LIST_PAGE="/common/process/taskplanner/loadDivision.html";
	public final static String TYPE_VIEW_PAGE="/common/process/taskplanner/viewTaskType.html";
	public final static String TYPE_EDIT_PAGE="/common/process/taskplanner/addUpdateTaskType.html";
	public final static String TYPE_LIST_PAGE="/common/process/taskplanner/loadTaskType.html";
	public final static String AUTHORIZATION_VIEW_PAGE="/common/process/taskplanner/viewAuthorization.html";
	public final static String AUTHORIZATION_EDIT_PAGE="/common/process/taskplanner/addUpdateAuthorization.html";
	public final static String AUTHORIZATION_LIST_PAGE="/common/process/taskplanner/loadAuthorizationLevel.html";
	public final static String CATEGORY_VIEW_PAGE="/common/process/taskplanner/viewCategory.html";
	public final static String CATEGORY_EDIT_PAGE="/common/process/taskplanner/addUpdateCategory.html";
	public final static String CATEGORY_LIST_PAGE="/common/process/taskplanner/loadTaskCategory.html";
	
	public final static String PROJECT_LIST_HTML="taskplanner/project/list.xhtml";
	public final static String PROJECT_VIEW_HTML="taskplanner/project/view.xhtml";
	public final static String PROJECT_EDIT_HTML="taskplanner/project/addUpdate.xhtml";
	public final static String CATEGORY_LIST_HTML="taskplanner/category/list.xhtml";
	public final static String CATEGORY_VIEW_HTML="taskplanner/category/view.xhtml";
	public final static String CATEGORY_EDIT_HTML="taskplanner/category/addUpdate.xhtml";
	public final static String DIVISION_LIST_HTML="taskplanner/division/list.xhtml";
	public final static String DIVISION_VIEW_HTML="taskplanner/division/view.xhtml";
	public final static String DIVISION_EDIT_HTML="taskplanner/division/addUpdate.xhtml";
	public final static String AUTHORIZATION_LIST_HTML="taskplanner/authorization/list.xhtml";
	public final static String AUTHORIZATION_VIEW_HTML="taskplanner/authorization/view.xhtml";
	public final static String AUTHORIZATION_EDIT_HTML="taskplanner/authorization/addUpdate.xhtml";
	public static final String TASK_DELETE_SUCCESS = "task_delete_success";
	public static final String TASK_ADDED_SUCCESS = "task_added_success";
	public static final String TASK_UPDATED_SUCCESS = "task_update_success";
	public final static String TYPE_LIST_HTML="taskplanner/type/list.xhtml";
	public final static String TYPE_VIEW_HTML="taskplanner/type/view.xhtml";
	public final static String TYPE_EDIT_HTML="taskplanner/type/addUpdate.xhtml";
	public static final String TYPE_DELETE_SUCCESS = "type_delete_success";
	public static final String TYPE_ADDED_SUCCESS = "type_added_success";
	public static final String TYPE_UPDATED_SUCCESS = "type_update_success";
	public static final String TYPE_LOCK_SUCCESS = "type_lock_success";
	public static final String TYPE_UNLOCK_SUCCESS = "type_unlock_success";
	
	
	//Disposals
	public final static String SELECTED_DISPOSALS_ID = "SELECTED_DISPOSALS_ID";
	public final static String DISPOSALS_LIST_PAGE = "/common/process/taskplanner/loadDisposals.html";
	public final static String DISPOSALS_EDIT_PAGE = "/common/process/taskplanner/manipulateDisposals.html";
	public final static String DISPOSALS_LIST_HTML = "taskplanner/disposals/list.xhtml";
	public final static String DISPOSALS_TOGGLER_LIST = "disposals_toggler_list" ;
	public final static String DISPOSALS_DELETE_SUCCESS = "disposals_delete_success";
	public final static String DISPOSALS_LOCKED_SUCCESS = "disposals_locked_success";
	public final static String DISPOSALS_UNLOCKED_SUCCESS = "disposals_unlocked_success";
	public final static String DISPOSALS_VIEW_PAGE = "/common/process/taskplanner/viewDisposals.html";
	public final static String DISPOSALS_EDIT_HTML = "taskplanner/disposals/addUpdate.xhtml";
	public final static String DISPOSALS_VIEW_HTML = "taskplanner/disposals/view.xhtml";
	public final static String DISPOSALS_ADDED_SUCCESS = "disposals_added_success";
	public final static String DISPOSALS_UPDATED_SUCCESS = "disposals_update_success";
	
	
	
	static{
		SUPPORTED_FORMAT = (Map<String,String>)CommonUtils.getBean("FILE_FORMATS");
	}
	
}
