package com.rest.security.auth;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
import com.orb.common.client.utils.CommonUtils;
import com.orb.participant.service.impl.ParticipantServiceImpl;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	private String boolAuthencation = null;
	
	@Autowired
	ParticipantServiceImpl participantServiceImpl;
	
	@Autowired 
	protected CommonError commonError;
	
    @Override
    public Authentication authenticate(Authentication auth) 
      throws AuthenticationException {
        String username = auth.getName();
        String password = auth.getCredentials()
            .toString();
        
        boolAuthencation = participantServiceImpl.getAuthenticationDetails(commonError,username, password);
        
        if (CommonConstants.TRUE.equals(CommonUtils.getValidString(boolAuthencation))) {
            return new UsernamePasswordAuthenticationToken
              (username, password, Collections.emptyList());
        } else {
            throw new
              BadCredentialsException("External system authentication failed");
        }
    }
 
    @Override
    public boolean supports(Class<?> auth) {
        return auth.equals(UsernamePasswordAuthenticationToken.class);
    }
}
