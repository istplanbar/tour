package com.rest.security.auth;

import java.io.IOException;
import java.util.Base64;
import java.util.regex.PatternSyntaxException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

public class AuthenticationTokenProcessingFilter extends GenericFilterBean{
	
	private static Log logger = LogFactory.getLog(AuthenticationTokenProcessingFilter.class);
	
	AuthenticationManager authManager;
	
	public AuthenticationTokenProcessingFilter(AuthenticationManager authManager) {
		this.authManager = authManager;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		try {
			String authorisation = ((HttpServletRequest) request).getHeader("Authorization");
			if(authorisation == null){
				throw new Exception("EmptyAuthStringException");
			}
			
			authorisation = authorisation.replace("Basic ", "");
			
			/*byte[]  bytesEncoded = Base64.getEncoder().encode("testuser:testpassword".getBytes());
			String encodedString = new String(bytesEncoded);
			System.out.println(encodedString);*/
			
			byte[] decodedByte = Base64.getDecoder().decode(authorisation);
			String decodedString = new String(decodedByte);
			
			if( !decodedString.contains(":") ){
				throw new Exception("InvalidAuthStringException");
			}
			
			String decryptedStringArr[] = decodedString.split(":");
			
			String userName = decryptedStringArr[0];
			String password = decryptedStringArr[1];
			
			
			
			// build an Authentication object with the user's info
			UsernamePasswordAuthenticationToken authentication = 
					new UsernamePasswordAuthenticationToken(userName, password);
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
			
			// set the authentication into the SecurityContext
			//SecurityContextHolder.getContext().setAuthentication(authManager.authenticate(authentication)); 
			
			chain.doFilter(request, response);
			
		}catch (BadCredentialsException e) {
			logger.info("Bad Credentials!!"+e.getMessage());
		}catch (PatternSyntaxException e) {
			logger.info("Delimiter issue"+e.getMessage());
		}catch (Exception e) {
			logger.info("Authorization key is not found in the header"+e.getMessage());
		}
		// continue thru the filter chain
		/*if(CommonConstants.TRUE.equals(boolAuthencation)){
		   
		}else{
			logger.info("CustomAuthenticationEntryPoint : Unauthorized: Authentication token was either missing or invalid.");
			ObjectMapper mapper = new ObjectMapper();
			((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(mapper.writeValueAsString("Invalid UserName and Password"));
		}*/
		
	}
}

