package com.rest.security.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint{
private static Log logger = LogFactory.getLog(CustomAuthenticationEntryPoint.class);

public void commence(HttpServletRequest request, HttpServletResponse response,
		AuthenticationException authException) throws IOException, ServletException {
	
	logger.info("CustomAuthenticationEntryPoint : Unauthorized: Authentication token was either missing or invalid.");
	ObjectMapper mapper = new ObjectMapper();
	response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	response.setContentType("application/json");
	response.setCharacterEncoding("UTF-8");
	response.getWriter().write(mapper.writeValueAsString("Unbefugt: Authentifizierungs-Token fehlt oder ist ung�ltig. Unbefugter Zugriff auf Web-Services."));
	}
}
