package com.orb.security.tag;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.orb.security.UserPrincipal;
import com.orb.security.util.FastStringTokenizer;

/**
 * @author 
 *
 */
public class IsUserInRoleTag extends TagSupport {

	private static final long serialVersionUID = -7753845621576417521L;

	protected String[] roleNames = null;

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {

		for (int i = 0; i < this.roleNames.length; i++) {

			HttpSession session = ((HttpServletRequest) this.pageContext
					.getRequest()).getSession();
			boolean result = isUserInRole(session, this.roleNames[i]);

			if (result)
				return EVAL_BODY_INCLUDE;
		}
		return SKIP_BODY;
	}

	/**
	 * @param roles
	 */
	public void setRoles(final String roles) {
		if (roles == null) {
			this.roleNames = new String[0];
			return;
		}
		ArrayList<String> list = new ArrayList<String>();
		FastStringTokenizer fst = new FastStringTokenizer(roles, ',');
		while (fst.hasMoreTokens()) {
			list.add(fst.nextToken().trim());
		}
		this.roleNames = list.toArray(new String[list.size()]);
	}

	/**
	 * @param session
	 * @param role
	 * @return
	 */
	public boolean isUserInRole(HttpSession session, final String role) {
		UserPrincipal user = (UserPrincipal) session
				.getAttribute("UserPrincipal");
		if (user == null) {
			return false;
		}
		List<String> userRoles = user.getGroups();

		if (userRoles == null) {
			return false;
		}
		return userRoles.contains(role);
	}

}
