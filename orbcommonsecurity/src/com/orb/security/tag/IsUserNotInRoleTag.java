package com.orb.security.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;

/**
 * @author 
 *
 */
public class IsUserNotInRoleTag extends IsUserInRoleTag {

	private static final long serialVersionUID = 1028075903712315506L;

	/* (non-Javadoc)
	 * @see 
	 */
	@Override
	public int doStartTag() throws JspException {
		boolean result = false;
		for (int i = 0; i < this.roleNames.length; i++) {

			HttpSession session = ((HttpServletRequest) this.pageContext
					.getRequest()).getSession();
			result = isUserInRole(session, this.roleNames[i]);

			if (result)
				return SKIP_BODY;
		}
		return EVAL_BODY_INCLUDE;

	}

}
