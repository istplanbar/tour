package com.orb.security.encrypt;

import java.util.Date;
import java.util.Random;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptionUtil {
	public static String encrypt(String password) {
	       Random random = new Random((new Date()).getTime());
	     BASE64Encoder encoder = new BASE64Encoder();
	     byte[] salt = new byte[8];
	     random.nextBytes(salt);
	     return encoder.encode(salt)+
	       encoder.encode(password.getBytes());
	}

    public static String decrypt(String encryptKey) {
	     if (encryptKey.length() > 12) {
	       String cipher = encryptKey.substring(12);
	       BASE64Decoder decoder = new BASE64Decoder();
	       try {
	         return new String(decoder.decodeBuffer(cipher));
	       } catch (Exception e) {
	             return null;
	       }
	     }
	       return null;
	}
    
	public static void main(String args[]){
		String pass = encrypt("kurver!#2015");
		System.out.println(pass);
		System.out.println(decrypt(pass));
	}
}
