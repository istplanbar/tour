package com.orb.security;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 
 * pojo for login module
 */
public class UserPrincipal implements Serializable {

	private static final long serialVersionUID = -5562582565346570640L;

	private String id;
	private String userId;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String language;
	private final List<String> groups = new ArrayList<String>();

	public UserPrincipal() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<String> getGroups() {
		return groups;
	}

	public boolean addGroup(final String name) {
		return this.groups.add(name);
	}

}
