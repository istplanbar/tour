package com.orb.security.login;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.orb.admin.client.service.AdminService;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.error.CommonError;
 
 
@Service
public class CustomUserDetailsService implements UserDetailsService {
     
	@Autowired
    private UserDAO userDAO;   
	
	
	private CommonError commonError;
	
	@Autowired 
	protected AdminService adminService;
	
    @Override
    public UserDetails loadUserByUsername(String loginUser)
            throws UsernameNotFoundException {
    	
    	 Map<String, String> adminConfMapForPass = null;
		 adminConfMapForPass = adminService.getAdminConfMap(commonError);
	     String passwordValidation = adminConfMapForPass.get("passwordChangeValid");
	     
    	com.orb.common.client.model.User domainUser = userDAO.getLoginUser(loginUser);
    	if(domainUser == null)
    		throw new UsernameNotFoundException("User is null");
    	
    	boolean enabled = true;
    	boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        
        Date today = new Date();
        Date tempDate = domainUser.getLastPwdChange();
        if( tempDate == null ) tempDate = domainUser.getCreatedDate();
        long passwordExpiry = (today.getTime() - tempDate.getTime())/ (1000 * 60 * 60 * 24);
        
    	if(today.compareTo(domainUser.getUserExpiry()) >= 0 && !getFormattedDate(today).equals(getFormattedDate(domainUser.getUserExpiry())) 
    			&& passwordValidation.equals("Yes")){
    		accountNonExpired = false;
    	}
    	
    	if(passwordValidation.equals("Yes")){
    	if( passwordExpiry >= CommonConstants.USER_LOCKED_IN_DAYS){
			if(passwordExpiry >= CommonConstants.USER_EXPIRY_IN_DAYS){
				credentialsNonExpired = false;
			}else{
				accountNonLocked = false;
			}
		}
    	}
        return new User(
        		loginUser, 
                domainUser.getPassword(), 
                enabled, 
                accountNonExpired, 
                credentialsNonExpired, 
                accountNonLocked,
                getAuthorities(domainUser.getRoleId())
        );
    }
     
    public Collection<? extends GrantedAuthority> getAuthorities(Integer role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
        return authList;
    }
     
    public List<String> getRoles(Integer role) {
 
        List<String> roles = new ArrayList<String>();
 
        if (role.intValue() == 1) {
            roles.add("ROLE_MODERATOR");
            roles.add("ROLE_ADMIN");
        } else if (role.intValue() == 2) {
            roles.add("ROLE_MODERATOR");
        }
        return roles;
    }
     
    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
         
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
    private Date getFormattedDate(Date formattedDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dbDateFormat = sdf.format(formattedDate);
		Date date = null;
		try {
			date = sdf.parse(dbDateFormat);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return date;
    }

}