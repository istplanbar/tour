package com.orb.security.login;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.orb.admin.client.dao.AdminDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.utils.CommonUtils;

public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private String formUsernameKey = UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;
    @Autowired
    private UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter;
    
    com.orb.common.client.model.User failedUser = new com.orb.common.client.model.User(); 
    
    @Autowired
	CommonDAO commonDAO;
    
    @Autowired
	AdminDAO adminDAO;

    //Failure logic:
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        
    	String usernameParameter = usernamePasswordAuthenticationFilter.getUsernameParameter();
	    String username = request.getParameter( usernameParameter );
	    HttpSession session = request.getSession(); 
	    session.setAttribute( CommonConstants.SPRING_SECURITY_LAST_USERNAME, username );
	    session.setAttribute( CommonConstants.SPRING_SECURITY_LAST_EXCEPTION, exception );
	    Map<String, String> adminConfMap = adminDAO.getAdminConfMap(null);
	    if(!CommonUtils.getValidString(username).isEmpty()){
        	failedUser.setUserId(username);
        	String ipAddress = getClientIpAddr(request);
            if(!CommonUtils.getValidString(ipAddress).isEmpty()){
            	String ipAddressLog = getFilteredIpAddress(adminConfMap,ipAddress);
            	failedUser.setIpAddress(ipAddressLog);
            }
        	commonDAO.loginAuditTrail(failedUser, CommonConstants.LOGIN_FAILED);
        }
    	super.onAuthenticationFailure(request, response, exception);
    }

     private String getFilteredIpAddress(Map<String, String> adminConfMap, String ipAddress) {
    	if("2".equals(adminConfMap.get("ipAddressLog"))){
    		 int index= ipAddress.lastIndexOf(".");
    		 ipAddress = ipAddress.substring(0, index)+".*";
    	}else if("3".equals(adminConfMap.get("ipAddressLog"))){
    		 ipAddress = "";
    	}
		return ipAddress;
	}

	public String getFormUsernameKey() {
        return formUsernameKey;
    }

    public void setFormUsernameKey(String formUsernameKey) {
        this.formUsernameKey = formUsernameKey;
    }
    
    private String getClientIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_FORWARDED");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_VIA");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("REMOTE_ADDR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }
}
