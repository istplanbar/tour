package com.orb.security.login;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Service;

import com.orb.admin.client.dao.AbstractAdminDAO;
import com.orb.admin.client.dao.AdminDAO;
import com.orb.admin.client.model.MetaTable;
import com.orb.admin.moduleactivity.model.ModuleActivity;
import com.orb.admin.modulemanagement.dao.ModuleManagementDAO;
import com.orb.admin.roleadministration.dao.RoleManagementDAO;
import com.orb.admin.roleadministration.model.Role;
import com.orb.admin.useradministration.dao.UserDAO;
import com.orb.common.client.constants.CommonConstants;
import com.orb.common.client.dao.CommonDAO;
import com.orb.common.client.utils.CommonUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler, ApplicationContextAware{
	private static Log logger = LogFactory.getLog(CustomAuthenticationSuccessHandler.class);
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	private ApplicationContext applicationContext;
	
	@Autowired
	UserDAO userDAO;
	@Autowired
    RoleManagementDAO roleManagementDAO;
	@Autowired
	ModuleManagementDAO moduleManagementDAO;
	@Autowired
	AdminDAO adminDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	AbstractAdminDAO abstractAdminDAO;
		 
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }
 
    @SuppressWarnings("unchecked")
	protected void handle(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
        String targetUrl = determineTargetUrl(authentication);
 
        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
        User user = (User)authentication.getPrincipal();
        com.orb.common.client.model.User loginUser = userDAO.getLoginUser(user.getUsername());
        List<Role> userRoles = userDAO.getUserRoles(null,loginUser.getId());
        IntStream roleIdStream = userRoles.stream().mapToInt(Role::getRoleId);
        List<Integer> roleIdList = roleIdStream.boxed().collect(Collectors.toList());
		if(roleIdList == null) roleIdList = new ArrayList<>();
        List<Role> roleMapList = new ArrayList<Role>();
        for(Role role : userRoles){
        	roleMapList = roleManagementDAO.getRoleModMapList(null, role.getIdentifier());
        	for(Role roleMaps : roleMapList){
        		if(roleMaps.getActivities() != null && !roleMaps.getActivities().isEmpty()){
        			roleMaps.setActivityList(moduleManagementDAO.getModuleActivity(null, roleMaps.getActivities()));	
   				}
        		roleMaps.setActivity(role.getActivityList().stream()
   					 .map(ModuleActivity::getActivityName)
   					 .collect(Collectors.joining(", ")));
        	}
        }
       logger.info(roleMapList);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (null == loginUser){
        	logger.error("loginUser details returns Null");
	    } else{
	    	if(loginUser.getStartDate()!= null && loginUser.getStartDate().after(date)){
	    		HttpSession session = request.getSession();
	    		session.setAttribute(CommonConstants.USER_START_DATE,loginUser.getStartDate());
	    		targetUrl = CommonConstants.LOGIN_VALIDATION_PAGE;
	    	}else{
	    	Map<String, String> adminConfMap = adminDAO.getAdminConfMap(null);
	        Set<String> tablePageTemplates = (Set<String>) applicationContext.getBean("tableDisplayPageTemplates");
	        if( tablePageTemplates == null ){
	        	loginUser.setActualDisplayRowsSize("5,10,25,50,100,250");
	        	}else{
	        	StringBuffer tempStr = new StringBuffer();
	        	if( !"".equals(CommonUtils.getValidString(loginUser.getDisplayRowsSize())) &&  !tablePageTemplates.contains(loginUser.getDisplayRowsSize()) ){
	        		tempStr.append(loginUser.getDisplayRowsSize());
	    			tempStr.append(",");
	        	}
	        	else if("".equals(CommonUtils.getValidString(loginUser.getDisplayRowsSize()))){
	        		loginUser.setDisplayRowsSize("5");
	        	}
	        	for( String pageTemplate : tablePageTemplates ){
	    			tempStr.append(pageTemplate);
	    			tempStr.append(",");
	    		}
	        	tempStr.setLength( tempStr.length()-1 );
	        	loginUser.setActualDisplayRowsSize(tempStr.toString());
	        }
	        HttpSession session = request.getSession();
	        String ipAddress = getClientIpAddr(request);
	        String ipAddressLog = getFilteredIpAddress(adminConfMap,ipAddress);
	        loginUser.setIpAddress(ipAddressLog);
            if(!"".equals(CommonUtils.getValidString(loginUser.getTimeZone()))){
		        	loginUser.setTimeZone(loginUser.getTimeZone().substring(0, loginUser.getTimeZone().indexOf("(")).trim());
		    }
	        
	        Locale locale = new Locale(loginUser.getLanguage());
	        session.setAttribute( CommonConstants.USER_LOCALE, locale );
	        session.setAttribute( CommonConstants.USER_IN_CONTEXT, loginUser );
	        session.setAttribute( CommonConstants.ADMIN_CONF, adminConfMap );
	        session.setAttribute( CommonConstants.COMPANY_NAME, adminConfMap.get("companyName") );
            if(roleIdList.stream().anyMatch(role -> role == 1)){
		        	session.setAttribute( CommonConstants.SHOW_ADMIN_BUTTON , CommonConstants.TRUE);
				}
		        
		        List<MetaTable> mainModules =  moduleManagementDAO.getAllMetaTableList(null);
		        session.setAttribute(CommonConstants.TASK_PLANNER_ACTIVE, mainModules.stream().filter(x -> x.getPageId() == 33 && CommonConstants.Y.equals(x.getActive())).map(i -> CommonConstants.TRUE).findAny().orElse(null));
	        HashMap<Integer, String> subModuleMap = abstractAdminDAO.getModulePermissions(loginUser.getId());
	        if (null == subModuleMap){
	        	logger.warn("subModuleMap details returns Null");
	        }
	        session.setAttribute( CommonConstants.SUB_MODULE_MAP, subModuleMap);
	        if(loginUser != null){
		        commonDAO.loginAuditTrail(loginUser, CommonConstants.LOGGED_IN);
		    }
	        SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request,response);
	        if( savedRequest != null ){
	        	String savedUrl = savedRequest.getRedirectUrl();
	        	logger.info(request.getContextPath());
	        	if( savedUrl.contains(request.getContextPath() + "/common/") || savedUrl.contains(request.getContextPath() + "/views/") ){
	        		if( !savedUrl.endsWith(request.getContextPath()+"/index.jsp") ){ // No need to use the Saved URL if its Login page.
	            		try {
	                        URL url = new URL(savedUrl);
	                        targetUrl =  url.getFile().substring(
	                                request.getContextPath().length());
	                    } catch (Exception e) {
	                    	logger.error("Exception occured when try to get save URL from Session. Exception ==> " + e.getMessage());
	                    }
	            	}
	        	}
	        	
	        }
	        
	        String authUserType = CommonUtils.getValidString(loginUser.getAuthenticateUser());
	        String privilege = CommonUtils.getValidString(loginUser.getSecondLevelAuthUserPrivilege());
	        logger.info("precheck the condition" +privilege);
	        if((CommonConstants.TRUE.equals(privilege) && (!authUserType.isEmpty()))||((CommonConstants.FALSE.equals(privilege)) && (!authUserType.isEmpty()))){	        	
	        	logger.info("inside if.. Privilege : " +privilege);
	        	if((CommonConstants.MAIL).equalsIgnoreCase(authUserType)){
	        		session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
		        	   targetUrl = CommonConstants.SECOND_LEVEL_AUTH; 
	        	}else if( (CommonConstants.SMS).equalsIgnoreCase(authUserType)){
	        		session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
		        	   targetUrl = CommonConstants.SECOND_LEVEL_AUTH;
	        	}else if (CommonConstants.GOOGLE_AUTH.equals(authUserType)){
	        		session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
		        	targetUrl = CommonConstants.GOOGLE_AUTH_ENTRY;
	        	}
	        	
	        }else if(authUserType.isEmpty()||privilege.isEmpty()){
	        	logger.info("inside else.. Auth User Type : " +authUserType+" Privilege: "+privilege);
	        	 if((CommonConstants.MAIL).equalsIgnoreCase(adminConfMap.get("secondLevelAuth"))){
				        	   session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
				        	   targetUrl = CommonConstants.SECOND_LEVEL_AUTH; 
				  }else if((CommonConstants.SMS).equalsIgnoreCase(adminConfMap.get("secondLevelAuth")) ){
				        		session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
				        	   targetUrl = CommonConstants.SECOND_LEVEL_AUTH; 
				  }else if (CommonConstants.GOOGLE_AUTH.equalsIgnoreCase(adminConfMap.get("secondLevelAuth"))){
				        	session.setAttribute( CommonConstants.CACHED_URL, targetUrl);
				        	targetUrl = CommonConstants.GOOGLE_AUTH_ENTRY; 
				  }
	        }else { 
	        	logger.info("outer else.. Privilege : " +privilege);
	        	session.setAttribute( CommonConstants.CACHED_URL, null);
	        }
		      
	    	}
			redirectStrategy.sendRedirect(request, response, targetUrl);
	    }
    }
 
    private String getFilteredIpAddress(Map<String, String> adminConfMap, String ipAddress) {
    	if("2".equals(adminConfMap.get("ipAddressLog"))){
    		 int index= ipAddress.lastIndexOf(".");
    		 ipAddress = ipAddress.substring(0, index)+".*";
    	}else if("3".equals(adminConfMap.get("ipAddressLog"))){
    		 ipAddress = "";
    	}
		return ipAddress;
	}

	/** Builds the target URL according to the logic defined in the main class Javadoc. */
    @SuppressWarnings("unused")
	protected String determineTargetUrl(Authentication authentication) {
        boolean isUser = false;
        boolean isAdmin = false;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
                isUser = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                isAdmin = true;
                break;
            }
        }
        return "/common/process/dashboard/loadDashboard.html";
      
    }
 
    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
 
    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
    private String getClientIpAddr(HttpServletRequest request) {  
        String ip = request.getHeader("X-Forwarded-For");  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_FORWARDED");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_CLIENT_IP");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_FORWARDED_FOR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_FORWARDED");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("HTTP_VIA");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("REMOTE_ADDR");  
        }  
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getRemoteAddr();  
        }  
        return ip;  
    }

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		
	}
    
}
