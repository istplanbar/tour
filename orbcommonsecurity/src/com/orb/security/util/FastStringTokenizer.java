package com.orb.security.util;

import java.util.NoSuchElementException;

/**
 * @author 
 *
 */
public class FastStringTokenizer {

	private final char delimter;
	private final String text;
	private final int length;
	private int pos;

	/**
	 * @param text
	 * @param delimiter
	 */
	public FastStringTokenizer(final String text, final char delimiter) {
		this.delimter = delimiter;
		this.text = text;
		this.length = text.length();
		if (this.length > 0 && text.charAt(0) == delimiter) {
			this.pos = 1;
		} else {
			this.pos = 0;
		}
	}

	/**
	 * @return
	 */
	public boolean hasMoreTokens() {
		return !(this.pos >= this.length);
	}

	/**
	 * @return
	 */
	public String nextToken() {
		if (this.pos >= this.length) {
			throw new NoSuchElementException();
		}
		int start = this.pos;
		while ((this.pos < this.length)
				&& (this.text.charAt(this.pos) != this.delimter)) {
			this.pos++;
		}
		String token = this.text.substring(start, this.pos);
		this.pos++;

		return token;
	}

}
